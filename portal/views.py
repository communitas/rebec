# -*- coding: utf-8 -*-
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.views.generic import ListView, TemplateView
from django.views.generic.base import TemplateView
from django.utils.translation import ugettext as _
from clinical_trials.models import ClinicalTrial
import json

from django import http
from django.conf import settings
from django.utils import timezone
from django.utils.http import is_safe_url
from django.utils.translation import check_for_language, to_locale, get_language


def handler404(request):
    response = render_to_response('base/404.html', {},
                                  context_instance=RequestContext(request))
    response.status_code = 404
    return response

def handler500(request):
    response = render_to_response('base/500.html', {},
                                  context_instance=RequestContext(request))
    response.status_code = 500
    return response

def handler403(request):
    response = render_to_response('base/403.html', {},
                                  context_instance=RequestContext(request))
    response.status_code = 403
    return response

def set_language(request):
    """
    Redirect to a given url while setting the chosen language in the
    session or cookie. The url and the language code need to be
    specified in the request parameters.

    Since this view changes how the user will see the rest of the site, it must
    only be accessed as a POST request. If called as a GET request, it will
    redirect to the page in the request (the 'next' parameter) without changing
    any state.
    """
    next = request.REQUEST.get('next')
    if not is_safe_url(url=next, host=request.get_host()):
        next = request.META.get('HTTP_REFERER')
        if not is_safe_url(url=next, host=request.get_host()):
            next = '/'
    response = http.HttpResponseRedirect(next)
    if request.method == 'POST':
        lang_code = request.POST.get('language', None)
        if lang_code and check_for_language(lang_code):
            next_url = next.split('/')
            if next_url[1] and check_for_language(next_url[1]):
                next_url[1] = lang_code
                next = '/'.join(next_url)
                response = http.HttpResponseRedirect(next)
            if hasattr(request, 'session'):
                request.session['django_language'] = lang_code
            else:
                response.set_cookie(settings.LANGUAGE_COOKIE_NAME, lang_code)
    return response


class ClinicalTrialRegisteredList(ListView):
    """
    Clinical trial registered list view
    """
    model = ClinicalTrial
    template_name = 'portal/ct_registered_list.html'
    paginate_by = 10

    def get_queryset(self):
        """
        Get only the registered clinical trials.
        But get it, considering the study_final_date attribute, that must
        not be greather than today.
        """
        return ClinicalTrial.objects.registered().filter(
                study_final_date__gte=timezone.now()
               ).order_by("-date_last_publication")


class ClinicalTrialRecruitingList(ListView):
    """
    Clinical trial recruiting list view
    """
    model = ClinicalTrial
    template_name = 'portal/ct_recruiting_list.html'
    paginate_by = 10

    def get_queryset(self):
        """Get only the recruiting clinical trials."""
        return ClinicalTrial.objects.recruiting().order_by(
                "-date_last_publication")


class ClinicalTrialSummaryView(TemplateView):

    template_name = "portal/ct_graph_summay.html"

    def get_context_data(self, **kwargs):
        context = super(ClinicalTrialSummaryView, self).get_context_data(**kwargs)

        registered = ClinicalTrial.objects.registered().count()
        recruiting = ClinicalTrial.objects.recruiting().count()
        analysing = ClinicalTrial.objects.analysing().count()
        drafting = ClinicalTrial.objects.drafting().count()

        dataset1 = [
            {
                u'label': _("Registered"),
                u'color': "#008000",
                u'data': registered
            },
            {
                u'label': _("Analysis"),
                u'color': "#CC0000",
                u'data': analysing
            },
            {
                u'label': _("Draf"),
                u'color': "#4169E1",
                u'data': drafting
            }
         ]

        dataset2 = [
            {
                u'label': _("Recruiting"),
                u'color': "#008000",
                u'data': recruiting
            },
            {
                u'label': _("Not Recruiting"),
                u'color': "#CC0000",
                u'data': registered - recruiting
            }
         ]

        options1 = {
            u'series': {
                u'pie': {
                    u'show': True,
                    u'label': {
                        u'show': True
                    },
                    u'radius': .8
                }
            },
            u'legend': {
                u'show': True,
                u'labelBoxBorderColor': "#ffffff",
                u'position': "ne",
                u'color': "#000000",
            },
            u'grid': {
                u'hoverable': True,
                u'clickable' : True
            }
        }

        options2 = {
            u'series': {
                u'pie': {
                    u'show': True,
                    u'label': {
                        u'show': True
                    },
                    u'radius': 0.73
                }
            },
            u'legend': {
                u'show': True,
                u'labelBoxBorderColor': "#ffffff",
                u'position': "ne",
                u'color': "#000000",
            },
            u'grid': {
                u'hoverable': True,
                u'clickable' : True
            }
        }

        context['graph1'] = {u'options': json.dumps(options1), u'dataset': json.dumps(dataset1)}
        context['graph2'] = {u'options': json.dumps(options2), u'dataset': json.dumps(dataset2)}
        return context


class SearchView(TemplateView):
    template_name='portal/search.html'

    def get_context_data(self):
        from workflows.models import StateObjectRelation, State
        from django.db.models import Q
        context = super(SearchView, self).get_context_data()
        q = self.request.GET.get(u'q', '')
        if q:
            published_pks = StateObjectRelation.objects.filter(
                state=State.STATE_PUBLICADO
            ).values_list(u'content_id', flat=True)
            context[u'trials'] = ClinicalTrial.objects.filter(
                                    pk__in=published_pks
                                )
            context[u'trials'] = context[u'trials'].filter(
                   Q(code__icontains=q) | Q(ctdetail__public_title__icontains=q)
            ).values(u'code', 'ctdetail__public_title').distinct(u'pk')

        context[u'search_query'] = q
        return context
