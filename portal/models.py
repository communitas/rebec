from django.db import models
from cms.models import CMSPlugin
from django.utils.translation import ugettext_lazy as _


class LatestClinicalTrialPlugin(CMSPlugin):
    """
        Model for the settings when using the latest Clinical Trials cms plugin
    """
    limit = models.PositiveIntegerField(
        verbose_name=_('Number of Clinical Trial items to show'),
        help_text=_('Limits the number of items that will be displayed'))


class BarraBrasil(models.Model):
    """
        Model for Barra Brasil
    """

    class Meta:
        verbose_name = _(u'Brazil Bar')
        verbose_name_plural = _(u'Brazil Bar')

    option = models.CharField(
        choices=(
            ('disable', _('disable')),
            ('link', _('link')),
            ('file', _('file'))
        ),
        max_length=10,
        verbose_name=_('Active Brazil Bar'),
        )

    link_javascript = models.CharField(
        null=True,
        blank=True,
        max_length=150,
        verbose_name=_('Link to Brazil Bar'),
        default='http://barra.brasil.gov.br/barra.js'
        )

    file_javascript = models.FileField(
        upload_to=u'static',
        null=True,
        blank=True,
        verbose_name=_('JavaScript for Brazil Bar'),
        )

    def __unicode__(self):
        return u'%s' % self.pk


class PortalScripts(models.Model):
    """
        Model to add JavaScripts.
    """
    class Meta:
        verbose_name = _(u'Portal JavaScript')
        verbose_name_plural = _(u'Portal JavaScript')

    title = models.CharField(
        blank=False,
        max_length=150,
        verbose_name=_('Title'),
        )

    enabled = models.BooleanField(
        default=False,
        blank=False,
        verbose_name=_('Enabled'))

    javascript = models.TextField(
        blank=False,
        max_length=1500,
        verbose_name=_('JavaScript'),
        )

    def __unicode__(self):
        return u'%s' % self.title
