from django.utils.translation import ugettext_lazy as _

from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool

from portal.models import LatestClinicalTrialPlugin
from clinical_trials.models import ClinicalTrial


class CMSLatestClinicalTrialPlugin(CMSPluginBase):
    """
        Plugin class for the latest Clinical Trials
    """
    model = LatestClinicalTrialPlugin
    name = _('Latest Clinical Trials')
    render_template = "portal/latest_ct.html"

    def render(self, context, instance, placeholder):
        """
            Render the latest Clinical Trials
        """
        registered = ClinicalTrial.objects.registered().order_by("-date_last_publication")[:instance.limit]
        recruiting = ClinicalTrial.objects.recruiting().order_by("-date_last_publication")[:instance.limit]
        context.update({
            'instance': instance,
            'registered': registered,
            'recruiting': recruiting,
            'placeholder': placeholder,
        })
        return context


plugin_pool.register_plugin(CMSLatestClinicalTrialPlugin)
