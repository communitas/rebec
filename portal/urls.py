from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView
from portal import views

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'rebec.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^$',
        HelpView.as_view(), name=u'home'),
)
