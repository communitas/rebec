from django.contrib import admin
from portal.models import BarraBrasil, PortalScripts

admin.site.register(BarraBrasil)
admin.site.register(PortalScripts)
