from django import template
from clinical_trials.models import ClinicalTrial
from portal.models import BarraBrasil, PortalScripts

register = template.Library()

@register.inclusion_tag('portal/ct_summay.html')
def ct_summary():
    """
        Get registered, analysing, drafting and recruiting Clinical Trial
    """

    registered = ClinicalTrial.objects.registered().count()
    recruiting = ClinicalTrial.objects.recruiting().count()
    analysing = ClinicalTrial.objects.analysing().count()
    drafting = ClinicalTrial.objects.drafting().count()
    total = registered + analysing + drafting

    return {
        'registered': registered,
        'recruiting': recruiting,
        'analysing': analysing,
        'drafting': drafting,
        'total': total,
    }


@register.inclusion_tag('portal/barra_brasil.html')
def barra_brasil():
    """
        Tag for Barra Brasil
    """
    object = BarraBrasil.objects.first()
    value = {}
    if object:
        if object.option == 'link':
            value = {'javascript': object.link_javascript}
        elif object.option == 'file':
            value = {'javascript': object.file_javascript.url}
    return value


@register.inclusion_tag('portal/links_help.html', takes_context=True)
def links_help(context):
    """
    Function that sends to the template list of the available aid
    for that particular profile.

    :param links_help: context.
    :type links_help: context.
    :rtype: list of objects helps and aid list names.
    """
    from administration.models import Help, HelpLanguage, ItemsHelps, Language
    from django.contrib.auth import get_user_model
    User = get_user_model()

    help_l = []
    request = context['request']
    linguagem = context['LANGUAGE_CODE']
    language = Language.objects.get(code=linguagem)
    helps = Help.objects.all()
    help_list = []
    help_list1 = []
    if request.user.is_authenticated():
        user = User.objects.get(id=request.user.id)
        groups = [group.name for group in user.groups.all()]
        if user.is_registrant_administrator():
            groups.append(u'register_admin')
        for help_l in helps:
            for help_user_type in help_l.type_user:
                if  help_user_type in groups:
                    if help_l.url_name not in help_list and 'edit' not in help_l.url_name:
                        help_list.append(str(help_l.url_name))
                    try:
                        helpp = HelpLanguage.objects.get(language=language, help=help_l.url_name)
                    except:
                        helpp = None
                    if helpp:
                        if helpp not in help_list1 and 'edit' not in help_l.url_name:
                            help_list1.append(helpp)

    return {'help':help_list1, 'help_list':help_list}


@register.inclusion_tag('portal/portal_javascripts.html')
def portal_javascripts():
    """
        Tag for JavaScripts
    """
    objects = PortalScripts.objects.all()
    return {'scripts': [item.javascript for item in objects if item.enabled]}


@register.simple_tag()
def url_replace(request, field, value):

    dict_ = request.GET.copy()
    dict_[field] = value
    return dict_.urlencode()
