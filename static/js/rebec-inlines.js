function rebec_inlines(prefix) {
    $.fn.getType = function(){ return this[0].tagName == "INPUT" ? this[0].type.toLowerCase() : this[0].tagName.toLowerCase(); }
    function replaceAll(str, de, para){
        var pos = str.indexOf(de);
        while (pos > -1){
            str = str.replace(de, para);
            pos = str.indexOf(de);
        }
        return (str);
    }
    $(function() {
        $('.form-row.inline.'+prefix).formset({
        prefix: prefix,
        addCssClass:"add-row button-hidden-add-form",
        added: function(form) {
            aaa = form;
            var setup_val = $('#id_form-__prefix__-setup').val();
            var total_forms = parseInt($('#id_'+prefix+'-TOTAL_FORMS').val()) - 1;
            form.attr('data-pk', form.attr('data-pk').replace('__prefix__', total_forms));
            form.find('a[data-toggle=modal]').attr('data-target', form.find('a[data-toggle=modal]').attr('data-target').replace('__prefix__', total_forms));
            form.find('td').each(function(){
                if($(this).attr('data-value') != undefined) {
                    $(this).attr('data-value', $(this).attr('data-value').replace('__prefix__', total_forms));
                }
            });
            form.find('div.modal').attr('id', form.find('div.modal').attr('id').replace('__prefix__', total_forms));
            form.find('button.btn-change-row[data-dismiss=modal]').attr('data-pk', form.find('button.btn-change-row[data-dismiss=modal]').attr('data-pk').replace('__prefix__', total_forms));
            form.find('a[data-toggle=modal]').click();
            form.find("input[id*=setup]").val(setup_val);
        }
        });
    });
    $('body').delegate('.btn-change-row', 'click', function(){
        var pk = $(this).attr('data-pk');
      $('table tr[data-pk='+pk+'] td').each(function(){
        var value = $(this).attr('data-value');
        var current_input = $('#'+value);
        if(current_input.length) {
            if(current_input.getType() == 'text' || current_input.getType() == 'number' || current_input.getType() == 'date') {
                $(this).html(current_input.val());
            }
            if(current_input.getType() == 'select') {
                $(this).html(current_input.find("option[value='"+current_input.val()+"']").html());
            }
            if(current_input.getType() == 'checkbox') {
                if(current_input.prop('checked')) {
                    $(this).html('<span class="glyphicon glyphicon-ok"></span>');
                }
                else {
                    $(this).html('<span class="glyphicon glyphicon-remove"></span>');
                }
            }
        }
      });
    });
    $('table tr td').each(function(){
        var value = $(this).attr('data-value');
        var current_input = $('#'+value);
        if(current_input.length) {
            if(current_input.getType() == 'text' || current_input.getType() == 'number' || current_input.getType() == 'date') {
                $(this).html(current_input.val());
            }
            if(current_input.getType() == 'select') {
                $(this).html(current_input.find("option[value='"+current_input.val()+"']").html());
            }
            if(current_input.getType() == 'checkbox') {
                if(current_input.prop('checked')) {
                    $(this).html('<span class="glyphicon glyphicon-ok"></span>');
                }
                else {
                    $(this).html('<span class="glyphicon glyphicon-remove"></span>');
                }
            }
        }
    });

}