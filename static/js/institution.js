$("#id_search_address").geocomplete()
        .bind("geocode:result", function(event, result){
          var search_result = {};
          $.each(result.address_components, function(index, object){
              var name = object.types[0];
              search_result[name] = object.long_name;
              search_result[name + "_short"] = object.short_name;
          });
          $('#id_postal_address').val(search_result.route + ', ' + search_result.neighborhood);
          $('#id_state').val(search_result.administrative_area_level_1);
          $('#id_city').val(search_result.locality);
          $('#id_country').val(search_result.country_short);
        });


$('span.field-help').tooltip({'trigger': 'click'});
window.onload = function(){
    if ($("#id_type_person").text().length>2){
        if ($("#id_type_person").val() == "PF"){
            $("#cnpj").hide();
            $("#cnpj input").val("");
            $("#cpf").show();
        }
        else{
            $("#cpf").hide();
            $("#cpf input").val("");
            $("#cnpj").show();
        }
    }
}
$(document).delegate("#id_type_person","change", function(){
    if ($("#id_type_person").val() == "PF"){
        $("#cnpj").hide();
        $("#cnpj input").val("");
        $("#cpf").show(300);
    }
    else{
        $("#cpf").hide();
        $("#cpf input").val("");
        $("#cnpj").show(300);
    }
  
});