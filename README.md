# ReBEC #

* Este repositório contém código-fonte aberto que substitui a versão atual do OpenTrials, usado nesta data, 16/10/2015, como plataforma para o Registro Brasileiro de Ensaios Clínicos - Rebec. Esta nova versão foi desenvolvida pelo Instituto Communitas e já foi testada pela equipe do Rebec em parceria com a Tostes Treinamentos. A fase de testes ainda continua sendo feita por grupos de usuários. A versão final está prevista para lançamento em abril de 2016.
* Conheça mais sobre o ReBEC visitando a [página institucional](http://ensaiosclinicos.gov.br/) e os registros primários públicos e gratuitos na página da [International Clinical Trials Research Platform](http://www.who.int/ictrp/en/).
* VERSÃO BETA.

# ReBEC #
 
* This repository contains source code designed to replace the current OpenTrials version used today, 10.16.2015, as a platform by the Brazilian Registry of Clinical Trials – Rebec. This new version was developed by Communitas Institute, and has already been tested by Rebec’s team in partnership with Tostes Training, but is still going through tests by users. The final version is scheduled for release in April 2016.
* Get to know more about ReBEC visiting our [home page](http://ensaiosclinicos.gov.br/), and more about public, free primary registries in the [International Clinical Trials Research Platform’s site](http://www.who.int/ictrp/en/).
* BETA VERSION.


# CREDITS #


## Authors ##

[![communitas_mini3.png](https://bitbucket.org/repo/eR4eyA/images/230298447-communitas_mini3.png)](http://www.communitas.org.br/)

## Contributors ##