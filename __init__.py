#-*-coding: utf-8 -*-
"""
Rebec Project

:Ajax Search Widget:
The default ajax search widget for this projet is the select2 jquery plugin http://ivaynberg.github.io/select2/,
used toguether with the django_select2 django app https://github.com/applegrew/django-select2 project.
Docs of django-select2 http://django-select2.readthedocs.org/en/latest/index.html
Sample use is in this project's clinical_trials app, in the Institution foreign key widget, in clinical_trials/forms.py.

"""