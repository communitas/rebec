# -*- coding: utf-8 -*-
from django.test import TestCase
from mock import Mock, MagicMock
from django.contrib.auth.models import Group
from django.contrib.auth import get_user_model
from log.middleware import *
from log.connection import *
from django.test.utils import override_settings
from django.conf import settings
from django.core.management import call_command
from django.db.models import loading
from log.tests.fakeapp.models import TestModel, TestModel2

User = get_user_model()


def create_user(groups=[]):
    """Create a user and add to a group parameter.
    """
    user = User.objects.create_user(
        'user', 'user@email.com',
        'pass', gender=1, race=1, country_id=u'BR')
    user.save()
    for group in groups:
        g = Group.objects.get(name=group)
        g.user_set.add(user)
    return user


class LogModelMiddlewareSignalsTestCase(TestCase):
    """Test LogModel Middleware Signals."""

    @classmethod
    def setUpClass(cls):
        """Register a fake app and a test Model."""
        cls.original_installed_apps = settings.INSTALLED_APPS
        settings.INSTALLED_APPS += ('log.tests.fakeapp',)
        loading.cache.loaded = False
        call_command('syncdb', interactive=False, verbosity=0)
        super(LogModelMiddlewareSignalsTestCase, cls).setUpClass()

    @classmethod
    def tearDownClass(cls):
        """Give back orginal settings."""
        settings.INSTALLED_APPS = cls.original_installed_apps
        loading.cache.loaded = False
        super(LogModelMiddlewareSignalsTestCase, cls).tearDownClass()

    @override_settings(MONGO_DATABASE_NAME='log_test')
    def setUp(self):
        """Mock functions."""
        self.request = Mock()
        self.user = create_user()
        self.request.user = self.user
        self.md = LogModel()
        self.original_router_pre = self.md._router_pre
        self.original_router_post = self.md._router_post
        self.original_router_m2m_changed = self.md._router_m2m_changed
        self.original_register_log = self.md.register_log

    @override_settings(MONGO_DATABASE_NAME='log_test')
    def tearDown(self):
        """Drop Mongo database test and give back original functions Mocked."""
        MONGO_CONNECTION.connection.drop_database('log_test')
        self.md._router_pre = self.original_router_pre
        self.md._router_post = self.original_router_post
        self.md._router_m2m_changed = self.original_router_m2m_changed
        self.md.register_log = self.original_register_log
        self.user.delete()

    @override_settings(MONGO_DATABASE_NAME='log_test')
    def test_middleware_signal(self):
        """Test middleware signal call router."""
        self.original_router_pre = self.md._router_pre
        self.original_router_post = self.md._router_post
        self.original_router_m2m_changed = self.md._router_m2m_changed
        self.md._router_pre = MagicMock()
        self.md._router_post = MagicMock()
        self.md._router_m2m_changed = MagicMock()
        self.md.process_request(self.request)
        delattr(self.request, "user")
        self.md.process_request(self.request)
        setattr(self.request, "user", self.user)

        # insert
        self.instance = TestModel.objects.create(field1='Foo')
        self.assertEqual(self.md._router_pre.call_count, 1)
        self.assertEqual(self.md._router_post.call_count, 1)
        self.assertEqual(self.md._router_m2m_changed.call_count, 0)

        # update
        self.instance.field1 = 'Bar'
        self.instance.save()
        self.assertEqual(self.md._router_pre.call_count, 2)
        self.assertEqual(self.md._router_post.call_count, 2)
        self.assertEqual(self.md._router_m2m_changed.call_count, 0)

        # insert or update ManyToMany Field
        self.instance2 = TestModel2.objects.create(field1='Foo')
        self.instance.many_to_many_field.add(self.instance2)
        # pre_save and post_save are call once
        self.assertEqual(self.md._router_pre.call_count, 3)
        self.assertEqual(self.md._router_post.call_count, 3)
        # m2m_changed is called two times, using action pre_add and post_add
        self.assertEqual(self.md._router_m2m_changed.call_count, 2)

        # delete
        self.instance.delete()
        # router_pre is not called on delete
        self.assertEqual(self.md._router_pre.call_count, 3)
        self.assertEqual(self.md._router_post.call_count, 4)
        self.assertEqual(self.md._router_m2m_changed.call_count, 2)
        self.instance2.delete()

    @override_settings(MONGO_DATABASE_NAME='log_test')
    def test_middleware_router(self):
        """Test middleware router call register_log function."""
        self.original_register_log = self.md.register_log
        self.md.register_log = MagicMock()

        router_pre = curry(self.md._router_pre, self.user)
        router_post = curry(self.md._router_post, self.user)
        router_m2m_changed = curry(self.md._router_m2m_changed, self.user)

        # A project model insert
        post_save.connect(router_post, sender=TestModel)
        self.instance = TestModel.objects.create(field1='Foo')
        self.assertEqual(self.md.register_log.call_count, 1)

        # A project model update
        pre_save.connect(router_pre, sender=TestModel)
        self.instance.field1 = 'Bar'
        self.instance.save()
        self.assertEqual(self.md.register_log.call_count, 2)

        # A project model update ManyToMany Field
        m2m_changed.connect(router_m2m_changed)
        self.instance2 = TestModel2.objects.create(field1='Foo')
        self.instance.many_to_many_field.add(self.instance2)
        self.assertEqual(self.md.register_log.call_count, 3)

        # A project model delete
        post_delete.connect(router_post, sender=TestModel)
        self.instance.delete()
        self.assertEqual(self.md.register_log.call_count, 4)

        # A Django model insert
        post_save.connect(router_post, sender=Group)
        self.group = Group.objects.create(name='Foo')
        self.assertEqual(self.md.register_log.call_count, 4)

        # A Django model update
        pre_save.connect(router_pre, sender=Group)
        self.group.name = 'Bar'
        self.group.save()
        self.assertEqual(self.md.register_log.call_count, 4)

        # A Django model delete
        post_delete.connect(router_post, sender=TestModel)
        self.group.delete()
        self.assertEqual(self.md.register_log.call_count, 4)
