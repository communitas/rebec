# -*- coding: utf-8 -*-
from django.test import TestCase
from mock import Mock, MagicMock
from django.core.files import File
from django.db.models.fields.files import ImageFieldFile, FieldFile
from phonenumber_field.modelfields import PhoneNumberField
from phonenumber_field.phonenumber import PhoneNumber
from django.contrib.auth.models import Group
from django.contrib.auth import get_user_model
from log.middleware import *
from log.connection import *
from log.middleware import (_get_field_values, _audit_model,
                            _coerce_to_bson_compatible)
from django.test.utils import override_settings
from django.conf import settings
from django.core.management import call_command
from django.db.models import loading
from log.tests.fakeapp.models import TestModel, TestModel2
from datetime import datetime, date
from django.utils import timezone
from decimal import Decimal
import shutil
import os
import base64
import ast

User = get_user_model()


def create_user(groups=[]):
    """Create a user and add to a group parameter.
    """
    user = User.objects.create_user(
        'user', 'user@email.com',
        'pass', gender=1, race=1, country_id=u'BR')
    user.save()
    for group in groups:
        g = Group.objects.get(name=group)
        g.user_set.add(user)
    return user


class LogModelMiddlewareLogTestCase(TestCase):
    """Test LogModel Middleware Log on Mongo."""

    @classmethod
    def setUpClass(cls):
        """Register a fake app and a test Model."""
        cls.original_installed_apps = settings.INSTALLED_APPS
        settings.INSTALLED_APPS += ('log.tests.fakeapp',)
        loading.cache.loaded = False
        call_command('syncdb', interactive=False, verbosity=0)
        super(LogModelMiddlewareLogTestCase, cls).setUpClass()

    @classmethod
    def tearDownClass(cls):
        """Give back orginal settings."""
        settings.INSTALLED_APPS = cls.original_installed_apps
        loading.cache.loaded = False
        shutil.rmtree(settings.MEDIA_ROOT+'/log')
        super(LogModelMiddlewareLogTestCase, cls).tearDownClass()

    @override_settings(MONGO_DATABASE_NAME='log_test')
    def setUp(self):
        """Mock functions."""
        self.request = Mock()
        self.user = create_user()
        self.request.user = self.user
        self.md = LogModel()

    @override_settings(MONGO_DATABASE_NAME='log_test')
    def tearDown(self):
        """Drop Mogo database test and give back original functions Mocked."""
        MONGO_CONNECTION.connection.drop_database('log_test')
        self.user.delete()

    @override_settings(MONGO_DATABASE_NAME='log_test')
    def test_mongo_log(self):
        """Test middleware signal call router."""
        self.assertEqual(self.md.process_request(self.request), None)
        instance = TestModel.objects.create(field1='Foo')

        # tests function 'register_log' logging
        self.assertEqual(self.md.register_log(instance, 'insert'), True)

        # tests function 'register_log' no_logging
        no_logging(instance)
        self.assertEqual(self.md.register_log(instance, 'insert'), False)

        # instance with datetime
        date_stamp = timezone.now()
        instance = TestModel.objects.create(field1='Foo', field2='Bar',
                                            date_stamp=date_stamp)

        # tests attr _not_log_fields
        setattr(instance, "_not_log_fields", ('field1',))
        self.assertEqual(self.md.register_log(instance, 'insert'), True)

        # tests attr _not_log_fields with nonexistent field
        setattr(instance, "_not_log_fields", ('field_nonexistent',))
        with self.assertRaises(AttributeError):
            self.md.register_log(instance, 'insert')
        delattr(instance, "_not_log_fields")

        # tests function '_get_field_values'

        # define data
        defined_fields_update = [f.name for f in instance._meta.fields]
        defined_fields = defined_fields_update + [f.name for f in instance._meta.many_to_many]

        data = {
            u'id': instance.pk,
            u'field1': instance.field1,
            u'field2': instance.field2,
            u'date_stamp': instance.date_stamp,
            u'file_field': instance.file_field,
            u'image_field': instance.image_field,
            u'phonenumber_field': instance.phonenumber_field,
            u'many_to_many_field': None,
        }
        none_data = {
            u'id': None,
            u'field1': None,
            u'field2': None,
            u'date_stamp': None,
            u'file_field': None,
            u'image_field': None,
            u'phonenumber_field': None,
            u'many_to_many_field': None,
        }

        # insert
        initial_values, final_values = _get_field_values(
            instance,
            defined_fields, 'insert')
        self.assertEqual(initial_values, none_data)
        self.assertEqual(final_values, data)

        # update
        data_initial = {
            u'id': instance.pk,
            u'field1': instance.field1,
            u'field2': instance.field2,
            u'date_stamp': instance.date_stamp,
            u'file_field': u'',
            u'image_field': u'',
            u'phonenumber_field': instance.phonenumber_field,
        }
        initial_values, final_values = _get_field_values(
            instance,
            defined_fields_update, 'update')
        self.assertEqual(initial_values, data_initial)
        data.pop('many_to_many_field')
        self.assertEqual(final_values, data)

        data['many_to_many_field'] = None

        # delete
        initial_values, final_values = _get_field_values(
            instance,
            defined_fields, 'delete')
        self.assertEqual(initial_values, none_data)
        self.assertEqual(final_values, data)

        # insert or update ManyToMany Field
        instance2 = TestModel2.objects.create(field1='Foo')
        instance3 = TestModel2.objects.create(field1='Bar')
        instance.many_to_many_field.add(instance2)

        data_many_to_many1 = dict([(f.name, [ unicode(item) if getattr(item, '__unicode__', False) else item.pk for item in getattr(instance, f.name).all() ]) for f in instance._meta.many_to_many])
        m2m = {'m2m_model': TestModel2,
               'm2m_value': [instance2.pk,]}
        initial_values, final_values = _get_field_values(
            instance,
            defined_fields, 'update', m2m=m2m)
        self.assertEqual(initial_values, {'many_to_many_field': []})
        self.assertEqual(final_values, data_many_to_many1)

        # Edit ManyToMany field with changes
        instance.many_to_many_field.add(instance3)
        data_many_to_many = dict([(f.name, [ unicode(item) if getattr(item, '__unicode__', False) else item.pk for item in getattr(instance, f.name).all() ]) for f in instance._meta.many_to_many])
        m2m = {'m2m_model': TestModel2,
               'm2m_value': [instance2.pk, instance3.pk]}
        initial_values, final_values = _get_field_values(
            instance,
            defined_fields, 'update', m2m=m2m)
        self.assertEqual(initial_values, {'many_to_many_field': []})
        self.assertEqual(final_values, data_many_to_many)

        # delete
        delete_data = data
        delete_data['many_to_many_field'] = data_many_to_many.get('many_to_many_field')
        initial_values, final_values = _get_field_values(
            instance,
            defined_fields, 'delete')
        self.assertEqual(initial_values, none_data)
        self.assertEqual(final_values, delete_data)

        # tests function '_audit_model'
        data_output = {
            u'id': instance.pk,
            u'field1': instance.field1,
            u'field2': instance.field2,
            u'date_stamp': instance.date_stamp,
            u'file_field': None,
            u'image_field': None,
            u'many_to_many_field': data_many_to_many.get('many_to_many_field'),
        }
        audit_output = {
            'object_pk': instance.pk,
            'date_stamp': date_stamp,
            'data': data_output,
            'user': self.user.username,
            'operation': 'insert',
        }

        # insert
        audit = _audit_model(instance, 'insert', none_data, data, self.user)
        audit['date_stamp'] = date_stamp
        self.assertEqual(audit, audit_output)

        # update
        extra_info = {'obs': 'field1 update for tests'}
        data_updated = {'field1': 'Baz'}
        data_updated.update(extra_info)

        audit_output_updated = audit_output.copy()
        audit_output_updated.update({
            'data': data_updated,
            'operation': 'update',
        })

        audit = _audit_model(instance, 'update', data, data_updated,
                             self.user, **extra_info)
        audit['date_stamp'] = date_stamp
        self.assertEqual(audit, audit_output_updated)

        # update ManyToMany whith changes
        data_updated = {'many_to_many_field': data_many_to_many.get('many_to_many_field')}
        data_updated1 = {'many_to_many_field': data_many_to_many1.get('many_to_many_field')}

        audit_output_updated = audit_output.copy()
        audit_output_updated.update({
            'data': data_updated1,
            'operation': 'update',
        })

        audit = _audit_model(instance, 'update', data_updated, data_updated1, self.user)
        audit['date_stamp'] = date_stamp
        self.assertEqual(audit, audit_output_updated)

        # update ManyToMany whith no changes
        audit = _audit_model(instance, 'update', data_updated, data_updated, self.user)
        self.assertEqual(audit, None)

        # delete
        audit_output['operation'] = 'delete'
        audit = _audit_model(instance, 'delete', none_data, data, self.user)
        audit['date_stamp'] = date_stamp
        self.assertEqual(audit, audit_output)

        # no update
        audit = _audit_model(instance, 'update', data, data, self.user)
        self.assertEqual(audit, None)

        # test function '_coerce_to_bson_compatible'

        # decimal
        dec = Decimal(55.555555)
        self.assertTrue(isinstance(dec, Decimal))
        self.assertTrue(isinstance(_coerce_to_bson_compatible(dec), float))

        # date
        dat = date.today()
        self.assertTrue(isinstance(dat, date))
        self.assertTrue(isinstance(_coerce_to_bson_compatible(dat), datetime))

        # file
        file_name = os.path.join(os.path.dirname(os.path.join(__file__)),
                                 "file_test.txt")
        file_field = File(open(file_name), "file_test.txt")
        instance = TestModel.objects.create(field1='Foo',
                                            file_field=file_field)
        self.assertTrue(isinstance(instance.file_field, FieldFile))
        file_field.open()
        self.assertEqual(_coerce_to_bson_compatible(instance.file_field),
                        base64.b64encode(file_field.read()))

        # image
        file_name = os.path.join(os.path.dirname(os.path.join(__file__)),
                                 "image_test.png")
        file_field = File(open(file_name), "file_test.txt")
        instance = TestModel.objects.create(field1='Foo',
                                            file_field=file_field)
        self.assertTrue(isinstance(instance.file_field, FieldFile))
        file_field.open()
        self.assertEqual(_coerce_to_bson_compatible(instance.file_field),
                         base64.b64encode(file_field.read()))

        # PhoneNumberField
        phone = u"+555400000000"
        instance = TestModel(field1='Foo',
                             phonenumber_field=phone)
        phonenumber_field = instance.phonenumber_field
        self.assertTrue(isinstance(phonenumber_field, PhoneNumber))
        self.assertEqual(_coerce_to_bson_compatible(phonenumber_field),
                                                    phone)

    @override_settings(MONGO_DATABASE_NAME='log_test')
    def test_not_log_attr(self):
        """
        Verify if instace has attr '_not_log'.
        """
        instance1 = TestModel.objects.create(field1='Foo')
        self.assertFalse(hasattr(instance1, '_not_log'))
        instance2 = TestModel.objects.create(field1='Bar')
        no_logging(instance2)
        self.assertTrue(hasattr(instance2, '_not_log'))
        instance2.delete()
        instance1.delete()
