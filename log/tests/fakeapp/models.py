from django.db import models


class TestModel(models.Model):
    """Defines test model"""
    field1 = models.CharField(max_length=100, null=True)
    field2 = models.CharField(max_length=100, null=True)
    date_stamp = models.DateTimeField(max_length=100, null=True)
    file_field = models.FileField(upload_to='log/tests/upload', null=True)
    image_field = models.ImageField(upload_to='log/tests/upload', null=True)
    phonenumber_field = models.CharField(null=True, max_length=15)
    many_to_many_field = models.ManyToManyField('fakeapp.TestModel2')


class TestModel2(models.Model):
    """Defines test model relation to ManyToManyField"""
    field1 = models.CharField(max_length=100, null=True)

    def __unicode__(self):
        return self.field1