#-*- coding: utf-8 -*-
from clinical_trials.models import CTDetail, ClinicalTrial
from log.connection import MONGO_CONNECTION
from log.middleware import GetLog
from workflows.models import StateObjectHistory, State
from clinical_trials import CLINICAL_TRIALS_STEPS_FIELDS
def get_steps_with_changed_fields(ct_object):
    fields_changed = []

    try:
        date_last_resubmited = StateObjectHistory.objects.filter(
                                    content_id=ct_object.pk,
                                    state__pk=State.STATE_RESUBMETIDO
                               ).latest('update_date').update_date
        initial_log_date_search = date_last_resubmited
        date_last_publish = StateObjectHistory.objects.filter(
                                    content_id=ct_object.pk,
                                    state__pk=State.STATE_PUBLICADO
                               )
        if date_last_publish.exists():
            date_last_publish = date_last_publish.latest('update_date').update_date
            initial_log_date_search = max(date_last_resubmited, date_last_publish)

        col_ct = MONGO_CONNECTION.get_collection('clinical_trials__ClinicalTrial')

        changed_steps = []

        #Check for the related queries
        for key, values in CLINICAL_TRIALS_STEPS_FIELDS[u'related_queries'].items():
            for related in values:
                model_name = getattr(ct_object, related).model.__name__
                for related_instances in \
                    getattr(ct_object, related).all():
                    logs = GetLog(model=model_name, pk=related_instances.pk,
                                 initial_date=initial_log_date_search)()
                    if logs.count():
                        for l in logs:
                            fields_changed.extend(l[u'data'].keys())
                        changed_steps.append(key)

        #check for the ctdetail fields
        for ctdetail in ct_object.ctdetail_set.all():

            logs = GetLog(model=CTDetail.__name__, pk=ctdetail.pk,
                          initial_date=initial_log_date_search)()

            for log in logs:
                changed_fields = log[u'data'].keys()
                fields_changed.extend(changed_fields)
                for key, values in CLINICAL_TRIALS_STEPS_FIELDS[u'ctdetail_fields'].items():
                    for changed_field in changed_fields:
                        if changed_field in values:
                            changed_steps.append(key)




        #check for ct fields
        logs = GetLog(model=ClinicalTrial.__name__, pk=ct_object.pk,
                      initial_date=initial_log_date_search)()
        for log in logs:
            changed_fields = log[u'data'].keys()
            fields_changed.extend(changed_fields)
            for key, values in CLINICAL_TRIALS_STEPS_FIELDS[u'ct_fields'].items():
                for changed_field in changed_fields:
                    if changed_field in values:
                        changed_steps.append(key)

        return set(changed_steps)

    except StateObjectHistory.DoesNotExist:
        return []


def get_changed_fields(ct_object):
    fields_changed = []
    try:
        date_last_resubmited = StateObjectHistory.objects.filter(
                                    content_id=ct_object.pk,
                                    state__pk=State.STATE_RESUBMETIDO
                               ).latest('update_date').update_date
        initial_log_date_search = date_last_resubmited
        date_last_publish = StateObjectHistory.objects.filter(
                                    content_id=ct_object.pk,
                                    state__pk=State.STATE_PUBLICADO
                               )
        if date_last_publish.exists():
            date_last_publish = date_last_publish.latest('update_date').update_date
            initial_log_date_search = max(date_last_resubmited, date_last_publish)


        col_ct = MONGO_CONNECTION.get_collection('clinical_trials__ClinicalTrial')

        #Check for the related queries
        for key, values in CLINICAL_TRIALS_STEPS_FIELDS[u'related_queries'].items():
            for related in values:
                model_name = getattr(ct_object, related).model.__name__
                for related_instances in \
                    getattr(ct_object, related).all():
                    logs = GetLog(model=model_name, pk=related_instances.pk,
                                 initial_date=initial_log_date_search)()
                    for l in logs:
                        fields_changed.extend(
                            [(l[u'object_pk'], l[u'data'].keys())]
                        )

        #check for the ctdetail fields
        for ctdetail in ct_object.ctdetail_set.all():

            logs = GetLog(model=CTDetail.__name__, pk=ctdetail.pk,
                          initial_date=initial_log_date_search)()

            for log in logs:
                changed_fields = log[u'data'].keys()
                fields_changed.extend([(log[u'object_pk'], changed_fields)])


        #check for ct fields
        logs = GetLog(model=ClinicalTrial.__name__, pk=ct_object.pk,
                      initial_date=initial_log_date_search)()

        for log in logs:
            changed_fields = log[u'data'].keys()
            fields_changed.extend([(log[u'object_pk'], changed_fields)])

        
        fields_changed_dict = {}
        for i in fields_changed:
            if not fields_changed_dict.has_key(i[0]):
                fields_changed_dict[i[0]] = []
            fields_changed_dict[i[0]].extend(i[1])

        return fields_changed_dict

    except StateObjectHistory.DoesNotExist:
        return []
