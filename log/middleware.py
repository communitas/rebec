# -*- coding: utf-8 -*-
from django.db.models.signals import pre_save, post_delete, post_save, m2m_changed
from django.utils.functional import curry
from django.db.models.fields import DecimalField
from django.db.models import get_models
from collections import defaultdict
from datetime import datetime, date
from decimal import Decimal
from logging import getLogger
from log.connection import *
from bson.json_util import dumps
from django.db.models.fields.files import ImageFieldFile, FieldFile
from django.db.models import ManyToManyField
import base64

_LOGGER = getLogger(__name__)

class _collection_handler(object):
    """
    Lazy way of accessing the collection which handles intermittent failures in
    the connection
    """

    def __init__(self, collection_name):
        """

        :param collection_name: The name of the collection to use
        :type collection_name: :class:`basestring`
        
        """

        self.collection_name = collection_name
        self.collection = None
        
    def _get_collection(self):
        """Actually get the connection and handle failure to acquire it"""
        try:
            self.collection = MONGO_CONNECTION.get_collection(self.collection_name)
        except MongoConnectionError:
            self.collection = None
            raise

    def __call__(self):
        """
        Return either the collection or allow the MongoConnectionError to
        propagate to the calling code
        
        :return: The collection (if available)
        :rtype: :class:`pymongo.collection.Collection`

        """
        
        if not self.collection:
            self._get_collection()
            
        return self.collection


def _coerce_to_bson_compatible(value):
    """
    Ensure that any types which cannot be encoded into BSON are converted
    appropriately

    BSON cannot handle the following:
    * dates - convert to datetime
    * decimals - convert to float
    * ImageFieldFile - convert to binary (base64)
    * FieldFile - convert to binary (base64)
    * PhoneNumber - convert to unicode
    """

    # Convert decimal to float:
    if isinstance(value, Decimal):
        return float(value)
    
    # Convert to date to datetime
    elif isinstance(value, date) and not isinstance(value, datetime):
        return datetime.fromordinal(value.toordinal())

    # convert ImageFieldFile or FieldFile to binary (base64)
    elif isinstance(value, ImageFieldFile) or isinstance(value, FieldFile):
        if bool(value) is False:
            return None
        else:
            # http://stackoverflow.com/questions/11915770/saving-picture-to-mongodb
            value.open()
            return base64.b64encode(value.read())

    return value


def _audit_model(instance, operation, initial_values, final_values, user, **extra_info):
    """
    Calculate the differences on a model as an adjunct for Model 
    
    :param instance: The Django model this change relates to
    :param operation: The operation: insert, update or delete
    :param initial_values: A :class:`dict` of initial values
    :param final_values: A :class:`dict` of final values
    :param user: A :class:`User` of user
    """

    if user:
        user = user.username

    audit = {}
    audit['object_pk'] = instance.pk
    audit['user'] = user
    audit['date_stamp'] = datetime.now()
    audit['operation'] = operation
    audit['data'] = {}

    changes = False
    for key, final_value in final_values.iteritems():
        initial_value = initial_values.get(key)
        # TODO: Can this be simplified? Seems to break the tests by doing so
        if initial_value is None and final_value is not None:
            audit['data'][key] = _coerce_to_bson_compatible(final_value)
            changes = True
        else:
            if isinstance(final_value, datetime):
                final_value = final_value.replace(tzinfo=None)
            if isinstance(initial_value, datetime):
                initial_value = initial_value.replace(tzinfo=None)
            if isinstance(initial_value, list) and isinstance(final_value, list):
                if set(initial_value) != set(final_value):
                    audit['data'][key] = _coerce_to_bson_compatible(final_value)
                    changes = True
            elif initial_value != final_value:
                audit['data'][key] = _coerce_to_bson_compatible(final_value)
                changes = True

    if extra_info:
        for key, value in extra_info.iteritems():
            audit['data'][key] = _coerce_to_bson_compatible(value)
            changes = True

    if not changes:
        # No point in writing this to to DB:
        return None
    return audit


def _get_field_values(instance, log_fields, operation, m2m=False):
    """
    Get field values to performs auditing on the model to record the differences
    before and after the commit to the DB.

    :param instance: The Django model this change relates to
    :param operation: The operation: insert, update or delete
    :param log_fields: Fields to be logged
    """
    empty_values = False
    initial_values = {}
    final_values = {}

    if operation in ('insert', 'delete'):
        empty_values = True
    elif m2m:
        for field in instance._meta.many_to_many:
            m2m_model = m2m.get('m2m_model')
            if field.related.parent_model == m2m_model:
                values = m2m_model.objects.filter(pk__in=m2m.get('m2m_value'))
                m2m_values = [unicode(item) if getattr(item, '__unicode__', False) else item.pk for item in values]
                final_values[field.name] = m2m_values
                # get initial_value from Mongo
                logged_field = GetLog(instance._meta.object_name, instance.pk, field.name)()
                if logged_field:
                    values = [i for i in logged_field]
                    if values:
                        value = values[-1]
                        data = value.get('data', None)
                        if data:
                            initial_values[field.name] = data.get(field.name, [])
                if not initial_values.get(field.name, None):
                    initial_values[field.name] = []
        return (initial_values, final_values)
    else:
        try:
            obj = instance.__class__.objects
            initial_values = obj.filter(pk=instance.pk).values(*log_fields)[0]
        except IndexError:
            empty_values = True

    if empty_values:
        # we don't know what the initial state is, so assume None:
        initial_values = {}
        for field in log_fields:
            initial_values[field] = None

    for field in log_fields:
        # We can't just get the attribute directly off the instance here
        # because we need to ensure it is of the same type as the initial
        # value. To do this we use the field's `to_python` method:
        field_inst = instance._meta.get_field_by_name(field)[0]
        if isinstance(field_inst, ManyToManyField):
            if getattr(instance, field).values_list():
                final_values[field] = \
                    [ f.get(getattr(instance, field).model._meta.pk.name) \
                        for f in getattr(instance, field).values() ]
            else:
                final_values[field] = None
        else:
            final_values[field] = field_inst.to_python(field_inst
                                                   .value_from_object(instance))
        if not final_values.has_key(field):
            final_values[field] = None
    return (initial_values, final_values)


def no_logging(cls):
    """
    Decorator to add attr '_not_log' that means the Model will not be logged.
    """
    def set_attr(cls):
        """
        Create '_not_log' attr

        :param cls: Class applied decorator
        """
        setattr(cls, '_not_log', False)
        return cls
    return set_attr(cls)


class LogModel(object):
    """
    Middleware to listen signals pre_save, post_save and pre_delete
    of to log model on MongoDB.
    """

    def process_request(self, request):
        if not request.method in ('GET', 'HEAD', 'OPTIONS', 'TRACE'):
            if hasattr(request, 'user') and request.user.is_authenticated():
                user = request.user
            else:
                user = None

            router_pre = curry(self._router_pre, user)
            router_post = curry(self._router_post, user)
            router_m2m_changed = curry(self._router_m2m_changed, user)

            # insert operation
            post_save.connect(router_post, dispatch_uid = "post_save", weak = False)
            # update operation
            pre_save.connect(router_pre, dispatch_uid = "pre_save", weak = False)
            # delete operation
            post_delete.connect(router_post, dispatch_uid = "post_delete", weak = False)
            # m2m_changed operation
            m2m_changed.connect(router_m2m_changed, dispatch_uid = "m2m_changed", weak = False)

    def __init__(self):
        self.ignored_apps = [
            'django',
            'login',
            'cms',
        ]

    def _router_m2m_changed(self, user, sender, instance, action, model, pk_set, **kwargs):
        """
        Verify if action is pre_add on signal m2m_changed to get
        ManyToManyField field's values.

        :param user: User perform action
        :param sender: A sender to receive signals
        :param instance: The Django model this change relates to
        """
        if instance.__module__.split(".")[0] in self.ignored_apps:
            return False
        self.user = user

        if action == 'post_add':
            m2m = {'m2m_model': model,
                   'm2m_value': list(pk_set),}
            self.register_log(instance, 'update', m2m=m2m)
        return False

    def _router_pre(self, user, sender, instance, **kwargs):
        """
        Verify if operation is update on pre_save signal.

        :param user: User perform action
        :param sender: A sender to receive signals
        :param instance: The Django model this change relates to
        """
        if instance.__module__.split(".")[0] in self.ignored_apps:
            return False
        self.user = user
        if instance.pk:
            self.register_log(instance, 'update')
        return False

    def _router_post(self, user, sender, instance, **kwargs):
        """
        Verify if operation is insert on post_save signal,
        or delete on post_save signal.

        :param user: User perform action
        :param sender: A sender to receive signals
        :param instance: The Django model this change relates to
        """
        if instance.__module__.split(".")[0] in self.ignored_apps:
            return False
        self.user = user
        op = kwargs.get('created', None)
        if op == True:
            self.register_log(instance, 'insert')
        if op == None:
            self.register_log(instance, 'delete')
        return False

    def register_log(self, instance, operation, m2m=False):
        """
        Verify if model can be logged,
        Verify fields to be logged,
        Set collection name based on app_name + model_name.

        :param instance: The Django model this change relates to
        :param operation: The operation: insert, update or delete
        """

        if hasattr(instance, '_not_log'):
            return False

        defined_fields = [f.name for f in instance._meta.fields]
        if operation != 'update' or m2m:
            defined_fields += [f.name for f in instance._meta.many_to_many]

        if hasattr(instance, '_not_log_fields'):
            not_log_fields = instance._not_log_fields
            # Remove field not to log
            # _not_log_fields is a tuple containing name od fields
            # Raise an exception if we're trying to log a field which doesn't
            # exist on the model:
            for field in not_log_fields:
                if field in defined_fields:
                    defined_fields.remove(field)
                else:
                    raise AttributeError("Field %r does not exist on the model" % field)
            defined_fields.remove('id')

        initial_values, final_values = _get_field_values(instance, defined_fields, operation, m2m)
        if operation != 'insert' and not initial_values:
            return True

        audit = _audit_model(instance, operation, initial_values, final_values, self.user)

        if audit:
            collection_name = instance._meta.app_label+'__'+instance._meta.object_name
            collection = _collection_handler(collection_name)
        
            # Write out the document and return it's DB id:
            try:
                collection().insert(audit)
                return True
            except MongoConnectionError, exc:
                _LOGGER.critical("Error while writing document to collection: %s "
                             "Audit data: %r.",  exc, audit)
        return True


class GetLog(object):
    """
    Get the values logged on the insert, update or delete 
    or None if not available 
    """

    def __init__(self, model, pk, field=None, initial_date=None, final_date=None, user=None):
        """
        :param model: Model name
        :param pk: Primary key of model
        :param field: The name of field
        :param initial_date: Initial date to get log
        :param final_date: Final date to get log
        :param user: username
        """
        self.model = model
        self.pk = pk
        self.field = field
        self.initial_date = initial_date
        self.final_date = final_date
        self.user = user
        self.collection = None

    def __call__(self):
        """
        Return log of related field model and pk
        
        :return: The document from MongoDB associated with of this record
        :rtype: :class:`dict`
        """
        
        if not self.collection:
            self._get_collection()

        if self.collection:
            return self._get_creation_log()

        return False

    def _get_collection(self):
        """
        Verify model app_label and object_name
        Return a conection with collection corresponding to ``model``

        :param model: Model name
        :type model: :string:
        :rtype: dict
        """

        for model_class in get_models():
            if model_class._meta.object_name == self.model:
                collection_name = model_class._meta.app_label+'__'+self.model
                    
                self.collection = _collection_handler(collection_name)
                self.instance = model_class

    def _get_creation_log(self):
        """
        Get the values logged on the creation of this instance or None if not
        available
        
        :return: The document from MongoDB associated with the creation of this
            record
        :rtype: :class:`list`
        """
        query = dict(object_pk=self.pk)

        if self.field:
            field = "data.%s" % self.field
            query[field] = {'$exists': True}

        if self.user:
            query['user'] = self.user

        if self.initial_date:
            query['date_stamp'] = {'$gte': self.initial_date}

        if self.final_date:
            query['date_stamp'] = {'$lt': self.final_date}

        try:
            result = self.collection().find(query).sort('date_stamp')
        except IndexError:
            return None
        else:
            return result
