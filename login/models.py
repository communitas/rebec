import datetime
import hashlib
import random
import re
from django.db import models
from django.db.models import Count, Q
from django.contrib.auth.models import AbstractUser
from django.core import validators
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from administration.models import CTGroupUser, Country


class RebecUser(AbstractUser):
    """
    Recec user model.
    """
    GENDER_CHOICES = (
        (1, _('Male')),
        (2, _('Female')),
    )

    RACE_CHOICES = (
        (1, _('White')),
        (2, _('Yellow')),
        (3, _('Indigenous')),
        (4, _('Brown')),
        (5, _('Black')),
    )

    name = models.CharField(
        _('name'),
        max_length=100)
    gender = models.IntegerField(
        _('gender'),
        choices=GENDER_CHOICES,)
    race = models.IntegerField(
        _('race'),
        choices=RACE_CHOICES)
    ocuppation = models.IntegerField(
        _('ocuppation'),
        blank=True,
        null=True)
    passport = models.CharField(
        _('Passport'),
        blank=True,
        null=True,
        max_length=14,
        help_text=_("Enter the number of your passport."))
    cpf =  models.CharField(
        _('CPF'),
        blank=True,
        null=True,
        max_length=14,
        help_text=_('The cpf has 11 digits.'))

    country = models.ForeignKey(Country, default=u'BR')

    REQUIRED_FIELDS = ['name', 'email', 'gender', 'race', 'country', 'passport', 'cpf',]

    def __unicode__(self):
        """
        Return username.
        """
        return self.username

    def is_registrant(self):
        return self.groups.filter(name=u'registrant').exists()

    def is_registrant_administrator(self):
        return (
            self.groups.filter(name=u'registrant').exists()
            and self.ctgroups.filter(
                    type_user=CTGroupUser.TYPE_ADMINISTRATOR
                ).exists()
            )

    def is_manager(self):
        return self.groups.filter(name=u'manager').exists()

    def is_reviser(self):
        return self.groups.filter(name=u'reviser').exists()

    def is_evaluator(self):
        return self.groups.filter(name=u'evaluator').exists()

    def is_administrador(self):
        return self.groups.filter(name=u'administrador').exists()

    def is_editor(self):
        return self.groups.filter(name=u'editor').exists()

    def get_pendencies(self):
        from administration.models import Institution, CTGroup, CTGroupUser
        from workflows.models import StateObjectRelation, State
        from clinical_trials.models import ClinicalTrial
        total = 0
        if self.is_administrador():
            moderation_status_cts = StateObjectRelation.objects.filter(
                                    state__pk__in=\
                                    (State.STATE_AGUARDANDO_MODERACAO,
                                     State.STATE_EM_MODERACAO_RESUBMETIDO)
                                ).values_list('content_id', flat=True)
            #Removed from the panel number red circle for critical performance reasons
            
            institution_pending1 = Institution.objects.filter(
                status=Institution.STATUS_MODERATION
            ).filter(
                Q(sponsor_items__clinical_trial__pk__in=moderation_status_cts)
            ).values_list(u'pk', flat=True)

            institution_pending2 = Institution.objects.filter(
                status=Institution.STATUS_MODERATION
            ).annotate(
                number_of_ctgroups=Count('ctgroups'),
            ).filter(
                number_of_ctgroups__gt=0
            ).values_list(u'pk', flat=True)

            if institution_pending1.exists() or institution_pending2.exists():
                total += len(set(tuple(institution_pending1) + tuple(institution_pending2)))

            group_institution_pending = CTGroup.objects.filter(
                    status_institution=CTGroup.STATUS_INSTITUTION_PENDING,
                    institution__status=Institution.STATUS_ACTIVE,
                    status=CTGroup.STATUS_ACTIVE,
                    ).exists()
            if group_institution_pending:
                total += CTGroup.objects.filter(
                    status_institution=CTGroup.STATUS_INSTITUTION_PENDING,
                    institution__status=Institution.STATUS_ACTIVE,
                    status=CTGroup.STATUS_ACTIVE,
                    ).count()

            group_administrator_pending = CTGroupUser.objects.filter(
                status=CTGroupUser.STATUS_PENDING_APROV_ADM).exists()

            if group_institution_pending:
                total += CTGroupUser.objects.filter(
                status=CTGroupUser.STATUS_PENDING_APROV_ADM).count()

        if self.is_registrant():
            resubmetido = StateObjectRelation.objects.filter(
                state__pk=State.STATE_RESUBMETIDO).values_list(u'content_id', flat=True)

            revisando_preenchimento = StateObjectRelation.objects.filter(
                state__pk=State.STATE_REVISANDO_PREENCHIMENTO).values_list(u'content_id', flat=True)

            revisando_preenchimento_resubmetido = StateObjectRelation.objects.filter(
                state__pk=State.STATE_REVISAO_PREENCHIMENTO_RESUBMETIDO).values_list(u'content_id', flat=True)


            cts = ClinicalTrial.objects.filter(
                    Q(pk__in=revisando_preenchimento) | Q(pk__in=resubmetido) | Q(pk__in=revisando_preenchimento_resubmetido)
                ).filter(holder=self, user=self)
            if cts.exists():
                total += cts.count()

            group_adm = CTGroup.objects.filter(
                users__user=self, users__type_user='a'
            )
            if group_adm.exists():
                if CTGroupUser.objects.filter(ct_group=group_adm).filter(status=1).exists():
                    total += CTGroupUser.objects.filter(
                    ct_group=group_adm).filter(status=1).count()
                if CTGroupUser.objects.filter(ct_group=group_adm).filter(status=4).exists():
                    total += CTGroupUser.objects.filter(
                        ct_group=group_adm).filter(status=4).count()
        return total


#Make the email field required
RebecUser._meta.get_field(u'email').blank = False

default_country_clean = RebecUser._meta.get_field(u'country').clean
def __clean_country(raw_value, *args, **kwargs):
    return Country.objects.get(pk=raw_value)
def _clean_country(*args, **kwargs):
    import sys
    if 'createsuperuser' in sys.argv:
        return __clean_country(*args, **kwargs)
    return default_country_clean(*args, **kwargs)
RebecUser._meta.get_field(u'country').clean = _clean_country
