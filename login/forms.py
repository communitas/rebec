# -*- coding: utf-8 -*-
import datetime
import hashlib
sha_constructor = hashlib.sha1
import random
import re
from django import forms
from django.contrib.auth.forms import ReadOnlyPasswordHashField, AuthenticationForm
from django.utils.translation import ugettext, ugettext_lazy
from login.models import RebecUser
from administration.models import Country
from django_select2 import AutoModelSelect2Field, AutoModelSelect2MultipleField, AutoHeavySelect2Widget
from localflavor.br.forms import BRCPFField
from django.contrib.auth import authenticate
from django.contrib.sites.models import RequestSite
from django.contrib.sites.models import Site
from registration.models import RegistrationProfile
from django.contrib import messages



password_regex = \
    re.compile("(?=^.{1,}$)((?=.*\d)(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$")

class PasswordField(forms.CharField):
    
    # Setup the Field
    def __init__(self, *args, **kwargs):
        super(PasswordField, self).__init__(min_length = 10, required = True,
                        widget = forms.PasswordInput(render_value = False),       
                        *args, **kwargs)
    
    # Validate - 1+ Numbers, 1+ Letters
    def clean (self, value):
        
        if not password_regex.search(value):
            raise forms.ValidationError(ugettext('Your password must include at least'
                                        u' one lowercase letter, one uppercase,'
                                        u' one number and one special character. '
                                        u'Also it must have at least 10 characters.'))

        return super(PasswordField, self).clean(value)

class CountryChoices(AutoModelSelect2Field):
    """
    A django_select2 class to configure the autocomplete search
    for Country.
    """
    queryset = Country.objects
    search_fields = [u'code__icontains',
                     u'description__icontains',]


#Change the passwordField in the django change password form
from django.contrib.auth.forms import PasswordChangeForm

PasswordChangeForm.base_fields['new_password1'] = PasswordField(label=ugettext_lazy('New password'))
PasswordChangeForm.base_fields['new_password2'].label = ugettext_lazy('New password confirmation')

class UserCreationForm(forms.ModelForm):
    """
    A form for creating new users. Includes all the required
    fields, plus a repeated password.
    """
    country = CountryChoices(
        required=True, verbose_name=ugettext_lazy('Country'),
        help_text=ugettext_lazy("Select your country of origin"),
        label=ugettext_lazy('Country'))
    cpf = BRCPFField(
        label=ugettext_lazy(u'CPF'), help_text=ugettext_lazy('The cpf has 11 digits.'),
        required=False)
    
    error_messages = {
        'duplicate_username': ugettext_lazy("A user with that username already exists."),
        'duplicate_email': ugettext_lazy("A user with that email already exists."),
        'duplicate_cpf': ugettext_lazy("A user with that cpf already exists."),
        'duplicate_passport': ugettext_lazy("A user with that passport already exists."),
        'password_mismatch': ugettext_lazy("The two password fields didn't match."),
    }
    username = forms.RegexField(
        label=ugettext_lazy("Username"), max_length=30,
        regex=r'^[\w.@+-]+$',
        help_text=ugettext_lazy("30 characters or less. Only letters, numbers and @ / / + / -. / _."),
        error_messages={
            'invalid': ugettext_lazy("This value may contain only letters, numbers and "
                         "@/./+/-/_ characters.")})
    password1 = PasswordField(
        label=ugettext_lazy('Password'),
        help_text=ugettext_lazy("10 characters or more. At least one lowercase letter, one uppercase, one number and one special character."))
    password2 = PasswordField(
        label=ugettext_lazy('Password confirmation'),
        help_text=ugettext_lazy("10 characters or more. At least one lowercase letter, one uppercase, one number and one special character."))

    class Meta:
        model = RebecUser
        fields = (
            'username', 'name', 'email', 'gender', 'race',
            'country', 'passport', 'cpf')

    def clean_username(self):
        """
        Verify if username already exists.
        """
        username = self.cleaned_data["username"]
        try:
            RebecUser._default_manager.get(username=username)
        except RebecUser.DoesNotExist:
            return username
        raise forms.ValidationError(
            self.error_messages['duplicate_username'],
            code='duplicate_username',
        )

    def clean_email(self):
        """
        Verify if email already exists.
        """
        email = self.cleaned_data["email"]
        try:
            RebecUser._default_manager.get(email=email)
        except RebecUser.DoesNotExist:
            return email
        raise forms.ValidationError(
            self.error_messages['duplicate_email'],
            code='duplicate_email',
        )

    def clean_cpf(self):
        """
        Verify if cpf already exists.
        """
        cpf = self.cleaned_data["cpf"]
        if cpf != u'':
            try:
                RebecUser.objects.get(cpf=cpf)
            except RebecUser.DoesNotExist:
                return cpf
            raise forms.ValidationError(
                self.error_messages['duplicate_cpf'],
                code='duplicate_cpf',
            )

    def clean_passport(self):
        """
        Verify if cpf already exists.
        """
        passport = self.cleaned_data["passport"]
        if passport != u'':
            try:
                RebecUser.objects.get(passport=passport)
            except RebecUser.DoesNotExist:
                return passport
            raise forms.ValidationError(
                self.error_messages['duplicate_passport'],
                code='duplicate_passport',
            )

    def clean_password2(self):
        """
        Validate password.
        """
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords don't match")
        return password2


    def clean(self, *args, **kwargs):
        if self.cleaned_data.get(u'country', 0) != 0:
            if self.cleaned_data.get(u'country', 0).pk == u'BR':
                if not self.cleaned_data.get(u'cpf', 0): 
                    raise forms.ValidationError(
                        ugettext('Enter your cpf.'))
            elif self.cleaned_data.get(u'country', 0).pk != u'BR':
                if not self.cleaned_data.get(u'passport', 0): 
                    raise forms.ValidationError(ugettext("Enter your passport."))
        return self.cleaned_data
   
    def save(self, commit=True):
        """
        Saves the password.
        """
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user



class UserChangeAdminForm(forms.ModelForm):
    """
    A form for updating users. Includes all the fields on
    the user, but replaces the password field with admin's
    password hash display field.
    """
    country = CountryChoices(
        required=True, verbose_name=ugettext_lazy('Country'),
        help_text=ugettext_lazy("Select your country of origin"),
        label=ugettext_lazy('Country'),
        attrs={'class' : 'country'})
    username = forms.RegexField(
        label=ugettext_lazy("Username"), max_length=30, regex=r"^[\w.@+-]+$",
        help_text=ugettext_lazy("Required. 30 characters or fewer. Letters, digits and "
                    "@/./+/-/_ only."),
        error_messages={
            'invalid': ugettext_lazy("This value may contain only letters, numbers and "
                         "@/./+/-/_ characters.")})
    password = ReadOnlyPasswordHashField()

    class Meta:
        model = RebecUser
        fields = ('username', 'name', 'email', 'gender', 'race', 'country', 'passport', 'cpf')


    def __init__(self, *args, **kwargs):
        super(UserChangeAdminForm, self).__init__(*args, **kwargs)
        # keeping same username
        f = self.fields.get('user_permissions', None)
        if f is not None:
            f.queryset = f.queryset.select_related('content_type')

    def clean_password(self):
        """Set initial password."""
        return self.initial["password"]

    def clean_username(self):
        """Set initial password."""
        return self.initial["username"]


class UserChangeForm(forms.ModelForm):
    """
    A form for updating users. Includes all the fields on
    the user, but replaces the password field with admin's
    password hash display field.
    """
    country = CountryChoices(
        required=True, verbose_name=ugettext_lazy('Country'),
        help_text=ugettext_lazy("Select your country of origin"),
        label=ugettext_lazy('Country'),
        attrs={'class' : 'country'})
    username = forms.RegexField(
        label=ugettext_lazy("Username"), max_length=30, regex=r"^[\w.@+-]+$",
        help_text=ugettext_lazy("Required. 30 characters or fewer. Letters, digits and "
                    "@/./+/-/_ only."),
        error_messages={
            'invalid': ugettext_lazy("This value may contain only letters, numbers and "
                         "@/./+/-/_ characters.")})
    password = ReadOnlyPasswordHashField()

    class Meta:
        model = RebecUser
        fields = ('username', 'name', 'email', 'gender', 'race', 'country', 'passport', 'cpf')


    def __init__(self, *args, **kwargs):
        super(UserChangeForm, self).__init__(*args, **kwargs)
        # keeping same username
        f = self.fields.get('user_permissions', None)
        if f is not None:
            f.queryset = f.queryset.select_related('content_type')


class AdminPasswordChangeForm(forms.Form):
    """
    A form used to change the password of a user in the admin interface.
    """
    error_messages = {
        'password_mismatch': ugettext_lazy("The two password fields didn't match."),
    }
    required_css_class = 'required'
    password1 = forms.CharField(label=ugettext_lazy("Password"),
                                widget=forms.PasswordInput)
    password2 = forms.CharField(label=ugettext_lazy("Password (again)"),
                                widget=forms.PasswordInput)

    def __init__(self, user, *args, **kwargs):
        self.user = user
        super(AdminPasswordChangeForm, self).__init__(*args, **kwargs)

    def clean_password2(self):
        """
        Validate password.
        """
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')
        if password1 and password2:
            if password1 != password2:
                raise forms.ValidationError(
                    self.error_messages['password_mismatch'],
                    code='password_mismatch',
                )
        return password2

    def save(self, commit=True):
        """
        Saves the new password.
        """
        self.user.set_password(self.cleaned_data["password1"])
        if commit:
            self.user.save()
        return self.user

    def _get_changed_data(self):
        """
        Return changed data.
        """
        data = super(AdminPasswordChangeForm, self).changed_data
        for name in self.fields.keys():
            if name not in data:
                return []
        return ['password']
    changed_data = property(_get_changed_data)

class ResendActivationEmailForm(forms.Form):
    email = forms.EmailField(required=True, label=ugettext_lazy('e-mail'))

    def send_email(self, user):
        for i in user:
            if not i.is_active:
                for profile in RegistrationProfile.objects.filter(user=user):
                    if profile.activation_key_expired():
                        salt = hashlib.sha1(str(random.random())).hexdigest()[:5]
                        profile.activation_key = hashlib.sha1(salt+user.username).hexdigest()
                        user.save()
                        profile.save()

                    if Site._meta.installed:
                        site = Site.objects.get_current()
                    else:
                        site = RequestSite(request)

                    profile.send_activation_email(site)

class AuthenticationForm(AuthenticationForm):
    """
    Base class for authenticating users. Extend this to get a form that accepts
    username/password logins.
    """
    username = forms.CharField(max_length=254, label=ugettext_lazy('Username'))
    password = forms.CharField(
        label=ugettext_lazy("Password"),
        widget=forms.PasswordInput)
    error_messages = {
        'invalid_login': ugettext_lazy("Please enter a correct %(username)s and password. "
        "Note that both fields may be case-sensitive."),
        'inactive': ugettext_lazy("This account is inactive."),
    }
    def __init__(self, request=None, *args, **kwargs):
        """
        The 'request' parameter is set for custom auth use by subclasses.
        The form data comes in via the standard 'data' kwarg.
        """
        self.request = request
        self.user_cache = None
        super(AuthenticationForm, self).__init__(*args, **kwargs)
        # Set the label for the "username" field.
    
    def clean(self):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')
        if username and password:
            self.user_cache = authenticate(username=username,
                password=password)
        if self.user_cache is None:
            raise forms.ValidationError(
                self.error_messages['invalid_login'],
                    code='invalid_login',
                    params={'username': self.username_field.verbose_name},
                )
        else:
            self.confirm_login_allowed(self.user_cache)
        return self.cleaned_data

    def confirm_login_allowed(self, user):
        if not user.is_active:
            raise forms.ValidationError(
                self.error_messages['inactive'],
                code='inactive',
            )
        return self.user_cache
