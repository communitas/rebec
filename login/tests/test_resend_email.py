# -*- coding: utf-8 -*-

from django.test import TestCase
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.test.client import Client, RequestFactory
from django.core.urlresolvers import reverse_lazy
from mock import Mock, MagicMock
from django.contrib import messages
from django.core.exceptions import PermissionDenied
from login.forms import *
from login.views import *
from mock import Mock, MagicMock
from django.contrib import messages
User = get_user_model()

class ResendActivationEmailTeste(TestCase):
    """Classe test Resend Activation email."""

    def setUp(self):
        """Create user"""
        self.user1 = User.objects.create(
            username=u'e',
            email=u'e@e.com',
            gender=1,
            race=1,
            country_id=u'BR',
            is_active=False
        )
        self.factory = RequestFactory()
        self.original_success = messages.success
        messages.success = Mock()
        self.original_info = messages.info
        messages.info = Mock()
        self.original_error = messages.error
        messages.error = Mock()

    def tearDown(self):
        """Delete user"""
        self.user1.delete()
        messages.success = self.original_success
        messages.info = self.original_info
        messages.error = self.original_error

    def test_user_resend_email(self):
        """Forward of User's account activation email."""
        post_data = { u'email': [u'e@e.com']}
        request = self.factory.post(
            reverse_lazy('resend_activation_email'), data=post_data)
        response = ResendActivationEmail.as_view()(request)
        self.assertEqual(response.status_code, 302)



