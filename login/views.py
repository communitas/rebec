#-*- coding: utf-8 -*-
from django.shortcuts import render
from django.views.generic.edit import FormView
from django.utils.translation import ugettext as _
from login.forms import ResendActivationEmailForm, AuthenticationForm
from login.models import RebecUser
from django.template.response import TemplateResponse
from django.contrib.auth import get_user_model
from django.utils.http import is_safe_url, urlsafe_base64_decode
from django.shortcuts import resolve_url
from django.views.decorators.csrf import csrf_protect
from django.contrib.auth import REDIRECT_FIELD_NAME, login as auth_login
from django.contrib.auth.decorators import login_required
from django.contrib.sites.models import get_current_site
from django.contrib.auth.forms import PasswordResetForm, SetPasswordForm, PasswordChangeForm
from django.contrib.auth.tokens import default_token_generator
from django.http import HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse_lazy, reverse
from rebec import settings
from django.contrib import messages



User = get_user_model()

def login(request, template_name='registration/login.html',
    redirect_field_name=REDIRECT_FIELD_NAME,
    authentication_form=AuthenticationForm,
    current_app=None, extra_context=None):
    """
    Displays the login form and handles the login action.
    """
    redirect_to = request.POST.get(redirect_field_name,
    request.GET.get(redirect_field_name, ''))

    if request.method == "POST":
        form = authentication_form(request, data=request.POST)
        if form.is_valid():
            # Ensure the user-originating redirection url is safe.
            if not is_safe_url(url=redirect_to, host=request.get_host()):
                redirect_to = resolve_url(settings.LOGIN_REDIRECT_URL)
            # Okay, security check complete. Log the user in.
            auth_login(request, form.get_user())
            return HttpResponseRedirect(redirect_to)
        else:
            for error in form.errors.values():
                for message in error:
                    if message == form.error_messages['inactive']:
                        username = form.cleaned_data['username']
                        user = RebecUser.objects.filter(username=username)
                        user_email = RebecUser.objects.filter(email=username)
                        if user:
                            for i in user:
                                if not i.is_active:
                                    return HttpResponseRedirect(reverse('resend_activation_email'))
                        if user_email:
                            for i in user_email:
                                if not i.is_active:
                                    return HttpResponseRedirect(reverse('resend_activation_email'))
    else:
        form = authentication_form(request)

    current_site = get_current_site(request)
    context = {
        'form': form,
        redirect_field_name: redirect_to,
        'site': current_site,
        'site_name': current_site.name,
    }
    if extra_context is not None:
        context.update(extra_context)
    return TemplateResponse(request, template_name, context,
        current_app=current_app)



class ResendActivationEmail(FormView):
    """View resend user account activation email."""
    template_name = "registration/resend_activate_email.html"
    form_class = ResendActivationEmailForm
    success_url = reverse_lazy('resend_activation_email')

    def form_valid(self, form):
        email = self.request.POST['email']
        user = RebecUser.objects.filter(email=email, is_active=0)
        if user:
            form.send_email(user)
            messages.success(self.request, _(u'Activation email sent.'))
        if not user:
            messages.error(self.request, _(u'This User account is already active'))
        return super(ResendActivationEmail, self).form_valid(form)
        