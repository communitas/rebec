#-*- coding: utf-8 -*-
from django.contrib import admin
from django.contrib.auth.models import Group
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import ugettext, ugettext_lazy as _
from login.forms import (
    UserChangeAdminForm, UserCreationForm, AdminPasswordChangeForm)
from login.models import RebecUser


class UserAdmin(UserAdmin):
    """User Admin class."""
    form = UserChangeAdminForm
    add_form = UserCreationForm
    change_password_form = AdminPasswordChangeForm
    change_form_template = u'login/change_user_admin_form.html'

    list_display = (
        'username', 'name', 'email', 'gender',
        'race', 'ocuppation', 'country', 'cpf', 'passport', 'is_staff')
    list_filter = ('groups', 'is_staff', 'is_superuser', 'is_active')
    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        (_('Personal info'), {'fields': (
            'username', 'name', 'gender', 'race', 'ocuppation', 'passport', 'cpf', 'country')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': (
                'username', 'name', 'email', 'gender', 'race','country',
                 'passport', 'cpf', 'password1', 'password2')}),
    )
    search_fields = ('username', 'email', 'name')
    ordering = ('username', 'name', 'email', 'gender', 'race', 'country', 'passport', 'cpf',)
    filter_horizontal = ()

    class Media:
        css = {
            "all": ("css/select2.css",)
        }
        js = ("js/jquery.js", "js/select2.js",)

admin.site.register(RebecUser, UserAdmin)
