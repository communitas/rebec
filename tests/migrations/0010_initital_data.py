# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
    	from django.core.management import call_command
        call_command("loaddata", "tests/migrations/fixtures/workflow_clinicaltrial_relation.json")

    complete_apps = ['tests']