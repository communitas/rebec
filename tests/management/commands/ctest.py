#-*- coding: utf-8 -*-
from django.core.management.base import BaseCommand, CommandError
import os

class Command(BaseCommand):
    """
    Command that runs tests with coverage
    """
    help = u"Command that runs tests with coverage"

    def handle(self, *args, **options):
        """
        Execute the default django tests and after coverage report.
        """
        named_args = ' '.join(['--%s=%s' %(key, value)
                             if value else ''
                             for key, value in options.items()])
        args = ' '.join(args)
        command = u"coverage run --source='.' manage.py test %s %s"
        os.system(command % (args, named_args))
        os.system(u'coverage report -m')