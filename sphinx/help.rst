Dynamic Help
===============
Dynamic help is a way to tour the site to explain the functionalities.
`Bootstrap-Tour <http://bootstraptour.com/>`_ is a css and js framework used to create tours.

Help Setup
----------------
The help is stored in the database, to create them have to follow the following steps:

    #. Create the help
        To create the help has to access the admin, the table will help fill in the fields:
        
        - **Url name** : In this field have to be preencido with the name of the url you want to do the tour help. To see the name of the url you will have to access the urls file in font code.

        - **Name** : Name that will help.
        
        - **User Type** : This field should be selected profiles of users who may have access to this help. Can be added more than one, if help will be published to unauthenticated users do not need any selecinar profile.

    #. Creating help languages
        The help may have one or more languages as they follow the language of the site. So you must create the Help Languages table, help for each language used on the site.
        In Help Language table in admin, the fields must be filled with:
        
        - **Help** :  Select the help, which is the name of the url that will make the tour.

        - **Language** : Select the language of help.

        - **Title** : Place the title of the aid, the title should be written with the liguagem selected

    #. Creating  Items help
        Now we have to create the steps of help. To do this go to the admin in the table items and help fill in the following fields:

        - **Help Language** : Select this field which helps belongs this step. Remember to write in the language of the selected help.
        
        - **Element** : Put here the id or class of the html element. Example: #element.

        - **Title help** : This will be the title of help.

        - **Content** : You put the explanatory text about that while you are creating.

        - **Placement** : Select here where it had been located, the screen, the window of step.

        - **Backdrop** : When selecting it you will do in this step becomes the dark screen, if not select it the screen is normal.

        - **Backdrop Padding** : You put the number corresponding to the size of the window border.

        - **Animation** : Select whether animation in this step.

        - **Next** : Here you should put what is the next step. Whether it's the place útimo step -1.

        - Prev : Here you should put what is the previous step. If it is the first step put -1.

        - **Obs.: Fields: OnShow, OnShown, OnHide, OnHidden, OnNext, OnPrev, Path,** do not work by default, look at the `Bootstrap-Tour documentation <http://bootstraptour.com/>`_

    #. Observations

        - You can create a help for url (page).

        - The help has to be registered by languages and for all site languages.

        - The help are seen in the first time you access the page with the browser.

        - It mounts a menu so you can be held the help again.

        - Can be done to help editing screens, but these do not appear in the help menu.

        - The help will be seen only by the selected profiles.

        - In ItemHelp table will be registered the intensities of aid. The steps are numbered in order of pk starting from 0. So the first aid item that is created will step 0.
