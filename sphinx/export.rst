Export
===============

The export configuration allow to create a specific XML configuration to export the trials.

Is it possible to have more than one configuration register. However just one can be active.

For each configuration tags have to be created to form the XML hierarchy.

.. figure:: ./export_form.png

	Tags form

Multiple
--------

This option have to be checked when the tag has more than one occurence of their childrens. "The atribute value" or "function to get the value" have to return a list of objects.

Function to get the value
-------------------------

When the value it is not just fixed or an attribute from database it is possible to configurate a function to get the value. This function should be registered in import_export_config/import_functions.py

Next an example of this.

.. code-block:: python

	def get_utn(trial_instance):
		try:
			return trial_instance.identifier_set.get(
					setup_identifier__name='UTN').code
		except trial_instance.identifier_set.model.DoesNotExist:
			return ''

	register_function(get_utn,
    	__(u'Return the UTN value'))

Attribute Value
---------------

The attribute values must be python style attribute getter. All attributes from Clinical Trial can be used, including nested attributes. To make sure the possible values check on Clinical Trial models file (clinical_trials/models.py)

It is possible to use any python code with clinical trial attributes. 

For example: date_last_publication.strftime("%d/%m/%Y")

Fixed Value
-----------

Any fix value the tag has to return.