Users and groups
================

Roles
-----

There are six possible roles available:

**Administrator**: they are the portal manager, they administrate users and institutions

**Registrant**: they create trials and send them to review

**Reviser**: they check the trials which were sent by registrants then publish them or return the trials if they need some corrections

**Evaluator**: they check the trials and register what can be changed

**Manager**: they manage reviews and the work load

**Editor**: they edit portal content

Users
-----

To create a user it is necessary to fill the form available on the site. An e-mail will be sent to confirm the process and to validate the user.

When a user is created the role **registrant** is automatic associated. If the administrator needs to associate another role then it is necessary to access the admin interface and change de role.

It is not possible to register two users with the same email, CPF or CNPJ.

Groups
------

Groups are necessary in order to share Clinical Trials. Any user can create as many groups as they like. Once the group is created the group administrator can associate members. 

Groups can be public or private. When a group is private only the administrator and members can see it. Whereas a public group can be visualized by any authenticated user. Public groups allow users to request association. The request has to be approved by the administrator.

Groups are specially important to manage institutional trials. When a Clinical Trial is created the registrant can choose a group or not. If a group is choosen then all members of the group will be able to see the trial. The group administrator can transfer trials from one member to another in the same group. This can be useful when a group changes by  adding or excluding members.

The user who created the group is automatically associated as administrator. It is mandatory for a group to have at least one administrator. A member will never be excluded just inactivated.

When a group is associated with an institution it gets the "pending institution" status until the site administrator approves it. The administrator of any institutional groups  has to be approved by the site administrator.