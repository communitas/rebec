.. rebec documentation master file, created by
   sphinx-quickstart on Mon Jun  9 15:21:29 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to ReBEC 2.0 documentation!
===================================

 The Brazilian Clinical Trials Registry (Rebec) is a virtual platform with free access to record experimental and non-experimental studies conducted in humans, in progress or completed, by Brazilian and foreign researchers. The Rebec is a joint project of the Ministry of Health (DECIT / MS), the Pan American Health Organization (PAHO) and the Oswaldo Cruz Foundation (FIOCRUZ). The Executive Committee is composed by Rebec, the aforementioned institutions and the National Health Surveillance Agency (ANVISA).

.. toctree::
   :maxdepth: 2

   portal
   users_and_groups
   regras_de_negocio
   clinical_trail
   export
   help
   

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


.. automodule:: administration.models
   :members:

.. automodule:: administration.views
   :members:

.. automodule:: administration.backends
   :members:

.. automodule:: administration.forms
   :members:

.. automodule:: administration.tests.test_adm
   :members:

.. automodule:: administration.tests.test_country
   :members:

.. automodule:: administration.tests.test_institution_type
   :members:

.. automodule:: clinical_trials.models
   :members:

.. automodule:: clinical_trials.views
   :members:

.. automodule:: clinical_trials.helpers
   :members:

.. automodule:: clinical_trials.forms
   :members:

.. automodule:: clinical_trials.tests.tests
   :members:

.. automodule:: generics.mixins
   :members:

.. automodule:: log.middleware
   :members:

.. automodule:: log.connection
   :members:

.. automodule:: log.tests.tests
   :members:

.. automodule:: log.tests.test_log
   :members:

.. automodule:: login.models
   :members:

.. automodule:: login.views
   :members:

.. automodule:: login.forms
   :members:

.. automodule:: login.admin
   :members:

.. automodule:: login.tests.test_resend_email
   :members:

.. automodule:: portal.models
   :members:

.. automodule:: portal.views
   :members:

.. automodule:: portal.utils
   :members:

.. automodule:: portal.cms_plugins
   :members:

.. automodule:: portal.cms_app
   :members:

.. automodule:: portal.templatetags.portal_tags
   :members:

.. automodule:: registration.views
   :members:

.. automodule:: registration.models
   :members:

.. automodule:: registration.forms
   :members:

.. automodule:: workflows.utils
   :members:

.. automodule:: workflows.models
   :members:

