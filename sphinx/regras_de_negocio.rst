Institutions and Contacts
=========================

Institutions
-------------

Institutions can be created by the site administrator using the management interface or by registrants when they are creating trials. 

When the trial is send to review and a new institution was created the trial goes through moderation.

The site administrator can approve, reject or change institutions. If any institution was rejected then the trial returns to the registrant who should change the institution for a valid one. An institution will only be on moderation after the Clinical Trial has been sent to review.

Contacts
---------

Contacts are shared within the group. That means that when a trial uses a contact all other trials in the same group can see it.

The contact can be associated with an institution, but this association does not need moderation. For it is only within the group context.