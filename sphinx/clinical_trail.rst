Clinical Trial
===============

Clinical trials are the main object of the system. They can be created by any user with register role. The information about the trial is divided in nine steps, all them have to be completed to send the trial to review.

To be published a clinical trial has to be reviewed. A workflow was established as we can check below.

Wokflow
----------------
Its concept is a sequence of steps to automate processes in accordance with a set of defined rules, allowing them to be transmitted from one person to another.

Workflow operation in Rebec
++++++++++++++++++++++++++++

    - **In development** - The trial remains in this state until be send to review. To send to review is it necessary complet all nine steps.
    
    - **Awaiting moderation** - When the register complete trials form and send to review and if there are sponsors which have to be moderated the trial will be set for "Awaiting moderation" state. The site administrator will execute the action on this state.
    
    - **Reviewing fill** - The trial return to this state when site administrator reject any sponsor used on the trial. In this case the register has to change the sponsor and resend the trial to review.

    - **Waiting Evaluation** - This is the first step of review. The trial will stay on this state until one evaluator take the trial to analyse.

    - **In Evaluation** - One evaluator took the trial to anylize. The evaluator will check the trial informations and register observations is it is necessary.

    - **Waiting Review** - The trial remains in this state while the reviewer does not start the review.

    - **In Review** - One review begins to analyze the trial.

    - **Resubmitted** - If there is any inconsistency in the study it leaves the state under review and return to the registrant with the resubmitted state. The registrant has to check the observations and do the necessary modifications.

    - **In Moderation Resubmitted** - It is the same as **Awaiting moderation** state. However this state is launch just after **resubmitted** state.

    - **Review resubmitted fill** - It is the same as **Reviewinf fill** state. However this state is launch just after **In Moderation Resubmitted**.

    - Published - The trial is publish on the site, no changes can be made.


.. figure::  imgs/diagrama_de_estados.jpg
   :align:   center

+-------------------------------+------------+-----------------+----------+------------+---------+--------------+
| States                        | Registrant |  Registrant Adm | Revisor  | Avaliador  | Gestor  | Administrador|
+===============================+============+=================+==========+============+=========+==============+
|In development                 | Edit \*1   | Edit \*2        |          |            |         |              |
+-------------------------------+------------+-----------------+----------+------------+---------+--------------+
|Awaiting moderation            | View \*1   | View \*2        |View \*4  | View \*4   |View \*4 | View \*4     |
+-------------------------------+------------+-----------------+----------+------------+---------+--------------+
|Reviewing fill                 | Edit\*1    | Edit\*2         |View \*4  | View \*4   |View \*4 | View \*4     |
+-------------------------------+------------+-----------------+----------+------------+---------+--------------+
|Waiting Evaluation             | View \*1   | View \*2        | View \*4 | View \*4   |View \*4 | View \*4     |
+-------------------------------+------------+-----------------+----------+------------+---------+--------------+
|In Evaluation                  | View \*1   | View \*2        | View \*4 | Edit \*5   |View \*4 | View \*4     |
+-------------------------------+------------+-----------------+----------+------------+---------+--------------+
|Waiting Review                 | View \*1   | View \*2        | View \*4 | View \*4   |View \*4 | View \*4     |
+-------------------------------+------------+-----------------+----------+------------+---------+--------------+
|In Review                      | View \*1   | View \*2        | Edit \*5 | View \*4   |View \*4 | View \*4     |
+-------------------------------+------------+-----------------+----------+------------+---------+--------------+
|Resubmitted                    | Edit \*1   | Edit \*2        | View \*4 | View \*4   |View \*4 | View \*4     |
+-------------------------------+------------+-----------------+----------+------------+---------+--------------+
|Published                      | View \*1   | View \*2        | View \*4 | View \*4   |View \*4 | View \*4     |
+-------------------------------+------------+-----------------+----------+------------+---------+--------------+
|Awaiting moderationResubmitted | View \*1   | View \*2        | View \*4 | View \*4   |View \*4 | View  \*3\*4 |
+-------------------------------+------------+-----------------+----------+------------+---------+--------------+
|Review Resubmitted Fill        | Edit\*1    | Edit\*2         | View \*4 | View \*4   |View \*4 | View \*4     |
+-------------------------------+------------+-----------------+----------+------------+---------+--------------+


- \*1 - Only the user responsible for the clinical trial.

- \*2 - Only the clinical trials that are associated with groups he manages.

- \*3 - No edits the clinical trial data but makes the moderation of the institution that can cause issue in Clinical Trail.

- \*4 - All.

- \*5 - Does not edit attributes of the clinical trial, just put comments in attributes.