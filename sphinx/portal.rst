Portal
===============

The portal is the site which presents news, information, assistance, links among other relevant information of the project. The Rebec portal is based on **Django CMS** which is a web publishing platform built with Django.
Django CMS offers out-of-the-box support for the common features you’d expect from a CMS, but it can also be easily customised and extended by developers to create a site that is tailored to their precise needs.

Configure Pages
----------------

Create an user with administrator role using Admin interface. The superuser has to do that.

Home page
+++++++++

	* Using the administrator user created, access the admin interface again and edit the model named "sites". It is necessary inform the URL and the name for the portal. Example:
		host: ensaiosclinicos.gov.br
		Name: Rebec

	* The first page to be added is the home. In admin interface go to CMS group and choose model "Pages". Press the add pages button. Fill the form with name and slug "home" and save. The page was created.

	.. figure:: ./home.png

		You should see this image.

	The next step is associate a struture for this page. Go to CMS menu on top select Page --> templates --> home. 

	.. figure:: ./home2.png

		Now the home page has a specific structure.

	Change the visualization to "structure", on top, right corner.

	.. figure:: ./home3.png

		Structure visualization

	In group "lastet_CT", click on menu icon on the right. A list appers, choose "Últimos ensaios clinicos". On form type the amount of Clinical Trials you would like to show on home page. Do the same for "Latest_News", but use "Últimas notícias". Click on button "public page now".

	.. figure:: ./home4.png

Add pages
+++++++++

	* Go to the menu: page / add page / new page
	* Go to the menu: page / models / base_template
	* In the structure input you can put the contents of the page.
	* Upload changes.

	Obs:
	* The pages have to be created in the all languages available on the site.
	* The **Slug** attribute must be the same for the all languages.

Edit pages
++++++++++

	* Go to the menu: Rebec / pages.
	* In the pages edition you can inform if they will be shown on the menu or not.

Top Bar
+++++++
The top bar can be configurated by the administrator using the manage interface. The options are: disabled, inform a link or upload a file.
