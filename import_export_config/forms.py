import magic
from django import forms
from import_export_config.models import Tag
from clinical_trials.models import ClinicalTrial
from django.utils.translation import ugettext as _
from django.utils.translation import ugettext_lazy

class TagEditForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(TagEditForm, self).__init__(*args, **kwargs)
        self.fields['parent_tag'].queryset = \
            self.fields['parent_tag'].queryset.order_by('tag_name')

        translations = {'export_config': _(u'Export Config'),
                        'tag_name': _(u'Tag Name'),
                        'parent_tag': _(u'Parent Tag'),
                        'multiple': _(u'Multiple'),
                        'value_function': _(u'Function to get the value'),
                        'attibute_value': _(u'Attribute Value'),
                        'fixed_value': _(u'Fixed Value'),
                        'tag_order': _(u'Tag order'),
                        }
        translations_help = {'tag_name':  _(u'Give this tag a name.'),
                             'parent_tag': _(u'Select the parent tag if exsists.'),
                             'multiple': _(u'Check this option if this tag can apper more than once.'
                                           u' The value of this tag must be an iterable object.'),
                             'value_function': _(u'Select a function that will return the value to this tag.'),
                             'attibute_value': _(u'Select a name of an attribute to write the value in inside this TAG.'),
                             'fixed_value': _(u'Give a fixed static string to write inside this TAG.'),
                             'tag_order': _(u"This TAG's order inside it."),
                            }
        for name, i in self.fields.items():
            i.label = translations.get(name, i.label)
            i.help_text = translations_help.get(name, i.help_text)

    class Meta:
        model = Tag
        fields = (u'parent_tag', u'tag_name', u'multiple',
                  u'value_function', u'attibute_value', u'fixed_value', u'tag_order')

    def clean(self, *args, **kwargs):
        parent_tag = self.cleaned_data.get(u'parent_tag', False)
        if parent_tag and not parent_tag.multiple:
            if parent_tag.fixed_value or parent_tag.value_function \
                or parent_tag.attibute_value:
                raise forms.ValidationError(_(u'The parent tag must not have a value'))
        if self.instance.children.all().count() and not self.cleaned_data.get(u'multiple', False):
            if self.cleaned_data.get(u'fixed_value', False) or \
               self.cleaned_data.get(u'value_function', False) or\
               self.cleaned_data.get(u'attibute_value', False):
                raise forms.ValidationError(
                    _(u'Since this tag has childrens, it cannot have a value.'))
        return self.cleaned_data

class TagCreationForm(forms.ModelForm):

    def __init__(self, config_pk, *args, **kwargs):
        super(TagCreationForm, self).__init__(*args, **kwargs)
        self.config_pk = config_pk
        self.fields['parent_tag'].queryset = \
            self.fields['parent_tag'].queryset.order_by('tag_name')

        translations = {'export_config': _(u'Export Config'),
                        'tag_name': _(u'Tag Name'),
                        'parent_tag': _(u'Parent Tag'),
                        'multiple': _(u'Multiple'),
                        'value_function': _(u'Function to get the value'),
                        'attibute_value': _(u'Attribute Value'),
                        'fixed_value': _(u'Fixed Value'),
                        'tag_order': _(u'Tag order'),
                        }
        translations_help = {'tag_name':  _(u'Give this tag a name.'),
                             'parent_tag': _(u'Select the parent tag if exsists.'),
                             'multiple': _(u'Check this option if this tag can apper more than once.'
                                           u' The value of this tag must be an iterable object.'),
                             'value_function': _(u'Select a function that will return the value to this tag.'),
                             'attibute_value': _(u'Select a name of an attribute to write the value in inside this TAG.'),
                             'fixed_value': _(u'Give a fixed static string to write inside this TAG.'),
                             'tag_order': _(u"This TAG's order inside it."),
                            }
        for name, i in self.fields.items():
            i.label = translations.get(name, i.label)
            i.help_text = translations_help.get(name, i.help_text)


    def save(self, *args, **kwargs):
        self.instance.export_config_id = self.config_pk
        return super(TagCreationForm, self).save(*args, **kwargs)

    class Meta:
        model = Tag
        fields = (u'parent_tag', u'tag_name', u'multiple',
                  u'value_function', u'attibute_value', u'fixed_value', u'tag_order')

class ExportTestForm(forms.Form):
    rbr = forms.CharField(label=u'RBR', max_length=20)
    def clean_rbr(self, *args, **kwargs):
        try:
            self.clinical_trial = ClinicalTrial.objects.get(
                code__iexact=self.cleaned_data.get(u'rbr', 'NO')
            )
        except ClinicalTrial.DoesNotExist:
            raise forms.ValidationError(_(u'Clinical trial not found.'))
        return self.cleaned_data

class TrialImportForm(forms.Form):
    trial_file = forms.FileField(label=ugettext_lazy('Trial File'))

    def clean(self):
        f = self.cleaned_data.get("trial_file", False)
        if f:
            filetype = magic.from_buffer(f.read())
            if not "XML" in filetype:
                raise forms.ValidationError(_(u"File is not XML."))
            f.seek(0)#return reader's read pointer to file's begin
        return self.cleaned_data
