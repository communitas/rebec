#-*- coding: utf-8 -*-
from django.views.generic import ListView, CreateView, DetailView, UpdateView,\
                                 View, DeleteView, FormView
from django.conf import settings
from django.core.cache import cache
from import_export_config.models import ExportConfig, Tag
from import_export_config.forms import TagCreationForm, ExportTestForm, \
                                       TagEditForm, TrialImportForm
from import_export_config.file_generation import generate_all_public_trials_xml                                       
from django.core.urlresolvers import reverse_lazy, reverse
from django.contrib import messages
from django.utils.translation import ugettext as _, ugettext_lazy
from extra_views import UpdateWithInlinesView, NamedFormsetsMixin, InlineFormSet
from django.http import HttpResponse, HttpResponseRedirect
from clinical_trials.models import ClinicalTrial, CTDetail, Sponsor, Contact, \
                                   CTContact, Outcome
from administration.models import InstitutionType, Institution, Country, Setup
from xml.etree import ElementTree


class ExportConfigsList(ListView):
    model = ExportConfig
    template_name = u'import_export_config/export_config_list.html'
    context_object_name = u'configs_list'


class ExportConfigNew(CreateView):
    model = ExportConfig
    template_name = u'base/form_view.html'

    def get_success_url(self):
        messages.success(
            self.request,
            _(u'Created a new export configuration.')
        )
        return reverse_lazy(u'list_export_configs')

class ExportConfigDetail(DetailView):
    model = ExportConfig
    template_name = u'import_export_config/export_config_detail.html'

    def get_context_data(self, *args, **kwargs):
        context = super(ExportConfigDetail, self).get_context_data(
            *args,
            **kwargs
        )
        context[u'test_form'] = ExportTestForm(self.request.POST or None)
        context[u'test_form'].is_valid()
        return context

    def post(self, *args, **kwargs):
        form = ExportTestForm(self.request.POST or None)
        if not form.is_valid():
            return super(ExportConfigDetail, self).get(*args, **kwargs)
        clinical_trial = ClinicalTrial.objects.filter(
            code=self.request.POST.get(u'rbr')
        ).using(u'published_cts').get()

        resp = self.get_object().return_xml_string(
            clinical_trial=clinical_trial)
        if isinstance(resp, list):
            for err in resp:
                messages.error(self.request, err)
            return super(ExportConfigDetail, self).get(*args, **kwargs)

        return HttpResponse(resp)


class ExportAllPublishedTrialsView(View):

    def get(self, *args, **kwargs):
        """
        Get from cache or generate it, the stores in cache.
        The cache can be generated with:
            >> python manage.py generate_public_trials_xml_cache
        """
        resp = cache.get(settings.PUBLIC_TRIALS_XML_CACHE_KEY,
                         None)
        if not resp:
            resp = generate_all_public_trials_xml()
            cache.set(sett.PUBLIC_TRIALS_XML_CACHE_KEY, resp)

        response = HttpResponse(resp, content_type='application/xml')

        response['Content-Disposition'] = 'attachment; filename=RBR-ictrp.xml'
        return response



class ExportConfigEditView(UpdateView):
    model = ExportConfig
    template_name = \
            u'import_export_config/export_config_edit.html'


    def get_all_order_tags(self):
        no_parent_tags = self.object.tags.no_parent_tags().order_by(u'tag_order')
        for i in no_parent_tags:
            yield i
            for j in i.children.order_by(u'tag_order'):
                yield j
                for k in j.children.order_by(u'tag_order'):
                    yield k
                    for l in k.children.order_by(u'tag_order'):
                        yield l
                        for m in l.children.order_by(u'tag_order'):
                            yield m
                            for n in m.children.order_by(u'tag_order'):
                                yield n


    def get_context_data(self, *args, **kwargs):
        context = super(ExportConfigEditView, self).get_context_data(
            *args, **kwargs)
        noparent_tags = context[u'object'].tags.no_parent_tags().order_by('tag_order')
        
        context[u'ordered_tags'] = self.get_all_order_tags()

        context[u'test_form'] = ExportTestForm()
        return context

class CreateNewTagView(CreateView):
    model = Tag
    template_name = u'base/form_view.html'
    form_class = TagCreationForm

    def dispatch(self, *args, **kwargs):
        self.config_pk = kwargs.get(u'config_pk')
        return super(CreateNewTagView, self).dispatch(*args, **kwargs)

    
    def get_form_kwargs(self):
        kw = super(CreateNewTagView, self).get_form_kwargs()
        kw[u'config_pk'] = self.config_pk
        return kw

    def get_form(self, *args, **kwargs):
        form = super(CreateNewTagView, self).get_form(*args, **kwargs)
        form.fields[u'parent_tag'].queryset = \
            form.fields[u'parent_tag'].queryset.filter(
                export_config=self.config_pk
            )
        return form

    def get_success_url(self):
        messages.success(
            self.request,
            _(u'Created a new export tag.')
        )
        return reverse_lazy(u'export_config_edit', args=(self.config_pk,))

class ChangeTagView(UpdateView):
    model = Tag
    template_name = u'base/form_view.html'
    form_class = TagEditForm

    def get_form(self, *args, **kwargs):
        form = super(ChangeTagView, self).get_form(*args, **kwargs)
        form.fields[u'parent_tag'].queryset = \
            form.fields[u'parent_tag'].queryset.filter(
                export_config=self.object.export_config.pk
            )
        return form

    def get_success_url(self):
        return reverse_lazy(
            u'export_config_edit',
            args=(self.object.export_config.pk,)
        )

class RemoveTagView(DeleteView):
    model = Tag
    template_name = u'base/delete_view.html'

    def get_context_data(self, *args, **kwargs):
        context = super(RemoveTagView, self).get_context_data(*args, **kwargs)
        qtd_filhos = self.object.children.all().count()
        if qtd_filhos > 0:
            context[u'warning'] = _(u'This TAG has childrens that will be deleted too')
        return context


    def get_success_url(self):
        messages.success(self.request, _(u'Success deleted %s') % self.object)
        return reverse_lazy(
            u'export_config_edit',
            args=(self.object.export_config.pk,)
        )    


class TestExportView(View):

    def get(self, *args, **kwargs):
        return HttpResponseRedirect(reverse(u'list_export_configs'))

    def post(self, request, pk, *args, **kwargs):
        form = ExportTestForm(request.POST)
        if not form.is_valid():
            for k,i in form.errors.items():
                messages.error(request, u'%s: %s' % (k,''.join(i)))
            return HttpResponseRedirect(
                reverse(u'export_config_edit', args=(pk,)))
        else:
            return self.render_xml(config=ExportConfig.objects.get(pk=pk),
                                   obj=form.clinical_trial)


    def render_xml(self, config, obj):
        response = HttpResponse(config.generate_xml(ct=obj),
                                         content_type='application/xml')
        response['Content-Disposition'] = 'attachment; filename="%s_%s.xml"' \
                                                % (config.file_name, obj.code)
        return response

class TrialImportView(FormView):
    template_name = 'base/form_view.html'
    form_class = TrialImportForm
    step_label = ugettext_lazy(u'Trial import')

    def _get_text(self, parser, path):
        p = path.split('>>')
        current = parser
        for i in p:
            current = current.findall(i)
            if not current:
                return None
            current = current[0]

        return current.text.strip() if current is not None else None


    def _sugest_country(self, description):
        """
        Try to get the country by the 3 first letters, if none found, return
        Brazil as default.
        """
        country = Country.objects.filter(description__istartswith=description)
        if country:
            return country[0]
        else:
            return Country.objects.get(pk=u'BR')

    def form_valid(self, form):
        """
        The form is valid. Import the file.
        """
        f = form.cleaned_data[u'trial_file']
        parser = ElementTree.fromstring(f.read())
        setup = Setup.objects.get()
        new_trial = ClinicalTrial()
        new_trial.language_1_mandatory = setup.language_1_mandatory
        new_trial.language_2_mandatory = setup.language_2_mandatory
        new_trial.language_3_mandatory = setup.language_3_mandatory
        new_trial.user = new_trial.holder = self.request.user
        new_trial.save()
        ctdetail = CTDetail()
        ctdetail.language_code = setup.language_1_code

        ctdetail.public_title = self._get_text(parser, 'trial>>main>>public_title')
        ctdetail.scientific_title = self._get_text(parser, 'trial>>main>>scientific_title')

        inst_type = InstitutionType.objects.get_or_create(description=u'Importação de ensaio')[0]

        primary_sponsor = self._get_text(parser, 'trial>>main>>primary_sponsor')
        if primary_sponsor:
            inst = Institution.objects.get_or_create(
                name=primary_sponsor,
                institution_type=inst_type,
                country=Country.objects.get(pk='BR'),
                description=u'Institution from imported trial')[0]
            sponsor1 = Sponsor(institution=inst,
                               sponsor_type=Sponsor.SPONSOR_TYPE_PRIMARY,
                               clinical_trial=new_trial)
            sponsor1.save()


        contacts = parser.find(u'trial').findall('contacts')

        if contacts:
            for c in contacts[0].findall(u'contact'):
                country_name = self._get_text(c, 'country1')
                country = self._sugest_country(country_name)
                new_contact = Contact.objects.get_or_create(
                    first_name=self._get_text(c, 'firstname'),
                    middle_name=self._get_text(c, 'middlename'),
                    last_name=self._get_text(c, 'lastname'),
                    address=self._get_text(c, 'address'),
                    city=self._get_text(c, 'city'),
                    postal_code=self._get_text(c, 'zip'),
                    phone=self._get_text(c, 'telephone'),
                    email=self._get_text(c, 'email'),
                    country=country,
                    state=u'São Paulo',
                )[0]

                if self._get_text(c, 'affiliation'):
                    inst = Institution.objects.get_or_create(
                        name=self._get_text(c, 'affiliation'),
                        institution_type=inst_type,
                        country=Country.objects.get(pk='BR'),
                        description=u'Institution from imported trial')[0]
                    new_contact.institution = inst
                    new_contact.save()
                contact_type = self._get_text(c, 'type')
                contact_type = {'scientific': 'S',
                                'public': 'P',
                                'site': 'W'}[contact_type.lower()]
                CTContact.objects.create(
                    contact_type=contact_type,
                    contact=new_contact,
                    clinical_trial=new_trial)

        ctdetail.inclusion_criteria = self._get_text(parser, 'trial>>criteria>>inclusion_criteria')
        ctdetail.exclusion_criteria = self._get_text(parser, 'trial>>criteria>>exclusion_criteria')

        if self._get_text(parser, 'trial>>primary_outcome>>prim_outcome'):
            new_trial.outcome_set.create(
                outcome_type=Outcome.TYPE_PRIMARY,
            )
            
        if self._get_text(parser, 'trial>>secondary_outcome>>sec_outcome'):
            new_trial.outcome_set.create(
                outcome_type=Outcome.TYPE_SECONDARY,
            )
            
        for i in parser.find('trial').findall('countries'):
            for country in i.findall(u'country'):
                new_trial.recruitment_countries.add(
                    self._sugest_country(country.text)
                )

        new_trial.save()
        ctdetail.clinical_trial = new_trial
        ctdetail.save()
        self.new_trial_id = new_trial.pk
        return super(TrialImportView, self).form_valid(form)

    def get_success_url(self):
        messages.success(self.request, u'Clinical trial imported, you can edit it below')
        return reverse(u'clinical_trial_edit_list', args=(self.new_trial_id,))
