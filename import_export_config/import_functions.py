#-*- coding: utf-8 -*-
from django.utils.translation import ugettext_lazy as __
from datetime import datetime

ALL_FUNCTIONS = []

#value to return to not show this tag.
NOT_SHOW_VALUE = u'sd5dsc2x1c84dsTDxs4cd541c2x1c6d1c68sd'

def register_function(function, help_text):
    ALL_FUNCTIONS.append((function, help_text))

#def test_function(trial_instance):
#    return 'returned value'
#register_function(test_function, u'A test function')

#def test_inner_function(loop_object, loop_index):
#    return loop_object.description

#register_function(test_function,
#    u'A test of inner function, inside a multiple TAG')


def get_utn(trial_instance):
	try:
		return trial_instance.identifier_set.get(
				setup_identifier__name='UTN').code
	except trial_instance.identifier_set.model.DoesNotExist:
		return ''

register_function(get_utn,
    __(u'Return the UTN value'))

def type_enrollment(trial_instance):
	now = datetime.now().date()
	if trial_instance.date_first_enrollment is not None and \
	trial_instance.date_last_enrollment is not None and \
	trial_instance.date_first_enrollment <= now and \
	trial_instance.date_last_enrollment >= now:
		return u'actual'

	return u'antecipated'

register_function(type_enrollment,
    __(u'Return the Type of the Enrollment'))

def primary_sponsor(trial_instance):
    try:
        return trial_instance.sponsor_set.get(
                sponsor_type='P').institution.name
    except trial_instance.sponsor_set.model.DoesNotExist:
        return ''

register_function(primary_sponsor,
    __(u'Return the Primary Sponsor value'))

def ctdetail_public_title(trial_instance):
    try:
        return trial_instance.ctdetail_set.get(
                language_code='English (United States)').public_title
    except trial_instance.ctdetail_set.model.DoesNotExist:
        return ''

register_function(ctdetail_public_title,
    __(u'Return the Public title.'))


def ctdetail_acronym(trial_instance):
    try:
        return trial_instance.ctdetail_set.get(
                language_code='English (United States)').acronym
    except trial_instance.ctdetail_set.model.DoesNotExist:
        return ''

register_function(ctdetail_acronym,
    __(u'Return the Acronym.'))

def ctdetail_scientific_title(trial_instance):
    try:
        return trial_instance.ctdetail_set.get(
                language_code='English (United States)').scientific_title
    except trial_instance.ctdetail_set.model.DoesNotExist:
        return ''

register_function(ctdetail_scientific_title,
    __(u'Return the Scientific Title .'))

def recruitment_status(trial_instance):
    bool_rec, string_rec = trial_instance.get_recruitment_status(
                               localized=False
                           )
    return string_rec

register_function(recruitment_status,
    __(u'Return the Recruitment status.'))

def ct_url(trial_instance):
    return trial_instance.get_absolute_public_url()

register_function(ct_url,
    __(u'Return the URL.'))

def ctdetail_inclusion_criteria(trial_instance):
    try:
        return trial_instance.ctdetail_set.get(
                language_code='English (United States)').inclusion_criteria
    except trial_instance.ctdetail_set.model.DoesNotExist:
        return ''

register_function(ctdetail_inclusion_criteria,
    __(u'Return the Inclusion Criteria.'))

def ctdetail_exclusion_criteria(trial_instance):
    try:
        return trial_instance.ctdetail_set.get(
                language_code='English (United States)').exclusion_criteria
    except trial_instance.ctdetail_set.model.DoesNotExist:
        return ''

register_function(ctdetail_exclusion_criteria,
    __(u'Return the Exclusion Criteria.'))

def ctdetail_health_condition(trial_instance):
    try:
        return trial_instance.ctdetail_set.get(
                language_code='English (United States)').health_condition
    except trial_instance.ctdetail_set.model.DoesNotExist:
        return ''

register_function(ctdetail_health_condition,
    __(u'Return the Health Condition.'))

def ct_outcome_primary(trial_instance):
    try:
        return trial_instance.outcome_set.get(
                outcome_type='P').found_outcome
    except trial_instance.outcome_set.model.DoesNotExist:
        return ''

register_function(ct_outcome_primary,
    __(u'Return the Outcome Primary.'))

def ct_outcome_secondary(trial_instance):
    try:
        return trial_instance.outcome_set.get(
                outcome_type='S').found_outcome
    except trial_instance.outcome_set.model.DoesNotExist:
        return ''

register_function(ct_outcome_secondary,
    __(u'Return the Outcome Secondary.'))

def secondary_sponsor(trial_instance):
    try:
        return trial_instance.sponsor_set.filter(
                sponsor_type='S')
    except trial_instance.sponsor_set.model.DoesNotExist:
        return ''

register_function(secondary_sponsor,
    __(u'Return the all Secondary Sponsors'))


def ct_contacts(trial_instance):
    try:
        return trial_instance.ctcontact_set.filter(contact_type__in=(u'P', u'S'))
    except trial_instance.ctcontact_set.model.DoesNotExist:
        return ''

register_function(ct_contacts,
    __(u'Return the Contacts.'))

def ct_healthcondition(trial_instance):
    try:
        return trial_instance.descriptor_set.filter(descriptor_type='G')
    except trial_instance.descriptor_set.model.DoesNotExist:
        return ''

register_function(ct_healthcondition,
    __(u'Return the all Helth Condition General.'))

def ct_healthcondition_specific(trial_instance):
    try:
        return trial_instance.descriptor_set.filter(descriptor_type='S')
    except trial_instance.descriptor_set.model.DoesNotExist:
        return ''

register_function(ct_healthcondition_specific,
    __(u'Return the all Helth Condition Specific.'))


def health_condition_vocabulary_item_pt_code(descriptor_instance):
    return descriptor_instance.vocabulary_item_pt.split(u' -')[0]

register_function(health_condition_vocabulary_item_pt_code,
    __(u'Return the descriptor code of the vocabulary_item pt.'))


def ct_interventioncode(trial_instance):
    try:
        return trial_instance.interventions.all()
    except trial_instance.interventions.model.DoesNotExist:
        return ''

register_function(ct_interventioncode,
    __(u'Return the all Intervention Code.'))

def ct_identifier(trial_instance):
    try:
        return trial_instance.identifier_set.exclude(
                   setup_identifier__name=u'UTN'
               )
    except trial_instance.identifier_set.model.DoesNotExist:
        return ''

register_function(ct_identifier,
    __(u'Return the all Identifiers(except UTN).'))

def ct_countries(trial_instance):
    try:
        return trial_instance.recruitment_countries.all()
    except trial_instance.recruitment_countries.model.DoesNotExist:
        return ''

register_function(ct_countries,
    __(u'Return the all Countries.'))

def ct_study_type(trial_instance):
    if trial_instance.study_type == 'I':
        return 'Interventional'
    return 'Observational'

register_function(ct_study_type,
    __(u'Return the all Study Type.'))

def ct_study_design(trial_instance):
    if trial_instance.study_type == 'I':
        try:
            text = u"Clinical Trial for " + trial_instance.study_purpose.description + ", " + trial_instance.allocation_type.description + ", " + trial_instance.intervention_assignment.description + "," + trial_instance.masking_type.description + "," + str(trial_instance.number_arms) + "," + u" phase " + trial_instance.study_phase.description
            return text
        except:
            return ''

    else:
        try:
            text = ""
            if trial_instance.followup_period == 'TR':
                text += 'Transversal'
            else:
                text += 'Longitudinal'
            if trial_instance.reference_period == 'RT':
                text += ' Retrospective'
            else:
                text += ' Prospective'
            return text
        except:
            return ''

register_function(ct_study_design,
    __(u'Return the all Study Design.'))

def ct_phase(trial_instance):
    if trial_instance.study_type == 'I':
        return trial_instance.study_phase.description
    return ''

register_function(ct_phase,
    __(u'Return the Phase value.'))

def ct_ifreetext(trial_instance):
    if trial_instance.study_type == 'I':
        try:
            interventions = trial_instance.interventions.all()
            text_description = ''
            for i in interventions:
                text_description += i.description
            return text_description
        except trial_instance.interventions.model.DoesNotExist:
            return ''
    else:
        try:
            observation = trial_instance.intervention_observations.all()
            text_description = ''
            for i in observation:
                text_description += i.description
            return text_description
        except trial_instance.intervention_observations.model.DoesNotExist:
            return ''

register_function(ct_ifreetext,
    __(u'Return the IFreetext.'))

def ct_source_support(trial_instance):
    try:
        return trial_instance.sponsor_set.filter(
                sponsor_type='U')
    except trial_instance.sponsor_set.model.DoesNotExist:
        return ''

register_function(ct_source_support,
    __(u'Return the Source Support.'))


def contact_institution_name(ctcontact_instance):
    return ctcontact_instance.contact.institution.name \
               if ctcontact_instance.contact.institution else ""

register_function(contact_institution_name,
    __(u"Return the Contact's institution name."))

def contact_type(ctcontact_instance):
    try:
        type_cont = dict(
                      ctcontact_instance.TYPE_CHOICES_ENGLISH
                    ).get(ctcontact_instance.contact_type, None)
        return type_cont
    except ctcontact_instance.model.DoesNotExist:
        return ''

register_function(contact_type,
    __(u"Return the Contact Type."))

def date_last_or_first_enrollment(trial_instance):
    return trial_instance.date_last_enrollment or trial_instance.date_first_enrollment

register_function(date_last_or_first_enrollment,
    __(u'Return the Date of Last the Enrollment, or the First'))


def sponsor_name(sponsor_instance):
    if not sponsor_instance.institution:
        return sponsor_instance.support_source
    description = sponsor_instance.institution.name + ' - '
    if sponsor_instance.institution.city:
        description += sponsor_instance.institution.city
    if sponsor_instance.institution.state:
        description += ', ' + sponsor_instance.institution.state
    if sponsor_instance.institution.country_id:
        description += ', ' + sponsor_instance.institution.country.description
    return description

register_function(sponsor_name,
    __(u'Return the Sponsor Name with the right format'))


def issuing_body(identifier_instance):
    if identifier_instance.setup_identifier.name.strip() in (u'OUTROS', u'OTHERS'):
        return identifier_instance.description
    return identifier_instance.setup_identifier.name

register_function(issuing_body,
    __(u'Return the Issuing Body'))

