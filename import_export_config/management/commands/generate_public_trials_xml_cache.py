#-*- coding: utf-8 -*-
from datetime import datetime

from django.core.management.base import BaseCommand
from django.conf import settings
from django.core.cache import cache

class Command(BaseCommand):
    help = u'Generates the xml of all published trials and stores it in CACHE'

    def _write_log(self, msg):
        now = datetime.now().strftime(u'[%d/%m/%Y %H:%M:%S]')
        self.stdout.write(u'{now} {msg}'.format(now=now, msg=msg))

    def handle(self, *args, **options):
        from import_export_config.file_generation import generate_all_public_trials_xml
        self._write_log(u'Starting...')
        xml_content = generate_all_public_trials_xml()
        cache.set(settings.PUBLIC_TRIALS_XML_CACHE_KEY,
                  xml_content)
        self._write_log(u'Finished!')

#StateObjectHistory.objects.filter(state__pk=State.STATE_PUBLICADO).distinct(u'content_id').values_list(u'content_id', flat=True)