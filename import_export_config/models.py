#-*- coding: utf-8 -*-
from xml.sax.saxutils import escape
from django.db import models
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext as _
from import_export_config.import_functions import ALL_FUNCTIONS as IMPORT_FUNCTIONS
from clinical_trials.models import ClinicalTrial

def multi_getattr(obj, attr, **kw):
    """
    Get multiple nested attrs separated by "."
    """
    attributes = attr.split(".")
    for i in attributes:
        try:
            obj = getattr(obj, i)
            if callable(obj):
                obj = obj()
        except AttributeError:
            if kw.has_key('default'):
                return kw['default']
            else:
                raise
    return obj

def get_tag_string(tag, obj, inside_loop=False):
    """
    Recursive function to get the tag String.
    """
    xml = ''
    if tag.multiple and inside_loop or not tag.multiple:
        xml += tag._get_xml_tag_open()
    value = tag.get_value(obj=obj) if not inside_loop else None
    if tag.multiple and not inside_loop:
        for current_obj in value:
            xml += get_tag_string(tag, current_obj, inside_loop=True)
    else:
        if not tag.children.exists():
            xml += escape(unicode(value if value is not None else u''))
        else:
            for child_tag in tag.children.all():
                xml += get_tag_string(child_tag, obj)
    if tag.multiple and inside_loop or not tag.multiple:
        xml += tag._get_xml_tag_close()
    return xml

class ExportConfig(models.Model):
    name = models.CharField(_(u'Name'), max_length=50)
    file_name = models.CharField(_(u'File Name Prefix'), max_length=100)
    active = models.BooleanField(_(u'Active'), default=True)

    def __init__(self, *args, **kwargs):
        super(ExportConfig, self).__init__(*args, **kwargs)
        translations = {'name': _(u'Name'),
                        'file_name': _(u'File Name Prefix'),
                        'active': _(u'Active')}
        for i in self._meta.fields:
            i.verbose_name = translations.get(i.name, i.verbose_name)

    def get_absolute_url(self):
        return reverse(u'export_config_edit', args=(self.pk,))

    def get_model_name(self):
        return _(u'Export Configuration')

    def __unicode__(self):
        return self.name

    def generate_xml(self, ct, with_declaration=True):
        published_ct = ClinicalTrial.objects.using(u'published_cts').get(
            code=ct.code
        )
        if with_declaration:
            xml = u'<?xml version="1.0" encoding="UTF-8"?>'
        else:
            xml = u''
        for tag in self.tags.no_parent_tags():
            xml += get_tag_string(tag=tag, obj=published_ct)
        return xml

class TagManager(models.Manager):
    def no_parent_tags(self):
        return self.get_query_set().filter(parent_tag__isnull=True)

class Tag(models.Model):
    objects = TagManager()

    export_config = models.ForeignKey(
        ExportConfig,
        verbose_name=_(u'Export Configuration'),
        related_name=u'tags'
    )
    tag_name = models.CharField(
        _(u'Tag Name'),
        max_length=100,
        null=True, blank=True,
        help_text=_(u'Give this tag a name.'),
    )
    parent_tag = models.ForeignKey(
        u'self',
        related_name='children',
        verbose_name=_(u'Parent Tag'),
        null=True,
        blank=True,
        help_text=_(u'Select the parent tag if exsists.'),
    )
    multiple = models.BooleanField(
        _(u'Multiple'),
        default=False,
        help_text=_(u'Check this option if this tag can apper more than once.'
                    u' The value of this tag must be an iterable object.'),
    )
    value_function = models.CharField(
        _(u'Value Function'),
        max_length=100,
        null=True,
        blank=True,
        choices=[(i[0].func_name, i[1]) for i in IMPORT_FUNCTIONS],
        help_text=_(u'Select a function that will return the value to this tag.'),
    )
    attibute_value = models.CharField(
        _(u'Attribute Value'),
        max_length=100,
        null=True,
        blank=True,
        help_text=_(u'Select a name of an attribute to write the value in inside this TAG.'),
    )

    fixed_value = models.CharField(
        _(u'Fixed Value'),
        max_length=100,
        null=True,
        blank=True,
        help_text=_(u'Give a fixed static string to write inside this TAG.'),
    )
    tag_order = models.IntegerField(
        _(u'Tag Order'),
        max_length=100,
        default=1,
        help_text=_(u"This TAG's order inside it's parent."),
    )

    def __init__(self, *args, **kwargs):
        super(Tag, self).__init__(*args, **kwargs)
        translations = {'export_config': _(u'Export Config'),
                        'tag_name': _(u'Tag Name'),
                        'parent_tag': _(u'Parent Tag'),
                        'multiple': _(u'Multiple'),
                        'value_function': _(u'Function to get the value'),
                        'attibute_value': _(u'Attribute Value'),
                        'fixed_value': _(u'Fixed Value'),
                        'tag_order': _(u'Tag order'),
                        }
        for i in self._meta.fields:
            i.verbose_name = translations.get(i.name, i.verbose_name)

    class Meta:
        ordering = [u'tag_order']

    def __unicode__(self):
        if self.parent_tag_id:
            return unicode(self.parent_tag) + ' > ' + self. tag_name
        return self.tag_name

    def as_tree(self):
        children = list(self.children.all())
        branch = bool(children)
        yield branch, self
        for child in children:
            for next in child.as_tree():
                yield next
        yield branch, None

    def get_model_name(self):
        return _(u'Export Configuration Tags')

    def _get_xml_tag_open(self):
        if self.tag_name:
            return u'<%s>' % self.tag_name
        return u''

    def _get_xml_tag_close(self):
        if self.tag_name:
            return u'</%s>' % self.tag_name
        return u''

    def get_value(self, obj):
        if self.fixed_value:
            return self.fixed_value
        if self.value_function:
            function_to_call = \
                [i for i in IMPORT_FUNCTIONS \
                    if i[0].func_name == self.value_function][0][0]
            return function_to_call(obj)
        if self.attibute_value:
            value = eval('obj.%s' % self.attibute_value)
            #value = multi_getattr(obj, self.attibute_value)
            if callable(value):
                return value()
            else:
                return value
        return ""
