# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'Tag.tag_name'
        db.alter_column(u'import_export_config_tag', 'tag_name', self.gf('django.db.models.fields.CharField')(max_length=100, null=True))

    def backwards(self, orm):

        # Changing field 'Tag.tag_name'
        db.alter_column(u'import_export_config_tag', 'tag_name', self.gf('django.db.models.fields.CharField')(default='', max_length=100))

    models = {
        u'import_export_config.exportconfig': {
            'Meta': {'object_name': 'ExportConfig'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'file_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'import_export_config.tag': {
            'Meta': {'ordering': "[u'tag_order']", 'object_name': 'Tag'},
            'attibute_value': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'export_config': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'tags'", 'to': u"orm['import_export_config.ExportConfig']"}),
            'fixed_value': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'multiple': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'parent_tag': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'children'", 'null': 'True', 'to': u"orm['import_export_config.Tag']"}),
            'tag_name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'tag_order': ('django.db.models.fields.IntegerField', [], {'default': '1', 'max_length': '100'}),
            'value_function': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['import_export_config']