#-*- coding: utf-8 -*-
"""
Url for this app.
"""
from django.conf.urls import patterns, url
from django.contrib.auth.decorators import login_required
from import_export_config import views

urlpatterns = patterns(
    '',
    url(
        r'list-export-configs/$',
        login_required(
            views.ExportConfigsList.as_view()),
        name='list_export_configs'),

    url(
        r'list-export-configs/new/$',
        login_required(
            views.ExportConfigNew.as_view()),
        name='create_new_configuration'),

    url(
        r'list-export-configs/(?P<pk>\d+)/edit/$',
        login_required(
            views.ExportConfigEditView.as_view()),
        name='export_config_edit'),

    url(
        r'list-export-configs/(?P<config_pk>\d+)/edit/new-tag/$',
        login_required(
            views.CreateNewTagView.as_view()),
        name='create_new_export_tag'),

    url(
        r'list-export-configs/tags/(?P<pk>\d+)/edit/$',
        login_required(
            views.ChangeTagView.as_view()),
        name='change_export_tag'),

    url(
        r'list-export-configs/tags/(?P<pk>\d+)/remove/$',
        login_required(
            views.RemoveTagView.as_view()),
        name='remove_export_tag'),

    url(
        r'list-export-configs/(?P<pk>\d+)/test-export/$',
        login_required(
            views.TestExportView.as_view()),
        name='test_export_xml'),


    url(
        r'import-trial/$',
        login_required(
            views.TrialImportView.as_view()),
        name='import_trial'),

    url(
        r'export-all-trials/$',
            views.ExportAllPublishedTrialsView.as_view(),
        name='export_all_trials'),

)
