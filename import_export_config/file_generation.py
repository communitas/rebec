#-*- coding: utf-8 -*-
from clinical_trials.models import ClinicalTrial
from import_export_config.models import ExportConfig, Tag
from import_export_config import import_functions
from django.shortcuts import get_object_or_404

OPEN_TAG = u'<%s>'
CLOSE_TAG = u'</%s>'

LINE_BREAK = '\b'

def make_ident(level):
    return ' ' * level

def new_line_with_ident(level):
    return LINE_BREAK + make_ident(level)

def get_tag_content(tag, object_instance):
    """
    Return a tag function
    """
    #depending on the type of return type,
    #function, attributo or fixed value
    if tag.value_function:
        return unicode(getattr(
            import_functions, tag.value_function)(object_instance))
    elif tag.attibute_value:
        return unicode(getattr(object_instance, tag.attibute_value))
    elif tag.fixed_value:
        return tag.fixed_value
    else:
        return ''

def print_tag(tag, object_instance, ignore_multiple_flag=False):
    #open the tag
    tag_content = OPEN_TAG % tag.tag_name

    #tag content(even the children tags)
    if tag.multiple and not ignore_multiple_flag:
        for current in get_tag_content(tag, object_instance):
            return print_tag(current, current, ignore_multiple_flag=True)

    elif not tag.children.exists():
        tag_content += get_tag_content(tag, object_instance)

    else:
        for child_tag in tag.children.all():
            tag_content += print_tag(child_tag, object_instance)

    #close the tag
    tag_content += CLOSE_TAG % tag.tag_name

    return tag_content

def generate_file(export_config_id, clinical_trial_id):

    file_content = u''
    export_config = get_object_or_404(ExportConfig, pk=export_config_id)
    clinical_trial = get_object_or_404(ClinicalTrial, pk=clinical_trial_id)

    #loop in all the tags with no parent tag
    for tag1 in export_config.tags.filter(parent_tag__isnull=True):
        file_content += print_tag(tag1, clinical_trial)

    return file_content


def generate_all_public_trials_xml():
    """
    Generate a XML content string with all published trials.
    """
    export_config = ExportConfig.objects.get()
    clinical_trials = ClinicalTrial.objects.all().using(u'published_cts')
    resp = u''
    for i in clinical_trials:
        resp = u'%s%s' % (resp, export_config.generate_xml(
            ct=i,
            with_declaration=False))
    resp = u'<trials>%s</trials>' % resp
    return resp
