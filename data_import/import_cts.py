#-*- coding: utf-8 -*-
"""
Arquivos necessários:
    /tmp/cts.csv
    /tmp/users.csv
    /tmp/trials_id.csv
    /tmp/trials_revisor.csv
    /tmp/contacts.csv
    /tmp/contacts_site.csv
    /tmp/contacts_public.csv
    /tmp/contacts_scientific.csv
    /tmp/countries.csv
"""
TIPO_DE_ESTUDO = 0
TITULO_CIENTIFICO_PT = 1
TITULO_CIENTIFICO_EN = 2
TITULO_CIENTIFICO_ES = 3
UTN = 4
TITULO_PUBLICO_PT = 5
TITULO_PUBLICO_EN = 6
TITULO_PUBLICO_ES = 7
ACRONIMO_CIENTIFICO_PT = 8
ACRONIMO_CIENTIFICO_EN = 9
ACRONIMO_CIENTIFICO_ES = 10
ID_SECUNDARIOS_1 = 11
ID_SECUNDARIOS_2 = 12
ID_SECUNDARIOS_3 = 13
PATROCINADOR_PRIMARIO = 14
PATROCINADOR_SECUNDARIO_1 = 15
PATROCINADOR_SECUNDARIO_2 = 16
APOIO_FINANCEIRO_OU_MATERIAL_1 = 17
APOIO_FINANCEIRO_OU_MATERIAL_2 = 18
CONDICOES_DE_SAUDE_PT = 19
CONDICOES_DE_SAUDE_EN = 20
CONDICOES_DE_SAUDE_ES = 21
DESCRITORES_GERAIS_1_PT = 22
DESCRITORES_GERAIS_1_EN = 23
DESCRITORES_GERAIS_1_ES = 24
DESCRITORES_GERAIS_2_PT = 25
DESCRITORES_GERAIS_2_EN = 26
DESCRITORES_GERAIS_2_ES = 27
DESCRITORES_ESPECIFICOS_1_PT = 28
DESCRITORES_ESPECIFICOS_1_EN = 29
DESCRITORES_ESPECIFICOS_1_ES = 30
DESCRITORES_ESPECIFICOS_2_PT = 31
DESCRITORES_ESPECIFICOS_2_EN = 32
DESCRITORES_ESPECIFICOS_2_ES = 33
CATEGORIA_DAS_INTERVENCOES_1_PT = 34
CATEGORIA_DAS_INTERVENCOES_1_EN = 35
CATEGORIA_DAS_INTERVENCOES_1_ES = 36
CATEGORIA_DAS_INTERVENCOES_2_PT = 37
CATEGORIA_DAS_INTERVENCOES_2_EN = 38
CATEGORIA_DAS_INTERVENCOES_2_ES = 39
INTERVENCOES_PT = 40
INTERVENCOES_EN = 41
INTERVENCOES_ES = 42
DESCRITORES_INTERVENCAO_1_PT = 43
DESCRITORES_INTERVENCAO_1_EN = 44
DESCRITORES_INTERVENCAO_1_ES = 45
DESCRITORES_INTERVENCAO_2_PT = 46
DESCRITORES_INTERVENCAO_2_EN = 47
DESCRITORES_INTERVENCAO_2_ES = 48
DESCRITORES_INTERVENCAO_3_PT = 49
DESCRITORES_INTERVENCAO_3_EN = 50
DESCRITORES_INTERVENCAO_3_ES = 51
SITUACAO_DO_RECRUTAMENTO = 52
PAIS_DE_RECRUTAMENTO_1 = 53
PAIS_DE_RECRUTAMENTO_2 = 54
PAIS_DE_RECRUTAMENTO_3 = 55
PAIS_DE_RECRUTAMENTO_4 = 56
DATA_PRIMEIRO_RECRUTAMENTO = 57
DATA_ULTIMO_RECRUTAMENTO = 58
TAMANHO_DA_AMOSTRA_ALVO = 59
GENERO = 60
IDADE_MIN = 61
UNIDADE_IDADE_MIN = 62
IDADE_MAX = 63
UNIDADE_IDADE_MAX = 64
CRITERIOS_DE_INCLUSAO_PT = 65
CRITERIOS_DE_INCLUSAO_EN = 66
CRITERIOS_DE_INCLUSAO_ES = 67
DESENHO_DO_ESTUDO_PT = 68
DESENHO_DO_ESTUDO_EN = 69
DESENHO_DO_ESTUDO_ES = 70
PROGRAMA_DE_ACESSO = 71
ENFOQUE_DO_ESTUDO = 72
DESENHO_DA_INTERVENCAO = 73
BRACOS = 74
MASCARAMENTO = 75
ALOCACAO = 76
FASE = 77
DESENHO_ESTUDO_OBSERVACIONAL = 78
TEMPORALIDADE = 79
DESFECHO_PRIMARIO_1_PT = 80
DESFECHO_PRIMARIO_1_EN = 81
DESFECHO_PRIMARIO_1_ES = 82
DESFECHO_PRIMARIO_2_PT = 83
DESFECHO_PRIMARIO_2_EN = 84
DESFECHO_PRIMARIO_2_ES = 85
DESFECHO_SECUNDARIO_1_PT = 86
DESFECHO_SECUNDARIO_1_EN = 87
DESFECHO_SECUNDARIO_1_ES = 88
DESFECHO_SECUNDARIO_2_PT = 89
DESFECHO_SECUNDARIO_2_EN = 90
DESFECHO_SECUNDARIO_2_ES = 91
LOGIN = 92
EMAIL = 93
STATUS = 94


def get_lines(file_abspath):
    import csv
    csvfile =  open(file_abspath, 'rb')
    spamreader = csv.reader(csvfile, delimiter=';', quotechar='"')
    for c, i in enumerate(spamreader):
        if c > 0:
            yield i

def get_lines_comma(file_abspath):
    import csv
    csvfile =  open(file_abspath, 'rb')
    spamreader = csv.reader(csvfile, delimiter=',', quotechar='"')
    for c, i in enumerate(spamreader):
        if c > 0:
            yield i



import_logs = []
lista_rbrs = [i for i in get_lines('/tmp/rbr.csv') if i[1].lower() != 'none']
lista_contacts_site = [i for i in get_lines('/tmp/contacts_site.csv')]
lista_contacts_public = [i for i in get_lines('/tmp/contacts_public.csv')]
lista_contacts_scientific = [i for i in get_lines('/tmp/contacts_scientific.csv')]
lista_countries = [{'id': i.split(';')[0], 'code': i.split(';')[1], 'description': i.split(';')[2]} for i in open('/tmp/countries.csv').readlines()[1:]]

def get_country(pk):
    print 'Buscando country %s' % pk
    try:
        return Country.objects.get(code=[i for i in lista_countries if i['id'] == pk][0]['code'])
    except Country.DoesNotExist:
        print 'CRIANDO COUNTRY %s' % [i for i in lista_countries if i['id'] == pk][0]['code']
        return Country.objects.create(code=[i for i in lista_countries if i['id'] == pk][0]['code'], description=[i for i in lista_countries if i['id'] == pk][0]['description'])

from login.models import RebecUser
from django.contrib.auth.models import Group
REGISTRANT_GROUP = Group.objects.get(name=u'registrant')
REVISER_GROUP = Group.objects.get(name='reviser')


def import_users():
    for current, user_data in enumerate(get_lines('/tmp/users.csv')):
        print user_data
        user = RebecUser.objects.get_or_create(
            pk=user_data[0],
            username=user_data[1].strip(),
            first_name=user_data[2].strip(),
            last_name=user_data[3].strip(),
            email=user_data[4].strip(),
            is_staff=user_data[6].strip() != '0',
            is_active=user_data[7].strip() != '0',
            is_superuser=user_data[8].strip() != '0',
            gender=1,
            race=1,
            country_id='BR',
        )[0]
        user.name = '%s %s' %(user.first_name, user.last_name)
        user.registrationprofile_set.get_or_create(
                activation_key=user.registrationprofile_set.model.ACTIVATED
        )
        user.set_password('rebec')
        print 'Created user %s... %s' % (user_data[1].strip(), current)
        user.groups.add(REGISTRANT_GROUP)
        user.save()

from clinical_trials.models import ClinicalTrial, CTDetail, Identifier, Sponsor, Contact
from administration.models import SetupIdentifier, InstitutionType, Institution, \
    InterventionCode, Vocabulary, VocabularyItem, Country

def import_contacts():
    for current, contact_data in enumerate(get_lines('/tmp/contacts.csv')):
        print contact_data
        contact = Contact.objects.get_or_create(
            pk=contact_data[0].strip(),
            first_name=contact_data[1].strip(),
            middle_name=contact_data[2].strip(),
            last_name=contact_data[3].strip(),
            email=contact_data[4].strip(),
            #institution=Institution.objects.get(pk=1),
            address=contact_data[6].strip(),
            city=contact_data[7].strip(),
            #state=contact_data[8].strip(),
            country=get_country(contact_data[8]),
            postal_code=contact_data[10].strip(),
            phone=contact_data[11].strip(),
        )
        print 'Created contact %s... %s' % (contact_data[1].strip(), current)

try:
    utn_object = SetupIdentifier.objects.get_or_create(name='UTN', mask='UNNNN-NNNN-NNNN')[0]
except SetupIdentifier.DoesNotExist:
    print u'Please create the UTN setup identifier before continue'

inst_type = InstitutionType.objects.get_or_create(description=u'Importação de dados')[0]
vocabulary = Vocabulary.objects.get_or_create(
    name=u'Import',
    version=u'1.00',
    current_version=True,
    usage=[Vocabulary.USAGE_INTERVENTIONS,
               Vocabulary.USAGE_HEALTH_CONDITIONS]
)[0]

donos_trials = [[i.split(';')[0].strip(), i.split(';')[1].strip()] for i in open('/tmp/trials_revisor.csv').readlines()]
donos_trials = {i[0]: i[1] for i in donos_trials}

utns_trials = [[i.split(';')[0].strip(), i.split(';')[1].strip()] for i in open('/tmp/trials_id.csv').readlines()]
utns_trials = {i[1]: i[0] for i in utns_trials}

def import_ct(ct_data, number_on_file):
    ct = ClinicalTrial()
    from login.models import RebecUser
    try:
        ct.user = RebecUser.objects.get(username=ct_data[LOGIN])
        ct.holder = RebecUser.objects.get(username=ct_data[LOGIN])
    except RebecUser.DoesNotExist:
        import_logs.append('Ensaio %s sem usuario (deveria ser %s).' % (number_on_file, ct_data[LOGIN]))
        print 'Ensaio sem usuario.'
        return

    #LOGIN = 92
    ct.save()
    from administration.models import Language
    lang_english = Language.objects.get(code='en')
    lang_spanish = Language.objects.get(code='es')
    lang_portuguese = Language.objects.get(code='pt')
    ctdetail_pt = CTDetail()
    ctdetail_en = CTDetail()
    ctdetail_es = CTDetail()
    ctdetail_pt.language_code = lang_portuguese
    ctdetail_en.language_code = lang_english
    ctdetail_es.language_code = lang_spanish
    ct.study_type = ct_data[TIPO_DE_ESTUDO][0].upper()

    ctdetail_pt.scientific_title = ct_data[TITULO_CIENTIFICO_PT]
    ctdetail_en.scientific_title = ct_data[TITULO_CIENTIFICO_EN]
    ctdetail_es.scientific_title = ct_data[TITULO_CIENTIFICO_ES]

    if ct_data[UTN].strip().upper() not in ('NONE', '') and \
        'NAO DEFINIDO' not in ct_data[UTN].strip().upper():
            identifier = Identifier(
                setup_identifier=utn_object,
                clinical_trial=ct,
                code=ct_data[UTN].strip())
            try:
                identifier.save()
            except Exception ,exc:
                import ipdb;ipdb.set_trace()

    else:
        import_logs.append('UTN nao definida para ensaio %s' % number_on_file)

    if 'NAO DEFINIDO' not in ct_data[TITULO_PUBLICO_PT].strip().upper():
        ctdetail_pt.public_title = ct_data[TITULO_PUBLICO_PT]
    if 'NAO DEFINIDO' not in ct_data[TITULO_PUBLICO_EN].strip().upper():
        ctdetail_en.public_title = ct_data[TITULO_PUBLICO_EN]
    if 'NAO DEFINIDO' not in ct_data[TITULO_PUBLICO_ES].strip().upper():
        ctdetail_es.public_title = ct_data[TITULO_PUBLICO_ES]

    if 'NAO DEFINIDO' not in ct_data[ACRONIMO_CIENTIFICO_PT].strip().upper():
        ctdetail_pt.scientific_acronym = ct_data[ACRONIMO_CIENTIFICO_PT]

    if 'NAO DEFINIDO' not in ct_data[ACRONIMO_CIENTIFICO_EN].strip().upper():
        ctdetail_en.scientific_acronym = ct_data[ACRONIMO_CIENTIFICO_EN]

    if 'NAO DEFINIDO' not in ct_data[ACRONIMO_CIENTIFICO_ES].strip().upper():
        ctdetail_es.scientific_acronym = ct_data[ACRONIMO_CIENTIFICO_ES]


    from administration.models import Country
    if 'NAO DEFINIDO' not in ct_data[PATROCINADOR_PRIMARIO].strip().upper():
        inst = Institution.objects.get_or_create(
            name=ct_data[PATROCINADOR_PRIMARIO],
            institution_type=inst_type,
            country=Country.objects.get(pk='BR'),
            description=u'Institution imported')[0]

        sponsor1 = Sponsor(institution=inst,
                               sponsor_type=Sponsor.SPONSOR_TYPE_PRIMARY,
                               clinical_trial=ct)
        sponsor1.save()
        if ct_data[STATUS].strip() in ('published', 'pending', 'resubmit'):
            inst.status = Institution.STATUS_ACTIVE
            inst.save()

    if 'NAO DEFINIDO' not in ct_data[PATROCINADOR_SECUNDARIO_1].strip().upper():
        inst = Institution.objects.get_or_create(
            name=ct_data[PATROCINADOR_SECUNDARIO_1],
            institution_type=inst_type,
            country=Country.objects.get(pk='BR'),
            description=u'Institution imported')[0]
        sponsor2 = Sponsor(institution=inst,
                           sponsor_type=Sponsor.SPONSOR_TYPE_SECUNDARY,
                           clinical_trial=ct)
        sponsor2.save()
        if ct_data[STATUS].strip() in ('published', 'pending', 'resubmit'):
            inst.status = Institution.STATUS_ACTIVE
            inst.save()

    if 'NAO DEFINIDO' not in ct_data[PATROCINADOR_SECUNDARIO_2].strip().upper():
        inst = Institution.objects.get_or_create(
            name=ct_data[PATROCINADOR_SECUNDARIO_2],
            institution_type=inst_type,
            country=Country.objects.get(pk='BR'),
            description=u'Institution imported')[0]
        sponsor3 = Sponsor(institution=inst,
                               sponsor_type=Sponsor.SPONSOR_TYPE_SUPPORT,
                               clinical_trial=ct)
        sponsor3.save()
        if ct_data[STATUS].strip() in ('published', 'pending', 'resubmit'):
            inst.status = Institution.STATUS_ACTIVE
            inst.save()
    #ID_SECUNDARIOS_1 = 11
    #ID_SECUNDARIOS_2 = 12
    #ID_SECUNDARIOS_3 = 13

    #APOIO_FINANCEIRO_OU_MATERIAL_1 = 17
    #APOIO_FINANCEIRO_OU_MATERIAL_2 = 18
    if 'NAO DEFINIDO' not in ct_data[CONDICOES_DE_SAUDE_PT].strip().upper():
        ctdetail_pt.health_condition = ct_data[CONDICOES_DE_SAUDE_PT]

    if 'NAO DEFINIDO' not in ct_data[CONDICOES_DE_SAUDE_EN].strip().upper():
        ctdetail_en.health_condition = ct_data[CONDICOES_DE_SAUDE_EN]

    if 'NAO DEFINIDO' not in ct_data[CONDICOES_DE_SAUDE_ES].strip().upper():
        ctdetail_es.health_condition = ct_data[CONDICOES_DE_SAUDE_ES]

    from clinical_trials.models import Descriptor
    if 'Nao definido'.upper() not in \
        ct_data[DESCRITORES_GERAIS_1_PT].upper() and \
      'Nao definido'.upper() not in \
        ct_data[DESCRITORES_GERAIS_1_EN].upper() and \
      'Nao definido'.upper() not in \
        ct_data[DESCRITORES_GERAIS_1_ES].upper():
      

        descriptor_general1 = Descriptor.objects.create(
            clinical_trial=ct,
            descriptor_type=Descriptor.TYPE_GENERAL,
            vocabulary_item= VocabularyItem.objects.get_or_create(
                description_lang_1=ct_data[DESCRITORES_GERAIS_1_PT],
                description_lang_2=ct_data[DESCRITORES_GERAIS_1_EN],
                description_lang_3=ct_data[DESCRITORES_GERAIS_1_ES],
                vocabulary=vocabulary,
            )[0]
        )

    if 'Nao definido'.upper() not in \
        ct_data[DESCRITORES_GERAIS_2_PT].upper() and \
      'Nao definido'.upper() not in \
        ct_data[DESCRITORES_GERAIS_2_EN].upper() and \
      'Nao definido'.upper() not in \
        ct_data[DESCRITORES_GERAIS_2_ES].upper():
        descriptor_general2 = Descriptor.objects.create(
            clinical_trial=ct,
            descriptor_type=Descriptor.TYPE_GENERAL,
            vocabulary_item= VocabularyItem.objects.get_or_create(
                description_lang_1=ct_data[DESCRITORES_GERAIS_2_PT],
                description_lang_2=ct_data[DESCRITORES_GERAIS_2_EN],
                description_lang_3=ct_data[DESCRITORES_GERAIS_2_ES],
                vocabulary=vocabulary,
            )[0]
        )

    if 'Nao definido'.upper() not in \
        ct_data[DESCRITORES_ESPECIFICOS_1_PT].upper() and \
      'Nao definido'.upper() not in \
        ct_data[DESCRITORES_ESPECIFICOS_1_EN].upper() and \
      'Nao definido'.upper() not in \
        ct_data[DESCRITORES_ESPECIFICOS_1_ES].upper():

        descriptor_specific1 = Descriptor.objects.create(
            clinical_trial=ct,
                    descriptor_type=Descriptor.TYPE_SPECIFIC,
            vocabulary_item= VocabularyItem.objects.get_or_create(
                description_lang_1=ct_data[DESCRITORES_ESPECIFICOS_1_PT],
                description_lang_2=ct_data[DESCRITORES_ESPECIFICOS_1_EN],
                description_lang_3=ct_data[DESCRITORES_ESPECIFICOS_1_ES],
                vocabulary=vocabulary,
            )[0]
        )


    if 'Nao definido'.upper() not in \
        ct_data[DESCRITORES_ESPECIFICOS_2_PT].upper() and \
      'Nao definido'.upper() not in \
        ct_data[DESCRITORES_ESPECIFICOS_2_EN].upper() and \
      'Nao definido'.upper() not in \
        ct_data[DESCRITORES_ESPECIFICOS_2_ES].upper():
        descriptor_specific2 = Descriptor.objects.create(
            clinical_trial=ct,
            descriptor_type=Descriptor.TYPE_SPECIFIC,
            vocabulary_item= VocabularyItem.objects.get_or_create(
                description_lang_1=ct_data[DESCRITORES_ESPECIFICOS_2_PT],
                description_lang_2=ct_data[DESCRITORES_ESPECIFICOS_2_EN],
                description_lang_3=ct_data[DESCRITORES_ESPECIFICOS_2_ES],
                vocabulary=vocabulary,
            )[0]
        )


    #DESCRITORES_GERAIS_1_PT = 22
    #DESCRITORES_GERAIS_1_EN = 23
    #DESCRITORES_GERAIS_1_ES = 24
    #DESCRITORES_GERAIS_2_PT = 25
    #DESCRITORES_GERAIS_2_EN = 26
    #DESCRITORES_GERAIS_2_ES = 27
    #DESCRITORES_ESPECIFICOS_1_PT = 28
    #DESCRITORES_ESPECIFICOS_1_EN = 29
    #DESCRITORES_ESPECIFICOS_1_ES = 30
    #DESCRITORES_ESPECIFICOS_2_PT = 31
    #DESCRITORES_ESPECIFICOS_2_EN = 32
    #DESCRITORES_ESPECIFICOS_2_ES = 33
    if 'Nao definido'.upper() not in \
        ct_data[CATEGORIA_DAS_INTERVENCOES_1_PT].upper():
        int_code = InterventionCode.objects.get_or_create(
            description=ct_data[CATEGORIA_DAS_INTERVENCOES_1_PT]
        )[0]
        ct.intervention_codes.add(int_code)

    if 'Nao definido'.upper() not in \
        ct_data[CATEGORIA_DAS_INTERVENCOES_2_PT].upper():
        int_code2 = InterventionCode.objects.get_or_create(
            description=ct_data[CATEGORIA_DAS_INTERVENCOES_2_PT]
        )[0]
        ct.intervention_codes.add(int_code2)

    #ct.intervention_codes.add(int_code2)
    if 'Nao definido' not in ct_data[INTERVENCOES_PT]:
        ctdetail_pt.intervention = ct_data[INTERVENCOES_PT]
    if 'Nao definido' not in ct_data[INTERVENCOES_EN]:
        ctdetail_en.intervention = ct_data[INTERVENCOES_EN]
    if 'Nao definido' not in ct_data[INTERVENCOES_ES]:
        ctdetail_es.intervention = ct_data[INTERVENCOES_ES]

    if 'Nao definido'.upper() not in \
        ct_data[DESCRITORES_INTERVENCAO_1_PT].upper() and \
      'Nao definido'.upper() not in \
        ct_data[DESCRITORES_INTERVENCAO_1_EN].upper() and \
      'Nao definido'.upper() not in \
        ct_data[DESCRITORES_INTERVENCAO_1_ES].upper():
      

        descriptor_general1 = Descriptor.objects.create(
            clinical_trial=ct,
            descriptor_type=Descriptor.TYPE_INTERVENTION,
            vocabulary_item= VocabularyItem.objects.get_or_create(
                description_lang_1=ct_data[DESCRITORES_INTERVENCAO_1_PT],
                description_lang_2=ct_data[DESCRITORES_INTERVENCAO_1_EN],
                description_lang_3=ct_data[DESCRITORES_INTERVENCAO_1_EN],
                vocabulary=vocabulary,
            )[0]
        )

    if 'Nao definido'.upper() not in \
        ct_data[DESCRITORES_INTERVENCAO_2_PT].upper() and \
      'Nao definido'.upper() not in \
        ct_data[DESCRITORES_INTERVENCAO_2_EN].upper() and \
      'Nao definido'.upper() not in \
        ct_data[DESCRITORES_INTERVENCAO_2_ES].upper():
      

        descriptor_general1 = Descriptor.objects.create(
            clinical_trial=ct,
            descriptor_type=Descriptor.TYPE_INTERVENTION,
            vocabulary_item= VocabularyItem.objects.get_or_create(
                description_lang_1=ct_data[DESCRITORES_INTERVENCAO_2_PT],
                description_lang_2=ct_data[DESCRITORES_INTERVENCAO_2_EN],
                description_lang_3=ct_data[DESCRITORES_INTERVENCAO_2_EN],
                vocabulary=vocabulary,
            )[0]
        )

    if 'Nao definido'.upper() not in \
        ct_data[DESCRITORES_INTERVENCAO_3_PT].upper() and \
      'Nao definido'.upper() not in \
        ct_data[DESCRITORES_INTERVENCAO_3_EN].upper() and \
      'Nao definido'.upper() not in \
        ct_data[DESCRITORES_INTERVENCAO_3_ES].upper():
      

        descriptor_general1 = Descriptor.objects.create(
            clinical_trial=ct,
            descriptor_type=Descriptor.TYPE_INTERVENTION,
            vocabulary_item= VocabularyItem.objects.get_or_create(
                description_lang_1=ct_data[DESCRITORES_INTERVENCAO_3_PT],
                description_lang_2=ct_data[DESCRITORES_INTERVENCAO_3_EN],
                description_lang_3=ct_data[DESCRITORES_INTERVENCAO_3_EN],
                vocabulary=vocabulary,
            )[0]
        )
    #DESCRITORES_INTERVENCAO_1_PT = 43
    #DESCRITORES_INTERVENCAO_1_EN = 44
    #DESCRITORES_INTERVENCAO_1_ES = 45
    #DESCRITORES_INTERVENCAO_2_PT = 46
    #DESCRITORES_INTERVENCAO_2_EN = 47
    #DESCRITORES_INTERVENCAO_2_ES = 48
    #DESCRITORES_INTERVENCAO_3_PT = 49
    #DESCRITORES_INTERVENCAO_3_EN = 50
    #DESCRITORES_INTERVENCAO_3_ES = 51

    #SITUACAO_DO_RECRUTAMENTO = 52
    if Country.objects.filter(description=ct_data[PAIS_DE_RECRUTAMENTO_1]).exists():
        ct.recruitment_countries.add(
            Country.objects.get(
                description=ct_data[PAIS_DE_RECRUTAMENTO_1]
            )
        )

    if Country.objects.filter(description=ct_data[PAIS_DE_RECRUTAMENTO_2]).exists():
        ct.recruitment_countries.add(
            Country.objects.get(
                description=ct_data[PAIS_DE_RECRUTAMENTO_2]
            )
        )

    if Country.objects.filter(description=ct_data[PAIS_DE_RECRUTAMENTO_3]).exists():
        ct.recruitment_countries.add(
            Country.objects.get(
                description=ct_data[PAIS_DE_RECRUTAMENTO_3]
            )
        )

    if Country.objects.filter(description=ct_data[PAIS_DE_RECRUTAMENTO_4]).exists():
        ct.recruitment_countries.add(
            Country.objects.get(
                description=ct_data[PAIS_DE_RECRUTAMENTO_4]
            )
        )
    #PAIS_DE_RECRUTAMENTO_1 = 53
    #PAIS_DE_RECRUTAMENTO_2 = 54
    #PAIS_DE_RECRUTAMENTO_3 = 55
    #PAIS_DE_RECRUTAMENTO_4 = 56
    from datetime import datetime
    if ct_data[DATA_PRIMEIRO_RECRUTAMENTO].strip().upper() not in ('NONE', ''):
        try:
            ct.date_first_enrollment = datetime.strptime(
                ct_data[DATA_PRIMEIRO_RECRUTAMENTO].strip(), '%Y-%m-%d'
            )
        except ValueError:
            try:
                ct.date_first_enrollment = datetime.strptime(
                    ct_data[DATA_PRIMEIRO_RECRUTAMENTO].strip(), '%d/%m/%Y'
                )
            except ValueError:
                ct.date_first_enrollment = datetime.strptime(
                    ct_data[DATA_PRIMEIRO_RECRUTAMENTO].strip(), '%d/%m%Y'
                )


    if ct_data[DATA_ULTIMO_RECRUTAMENTO].strip().upper() not in ('NONE', ''):
        try:
            ct.date_last_enrollment = datetime.strptime(
                ct_data[DATA_ULTIMO_RECRUTAMENTO].strip(), '%Y-%m-%d'
            )
        except ValueError:
            try:
                ct.date_last_enrollment = datetime.strptime(
                    ct_data[DATA_ULTIMO_RECRUTAMENTO].strip(), '%d/%m/%Y'
                )
            except ValueError:
                ct.date_last_enrollment = datetime.strptime(
                    ct_data[DATA_ULTIMO_RECRUTAMENTO].strip(), '%d/%m%Y'
                )
    #DATA_PRIMEIRO_RECRUTAMENTO = 57
    #DATA_ULTIMO_RECRUTAMENTO = 58

    ct.target_sample_size = ct_data[TAMANHO_DA_AMOSTRA_ALVO]
    #TAMANHO_DA_AMOSTRA_ALVO = 59
    ct.gender = {'F': 'F', 'M': 'M', 'A': 'B'}[ct_data[GENERO][0].upper()]
    #GENERO = 60

    ct.inclusion_minimum_age = ct_data[IDADE_MIN]
    #IDADE_MIN = 61
    if ct_data[UNIDADE_IDADE_MIN].strip().upper() == 'SEM LIMITE':
        ct.minimum_age_no_limit = True
    else:
        ct.minimum_age_no_limit = False
        ct.minimum_age_unit = \
            {'A': 'Y',
             'M': 'M',
             'S': 'W',
             'D': 'D',
             'H': 'H'}[ct_data[UNIDADE_IDADE_MIN][0].upper()]
        #UNIDADE_IDADE_MIN = 62

    ct.inclusion_minimum_age = ct_data[IDADE_MAX]
    #IDADE_MAX = 63
    if ct_data[UNIDADE_IDADE_MAX].strip().upper() == 'SEM LIMITE':
        ct.maximum_age_no_limit = True
    else:
        ct.maximum_age_no_limit = False
        ct.maximum_age_unit = \
            {'A': 'Y',
             'M': 'M',
             'S': 'W',
             'D': 'D',
             'H': 'H'}[ct_data[UNIDADE_IDADE_MAX][0].upper()]
        #UNIDADE_IDADE_MAX = 64

    ctdetail_pt.inclusion_criteria = ct_data[CRITERIOS_DE_INCLUSAO_PT].strip()
    #CRITERIOS_DE_INCLUSAO_PT = 65

    ctdetail_en.inclusion_criteria = ct_data[CRITERIOS_DE_INCLUSAO_EN].strip()
    #CRITERIOS_DE_INCLUSAO_EN = 66

    ctdetail_es.inclusion_criteria = ct_data[CRITERIOS_DE_INCLUSAO_ES].strip()
    #CRITERIOS_DE_INCLUSAO_ES = 67

    ctdetail_pt.study_design = ct_data[DESENHO_DO_ESTUDO_PT].strip()
    #DESENHO_DO_ESTUDO_PT = 68

    ctdetail_en.study_design = ct_data[DESENHO_DO_ESTUDO_EN].strip()
    #DESENHO_DO_ESTUDO_EN = 69

    ctdetail_es.study_design = ct_data[DESENHO_DO_ESTUDO_ES].strip()
    #DESENHO_DO_ESTUDO_ES = 70

    ct.expanded_access_program = {'D': 'U', 'S': 'Y', 'N': 'N'}[ct_data[PROGRAMA_DE_ACESSO][0].upper()]
    #PROGRAMA_DE_ACESSO = 71

    from administration.models import StudyPurpose
    if 'Nao definido' not in ct_data[ENFOQUE_DO_ESTUDO]:
        ct.study_purpose = \
            StudyPurpose.objects.get_or_create(
                description=ct_data[ENFOQUE_DO_ESTUDO].strip()
            )[0]
    #ENFOQUE_DO_ESTUDO = 72

    DESENHO_DA_INTERVENCAO = 73 #TODO: ver qual campo é

    if ct_data[BRACOS] != 'None':
        ct.number_arms = ct_data[BRACOS]
    #BRACOS = 74

    from administration.models import MaskingType
    if 'Nao definido' not in ct_data[MASCARAMENTO]:
        ct.masking_type = \
            MaskingType.objects.get_or_create(
                description=ct_data[MASCARAMENTO].strip()
            )[0]
    #MASCARAMENTO = 75

    from administration.models import AllocationType
    if 'Nao definido' not in ct_data[ALOCACAO]:
        ct.allocation_type = \
            AllocationType.objects.get_or_create(
                description=ct_data[ALOCACAO].strip()
            )[0]
    #ALOCACAO = 76

    from administration.models import StudyPhase
    if 'Nao definido' not in ct_data[FASE]:
        ct.study_phase = \
            StudyPhase.objects.get_or_create(
                description=ct_data[FASE].strip()
            )[0]
    #FASE = 77

    from administration.models import ObservationStudyDesign
    if 'Nao definido' not in ct_data[DESENHO_ESTUDO_OBSERVACIONAL]:
        ct.observational_study_design = \
            ObservationStudyDesign.objects.get_or_create(
                description=ct_data[DESENHO_ESTUDO_OBSERVACIONAL].strip()
            )[0]
    #DESENHO_ESTUDO_OBSERVACIONAL = 78


    #TEMPORALIDADE = 79#TODO: see where this information goes
    from clinical_trials.models import Outcome
    odl1 = None
    odl2 = None
    odl3 = None
    if 'Nao definido' not in ct_data[DESFECHO_PRIMARIO_1_PT]:
        odl1=ct_data[DESFECHO_PRIMARIO_1_PT].strip()
    if 'Nao definido' not in ct_data[DESFECHO_PRIMARIO_1_EN]:
        odl2=ct_data[DESFECHO_PRIMARIO_1_EN].strip()
    if 'Nao definido' not in ct_data[DESFECHO_PRIMARIO_1_ES]:
        odl3=ct_data[DESFECHO_PRIMARIO_1_ES].strip()

    if odl1 or odl2 or odl3:
        try:
            ct.outcome_set.create(
                outcome_type=Outcome.TYPE_PRIMARY,
                description_lang_1=odl1,
                description_lang_2=odl2,
                description_lang_3=odl3,
            )
        except Exception ,exc:
                import ipdb;ipdb.set_trace()
    #DESFECHO_PRIMARIO_1_PT = 80
    #DESFECHO_PRIMARIO_1_EN = 81
    #DESFECHO_PRIMARIO_1_ES = 82

    odl1 = None
    odl2 = None
    odl3 = None
    if 'Nao definido' not in ct_data[DESFECHO_SECUNDARIO_1_PT]:
        odl1=ct_data[DESFECHO_SECUNDARIO_1_PT].strip()
    if 'Nao definido' not in ct_data[DESFECHO_SECUNDARIO_1_EN]:
        odl2=ct_data[DESFECHO_SECUNDARIO_1_EN].strip()
    if 'Nao definido' not in ct_data[DESFECHO_SECUNDARIO_1_ES]:
        odl3=ct_data[DESFECHO_SECUNDARIO_1_ES].strip()

    if odl1 or odl2 or odl3:
        try:
            ct.outcome_set.create(
                outcome_type=Outcome.TYPE_SECONDARY,
                description_lang_1=odl1,
                description_lang_2=odl2,
                description_lang_3=odl3,
            )
        except Exception ,exc:
                import ipdb;ipdb.set_trace()
    #DESFECHO_SECUNDARIO_1_PT = 86
    #DESFECHO_SECUNDARIO_1_EN = 87
    #DESFECHO_SECUNDARIO_1_ES = 88


    odl1 = None
    odl2 = None
    odl3 = None
    if 'Nao definido' not in ct_data[DESFECHO_SECUNDARIO_2_PT]:
        odl1=ct_data[DESFECHO_SECUNDARIO_2_PT].strip()
    if 'Nao definido' not in ct_data[DESFECHO_SECUNDARIO_2_EN]:
        odl2=ct_data[DESFECHO_SECUNDARIO_2_EN].strip()
    if 'Nao definido' not in ct_data[DESFECHO_SECUNDARIO_2_ES]:
        odl3=ct_data[DESFECHO_SECUNDARIO_2_ES].strip()

    if odl1 or odl2 or odl3:
        ct.outcome_set.create(
            outcome_type=Outcome.TYPE_SECONDARY,
            description_lang_1=odl1,
            description_lang_2=odl2,
            description_lang_3=odl3,
        )

    #DESFECHO_SECUNDARIO_2_PT = 89
    #DESFECHO_SECUNDARIO_2_EN = 90
    #DESFECHO_SECUNDARIO_2_ES = 91
    from administration.models import TimePerspective
    if 'Nao Definido'.upper() not in ct_data[TEMPORALIDADE].upper():
        ct.time_perspective = \
            TimePerspective.objects.get_or_create(
                description=ct_data[TEMPORALIDADE].strip()
            )[0]
    ct.save()
    ctdetail_pt.clinical_trial = ct
    ctdetail_en.clinical_trial = ct
    ctdetail_es.clinical_trial = ct
    ctdetail_pt.save()
    ctdetail_en.save()
    ctdetail_es.save()


    #IMPORTA CONTATOS DO SITE
    if ct.identifier_set.all().exists():
        trial_id = utns_trials.get(ct.identifier_set.all()[0].code, None)
        if trial_id is not None:
            for talvez_contact_site in lista_contacts_site:
                if trial_id == talvez_contact_site[0].strip():
                    ct.ctcontact_set.create(
                        contact_type=CTContact.TYPE_SITE_QUERIES,
                        contact=Contact.objects.get(pk=int(talvez_contact_site[1].strip())))
                    print 'Criado contato de site %s para o CT %s' % (talvez_contact_site[1], ct.pk)

    #IMPORTA CONTATOS PUBLICOS
    if ct.identifier_set.all().exists():
        trial_id = utns_trials.get(ct.identifier_set.all()[0].code, None)
        if trial_id is not None:
            for talvez_contact_public in lista_contacts_public:
                if trial_id == talvez_contact_public[0].strip():
                    ct.ctcontact_set.create(
                        contact_type=CTContact.TYPE_PUBLIC_QUERIES,
                        contact=Contact.objects.get(pk=int(talvez_contact_public[1].strip())))
                    print 'Criado contato publico %s para o CT %s' % (talvez_contact_public[1], ct.pk)


    #IMPORTA CONTATOS CIENTIFICOS
    if ct.identifier_set.all().exists():
        trial_id = utns_trials.get(ct.identifier_set.all()[0].code, None)
        if trial_id is not None:
            for talvez_contact_scientific in lista_contacts_scientific:
                if trial_id == talvez_contact_scientific[0].strip():
                    ct.ctcontact_set.create(
                        contact_type=CTContact.TYPE_SCIENTIFIC_QUERIES,
                        contact=Contact.objects.get(pk=int(talvez_contact_scientific[1].strip())))
                    print 'Criado contato cientifico %s para o CT %s' % (talvez_contact_scientific[1], ct.pk)



    from workflows.models import State, Transition
    from workflows.utils import do_transition
    statuses = {'draft': State.STATE_EM_PREENCHIMENTO,
    'pending': State.STATE_AGUARDANDO_MODERACAO,
    'published': State.STATE_PUBLICADO,
    'resubmit': State.STATE_RESUBMETIDO}
    if ct_data[STATUS].strip() == 'pending':
        do_transition(ct, Transition.objects.get(pk=Transition.TRANSITION_CONCLUIR_PREENCHIMENTO), ct.user, u'Automatic transition on import to Rebec2.0.')
    if ct_data[STATUS].strip() == 'resubmit':
        do_transition(ct, Transition.objects.get(pk=Transition.TRANSITION_CONCLUIR_PREENCHIMENTO), ct.user, u'Automatic transition on import to Rebec2.0.')
        do_transition(ct, Transition.objects.get(pk=Transition.TRANSITION_RESUBMETER), ct.user, u'Automatic transition on import to Rebec2.0.')
    if ct_data[STATUS].strip() == 'published':
        do_transition(ct, Transition.objects.get(pk=Transition.TRANSITION_CONCLUIR_PREENCHIMENTO), ct.user, u'Automatic transition on import to Rebec2.0.')
        do_transition(ct, Transition.objects.get(pk=Transition.TRANSITION_CONCLUIR_REVISAO), ct.user, u'Automatic transition on import to Rebec2.0.')


    if ct.identifier_set.all().exists():
        trial_id = utns_trials.get(ct.identifier_set.all()[0].code, None)
        if trial_id is not None:
            reviser_of_this = donos_trials.get(trial_id, None)
            if reviser_of_this is not None:
                try:
                    ct.holder = RebecUser.objects.get(username=reviser_of_this)
                    ct.save()
                    ct.holder.groups.clear()
                    ct.holder.groups.add(Group.objects.get(name='reviser'))
                    ct.holder.save()
                    print 'Clinical trial %s atribuido para o revisor %s' \
                        % (ct_data[UTN], ct.holder.username)
                except RebecUser.DoesNotExist:
                    import_logs.append('REVISOR NAO ENCONTRADO %s' % reviser_of_this)
                    print 'REVISOR NAO ENCONTRADO %s' % reviser_of_this
            else:
                if ct.get_workflow_state().pk == State.STATE_AGUARDANDO_REVISAO:
                    from clinical_trials.helpers import get_next_by_occupation
                    #ct.holder = get_next_by_occupation(u'reviser')
                    #if ct.holder:
                    #    print 'Aplicado balanceamento de carga para o ct %s, que ficou para o revisor %s' % (trial_id, ct.holder.username)


    if ct.identifier_set.all().exists():
        trial_id = utns_trials.get(ct.identifier_set.all()[0].code, None)
        if trial_id is not None:
            for talvez_rbr in lista_rbrs:
                if trial_id == talvez_rbr[0]:
                    ct.code = talvez_rbr[1]
                    ct.save()
                    print 'Atribuido RBR %s para o CT %s' % (talvez_rbr[1], ct.pk)

    ct.save()



EMAIL = 93
STATUS = 94





def import_all_cts():
    for c, line in enumerate(get_lines('/tmp/cts.csv')):
        import_ct(line, c)
        print 'imported %s' % c
    print "#"
    print "##"
    print "###"
    print "####"
    print "LOGS DE PROBLEMAS ENCONTRADOS:"
    if not len(import_logs):
        print "Nenhum problema :)"
    for import_log in import_logs:
        print import_log
print "IMPORTANDO USERS"
#import_users()    
print "FIM"
print "IMPORTANDO CONTATOS"
import_contacts()    
print "FIM"
print "IMPORTANDO ENSAIOS"
#import_all_cts()
print "FIM"

print 'INICIANDO AJUSTE DE CLINICAL TRIALS SEM REVISOR'
for ct in ClinicalTrial.objects.filter(holder__isnull=True):
    if ct.get_workflow_state().pk == State.STATE_AGUARDANDO_REVISAO:
        from clinical_trials.helpers import get_next_by_occupation
        ct.holder = get_next_by_occupation(u'reviser')
        if ct.holder:
            print 'Aplicado balanceamento de carga para o ct %s, que ficou para o revisor %s' % (ct.pk, ct.holder.username)
print 'FIM'