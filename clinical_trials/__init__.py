#-*- coding: utf-8 -*-
"""
clinical_trials app.

This app is responsible for the CRUD of the Clinical Trials.
"""

CLINICAL_TRIALS_STEPS_FIELDS = {
    u'related_queries':  {2: ('ctattachment_set',),
                                    9: ('ctcontact_set',),
                                    8: ('outcome_set',),
                                    6: ('descriptor_set',),
                                    5: ['interventions', 'intervention_observations'],
                                    3: ('sponsor_set',),
                                    1: ('identifier_set',),},

    u'ctdetail_fields': {1: ('scientific_title',
                           'public_title', 'acronym',
                           'study_description'),
                           4: ('health_condition', ),
                           6: ('inclusion_criteria', 'exclusion_criteria',),
                        },
    u'ct_fields': {1: (u'study_final_date', 'study_type'),
                 5: ('intervention_codes',),
                 6: ('recruitment_countries',
                     'date_first_enrollment', 'date_last_enrollment',
                     'gender', 'minimum_age_no_limit',
                     'inclusion_minimum_age', 'minimum_age_unit',
                     'maximum_age_no_limit', 'inclusion_maximum_age',
                     'maximum_age_unit', 'world_target_size',
                     'brazil_target_size', 'statistical_sample_analysis'),
                 7: ('expanded_access_program',
                     'study_purpose', 'intervention_assignment',
                     'number_arms', 'masking_type', 'allocation_type',
                     'study_phase', 'followup_period', 'reference_period')},
}