#-*- coding: utf-8 -*-
import json
import urllib
from datetime import datetime
from lxml import html, etree
from django.template import RequestContext
from django.views.generic import ListView, CreateView, DetailView, \
    DeleteView, View, UpdateView, FormView, TemplateView
from django.utils import timezone
from clinical_trials import CLINICAL_TRIALS_STEPS_FIELDS
from clinical_trials.models import ClinicalTrial, CTDetail, Identifier, \
    Sponsor, Descriptor, CTContact, CTAttachment, Outcome, Contact, \
    CTIntervention, CTInterventionObservation
from clinical_trials.forms import ClinicalTrialForm, \
    CTDetailIdentificationFormSet, CTDetailIdentificationForm, \
    CTDetailHealthConditionFormSet, ClinicalTrialSponsorForm, IdentifierForm, \
    HealthConditionsDescriptorForm, \
    ClinicalTrialInterventionAssignmentsForm, CTDetailRecruitmentFormSet, \
    ClinicalRecruitmentStepForm, \
    ClinicalStudyTypeStepForm, CTContactForm, CTAttachmentForm, OutcomeForm, \
    InterventionsDescriptorForm, ClinicalTrialManagementForm, \
    ClinicalTrialManagementSearchForm, ClinicalTrialGroupChangeForm, \
    ContactForm2, ShowJustRevisorsAndEvaluatorsForm, \
    ClinicalTrialIdentificationForm, AddNewWorkflowGeneralObservationForm, \
    ChangeStudyStatusForm
from django.contrib import messages
from django.db.models import FieldDoesNotExist
from django.utils.translation import ugettext as _
from django.core.urlresolvers import reverse, reverse_lazy
from extra_views import CreateWithInlinesView, UpdateWithInlinesView, \
    InlineFormSet, NamedFormsetsMixin, FormSetView, ModelFormSetView
from administration.models import Setup, Institution, InstitutionType, Country
from django.shortcuts import get_object_or_404, render_to_response
from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.core.exceptions import PermissionDenied
from django import forms
from django.forms.models import modelform_factory
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from generics.mixins import PermissionRequiredMixin
from administration.models import CTGroupUser, CTGroup, SetupIdentifier
from workflows.utils import get_allowed_transitions, do_transition, get_state
from workflows.models import State, Transition
from django.contrib.auth.models import Group
from django.core.exceptions import PermissionDenied
from django.contrib.contenttypes.models import ContentType
from django.utils import translation
from clinical_trials.helpers import get_next_by_occupation, generate_trial_id
from django.utils.safestring import mark_safe
from django.forms.widgets import Textarea
from django.db.models import Q
from django.template.loader import render_to_string
from django.conf import settings
from django.core.mail import send_mail
from import_export_config.models import ExportConfig
from log.middleware import GetLog
from log.utils import get_steps_with_changed_fields, get_changed_fields

def disable_all_formset_fields(formset):
    for form in formset:
        for field in form:
            field.field.widget.attrs[u'readonly'] = u'readonly'
            field.field.widget.attrs[u'disabled'] = u'disabled'

def disable_all_form_fields(form):
    for field in form:
        field.field.widget.attrs[u'readonly'] = u'readonly'
        field.field.widget.attrs[u'disabled'] = u'disabled'

def edit_views_get_object(self, obj):
    if self.request.user.is_registrant():
        if obj.holder == self.request.user or \
           getattr(obj.group, u'pk', None) in \
                self.request.user.ctgroups.filter(
                    type_user=CTGroupUser.TYPE_ADMINISTRATOR
                ).values_list(u'ct_group__pk', flat=True):

            if obj.get_workflow_state().pk in (
                State.STATE_EM_PREENCHIMENTO,
                State.STATE_REVISANDO_PREENCHIMENTO,
                State.STATE_RESUBMETIDO,
                State.STATE_REVISAO_PREENCHIMENTO_RESUBMETIDO,
            ):
                return obj
    if self.request.user.is_reviser():
        if obj.get_workflow_state().pk in (
            State.STATE_EM_REVISAO,
            State.STATE_AGUARDANDO_REVISAO):
            return obj
    if self.request.user.is_evaluator():
        if obj.get_workflow_state().pk in (
            State.STATE_EM_AVALIACAO,
            State.STATE_AGUARDANDO_AVALIACAO):
            return obj

    #threats the situation of "reopen for filling"
    if obj.get_workflow_state().pk == State.STATE_PUBLICADO \
       and obj.user == self.request.user \
       and self.request.method == 'POST' \
       and self.request.POST.get(u'reopen_for_filling', False) == u'yes':
       return obj

    raise PermissionDenied


class ClinicalTrialAddView(CreateView):
    """
    View to add a clinical trial
    """
    model = ClinicalTrial
    form_class = ClinicalTrialForm

    def dispatch(self, request):
        if not request.user.is_registrant():
            raise PermissionDenied

        return super(ClinicalTrialAddView, self).dispatch(request)

    def get_context_data(self, *args, **kwargs):
        """
        Add the terms of use to the context
        """
        context = super(ClinicalTrialAddView, self).get_context_data(
                *args,
                **kwargs)
        lang = translation.get_language()
        setup = Setup.objects.get()

        lang2 = []
        if setup.language_2_code:
            lang2.append(setup.language_2_code.code.lower())
            try:
                lang2.append(setup.language_2_code.code.lower().split('-')[0])
            except:
                pass

        lang3 = []
        if setup.language_3_code:
            lang3.append(setup.language_3_code.code.lower())
            try:
                lang3.append(setup.language_3_code.code.lower().split('-')[0])
            except:
                pass

        if lang.lower() in lang2:
            context[u'terms_of_use'] = Setup.objects.get().terms_of_use_lang2
        elif lang.lower() in lang3:
            context[u'terms_of_use'] = Setup.objects.get().terms_of_use_lang3
        else:
            context[u'terms_of_use'] = Setup.objects.get().terms_of_use

        return context

    def get_form_kwargs(self):
        """
        Returns the keyword arguments for instantiating the form.
        """
        kwargs = super(ClinicalTrialAddView, self).get_form_kwargs()
        kwargs.update({'instance': self.object})
        kwargs.update({'request': self.request})
        return kwargs

    def form_valid(self, form, *args, **kwargs):
        """
        Set the create instance's user to the logged in user.
        """
        form.instance.user = self.request.user
        form.instance.holder = self.request.user

        return_content = super(ClinicalTrialAddView, self).form_valid(
                form,
                *args,
                **kwargs)

        if form.instance.group:
            for i in form.instance.group.users.filter(type_user='a'):
                accept_in_group_link = reverse(
                                           'clinical_trial_accept_in_group',
                                           args=(form.instance.pk,)
                                       )
                email_text = render_to_string(
                                 u'clinical_trials/email_to_approve_trial.html',
                                 {u'link': accept_in_group_link,
                                  u'site_url': settings.SITE_URL})

                send_mail(
                    _(u'Approve of clinical trial'),
                    email_text,
                    None,
                    [i.user.email])

        return return_content

    def get_success_url(self):
        """
        On the success, create the necessaries CTDetail's instances,
        set a success message and return the url to redirect.
        """
        setup = Setup.objects.get()
        if self.object.language_1_mandatory:
            self.object.ctdetail_set.create(language_code=setup.language_1_code)
        if self.object.language_2_mandatory:
            self.object.ctdetail_set.create(language_code=setup.language_2_code)
        if self.object.language_3_mandatory:
            self.object.ctdetail_set.create(language_code=setup.language_3_code)

        messages.success(self.request, _(u'Clinical Trial Saved'))
        return reverse(u'clinical_trial_edit_list', args=(self.object.pk,))


class ClinicalTrialDetailView(DetailView):
    """
    Shows the details of a clinical trial.
    """
    model = ClinicalTrial

    def get_object(self):
        """
        Check the detail access permissions based on the user
        function
        """
        obj = super(ClinicalTrialDetailView, self).get_object()
        if self.request.user.is_registrant() \
           and self.request.user.groups.all().count() == 1:
            if not obj.user == self.request.user:
                if getattr(obj.group, u'pk', None) not in \
                    self.request.user.ctgroups.filter(
                        type_user=CTGroupUser.TYPE_ADMINISTRATOR
                    ).values_list(u'ct_group__pk', flat=True):
                    raise PermissionDenied

        return obj

    def get_context_data(self, *args, **kwargs):
        context = super(ClinicalTrialDetailView, self).get_context_data(
            *args, **kwargs
        )
        context[u'workflow_transition_allowed'] = \
            self.object.check_all_steps_valid() \
            and self.object.holder == self.request.user \
            and self.object.get_workflow_state().pk == State.STATE_EM_PREENCHIMENTO \
            or True

        context[u'workflow_current_state'] = get_state(self.object)
        context[u'state_model'] = State
        self.object.current_ctdetail = self.object.ctdetail_set.first()
        context[u'ctdetails'] = self.object.ctdetail_set.all().order_by(u'language_code')

        #Do the control if the "clinical trial edit" step on the breadcrumbs
        #must appear.
        context[u'must_show_ct_edit_breadcrumb_link'] = \
            self.request.user.is_authenticated()
        if context[u'must_show_ct_edit_breadcrumb_link']:
            if self.object.get_workflow_state().pk not in (
                State.STATE_EM_PREENCHIMENTO,
                State.STATE_RESUBMETIDO,
                State.STATE_REVISANDO_PREENCHIMENTO,
                State.STATE_REVISAO_PREENCHIMENTO_RESUBMETIDO
            ) and self.request.user.is_registrant():
                context[u'must_show_ct_edit_breadcrumb_link'] = False

        try:
            #control to show the history link is the same of edit views
            edit_views_get_object(self, context[u'object'])
            can_view_history = True
        except PermissionDenied:
            can_view_history = False

        context[u'can_view_history'] = can_view_history
        return context


class ClinicalTrialDownloadView(View):

    def get_object(self, rbr):
        """
        Check the detail access permissions based on the user
        function
        """
        try:
            return ClinicalTrial.objects.using(u'published_cts').get(code=rbr)
        except ClinicalTrial.DoesNotExist:
            raise Http404

    def get(self, request, rbr):
        config = ExportConfig.objects.get(
                        active=True
                    )
        response = HttpResponse(
                    config.generate_xml(ct=self.get_object(rbr)),
                    content_type='application/xml'
                    )
        response['Content-Disposition'] = 'attachment; filename=%s_%s.xml' \
                                            % (config.file_name, rbr)
        return response

class PublicClinicalTrialDetailView(DetailView):
    model = ClinicalTrial

    def post(self, *args, **kwargs):
        return self.get(*args, **kwargs)

    def get_object(self):
        """
        Just return the object if the state is published
        """
        try:
            return ClinicalTrial.objects.using(u'published_cts').filter(
                code=self.kwargs[u'rbr']).latest(u'pk')
        except ClinicalTrial.DoesNotExist:
            raise Http404

    def get_context_data(self, *args, **kwargs):
        context = super(PublicClinicalTrialDetailView, self).get_context_data(
            *args, **kwargs
        )
        self.object.current_ctdetail = self.object.ctdetail_set.first()
        context[u'ctdetails'] = self.object.ctdetail_set.all()
        if self.object.get_workflow_state().pk == State.STATE_PUBLICADO \
           and self.object.user == self.request.user:
           context[u'workflow_transition_allowed'] = True
        context[u'publications_dates'] = StateObjectHistory.objects.filter(
                                             content_id=self.object.id,
                                             state_id=State.STATE_PUBLICADO
                                         ).order_by(
                                             u'update_date'
                                         ).values(
                                             u'update_date',
                                             u'pk'
                                         )
        context[u'has_date_finished'] = \
            self.object.study_final_date < timezone.now().date()

        return context

class ClinicalTrialEditListView(DetailView):
    """
    Shows the list of steps and steps status of a clinical trial.
    """
    model = ClinicalTrial
    template_name = u'clinical_trials/clinicaltrial_edit_detail.html'

    def get(self, *args, **kwargs):
        """
        If clinical trial is in waiting revision or waiting evaluation
        states, redirect to its detail page.
        """
        response = super(ClinicalTrialEditListView, self).get(*args, **kwargs)
        if self.object.get_workflow_state().pk in (
            State.STATE_AGUARDANDO_AVALIACAO,
            State.STATE_AGUARDANDO_REVISAO):
            return HttpResponseRedirect(
                    reverse(u'clinical_trial_detail', args=(self.object.pk,))
                )
        return response

    def get_object(self):
        obj = super(ClinicalTrialEditListView, self).get_object()
        return edit_views_get_object(self, obj)

    def get_context_data(self, *args, **kwargs):
        context = super(ClinicalTrialEditListView, self).get_context_data(
            *args, **kwargs
        )
        context[u'workflow_transition_allowed'] = True
        if self.object.get_workflow_state().pk == State.STATE_EM_PREENCHIMENTO:
            context[u'workflow_transition_allowed'] = self.object.check_all_steps_valid()

        context[u'workflow_current_state'] = get_state(self.object)
        context[u'state_model'] = State
        context[u'steps_with_changed_fields'] = self._get_steps_with_changed_fields()
        try:
            #control to show the history link is the same of edit views
            edit_views_get_object(self, context[u'object'])
            can_view_history = True
        except PermissionDenied:
            can_view_history = False
        context[u'can_view_history'] = can_view_history
        context[u'has_pending_observations'] = self._has_pending_observations()

        return context

    def _has_pending_observations(self):
        try:
            date_last_resubmited = StateObjectHistory.objects.filter(
                                       content_id=self.object.pk,
                                       state__pk=State.STATE_RESUBMETIDO
                                   ).latest('update_date').update_date
        except StateObjectHistory.DoesNotExist:
            return False
        pending_specific = StateObjectFieldObservation.objects.filter(
                               content_id=self.object.pk,
                               update_date__gte=date_last_resubmited
                           ).exists()
        pending_general = StateObjectGeneralObservation.objects.filter(
                              content_id=self.object.pk,
                              update_date__gte=date_last_resubmited
                          ).exists()
        return pending_specific and pending_general


    def _get_steps_with_changed_fields(self):
        return get_steps_with_changed_fields(self.object)


    def post(self, *args, **kwargs):
        self.object = self.get_object()
        if self.request.POST.get(u'complete_filling', u'no') == u'yes':
            return self.complete_filling()
        if self.request.POST.get(u'resubmit', u'no') == u'yes':
            return self.resubmit()

        if self.request.POST.get(u'publish', u'no') == u'yes':
            return self.publish()

        if self.request.POST.get(u'finish_evaluation', u'no') == u'yes':
            return self.finish_evaluation()
        if self.request.POST.get(u'finish_filling_revision', u'no') == u'yes':
            return self.finish_filling_revision()
        if self.request.POST.get(u'resend_to_revision', u'no') == u'yes':
            return self.resend_to_revision()
        if self.request.POST.get(
            u'finish_filling_revision_resubmited',
            u'no'
        ) == u'yes':
            return self.finish_filling_revision_resubmited()
        if self.request.POST.get(u'start_evaluation', u'no') == u'yes':
            return self.start_evaluation()
        if self.request.POST.get(u'start_revision', u'no') == u'yes':
            return self.start_revision()
        if self.request.POST.get(u'reopen_for_filling', u'no') == u'yes':
            return self.reopen_for_filling()

        return HttpResponseRedirect(
            reverse(
                u'clinical_trial_edit_list',
                args=(self.object.pk,)
            )
        )

    def reopen_for_filling(self):
        """
        Reopen a published clinical trial for filling again
        """
        state = get_state(self.object)
        if not state.pk == State.STATE_PUBLICADO:
            messages.error(
                    self.request,
                    _(u"You can only reopen published trials.")
                )
            return HttpResponseRedirect(
                reverse(u'clinical_trial_detail', args=(self.object.pk,))
            )
        do_transition(
          self.object,
            Transition.objects.get(
                pk=Transition.\
                   TRANSITION_REVISAR
            ),
            self.request.user
        )
        self.object.holder = self.request.user
        self.object.save()
        messages.success(
            self.request,
            _(u'Changed to filling state.')
        )
        return HttpResponseRedirect(
                reverse(u'clinical_trial_edit_list', args=(self.object.pk,))
            )

    def start_evaluation(self):
        """
        Start the evaluation
        """
        state = get_state(self.object)
        do_transition(
          self.object,
            Transition.objects.get(
                pk=Transition.\
                   TRANSITION_INICIAR_AVALIACAO
            ),
            self.request.user
        )
        messages.success(
            self.request,
            _(u'Changed to evaluation state.')
        )
        return HttpResponseRedirect(
                reverse(u'clinical_trial_edit_list', args=(self.object.pk,))
            )

    def start_revision(self):
        """
        Start the revision
        """
        state = get_state(self.object)
        do_transition(
          self.object,
            Transition.objects.get(
                pk=Transition.\
                   TRANSITION_INICIAR_REVISAO
            ),
            self.request.user
        )
        messages.success(
            self.request,
            _(u'Changed to revision state.')
        )
        return HttpResponseRedirect(
                reverse(u'clinical_trial_edit_list', args=(self.object.pk,))
            )

    def finish_filling_revision_resubmited(self):
        if self.object.check_all_steps_valid():
            state = get_state(self.object)
            do_transition(
                self.object,
                    Transition.objects.get(
                        pk=Transition.\
                           TRANSITION_ENVIAR_MODERACAO_RESUBMETIDO
                    ),
                    self.request.user
            )
            messages.success(
                    self.request,
                    _(u'Changed to revision state.')
                )
            return HttpResponseRedirect(
                reverse(u'home')
            )
        else:
            messages.error(self.request, _(u'This clinical trial is incomplete.'))
            return HttpResponseRedirect(
                reverse(u'clinical_trial_edit_list', args=(self.object.pk,))
            )

            """
            if self.object.sponsor_set.filter(
                  institution__status=u'M'
               ).exists():
                do_transition(
                  self.object,
                    Transition.objects.get(
                        pk=Transition.\
                           TRANSITION_ENVIAR_MODERACAO_RESUBMETIDO
                    ),
                    self.request.user
               )
                messages.success(
                    self.request,
                    _(u'Changed to moderation state.')
                )
            else:
                do_transition(
                    self.object,
                    Transition.objects.get(
                        pk=Transition.TRANSITION_REENVIAR_A_REVISAO
                    ),
                    self.request.user
                )
                messages.success(
                    self.request,
                    _(u'Changed to revision state.')
                )
            """

    def resend_to_revision(self):
        if self.object.check_all_steps_valid():
            if not Group.objects.get(name=u'reviser').user_set.all().exists():
                messages.error(self.request, _(u'The system has no Revisers Users Yet.'))
                return HttpResponseRedirect(
                    reverse(u'clinical_trial_edit_list', args=(self.object.pk,))
                )

            #if  self.object.check_has_wk_pending_observations_all_steps():
            #    messages.error(self.request, _(u'This trial still have unverified revision observations.'))
            #    return HttpResponseRedirect(
            #        reverse(u'clinical_trial_edit_list', args=(self.object.pk,))
            #    )

            state = get_state(self.object)

            if self.object.sponsor_set.filter(
                  institution__status=u'M'
               ).exists():
                do_transition(
                  self.object,
                    Transition.objects.get(
                        pk=Transition.\
                           TRANSITION_AVALIAR_PREENCHIMENTO_RESUBMETIDO
                    ),
                    self.request.user
               )
                messages.success(
                    self.request,
                    _(u'Changed to moderation state.')
                )
            else:
                do_transition(
                    self.object,
                    Transition.objects.get(
                        pk=Transition.TRANSITION_REENVIAR_A_REVISAO
                    ),
                    self.request.user
                )

                #send it back to the revisor that resubmitted it the last time
                self.object.holder = \
                    StateObjectHistory.objects.filter(
                        content_id=self.object.id
                    ).filter(
                            state_id=State.STATE_RESUBMETIDO
                        ).latest('update_date').user

                self.object.save()
                messages.success(
                    self.request,
                    _(u'Changed to revision state.')
                )
            return HttpResponseRedirect(
                reverse(u'home')
            )
        else:
            messages.error(self.request, _(u'This clinical trial is incomplete.'))
            return HttpResponseRedirect(
                reverse(u'clinical_trial_edit_list', args=(self.object.pk,))
            )


    def finish_filling_revision(self):
        """
        Change the state from STATE_REVISANDO_PREENCHIMENTO
        to moderation, waiting evaluation or waiting revision state.
        """
        if self.object.check_all_steps_valid():
            state = get_state(self.object)
            if self.object.sponsor_set.filter(
                  institution__status=u'M'
               ).exists():
                do_transition(
                  self.object,
                    Transition.objects.get(
                        pk=Transition.TRANSITION_ENVIAR_MODERACAO
                    ),
                    self.request.user
               )
                messages.success(
                    self.request,
                    _(u'Changed to moderation state.')
                )
            elif Group.objects.get(
                     name=u'evaluator'
                 ).user_set.all().exists():
                do_transition(
                    self.object,
                    Transition.objects.get(
                        pk=Transition.TRANSITION_CONCLUIR_REVISAO_AVALIACAO
                    ),
                    self.request.user
                )
                self.object.holder = get_next_by_occupation(u'evaluator')
                self.object.save()
                messages.success(
                    self.request,
                    _(u'Changed to evaluation state.')
                )
            else:
                if not Group.objects.get(name=u'reviser').user_set.all().exists():
                    messages.error(self.request, _(u'The system has no Revisers Users Yet.'))
                    return HttpResponseRedirect(
                        reverse(u'clinical_trial_edit_list', args=(self.object.pk,))
                    )
                do_transition(
                    self.object,
                    Transition.objects.get(
                        pk=Transition.TRANSITION_CONCLUIR_REVISAO_REVISAO
                    ),
                    self.request.user
                )
                self.object.holder = get_next_by_occupation(u'reviser')
                self.object.save()
                messages.success(
                    self.request,
                    _(u'Changed to revision state.')
                )

            return HttpResponseRedirect(
               reverse(u'home')
            )

        else:
            messages.error(self.request, _(u'This clinical trial is incomplete.'))
            return HttpResponseRedirect(
                reverse(u'clinical_trial_edit_list', args=(self.object.pk,))
            )

    def finish_evaluation(self):
        """
        Set to waiting revision state and change the holder
        to the result of balance distribution function result
        """
        if not Group.objects.get(name=u'reviser').user_set.all().exists():
            messages.error(self.request, _(u'The system has no Revisers Users Yet.'))
            return HttpResponseRedirect(
                reverse(u'clinical_trial_edit_list', args=(self.object.pk,))
            )
        do_transition(
            self.object,
            Transition.objects.get(
                pk=Transition.TRANSITION_CONCLUIR_AVALIACAO
            ),
            self.request.user
        )
        self.object.holder = get_next_by_occupation(u'reviser')
        self.object.save()
        messages.success(
            self.request,
            _(u'Clinical Trial Changed to Revision State')
        )
        return HttpResponseRedirect(reverse(u'home'))


    def resubmit(self):
        """
        Set to resubmit state and change the holder to the
        registrant user.
        """
        if  not self.object.check_has_wk_pending_observations_all_steps():
            if not self.object.check_has_wk_pending_general_observations():
                messages.error(self.request, _(u'This trial has no revision messages.'))
                return HttpResponseRedirect(
                    reverse(u'clinical_trial_edit_list', args=(self.object.pk,))
                )

        do_transition(
            self.object,
            Transition.objects.get(
                pk=Transition.TRANSITION_RESUBMETER
            ),
            self.request.user
        )
        self.object.holder = self.object.user
        self.object.save()
        messages.success(
            self.request,
            _(u'Clinical Trial Resubmitted to Registrant')
        )
        return HttpResponseRedirect(reverse(u'home'))


    def publish(self):

        do_transition(
            self.object,
            Transition.objects.get(
                pk=Transition.TRANSITION_CONCLUIR_REVISAO
            ),
            self.request.user
        )
        #generate de RBR code, if doesn't have one yet
        new_code_generated = False
        if self.object.code is None:
            self.object.code = generate_trial_id(u'RBR', 6)
            new_code_generated = True
        self.object.holder = self.object.user
        self.object.save()

        # save the clinical trial data on the historical database
        # overriding possible trial data that may exist there
        #from IPython import embed;embed()
        self.object.save_historical_data()

        last_history = StateObjectHistory.objects.filter(
                           content_id=self.object.pk).latest(u'update_date')        

        public_detail_content = PublicClinicalTrialDetailView.as_view()(
            self.request, rbr=self.object.code).rendered_content
        content = html.fromstring(public_detail_content).cssselect(
            u'#main-content')[0]
        last_history.trial_snapshot = etree.tostring(content)
        last_history.save()

        if new_code_generated:
            messages.success(
                self.request,
                _(u'Clinical trial published, generated code ') + self.object.code
            )
        else:
            messages.success(
                self.request,
                _(u'Clinical trial published again.')
            )
        return HttpResponseRedirect(reverse(u'home'))


    def complete_filling(self):
        if self.object.check_all_steps_valid():
                state = get_state(self.object)
                if self.object.sponsor_set.filter(
                      institution__status=u'M'
                   ).exists():
                    do_transition(
                      self.object,
                        Transition.objects.get(
                            pk=Transition.TRANSITION_AVALIAR_PREENCHIMENTO
                        ),
                        self.request.user
                   )
                    messages.success(
                        self.request,
                        _(u'Changed to moderation state.')
                    )
                elif Group.objects.get(
                         name=u'evaluator'
                     ).user_set.all().exists() and not self.object.date_last_publication:
                    do_transition(
                        self.object,
                        Transition.objects.get(
                            pk=Transition.TRANSITION_AGUARDAR_AVALIACAO
                        ),
                        self.request.user
                    )
                    self.object.holder = get_next_by_occupation(u'evaluator')
                    self.object.save()
                    messages.success(
                        self.request,
                        _(u'Changed to evaluation state.')
                    )
                else:
                    if not Group.objects.get(name=u'reviser').user_set.all().exists():
                        messages.error(self.request, _(u'The system has no Revisers Users Yet.'))
                        return HttpResponseRedirect(
                            reverse(u'clinical_trial_edit_list', args=(self.object.pk,))
                        )
                    do_transition(
                        self.object,
                        Transition.objects.get(
                            pk=Transition.TRANSITION_AGUARDAR_REVISAO
                        ),
                        self.request.user
                    )
                    if self.object.date_last_publication:
                        #send it back to the revisor that published it last time
                        self.object.holder = \
                            StateObjectHistory.objects.filter(
                                content_id=self.object.id
                            ).filter(
                                    state_id=State.STATE_PUBLICADO
                                ).latest('update_date').user
                    else:
                       self.object.holder = get_next_by_occupation(u'reviser')
                    self.object.save()
                    messages.success(
                        self.request,
                        _(u'Changed to revision state.')
                    )


                if self.object.group:
                    for i in self.object.group.users.filter(type_user='a'):
                        accept_in_group_link = reverse(
                                                   'clinical_trial_accept_in_group',
                                                   args=(self.object.pk,)
                                               )
                        email_text = render_to_string(
                                         u'clinical_trials/email_to_approve_trial.html',
                                         {u'link': accept_in_group_link,
                                          u'site_url': settings.SITE_URL})

                        send_mail(
                            _(u'Approve of clinical trial'),
                            email_text,
                            None,
                            [i.user.email])                

                return HttpResponseRedirect(
                    reverse(u'home')
                )

        else:
            messages.error(self.request, _(u'This clinical trial is incomplete.'))
            return HttpResponseRedirect(
                reverse(u'clinical_trial_edit_list', args=(self.object.pk,))
            )

class ClinicalTrialDeleteView(DeleteView):
    """
    View to delete a clinical trial.
    """
    model = ClinicalTrial
    success_url = reverse_lazy(u'home')

    def get_object(self):
        obj = super(ClinicalTrialDeleteView, self).get_object()
        if not obj.holder == self.request.user:
            raise PermissionDenied
        if not obj.can_be_deleted():
            raise PermissionDenied
        return obj




class CTDetailIdentificationInlineFormSet(InlineFormSet):
    """
    "Identeificaton" step of a clinical trial view.
    """
    model = CTDetail
    formset_class = CTDetailIdentificationFormSet

    def get_formset(self):
        """
        Returns the formset class to be used
        """
        return self.formset_class

    def get_formset_kwargs(self, *args, **kwargs):
        """
        Delete some kwargs that the formset isn't expecting(for some reason).
        """
        ret = super(
                CTDetailIdentificationInlineFormSet,
                self).get_formset_kwargs(*args, **kwargs)
        ret[u'queryset'] = self.object.ctdetail_set.all()
        if ret.has_key(u'save_as_new'):
            ret.pop(u'save_as_new')
        if ret.has_key(u'instance'):
            ret.pop(u'instance')
        return ret

class ClinicalTrialIdentifiersInlineFormSet(InlineFormSet):
    """
    InlineFormset(options) for use of the UpdateWithInlinesView
    """
    model = Identifier
    form_class = IdentifierForm
    def __init__(self, *args, **kwargs):
        super(ClinicalTrialIdentifiersInlineFormSet, self).__init__(
            *args, **kwargs
        )
        self.extra_initial_kwargs = []
        instance_identifiers = self.object.identifier_set.all().values_list(
            u'setup_identifier__pk', flat=True)

        for i in SetupIdentifier.objects.filter(
                     appear_in_identifications_step=True
                 ).values_list('pk', flat=True):
            if i not in instance_identifiers:
                self.extra_initial_kwargs.append({u'setup_identifier': i})

        self.extra = len(self.extra_initial_kwargs)

    def get_formset_kwargs(self):
        """
        Set initial data to all the required identifiers that aren't in the
        instance yet.
        """
        kwargs = super(
            ClinicalTrialIdentifiersInlineFormSet,
            self).get_formset_kwargs()

        kwargs[u'initial'] = self.extra_initial_kwargs
        return kwargs


class ClinicalTrialIdentificationView(NamedFormsetsMixin,
                                      UpdateWithInlinesView):
    model = ClinicalTrial
    form_class = ClinicalTrialIdentificationForm
    inlines = [CTDetailIdentificationInlineFormSet,
               ClinicalTrialIdentifiersInlineFormSet]
    inlines_names = [u'identifications_formset', u'identifiers_formset']
    template_name = \
            u'clinical_trials/clinical_trial_detail_identification_form.html'

    def get(self, *args, **kwargs):
        """
        If clinical trial is in waiting revision or waiting evaluation
        states, redirect to its detail page.
        """
        response = super(ClinicalTrialIdentificationView, self).get(*args, **kwargs)
        if self.object.get_workflow_state().pk in (
            State.STATE_AGUARDANDO_AVALIACAO,
            State.STATE_AGUARDANDO_REVISAO):
            return HttpResponseRedirect(
                    reverse(u'clinical_trial_detail', args=(self.object.pk,))
                )
        return response


    def post(self, request, pk):
        study_type = request.POST.get(u'study_type', None)
        self.study_type_changed = \
            study_type and study_type <> ClinicalTrial.objects.get(pk=pk).study_type
        return super(ClinicalTrialIdentificationView, self).post(request, pk)


    def get_object(self):
        obj = super(ClinicalTrialIdentificationView, self).get_object()
        return edit_views_get_object(self, obj)

    def get_success_url(self):
        """
        Set a success message and return th url to redirect user to.
        """
        #if the study type was changed, then delete all the interventions associated
        #and the interventions observations too
        if self.study_type_changed:
            self.object.interventions.all().delete()
            self.object.intervention_observations.all().delete()
            self.object.followup_period = None
            self.object.reference_period = None

        messages.success(self.request, _(u'Identification saved.'))
        return reverse(u'clinical_trial_edit_list', args=(self.kwargs[u'pk'],))

    def get_context_data(self, *args, **kwargs):
        context = super(
            ClinicalTrialIdentificationView,
            self
        ).get_context_data(*args, **kwargs)
        context[u'form_style'] = u'default'
        if self.object.get_workflow_state().pk in (
            State.STATE_EM_REVISAO,
            State.STATE_EM_AVALIACAO):
            context[u'form_style'] = u'observations'
            disable_all_formset_fields(context[u'identifications_formset'])
            disable_all_formset_fields(context[u'identifiers_formset'])
            disable_all_form_fields(context[u'form'])
        context[u'identifications_formset'].content_type_id = \
            ContentType.objects.get_for_model(CTDetail).pk
        context[u'identifiers_formset'].content_type_id = \
            ContentType.objects.get_for_model(Identifier).pk
        #send to context all the help_text of all identifiers
        #Try to ghet for the correct language, if cant match, use the lang. 1
        setup = Setup.objects.get()
        get_value = u'help_text_lang_1'
        get_value_code = u'help_text_code_lang_1'
        get_value_description = u'help_text_description_lang_1'
        get_value_date = u'help_text_date_lang_1'
        if setup.language_2_code_id and \
            setup.language_2_code.code == self.request.LANGUAGE_CODE:
            get_value = u'help_text_lang_2'
            get_value_code = u'help_text_code_lang_2'
            get_value_description = u'help_text_description_lang_2'
            get_value_date = u'help_text_date_lang_2'
        if setup.language_3_code_id and \
            setup.language_3_code.code == self.request.LANGUAGE_CODE:
            get_value = u'help_text_lang_3'
            get_value_code = u'help_text_code_lang_3'
            get_value_description = u'help_text_description_lang_3'
            get_value_date = u'help_text_date_lang_3'

        context[u'identifiers_help_text'] = \
            json.dumps(
                {i[u'pk']: {u'identifier': i[get_value],
                            u'code': i[get_value_code],
                            u'description': i[get_value_description],
                            u'date': i[get_value_date]} \
                    for i in SetupIdentifier.objects.all().values(
                                                        u'pk',
                                                        get_value,
                                                        get_value_code,
                                                        get_value_description,
                                                        get_value_date)
                }
            )
        context[u'workflow_current_state'] = get_state(self.object)
        context[u'state_model'] = State
        context[u'clinical_trial_content_type_id'] = \
            ContentType.objects.get_for_model(ClinicalTrial).pk

        return context


class ClinicalTrialSponsorsInlineFormSet(InlineFormSet):
    """
    InlineFormset(options) for use of the UpdateWithInlinesView
    """
    model = Sponsor
    form_class = ClinicalTrialSponsorForm
    extra = 0


class ClinicalTrialSponsorsView(NamedFormsetsMixin, UpdateWithInlinesView):
    model = ClinicalTrial
    fields = []
    inlines = [ClinicalTrialSponsorsInlineFormSet,]
    inlines_names = [u'sponsors_formset',]
    template_name = u'clinical_trials/clinical_trial_detail_sponsors_form.html'

    def get(self, *args, **kwargs):
        """
        If clinical trial is in waiting revision or waiting evaluation
        states, redirect to its detail page.
        """
        response = super(ClinicalTrialSponsorsView, self).get(*args, **kwargs)
        if self.object.get_workflow_state().pk in (
            State.STATE_AGUARDANDO_AVALIACAO,
            State.STATE_AGUARDANDO_REVISAO):
            return HttpResponseRedirect(
                    reverse(u'clinical_trial_detail', args=(self.object.pk,))
                )
        return response

    def get_object(self):
        obj = super(ClinicalTrialSponsorsView, self).get_object()
        return edit_views_get_object(self, obj)

    def get_success_url(self):
        """
        Set a success message and return th url to redirect user to.
        """
        messages.success(self.request, _(u'Sponsors saved.'))
        return reverse(u'clinical_trial_edit_list', args=(self.kwargs[u'pk'],))

    def get_context_data(self, *args, **kwargs):
        context = super(
            ClinicalTrialSponsorsView,
            self
        ).get_context_data(*args, **kwargs)
        context[u'form_style'] = u'default'
        if self.object.get_workflow_state().pk in (
            State.STATE_EM_REVISAO,
            State.STATE_EM_AVALIACAO):
            context[u'form_style'] = u'observations'
            disable_all_formset_fields(context[u'sponsors_formset'])
        context[u'sponsor_content_type_id'] = \
            ContentType.objects.get_for_model(Sponsor).pk
        context[u'workflow_current_state'] = get_state(self.object)
        context[u'state_model'] = State
        context[u'changed_fields'] = get_changed_fields(self.object)
        return context


class CTDetailHealthConditionInlineFormSet(InlineFormSet):
    model = CTDetail
    formset_class = CTDetailHealthConditionFormSet

    def get_formset(self):
        """
        Returns the formset class from the formset factory
        """
        return self.formset_class

    def get_formset_kwargs(self, *args, **kwargs):
        """
        Delete some kwargs that the formset isn't expecting(for some reason).
        """
        ret = super(
                CTDetailHealthConditionInlineFormSet,
                self).get_formset_kwargs(*args, **kwargs)
        ret[u'queryset'] = self.object.ctdetail_set.all()
        if ret.has_key(u'save_as_new'):
            ret.pop(u'save_as_new')
        if ret.has_key(u'instance'):
            ret.pop(u'instance')
        return ret

class ClinicalTrialDescriptorsInlineFormSet(InlineFormSet):
    """
    InlineFormset(options) for use of the UpdateWithInlinesView
    """
    model = Descriptor
    form_class = HealthConditionsDescriptorForm
    extra = 0

    def get_formset_kwargs(self, *args, **kwargs):
        ret = super(
                ClinicalTrialDescriptorsInlineFormSet,
                self).get_formset_kwargs(*args, **kwargs)
        ret[u'queryset'] = self.object.descriptor_set.exclude(descriptor_type=u'I')

        return ret

    def get_formset(self):
        formset = super(ClinicalTrialDescriptorsInlineFormSet,
                        self).get_formset()
        def formset_clean(self):
            """
            Formset clean method, to replace for the original one.
            """
            if not hasattr(self, u'cleaned_data'):
                return None

            has_general = filter(
                lambda x: x.get(u'descriptor_type', None) == 'G' \
                          and not x.get(u'DELETE'), self.cleaned_data
            )
            has_specific = filter(
                lambda x: x.get(u'descriptor_type', None) == 'S' \
                          and not x.get(u'DELETE'), self.cleaned_data
            )
            if any(has_general) and any(has_specific):
               return self.cleaned_data

            raise forms.ValidationError(_(u'The trial must have at least one '
                                           u'general and one specific '
                                           u'descriptor'))
        formset.clean = formset_clean
        return formset

class ClinicalTrialHealthConditionView(NamedFormsetsMixin,
                                       UpdateWithInlinesView):
    """
    View of the "Health Conditions" step.
    """
    model = ClinicalTrial
    fields = []
    inlines = [CTDetailHealthConditionInlineFormSet,
               ClinicalTrialDescriptorsInlineFormSet]
    inlines_names = [u'health_conditions_formset', u'descriptors_formset']
    template_name = \
            u'clinical_trials/clinical_trial_detail_health_condition_form.html'

    def get(self, *args, **kwargs):
        """
        If clinical trial is in waiting revision or waiting evaluation
        states, redirect to its detail page.
        """
        response = super(ClinicalTrialHealthConditionView, self).get(*args, **kwargs)
        if self.object.get_workflow_state().pk in (
            State.STATE_AGUARDANDO_AVALIACAO,
            State.STATE_AGUARDANDO_REVISAO):
            return HttpResponseRedirect(
                    reverse(u'clinical_trial_detail', args=(self.object.pk,))
                )
        return response

    def get_object(self):
        obj = super(ClinicalTrialHealthConditionView, self).get_object()
        return edit_views_get_object(self, obj)

    def get_success_url(self):
        """
        Set a success message and return th url to redirect user to.
        """
        messages.success(self.request, _(u'Health Conditions saved.'))
        return reverse(u'clinical_trial_edit_list', args=(self.kwargs[u'pk'],))

    def get_context_data(self, *args, **kwargs):
        context = super(
            ClinicalTrialHealthConditionView,
            self
        ).get_context_data(*args, **kwargs)
        context[u'form_style'] = u'default'
        if self.object.get_workflow_state().pk in (
            State.STATE_EM_REVISAO,
            State.STATE_EM_AVALIACAO):
            context[u'form_style'] = u'observations'
            disable_all_formset_fields(context[u'health_conditions_formset'])
            disable_all_formset_fields(context[u'descriptors_formset'])
        context[u'ctdetail_content_type_id'] = \
            ContentType.objects.get_for_model(CTDetail).pk
        context[u'descriptor_content_type_id'] = \
            ContentType.objects.get_for_model(Descriptor).pk
        context[u'workflow_current_state'] = get_state(self.object)
        context[u'state_model'] = State
        context[u'language'] = translation.get_language()
        return context


class ClinicalTrialDescriptorsInlineFormSet2(InlineFormSet):
    """
    InlineFormset(options) for use of the UpdateWithInlinesView
    """
    model = Descriptor
    form_class = InterventionsDescriptorForm

    def get_formset_kwargs(self, *args, **kwargs):
        ret = super(
                ClinicalTrialDescriptorsInlineFormSet2,
                self).get_formset_kwargs(*args, **kwargs)
        ret[u'queryset'] = self.object.descriptor_set.filter(descriptor_type=u'I')
        return ret

class CTInterventionsInlineFormSet(InlineFormSet):
    model = CTIntervention
    extra = 1

    def get_formset_kwargs(self, *args, **kwargs):
        """
        Delete some kwargs that the formset isn't expecting(for some reason).
        """
        ret = super(
                CTInterventionsInlineFormSet,
                self).get_formset_kwargs(*args, **kwargs)
        ret[u'queryset'] = self.object.interventions.all()

        return ret


class CTInterventionObservationFormSet(InlineFormSet):
    model = CTInterventionObservation
    extra = 1

    def get_formset_kwargs(self, *args, **kwargs):
        """
        Change the queryset of the formset.
        """
        ret = super(
                CTInterventionObservationFormSet,
                self).get_formset_kwargs(*args, **kwargs)
        ret[u'queryset'] = self.object.intervention_observations.all()
        return ret

    def get_factory_kwargs(self, *args, **kwargs):
        """
        Change the queryset of the formset.
        """
        ret = super(
                CTInterventionObservationFormSet,
                self).get_factory_kwargs(*args, **kwargs)

        ret[u'extra'] = 0 if \
            self.object.intervention_observations.all().count() else 1
        return ret

class ClinicalTrialInterventionView(NamedFormsetsMixin,
                                    UpdateWithInlinesView):
    """
    View of the "Health Conditions" step.
    """
    model = ClinicalTrial
    form_class = ClinicalTrialInterventionAssignmentsForm
    inlines = [CTInterventionsInlineFormSet,
               CTInterventionObservationFormSet]
    inlines_names = [u'interventions_formset',
                     u'observations_formset']
    template_name = \
            u'clinical_trials/clinical_trial_detail_intervention_form.html'

    def get(self, *args, **kwargs):
        """
        If clinical trial is in waiting revision or waiting evaluation
        states, redirect to its detail page.
        """
        response = super(ClinicalTrialInterventionView, self).get(*args, **kwargs)
        if self.object.get_workflow_state().pk in (
            State.STATE_AGUARDANDO_AVALIACAO,
            State.STATE_AGUARDANDO_REVISAO):
            return HttpResponseRedirect(
                    reverse(u'clinical_trial_detail', args=(self.object.pk,))
                )
        return response

    def get_object(self):
        obj = super(ClinicalTrialInterventionView, self).get_object()
        return edit_views_get_object(self, obj)

    def get_success_url(self):
        """
        Set a success message and return th url to redirect user to.
        """
        step_name = _(u'Interventions') if self.object.study_type == u'I' \
                    else u'Observations'
        messages.success(self.request, _(u'%s saved.' % step_name))
        return reverse(u'clinical_trial_edit_list', args=(self.kwargs[u'pk'],))

    def get_context_data(self, *args, **kwargs):
        context = super(
            ClinicalTrialInterventionView,
            self
        ).get_context_data(*args, **kwargs)
        context[u'form_style'] = u'default'
        if self.object.get_workflow_state().pk in (
            State.STATE_EM_REVISAO,
            State.STATE_EM_AVALIACAO):
            context[u'form_style'] = u'observations'
            disable_all_formset_fields(context[u'interventions_formset'])
            disable_all_formset_fields(context[u'observations_formset'])
            disable_all_form_fields(context[u'form'])
        context[u'interventions_formset'].content_type_id = \
            ContentType.objects.get_for_model(CTIntervention).pk
        context[u'observations_formset'].content_type_id = \
            ContentType.objects.get_for_model(CTInterventionObservation).pk
        context[u'clinical_trial_content_type_id'] = \
            ContentType.objects.get_for_model(ClinicalTrial).pk
        context[u'workflow_current_state'] = get_state(self.object)
        context[u'state_model'] = State
        return context


class CTDetailRecruitmentInlineFormSet(InlineFormSet):
    """
    InlineFormset(options) for use of the UpdateWithInlinesView
    """
    model = CTDetail
    formset_class = CTDetailRecruitmentFormSet

    def get_formset(self):
        """
        Returns the formset class from the formset factory
        """
        return self.formset_class

    def get_formset_kwargs(self, *args, **kwargs):
        """
        Delete some kwargs that the formset isn't expecting(for some reason).
        """
        ret = super(
                CTDetailRecruitmentInlineFormSet,
                self).get_formset_kwargs(*args, **kwargs)
        ret[u'queryset'] = self.object.ctdetail_set.all()
        if ret.has_key(u'save_as_new'):
            ret.pop(u'save_as_new')
        if ret.has_key(u'instance'):
            ret.pop(u'instance')
        return ret


class ClinicalTrialRecruitmentView(NamedFormsetsMixin,
                                   UpdateWithInlinesView):
    """
    View of the "Recruitment" step.
    """
    model = ClinicalTrial
    form_class = ClinicalRecruitmentStepForm
    inlines = [CTDetailRecruitmentInlineFormSet,]
    inlines_names = [u'recruitment_formset',]
    template_name = \
            u'clinical_trials/clinical_trial_detail_recruitment_form.html'

    def get_form_kwargs(self, *args, **kwargs):
        kw = super(ClinicalTrialRecruitmentView, self).get_form_kwargs(
            *args, **kwargs)
        kw.update({u'request': self.request})
        #kw.update({'initial': {'minimum_age_no_limit': 'True',
        #                       'maximum_age_no_limit': 'True'}})
        return kw

    def get(self, *args, **kwargs):
        """
        If clinical trial is in waiting revision or waiting evaluation
        states, redirect to its detail page.
        """
        response = super(ClinicalTrialRecruitmentView, self).get(*args, **kwargs)
        if self.object.get_workflow_state().pk in (
            State.STATE_AGUARDANDO_AVALIACAO,
            State.STATE_AGUARDANDO_REVISAO):
            return HttpResponseRedirect(
                    reverse(u'clinical_trial_detail', args=(self.object.pk,))
                )
        return response

    def get_object(self):
        obj = super(ClinicalTrialRecruitmentView, self).get_object()
        return edit_views_get_object(self, obj)

    def get_success_url(self):
        """
        Set a success message and return th url to redirect user to.
        """
        messages.success(self.request, _(u'Recruitment saved.'))
        return reverse(u'clinical_trial_edit_list', args=(self.kwargs[u'pk'],))

    def get_context_data(self, *args, **kwargs):
        context = super(
            ClinicalTrialRecruitmentView,
            self
        ).get_context_data(*args, **kwargs)
        context[u'form_style'] = u'default'
        if self.object.get_workflow_state().pk in (
            State.STATE_EM_REVISAO,
            State.STATE_EM_AVALIACAO):
            context[u'form_style'] = u'observations'
            disable_all_formset_fields(context[u'recruitment_formset'])
            disable_all_form_fields(context[u'form'])
        context[u'recruitment_formset'].content_type_id = \
            ContentType.objects.get_for_model(CTDetail).pk
        context[u'clinical_trial_content_type_id'] = \
            ContentType.objects.get_for_model(ClinicalTrial).pk
        context[u'workflow_current_state'] = get_state(self.object)
        context[u'state_model'] = State
        return context


class ClinicalTrialStudyTypeView(NamedFormsetsMixin,
                                 UpdateWithInlinesView):
    """
    View of the "Study Type" step.
    """
    model = ClinicalTrial
    form_class = ClinicalStudyTypeStepForm
    template_name = \
            u'clinical_trials/clinical_trial_detail_study_type_form.html'

    def get(self, *args, **kwargs):
        """
        If clinical trial is in waiting revision or waiting evaluation
        states, redirect to its detail page.
        """
        response = super(ClinicalTrialStudyTypeView, self).get(*args, **kwargs)
        if self.object.get_workflow_state().pk in (
            State.STATE_AGUARDANDO_AVALIACAO,
            State.STATE_AGUARDANDO_REVISAO):
            return HttpResponseRedirect(
                    reverse(u'clinical_trial_detail', args=(self.object.pk,))
                )
        return response

    def get_object(self):
        obj = super(ClinicalTrialStudyTypeView, self).get_object()
        return edit_views_get_object(self, obj)

    def get_success_url(self):
        """
        Set a success message and return th url to redirect user to.
        """
        messages.success(self.request, _(u'Study Design saved.'))
        return reverse(u'clinical_trial_edit_list', args=(self.kwargs[u'pk'],))

    def get_context_data(self, *args, **kwargs):
        context = super(
            ClinicalTrialStudyTypeView,
            self
        ).get_context_data(*args, **kwargs)
        context[u'form_style'] = u'default'
        if self.object.get_workflow_state().pk in (
            State.STATE_EM_REVISAO,
            State.STATE_EM_AVALIACAO):
            context[u'form_style'] = u'observations'
            #disable_all_formset_fields(context[u'study_type_formset'])
            disable_all_form_fields(context[u'form'])
        context[u'clinical_trial_content_type_id'] = \
            ContentType.objects.get_for_model(ClinicalTrial).pk
        context[u'workflow_current_state'] = get_state(self.object)
        context[u'state_model'] = State
        context[u'followup_period_and_reference_period'] = \
            [u'followup_period', u'reference_period']
        return context


class ClinicalTrialOutcomesInlineFormSet(InlineFormSet):
    """
    InlineFormset(options) for use of the UpdateWithInlinesView
    """
    model = Outcome
    extra=0

    def get_form_class(self):

        _OutcomeForm = modelform_factory(Outcome)
        class OutcomeForm(_OutcomeForm):
            required_fields = (u'name', u'measurement_method',
                               u'point_of_time_of_interest')
            def __init__(self, data=None, *args, **kwargs):
                super(OutcomeForm, self).__init__(data, *args, **kwargs)
                for i in self.fields:
                    self.fields[i].required = False


            def clean(self):
                """
                Secondary outomes must only be required if the user typed at
                least one value(not including the outcome_type and the clinical
                trial foreign key).
                """

                #Check if the outcome_type is "Secondary"("S").
                if self.cleaned_data.get(u'outcome_type', None) == u"S":
                    #Check if any other field was filled,
                    #or just the outcome_type.
                    if not self.cleaned_data.get(u'found_outcome', None) and \
                       not self.cleaned_data.get(u'measurement_method', None) \
                       and not self.cleaned_data.get(u'name', None) and \
                       not self.cleaned_data.get(
                            u'point_of_time_of_interest', None) and \
                       not self.cleaned_data.get(u'id', None):
                       return {}

                
                #This outcome must have the required fields validated
                for i in self.required_fields:
                    if not self.cleaned_data.get(i, None):
                        self.errors[i] = [_(u'This field is required.')]

                return self.cleaned_data
                
                
        return OutcomeForm

    def get_formset_class(self):
        _OutcomeFormSet = super(ClinicalTrialOutcomesInlineFormSet, self).get_formset_class()
        class OutcomeFormSet(_OutcomeFormSet):
            def clean(self):
                """
                A clinical trial can't have more than one primary outcome.
                """
                if not hasattr(self, u'cleaned_data'):
                    return None

                if len([i[u'outcome_type'] \
                        for i in self.cleaned_data \
                            if i.get(u'outcome_type', None) == u'P']) > 1:
                    raise forms.ValidationError(
                            _(u'You must have just one Primary Outcome.'))
                return self.cleaned_data

        return OutcomeFormSet
        
        

class ClinicalTrialOutcomesView(NamedFormsetsMixin,
                                UpdateWithInlinesView):
    """
    View of the "Outcomes" step.
    """
    model = ClinicalTrial
    fields = []
    inlines = [ClinicalTrialOutcomesInlineFormSet,]
    inlines_names = [u'outcomes_formset',]
    template_name = \
            u'clinical_trials/clinical_trial_detail_outcomes_form.html'

    def get(self, *args, **kwargs):
        """
        If clinical trial is in waiting revision or waiting evaluation
        states, redirect to its detail page.
        """
        response = super(ClinicalTrialOutcomesView, self).get(*args, **kwargs)
        if self.object.get_workflow_state().pk in (
            State.STATE_AGUARDANDO_AVALIACAO,
            State.STATE_AGUARDANDO_REVISAO):
            return HttpResponseRedirect(
                    reverse(u'clinical_trial_detail', args=(self.object.pk,))
                )
        return response

    def get_object(self):
        obj = super(ClinicalTrialOutcomesView, self).get_object()
        return edit_views_get_object(self, obj)

    def get_success_url(self):
        """
        Set a success message and return th url to redirect user to.
        """
        #delete all the outcomes of this clinical trial without outcome_type
        #value(in the case of secondary outcomes with incomplete filling, the
        #clean() method returns an empty dict).
        ct = ClinicalTrial.objects.get(pk=self.kwargs.get(u'pk'))
        ct.outcome_set.filter(outcome_type='').delete()
        ct.outcome_set.filter(outcome_type__isnull=True).delete()

        messages.success(self.request, _(u'Outcomes saved.'))

        return reverse(u'clinical_trial_edit_list', args=(self.kwargs[u'pk'],))


    def get_context_data(self, *args, **kwargs):
        context = super(
            ClinicalTrialOutcomesView,
            self
        ).get_context_data(*args, **kwargs)
        context[u'form_style'] = u'default'
        if self.object.get_workflow_state().pk in (
            State.STATE_EM_REVISAO,
            State.STATE_EM_AVALIACAO):
            context[u'form_style'] = u'observations'
            disable_all_formset_fields(context[u'outcomes_formset'])
            disable_all_form_fields(context[u'form'])
        context[u'outcomes_formset'].content_type_id = \
            ContentType.objects.get_for_model(Outcome).pk
        context[u'workflow_current_state'] = get_state(self.object)
        context[u'state_model'] = State
        context[u'is_get_request'] = self.request.method == u'GET'
        return context


class ClinicalTrialContactsInlineFormSet(InlineFormSet):
    """
    InlineFormset(options) for use of the UpdateWithInlinesView
    """
    model = CTContact
    form_class = CTContactForm
    extra = 0

    def get_extra_form_kwargs(self, *args, **kwargs):
        from django.core.urlresolvers import resolve
        return {u'ct_id': resolve(self.request.path).kwargs[u'pk']}

class ClinicalTrialContactsView(NamedFormsetsMixin, UpdateWithInlinesView):
    model = ClinicalTrial
    fields = []
    inlines = [ClinicalTrialContactsInlineFormSet,]
    inlines_names = [u'contacts_formset',]
    template_name = u'clinical_trials/clinical_trial_detail_contacts_form.html'

    def get(self, *args, **kwargs):
        """
        If clinical trial is in waiting revision or waiting evaluation
        states, redirect to its detail page.
        """
        response = super(ClinicalTrialContactsView, self).get(*args, **kwargs)
        if self.object.get_workflow_state().pk in (
            State.STATE_AGUARDANDO_AVALIACAO,
            State.STATE_AGUARDANDO_REVISAO):
            return HttpResponseRedirect(
                    reverse(u'clinical_trial_detail', args=(self.object.pk,))
                )
        return response

    def get_object(self):
        obj = super(ClinicalTrialContactsView, self).get_object()
        return edit_views_get_object(self, obj)

    def get_success_url(self):
        """
        Set a success message and return th url to redirect user to.
        """
        #delete all the orphan contacts
        Contact.delete_orphan_contacts()
        messages.success(self.request, _(u'Contacts saved.'))
        return reverse(u'clinical_trial_edit_list', args=(self.kwargs[u'pk'],))

    def get_context_data(self, *args, **kwargs):
        context = super(
            ClinicalTrialContactsView,
            self
        ).get_context_data(*args, **kwargs)
        context[u'form_style'] = u'default'
        if self.object.get_workflow_state().pk in (
            State.STATE_EM_REVISAO,
            State.STATE_EM_AVALIACAO):
            context[u'form_style'] = u'observations'
            disable_all_formset_fields(context[u'contacts_formset'])
        context[u'contacts_formset'].content_type_id = \
            ContentType.objects.get_for_model(CTContact).pk
        context[u'workflow_current_state'] = get_state(self.object)
        context[u'state_model'] = State
        return context


class ClinicalTrialCreateContact(CreateView):
    """
    Register Contact and add as a Clinical Trial Contact
    """
    model = Contact
    #fields = [field.name for field in Institution._meta.fields if field.name != 'status']
    form_class = ContactForm2
    template_name = 'clinical_trials/clinical_trial_create_contact.html'

    def get_context_data(self, *args, **kwargs):
        """
        Return a message to confirm to add Contact as Clinical Trial
        Contact and return Clinical Trial pk to view.
        """
        context = super(
            ClinicalTrialCreateContact, self).get_context_data(*args, **kwargs)
        context[u'message'] = _(
            "Confirm it will add this contact to the clinical trial.")
        context[u'cancel_url'] = reverse_lazy(
                                    u'clinical_trial_edit_contacts',
                                    args=(self.kwargs['clinical_trial_pk'],)
                                 )
        context[u'clinical_trial_pk'] = self.kwargs[u'clinical_trial_pk']
        return context

    def get_success_url(self, *args, **kwargs):
        """
        Save Contact as a Clinical Trial Contact
        and redirect to Clinical Trial edit view.
        """
        contact_type = self.kwargs[u'contact_type']
        clinical_trial_pk = self.kwargs['clinical_trial_pk']
        new = CTContact(
                    clinical_trial_id=clinical_trial_pk,
                    contact=self.object, contact_type=contact_type)
        new.save()
        messages.success(self.request, _(u'Created a new contact.'))
        return reverse(
            u'clinical_trial_edit_contacts',
            args=(clinical_trial_pk,)
        )


class ClinicalTrialAttachmentInlineFormSet(InlineFormSet):
    """
    InlineFormset(options) for use of the UpdateWithInlinesView
    """
    model = CTAttachment
    form_class = CTAttachmentForm
    extra = 2


class ClinicalTrialAttachmentsView(NamedFormsetsMixin, UpdateWithInlinesView):
    """
    View for the attachments step.
    """
    model = ClinicalTrial
    fields = []
    inlines = [ClinicalTrialAttachmentInlineFormSet,]
    inlines_names = [u'attachment_formset',]
    template_name = \
                   u'clinical_trials/clinical_trial_detail_attachment_form.html'

    def get(self, *args, **kwargs):
        """
        If clinical trial is in waiting revision or waiting evaluation
        states, redirect to its detail page.
        """
        response = super(ClinicalTrialAttachmentsView, self).get(*args, **kwargs)
        if self.object.get_workflow_state().pk in (
            State.STATE_AGUARDANDO_AVALIACAO,
            State.STATE_AGUARDANDO_REVISAO):
            return HttpResponseRedirect(
                    reverse(u'clinical_trial_detail', args=(self.object.pk,))
                )
        return response

    def get_object(self):
        obj = super(ClinicalTrialAttachmentsView, self).get_object()
        return edit_views_get_object(self, obj)

    def get_success_url(self):
        """
        Set a success message and return the url to redirect user to.
        """
        messages.success(self.request, _(u'Attachments saved.'))
        return reverse(u'clinical_trial_edit_list', args=(self.kwargs[u'pk'],))


    def get_context_data(self, *args, **kwargs):
        context = super(
            ClinicalTrialAttachmentsView,
            self
        ).get_context_data(*args, **kwargs)
        context[u'form_style'] = u'default'
        if self.object.get_workflow_state().pk in (
            State.STATE_EM_REVISAO,
            State.STATE_EM_AVALIACAO):
            context[u'form_style'] = u'observations'
            disable_all_formset_fields(context[u'attachment_formset'])
        context[u'attachment_formset'].content_type_id = \
            ContentType.objects.get_for_model(CTAttachment).pk
        context[u'workflow_current_state'] = get_state(self.object)
        context[u'state_model'] = State
        from administration.models import Attachment
        import json
        attachment_types = \
            json.dumps(
                dict(Attachment.objects.all().values_list('pk', 'attachment_type'))
            )
        context[u'attachment_types'] = attachment_types
        return context



class ClinicalTrialSendView(DetailView):
    """
    View for the attachments step.
    """
    model = ClinicalTrial
    fields = []
    template_name = \
                   u'clinical_trials/clinical_trial_detail_send_form.html'

    def get(self, *args, **kwargs):
        """
        If clinical trial is in waiting revision or waiting evaluation
        states, redirect to its detail page.
        """
        response = super(ClinicalTrialSendView, self).get(*args, **kwargs)
        if self.object.get_workflow_state().pk in (
            State.STATE_AGUARDANDO_AVALIACAO,
            State.STATE_AGUARDANDO_REVISAO):
            return HttpResponseRedirect(
                    reverse(u'clinical_trial_detail', args=(self.object.pk,))
                )
        return response

    def get_object(self):
        obj = super(ClinicalTrialSendView, self).get_object()
        return edit_views_get_object(self, obj)

    def get_context_data(self, *args, **kwargs):
        context = super(
            ClinicalTrialSendView,
            self
        ).get_context_data(*args, **kwargs)
        context[u'form_style'] = u'default'
        if self.object.get_workflow_state().pk in (
            State.STATE_EM_REVISAO,
            State.STATE_EM_AVALIACAO):
            context[u'form_style'] = u'observations'

        context[u'workflow_transition_allowed'] = True
        if self.object.get_workflow_state().pk == State.STATE_EM_PREENCHIMENTO:
            context[u'workflow_transition_allowed'] = self.object.check_all_steps_valid()

        context[u'workflow_current_state'] = get_state(self.object)
        context[u'state_model'] = State
        return context


class ClinicalTrialGroupManagementView(UpdateView):
    """
    View to change the group of a clinical trial.
    """
    model = ClinicalTrial
    form_class = ClinicalTrialGroupChangeForm
    template_name = \
                   u'clinical_trials/clinical_trial_group_management_form.html'

    def get_form_kwargs(self):
        """
        Returns the keyword arguments for instantiating the form.
        """
        kwargs = super(ClinicalTrialGroupManagementView, self).get_form_kwargs()
        kwargs.update({'instance': self.object})
        kwargs.update({'request': self.request})
        return kwargs

    def get_success_url(self):
        """
        Set a success message and return the url to redirect user to.
        """
        messages.success(self.request, _(u'Group management success.'))
        return reverse(u'home')


    def get_success_url(self):
        """
        Set a success message and return the url to redirect user to.
        """
        messages.success(self.request, _(u'Clinical Trial management success.'))
        return self.request.get_full_path()


class ClinicalTrialManageView(PermissionRequiredMixin, ModelFormSetView):
    """
    Management of clinical trials for administrators
    Show a formset of Clinical Trials with pagination support, to change
    just the current page's Clinical Trials.
    """
    model = ClinicalTrial
    form_class = ClinicalTrialManagementForm
    template_name = \
                   u'clinical_trials/clinical_trial_admin_manage_form.html'
    extra = 0
    permissions_required = ['admin.registrant_permission', 'admin.manager_permission', 'admin.administrador_permission']

    def get_pagination(self, queryset):
        paginator = Paginator(queryset, 20)

        return paginator

    def get_page(self):
        return self.request.GET.get(u'page', 1)

    def get_page_object_list(self, queryset, page):
        page = self.get_page()
        paginator = self.get_pagination(queryset)
        try:
            self.page = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            self.page = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            self.page = paginator.page(paginator.num_pages)


    def get_queryset(self):
        queryset = super(ClinicalTrialManageView, self).get_queryset()

        my_groups = self.request.user.ctgroups.filter(
            type_user=CTGroupUser.TYPE_ADMINISTRATOR,
            status=CTGroupUser.STATUS_ACTIVE
            ).values_list(
                u'ct_group_id',
                flat=True
                )

        queryset = queryset.filter(Q(group_id__in=my_groups) | Q(group__isnull=True, user=self.request.user))

        user = self.request.GET.get(u'user', None)
        group = self.request.GET.get(u'group', None)
        ct = self.request.GET.get(u'clinical_trial', None)

        if user:
            queryset = queryset.filter(user=user)
        if group:
            queryset = queryset.filter(group=group)
        if ct:
            queryset = queryset.filter(id=ct)

        queryset = queryset.order_by(u'-pk')
        self.get_page_object_list(queryset, self.get_page())
        queryset = queryset.filter(pk__in=[i.pk for i in self.page.object_list])


        return queryset.distinct()

    def get_context_data(self, **kwargs):
        context = super(ClinicalTrialManageView, self).get_context_data(
            **kwargs)
        context[u'current_page'] = self.page
        context[u'search_form'] = ClinicalTrialManagementSearchForm(
            self.request.GET)
        context[u'get_parameters'] = self.request.get_full_path().split('/')[-1]

        return context

    def get_success_url(self):
        messages.success(self.request, _(u'Clinical trials management success'))
        return reverse(u'clinical_trial_admin_manage')

from workflows.models import StateObjectFieldObservation, StateObjectHistory, \
                             StateObjectGeneralObservation

class ClinicalTrialFieldRevisorView(TemplateView):
    template_name = u'clinical_trials/field-revisor-screen.html'

        
    def get_context_data(self, *args, **kwargs):
        context = super(
            ClinicalTrialFieldRevisorView,
            self
        ).get_context_data(*args, **kwargs)
        content_type_id = kwargs[u'content_type_id']
        content_id = kwargs[u'content_id']
        field_name = kwargs[u'field_name']
        clinical_trial_id = self.kwargs[u'clinical_trial_id']
        model_class = ContentType.objects.get(pk=content_type_id).model_class()
        model_name = model_class.__name__
        #context[u'history'] = self._get_history()
        context[u'content_id'] = content_id
        context[u'content_type_id'] = content_type_id
        context[u'clinical_trial_id'] = clinical_trial_id
        context[u'field_label'] = self._get_field_verbose_name(content_type_id,
                                                              content_id,
                                                              field_name)
        context[u'field_name'] = field_name
        context[u'field_content'] = self._get_field_current_content(model_class,
                                                                    content_id,
                                                                    field_name)

        
        history = GetLog(model_name, int(kwargs[u'content_id']), kwargs[u'field_name'])()
        history = [
            {u'user': i[u'user'],
             u'content': self._convert_field_value(
                            model_class,
                            int(kwargs[u'content_id']),
                            kwargs[u'field_name'],
                            i[u'data'][kwargs[u'field_name']]
                            ),
             u'date': i[u'date_stamp'],
             u'new': False,
             u'field_type': self._get_field_type(model_class,
                                                 kwargs[u'field_name'])
            } for i in history]

        context[u'history'] = history
        context[u'revisor_observation_history'] = self._get_reviser_observation_history(
            clinical_trial_id, content_type_id, content_id, field_name
        )
        context[u'registrant_observation_history'] = self._get_registrant_observation_history(
            clinical_trial_id, content_type_id, content_id, field_name
        )

        return context

    def post(self, *args, **kwargs):
        """
        Handle a ajax post request to create a new observation.
        """
        content_type_id = kwargs[u'content_type_id']
        content_id = kwargs[u'content_id']
        field_name = kwargs[u'field_name']
        clinical_trial_id = self.kwargs[u'clinical_trial_id']
    
        if not self.request.POST[u'new_rev_obs']:
            messages.error(self.request, _(u'The observation is required'))
        else:
            StateObjectFieldObservation.objects.create(
                content_type_id=content_type_id,
                content_id=content_id,
                field_name=field_name,
                user=self.request.user,
                object_state=StateObjectHistory.objects.filter(
                                content_id=clinical_trial_id
                             ).latest(u'update_date'),
                observation=self.request.POST[u'new_rev_obs'],
            )
            messages.success(self.request, _(u'New observation created.'))
        return HttpResponseRedirect(self.request.GET[u'back_url'])


    def _get_field_current_content(self, model_class, content_id, field_name):
        form = modelform_factory(model=model_class, fields=(field_name,))(
                            instance=model_class.objects.get(pk=content_id),
                            data=self.request.POST or None)
        import six
        form.fields[field_name].widget.attrs[u'readonly'] = u'readonly'
        form.fields[field_name].widget.attrs[u'disabled'] = u'disabled'
        return six.text_type(form[field_name])


    def _get_field_verbose_name(self, content_type_id, content_id, field_name):
        try:
            return ContentType.objects.get(
                               pk=content_type_id
                           ).get_object_for_this_type(
                                pk=content_id
                           )._meta.get_field(field_name).verbose_name
        except FieldDoesNotExist:
            raise Http404


    def _get_reviser_observation_history(self, clinical_trial_id,
                                         content_type_id, content_id, field_name):
        """
        Get this field's observations made by revisers
        """
        hist = StateObjectFieldObservation.objects.filter(
            content_type_id=content_type_id,
            content_id=content_id,
            field_name=field_name,
            user__groups__name__in=(u'reviser', u'evaluator')
            ).order_by(u'update_date')

        return hist


    def _get_registrant_observation_history(self, clinical_trial_id,
                                         content_type_id, content_id, field_name):
        """
        Get this field's observations made by registrants
        """
        hist = StateObjectFieldObservation.objects.filter(
            content_type_id=content_type_id,
            content_id=content_id,
            field_name=field_name,
            user__groups__name=u'registrant'
            ).order_by(u'update_date')

        return hist


    def _get_field_type(self, model_class, field_name):
        field = model_class._meta.get_field(field_name)
        return field.get_internal_type()


    def _convert_field_value(self, model_class,
                             content_id, field_name,
                             field_value):
        """
        Convert the field's value based on field type
        """
        try:
            field = model_class._meta.get_field(field_name)
            field_type = self._get_field_type(model_class, field_name)
            if field_type == u'ForeignKey':
                return field.rel.to.objects.get(pk=field_value)
            if getattr(field, u'choices', False):
                return dict(field.choices)[field_value]

            if field_type == u'BooleanField':
                if field_value:
                    return mark_safe('<img src="/static/img/field-ok.png"/>')
                else:
                    return mark_safe('<img src="/static/img/field-error.png"/>')
            if field_type == u'ManyToManyField':
                if field_value:
                    return ', '.join(field_value)
            if field_type == u'FileField':
                return  u''
        except Exception, exc:
            return _(u"Error getting fields's value: %s" % exc)

        return field_value



class ClinicalTrialFieldRegistrantView(TemplateView):
    template_name = u'clinical_trials/field-registrant-screen.html'

        
    def get_context_data(self, *args, **kwargs):
        context = super(
            ClinicalTrialFieldRegistrantView,
            self
        ).get_context_data(*args, **kwargs)
        content_type_id = kwargs[u'content_type_id']
        content_id = kwargs[u'content_id']
        field_name = kwargs[u'field_name']
        clinical_trial_id = self.kwargs[u'clinical_trial_id']
        model_class = ContentType.objects.get(pk=content_type_id).model_class()
        model_name = model_class.__name__
        #context[u'history'] = self._get_history()
        context[u'content_id'] = content_id
        context[u'content_type_id'] = content_type_id
        context[u'clinical_trial_id'] = clinical_trial_id
        context[u'field_label'] = self._get_field_verbose_name(content_type_id,
                                                              content_id,
                                                              field_name)
        context[u'field_name'] = field_name
        context[u'field_content'] = self._get_field_current_content(content_type_id,
                                                                    content_id,
                                                                    field_name)
        context[u'form'] = modelform_factory(model=model_class, fields=(field_name,))(
                            instance=model_class.objects.get(pk=content_id),
                            data=self.request.POST or None)


        if self.request.GET.has_key(u'back_url'):
            context[u'back_url'] = self.request.GET[u'back_url']

        
        history = GetLog(model_name, int(kwargs[u'content_id']), kwargs[u'field_name'])()
        history = [
            {u'user': i[u'user'],
             u'content': self._convert_field_value(
                            model_class,
                            int(kwargs[u'content_id']),
                            kwargs[u'field_name'],
                            i[u'data'][kwargs[u'field_name']]
                            ),
             u'date': i[u'date_stamp'],
             u'new': False,
             u'field_type': self._get_field_type(model_class,
                                                 kwargs[u'field_name'])
            } for i in history]

        context[u'history'] = history
        context[u'revisor_observation_history'] = self._get_reviser_observation_history(
            clinical_trial_id, content_type_id, content_id, field_name
        )
        context[u'registrant_observation_history'] = self._get_registrant_observation_history(
            clinical_trial_id, content_type_id, content_id, field_name
        )


        
        return context


    def post(self, *args, **kwargs):
        """
        Handle a ajax post request to create a new observation.
        """
        content_type_id = kwargs[u'content_type_id']
        content_id = kwargs[u'content_id']
        field_name = kwargs[u'field_name']
        clinical_trial_id = self.kwargs[u'clinical_trial_id']
    
        if not self.request.POST[u'new_reg_obs']:
            messages.error(self.request, _(u'The observation is required'))
        else:
            StateObjectFieldObservation.objects.create(
                content_type_id=content_type_id,
                content_id=content_id,
                field_name=field_name,
                user=self.request.user,
                object_state=StateObjectHistory.objects.filter(
                                content_id=clinical_trial_id
                             ).latest(u'update_date'),
                observation=self.request.POST[u'new_reg_obs']
            )
            messages.success(self.request, _(u'New observation created.'))
        return HttpResponseRedirect(self.request.GET[u'back_url'])


    def _get_field_current_content(self, content_type_id, content_id, field_name):
        return getattr(ContentType.objects.get(
                           pk=content_type_id
                       ).get_object_for_this_type(
                            pk=content_id
                       ),
                       field_name
                )


    def _get_field_verbose_name(self, content_type_id, content_id, field_name):
        return ContentType.objects.get(
                           pk=content_type_id
                       ).get_object_for_this_type(
                            pk=content_id
                       )._meta.get_field(field_name).verbose_name


    def _get_reviser_observation_history(self, clinical_trial_id,
                                         content_type_id, content_id, field_name):
        """
        Get this field's observations made by revisers
        """
        hist = StateObjectFieldObservation.objects.filter(
            content_type_id=content_type_id,
            content_id=content_id,
            field_name=field_name,
            user__groups__name__in=(u'reviser', u'evaluator')
            ).order_by(u'update_date')

        return hist


    def _get_registrant_observation_history(self, clinical_trial_id,
                                         content_type_id, content_id, field_name):
        """
        Get this field's observations made by registrants
        """
        hist = StateObjectFieldObservation.objects.filter(
            content_type_id=content_type_id,
            content_id=content_id,
            field_name=field_name,
            user__groups__name=u'registrant'
            ).order_by(u'update_date')

        return hist


    def _get_field_type(self, model_class, field_name):
        field = model_class._meta.get_field(field_name)
        return field.get_internal_type()

    def _convert_field_value(self, model_class,
                             content_id, field_name,
                             field_value):
        """
        Convert the field's value based on field type
        """
        try:
            field = model_class._meta.get_field(field_name)
            field_type = self._get_field_type(model_class, field_name)

            if field_type == u'ForeignKey':
                return field.rel.to.objects.get(pk=field_value)
            if getattr(field, u'choices', False):
                return dict(field.choices)[field_value]

            if field_type == u'BooleanField':
                if field_value:
                    return mark_safe('<img src="/static/img/field-ok.png"/>')
                else:
                    return mark_safe('<img src="/static/img/field-error.png"/>')
            if field_type == u'ManyToManyField':
                if field_value:
                    return ', '.join(field_value)
        except Exception, exc:
            return _(u"Error getting fields's value: %s" % exc)

        return field_value



class ClinicalTrialAddFieldObservationView(CreateView):
    template_name = u'clinical_trials/stateobjectfieldobservation_form.html'
    model = StateObjectFieldObservation
    fields = (u'observation',)

    def form_valid(self, form):
        form.instance.content_id = self.kwargs[u'content_id']
        form.instance.content_type_id = self.kwargs[u'content_type_id']
        form.instance.field_name = self.kwargs[u'field_name']
        form.instance.user = self.request.user
        form.instance.object_state = StateObjectHistory.objects.filter(content_id=self.kwargs[u'clinical_trial_id']).latest(u'update_date')

        return super(
            ClinicalTrialAddFieldObservationView,
            self
        ).form_valid(form)

    def get_context_data(self, form):
        context = super(
            ClinicalTrialAddFieldObservationView,
            self
        ).get_context_data(form=form)
        context[u'history'] = self._get_history()
        context[u'clinical_trial_id'] = self.kwargs[u'clinical_trial_id']
        return context

    def _get_history(self):
        return StateObjectFieldObservation.objects.filter(
            content_type_id=self.kwargs[u'content_type_id'],
            content_id=self.kwargs[u'content_id'],
            field_name=self.kwargs[u'field_name']
        ).order_by(u'update_date')

    def get_success_url(self):
        """
        Set a success message and return the url to redirect user to.
        """
        messages.success(self.request, _(u'Field observation saved.'))
        if self.request.GET.get(u'back_url', False):
            return self.request.GET.get(u'back_url')
        return reverse(
            u'clinical_trial_edit_list',
            args=(self.kwargs[u'clinical_trial_id'],)
        )

class ClinicalTrialListFieldObservationView(TemplateView):
    template_name = u'clinical_trials/stateobjectfieldobservation_list.html'

    def get_context_data(self, **kwargs):
        context = super(
            ClinicalTrialListFieldObservationView,
            self
        ).get_context_data(**kwargs)
        context[u'history'] = self._get_history()
        context[u'clinical_trial_id'] = self.kwargs[u'clinical_trial_id']
        context[u'clinical_trial'] = ClinicalTrial.objects.get(pk=self.kwargs[u'clinical_trial_id'])
        context[u'HTTP_REFERER'] = self.request.META.get(u'HTTP_REFERER', None)
        if self.request.GET.has_key(u'clinical_trial_url_step'):
            context[u'clinical_trial_url_step'] = self.request.GET[u'clinical_trial_url_step']
        return context

    def _get_history(self):
        hist = StateObjectFieldObservation.objects.filter(
            object_state__pk__in=StateObjectHistory.objects.filter(
                                    content_id=self.kwargs[u'clinical_trial_id']
                                  ),
            content_type_id=self.kwargs[u'content_type_id'],
            content_id=self.kwargs[u'content_id'],
            field_name=self.kwargs[u'field_name']).order_by(u'update_date')
        return hist


class ClinicalTrialListFieldChangeHistoryView(TemplateView):
    template_name = u'clinical_trials/fieldchangehistory_list.html'

    def _get_field_type(self, model_class, field_name):
        field = model_class._meta.get_field(field_name)
        return field.get_internal_type()

    def _convert_field_value(self, model_class,
                             content_id, field_name,
                             field_value):
        """
        Convert the field's value based on field type
        """
        try:
            field = model_class._meta.get_field(field_name)
            field_type = self._get_field_type(model_class, field_name)

            if field_type == u'ForeignKey':
                return field.rel.to.objects.get(pk=field_value)
            if getattr(field, u'choices', False):
                return dict(field.choices)[field_value]

            if field_type == u'BooleanField':
                if field_value:
                    return mark_safe('<img src="/static/img/field-ok.png"/>')
                else:
                    return mark_safe('<img src="/static/img/field-error.png"/>')
            if field_type == u'ManyToManyField':
                if field_value:
                    return ', '.join(field_value)
        except Exception, exc:
            return _(u"Error getting fields's value: %s" % exc)

        return field_value

    def get_context_data(self, **kwargs):
        context = super(
            ClinicalTrialListFieldChangeHistoryView,
            self
        ).get_context_data(**kwargs)
        model_class = ContentType.objects.get(pk=kwargs[u'content_type_id']).model_class()
        name = model_class.__name__

        history = GetLog(name, int(kwargs[u'content_id']), kwargs[u'field_name'])()
        history = [
            {u'user': i[u'user'],
             u'content': self._convert_field_value(
                            model_class,
                            int(kwargs[u'content_id']),
                            kwargs[u'field_name'],
                            i[u'data'][kwargs[u'field_name']]
                            ),
             u'date': i[u'date_stamp'],
             u'new': False,
             u'field_type': self._get_field_type(model_class,
                                                 kwargs[u'field_name'])
            } for i in history]

        clinical_trial = ClinicalTrial.objects.get(pk=kwargs[u'clinical_trial_id'])
        if clinical_trial.date_last_publication:
            clinical_trial.date_last_publication = clinical_trial.date_last_publication.replace(tzinfo=None)
            for his in history:
                if his[u'date'] > clinical_trial.date_last_publication:
                    his[u'new'] = True
        context[u'history'] = history
        context[u'HTTP_REFERER'] = self.request.META.get(u'HTTP_REFERER', None)
        if self.request.GET.has_key(u'clinical_trial_url_step'):
            context[u'clinical_trial_url_step'] = self.request.GET[u'clinical_trial_url_step']
        return context

ACTION_TEXT = _(u'set state to')
from workflows.models import StateObjectHistory
class ClinicalTrialHistoryView(TemplateView):
    template_name = u'clinical_trials/clinicaltrialhistory_list.html'

    def get_context_data(self, pk):
        if not self.request.user.is_authenticated():
            raise Http404
        context = super(ClinicalTrialHistoryView, self).get_context_data(pk=pk)
        context[u'object'] = get_object_or_404(ClinicalTrial, pk=pk)

        #control to access this view is the same of edit views
        edit_views_get_object(self, context[u'object'])

        history = StateObjectHistory.objects.filter(
            content_id=pk).order_by('update_date')

        history_list = []
        for i in history:
            current = '%s %s %s.' % (i.user.name, ACTION_TEXT, i.state.name)
            history_list.append((i.update_date, current))
        context[u'history_list'] = history_list
        context[u'back_url'] = self.request.META.get(u'HTTP_REFERER', None)

        return context


class NoholderCTView(PermissionRequiredMixin, ModelFormSetView):
    model = ClinicalTrial
    fields = (u'holder',)
    form_class = ShowJustRevisorsAndEvaluatorsForm
    template_name = \
                   u'clinical_trials/clinical_trial_without_holder_list.html'
    extra = 0
    permissions_required = ['admin.manager_permission', 'admin.administrador_permission']

    def get_pagination(self, queryset):
        paginator = Paginator(queryset, 20)

        return paginator

    def get_page(self):
        return self.request.GET.get(u'page', 1)

    def get_page_object_list(self, queryset, page):
        page = self.get_page()
        paginator = self.get_pagination(queryset)
        try:
            self.page = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            self.page = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            self.page = paginator.page(paginator.num_pages)


    def get_queryset(self):
        queryset = super(NoholderCTView, self).get_queryset()

        queryset = queryset.filter(holder__isnull=True)

        user = self.request.GET.get(u'user', None)
        group = self.request.GET.get(u'group', None)
        ct = self.request.GET.get(u'clinical_trial', None)

        queryset = queryset.order_by(u'-pk')
        self.get_page_object_list(queryset, self.get_page())
        queryset = queryset.filter(pk__in=[i.pk for i in self.page.object_list])

        return queryset.distinct()

    def get_context_data(self, **kwargs):
        context = super(NoholderCTView, self).get_context_data(
            **kwargs)
        context[u'current_page'] = self.page
        context[u'get_parameters'] = self.request.get_full_path().split('/')[-1]

        return context

    def get_success_url(self):
        messages.success(self.request, _(u'Holders changed'))
        return reverse(u'no_holder_cts_list')


class ClinicalTrialListObservationsView(FormView):
    template_name = u'clinical_trials/list_pending_observations.html'
    form_class = AddNewWorkflowGeneralObservationForm


    def form_valid(self, form):
        """
        Create the new observation
        """
        if self.request.user.is_reviser() or self.request.user.is_evaluator():
            StateObjectGeneralObservation.objects.create(
                user=self.request.user,
                content_type=ContentType.objects.get_for_model(ClinicalTrial),
                content_id=self.kwargs[u'clinical_trial_id'],
                observation=form.cleaned_data[u'new_obs'],
                object_state=StateObjectHistory.objects.filter(
                                 content_id=self.kwargs[u'clinical_trial_id']
                             ).latest(u'update_date')
            )
            messages.success(self.request,
                             _(u'New general observation created'))
        return HttpResponseRedirect(self.request.path)

    def get_context_data(self, **kwargs):
        context = super(
            ClinicalTrialListObservationsView,
            self
        ).get_context_data(**kwargs)
        try:
            date_last_resubmited = StateObjectHistory.objects.filter(
                                       content_id=self.kwargs[u'clinical_trial_id'],
                                       state__pk=State.STATE_RESUBMETIDO
                                   ).latest('update_date').update_date
            #Here, we need to get the latest state change before the RESUBMETIDO state,
            #because after this state, the reviser/evaluator has made the new observations.
            date_last_resubmited = StateObjectHistory.objects.filter(
                                       content_id=self.kwargs[u'clinical_trial_id'],
                                       update_date__lt=date_last_resubmited
                                   ).latest(u'update_date').update_date
        except StateObjectHistory.DoesNotExist:
            date_last_resubmited = datetime(1900, 1, 1)
        context[u'history'] = self._get_history(date_last_resubmited, news=False)
        context[u'specific_new'] = self._get_history(date_last_resubmited, news=True)
        context[u'general_history'] = \
            StateObjectGeneralObservation.objects.filter(
                content_id=self.kwargs[u'clinical_trial_id'],
                update_date__lt=date_last_resubmited
            ).order_by(u'update_date')

        context[u'general_new'] = \
            StateObjectGeneralObservation.objects.filter(
                content_id=self.kwargs[u'clinical_trial_id'],
                update_date__gte=date_last_resubmited
            ).order_by(u'update_date')

        context[u'clinical_trial_id'] = self.kwargs[u'clinical_trial_id']
        context[u'clinical_trial'] = get_object_or_404(ClinicalTrial, pk=self.kwargs[u'clinical_trial_id'])
        context[u'show_new_general_obs_form'] = \
            self.request.user.is_reviser() or self.request.user.is_evaluator()
        #from IPython import embed;embed()
        return context

    def _get_history(self, date_last_resubmited, news):
        """
        Return the specific fields observations history
        date_last_resubmited: the date the trial was last resubmited
        news: return just new ones(after last resubmition), or just the old ones
        """
     
        hist = StateObjectFieldObservation.objects.filter(
            object_state__pk__in=StateObjectHistory.objects.filter(
                                    content_id=self.kwargs[u'clinical_trial_id']
                                  )
            ).order_by(u'field_name', u'update_date')
        if news:
            hist = hist.filter(update_date__gte=date_last_resubmited)
        else:
            hist = hist.filter(update_date__lte=date_last_resubmited)

        hist_list = []
        ct = ClinicalTrial.objects.get(pk=self.kwargs[u'clinical_trial_id'])
        for i in hist:
            step_location = None
            for k, v in CLINICAL_TRIALS_STEPS_FIELDS[u'related_queries'].items():
                if i.field_name in [field.name for field in getattr(ct, v[0]).model._meta.fields] and getattr(ct, v[0]).model == i.content_type.model_class():
                    step_location = k
                    break
            for k, v in CLINICAL_TRIALS_STEPS_FIELDS[u'ctdetail_fields'].items():
                if i.field_name in v:
                    step_location = k
                    break
            for k, v in CLINICAL_TRIALS_STEPS_FIELDS[u'ct_fields'].items():
                if i.field_name in v:
                    step_location = k
                    break
            try:
                hist_list.append(
                    {u'attribute': i.content._meta.get_field(
                                                    i.field_name).verbose_name,
                     u'step': step_location,
                     u'observation': i.observation,
                     u'obs_pk': i.pk,
                     u'user': i.user,
                     u'update_date': i.update_date
                    })
            except FieldDoesNotExist:
                pass
        #return the hist_list ordered by step key
        return sorted(hist_list, key=lambda x: x['step'])


class DescriptorsCid10SearchJsonView(View):

    BASEURL = "http://bases.bireme.br/cgi-bin/mxlindG4.exe/cgi=@cid10/cid10?%s"

    def get(self, request):
        """
        Return the results based on the webservice
        """
        
        def mount_url(term):
            term = unicode(term).encode(u'utf-8')
            return self.BASEURL % urllib.urlencode({'words': term})

        def mount_response(data_list):
            return [(i.text, i.text) for i in data_list]

        term = request.GET.get(u'term')
        content = urllib.urlopen(mount_url(term)).read()
        css_parser = html.fromstring(content)
        
        response = []
        for cid10ws_response in css_parser.cssselect('cid10ws_response'):
            tree_id = cid10ws_response.attrib.get(u'tree_id')
            descriptor_en = cid10ws_response.cssselect('descriptor[lang=en]')[0].text
            descriptor_es = cid10ws_response.cssselect('descriptor[lang=es]')[0].text
            descriptor_pt = cid10ws_response.cssselect('descriptor[lang=pt]')[0].text
            response.append({u'tree_id': tree_id,
                             u'descriptor_en': descriptor_en,
                             u'descriptor_es': descriptor_es,
                             u'descriptor_pt': descriptor_pt})

        return HttpResponse(json.dumps(response),
                            content_type=u'application/json')


        
class DescriptorsDeCSSearchJsonView(View):

    BASEURL = "http://decs.bvsalud.org/cgi-bin/mx/cgi=@vmx/decs/?%s"

    def get(self, request):
        """
        Return the results based on the webservice
        """
        
        def mount_url(term):
            term = unicode(term).encode(u'utf-8')
            return self.BASEURL % urllib.urlencode({'words': term})

        def mount_response(data_list):
            return [(i.text, i.text) for i in data_list]

        term = request.GET.get(u'term')
        content = urllib.urlopen(mount_url(term)).read()
        css_parser = html.fromstring(content)
        
        response = []
        for decsws_response in css_parser.cssselect('decsws_response'):
            tree_id = decsws_response.attrib.get(u'tree_id')
            descriptor_en = decsws_response.cssselect('descriptor[lang=en]')[0].text
            descriptor_es = decsws_response.cssselect('descriptor[lang=es]')[0].text
            descriptor_pt = decsws_response.cssselect('descriptor[lang=pt]')[0].text
            definition = decsws_response.cssselect('definition occ')[0].attrib.get(u'n')
            response.append({u'tree_id': tree_id,
                             u'descriptor_en': descriptor_en,
                             u'descriptor_es': descriptor_es,
                             u'descriptor_pt': descriptor_pt,
                             u'definition': definition})

        return HttpResponse(json.dumps(response),
                            content_type=u'application/json')


        
class ClinicalTrialAcceptInGroupView(TemplateView):
    template_name = u'clinical_trials/approve_in_group.html'

    def get_context_data(self, *args, **kwargs):
        context = super(ClinicalTrialAcceptInGroupView,
                        self).get_context_data(*args, **kwargs)

        pk = kwargs.get(u'pk')
        context[u'trial'] = get_object_or_404(ClinicalTrial, pk=pk)

        get_object_or_404(ClinicalTrial,
                          pk=pk,
                          group__users__user__pk=self.request.user.pk,
                          group__users__type_user=u'a')

        return context

    def post(self, *args, **kwargs):
        pk = kwargs.get(u'pk')
        trial = get_object_or_404(ClinicalTrial, pk=pk)
        get_object_or_404(ClinicalTrial,
                          pk=pk,
                          group__users__user__pk=self.request.user.pk,
                          group__users__type_user=u'a')

        if self.request.POST.has_key(u'accept'):
            email_text = render_to_string(
                             u'clinical_trials/email_adm_approved_trial.html',
                             {u'trial': trial})
            send_mail(
                    _(u'Clinical trial approved'),
                    email_text,
                    None,
                    [trial.user.email])
            messages.success(self.request,
                             _(u'Clinical trial approved in group.'))
        else:
            trial.group = None
            trial.save()
            email_text = render_to_string(
                             u'clinical_trials/email_adm_declined_trial.html',
                             {u'trial': trial})
            send_mail(
                    _(u'Clinical trial declined'),
                    email_text,
                    None,
                    [trial.user.email])
            messages.success(self.request,
                             _(u'Clinical trial removed from group.'))

        return HttpResponseRedirect(reverse(u'home'))



class ClinicalTrialChangeStudyStatusView(DetailView):
    model = ClinicalTrial
    template_name = u'clinical_trials/check_for_changes_study_status.html'


    def get_object(self):
        obj = get_object_or_404(ClinicalTrial, pk=self.kwargs.get(u'pk'))
        if not obj.get_workflow_state().pk == State.STATE_PUBLICADO:
            raise Http404

        if (datetime.today().date() - obj.date_last_change).days < 180:
            raise Http404

        if not obj.user == self.request.user:
            raise Http404

        obj.days_last_check = (datetime.today().date() - obj.date_last_change).days
        return obj

    def get_context_data(self, object):
        self.object = object
        context_data = super(
                           ClinicalTrialChangeStudyStatusView,
                           self
                       ).get_context_data()
        if self.request.method == u'GET':
            context_data[u'form'] = ChangeStudyStatusForm()
            context_data[u'form'].initial[u'study_status'] = \
                                            context_data[u'object'].study_status

        return context_data


    def post(self, request, pk):
        obj = self.get_object()
        if request.POST.get(u'next', None) == u'Next':
            messages.success(request, _(u'You has just clicked "Next", wich means that your study status has not changed.'))
            return HttpResponseRedirect(reverse(u'clinical_trial_check_for_changes_step2', args=(pk,)))
        form = ChangeStudyStatusForm(data=request.POST)
        if form.is_valid():
            obj.study_status = form.cleaned_data[u'study_status']
            obj.save()
            obj.save(using=u'published_cts')
            messages.success(request, _(u'Trial study status successfully changed.'))
            return HttpResponseRedirect(reverse(u'clinical_trial_check_for_changes_step2', args=(pk,)))

        context = self.get_context_data(obj)
        context[u'form'] = form
        return render_to_response(self.template_name, RequestContext(request, context))

        



class ClinicalTrialCheckForChangesView(DetailView):
    model = ClinicalTrial
    template_name = u'clinical_trials/check_for_changes.html'


    def get_object(self):
        obj = get_object_or_404(ClinicalTrial, pk=self.kwargs.get(u'pk'))
        if not obj.get_workflow_state().pk == State.STATE_PUBLICADO:
            raise Http404

        if (datetime.today().date() - obj.date_last_change).days < 180:
            raise Http404

        if not obj.user == self.request.user:
            raise Http404

        obj.days_last_check = (datetime.today().date() - obj.date_last_change).days
        return obj

    def post(self, request, pk):
        obj = self.get_object()

        if request.POST.has_key(u'reopen'):
            do_transition(
                obj,
                Transition.objects.get(
                    pk=Transition.\
                       TRANSITION_REVISAR
                ),
                self.request.user
            )
            obj.holder = self.request.user
            obj.save()
            messages.success(
                self.request,
                _(u'Changed to filling state.')
            )
            return HttpResponseRedirect(
                    reverse(u'clinical_trial_edit_list', args=(obj.pk,))
                )

        elif request.POST.has_key(u'no_changes'):
            obj.date_last_change = datetime.today().date()
            obj.save()
            obj.save(using=u'published_cts')
            messages.success(
                self.request,
                _(u'No changes for this trial. Thanks for your feedback!')
            )
            return HttpResponseRedirect(
                    reverse(u'home')
                )
        else:
            raise PermissionDenied

class ClinicalTrialSnapshotView(View):

    def get(self, request, pk):
        history = get_object_or_404(StateObjectHistory, pk=pk,
                                    trial_snapshot__isnull=False)

        return render_to_response(u'clinical_trials/clinicaltrial_detail.html',
                                  {u'html_snapshot': history.trial_snapshot,
                                   u'snapshot_date': history.update_date},
                                  context_instance=RequestContext(request))