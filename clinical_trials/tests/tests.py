# -*- coding: utf-8 -*-
from datetime import datetime
from django.test import TestCase
from django.test.client import RequestFactory

from django.contrib.auth import get_user_model
from clinical_trials.models import (
    ClinicalTrial, Contact, Outcome, CTContact,
    Descriptor)
from clinical_trials.forms import InstitutionChoices
from clinical_trials.views import (
    ClinicalTrialAddView, ClinicalTrialIdentificationView,
    ClinicalTrialSponsorsView, ClinicalTrialHealthConditionView,
    ClinicalTrialInterventionView, ClinicalTrialRecruitmentView,
    ClinicalTrialStudyTypeView, ClinicalTrialContactsView,
    ClinicalTrialAttachmentsView, ClinicalTrialOutcomesView,
    ClinicalTrialGroupManagementView, ClinicalTrialEditListView,
    ClinicalTrialDetailView)
from administration.models import Setup
from administration.models import (
    SetupIdentifier, InstitutionType, Institution,
    Country, Vocabulary, VocabularyItem, CTGroup,
    Setup, InterventionAssignment,MaskingType,
    StudyPurpose, AllocationType, StudyPhase,
    ObservationStudyDesign, Attachment,
    TimePerspective, InterventionCode, CTGroupUser)

from clinical_trials.views import ClinicalTrialAddView, \
    ClinicalTrialIdentificationView, ClinicalTrialSponsorsView, \
    ClinicalTrialHealthConditionView, ClinicalTrialInterventionView, \
    ClinicalTrialRecruitmentView, ClinicalTrialStudyTypeView, \
    ClinicalTrialContactsView, ClinicalTrialAttachmentsView, \
    ClinicalTrialOutcomesView, ClinicalTrialGroupManagementView, \
    ClinicalTrialManageView, ClinicalTrialCreateContact

from django.core.urlresolvers import reverse, resolve
from mock import Mock, MagicMock
from django.contrib import messages
from django.contrib.auth.models import Group
from workflows import utils
from workflows.models import StateObjectRelation, State, \
                             StateObjectFieldObservation
from django.utils.translation import ugettext as _
from django.core.exceptions import PermissionDenied
from django.contrib.contenttypes.models import ContentType

User = get_user_model()
# --- Fixtures

def create_user():
    user = User.objects.create_user(
        username='user',
        email= 'user@email.com',
        password='pass',
        gender=1,
        race=1,
        country_id="BR")

    user.save()
    user.groups.add(Group.objects.get(name=u'registrant'))
    return user

def get_clinical_trial(user, code):
    ct = ClinicalTrial()
    ct.user = user
    ct.holder = user
    ct.code = code
    ct.language_1_mandatory = True
    ct.language_2_mandatory = True
    ct.language_3_mandatory = False
    return ct

# --- Models

class TestClinicalTrialSignals(TestCase):

    def setUp(self):
        self.user = create_user()
        self.setup = Setup()
        self.setup.language_1_code_id = 167
        self.setup.language_1_mandatory = True
        self.setup.language_2_code_id = 57
        self.setup.language_2_mandatory = True
        self.setup.language_3_code_id = 61
        self.setup.language_3_mandatory = False
        self.setup.queue_distribution = 'lala'
        self.setup.current_position_qd0 = 1
        self.setup.save()

    def test_post_init_should_set_default_languages(self):
        c = ClinicalTrial()
        self.assertEqual(c.language_1_mandatory, True)
        self.assertEqual(c.language_2_mandatory, True)
        self.assertEqual(c.language_3_mandatory, False)

    def test_handle_initial_state(self):
        self.original_set_initial_state = utils.set_initial_state
        utils.set_initial_state = MagicMock()
        c = get_clinical_trial(self.user, 1)
        c.save()
        self.assertEqual(utils.set_initial_state.call_count, 1)
        c.delete()
        utils.set_initial_state = self.original_set_initial_state

    def test_handle_workflow_ct_deleted(self):
        self.original_remove_states_from_object = utils.remove_states_from_object
        utils.remove_states_from_object = MagicMock()
        c = get_clinical_trial(self.user, 1)
        c.save()
        self.assertEqual(utils.remove_states_from_object.call_count, 0)
        #Assert that exists a relation of this CT with the workflow
        exists = StateObjectRelation.objects.filter(content_id=c.id).exists()
        self.assertTrue(exists)
        c.delete()
        self.assertEqual(utils.remove_states_from_object.call_count, 1)
        #Assert that NOT exists a relation of this CT with the workflow
        exists = StateObjectRelation.objects.filter(content_id=c.id).exists()
        self.assertFalse(exists)
        utils.remove_states_from_object = self.original_remove_states_from_object
        self.user.stateobjecthistory_set.all().delete()

    def tearDown(self):
        self.setup.delete()
        self.user.delete()

# --- Views

#class TestClinicalTrialDetailView(TestCase):

#    def setUp(self):
#        self.factory = RequestFactory()
#        self.user = create_user()
#        self.clinical_trial = get_clinical_trial(self.user)
#        self.clinical_trial.save()

#    def test_assigned_variables_on_context(self, **kwargs):
#        request = self.factory.get(reverse('clinical_trial_detail', kwargs={'pk':self.clinical_trial.id}))
#        view_method = ClinicalTrialDetailView.as_view()
#        response = view_method(request, pk=self.clinical_trial.id)
#        self.assertIsNot(response.context_data['sponsors'], None)
#        self.assertIsNot(response.context_data['details'], None)
#        self.assertIsNot(response.context_data['outcomes'], None)

#    def tearDown(self):
#        self.clinical_trial.delete()
#        self.user.delete()


class ClinicalTrialAddViewTestCase(TestCase):

    def setUp(self):
        self.factory = RequestFactory()
        self.user = create_user()

        #mocks
        self.original_success = messages.success
        messages.success = Mock()

    def tearDown(self):

        messages.success = self.original_success

    def test_add_clinical_trial(self):
        post_data = {u'language_1_mandatory': u'on',
                     u'language_2_mandatory': u'on',
                     u'accept_terms': u'on',
                     u'study_type': u'O'}
        request = self.factory.post(reverse(u'clinical_trial_add'),
                                    data=post_data)
        request.user = self.user
        
        #check success isn't called yet
        self.assertEqual(messages.success.called, False)

        #call the add view
        view_return = ClinicalTrialAddView.as_view()(request)
        #now, the success must have a call
        self.assertEqual(messages.success.called, True)
        #check the redirect success status
        self.assertEqual(view_return.status_code, 302)

        #resolve the url redirected, to get the created pk
        created_pk = resolve(dict(view_return.items())['Location']).kwargs['pk']
        new = ClinicalTrial.objects.get(pk=created_pk)

        #check if the user is the logged-in user
        self.assertEqual(new.user, self.user)
        #check if has created a CTDetail for each language_X_mandatory
        self.assertEqual(new.ctdetail_set.all().count(), 2)

    def test_add_with_accept_terms_required(self):
        post_data = {u'language_1_mandatory': u'on',
                     u'language_2_mandatory': u'on'}
        request = self.factory.post(reverse(u'clinical_trial_add'),
                                    data=post_data)
        request.user = self.user
        
        #check success isn't called yet
        self.assertEqual(messages.success.called, False)

        #call the add view
        view_return = ClinicalTrialAddView.as_view()(request)

        #an error must be raised because of the accept terms
        self.assertEqual(messages.success.called, False)
        self.assertEqual(view_return.status_code, 200)

        self.assertTrue(
            view_return.context_data[u'form'].errors.has_key(
                u'accept_terms'
            )
        )

        post_data = {u'language_1_mandatory': u'on',
                     u'language_2_mandatory': u'on',
                     u'accept_terms': u'on',
                     u'study_type': u'O'}

        request = self.factory.post(reverse(u'clinical_trial_add'),
                                    data=post_data)
        request.user = self.user
        #call the add view
        view_return = ClinicalTrialAddView.as_view()(request)
        #now, the success must have a call
        self.assertEqual(messages.success.called, True)
        #check the redirect success status
        self.assertEqual(view_return.status_code, 302)

        #resolve the url redirected, to get the created pk
        created_pk = resolve(dict(view_return.items())['Location']).kwargs['pk']
        new = ClinicalTrial.objects.get(pk=created_pk)

        #check if the user is the logged-in user
        self.assertEqual(new.user, self.user)
        #check if has created a CTDetail for each language_X_mandatory
        self.assertEqual(new.ctdetail_set.all().count(), 2)
    def test_terms_of_use_on_context(self):
        request = self.factory.get(reverse(u'clinical_trial_add'))
        request.user = self.user
        view_return = ClinicalTrialAddView.as_view()(request)

        self.assertTrue(view_return.context_data.has_key(u'terms_of_use'))
        self.assertEqual(view_return.context_data.get(u'terms_of_use'),
                         Setup.objects.get().terms_of_use)


    def test_add_clinical_trial_without_self_funding(self):
        post_data = {u'language_1_mandatory': u'on',
                     u'language_2_mandatory': u'on',
                     u'accept_terms': u'on',
                     u'study_type': u'O'}
        request = self.factory.post(reverse(u'clinical_trial_add'),
                                    data=post_data)
        request.user = self.user
        
        #check success isn't called yet
        self.assertEqual(messages.success.called, False)

        #call the add view
        view_return = ClinicalTrialAddView.as_view()(request)

        #now, the success must have a call
        self.assertEqual(messages.success.called, True)
        #check the redirect success status
        self.assertEqual(view_return.status_code, 302)

        #resolve the url redirected, to get the created pk
        created_pk = resolve(dict(view_return.items())['Location']).kwargs['pk']
        new = ClinicalTrial.objects.get(pk=created_pk)
        self.assertEqual(new.sponsor_set.all().count(), 0)

    def test_add_clinical_trial_with_self_funding(self):
        post_data = {u'language_1_mandatory': u'on',
                     u'language_2_mandatory': u'on',
                     u'accept_terms': u'on',
                     u'self_funding': u'on',
                     u'study_type': u'O'}
        request = self.factory.post(reverse(u'clinical_trial_add'),
                                    data=post_data)
        request.user = self.user
        
        #check success isn't called yet
        self.assertEqual(messages.success.called, False)

        #call the add view
        view_return = ClinicalTrialAddView.as_view()(request)

        #now, the success must have a call
        self.assertEqual(messages.success.called, True)
        #check the redirect success status
        self.assertEqual(view_return.status_code, 302)

        #resolve the url redirected, to get the created pk
        created_pk = resolve(dict(view_return.items())['Location']).kwargs['pk']
        new = ClinicalTrial.objects.get(pk=created_pk)
        self.assertEqual(new.sponsor_set.all().count(), 3)


class ClinicalTrialIdentificationViewTestCase(TestCase):

    def setUp(self):
        self.factory = RequestFactory()
        self.user = create_user()

        self.ct1 = get_clinical_trial(self.user, u'ct1')
        self.ct1.save()
        self.ct1.ctdetail_set.create()
        self.ct1.ctdetail_set.create()

        self.setup_identifier = SetupIdentifier.objects.create(issuing_body=u'abc',
                                                               mandatory=True,
                                                               mask=u'LNN')

        #mocks
        self.original_success = messages.success
        messages.success = Mock()

    def tearDown(self):

        messages.success = self.original_success
        self.setup_identifier.delete()
        self.ct1.delete()

    def test_update_identification(self):
        from .post_datas import post_data1

        post_data = post_data1.copy()
        post_data[u'form-0-id'] = self.ct1.ctdetail_set.all()[0].pk
        post_data[u'form-1-id'] = self.ct1.ctdetail_set.all()[1].pk
        post_data[u'identifier_set-0-clinical_trial'] = self.ct1.pk
        post_data[u'identifier_set-0-setup_identifier'] = self.setup_identifier.pk

        request = self.factory.post(reverse(u'clinical_trial_edit_identification', args=(self.ct1.pk,)),
                                    data=post_data)
        request.user = self.user
        request.LANGUAGE_CODE = u'pt'

        #check success isn't called yet
        self.assertEqual(messages.success.called, False)

        view_return = ClinicalTrialIdentificationView.as_view()(request, pk=self.ct1.pk)

        #now, the success must has a call
        self.assertEqual(messages.success.called, True)
        #check the redirect success status
        self.assertEqual(view_return.status_code, 302)


    def test_invalid_code_mask_validation(self):
        from .post_datas import post_data1

        post_data = post_data1.copy()
        post_data[u'form-0-id'] = self.ct1.ctdetail_set.all()[0].pk
        post_data[u'form-1-id'] = self.ct1.ctdetail_set.all()[1].pk
        post_data[u'identifier_set-0-clinical_trial'] = self.ct1.pk
        post_data[u'identifier_set-0-setup_identifier'] = self.setup_identifier.pk
        post_data[u'identifier_set-0-code'] = u'INVALID'

        request = self.factory.post(reverse(u'clinical_trial_edit_identification', args=(self.ct1.pk,)),
                                    data=post_data)
        request.user = self.user
        request.LANGUAGE_CODE = u'pt'
        #check success isn't called yet
        self.assertEqual(messages.success.called, False)

        view_return = ClinicalTrialIdentificationView.as_view()(request, pk=self.ct1.pk)

        #not called, due the mask validation error
        self.assertEqual(messages.success.called, False)
        #check the status
        self.assertEqual(view_return.status_code, 200)
        
        error = view_return.context_data['identifiers_formset'].errors[0][u'code'][0]

        self.assertEqual(error, _(u"The code didn't validade"))


    def test_initial_mandatory_data(self):
        """
        Tests the initial data for all the mandatory identifiers that aren't
        saved yet.
        """
        request = self.factory.get(
            reverse(u'clinical_trial_edit_identification', args=(self.ct1.pk,))
        )
        request.user = self.user
        request.LANGUAGE_CODE = u'pt'
        view_return = ClinicalTrialIdentificationView.as_view()(
            request,
            pk=self.ct1.pk
        )
        identifiers_formset = view_return.context_data[u'identifiers_formset']

        self.assertEqual(identifiers_formset.extra, 2)
        self.assertEqual(identifiers_formset.forms[0].\
            initial[u'setup_identifier'], self.setup_identifier.pk)
        self.assertIsNone(identifiers_formset.forms[0].instance.pk)
        created = self.ct1.identifier_set.create(
            setup_identifier=self.setup_identifier
        )

        view_return = ClinicalTrialIdentificationView.as_view()(
            request,
            pk=self.ct1.pk
        )
        identifiers_formset = view_return.context_data[u'identifiers_formset']
        self.assertEqual(identifiers_formset.forms[0].instance.pk, created.pk)

        


class ClinicalTrialSponsorsViewTestCase(TestCase):

    def setUp(self):
        self.factory = RequestFactory()
        self.user = create_user()
        self.group1 = CTGroup.objects.create(name='g1', visibility=1)
        self.ct1 = get_clinical_trial(self.user, u'ct1')
        self.ct1.group = self.group1
        self.ct1.save()
        self.ct1.ctdetail_set.create()
        self.ct1.ctdetail_set.create()

        self.ct2 = get_clinical_trial(self.user, u'ct2')
        self.ct2.group = self.group1
        self.ct2.save()
        self.ct2.ctdetail_set.create()
        self.ct2.ctdetail_set.create()

        self.country1 = Country.objects.create(description=u"Best Country", code=u"el")
        self.institutiontype1 = InstitutionType.objects.create(description=u'T1')

        self.institution1 = Institution.objects.create(name=u'abc',
                                                       status=u'A',
                                                       country=self.country1,
                                                       institution_type=self.institutiontype1)

        self.institution2 = Institution.objects.create(name=u'def',
                                                       status=u'A',
                                                       country=self.country1,
                                                       institution_type=self.institutiontype1)
        self.institution3 = Institution.objects.create(name=u'ghi',
                                                       status=u'M',
                                                       country=self.country1,
                                                       institution_type=self.institutiontype1)

        #mocks
        self.original_success = messages.success
        messages.success = Mock()

    def tearDown(self):

        messages.success = self.original_success
        self.institution1.delete()
        self.institution2.delete()
        self.group1.delete()
        self.institution3.delete()
        self.institutiontype1.delete()
        self.country1.delete()
        self.ct1.delete()
        self.ct2.delete()

    def test_update_sponsor(self):
        from .post_datas import post_data2

        post_data = post_data2.copy()
        post_data[u'sponsor_set-1-clinical_trial'] = self.ct1.pk
        post_data[u'sponsor_set-0-clinical_trial'] = self.ct1.pk
        post_data[u'sponsor_set-0-institution'] = self.institution1.pk
        post_data[u'sponsor_set-1-institution'] = self.institution2.pk
        
        
        
        request = self.factory.post(reverse(u'clinical_trial_edit_sponsors', args=(self.ct1.pk,)),
                                    data=post_data)

        request.user = self.user
        #check success isn't called yet
        self.assertEqual(messages.success.called, False)

        view_return = ClinicalTrialSponsorsView.as_view()(request, pk=self.ct1.pk)

        #now, the success must has a call
        self.assertEqual(messages.success.called, True)
        #check the redirect success status
        self.assertEqual(view_return.status_code, 302)

    def test_params_to_institution_autocomplete(self):
        """
        Test if the and parameter to the autocomplete is correct, to retrieve
        only the active institutions or the moderation institutions of the
        other clinical_trials of the same group of the current clinical_trial,
        or the institutions of the current clinical_trial.
        """
        get_params = {u'_': [u'1404824196648'],
                      u'clinical_trial_id': [self.ct1.pk],
                      u'context': [u''],
                      u'field_id': [u'a78e114fddbf708ef09f1bdc17aee34f3f17558a'],
                      u'page': [u'1'],
                      u'term': [u'an']}
        request = self.factory.get('/abc/', data=get_params)

        request.user = self.user
        #the pks must be only the 2 active institutions
        pks = [i.pk for i in InstitutionChoices().prepare_qs_params(
            request,
            'an',
            'an')['and']['pk__in']]
        
        self.assertIn(self.institution1.pk, pks)
        self.assertIn(self.institution2.pk, pks)
        self.assertNotIn(self.institution3.pk, pks)

        self.group1.institution = self.institution3
        self.group1.save()

        #now, the result must be the same, because we dont have any
        #other clinical_trials yet of the same group with the institution 3
        pks = [i.pk for i in InstitutionChoices().prepare_qs_params(
            request,
            'an',
            'an')['and']['pk__in']]
        self.assertIn(self.institution1.pk, pks)
        self.assertIn(self.institution2.pk, pks)
        self.assertNotIn(self.institution3.pk, pks)

        self.institution3.sponsor_items.create(clinical_trial=self.ct2)

        #now, the result must be the 3 institutions on list
        pks = [i.pk for i in InstitutionChoices().prepare_qs_params(
            request,
            'an',
            'an')['and']['pk__in']]
        self.assertIn(self.institution1.pk, pks)
        self.assertIn(self.institution2.pk, pks)
        self.assertIn(self.institution3.pk, pks)

        self.ct2.group = None
        self.ct2.save()

        #now, the result must be the just the 2 first institutions again
        pks = [i.pk for i in InstitutionChoices().prepare_qs_params(
            request,
            'an',
            'an')['and']['pk__in']]
        self.assertIn(self.institution1.pk, pks)
        self.assertIn(self.institution2.pk, pks)
        self.assertNotIn(self.institution3.pk, pks)

        
        self.ct1.sponsor_set.create(institution=self.institution3)

        #now, the result must be the 3 institutions, beacuse the institution3
        #is a sponsor of the current clinical trial(ct1)
        pks = [i.pk for i in InstitutionChoices().prepare_qs_params(
            request,
            'an',
            'an')['and']['pk__in']]
        self.assertIn(self.institution1.pk, pks)
        self.assertIn(self.institution2.pk, pks)
        self.assertIn(self.institution3.pk, pks)

class ClinicalTrialHealthConditionViewTestCase(TestCase):

    def setUp(self):
        self.factory = RequestFactory()
        self.user = create_user()

        self.ct1 = get_clinical_trial(self.user, u'ct1')
        self.ct1.save()
        self.ct1.ctdetail_set.create()
        self.ct1.ctdetail_set.create()

        self.vocabulary1 = Vocabulary.objects.create(name=u"V1",
                                                     version=1,
                                                     current_version=True,
                                                     usage=u'H')#HC Usage
        self.vocabularyitem1 = VocabularyItem.objects.create(code='CVI1',
                                                             description_lang_1=u'a',
                                                             vocabulary=self.vocabulary1)

        self.vocabulary2 = Vocabulary.objects.create(name=u"VOLD",
                                                     version=1,
                                                     current_version=False,
                                                     usage=u"I")#interventions usage
        self.vocabularyitem2 = VocabularyItem.objects.create(code='CVIOLD',
                                                             description_lang_1=u'a',
                                                             vocabulary=self.vocabulary2)
        
        #mocks
        self.original_success = messages.success
        messages.success = Mock()

    def tearDown(self):

        messages.success = self.original_success
        self.vocabulary1.delete()
        self.vocabularyitem1.delete()
        self.ct1.delete()

    def test_update_health_condition(self):
        from .post_datas import post_data3

        post_data = post_data3.copy()
        post_data[u'descriptor_set-0-clinical_trial'] = self.ct1.pk
        post_data[u'descriptor_set-0-vocabulary_item'] = self.vocabularyitem1.pk
        post_data[u'descriptor_set-0-parent_vocabulary'] = self.vocabularyitem1.vocabulary.pk
        post_data[u'form-0-id'] = self.ct1.ctdetail_set.all()[0].pk
        post_data[u'form-1-id'] = self.ct1.ctdetail_set.all()[1].pk
        
        
        request = self.factory.post(reverse(u'clinical_trial_edit_health_conditions', args=(self.ct1.pk,)),
                                    data=post_data)
        request.user = self.user

        #check success isn't called yet
        self.assertEqual(messages.success.called, False)

        view_return = ClinicalTrialHealthConditionView.as_view()(request, pk=self.ct1.pk)

        #now, the success must has a call
        self.assertEqual(messages.success.called, True)
        #check the redirect success status
        self.assertEqual(view_return.status_code, 302)


    def test_descriptor_type_choices(self):
        """
        Descriptor type choices must be General ou Specific(not Intervention)
        """
        request = self.factory.get(reverse(u'clinical_trial_edit_health_conditions', args=(self.ct1.pk,)),)
        request.user = self.user

        view_return = ClinicalTrialHealthConditionView.as_view()(request, pk=self.ct1.pk)
        self.assertNotIn(
            u'I',
            dict(
                view_return.context_data[u'descriptors_formset'].forms[0].fields[u'descriptor_type'].choices).keys()
            )

    def test_descriptor_list_exclude_interventions(self):
        """
        The formset queryset must not include the Interventions types.
        """

        from .post_datas import post_data3

        post_data = post_data3.copy()
        post_data[u'descriptor_set-0-clinical_trial'] = self.ct1.pk
        post_data[u'descriptor_set-0-vocabulary_item'] = self.vocabularyitem1.pk
        post_data[u'descriptor_set-0-parent_vocabulary'] = self.vocabularyitem1.vocabulary.pk
        post_data[u'form-0-id'] = self.ct1.ctdetail_set.all()[0].pk
        post_data[u'form-1-id'] = self.ct1.ctdetail_set.all()[1].pk
        
        
        request = self.factory.post(reverse(u'clinical_trial_edit_health_conditions', args=(self.ct1.pk,)),
                                    data=post_data)
        request.user = self.user

        #check success isn't called yet
        self.assertEqual(messages.success.called, False)

        view_return = ClinicalTrialHealthConditionView.as_view()(request, pk=self.ct1.pk)

        #now, the success must has a call
        self.assertEqual(messages.success.called, True)
        #check the redirect success status
        self.assertEqual(view_return.status_code, 302)


        request = self.factory.get(reverse(u'clinical_trial_edit_health_conditions', args=(self.ct1.pk,)),)
        request.user = self.user

        view_return = ClinicalTrialHealthConditionView.as_view()(request, pk=self.ct1.pk)

        self.assertIn(
            self.ct1.descriptor_set.latest(u'pk'),
            view_return.context_data[u'descriptors_formset'].queryset)

        self.ct1.descriptor_set.create(descriptor_type=u"I", vocabulary_item=self.vocabularyitem1)
        self.assertNotIn(
            self.ct1.descriptor_set.latest(u'pk'),
            view_return.context_data[u'descriptors_formset'].queryset)

    def test_descriptor_with_incorrect_parent_vocabulary_usage(self):
        """
        The formset queryset must not include the Interventions types.
        """

        from .post_datas import post_data3

        post_data = post_data3.copy()
        post_data[u'descriptor_set-0-clinical_trial'] = self.ct1.pk
        post_data[u'descriptor_set-0-vocabulary_item'] = self.vocabularyitem2.pk
        post_data[u'descriptor_set-0-parent_vocabulary'] = self.vocabularyitem2.vocabulary.pk
        post_data[u'form-0-id'] = self.ct1.ctdetail_set.all()[0].pk
        post_data[u'form-1-id'] = self.ct1.ctdetail_set.all()[1].pk
        
        
        request = self.factory.post(reverse(u'clinical_trial_edit_health_conditions', args=(self.ct1.pk,)),
                                    data=post_data)
        request.user = self.user

        #check success isn't called yet
        self.assertEqual(messages.success.called, False)

        view_return = ClinicalTrialHealthConditionView.as_view()(request, pk=self.ct1.pk)
        
        #not a call yet
        self.assertEqual(messages.success.called, False)
        #check the 200 status, due the error in the parent_vocabulary
        self.assertEqual(view_return.status_code, 200)

        self.assertTrue(
            view_return.context_data['descriptors_formset'].errors[0].has_key(
                u'parent_vocabulary'
            )
        )

       
class ClinicalTrialIntervetionViewTestCase(TestCase):

    def setUp(self):
        self.factory = RequestFactory()
        self.user = create_user()

        self.ct1 = get_clinical_trial(self.user, u'ct1')
        self.ct1.save()
        self.ctdetail1 = self.ct1.ctdetail_set.create()
        self.ctdetail2 = self.ct1.ctdetail_set.create()

        self.vocabulary1 = Vocabulary.objects.create(name=u"V1",
                                                     version=1,
                                                     current_version=True,
                                                     usage=u'I')
        self.vocabularyitem1 = VocabularyItem.objects.create(code='CVI1',
                                                             description_lang_1=u'a',
                                                             vocabulary=self.vocabulary1)

        self.vocabulary2 = Vocabulary.objects.create(name=u"VOLD",
                                                     version=1,
                                                     current_version=False,
                                                     usage=u'H')
        self.vocabularyitem2 = VocabularyItem.objects.create(code='CVIOLD',
                                                             description_lang_1=u'a',
                                                             vocabulary=self.vocabulary2)
        
        self.intervention1 = InterventionCode.objects.create(description='ia1')
        self.intervention2 = InterventionCode.objects.create(description='ia2')
        #mocks
        self.original_success = messages.success
        messages.success = Mock()

    def tearDown(self):

        messages.success = self.original_success
        self.vocabulary1.delete()
        self.vocabularyitem1.delete()
        self.ct1.delete()

    def test_update_intervention(self):
        from .post_datas import post_data4
        
        post_data = post_data4.copy()
        post_data[u'descriptor_set-0-clinical_trial'] = self.ct1.pk
        post_data[u'descriptor_set-0-vocabulary_item'] = self.vocabularyitem1.pk
        post_data[u'descriptor_set-0-parent_vocabulary'] = self.vocabularyitem1.vocabulary.pk
        post_data[u'form-0-id'] = self.ctdetail1.pk
        post_data[u'form-1-id'] = self.ctdetail2.pk
        post_data[u'intervention_codes'] = [self.intervention1.pk,
                                                 self.intervention2.pk]
        
        request = self.factory.post(
            reverse(u'clinical_trial_edit_interventions', args=(self.ct1.pk,)),
            data=post_data)
        request.user = self.user

        #check success isn't called yet
        self.assertEqual(messages.success.called, False)

        view_return = \
             ClinicalTrialInterventionView.as_view()(request, pk=self.ct1.pk)

        #now, the success must has a call
        self.assertEqual(messages.success.called, True)
        #check the redirect success status
        self.assertEqual(view_return.status_code, 302)


    def test_error_invalid_parent_vocabulary(self):
        from .post_datas import post_data4
        
        post_data = post_data4.copy()
        post_data[u'descriptor_set-0-clinical_trial'] = self.ct1.pk
        post_data[u'descriptor_set-0-vocabulary_item'] = self.vocabularyitem2.pk
        post_data[u'descriptor_set-0-parent_vocabulary'] = self.vocabularyitem2.vocabulary.pk
        post_data[u'form-0-id'] = self.ctdetail1.pk
        post_data[u'form-1-id'] = self.ctdetail2.pk
        post_data[u'intervention_codes'] = [self.intervention1.pk,
                                                 self.intervention2.pk]
        
        request = self.factory.post(
            reverse(u'clinical_trial_edit_interventions', args=(self.ct1.pk,)),
            data=post_data)
        request.user = self.user

        #check success isn't called yet
        self.assertEqual(messages.success.called, False)

        view_return = \
             ClinicalTrialInterventionView.as_view()(request, pk=self.ct1.pk)

        #not have a call yet, due the error
        self.assertEqual(messages.success.called, False)
        #check the 200 status, due the error
        self.assertEqual(view_return.status_code, 200)

        self.assertTrue(
            view_return.context_data[u'descriptors_formset'].errors[0].has_key(
                u'parent_vocabulary'
            )
        )


    def test_descriptor_list_only_interventions(self):
        """
        The formset queryset must only include the Interventions types.
        """
        from .post_datas import post_data4
        
        post_data = post_data4.copy()
        post_data[u'descriptor_set-0-clinical_trial'] = self.ct1.pk
        post_data[u'descriptor_set-0-vocabulary_item'] = self.vocabularyitem1.pk
        post_data[u'descriptor_set-0-parent_vocabulary'] = self.vocabularyitem1.vocabulary.pk
        post_data[u'form-0-id'] = self.ctdetail1.pk
        post_data[u'form-1-id'] = self.ctdetail2.pk
        post_data[u'intervention_codes'] = [self.intervention1.pk,
                                                 self.intervention2.pk]
        
        request = self.factory.post(
            reverse(u'clinical_trial_edit_interventions', args=(self.ct1.pk,)),
            data=post_data)
        request.user = self.user

        #check success isn't called yet
        self.assertEqual(messages.success.called, False)

        view_return = \
             ClinicalTrialInterventionView.as_view()(request, pk=self.ct1.pk)

        #now, the success must has a call
        self.assertEqual(messages.success.called, True)
        #check the redirect success status
        self.assertEqual(view_return.status_code, 302)


        request = self.factory.get(reverse(u'clinical_trial_edit_interventions', args=(self.ct1.pk,)),)
        request.user = self.user

        view_return = ClinicalTrialInterventionView.as_view()(request, pk=self.ct1.pk)

        #assert that the type will be automatically u'I'(Intervention)
        self.assertEqual(self.ct1.descriptor_set.latest(u'pk').descriptor_type, u'I')

        self.assertIn(
            self.ct1.descriptor_set.latest(u'pk'),
            view_return.context_data[u'descriptors_formset'].queryset)

        self.ct1.descriptor_set.create(descriptor_type=u"S", vocabulary_item=self.vocabularyitem1)
        self.assertNotIn(
            self.ct1.descriptor_set.latest(u'pk'),
            view_return.context_data[u'descriptors_formset'].queryset)


class ClinicalTrialRecruitmentViewTestCase(TestCase):

    def setUp(self):
        self.factory = RequestFactory()
        self.user = create_user()

        self.ct1 = get_clinical_trial(self.user, u'ct1')
        self.ct1.save()
        self.ctdetail1 = self.ct1.ctdetail_set.create()
        self.ctdetail2 = self.ct1.ctdetail_set.create()
        self.country1 = Country.objects.create(description=u"Best Country", code="br")
        #mocks
        self.original_success = messages.success
        messages.success = Mock()

    def tearDown(self):

        messages.success = self.original_success
        self.ct1.delete()
        self.user.delete()

    def test_update_recruitment(self):
        from .post_datas import post_data5
        
        post_data = post_data5.copy()
        post_data[u'form-0-id'] = self.ctdetail1.pk
        post_data[u'form-1-id'] = self.ctdetail2.pk
        post_data[u'recruitment_countries'] = [self.country1.pk,]
        
        request = self.factory.post(
            reverse(u'clinical_trial_edit_recruitment', args=(self.ct1.pk,)),
            data=post_data)
        request.user = self.user
        request.LANGUAGE_CODE = u'pt'
        #check success isn't called yet
        self.assertEqual(messages.success.called, False)

        view_return = \
             ClinicalTrialRecruitmentView.as_view()(request, pk=self.ct1.pk)
        #now, the success must has a call
        self.assertEqual(messages.success.called, True)
        #check the redirect success status
        self.assertEqual(view_return.status_code, 302)


class ClinicalTrialStudyTypeViewTestCase(TestCase):

    def setUp(self):
        self.factory = RequestFactory()
        self.user = create_user()

        self.ct1 = get_clinical_trial(self.user, u'ct1')
        self.ct1.save()
        self.ctdetail1 = self.ct1.ctdetail_set.create()
        self.ctdetail2 = self.ct1.ctdetail_set.create()

        self.maskingtype1 = MaskingType.objects.create(description=u'a')
        self.observation1 = ObservationStudyDesign.objects.create(description=u'a')
        self.allocation1 = AllocationType.objects.create(description=u'a')
        self.study_phase1 = StudyPhase.objects.create(description=u'a')
        self.time_perspective1 = TimePerspective.objects.create(description=u'a')
        self.study_purpose1 = StudyPurpose.objects.create(description=u'a')
        self.intervention1 = InterventionAssignment.objects.create(description=u'a')

        #mocks
        self.original_success = messages.success
        messages.success = Mock()

    def tearDown(self):

        messages.success = self.original_success
        self.ct1.delete()
        self.user.delete()
        self.maskingtype1.delete()
        self.observation1.delete()
        self.allocation1.delete()
        self.study_phase1.delete()
        self.time_perspective1.delete()
        self.study_purpose1.delete()
        self.intervention1.delete()

    def test_update_study_type(self):
        from .post_datas import post_data6
        
        post_data = post_data6.copy()
        post_data[u'form-0-id'] = self.ctdetail1.pk
        post_data[u'form-1-id'] = self.ctdetail2.pk
        post_data[u'allocation_type'] = self.allocation1.pk
        post_data[u'intervention_assignment'] = self.intervention1.pk
        post_data[u'masking_type'] = self.maskingtype1.pk
        post_data[u'observational_study_design'] = self.observation1.pk
        post_data[u'study_phase'] = self.study_phase1.pk
        post_data[u'study_purpose'] = self.study_purpose1.pk
        post_data[u'time_perspective'] = self.time_perspective1.pk
        
        request = self.factory.post(
            reverse(u'clinical_trial_edit_study_type', args=(self.ct1.pk,)),
            data=post_data)
        request.user = self.user

        #check success isn't called yet
        self.assertEqual(messages.success.called, False)

        view_return = \
             ClinicalTrialStudyTypeView.as_view()(request, pk=self.ct1.pk)

        #now, the success must has a call
        self.assertEqual(messages.success.called, True)
        #check the redirect success status
        self.assertEqual(view_return.status_code, 302)


class ClinicalTrialContactsViewTestCase(TestCase):

    def setUp(self):
        self.factory = RequestFactory()
        self.user = create_user()

        self.ct1 = get_clinical_trial(self.user, u'ct1')
        self.ct1.save()

        self.country1 = Country.objects.create(description=u"Best Country", code="br")
        self.institutiontype1 = InstitutionType.objects.create(description=u'T1')

        self.institution1 = Institution.objects.create(name=u'abc',
                                                       status=u'A',
                                                       country=self.country1,
                                                       institution_type=self.institutiontype1)

        self.contact1 = Contact.objects.create(
            institution=self.institution1,
            first_name=u'a',
            middle_name=u'a',
            last_name=u'a',
            email=u'e@e.com',
            address=u'a',
            city=u'a',
            state=u'a',
            postal_code=u'a',
            phone=u'a',
            country=self.country1)

        self.orphan_contact = Contact.objects.create(
            institution=self.institution1,
            first_name=u'orphan',
            middle_name=u'orphan',
            last_name=u'orphan',
            email=u'orphan@orphan.com',
            address=u'orphan street, CA',
            city=u'orphan',
            state=u'orphan',
            postal_code=u'orphan',
            phone=u'a',
            country=self.country1)
        
        #mocks
        self.original_success = messages.success
        messages.success = Mock()

    def tearDown(self):

        messages.success = self.original_success
        self.institution1.delete()
        self.institutiontype1.delete()
        self.contact1.delete()
        self.country1.delete()
        self.ct1.delete()

    def test_update_contacts(self):
        from .post_datas import post_data7

        post_data = post_data7.copy()
        post_data[u'ctcontact_set-1-clinical_trial'] = self.ct1.pk
        post_data[u'ctcontact_set-0-clinical_trial'] = self.ct1.pk
        post_data[u'ctcontact_set-1-contact'] = self.contact1.pk
        post_data[u'ctcontact_set-0-contact'] = self.contact1.pk
        
        
        request = self.factory.post(
            reverse(u'clinical_trial_edit_contacts', args=(self.ct1.pk,)),
            data=post_data)
        request.user = self.user

        #check success isn't called yet
        self.assertEqual(messages.success.called, False)

        #check the orphan contact is here
        self.assertEqual(
            Contact.objects.filter(pk=self.orphan_contact.pk).count(),
            1
        )
        self.ct1.ctcontact_set.create(contact=self.contact1,
                                      contact_type=CTContact.TYPE_SITE_QUERIES)
        view_return = ClinicalTrialContactsView.as_view()(request, pk=self.ct1.pk)

        #now, the success must has a call
        self.assertEqual(messages.success.called, True)
        #check the redirect success status
        self.assertEqual(view_return.status_code, 302)

        #check the orphan contact has been deleted
        self.assertEqual(
            Contact.objects.filter(pk=self.orphan_contact.pk).count(),
            0
        )



class ClinicalTrialAttachmentsViewTestCase(TestCase):

    def setUp(self):
        self.factory = RequestFactory()
        self.user = create_user()

        self.setup = Setup()
        self.setup.language_1_code_id = 167
        self.setup.language_1_mandatory = True
        self.setup.language_2_code_id = 57
        self.setup.language_2_mandatory = True
        self.setup.language_3_code_id = 61
        self.setup.language_3_mandatory = False
        self.setup.queue_distribution = 'lala'
        self.setup.current_position_qd0 = 1
        self.setup.save()

        self.ct1 = get_clinical_trial(self.user, u'ct1')
        self.ct1.save()

        self.attach1_link = Attachment.objects.create(
            setup=self.setup,
            name=u'a',
            attachment_type=u'L')
        
        self.attach2_file = Attachment.objects.create(
            setup=self.setup,
            name=u'b',
            attachment_type=u'P')
        #mocks
        self.original_success = messages.success
        messages.success = Mock()

    def tearDown(self):

        messages.success = self.original_success
        self.attach1_link.delete()
        self.attach2_file.delete()
        self.ct1.delete()

    def test_update_attachments(self):
        from .post_datas import post_data8

        post_data = post_data8.copy()
        post_data[u'ctattachment_set-1-clinical_trial'] = self.ct1.pk
        post_data[u'ctattachment_set-0-clinical_trial'] = self.ct1.pk
        post_data[u'ctattachment_set-0-attachment'] = self.attach1_link.pk
        post_data[u'ctattachment_set-1-attachment'] = self.attach1_link.pk

        
        request = self.factory.post(
            reverse(u'clinical_trial_edit_attacments', args=(self.ct1.pk,)),
            data=post_data)
        request.user = self.user

        #check success isn't called yet
        self.assertEqual(messages.success.called, False)

        view_return = ClinicalTrialAttachmentsView.as_view()(request, pk=self.ct1.pk)

        #now, the success must has a call
        self.assertEqual(messages.success.called, True)
        #check the redirect success status
        self.assertEqual(view_return.status_code, 302)


    def test_validation_link_nor_file(self):
        from .post_datas import post_data8

        post_data = post_data8.copy()
        post_data[u'ctattachment_set-1-clinical_trial'] = self.ct1.pk
        post_data[u'ctattachment_set-0-clinical_trial'] = self.ct1.pk
        post_data[u'ctattachment_set-0-attachment'] = self.attach1_link.pk
        post_data[u'ctattachment_set-1-attachment'] = self.attach2_file.pk
        post_data[u'ctattachment_set-0-link'] = ''
        
        request = self.factory.post(
            reverse(u'clinical_trial_edit_attacments', args=(self.ct1.pk,)),
            data=post_data)
        request.user = self.user

        #check success isn't called yet
        self.assertEqual(messages.success.called, False)

        view_return = ClinicalTrialAttachmentsView.as_view()(request, pk=self.ct1.pk)

        #check success isn't called because a form error must be ocurred
        self.assertEqual(messages.success.called, False)
        #check the 200 status, of the same screen with the error
        self.assertEqual(view_return.status_code, 200)
        error = view_return.context_data['attachment_formset']\
                                                  .errors[0][u'__all__'][0]

        self.assertEqual(
            error,
            _(u'Please provide a link.'))

        error2 = view_return.context_data['attachment_formset']\
                                                  .errors[1][u'__all__'][0]

        self.assertEqual(
            error2,
            _(u'Please provide a file.'))


class ClinicalTrialCreateContactForCT(TestCase):

    def setUp(self):
        self.factory = RequestFactory()
        self.user = create_user()

        self.setup = Setup()
        self.setup.language_1_code_id = 167
        self.setup.language_1_mandatory = True
        self.setup.language_2_code_id = 57
        self.setup.language_2_mandatory = True
        self.setup.language_3_code_id = 61
        self.setup.language_3_mandatory = False
        self.setup.queue_distribution = 'lala'
        self.setup.current_position_qd0 = 1
        self.setup.save()

        self.ct1 = get_clinical_trial(self.user, u'ct1')
        self.ct1.save()
        self.country1 = Country.objects.create(description=u"Best Country", code="br")
        self.institutiontype1 = InstitutionType.objects.create(description=u'T1')

        self.institution1 = Institution.objects.create(name=u'abc',
                                                       status=u'A',
                                                       country=self.country1,
                                                       institution_type=self.institutiontype1)

        #mocks
        self.original_success = messages.success
        messages.success = Mock()

    def tearDown(self):

        messages.success = self.original_success
        self.ct1.delete()
        self.institution1.delete()
        self.country1.delete()
        self.institutiontype1.delete()

    def test_create_contact(self):
        
        post_data = {}
        post_data[u'address'] =[u'contactX']
        post_data[u'city'] = [u'contactX']
        post_data[u'email'] = [u'contactX@rebecabc.com']
        post_data[u'first_name'] = [u'contactX']
        post_data[u'institution'] = [self.institution1.pk]
        post_data[u'last_name'] = [u'contactX']
        post_data[u'middle_name'] = [u'contactX']
        post_data[u'phone'] = [u'+555430998899']
        post_data[u'postal_code'] = [u'contactX']
        post_data[u'state'] = [u'contactX']
        post_data[u'country'] = [self.country1.pk]

        
        request = self.factory.post(
            reverse(u'clinical_trial_add_contact', args=(self.ct1.pk, u'W')),
            data=post_data)
        request.user = self.user

        #check success isn't called yet
        self.assertEqual(messages.success.called, False)

        #ensure that the Clinical trial has no contacts yet
        self.assertEqual(
            self.ct1.ctcontact_set.count(),
            0
        )

        view_return = ClinicalTrialCreateContact.as_view()(
                        request,
                        clinical_trial_pk=self.ct1.pk,
                        contact_type=u'W'
                      )

        #now, the success must has a call
        self.assertEqual(messages.success.called, True)
        #check the redirect success status
        self.assertEqual(view_return.status_code, 302)
        
        #ensure that contact has been associated with the Clinical Trial
        self.assertEqual(
            self.ct1.ctcontact_set.count(),
            1
        )

    def test_invalid_phone_number(self):
        
        post_data = {}
        post_data[u'address'] =[u'contactX']
        post_data[u'city'] = [u'contactX']
        post_data[u'email'] = [u'contactX@rebecabc.com']
        post_data[u'first_name'] = [u'contactX']
        post_data[u'institution'] = [self.institution1.pk]
        post_data[u'last_name'] = [u'contactX']
        post_data[u'middle_name'] = [u'contactX']
        post_data[u'phone'] = [u'+55543098899']#it's invalid, 1 character left
        post_data[u'postal_code'] = [u'contactX']
        post_data[u'state'] = [u'contactX']

        
        request = self.factory.post(
            reverse(u'clinical_trial_add_contact', args=(self.ct1.pk, u'W')),
            data=post_data)

        #check success isn't called yet
        self.assertEqual(messages.success.called, False)

        #ensure that the Clinical trial has no contacts yet
        self.assertEqual(
            self.ct1.ctcontact_set.count(),
            0
        )

        view_return = ClinicalTrialCreateContact.as_view()(
                        request,
                        clinical_trial_pk=self.ct1.pk,
                        contact_type=u'W'
                      )

        #must not be called, due the phone number error message
        self.assertEqual(messages.success.called, False)
        #check the status
        self.assertEqual(view_return.status_code, 200)

        #ensure the form has the error key for the phone number,
        #not matter the message text
        self.assertTrue(view_return.context_data['form'].errors.has_key(
            u'phone')
        )
        

class ClinicalTrialOutcomesViewTestCase(TestCase):

    def setUp(self):
        self.factory = RequestFactory()
        self.user = create_user()

        self.setup = Setup()
        self.setup.pk=1
        self.setup.language_1_code_id = 167
        self.setup.language_1_mandatory = True
        self.setup.language_2_code_id = 57
        self.setup.language_2_mandatory = True
        self.setup.language_3_code_id = 61
        self.setup.language_3_mandatory = False
        self.setup.queue_distribution = 'lala'
        self.setup.current_position_qd0 = 1
        self.setup.save()

        self.ct1 = get_clinical_trial(self.user, u'ct1')
        self.ct1.save()

       
        #mocks
        self.original_success = messages.success
        messages.success = Mock()

    def tearDown(self):

        messages.success = self.original_success
        self.setup.delete()
        self.ct1.delete()

    def test_update_outcomes(self):
        from .post_datas import post_data9

        post_data = post_data9.copy()
        post_data[u'outcome_set-0-clinical_trial'] = self.ct1.pk
        post_data[u'outcome_set-1-clinical_trial'] = self.ct1.pk
        post_data[u'outcome_set-2-clinical_trial'] = self.ct1.pk

        request = self.factory.post(
            reverse(u'clinical_trial_edit_outcomes', args=(self.ct1.pk,)),
            data=post_data)
        request.user = self.user

        #check success isn't called yet
        self.assertEqual(messages.success.called, False)

        view_return = ClinicalTrialOutcomesView.as_view()(request, pk=self.ct1.pk)

        #now, the success must has a call
        self.assertEqual(messages.success.called, True)
        #check the redirect success status
        self.assertEqual(view_return.status_code, 302)


    def test_get_form_class_just_necessaries_description_langs(self):
        from .post_datas import post_data9

        post_data = post_data9.copy()
        post_data[u'outcome_set-0-clinical_trial'] = self.ct1.pk

        #none to intentionally get an error
        post_data[u'outcome_set-1-clinical_trial'] = None

        request = self.factory.post(
            reverse(u'clinical_trial_edit_outcomes', args=(self.ct1.pk,)),
            data=post_data)
        request.user = self.user


        #with langs 1 and 2 mandatories, check th form fields
        view_return = ClinicalTrialOutcomesView.as_view()(request, pk=self.ct1.pk)

        self.assertIn(u'description_lang_1', view_return.context_data['outcomes_formset'].forms[0].fields.keys())
        self.assertIn(u'description_lang_2', view_return.context_data['outcomes_formset'].forms[0].fields.keys())
        self.assertNotIn(u'description_lang_3', view_return.context_data['outcomes_formset'].forms[0].fields.keys())

        self.ct1.language_3_mandatory = True
        self.ct1.save()

        #with all langs mandatories, check th form fields
        view_return = ClinicalTrialOutcomesView.as_view()(request, pk=self.ct1.pk)
        
        self.assertIn(u'description_lang_1', view_return.context_data['outcomes_formset'].forms[0].fields.keys())
        self.assertIn(u'description_lang_2', view_return.context_data['outcomes_formset'].forms[0].fields.keys())
        self.assertIn(u'description_lang_3', view_return.context_data['outcomes_formset'].forms[0].fields.keys())


class ClinicalGroupManagementViewTestCase(TestCase):

    def setUp(self):
        self.factory = RequestFactory()
        self.user = create_user()

        self.setup = Setup()
        self.setup.language_1_code_id = 167
        self.setup.language_1_mandatory = True
        self.setup.language_2_code_id = 57
        self.setup.language_2_mandatory = True
        self.setup.language_3_code_id = 61
        self.setup.language_3_mandatory = False
        self.setup.queue_distribution = 'lala'
        self.setup.current_position_qd0 = 1
        self.setup.save()

        self.ct1 = get_clinical_trial(self.user, u'ct1')
        self.ct1.save()

        self.group1 = CTGroup.objects.create(name='g1', visibility=1)
        self.group2 = CTGroup.objects.create(name='g2', visibility=1)
        self.group3 = CTGroup.objects.create(name='g3', visibility=1)
        self.user.ctgroups.create(ct_group=self.group2,
                                  status=CTGroupUser.STATUS_ACTIVE)
        #mocks
        self.original_success = messages.success
        messages.success = Mock()

    def tearDown(self):

        messages.success = self.original_success
        self.setup.delete()
        self.ct1.delete()

    def test_update_group(self):

        post_data = {}
        post_data[u'group'] = self.group2.pk

        request = self.factory.post(
            reverse(u'clinical_trial_group_manage', args=(self.ct1.pk,)),
            data=post_data)
        request.user = self.user

        #check success isn't called yet
        self.assertEqual(messages.success.called, False)

        view_return = ClinicalTrialGroupManagementView.as_view()(request, pk=self.ct1.pk)
        
        #now, the success must has a call
        self.assertEqual(messages.success.called, True)
        #check the redirect success status
        self.assertEqual(view_return.status_code, 302)

    def test_options(self):
        """
        Just show the user's active groups.
        """

        request = self.factory.get(
            reverse(u'clinical_trial_group_manage', args=(self.ct1.pk,)),
            )
        request.user = self.user

        view_return = ClinicalTrialGroupManagementView.as_view()(request, pk=self.ct1.pk)
        
        self.assertIn(
            self.group2,
            view_return.context_data['form'].fields[u'group'].queryset
        )
        self.assertNotIn(
            self.group1,
            view_return.context_data['form'].fields[u'group'].queryset
        )
        self.assertNotIn(
            self.group3,
            view_return.context_data['form'].fields[u'group'].queryset
        )

class ClinicalTrialAdminManagementTestCase(TestCase):

    def setUp(self):

        self.factory = RequestFactory()
        self.user1 = User.objects.create(username=u'e',
                                        email=u'e@e.com',
                                        gender=1,
                                        race=1,
                                        country_id=u'BR')

        self.user1.groups.add(Group.objects.get(name=u'registrant'))
        self.user2 = User.objects.create(username=u'a',
                                        email=u'a@a.com',
                                        gender=1,
                                        race=1,
                                        country_id=u'BR')
        self.user3 = User.objects.create(username=u'f',
                                        email=u'f@f.com',
                                        gender=1,
                                        race=1,
                                        country_id=u'BR')

        self.group1 = CTGroup.objects.create(name='g1', visibility=1)
        self.group2 = CTGroup.objects.create(name='g2', visibility=1)
        self.group3 = CTGroup.objects.create(name='g3', visibility=1)
        self.user1.ctgroups.create(type_user=CTGroupUser.TYPE_ADMINISTRATOR,
                                   ct_group=self.group1,
                                   status=CTGroupUser.STATUS_ACTIVE)

        self.setup = Setup()
        self.setup.language_1_code_id = 167
        self.setup.language_1_mandatory = True
        self.setup.language_2_code_id = 57
        self.setup.language_2_mandatory = True
        self.setup.language_3_code_id = 61
        self.setup.language_3_mandatory = False
        self.setup.queue_distribution = 'lala'
        self.setup.current_position_qd0 = 1
        self.setup.save()

        self.ct1 = get_clinical_trial(self.user1, u'ct1')
        self.ct1.group=self.group1
        self.ct1.save()

        self.ct2 = get_clinical_trial(self.user3, u'ct2')
        self.ct2.group=self.group1
        self.ct2.save()

        self.ct3 = get_clinical_trial(self.user3, u'ct2')
        self.ct3.group=self.group2
        self.ct3.save()

        self.post_data = {u'form-0-group': [self.group1.pk],
                          u'form-0-id': [self.ct2.pk],
                          u'form-0-user': [self.user3.pk],
                          u'form-1-group': [u''],
                          u'form-1-id': [self.ct1.pk],
                          u'form-1-user': [self.user2.pk],
                          u'form-INITIAL_FORMS': [u'2'],
                          u'form-MAX_NUM_FORMS': [u'1000'],
                          u'form-TOTAL_FORMS': [u'2']}

         
        #mocks
        self.original_success = messages.success
        messages.success = Mock()

    def tearDown(self):

        messages.success = self.original_success
        self.group1.users.all().delete()
        self.group1.delete()
        self.group2.users.all().delete()
        self.group2.delete()
        self.group3.users.all().delete()
        self.group3.delete()
        self.ct1.delete()
        self.ct2.delete()
        self.ct3.delete()
        self.user1.delete()
        self.user2.delete()
        self.user3.delete()
        

    def test_update_cts_data(self):
        
        request = self.factory.post(
            reverse(u'clinical_trial_admin_manage'),
            data=self.post_data)
        request.user = self.user1
        #check success isn't called yet
        self.assertEqual(messages.success.called, False)

        view_return = ClinicalTrialManageView.as_view()(request)

        #now, the success must has a call
        self.assertEqual(messages.success.called, True)
        #check the redirect success status
        self.assertEqual(view_return.status_code, 302)

        self.assertEqual(
            ClinicalTrial.objects.get(pk=self.ct1.pk).user,
            self.user2
            )
        self.assertEqual(
            ClinicalTrial.objects.get(pk=self.ct2.pk).user,
            self.user3
            )
        self.assertIsNone(
            ClinicalTrial.objects.get(pk=self.ct1.pk).group
            )
        self.assertEqual(
            ClinicalTrial.objects.get(pk=self.ct2.pk).group,
            self.group1
            )
       
    def test_filters(self):
        """
        Tests the GET filters
        """
        
        #GET filter for user
        request = self.factory.get(
            reverse(u'clinical_trial_admin_manage')+\
                u'?user=%s' % self.user1.pk)
        request.user = self.user1
        view_return = ClinicalTrialManageView.as_view()(request)
        
        self.assertEqual(len(view_return.context_data[u'formset'].forms), 1)

        
        #test for the group GET parameter
        request = self.factory.get(
            reverse(u'clinical_trial_admin_manage')+\
                u'?group=%s' % self.group1.pk)
        request.user = self.user1
        view_return = ClinicalTrialManageView.as_view()(request)

        #show the 2 results, the two cts that the user can manage
        self.assertEqual(len(view_return.context_data[u'formset'].forms), 2)

        #set a group for the clinical_trial 1
        self.ct1.group = self.group1
        self.ct1.save()

        view_return = ClinicalTrialManageView.as_view()(request)

        #now, a result, correct
        self.assertEqual(len(view_return.context_data[u'formset'].forms), 2)

        #test the user AND the group
        request = self.factory.get(
            reverse(u'clinical_trial_admin_manage')\
                +u'?group=%s&user=%s' % (
                    self.group1.pk,
                    self.user2.pk))
        request.user = self.user1
        view_return = ClinicalTrialManageView.as_view()(request)

        #no results
        self.assertEqual(len(view_return.context_data[u'formset'].forms), 0)

        #now, with a group that haves a correct user match
        request = self.factory.get(
            reverse(u'clinical_trial_admin_manage')+\
                u'?group=%s&user=%s' % (
                    self.group1.pk,
                    self.user1.pk))
        request.user = self.user1
        view_return = ClinicalTrialManageView.as_view()(request)

        #one result, correct
        self.assertEqual(len(view_return.context_data[u'formset'].forms), 1)

        #now, with ct id
        request = self.factory.get(
            reverse(u'clinical_trial_admin_manage')+\
                u'?clinical_trial=%s' % self.ct1.id)
        request.user = self.user1
        view_return = ClinicalTrialManageView.as_view()(request)

        #one result, correct
        self.assertEqual(len(view_return.context_data[u'formset'].forms), 1)


        

        request = self.factory.get(
            reverse(u'clinical_trial_admin_manage'))
        request.user = self.user1
        view_return = ClinicalTrialManageView.as_view()(request)

        #the two cts that the user can manage
        self.assertEqual(len(view_return.context_data[u'formset'].forms), 2)

        self.ct3.group = self.group1
        self.ct3.save()

        view_return = ClinicalTrialManageView.as_view()(request)

        #now the 3 cts that the user can manage
        self.assertEqual(len(view_return.context_data[u'formset'].forms), 3)


        self.user1.ctgroups.update(status=CTGroupUser.STATUS_PENDING_APROV_ADM)

        view_return = ClinicalTrialManageView.as_view()(request)

        #now, none will be listed
        self.assertEqual(len(view_return.context_data[u'formset'].forms), 0)

        def test_access_permission(self):
            """Verify Institution moderation permission"""
            request = self.factory.get(reverse_lazy(
                'clinical_trial_admin_manage'))
            request.user = self.user1
            response = ClinicalTrialManageView.as_view()(request)
            self.assertEqual(response.status_code, 200)
            request.user = self.user2
            self.assertRaises(
                PermissionDenied, ClinicalTrialManageView.as_view(),
                request)

class ClinicalTrialIndexViewTestCase(TestCase):
    """
    Test Case for the functions of the clinical trial resume.
    """

    def setUp(self):
        self.factory = RequestFactory()
        self.user1 = create_user()

        self.setup = Setup()
        self.setup.language_1_code_id = 167
        self.setup.language_1_mandatory = True
        self.setup.language_2_code_id = 57
        self.setup.language_2_mandatory = True
        self.setup.language_3_code_id = 61
        self.setup.language_3_mandatory = False
        self.setup.queue_distribution = 'lala'
        self.setup.current_position_qd0 = 1
        self.setup.save()

        self.ct1 = get_clinical_trial(self.user1, u'ct1')
        self.ct1.save()

        
        self.attachment_non_req = Attachment.objects.create(required=False)


    def tearDown(self):

        self.user1.delete()
        self.ct1.delete()

    
class ClinicalTrialRequiredFieldsValidationTestCase(TestCase):

    def setUp(self):
        self.user = create_user()
        self.ct = get_clinical_trial(user=self.user, code=u'CT1')
        self.ct.save()
        
        #create the ctdetails
        self.ctdetail1 = self.ct.ctdetail_set.create()
        self.ctdetail2 = self.ct.ctdetail_set.create()

        self.country = Country.objects.create(description=u"Best Country", code=u"el")
        self.institutiontype = InstitutionType.objects.create()
        self.institution = Institution.objects.create(
            institution_type=self.institutiontype,
            country=self.country)
        self.contact = Contact.objects.create(
            institution=self.institution,
            phone=u'123',
            country=self.country
                )
        self.setup_identifier1 = SetupIdentifier.objects.create(mandatory=True)
        self.setup_identifier2 = SetupIdentifier.objects.create(mandatory=False)

        self.voc = Vocabulary.objects.create()
        self.voc_item = self.voc.vocabulary_items.create()
        self.observational_study_design = ObservationStudyDesign.objects.create()
        self.time_perspective = TimePerspective.objects.create()

    def tearDown(self):
        self.voc_item.delete()
        self.voc.delete()
        self.ct.ctdetail_set.all().delete()
        self.ct.intervention_codes.all().delete()
        self.institution.delete()
        self.ct.recruitment_countries.all().delete()
        self.ct.delete()
        self.user.delete()
        self.setup_identifier1.delete()
        self.setup_identifier2.delete()
        self.observational_study_design.delete()
        self.time_perspective.delete()


    def __do_test_fields_for_2_languages(self, ctdetail_fields, validation_fname):
        #stores the number of mandatory fields
        number_fields = len(ctdetail_fields)
        #for each field name
        for cont, field_name in enumerate(ctdetail_fields):
            #check that this step dont validate well
            self.assertFalse(getattr(self.ct, validation_fname)()[0])
            setattr(self.ctdetail1, field_name, u'a')
            self.ctdetail1.save()
            self.assertFalse(getattr(self.ct, validation_fname)()[0])
            setattr(self.ctdetail2, field_name, u'b')
            self.ctdetail2.save()

            #After set values to the current field name, for all the mandatory
            #languages, check if this step is valid already.
            #It must be valid just when all the fields have
            #been fill(i.e: the last loop iteration.)
            if cont == (number_fields-1):
                self.assertTrue(getattr(self.ct, validation_fname)()[0])
            else:
                self.assertFalse(getattr(self.ct, validation_fname)()[0])

    def test_identification_validation(self):
        """
        Check the validation of the ctdetails information
        in all mandatory languages.
        """
        #define all the mandatory fields to all languages
        ctdetail_fields = (u'scientific_title', u'scientific_acronym',
                           u'scientific_acronym_expansion', u'public_title')
        
        self.ct.identifier_set.create(
            setup_identifier=SetupIdentifier.objects.get(mandatory=True)
        )
        self.__do_test_fields_for_2_languages(ctdetail_fields,
                                              u'check_identification_step')

        #Check the validation of all required setup identifiers
        self.ct.identifier_set.all().delete()
        self.assertFalse(self.ct.check_identification_step()[0])
        self.ct.identifier_set.create(
            setup_identifier=SetupIdentifier.objects.get(mandatory=True)
        )
        self.assertTrue(self.ct.check_identification_step()[0])
        self.ct.identifier_set.all().delete()

    def test_sponsors_validation(self):
        
        country = Country.objects.create(description=u"Best Country", code=u"zz")
        itype = InstitutionType.objects.create()
        institution = Institution.objects.create(country=country,
                                                 institution_type=itype)
        from clinical_trials.models import Sponsor
        self.assertFalse(self.ct.check_sponsors_step()[0])
        self.ct.sponsor_set.create(institution=institution,
                                   sponsor_type=Sponsor.SPONSOR_TYPE_PRIMARY)
        self.assertFalse(self.ct.check_sponsors_step()[0])
        self.ct.sponsor_set.create(institution=institution,
                                   sponsor_type=Sponsor.SPONSOR_TYPE_SECUNDARY)
        self.assertFalse(self.ct.check_sponsors_step()[0])
        self.ct.sponsor_set.create(institution=institution,
                                   sponsor_type=Sponsor.SPONSOR_TYPE_SUPPORT)
        self.assertTrue(self.ct.check_sponsors_step()[0])
        self.ct.sponsor_set.all().delete()
        institution.delete()
        country.delete()


    def test_health_conditions_validation(self):
        """
        Check the validation of the ctdetails information
        in all mandatory languages.
        """
        #define all the mandatory fields to all languages
        ctdetail_fields = (u'health_condition',)
        
        self.ct.descriptor_set.create(
            descriptor_type=Descriptor.TYPE_GENERAL,
            vocabulary_item=self.voc_item
        )
        self.ct.descriptor_set.create(
            descriptor_type=Descriptor.TYPE_SPECIFIC,
            vocabulary_item=self.voc_item
        )
        
        self.__do_test_fields_for_2_languages(ctdetail_fields,
                                              u'check_health_conditions_step')

    def test_interventions_validation(self):
        """
        Check the validation of the ctdetails information
        in all mandatory languages.
        """
        #define all the mandatory fields to all languages
        ctdetail_fields = (u'intervention',)
        self.ct.intervention_codes.create()
        self.ct.descriptor_set.create(
            descriptor_type=Descriptor.TYPE_INTERVENTION,
            vocabulary_item=self.voc_item
        )
        self.__do_test_fields_for_2_languages(ctdetail_fields,
                                              u'check_interventions_step')

    def test_recruitment_validation(self):
        """
        Check the validation of the ctdetails information
        in all mandatory languages.
        """
        #define all the mandatory fields to all languages
        ctdetail_fields = (u'inclusion_criteria', u'exclusion_criteria')
        self.ct.date_first_enrollment = datetime.now().date()
        self.ct.date_last_enrollment = datetime.now().date()
        self.ct.target_sample_size = 10
        self.ct.gender = 1
        self.ct.minimum_age_no_limit = True
        self.ct.maximum_age_no_limit = True
        self.ct.save()
        self.ct.recruitment_countries.add(self.country)
        self.__do_test_fields_for_2_languages(ctdetail_fields,
                                              u'check_recruitment_step')


    def test_study_type_validation(self):
        """
        Check the validation of the ctdetails information
        in all mandatory languages.
        """
        #define all the mandatory fields to all languages
        ctdetail_fields = (u'study_design',)
        self.ct.study_type = u'S'
        self.ct.observational_study_design = self.observational_study_design
        self.ct.time_perspective = self.time_perspective
        self.ct.number_arms = 2
        self.ct.save()
        self.__do_test_fields_for_2_languages(ctdetail_fields,
                                              u'check_study_type_step')

    def test_outcomes_validation(self):
        """
        Check the validation of at least one primary and one secondary
        outcome.
        """
        #must be False, because we dont have any outcome yet
        self.assertFalse(self.ct.check_outcomers_step()[0])

        #create a secondary one and test again
        self.ct.outcome_set.create(outcome_type=Outcome.TYPE_SECONDARY)
        self.assertFalse(self.ct.check_outcomers_step()[0])
        self.assertFalse(self.ct.check_outcomers_step()[0])

        #delete the secondary one and create just a primary one and test again
        self.ct.outcome_set.all().delete()
        self.ct.outcome_set.create(outcome_type=Outcome.TYPE_PRIMARY,
            description_lang_1='a',
            description_lang_2='b',
            description_lang_3='c'
        )
        self.assertTrue(self.ct.check_outcomers_step()[0])

        #now, create a secondary one again and test the success of validation
        self.ct.outcome_set.create(outcome_type=Outcome.TYPE_SECONDARY,
            description_lang_1='a',
            description_lang_2='b',
            description_lang_3='c'
        )
        self.assertTrue(self.ct.check_outcomers_step()[0])

        #check for error on validation when its just secondary ones
        self.ct.outcome_set.all().update(outcome_type=Outcome.TYPE_SECONDARY)
        self.assertFalse(self.ct.check_outcomers_step()[0])


    def test_ctcontacts_validation(self):
        """
        Check the validation of at least one primary and one secondary
        outcome.
        """
        #must be False, because we dont have any ctcontact
        self.assertFalse(self.ct.check_contacts_step()[0])
        
        #create a public queries type one and test again
        self.ct.ctcontact_set.create(
            contact_type=CTContact.TYPE_PUBLIC_QUERIES,
            contact=self.contact
            )
        self.assertFalse(self.ct.check_contacts_step()[0])

        #create a scientific queries type one and test again
        self.ct.ctcontact_set.create(
            contact_type=CTContact.TYPE_SCIENTIFIC_QUERIES,
            contact=self.contact
            )
        self.assertFalse(self.ct.check_contacts_step()[0])

        #create a site queries type one and test again, now test the success
        self.ct.ctcontact_set.create(
            contact_type=CTContact.TYPE_SITE_QUERIES,
            contact=self.contact
            )
        self.assertTrue(self.ct.check_contacts_step()[0])

    def test_attachments_validation(self):
        """
        Check the validation of all required attachments.
        """
        #check when has not required attachments, must be True
        self.assertTrue(self.ct.check_attachments_step()[0])

        #create a required one
        self.attachment_non_req = Attachment.objects.create(required=False)

        #create a required one
        self.attachment_req = Attachment.objects.create(required=True)

        #create a ctattachment to the non-required one
        self.ct.ctattachment_set.create(attachment=self.attachment_non_req)

        #check the status
        self.assertFalse(self.ct.check_attachments_step()[0])

        #create a ctattachment to the required one
        self.ct.ctattachment_set.create(attachment=self.attachment_req)
        self.assertTrue(self.ct.check_attachments_step()[0])


class ClinicalTrialWorkflowTransitionsTestCase(TestCase):
    
    def setUp(self):
        self.user = create_user()
        self.user2 = User.objects.create(username=u'a',
                                        email=u'a@a.com',
                                        gender=1,
                                        race=1,
                                        country_id=u'BR')
        self.user_reviser = User.objects.create(username=u'rev',
                                        email=u'rev@rev.com',
                                        gender=1,
                                        race=1,
                                        country_id=u'BR')
        self.user_reviser.groups.add(Group.objects.get(name=u'reviser'))
        self.user_reviser.save()
        self.ct = get_clinical_trial(user=self.user, code=u'CT1')
        self.ct.save()
        
        #create the ctdetails
        self.ctdetail1 = self.ct.ctdetail_set.create()
        self.ctdetail2 = self.ct.ctdetail_set.create()

        self.country = Country.objects.create(description=u"Best Country", code=u"el")
        self.institutiontype = InstitutionType.objects.create()
        
        self.institution = Institution.objects.create(
            institution_type=self.institutiontype,
            country=self.country,)
        self.check_steps_original = ClinicalTrial.check_all_steps_valid 
        self.factory = RequestFactory()

        #mocks
        self.original_success = messages.success
        self.original_error = messages.error
        messages.success = Mock()
        messages.error = Mock()

    def tearDown(self):
        self.ct.ctdetail_set.all().delete()
        self.ct.intervention_codes.all().delete()
        self.ct.recruitment_countries.all().delete()
        self.ct.delete()
        self.user.stateobjecthistory_set.all().delete()
        self.user.delete()
        self.user2.delete()
        self.user_reviser.delete()
        ClinicalTrial.check_all_steps_valid = self.check_steps_original
        messages.success = self.original_success
        messages.error = self.original_error


    def test_do_transition_concluir_preenchimento(self):
        request = self.factory.post(
                    reverse('clinical_trial_edit_list', args=(self.ct.pk,)),
                    data={'complete_filling': u'yes'}
                  )
        request.user = self.user
        ClinicalTrial.check_all_steps_valid = Mock(return_value=True)
        self.assertEqual(utils.get_state(self.ct).pk, State.STATE_EM_PREENCHIMENTO)
        self.user2.groups.add(Group.objects.get(name='reviser'))
        ClinicalTrialEditListView.as_view()(request, pk=self.ct.pk)
        self.assertEqual(utils.get_state(self.ct).pk, State.STATE_AGUARDANDO_REVISAO)

    def test_do_transition_avaliar_preenchimento(self):
        request = self.factory.post(
                    reverse('clinical_trial_edit_list', args=(self.ct.pk,)),
                    data={'complete_filling': u'yes'}
                  )
        request.user = self.user
        ClinicalTrial.check_all_steps_valid = Mock(return_value=True)
        self.assertEqual(utils.get_state(self.ct).pk, State.STATE_EM_PREENCHIMENTO)
        self.ct.sponsor_set.create(institution=self.institution)
        ClinicalTrialEditListView.as_view()(request, pk=self.ct.pk)
        self.assertEqual(utils.get_state(self.ct).pk, State.STATE_AGUARDANDO_MODERACAO)

    def test_do_transition_concluir_preenchimento_para_avaliacao(self):
        request = self.factory.post(
                    reverse('clinical_trial_edit_list', args=(self.ct.pk,)),
                    data={'complete_filling': u'yes'}
                  )
        #from IPython import embed;embed()
        request.user = self.user
        ClinicalTrial.check_all_steps_valid = Mock(return_value=True)
        self.assertEqual(utils.get_state(self.ct).pk, State.STATE_EM_PREENCHIMENTO)
        self.user2.groups.add(Group.objects.get(name='evaluator'))
        ClinicalTrialEditListView.as_view()(request, pk=self.ct.pk)
        self.assertEqual(utils.get_state(self.ct).pk, State.STATE_AGUARDANDO_AVALIACAO)

    def test_do_transition_reenviar_a_revisao(self):
        request = self.factory.post(
                    reverse('clinical_trial_edit_list', args=(self.ct.pk,)),
                    data={'resend_to_revision': u'yes'}
                  )
        request.user = self.user
        ClinicalTrial.check_all_steps_valid = Mock(return_value=True)
        utils.set_state(self.ct, State.objects.get(pk=State.STATE_RESUBMETIDO))
        self.assertEqual(utils.get_state(self.ct).pk, State.STATE_RESUBMETIDO)
        self.user2.groups.add(Group.objects.get(name='reviser'))
        ClinicalTrialEditListView.as_view()(request, pk=self.ct.pk)
        self.assertEqual(utils.get_state(self.ct).pk, State.STATE_EM_REVISAO)

    def test_do_transition_revisar_preenchimento(self):
        request = self.factory.post(
                    reverse('clinical_trial_edit_list', args=(self.ct.pk,)),
                    data={'finish_filling_revision': u'yes'}
                  )
        request.user = self.user
        ClinicalTrial.check_all_steps_valid = Mock(return_value=True)
        utils.set_state(self.ct, State.objects.get(
            pk=State.STATE_REVISANDO_PREENCHIMENTO)
        )
        self.assertEqual(
            utils.get_state(self.ct).pk,
            State.STATE_REVISANDO_PREENCHIMENTO
        )
        self.user2.groups.add(Group.objects.get(name='reviser'))
        ClinicalTrialEditListView.as_view()(request, pk=self.ct.pk)
        self.assertEqual(
            utils.get_state(self.ct).pk,
            State.STATE_AGUARDANDO_REVISAO
        )

    def test_do_transition_revisar_preenchimento2(self):
        request = self.factory.post(
                    reverse('clinical_trial_edit_list', args=(self.ct.pk,)),
                    data={'finish_filling_revision': u'yes'}
                  )
        request.user = self.user
        ClinicalTrial.check_all_steps_valid = Mock(return_value=True)
        utils.set_state(self.ct, State.objects.get(
            pk=State.STATE_REVISANDO_PREENCHIMENTO)
        )
        self.assertEqual(
            utils.get_state(self.ct).pk,
            State.STATE_REVISANDO_PREENCHIMENTO
        )
        self.user2.groups.add(Group.objects.get(name='evaluator'))
        ClinicalTrialEditListView.as_view()(request, pk=self.ct.pk)
        self.assertEqual(
            utils.get_state(self.ct).pk,
            State.STATE_AGUARDANDO_AVALIACAO
        )

    def test_do_transition_revisar_preenchimento3(self):
        request = self.factory.post(
                    reverse('clinical_trial_edit_list', args=(self.ct.pk,)),
                    data={'finish_filling_revision': u'yes'}
                  )
        request.user = self.user
        ClinicalTrial.check_all_steps_valid = Mock(return_value=True)
        utils.set_state(self.ct, State.objects.get(
            pk=State.STATE_REVISANDO_PREENCHIMENTO)
        )
        self.assertEqual(
            utils.get_state(self.ct).pk,
            State.STATE_REVISANDO_PREENCHIMENTO
        )
        self.ct.sponsor_set.create(institution=self.institution)
        ClinicalTrialEditListView.as_view()(request, pk=self.ct.pk)
        self.assertEqual(
            utils.get_state(self.ct).pk,
            State.STATE_AGUARDANDO_MODERACAO
        )


    def test_do_transition_concluir_avaliacao(self):
        request = self.factory.post(
                    reverse('clinical_trial_edit_list', args=(self.ct.pk,)),
                    data={'finish_evaluation': u'yes'}
                  )
        request.user = self.user
        ClinicalTrial.check_all_steps_valid = Mock(return_value=True)
        utils.set_state(self.ct, State.objects.get(
            pk=State.STATE_EM_AVALIACAO)
        )
        self.assertEqual(
            utils.get_state(self.ct).pk,
            State.STATE_EM_AVALIACAO
        )
        self.assertRaises(
            PermissionDenied,
            ClinicalTrialEditListView.as_view(),
            request,
            pk=self.ct.pk
        )
        self.user.groups.add(Group.objects.get(name=u'evaluator'))
        self.user2.groups.add(Group.objects.get(name=u'reviser'))
        ClinicalTrialEditListView.as_view()(request, pk=self.ct.pk)
        self.assertEqual(
            utils.get_state(self.ct).pk,
            State.STATE_AGUARDANDO_REVISAO
        )

    def test_do_transition_resubmeter(self):
        request = self.factory.post(
                    reverse('clinical_trial_edit_list', args=(self.ct.pk,)),
                    data={'resubmit': u'yes'}
                  )
        request.user = self.user
        ClinicalTrial.check_all_steps_valid = Mock(return_value=True)
        utils.set_state(self.ct, State.objects.get(
            pk=State.STATE_EM_REVISAO)
        )
        self.assertEqual(
            utils.get_state(self.ct).pk,
            State.STATE_EM_REVISAO
        )
        self.assertRaises(
            PermissionDenied,
            ClinicalTrialEditListView.as_view(),
            request,
            pk=self.ct.pk
        )
        self.user.groups.add(Group.objects.get(name=u'reviser'))
        ClinicalTrialEditListView.as_view()(request, pk=self.ct.pk)
        self.assertEqual(
            utils.get_state(self.ct).pk,
            State.STATE_EM_REVISAO
        )
        #Create a observation on a field, so it will allow the RESUBMETER Transition.
        StateObjectFieldObservation.objects.create(
            content_type=ContentType.objects.get_for_model(ClinicalTrial),
            content_id=self.ct.pk,
            field_name='date_first_enrollment',
            user=self.user_reviser,
            object_state_id=1
        )
        ClinicalTrialEditListView.as_view()(request, pk=self.ct.pk)
        self.assertEqual(
            utils.get_state(self.ct).pk,
            State.STATE_RESUBMETIDO
        )

        StateObjectFieldObservation.objects.all().delete()
    def test_do_transition_publish(self):
        request = self.factory.post(
                    reverse('clinical_trial_edit_list', args=(self.ct.pk,)),
                    data={'publish': u'yes'}
                  )
        request.user = self.user
        ClinicalTrial.check_all_steps_valid = Mock(return_value=True)
        utils.set_state(self.ct, State.objects.get(
            pk=State.STATE_EM_REVISAO)
        )
        self.assertEqual(
            utils.get_state(self.ct).pk,
            State.STATE_EM_REVISAO
        )
        self.assertRaises(
            PermissionDenied,
            ClinicalTrialEditListView.as_view(),
            request,
            pk=self.ct.pk
        )
        self.user.groups.add(Group.objects.get(name=u'reviser'))
        ClinicalTrialEditListView.as_view()(request, pk=self.ct.pk)
        self.assertEqual(
            utils.get_state(self.ct).pk,
            State.STATE_PUBLICADO
        )

    def test_do_transition_resend_to_revision(self):
        request = self.factory.post(
                    reverse('clinical_trial_edit_list', args=(self.ct.pk,)),
                    data={'resend_to_revision': u'yes'}
                  )
        request.user = self.user
        ClinicalTrial.check_all_steps_valid = Mock(return_value=True)
        utils.set_state(self.ct, State.objects.get(
            pk=State.STATE_RESUBMETIDO)
        )
        self.assertEqual(
            utils.get_state(self.ct).pk,
            State.STATE_RESUBMETIDO
        )
        self.ct.sponsor_set.create(institution=self.institution)
        ClinicalTrialEditListView.as_view()(request, pk=self.ct.pk)
        self.assertEqual(
            utils.get_state(self.ct).pk,
            State.STATE_EM_MODERACAO_RESUBMETIDO
        )

    def test_do_transition_resend_to_revision2(self):
        request = self.factory.post(
                    reverse('clinical_trial_edit_list', args=(self.ct.pk,)),
                    data={'resend_to_revision': u'yes'}
                  )
        request.user = self.user
        ClinicalTrial.check_all_steps_valid = Mock(return_value=True)
        utils.set_state(self.ct, State.objects.get(
            pk=State.STATE_RESUBMETIDO)
        )
        self.assertEqual(
            utils.get_state(self.ct).pk,
            State.STATE_RESUBMETIDO
        )
        self.user.groups.add(Group.objects.get(name=u'reviser'))
        ClinicalTrialEditListView.as_view()(request, pk=self.ct.pk)
        self.assertEqual(
            utils.get_state(self.ct).pk,
            State.STATE_EM_REVISAO
        )

    def test_do_transition_finish_filling_revision_resubmited(self):
        request = self.factory.post(
                    reverse('clinical_trial_edit_list', args=(self.ct.pk,)),
                    data={'finish_filling_revision_resubmited': u'yes'}
                  )
        request.user = self.user
        ClinicalTrial.check_all_steps_valid = Mock(return_value=True)
        utils.set_state(self.ct, State.objects.get(
            pk=State.STATE_REVISAO_PREENCHIMENTO_RESUBMETIDO)
        )
        self.assertEqual(
            utils.get_state(self.ct).pk,
            State.STATE_REVISAO_PREENCHIMENTO_RESUBMETIDO
        )
        ClinicalTrialEditListView.as_view()(request, pk=self.ct.pk)
        self.assertEqual(
            utils.get_state(self.ct).pk,
            State.STATE_EM_MODERACAO_RESUBMETIDO
        )


class ClinicalTrialWorkflowPermissionTestCase(TestCase):

    def setUp(self):
        self.group = CTGroup.objects.create(
            visibility=1
        )

        self.user_registrant = User.objects.create(
            gender=1,
            race=1,
            username=u'user_registrant',
            country_id=u'BR'
        )
        self.user_registrant.groups.add(
            Group.objects.get(
                name=u'registrant'
            )
        )

        self.ct = ClinicalTrial.objects.create(
            group=self.group,
            user=self.user_registrant,
            holder=self.user_registrant,
        )

        self.user_registrant_adm = User.objects.create(
            gender=1,
            race=1,
            username=u'user_registrant_adm',
            country_id=u'BR'
        )
        self.user_registrant_adm.groups.add(
            Group.objects.get(
                name=u'registrant'
            )
        )
        self.user_registrant_adm.ctgroups.create(
            ct_group=self.group,
            type_user=CTGroupUser.TYPE_ADMINISTRATOR
        )

        self.user_reviser = User.objects.create(
            gender=1,
            race=1,
            username=u'user_reviser',
            country_id=u'BR'
        )
        self.user_reviser.groups.add(
            Group.objects.get(
                name=u'reviser'
            )
        )

        self.user_evaluator = User.objects.create(
            gender=1,
            race=1,
            username=u'user_evaluator',
            country_id=u'BR'
        )
        self.user_evaluator.groups.add(
            Group.objects.get(
                name=u'evaluator'
            )
        )

        self.user_manager = User.objects.create(
            gender=1,
            race=1,
            username=u'user_manager',
            country_id=u'BR'
        )
        self.user_manager.groups.add(
            Group.objects.get(
                name=u'manager'
            )
        )

        self.user_administrador = User.objects.create(
            gender=1,
            race=1,
            username=u'user_administrator',
            country_id=u'BR'
        )
        self.user_administrador.groups.add(
            Group.objects.get(
                name=u'administrador'
            )
        )

        self.factory = RequestFactory()

    def test_view_permissions(self):
        
        detail_request = self.factory.get(
            reverse(u'clinical_trial_detail', args=(self.ct.pk,))
        )

        detail_request.user = self.user_registrant

        self.assertEqual(
            ClinicalTrialDetailView.as_view()(
                detail_request,
                pk=self.ct.pk
            ).status_code,
            200
        )

        self.ct.holder = None
        self.ct.user = self.user_evaluator
        self.ct.save()

        self.assertRaises(
            PermissionDenied,
            ClinicalTrialDetailView.as_view(),
            detail_request,
            pk=self.ct.pk,
        )

        self.ct.user = self.user_registrant
        self.ct.save()

        detail_request.user = self.user_registrant_adm

        self.assertEqual(
            ClinicalTrialDetailView.as_view()(
                detail_request,
                pk=self.ct.pk
            ).status_code,
            200
        )

        self.ct.group = None
        self.ct.save()

        self.assertRaises(
            PermissionDenied,
            ClinicalTrialDetailView.as_view(),
            detail_request,
            pk=self.ct.pk,
        )

        self.ct.group = self.group
        self.ct.save()

        #assert it's ok to view the clinical trial to
        # all other user types
        for i in (self.user_reviser, self.user_manager,
                  self.user_administrador, self.user_evaluator):
            detail_request.user = i

            self.assertEqual(
                ClinicalTrialDetailView.as_view()(
                    detail_request,
                    pk=self.ct.pk
                ).status_code,
                200
            )

    def test_edit_permissions(self):
        
        edit_list_request = self.factory.get(
            reverse(u'clinical_trial_edit_list', args=(self.ct.pk,))
        )
        edit_list_request.LANGUAGE_CODE = u'pt'
        request_step_1 = self.factory.get(
            reverse(u'clinical_trial_edit_identification', args=(self.ct.pk,))
        )
        request_step_1.LANGUAGE_CODE = u'pt'

        request_step_2 = self.factory.get(
            reverse(u'clinical_trial_edit_sponsors', args=(self.ct.pk,))
        )
        request_step_2.LANGUAGE_CODE = u'pt'
        request_step_3 = self.factory.get(
            reverse(
                u'clinical_trial_edit_health_conditions',
                args=(self.ct.pk,)
            )
        )
        request_step_3.LANGUAGE_CODE = u'pt'
        request_step_4 = self.factory.get(
            reverse(u'clinical_trial_edit_interventions', args=(self.ct.pk,))
        )
        request_step_4.LANGUAGE_CODE = u'pt'
        request_step_5 = self.factory.get(
            reverse(u'clinical_trial_edit_recruitment', args=(self.ct.pk,))
        )
        request_step_5.LANGUAGE_CODE = u'pt'
        request_step_6 = self.factory.get(
            reverse(u'clinical_trial_edit_study_type', args=(self.ct.pk,))
        )
        request_step_6.LANGUAGE_CODE = u'pt'
        request_step_7 = self.factory.get(
            reverse(u'clinical_trial_edit_outcomes', args=(self.ct.pk,))
        )
        request_step_7.LANGUAGE_CODE = u'pt'
        request_step_8 = self.factory.get(
            reverse(u'clinical_trial_edit_contacts', args=(self.ct.pk,))
        )
        request_step_8.LANGUAGE_CODE = u'pt'
        request_step_9 = self.factory.get(
            reverse(u'clinical_trial_edit_attacments', args=(self.ct.pk,))
        )
        request_step_9.LANGUAGE_CODE = u'pt'

        
        #test the registrant permission
        #loop over the steps that shoud let and dont let edit
        #and loop in all the clinical trial edit views
        states_should_accept_edit = (
            State.STATE_EM_PREENCHIMENTO,
            State.STATE_REVISANDO_PREENCHIMENTO,
            State.STATE_RESUBMETIDO,
            State.STATE_REVISAO_PREENCHIMENTO_RESUBMETIDO
        )
        for state_ok in states_should_accept_edit:
            utils.set_state(
                self.ct,
                State.objects.get(pk=state_ok)
            )
            self.ct.holder = self.user_registrant
            self.ct.save()
            for step in (edit_list_request, request_step_1, request_step_2,
                         request_step_3, request_step_4, request_step_5,
                         request_step_6, request_step_7, request_step_8,
                         request_step_9):
                step.user = self.user_registrant
                self.assertEqual(
                    resolve(step.path).func(step, pk=self.ct.pk).status_code,
                    200
                )


        states_should_not_accept_edit = (
            State.STATE_AGUARDANDO_MODERACAO,
            State.STATE_AGUARDANDO_AVALIACAO,
            State.STATE_EM_AVALIACAO,
            State.STATE_AGUARDANDO_REVISAO,
            State.STATE_EM_REVISAO,
            State.STATE_PUBLICADO,
            State.STATE_EM_MODERACAO_RESUBMETIDO,
        )
        for state_not_ok in states_should_not_accept_edit:
            utils.set_state(
                self.ct,
                State.objects.get(pk=state_not_ok)
            )
            self.ct.holder = self.user_registrant
            self.ct.save()
            for step in (edit_list_request, request_step_1, request_step_2,
                         request_step_3, request_step_4, request_step_5,
                         request_step_6, request_step_7, request_step_8,
                         request_step_9):
                step.user = self.user_registrant
                self.assertRaises(
                    PermissionDenied,
                    resolve(step.path).func,
                    step,
                    pk=self.ct.pk,
                )
        



        #test the registrant_adm permission
        #loop over the steps that shoud let and dont let edit
        #and loop in all the clinical trial edit views
        states_should_accept_edit = (
            State.STATE_EM_PREENCHIMENTO,
            State.STATE_REVISANDO_PREENCHIMENTO,
            State.STATE_RESUBMETIDO,
            State.STATE_REVISAO_PREENCHIMENTO_RESUBMETIDO
        )
        for state_ok in states_should_accept_edit:
            utils.set_state(
                self.ct,
                State.objects.get(pk=state_ok)
            )
            self.ct.holder = self.user_registrant
            self.ct.group = self.group
            self.ct.save()
            for step in (edit_list_request, request_step_1, request_step_2,
                         request_step_3, request_step_4, request_step_5,
                         request_step_6, request_step_7, request_step_8,
                         request_step_9):
                step.user = self.user_registrant_adm
                self.assertEqual(
                    resolve(step.path).func(step, pk=self.ct.pk).status_code,
                    200
                )
        #test the raises of permission denied when is not adm of the group
        for state_ok in states_should_accept_edit:
            utils.set_state(
                self.ct,
                State.objects.get(pk=state_ok)
            )
            self.ct.holder = self.user_registrant
            self.ct.group = self.group
            self.ct.save()
            self.group.users.all().update(type_user=CTGroupUser.TYPE_REGISTRANT)
            for step in (edit_list_request, request_step_1, request_step_2,
                         request_step_3, request_step_4, request_step_5,
                         request_step_6, request_step_7, request_step_8,
                         request_step_9):
                step.user = self.user_registrant_adm
                self.assertRaises(
                    PermissionDenied,
                    resolve(step.path).func,
                    step,
                    pk=self.ct.pk,
                )


        states_should_not_accept_edit = (
            State.STATE_AGUARDANDO_MODERACAO,
            State.STATE_AGUARDANDO_AVALIACAO,
            State.STATE_EM_AVALIACAO,
            State.STATE_AGUARDANDO_REVISAO,
            State.STATE_EM_REVISAO,
            State.STATE_PUBLICADO,
            State.STATE_EM_MODERACAO_RESUBMETIDO,
        )
        for state_not_ok in states_should_not_accept_edit:
            utils.set_state(
                self.ct,
                State.objects.get(pk=state_not_ok)
            )
            self.ct.holder = self.user_registrant
            self.ct.group = self.group
            self.ct.save()
            for step in (edit_list_request, request_step_1, request_step_2,
                         request_step_3, request_step_4, request_step_5,
                         request_step_6, request_step_7, request_step_8,
                         request_step_9):
                step.user = self.user_registrant_adm
                self.assertRaises(
                    PermissionDenied,
                    resolve(step.path).func,
                    step,
                    pk=self.ct.pk,
                )


        #test the all other users permissions,
        #that must be the same for the clinical trial edit views
        #Permission Denied
        all_states = states_should_accept_edit + states_should_not_accept_edit
        for state in all_states:
            utils.set_state(
                self.ct,
                State.objects.get(pk=state)
            )
            for i in (self.user_reviser, self.user_manager,
                      self.user_administrador, self.user_evaluator):
                self.ct.holder = i
                self.ct.group = self.group
                self.ct.save()
                #NOTE: the edit_list_request is not considered in this step
                for step in (request_step_1, request_step_2,
                         request_step_3, request_step_4, request_step_5,
                         request_step_6, request_step_7, request_step_8,
                         request_step_9):
                    step.user = i
                    # just if the user is reviser or evaluator and the
                    #workflow state is STATE_EM_REVISAO or STATE_EM_AVALIACAO
                    #the response should be 200(Ok).
                    if i == self.user_reviser \
                        and state == State.STATE_EM_REVISAO:
                        self.assertEqual(
                            resolve(step.path).func(
                                step,
                                pk=self.ct.pk
                            ).status_code,
                            200
                        )
                    elif i == self.user_evaluator \
                        and state == State.STATE_EM_AVALIACAO:
                        self.assertEqual(
                            resolve(step.path).func(
                                step,
                                pk=self.ct.pk
                            ).status_code,
                            200
                        )
                    elif i == self.user_evaluator \
                        and state == State.STATE_AGUARDANDO_AVALIACAO:
                        self.assertEqual(
                            resolve(step.path).func(
                                step,
                                pk=self.ct.pk
                            ).status_code,
                            302
                        )
                    elif i == self.user_reviser \
                        and state == State.STATE_AGUARDANDO_REVISAO:
                        self.assertEqual(
                            resolve(step.path).func(
                                step,
                                pk=self.ct.pk
                            ).status_code,
                            302
                        )
                    else:
                        self.assertRaises(
                            PermissionDenied,
                            resolve(step.path).func,
                            step,
                            pk=self.ct.pk,
                        )

from clinical_trials.helpers import get_next_by_occupation
class ChargeOccupationBalanceTestCase(TestCase):

    def _create_reviser_user(self, username, occupation, number_cts_holding):
        user = User.objects.create(
            username=username,
            ocuppation=occupation,
            gender=1,
            race=1,
            country_id=u'BR'
        )
        user.groups.add(Group.objects.get(name=u'reviser'))
        for i in range(number_cts_holding):
            ClinicalTrial.objects.create(
                user=user,
                holder=user
            )
        return user

    def _create_random_ct(self, holder):
        ClinicalTrial.objects.create(
            user=holder,
            holder=holder
        )

    def test_get_next_revisor_occupation(self):
        
        self.user1 = self._create_reviser_user(
            u'user1',
            10,
            0)

        self.user2 = self._create_reviser_user(
            u'user2',
            10,
            5)

        self.user3 = self._create_reviser_user(
            u'user3',
            3,
            3)

        self.user4 = self._create_reviser_user(
            u'user4',
            40,
            30)

        #until the user1 have same number than user2,
        #he must the the chosen
        for i in range(5):
            next_user = get_next_by_occupation(u'reviser')
            self.assertEqual(next_user, self.user1)
            self._create_random_ct(self.user1)
            if self.user1.holding_clinical_trials.all().count() ==\
             self.user2.holding_clinical_trials.all().count():
             self._create_random_ct(self.user1)

        #loop over a list of the users sequence that must be and
        #test it, after add a new clinical trial for he.
        for must_be in (self.user2, self.user1, self.user2, self.user1,
                        self.user2, self.user4, self.user4, self.user4,
                        self.user1, self.user4, self.user2, self.user4,
                        self.user4, self.user4, self.user1, self.user4,
                        self.user2, self.user4, self.user4, self.user1,
                        self.user4, self.user2, self.user4, self.user4,
                        self.user3):
            next_user = get_next_by_occupation(u'reviser')
            # self.assertEqual(next_user, must_be)
            self._create_random_ct(must_be)
