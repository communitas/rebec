# -*- coding: utf-8 -*-
"""
Models for clinical_trials app.
"""

from django.db import models
from django.db.models.signals import (post_init, post_save,
                                      pre_save, pre_delete)
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from django.utils.translation import ugettext_lazy
from django.utils.translation import get_language
from django.utils import timezone
from django.core.urlresolvers import reverse
from workflows.models import StateObjectFieldObservation, StateObjectHistory, \
    State, StateObjectRelation
from django.contrib.contenttypes.models import ContentType
from datetime import datetime, timedelta
from administration.models import StateDaysLimit, Setup, Institution
from clinical_trials import CLINICAL_TRIALS_STEPS_FIELDS


def post_init_clinical_trial(sender, instance, *args, **kwargs):
    """
    Sets the the default languages to the clinical trial based on the setup
    """
    from administration.models import Setup
    
    if instance.pk is None:
        setup = Setup.objects.first()
        if setup:
            instance.language_1_mandatory = setup.language_1_mandatory
            instance.language_2_mandatory = setup.language_2_mandatory
            instance.language_3_mandatory = setup.language_3_mandatory

def handle_initial_state(sender, instance, *args, **kwargs):
    """
    A handle to post_save on create to set initial state on workflow

    :param sender: A sender to receive signals
    :param instance: The Django Model instance
    """
    from workflows.utils import set_initial_state
    if kwargs.get('created', False) \
      and kwargs.get(u'using', None) == u'default':
        set_initial_state(instance)
        return True

def handle_workflow_ct_deleted(sender, instance, *args, **kwargs):
    """
    A handle to post_delete on delete to remove all workflow register

    :param sender: A sender to receive signals
    :param instance: The Django Model instance
    """
    from workflows.utils import get_state, remove_states_from_object
    state = get_state(instance)
    # <State: Em Preenchimento (ClinicalTrialWorkflow)>
    if state and state.pk == state.STATE_EM_PREENCHIMENTO:
        remove_states_from_object(instance)
        return True

class ClinicalTrialStateManager(models.Manager):
    
    def recruiting(self):
        """
        Get only the recruiting clinical trials.
        """
        qset = super(ClinicalTrialStateManager, self).get_query_set().using(
            u'published_cts')
        now = datetime.now()
        qset = qset.filter(
            date_first_enrollment__lte=now,
            date_last_enrollment__gte=now
        )
        pks = []
        for i in qset:
            if not i.is_interrupted():
                pks.append(i.pk)

        return qset.filter(pk__in=pks)

    def registered(self):
        """
        Get only the registered clinical trials.
        """
        qset = super(ClinicalTrialStateManager, self).get_query_set()

        return qset.using(u'published_cts')

    def analysing(self):
        """
        Get only the analysing clinical trials.
        """
        qset = super(ClinicalTrialStateManager, self).get_query_set()
        pks = StateObjectRelation.objects.filter(
            state__pk__in=[
                State.STATE_EM_AVALIACAO,
                State.STATE_EM_REVISAO,
                State.STATE_AGUARDANDO_AVALIACAO,
                State.STATE_AGUARDANDO_REVISAO,
                State.STATE_EM_MODERACAO_RESUBMETIDO,
                State.STATE_AGUARDANDO_MODERACAO]
            ).values_list(u'content_id', flat=True)

        return qset.filter(pk__in=pks)

    def drafting(self):
        """
        Get only the drafting clinical trials.
        """
        qset = super(ClinicalTrialStateManager, self).get_query_set()
        pks = StateObjectRelation.objects.filter(
            state__pk__in=[
                State.STATE_EM_PREENCHIMENTO,
                State.STATE_RESUBMETIDO]
            ).values_list(u'content_id', flat=True)

        return qset.filter(pk__in=pks)


class ClinicalTrial(models.Model):
    """Defines the base model for a clinical trial"""

    objects = ClinicalTrialStateManager()

    AGE_UNIT_CHOICES = (
        ('Y', _('Years')),
        ('M', _('Months')),
        ('W', _('Weeks')),
        ('D', _('Days')),
        ('H', _('Hours'))
    )

    GENDER_CHOICES = (
        ('M', _('Male')),
        ('F', _('Female')),
        ('B', _('Both')),
    )

    GENDER_ENGLISH_CHOICES = (
        ('M', 'Male'),
        ('F', 'Female'),
        ('B', 'Both'),
    )

    STUDY_TYPE_CHOICES = (
        ('I', _('Interventional')),
        ('O', _('Observational'))
    )

    EXPANDED_ACCESS_PROGRAM_CHOICES = (
        ('U', _('Unknown')),
        ('Y', _('Yes')),
        ('N', _('No'))
    )

    STATISTICAL_SAMPLE_ANALYSIS_CHOICES = (
        (u'F', _(u'Fixed Sample')),
        (u'S', _(u'Sequential Sample')),
    )

    STUDY_STATUS_CHOICES = (
        (u'N', _(u'Not yet recruiting')),
        (u'R', _(u'Recruiting')),
        (u'S', _(u'Suspended')),
        (u'C', _(u'Recruitment completed')),
        (u'O', _(u'Other')),
        (u'T', _(u'Terminated')),
        (u'W', _(u'Withdrawn')),
        (u'A', _(u'Data analysis completed')),
    )

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name=_('User'))

    group = models.ForeignKey(
        'administration.CTGroup',
        verbose_name=_('Group'),
        null=True,
        blank=True,
        help_text=\
        _(u"<p>Groups are used for research teams to share their clinical trials.</p><p>A clinical trial may or may not be associated with a group. If it is associated, all contacts are shared among the group of clinical trials and the group administrator can transfer clinical trials between group members.</p>"))

    language_1_mandatory = models.BooleanField(
        blank=True,
        default=False,
        verbose_name=_('Language 1 mandatory'))

    language_2_mandatory = models.BooleanField(
        blank=True,
        default=False,
        verbose_name=_('Language 2 mandatory'))

    language_3_mandatory = models.BooleanField(
        blank=True,
        default=False,
        verbose_name=_('Language 3 mandatory'))

    code = models.CharField(
        max_length=50,
        null=True,
        verbose_name=_('Code'))

    date_creation = models.DateField(
        auto_now_add=True,
        verbose_name=_('Date creation'),
    )

    date_last_state_change = models.DateField(
        verbose_name=_('Date last workflow state change'),
        null=False,
        blank=False,
        auto_now_add=True,
    )

    date_last_publication = models.DateTimeField(
        verbose_name=_(u'Date last workflow publication'),
        null=True,
        blank=True)

    study_status = models.CharField(
        verbose_name=_(u'Study Status'),
        max_length=1,
        choices=STUDY_STATUS_CHOICES,
        help_text=_(u'Study current status')
    )

    date_first_enrollment = models.DateField(
        null=True,
        blank=True,
        verbose_name=_('Date first enrollment'),
        help_text=('O registrante deverá selecionar a data prevista ou atual do recrutamento do primeiro participante do estudo. '))

    date_last_enrollment = models.DateField(
        null=True,
        blank=True,
        verbose_name=ugettext_lazy('Date last enrollment'),
        help_text=('O registrante deverá selecionar a data prevista ou atual do recrutamento do último participante do estudo.'))

    minimum_age_no_limit = models.BooleanField(
        default=True,
        verbose_name=ugettext_lazy(u'No Limit minimum age?'),
    )

    minimum_age_unit = models.CharField(
        null=True,
        blank=True,
        max_length=1,
        choices=AGE_UNIT_CHOICES,
        verbose_name=_('Minimum age unit'),
        help_text=('O registrante deverá escolher entre as opções apresentadas (anos, meses, semanas, dias e horas), a unidade de medida para idade mínima de inclusão. Favor ficar atento para a coerência dos campos.'))

    maximum_age_no_limit = models.BooleanField(
        default=True,
        verbose_name=ugettext_lazy(u'No limit maximum age?'),
    )

    maximum_age_unit = models.CharField(
        null=True,
        blank=True,
        max_length=1,
        choices=AGE_UNIT_CHOICES,
        verbose_name=_('Maximum age unit'),
        help_text=('O registrante deverá escolher entre as opções apresentadas (anos, meses, semanas, dias e horas), a unidade de medida para idade máxima de inclusão. Favor ficar atento para a coerência dos campos.'))

    inclusion_minimum_age = models.IntegerField(
        null=True,
        blank=True,
        verbose_name=_('Inclusion minimum age'),
        help_text = ('O registrante deverá escrever numericamente, a idade mínima (caso haja), para inclusão de participantes em seu estudo.'))

    inclusion_maximum_age = models.IntegerField(
        null=True,
        blank=True,
        verbose_name=_('Inclusion maximum age'),
        help_text=('O registrante deverá escrever numericamente, a idade máxima (caso haja), para inclusão de participantes em seu estudo. Caso não haja idade máxima, deixar o campo em branco.'))

    gender = models.CharField(
        null=True,
        blank=True,
        max_length=1,
        choices=GENDER_CHOICES,
        verbose_name=_('Gender'),
        help_text=('No campo GÊNERO, o registrante deverá selecionar entre as opções (ambos, feminino e masculino) de acordo com as informações apresentadas por ele mesmo sobre o estudo nos passos anteriores.'))

    study_type = models.CharField(
        null=True,
        blank=True,
        max_length=1,
        choices=STUDY_TYPE_CHOICES,
        verbose_name=_('Study type'),
        help_text=(u'<p>Caro registrante, a revisão da seção TIPO DO ESTUDO depende diretamente das informações contidas no campo INTERVENÇÃO. Portanto, solicitamos especial atenção à(s) observação(ões) aberta(s) para o campo. Favor marcar a opção: “intervencional” ou “observacional”. Ressaltando, um estudo intervencional (ou ensaio clínico) envolve a realização em pacientes voluntários de intervenções – uso de drogas, equipamentos, procedimentos, dietas.</p><p>O estudo intervencional possui desenhos com especificação de enfoque do estudo, desenho da intervenção, número de braços, tipo de mascaramento, tipo de alocação e fase do estudo. Já no estudo observacional, os pesquisadores não realizam intervenções de acordo com o plano/protocolo de pesquisa, mas observam pacientes e desfechos de uma evolução na qual eles não intervieram. Os Estudos Observacionais aceitos pelo ReBEC são os analíticos (caso-controle, coorte e corte transversal).</p>'))

    expanded_access_program = models.CharField(
        null=True,
        blank=True,
        max_length=1,
        choices=EXPANDED_ACCESS_PROGRAM_CHOICES,
        verbose_name=_('Expanded access program'),
        help_text= ('<p>SOMENTE OS ESTUDOS DO TIPO “INTERVENCIONAL” COM DROGAS DEVERÃO TER O CAMPO “PROGRAMA DE ACESSO EXPANDIDO” PREENCHIDO.</p><p>O registrante deverá escolher entre as opções disponíveis (desconhecido, sim e não). O acesso expandido é um processo patrocinado de disponibilidade de produto novo, promissor, ainda sem registro na agência nacional de vigilância sanitária – ANVISA – para pacientes com doenças graves e que ameaçam a vida, na ausência de alternativas terapêuticas satisfatórias disponibilizadas no país, sem ônus adicional para o paciente. O produto deve estar em estudo de fase III em desenvolvimento no Brasil ou no país de origem e com programa de acesso expandido aprovado no país de origem, ou com registro do produto no país de origem (RDC).</p>'))

    number_arms = models.IntegerField(
        null=True,
        blank=True,
        verbose_name=_('Number arms'),
        help_text=('<p>SOMENTE OS ESTUDOS DO TIPO “INTERVENCIONAL” DEVERÃO TER O CAMPO “NÚMERO DE BRAÇOS” PREENCHIDO.<br />O registrante deverá inserir o número de grupos de intervenção, caso haja. Quando existir apenas um braço, o registrante deverá digitar apenas o número 1. Este campo não aceita caracteres alfanuméricos (letras). Importante destacar que o grupo controle conta como braço.</p>'))


    intervention_codes = models.ManyToManyField(
        'administration.InterventionCode',
        verbose_name=_('Intervention codes'),
        null=True,
        blank=True)

    recruitment_countries = models.ManyToManyField(
        'administration.Country',
        verbose_name=_('Recruitment countries'),
    )

    intervention_assignment = models.ForeignKey(
        u'administration.InterventionAssignment',
        verbose_name=_(u'Intervention assignment'),
        null=True,
        blank=True,
        help_text=('<p>SOMENTE OS ESTUDOS DO TIPO “INTERVENCIONAL” DEVERÃO TER O CAMPO “DESENHO DA INTERVENÇÃO” PREENCHIDO.<br /> O registrante do estudo intervencional deverá selecionar entre as opções apresentadas, a que mais se adequa ao seu estudo, de acordo com o que foi exposto até aqui.<br />As opções são:</p><p>Grupo Único- estudo de braço único;<br />Paralelo - os participantes são alocados em um ou mais grupos em paralelo para a duração do estudo;<br />Cross-over - os participantes recebem uma das duas intervenções alternativas durante a fase inicial do estudo e recebe a outra intervenção durante a segunda fase do estudo;<br />Fatorial- duas ou mais intervenções, cada uma isoladamente e em combinação são avaliadas em paralelo contra um grupo de controle.</p>'))

    study_purpose = models.ForeignKey(
        u'administration.StudyPurpose',
        verbose_name=_('Study purpose'),
        null=True,
        blank=True,
        help_text=('<p>SOMENTE OS ESTUDOS DO TIPO “INTERVENCIONAL” DEVERÃO TER O CAMPO “ENFOQUE DO ESTUDO” PREENCHIDO.O registrante deverá selecionar uma das opções como o principal enfoque do seu estudo. Apenas deverá escolher a opção "outros" se nenhuma das categorias mencionadas for suficiente para definir o enfoque do seu estudo.<br />As opções são:</p><p>DIAGNÓSTICO - realizado para encontrar melhores testes ou procedimentos para o diagnóstico de uma determinada doença ou condição;</p><p>ETIOLÓGICO - identificar causas de doenças;</p><p>PROGNÓSTICO - estima o provável curso clínico da doença no tempo e antecipar suas prováveis complicações;</p><p>PREVENÇÃO – busca melhores formas (medicamentos, vitaminas, vacinas, sais minerais, ou mudanças de estilo de vida) de prevenir a doença em pessoas que nunca tiveram a doença ou evitar uma doença de regressar;</p><p>TRATAMENTO - testa tratamentos experimentais, novas combinações de drogas, ou novas abordagens para a cirurgia ou radioterapia.</p>'))

    followup_period = models.CharField(
        max_length=2,
        verbose_name=_(u'Follow-Up Period'),
        choices=((u'TR', _(u'Transversal')),
                 (u'LN', _(u'Longitudinal')),),
        help_text = ('<p>1) Transversal:  não apresenta um período de seguimento, os dados são coletados em um único ponto de tempo e representam um corte transversal ou fotografia das características da população em estudo e são usados, por exemplo, para estudar a prevalência das doenças.</p><p>2) Longitudinais: existe um período de seguimento, mais ou menos longo, dos indivíduos, existem pelo menos dois pontos no tempo em que se colhem dados e permitem estudar as mudanças de estado que ocorreram na população durante o período em que esta foi seguida, são usados, por exemplo para estudar a incidência das doenças.</p>'),
        null=True,
        blank=True
    )

    reference_period = models.CharField(
        max_length=2,
        verbose_name=_(u'Reference Period'),
        choices=((u'RT', _(u'Retrospective')),
                (u'PR', _(u'Prospective')),),
        help_text=('<p>1) Retrospectivos : colhem-se dados sobre exposições ou doenças que ocorreram no passado.</p><p>2) Prospectivos:  colhem-se dados sobre exposições ou doenças que ocorrem no presente ou que vão ocorrer no futuro, durante o período de seguimento dos indivíduos.</p>'),
        null=True,
        blank=True
    )

    study_phase = models.ForeignKey(
        u'administration.StudyPhase',
        verbose_name=_(u'Study phase'),
        null=True,
        blank=True,
        help_text=('<p>SOMENTE OS ESTUDOS DO TIPO “INTERVENCIONAL” COM DROGAS DEVERÃO TER O CAMPO “FASE DO ESTUDO”PREENCHIDO.O registrante deverá selecionar, entre as opções disponíveis (n/a,1,2,3,4), aquela que mais se encaixa com a fase em que se encontra seu estudo. Se o estudo se enquadrar em mais de uma fase, considerar a fase mais precoce.Esta informação, assim como as demais, deverá estar DE ACORDO COM AS INFORMAÇÕES FORNECIDAS NA PLATAFORMA BRASIL E PROTOCOLO DO ESTUDO. SE ESSA INFORMAÇÃO NÃO CONSTAR NO PROTOCOLO DO ESTUDO , SEU ESTUDO NÃO POSSUIRÁ FASE. Para estudos SEM FASE a seleção será sempre n/a.</p><p>- A Fase 0 ou Fase pré-clínica é caracterizada pelos estudos in vitro e testes em animais.</p><p>- Na Fase I há o teste de um medicamento experimental pela primeira vez em um pequeno número de seres humanos saudáveis para verificar segurança, dosagem segura e efeitos colaterais (10-100 indivíduos). </p><p>- Na Fase II, o medicamento é administrado em um número maior de pessoas que têm uma doença ou condição particular, para verificar eficácia e sua segurança (100-500 indivíduos).</p><p>- A Fase III envolve centenas ou milhares de pessoas com a condição ou doença e objetiva avaliar melhor segurança e eficácia, monitorar efeitos colaterais, e compará-la a tratamentos já utilizados (2.000-5.000).</p><p>-  Fase IV consiste no monitoramento após a liberação da ANVISA e comercialização do medicamento.</p>'))

    allocation_type = models.ForeignKey(
        u'administration.AllocationType',
        verbose_name=_(u'Allocation type'),
        null=True,
        blank=True,
        help_text = ('<p>SOMENTE OS ESTUDOS DO TIPO “INTERVENCIONAL” DEVERÃO TER O CAMPO “TIPO DE MASCARAMENTO” PREENCHIDO.</p><p>Randomização simples: conforme cada sujeito entra no estudo, é atribuído o tratamento de maneira serial.</p><p>Randomização por blocos: somente a cada X número de sujeitos que entrarem na pesquisa, e sendo X uma constante ao longo do estudo, o braço de randomização será alterado.</p><p>Randomização estratificada: somente a cada X número de sujeitos que entrarem na pesquisa, e não sendo X uma constante ao longo do estudo, o braço de randomização será alterado.</p><p>Não-randomizada: não há esquema de alocação de pacientes no estudo</p>'))

    masking_type = models.ForeignKey(
        u'administration.MaskingType',
        verbose_name=_(u'Masking type'),
        null=True,
        blank=True,
        help_text = ('<p>SOMENTE OS ESTUDOS DO TIPO “INTERVENCIONAL” DEVERÃO TER O CAMPO “TIPO DE MASCARAMENTO” PREENCHIDO.<br />Aberto: Não há qualquer mascaramento quanto ao tratamento. Sujeito de pesquisa e pesquisador sabem o que o paciente está fazendo uso.</p><p>Simples cego: somente os sujeitos de pesquisa não sabem o produto em investigação que estão recebendo.</p><p>Duplo cego não simulado: quando o pesquisador e os sujeitos de pesquisa não sabem a dose do produto em investigação que cada voluntário está recebendo. Neste tipo de estudo, o recebimento de uma forma farmacêutica já é suficiente para garantir o mascaramento do estudo. Por exemplo: o sujeito só precisa fazer uso de comprimidos orais, que podem conter substância ativa ou não, para garantir o mascaramento do tratamento.</p><p>Duplo cego duplo simulado (dummy): quando o pesquisador e o sujeito de pesquisa não sabem o produto em investigação que cada paciente está recebendo. Neste tipo de estudo o voluntário da pesquisa precisa receber pelo menos duas formas farmacêuticas para garantir a cegagem do estudo. Por exemplo: o sujeito deve fazer uso de comprimidos orais e injeção, que podem conter substância ativa ou não.</p><p>Triplo cego não simulado: além do pesquisador e do sujeito de pesquisa, quem analisa os resultados desconhece o tratamento. Neste tipo de estudo, o recebimento de uma forma farmacêutica já é suficiente para garantir o mascaramento do estudo. Por exemplo: o sujeito só precisa uso somente de comprimidos orais, que podem conter substância ativa ou não, para garantir a cegagem do tratamento.</p><p>Triplo cego simulado (dummy): além do pesquisador e do sujeito de pesquisa, quem analisa os resultados desconhece o tratamento. Neste tipo de estudo o voluntário precisa receber pelo menos três formas farmacêuticas para garantir a cegagem do estudo. Por exemplo: o sujeito deve fazer uso de comprimidos orais, cápsulas e injeção, que podem conter substância ativa ou não.</p>'))

    holder = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name=_('User'),
        related_name=u'holding_clinical_trials',
        null=True,
        blank=True)

    world_target_size = models.IntegerField(
        null=True,
        blank=True,
        help_text=('O registrante deverá indicar o número de participantes que este estudo pretende inscrever  (ou inscreveu, no caso de recrutamento completo e/ou análise de dados completa) no Total (Mundo).'),
        verbose_name=_('World')
    )

    brazil_target_size = models.IntegerField(
        null=True,
        blank=True,
        help_text=('O registrante deverá indicar o número de participantes que este estudo pretende inscrever  (ou inscreveu, no caso de recrutamento completo e/ou análise de dados completa) no Brasil.'),
        verbose_name=_('Brazil')
    )

    statistical_sample_analysis = models.CharField(
        max_length=1,
        null=False,
        blank=False,
        verbose_name=_('Statistical Sample Analysis'),
        choices=STATISTICAL_SAMPLE_ANALYSIS_CHOICES,
        help_text = ('<p>Amostra fixa: o tamanho da amostra é pré-definido.</p><p>Amostra sequencial: o tamanho da amostra depende do resultado das análises interinas.</p>'),
    )

    study_final_date = models.DateField(
        null=True,
        blank=True,
        verbose_name=_('Study Final Date'),
        help_text=_('Date for finish of this study.'))


    #This date is the date of the last publication, or the date of the last time the
    #owner of this trial reviewed it and said that nothing has changed
    #(6 from 6 months until the self.study_final_date)
    date_last_change = models.DateField(
        auto_now_add=True,
        verbose_name=_('Date last change'),
        help_text=_('Date of last study change/verification.'))

    class Meta:
        verbose_name = _('Clinical Trial')
        verbose_name_plural = _('Clinical Trials')

    def __unicode__(self):
        if self.code:
            return u'%s - %s' %(self.pk, self.code)
        return u'%s' % self.pk

    def get_absolute_url(self):
        """This is used by generic views to redirect"""
        return reverse('clinical_trial_detail', kwargs={'pk':self.id})

    def get_absolute_public_url(self):
        """This is used to get public url"""
        if self.code:
            return '%s%s' % (settings.SITE_URL, reverse('clinical_trial_detail_public', args=(self.code,)))
        return u''


    def get_gender_english_display(self):
        """
        Returns the gender text in english.
        """
        return dict(self.GENDER_ENGLISH_CHOICES).get(self.gender, None)


    def has_date_for_recruiting(self):
        now = datetime.now().date()
        return self.date_first_enrollment and self.date_last_enrollment \
            and self.date_first_enrollment <= now \
            and self.date_last_enrollment >= now

    def get_recruitment_status(self, localized=True):
        if self.has_date_for_recruiting():
            if self.recruitment_interruptions.exists() \
            and not self.recruitment_interruptions.latest(u'date_time').interrupt:
                if localized:
                    return (True, _('Recruiting'))
                return (True, 'Recruiting')
            elif not self.recruitment_interruptions.exists():
                if localized:
                    return (True, _('Recruiting'))
                return (True, 'Recruiting')
        if localized:
            return (False, _('Not Recruiting'))
        return (False, 'Not Recruiting')


    def _check_fields_for_all_ctdetails(self, fields):
        message = _(u'The following fields must be filled: ')
        error_fields = []
        for detail in self.ctdetail_set.all():
            for field_name in fields:
                if not getattr(detail, field_name):
                    error_fields.append(
                        unicode(ugettext_lazy(detail._meta.get_field(field_name).verbose_name))
                    )

        message += ', '.join(set(error_fields))
        if error_fields:
            return (False, message)
        return (True, '')

    def _check_required_fields(self, fields):
        message = _(u'The following fields must be filled: ')
        error_fields = []
        for field_name in fields:
            if not getattr(self, field_name):
                error_fields.append(
                    unicode(self._meta.get_field(field_name).verbose_name)
                )

        message += ', '.join(set(error_fields))
        if error_fields:
            return (False, message)
        return (True, '')

    def check_identification_step(self):
        from administration.models import SetupIdentifier
        ctdetail_fields = (u'scientific_title', u'public_title')
        ok = self._check_fields_for_all_ctdetails(ctdetail_fields)

        if not ok[0]:
            return ok

        this_required_setups = self.identifier_set.filter(
            setup_identifier__mandatory=True).values_list(
                u'setup_identifier__pk', flat=True)
        all_required_setups = SetupIdentifier.objects.filter(
            mandatory=True).count()

        if len(set(this_required_setups)) != all_required_setups:
            return (False, ugettext_lazy(u'Still have required Identifiers.'))
        

        ok = self._check_required_fields([u'study_final_date'])
        if not ok[0]:
            return ok

        if self.study_final_date \
            and self.study_final_date < timezone.now().date():
            return (False, ugettext_lazy(u"Study final date can't be lower "
                              u"than the current date."))

        return (True, '')
        

    def check_sponsors_step(self):
        if not self.sponsor_set.filter(
            sponsor_type=Sponsor.SPONSOR_TYPE_PRIMARY).exists():
            return (False, ugettext_lazy(u'You must provide a primary type sponsor.'))

        if not self.sponsor_set.filter(
            sponsor_type=Sponsor.SPONSOR_TYPE_SECUNDARY).exists():
            return (False, ugettext_lazy(u'You must provide a secondary type sponsor.'))

        if not self.sponsor_set.filter(
            sponsor_type=Sponsor.SPONSOR_TYPE_SUPPORT).exists():
            return (False, ugettext_lazy(u'You must provide a support type sponsor.'))

        if self.sponsor_set.filter(
            institution__status=Institution.STATUS_INACTIVE).exists():
            from workflows.models import StateObjectHistory
            message = StateObjectHistory.objects.filter(
                content_id=self.pk
                ).latest(u'update_date').observation
            return (False, message or ugettext_lazy(u'You have inactive institutions.'))
        
        return (True, '')

    def check_health_conditions_step(self):
        ctdetail_fields = (u'health_condition',)
        ok = self._check_fields_for_all_ctdetails(ctdetail_fields)

        if not ok[0]:
            return ok

        #must have at least onte intervention type descriptor
        if not self.descriptor_set.filter(
            descriptor_type=Descriptor.TYPE_GENERAL
        ).exists():
            return (False, _(u'Must have at least one general descriptor.'))

        if not self.descriptor_set.filter(
            descriptor_type=Descriptor.TYPE_SPECIFIC
        ).exists():
            return (False, _(u'Must have at least one specific descriptor.'))

        return (True, '')


    def check_interventions_step(self):

        if not self.intervention_codes.count() and self.study_type == u'I':
            return (False, _(u'Interventions codes are required.'))

        if not self.intervention_observations.count() and self.study_type == u'O':
            return (False, _(u'At least one observation is required.'))

        return (True, '')


    def check_recruitment_step(self):
        ctdetail_fields = (u'inclusion_criteria', u'exclusion_criteria')
        ok = self._check_fields_for_all_ctdetails(ctdetail_fields)
        if not ok[0]:
            return ok

        if not self.recruitment_countries.count():
            return (False, _(u'Recruitment Countries are required.'))

        if not self.date_first_enrollment or \
            not self.date_last_enrollment or \
            not self.gender or \
            not (self.minimum_age_no_limit or (
                                str(self.inclusion_minimum_age) and \
                                self.minimum_age_unit)
                ) or \
            not (self.maximum_age_no_limit or (
                                str(self.inclusion_maximum_age) and \
                                self.maximum_age_unit)
                ):
            req_fields = [u'date_first_enrollment', u'date_last_enrollment',
                          u'gender']
            message = _(u'The following fields must be filled:')
            for i in req_fields:
                message += ' %s,' % unicode(self._meta.get_field(i).verbose_name)
            return (False, message)
        
        if self.study_final_date and self.date_last_enrollment:
            if self.study_final_date < self.date_last_enrollment:
                message = _(u'The enrollment dates must be lower'
                            u' than the study final date')
                return (False, message)


        return (True, '')

    def check_study_type_step(self):
        
        if self.number_arms and self.study_type == u'I' is None:
            return (
                    False,
                    _(u'Number of arms must be filled.')
                    )

        if self.study_type == u'I':
            req_fields = [u'expanded_access_program',
                           u'study_purpose',
                           u'intervention_assignment',
                           u'masking_type',
                           u'allocation_type',
                           u'study_phase']
        else:
            req_fields = [u'followup_period', u'reference_period']


            

        return self._check_required_fields(req_fields)

    def check_outcomers_step(self):
        """
        Must have at least one pimary and a secondary outcome type
        """
        has_primary = self.outcome_set.filter(
            outcome_type=Outcome.TYPE_PRIMARY).exists()

        if not has_primary:
            return (
                False,
                _(u'Must have one primary outcome.')
                )

        return (True, '')

    def check_contacts_step(self):
        """
        Must have a ctcontact of each type
        """
        has_public_queries = self.ctcontact_set.filter(
            contact_type=CTContact.TYPE_PUBLIC_QUERIES).exists()

        has_scientfic_queries = self.ctcontact_set.filter(
            contact_type=CTContact.TYPE_SCIENTIFIC_QUERIES).exists()

        has_site_queries = self.ctcontact_set.filter(
            contact_type=CTContact.TYPE_SITE_QUERIES).exists()

        if not (has_public_queries and 
                has_scientfic_queries and
                has_site_queries):
            return (
                False,
                _(u'Must have at least one of each type.')
                )

        return (True, '')


    def check_attachments_step(self):
        """
        Must return an iterable with the messages
        """
        from administration.models import Attachment
        #it's requied at least one attachment(any type)
        if self.ctattachment_set.all().count() == 0:
            return (False,
                    _(u'At least one attachment is required (any type).'))
        required_attachments = Attachment.objects.filter(required=True)
        if required_attachments.count() == 0:
            return (True, '')
        else:
            ct_required_attachments = self.ctattachment_set.filter(
                attachment__in=required_attachments
            ).count()
            if ct_required_attachments >= required_attachments.count():
                return (True, u'')
            else:
                return (False, _(u'Still have required attachments.'))

    def check_all_steps_valid(self):
        """
        Call the validation methods to determine if this clinical_trial is valid
        """
        validation_methods = (u'check_attachments_step',
                              u'check_contacts_step',
                              u'check_outcomers_step',
                              u'check_study_type_step',
                              u'check_recruitment_step',
                              u'check_interventions_step',
                              u'check_health_conditions_step',
                              u'check_sponsors_step',
                              u'check_identification_step',)
        
        for vm in validation_methods:
            if getattr(self, vm)()[0] == False:
                return False

        return True

    def get_workflow_state(self):
        from workflows.utils import get_state
        return get_state(self)

    def is_interrupted(self):
        last_interruption = self.recruitment_interruptions.all().order_by(
                                '-date_time',
                            )
        try:
            return last_interruption[0].interrupt
        except IndexError:
            return False


    def check_has_wk_pending_general_observations(self):
        """
        Return a boolean indicating if this trial haves
        pending general observations.
        """
        return StateObjectHistory.objects.filter(
            content_id=self.pk).latest('pk').general_observations.all().exists()


    def check_has_wk_pending_observations_all_steps(self):
        """
        Call the validation methods to determine if this clinical_trial has pending observations
        depending of the authenticated user type;
        """
        validation_methods = (u'check_has_wk_state_obs_for_identifications_step',
                              u'check_has_wk_state_obs_for_sponsors_step',
                              u'check_has_wk_state_obs_for_health_conditions_step',
                              u'check_has_wk_state_obs_for_interventions_step',
                              u'check_has_wk_state_obs_for_recruitment_step',
                              u'check_has_wk_state_obs_for_study_type_step',
                              u'check_has_wk_state_obs_for_outcomes_step',
                              u'check_has_wk_state_obs_for_contacts_step',
                              u'check_has_wk_state_obs_for_attachments_step',)

        for vm in validation_methods:
            if getattr(self, vm)() == True:                
                return True

        return False


    def get_last_wk_state_observations_for_reviser(self):
        fields_names = [i.name for i in self._meta.fields + self._meta.many_to_many]

        last_time_reviser = StateObjectHistory.objects.filter(
                                content_id=self.id,
                                state_id__in=(State.STATE_EM_REVISAO,
                                              State.STATE_EM_AVALIACAO)
                               ).order_by(u'-update_date')

        #If the 3rd last state was evaluation, state, we need to consider that date
        if last_time_reviser.count() >= 3 \
            and last_time_reviser[2].state_id == State.STATE_EM_AVALIACAO:
            last_time_reviser = last_time_reviser[2].update_date
        else:
            #Try to get the 2-times last, if it doesn't exists,
            #get the last time of revision.. and if it not exsits(still in evaluation)
            #so use the 1920 year date(not matter the date fr this case)
            last_time_reviser = last_time_reviser[1].update_date \
                                        if last_time_reviser.count() > 1 \
                                        else (
                                            last_time_reviser[0].update_date \
                                                if last_time_reviser.exists() \
                                                else datetime(1920, 1, 1)
                                        )

        query = StateObjectFieldObservation.objects.filter(
            field_name__in=fields_names,
            content_id=self.id,
            content_type_id=ContentType.objects.get_for_model(self),
            update_date__gt=last_time_reviser
        ).order_by(u'update_date').values_list(u'field_name', flat=True)

        if self.clinical_trial.date_last_publication:
            query = query.filter(update_date__gt=self.clinical_trial.date_last_publication)

        return query


    def get_last_wk_state_observations_for_registrant(self):
        fields_names = [i.name for i in self._meta.fields + self._meta.many_to_many]
        last_time_registrant = StateObjectHistory.objects.filter(
                                content_id=self.id,
                                state_id__in=(State.STATE_EM_PREENCHIMENTO,
                                              State.STATE_RESUBMETIDO)
                               ).order_by(u'-update_date')
        last_time_registrant = last_time_registrant[1] \
                                    if last_time_registrant.count() > 1 \
                                    else last_time_registrant[0]
        query = StateObjectFieldObservation.objects.filter(
            field_name__in=fields_names,
            content_id=self.id,
            content_type_id=ContentType.objects.get_for_model(self),
            update_date__gt=last_time_registrant.update_date
        ).order_by(u'update_date').values_list(u'field_name', flat=True)

        if self.date_last_publication:
            query = query.filter(update_date__gt=self.date_last_publication)
        return query


    def check_has_wk_state_obs_for_identifications_step(self):
        ctdetail_fields = CLINICAL_TRIALS_STEPS_FIELDS[u'ctdetail_fields'][1]
        has = False

        if self.holder.is_reviser() or self.holder.is_evaluator():
            has = self._has_history_for_ctdetail_fields_for_reviser(ctdetail_fields)
        if self.holder.is_registrant() and not has:
            has = self._has_history_for_ctdetail_fields_for_registrant(ctdetail_fields)

        if self.holder.is_registrant() and not has:
            has = self._has_history_for_ct_fields_for_registrant((u'study_final_date', u"study_type"))

        if (self.holder.is_reviser() or self.holder.is_evaluator()) and not has:
            has = self._has_history_for_ct_fields_for_reviser((u'study_final_date', u"study_type"))

        if has:
            return True

        for i in self.identifier_set.all():
            if self.holder.is_reviser() or self.holder.is_evaluator():
                if i.get_last_wk_state_observations_for_reviser():
                    return True
            if self.holder.is_registrant():
                if i.get_last_wk_state_observations_for_registrant():
                    return True

        return False


    def check_has_wk_state_obs_for_sponsors_step(self):
        for i in self.sponsor_set.all():
            if self.holder.is_reviser() or self.holder.is_evaluator():
                if i.get_last_wk_state_observations_for_reviser():
                    return True
            if self.holder.is_registrant():
                if i.get_last_wk_state_observations_for_registrant():
                    return True

        return False


    def check_has_wk_state_obs_for_health_conditions_step(self):
        ctdetail_fields = (u'health_condition',)
        has = False
        if self.holder.is_reviser() or self.holder.is_evaluator():
            has = self._has_history_for_ctdetail_fields_for_reviser(ctdetail_fields)
        if self.holder.is_registrant():
            has = self._has_history_for_ctdetail_fields_for_registrant(ctdetail_fields)
        if has:
            return True

        for i in self.descriptor_set.exclude(descriptor_type=u'I'):
            if self.holder.is_reviser() or self.holder.is_evaluator():
                if i.get_last_wk_state_observations_for_reviser():
                    return True
            if self.holder.is_registrant():
                if i.get_last_wk_state_observations_for_registrant():
                    return True

        return False

    def check_has_wk_state_obs_for_interventions_step(self):
        ctdetail_fields = (u'intervention',)
        has = False
        if self.holder.is_reviser() or self.holder.is_evaluator():
            has = self._has_history_for_ctdetail_fields_for_reviser(ctdetail_fields)
        if self.holder.is_registrant():
            has = self._has_history_for_ctdetail_fields_for_registrant(ctdetail_fields)
        if has:
            return True
        if self.holder.is_registrant():
            if self._has_history_for_ct_fields_for_registrant((u'intervention_codes',)):
                return True
        if self.holder.is_reviser() or self.holder.is_evaluator():
            if self._has_history_for_ct_fields_for_reviser((u'intervention_codes',)):
                return True


        for i in self.descriptor_set.filter(descriptor_type=u'I'):
            if self.holder.is_reviser() or self.holder.is_evaluator():
                if i.get_last_wk_state_observations_for_reviser():
                    return True
            if self.holder.is_registrant():
                if i.get_last_wk_state_observations_for_registrant():
                    return True

        for i in self.intervention_observations.all():
            if self.holder.is_reviser() or self.holder.is_evaluator():
                if i.get_last_wk_state_observations_for_reviser():
                    return True
            if self.holder.is_registrant():
                if i.get_last_wk_state_observations_for_registrant():
                    return True


        for i in self.interventions.all():
            if self.holder.is_reviser() or self.holder.is_evaluator():
                if i.get_last_wk_state_observations_for_reviser():
                    return True
            if self.holder.is_registrant():
                if i.get_last_wk_state_observations_for_registrant():
                    return True

        return False


    def check_has_wk_state_obs_for_recruitment_step(self):
        ctdetail_fields = (u'inclusion_criteria', u'exclusion_criteria')
        has = False
        if self.holder.is_reviser() or self.holder.is_evaluator():
            has = self._has_history_for_ctdetail_fields_for_reviser(ctdetail_fields)
        if self.holder.is_registrant():
            has = self._has_history_for_ctdetail_fields_for_registrant(ctdetail_fields)
        if has:
            return True

        fields = ('recruitment_countries',
                         'date_first_enrollment', 'date_last_enrollment',
                         'gender', 'minimum_age_no_limit',
                         'inclusion_minimum_age', 'minimum_age_unit',
                         'maximum_age_no_limit', 'inclusion_maximum_age',
                         'maximum_age_unit', 'world_target_size',
                         'brazil_target_size', 'statistical_sample_analysis',)

        if self.holder.is_registrant():
            if self._has_history_for_ct_fields_for_registrant(fields):
                return True
        if self.holder.is_reviser() or self.holder.is_evaluator():
            if self._has_history_for_ct_fields_for_reviser(fields):
                return True

        return False


    def check_has_wk_state_obs_for_study_type_step(self):
        fields = (u'study_purpose', u'expanded_access_program',
             u'intervention_assignment', u'number_arms', u'masking_type',
             u'allocation_type', u'study_phase', u'followup_period',
             u'reference_period')
        if self.holder.is_registrant():
            if self._has_history_for_ct_fields_for_registrant(fields):
                return True
        if self.holder.is_reviser() or self.holder.is_evaluator():
            if self._has_history_for_ct_fields_for_reviser(fields):
                return True

        return False


    def check_has_wk_state_obs_for_outcomes_step(self):
        for i in self.outcome_set.all():
            if self.holder.is_reviser() or self.holder.is_evaluator():
                if i.get_last_wk_state_observations_for_reviser():
                    return True
            if self.holder.is_registrant():
                if i.get_last_wk_state_observations_for_registrant():
                    return True

    def check_has_wk_state_obs_for_contacts_step(self):
        for i in self.ctcontact_set.all():
            if self.holder.is_reviser() or self.holder.is_evaluator():
                if i.get_last_wk_state_observations_for_reviser():
                    return True
            if self.holder.is_registrant():
                if i.get_last_wk_state_observations_for_registrant():
                    return True

    def check_has_wk_state_obs_for_attachments_step(self):
        for i in self.ctattachment_set.all():
            if self.holder.is_reviser() or self.holder.is_evaluator():
                if i.get_last_wk_state_observations_for_reviser():
                    return True
            if self.holder.is_registrant():
                if i.get_last_wk_state_observations_for_registrant():
                    return True

    def _has_history_for_ctdetail_fields_for_reviser(self, fields_names):
        last_time_reviser = StateObjectHistory.objects.filter(
                                content_id=self.id,
                                state_id__in=(State.STATE_EM_REVISAO,
                                              State.STATE_EM_AVALIACAO)
                               ).order_by(u'-update_date')
        #If the 3rd last state was evaluation, state, we need to consider that date
        if last_time_reviser.count() >= 3 \
            and last_time_reviser[2].state_id == State.STATE_EM_AVALIACAO:
            last_time_reviser = last_time_reviser[2].update_date
        else:
            #Try to get the 2-times last, if it doesn't exists,
            #get the last time of revision.. and if it not exsits(still in evaluation)
            #so use the 1920 year date(not matter the date fr this case)
            last_time_reviser = last_time_reviser[1].update_date \
                                        if last_time_reviser.count() > 1 \
                                        else (
                                            last_time_reviser[0].update_date \
                                                if last_time_reviser.exists() \
                                                else datetime(1920, 1, 1)
                                        )
        query = StateObjectFieldObservation.objects.filter(
            field_name__in=fields_names,
            content_id__in=self.ctdetail_set.all().values_list(u'pk', flat=True),
            content_type_id=ContentType.objects.get_for_model(CTDetail),
            update_date__gt=last_time_reviser
        )

        if self.date_last_publication:
            query = query.filter(update_date__gt=self.date_last_publication)

        return query.exists()

    def _has_history_for_ctdetail_fields_for_registrant(self, fields_names):
        last_time_registrant = StateObjectHistory.objects.filter(
                                content_id=self.id,
                                state_id__in=(State.STATE_EM_PREENCHIMENTO,
                                              State.STATE_RESUBMETIDO)
                               ).order_by(u'-update_date')
        last_time_registrant = last_time_registrant[1] \
                                    if last_time_registrant.count() > 1 \
                                    else last_time_registrant[0]

        query =  StateObjectFieldObservation.objects.filter(
            field_name__in=fields_names,
            content_id__in=self.ctdetail_set.all().values_list(u'pk', flat=True),
            content_type_id=ContentType.objects.get_for_model(CTDetail),
            update_date__gt=last_time_registrant.update_date
        )

        if self.date_last_publication:
            query = query.filter(update_date__gt=self.date_last_publication)

        return query.exists()

    def get_remaining_days(self):
        state = self.get_workflow_state()
        days_state = StateDaysLimit.objects.filter(state=state)

        color = ""
        days_remaining = ""
        for i in days_state:
            days_passed = (datetime.now().date() - self.date_last_state_change).days
            days_remaining = i.red - days_passed

            if (days_passed >= 0 and days_passed < i.green):
                color = "green"
            elif (days_passed <= i.yellow):
                color = "yellow"
            elif (days_passed <= i.red):
                color = "red"
            elif (days_passed > i.red):
                color = "black"

        return days_remaining, color

    def _has_history_for_ct_fields_for_registrant(self, fields_names):
        last_time_registrant = StateObjectHistory.objects.filter(
                                content_id=self.id,
                                state_id__in=(State.STATE_EM_PREENCHIMENTO,
                                              State.STATE_RESUBMETIDO)
                               ).order_by(u'-update_date')
        last_time_registrant = last_time_registrant[1] \
                                    if last_time_registrant.count() > 1 \
                                    else last_time_registrant[0]
        query = StateObjectFieldObservation.objects.filter(
            field_name__in=fields_names,
            content_id=self.id,
            content_type_id=ContentType.objects.get_for_model(ClinicalTrial),
            update_date__gt=last_time_registrant.update_date
        )

        if self.date_last_publication:
            query = query.filter(update_date__gt=self.date_last_publication)

        return query.exists()

    def _has_history_for_ct_fields_for_reviser(self, fields_names):
        last_time_reviser = StateObjectHistory.objects.filter(
                                content_id=self.id,
                                state_id__in=(State.STATE_EM_REVISAO,
                                              State.STATE_EM_AVALIACAO)
                               ).order_by(u'-update_date')

        #If the 3rd last state was evaluation, state, we need to consider that date
        if last_time_reviser.count() >= 3 \
            and last_time_reviser[2].state_id == State.STATE_EM_AVALIACAO:
            last_time_reviser = last_time_reviser[2].update_date
        else:
            #Try to get the 2-times last, if it doesn't exists,
            #get the last time of revision.. and if it not exsits(still in evaluation)
            #so use the 1920 year date(not matter the date fr this case)
            last_time_reviser = last_time_reviser[1].update_date \
                                        if last_time_reviser.count() > 1 \
                                        else (
                                            last_time_reviser[0].update_date \
                                                if last_time_reviser.exists() \
                                                else datetime(1920, 1, 1)
                                        )

        query =  StateObjectFieldObservation.objects.filter(
            field_name__in=fields_names,
            content_id=self.id,
            content_type_id=ContentType.objects.get_for_model(ClinicalTrial),
            update_date__gt=last_time_reviser
        )

        if self.date_last_publication:
            query = query.filter(update_date__gt=self.date_last_publication)

        return query.exists()

    def can_be_deleted(self):
        return self.code is None and self.get_workflow_state().pk in(
            State.STATE_EM_PREENCHIMENTO,
            State.STATE_RESUBMETIDO,
            State.STATE_REVISANDO_PREENCHIMENTO,
            State.STATE_REVISAO_PREENCHIMENTO_RESUBMETIDO
        )

    def get_public_title(self):
        return self.ctdetail_set.first().public_title

    def get_help_text(self):
        """Given a field name, return it's help text."""
        print "ok"
        for field in self._meta.fields:
            if self.field.name == field:
                return field.help_text

    def save_historical_data(self):
        if self.allocation_type:
            self.allocation_type.save(using=u'published_cts')
        if self.group:
            self.group.save(using=u'published_cts')
        if self.holder:
            self.holder.country.save(using=u'published_cts')
            self.holder.save(using=u'published_cts')
        if self.user:
            self.user.country.save(using=u'published_cts')
            self.user.save(using=u'published_cts')
        if self.intervention_assignment:
            self.intervention_assignment.save(using=u'published_cts')
        if self.masking_type:
            self.masking_type.save(using=u'published_cts')
        if self.study_phase:
            self.study_phase.save(using=u'published_cts')
        if self.study_purpose:
            self.study_purpose.save(using=u'published_cts')
        #self.save(using=u'published_cts')
        ClinicalTrial.objects.get(pk=self.pk).save(using=u'published_cts')
        for i in self.ctattachment_set.using(u'default').all():
            if i.attachment.setup.language_1_code:
                i.attachment.setup.language_1_code.save(using=u'published_cts')
            if i.attachment.setup.language_2_code:
                i.attachment.setup.language_2_code.save(using=u'published_cts')
            if i.attachment.setup.language_3_code:
                i.attachment.setup.language_3_code.save(using=u'published_cts')
            i.attachment.setup.save(using=u'published_cts')
            i.attachment.save(using=u'published_cts')
            i.save(using=u'published_cts')

        for i in self.ctcontact_set.using(u'default').all():
            if i.contact:
                i.contact.country.save(using=u'published_cts')
                if i.contact.institution:
                    i.contact.institution.country.save(using=u'published_cts')
                    i.contact.institution.institution_type.save(
                        using=u'published_cts')
                    i.contact.institution.save(using=u'published_cts')
                i.contact.save(using=u'published_cts')
            i.save(using=u'published_cts')

        for i in self.ctdetail_set.using(u'default').all():
            i.save(using=u'published_cts')

        for i in self.descriptor_set.using(u'default').all():
            i.save(using=u'published_cts')

        for i in self.identifier_set.using(u'default').all():
            i.setup_identifier.save(using=u'published_cts')
            i.save(using=u'published_cts')

        for i in self.interventions.using(u'default').all():
            i.save(using=u'published_cts')
            ClinicalTrial.objects.using(u'published_cts').get(
                pk=self.pk).interventions.add(i)
            #ver para adicionar ao ct

        for i in self.recruitment_countries.using(u'default').all():
            i.save(using=u'published_cts')
            ClinicalTrial.objects.using(u'published_cts').get(
                pk=self.pk).recruitment_countries.add(i)
            #ver para adicionar ao ct

        for i in self.recruitment_interruptions.using(u'default').all():
            i.save(using=u'published_cts')


        for i in self.intervention_observations.using(u'default').all():
            i.save(using=u'published_cts')

        for i in self.interventions.using(u'default').all():
            i.save(using=u'published_cts')

        for i in self.outcome_set.using(u'default').all():
            i.save(using=u'published_cts')


        for i in self.sponsor_set.using(u'default').all():
            if i.institution:
                i.institution.country.save(using=u'published_cts')
                i.institution.institution_type.save(using=u'published_cts')
                i.institution.save(using=u'published_cts')
            i.save(using=u'published_cts')

        for i in self.studystatus_set.using(u'default').all():
            i.save(using=u'published_cts')


        return ClinicalTrial.objects.using(u'published_cts').get(pk=self.pk)

post_init.connect(post_init_clinical_trial, sender=ClinicalTrial)
post_save.connect(handle_initial_state, dispatch_uid="workflow_init", weak=False, sender=ClinicalTrial)
pre_delete.connect(handle_workflow_ct_deleted, dispatch_uid="workflow_delete", weak=False, sender=ClinicalTrial)

class RecruitmentInterrupt(models.Model):
    """
    Represents a action to interrupt/cancel interruption of a recruitment.
    """
    clinical_trial = models.ForeignKey(
        ClinicalTrial,
        related_name=u'recruitment_interruptions',
        verbose_name=u'Clinical trial',
         
    )
    date_time = models.DateTimeField(
        auto_now_add=True
    )
    interrupt = models.BooleanField()
    comments = models.TextField(
        _(u'Comments'), null=True, blank=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name=_(u'User'),
    )

    class Meta:
        verbose_name = u'Recruitment Interruption'
        verbose_name_plural = u'Recruitment Interruptions'

class CTDetail(models.Model):
    """Defines the details for the clinical trial in one specified language"""
    clinical_trial = models.ForeignKey(
        'ClinicalTrial',
        verbose_name=_('Clinical trial'),
         
    )

    language_code = models.CharField(
        max_length=100,
        verbose_name=_('Language code'),
         
    )

    scientific_title = models.TextField(
        null=True,
        blank=True,
        verbose_name=_('Scientific title'))
    
    public_title = models.TextField(
        null=True,
        blank=True,
        verbose_name=_('Public title'))

    acronym = models.TextField(
        null=True,
        blank=True,
        verbose_name=_('Acronym'))

    study_description = models.TextField(
        null=False,
        blank=False,
        verbose_name=_('Study description'),
    )

    health_condition = models.TextField(
        null=True,
        blank=True,
        verbose_name=_('Health condition'))

    intervention = models.TextField(
        null=True,
        blank=True,
        verbose_name=_('Intervention'))

    inclusion_criteria = models.TextField(
        null=True,
        blank=True,
        verbose_name=_('Inclusion criteria'))

    exclusion_criteria = models.TextField(
        null=True,
        blank=True,
        verbose_name=_('Exclusion criteria'))


    class Meta:
        verbose_name = _('Detail')
        verbose_name_plural = _('Details')

    def __unicode__(self):
        return self.language_code

    def get_last_wk_state_observations_for_reviser(self):
        fields_names = [i.name for i in self._meta.fields + self._meta.many_to_many]

        last_time_reviser = StateObjectHistory.objects.filter(
                                content_id=self.clinical_trial.id,
                                state_id__in=(State.STATE_EM_REVISAO,
                                              State.STATE_EM_AVALIACAO)
                               ).order_by(u'-update_date')
        #If the 3rd last state was evaluation, state, we need to consider that date
        if last_time_reviser.count() >= 3 \
            and last_time_reviser[2].state_id == State.STATE_EM_AVALIACAO:
            last_time_reviser = last_time_reviser[2].update_date
        else:
            #Try to get the 2-times last, if it doesn't exists,
            #get the last time of revision.. and if it not exsits(still in evaluation)
            #so use the 1920 year date(not matter the date fr this case)
            last_time_reviser = last_time_reviser[1].update_date \
                                        if last_time_reviser.count() > 1 \
                                        else (
                                            last_time_reviser[0].update_date \
                                                if last_time_reviser.exists() \
                                                else datetime(1920, 1, 1)
                                        )

        query = StateObjectFieldObservation.objects.filter(
            field_name__in=fields_names,
            content_id=self.id,
            content_type_id=ContentType.objects.get_for_model(self),
            update_date__gt=last_time_reviser
        ).order_by(u'update_date').values_list(u'field_name', flat=True)

        if self.clinical_trial.date_last_publication:
            query = query.filter(update_date__gt=self.clinical_trial.date_last_publication)

        return query

    def get_last_wk_state_observations_for_registrant(self):
        fields_names = [i.name for i in self._meta.fields + self._meta.many_to_many]
        last_time_registrant = StateObjectHistory.objects.filter(
                                content_id=self.clinical_trial.id,
                                state_id__in=(State.STATE_EM_PREENCHIMENTO,
                                              State.STATE_RESUBMETIDO)
                               ).order_by(u'-update_date')
        last_time_registrant = last_time_registrant[1] \
                                    if last_time_registrant.count() > 1 \
                                    else last_time_registrant[0]
        query = StateObjectFieldObservation.objects.filter(
            field_name__in=fields_names,
            content_id=self.id,
            content_type_id=ContentType.objects.get_for_model(self),
            update_date__gt=last_time_registrant.update_date

        ).order_by(u'update_date').values_list(u'field_name', flat=True)

        if self.clinical_trial.date_last_publication:
            query = query.filter(update_date__gt=self.clinical_trial.date_last_publication)

        return query

  
class Outcome(models.Model):
    """Defines the outcomes of the clinical trial"""

    TYPE_PRIMARY = u'P'
    TYPE_SECONDARY = u'S'

    TYPE_CHOICES = (
        (TYPE_PRIMARY, _('Primary')),
        (TYPE_SECONDARY, _('Secondary'))
    )
    clinical_trial = models.ForeignKey(
        'ClinicalTrial',
        verbose_name=_('CTDetail'),
         
    )

    outcome_type = models.CharField(
        max_length=1,
        choices=TYPE_CHOICES,
        verbose_name=_('Outcome type'),
        help_text=('<p>O registrante deve inserir nesse campo o(s) desfecho(s) primário(s) esperado(s) no estudo e descrever as medidas e parâmetros utilizados na avaliação dos desfechos. PARA O CASO DE EXISTIREM VÁRIOS DESFECHOS PRIMÁRIOS, O REGISTRANTE DEVE ELEGER UM PRINCIPAL, OU DE MAIS IMPORTÂNCIA E/OU PESO. DESFECHOS PRIMÁRIOS SÃO EVENTOS, VARIÁVEIS OU EXPERIÊNCIAS QUE SÃO MEDIDAS PORQUE SE SUPÕE QUE SEJAM INFLUENCIADAS PELO OBJETO EM ESTUDO. O DESFECHO PRIMÁRIO É O DESFECHO ESPERADO E QUE FOI UTILIZADO PELO PESQUISADOR PARA CALCULAR O TAMANHO DA AMOSTRA E/OU DETERMINAR OS EFEITOS DAS INTERVENÇÕES. São, enfim, os achados, outcomes, endpoints, os “resultados” observados ao final do estudo.</p><br /><p>O registrante deve inserir nesse campo o(s) desfecho(s) secundário(s), caso este(s) seja(m) esperado(s) no estudo, e descrever as medidas e parâmetros utilizados na avaliação do(s) desfecho(s). Desfechos secundários são geralmente são dados que surgem durante o estudo e/ou são previstos antes do início do mesmo, e estes são usados para avaliar efeitos adicionais da intervenção.<br />Por exemplo: Um desfecho secundário 1) pode envolver o mesmo evento, variável ou experiência do desfecho primário, mas medido em um período de tempo distinto do desfecho primário. Desfecho primário: mortalidade por todas as causas em 5 anos; desfecho secundário: mortalidade por todas as causas em 1 ano e 3 anos; OU 2) pode estar relacionado a um evento, variável ou experiência distinta. Desfecho primário: mortalidade por todas as causas após 5 anos; desfecho secundário: taxa de hospitalização em 5 anos.</p>'),
    )

    name = models.CharField(
        max_length=20000,
        verbose_name=_('Name'),
        help_text = ('<p>É o desfecho que será avaliado com a intervenção. NÃO USAR ABREVIATURAS.</p><p>Exemplo 1:</p><p>Nome: Diminuição da depressão;</p><p>Exemplo 2:</p><p>-  Nome: Aderência a medicação para HIV.</p><p>Exemplo 3: </p><p>- Nome: Aumento da secreção de vitamina E no leite materno após suplementação.</p>')
    )

    measurement_method = models.CharField(
        max_length=20000,
        null=False,
        blank=False,
        verbose_name=_('Measurement Method'),
        help_text=('<p>É a métrica ou método de medição utilizado para medir o desfecho. SER O MAIS ESPECÍFICO POSSÍVEL;</p><p>Exemplo 1:</p><p>Método de medição: Questionário de Beck.</p><p>Exemplo 2:</p><p>- Método de Medição: 1) técnicas de observação de campo (rotina do paciente na instituição: idas à farmácia, freqüência das consultas médicas, etc.);</p><p>2) análise do banco de dados da farmácia (quantidade de pacientes inscritos para retirada de ARV, freqüência de retirada, sexo e faixa etária dos pacientes que fazem uso de ARV); 3) Roteiro estruturado da entrevista.</p><p>Exemplo 3:</p><p>-  Método de Medição: Verificado por meio da determinação da concentração de alfa-tocoferol no leite materno antes e depois da suplementação.</p>')
    )

    point_of_time_of_interest = models.CharField(
        max_length=20000,
        null=False,
        blank=False,
        verbose_name=_('Point of time of interest'),
        help_text=('<p>Momento em que a medida de resultado é avaliada. NÃO é a mesma coisa que duração do estudo ou período de seguimento do estudo.</p><p>Exemplo 1:<br />Ponto de tempo de Interesse: 18 semanas após o fim do tratamento.</p><p>Exemplo 2:<br />- Ponto de tempo de interesse: Um ano durante o tratamento.</p><p>Exemplo 3:<br />-  Ponto de tempo de interesse: Uma semana após a suplementação.</p>')
    )

    found_outcome = models.TextField(
        max_length=20000,
        null=True,
        blank=True,
        verbose_name=_('Found Outcome'),
        help_text = ('<p>Tanto no campo “Desfecho Primário” Quanto no campo “Desfecho Secundário” CASO O ESTUDO JÁ ESTEJA CONCLUÍDO, use o campo Desfecho Encontrado para descrever os desfechos efetivamente observados ao término do estudo.</p><p>Exemplo 1:<br />Desfecho encontrado</p><p>Mulheres que receberam suplementação apresentaram maiores concentrações do alfa-tocoferol no colostro do que o grupo controle, com 57% e 39% de aumento nas mulheres suplementadas com as formas natural e sintética da vitamina E, respectivamente.</p><p>Exemplo 2: Desfechos encontrados:</p><p>Os resultados mostraram que para os aderentes, a soropositividade aparece associada a uma nova normatividade (tomar remédios). O remédio é objetivado destruindo o vírus e existe a ancoragem no saber científico. Para os não aderentes, a soropositividade é uma vivência ameaçadora. O uso da negação impede a ancoragem no saber científico. Os remédios tornam-se a objetivação da doença. Como conclusão, pode-se afirmar que,enquanto nos pacientes aderentes observou-se RS  bem estruturadas da doença e do tratamento ARV, nos não aderentes tais RS parecem ainda estar em construção.</p>')
    )

    class Meta:
        verbose_name = _('Outcome')
        verbose_name_plural = _('Outcomes')

    def __unicode__(self):
        return self.name

    def get_last_wk_state_observations_for_reviser(self):
        fields_names = [i.name for i in self._meta.fields + self._meta.many_to_many]

        last_time_reviser = StateObjectHistory.objects.filter(
                                content_id=self.clinical_trial.id,
                                state_id__in=(State.STATE_EM_REVISAO,
                                              State.STATE_EM_AVALIACAO)
                               ).order_by(u'-update_date')
        #If the 3rd last state was evaluation, state, we need to consider that date
        if last_time_reviser.count() >= 3 \
            and last_time_reviser[2].state_id == State.STATE_EM_AVALIACAO:
            last_time_reviser = last_time_reviser[2].update_date
        else:
            #Try to get the 2-times last, if it doesn't exists,
            #get the last time of revision.. and if it not exsits(still in evaluation)
            #so use the 1920 year date(not matter the date fr this case)
            last_time_reviser = last_time_reviser[1].update_date \
                                        if last_time_reviser.count() > 1 \
                                        else (
                                            last_time_reviser[0].update_date \
                                                if last_time_reviser.exists() \
                                                else datetime(1920, 1, 1)
                                        )

        query = StateObjectFieldObservation.objects.filter(
            field_name__in=fields_names,
            content_id=self.id,
            content_type_id=ContentType.objects.get_for_model(self),
            update_date__gt=last_time_reviser
        ).order_by(u'update_date').values_list(u'field_name', flat=True)


        if self.clinical_trial.date_last_publication:
            query = query.filter(update_date__gt=self.clinical_trial.date_last_publication)

        return query


    def get_last_wk_state_observations_for_registrant(self):
        fields_names = [i.name for i in self._meta.fields + self._meta.many_to_many]
        last_time_registrant = StateObjectHistory.objects.filter(
                                content_id=self.clinical_trial.id,
                                state_id__in=(State.STATE_EM_PREENCHIMENTO,
                                              State.STATE_RESUBMETIDO)
                               ).order_by(u'-update_date')
        last_time_registrant = last_time_registrant[1] \
                                    if last_time_registrant.count() > 1 \
                                    else last_time_registrant[0]
        query = StateObjectFieldObservation.objects.filter(
            field_name__in=fields_names,
            content_id=self.id,
            content_type_id=ContentType.objects.get_for_model(self),
            update_date__gt=last_time_registrant.update_date
        ).order_by(u'update_date').values_list(u'field_name', flat=True)

        if self.clinical_trial.date_last_publication:
            query = query.filter(update_date__gt=self.clinical_trial.date_last_publication)

        return query


    def get_language_code_1(self):
        lang = Setup.objects.get().language_1_code
        if lang:
            return lang.description

        return ''

    def get_language_code_2(self):
        lang = Setup.objects.get().language_2_code
        if lang:
            return lang.description

        return ''

    def get_language_code_3(self):
        lang = Setup.objects.get().language_3_code
        if lang:
            return lang.description

        return ''
        

class StudyStatus(models.Model):
    """Defines the status of the clinical trial"""
    clinical_trial = models.ForeignKey(
        'ClinicalTrial',
        verbose_name=_('ClinicalTrial'),
         )

    description = models.CharField(
        max_length=100,
        verbose_name=_('Description'),
         )

    class Meta:
        verbose_name = _('Study status')
        verbose_name_plural = _('Study status')

    def __unicode__(self):
        return self.description

    def get_last_wk_state_observations_for_reviser(self):
        fields_names = [i.name for i in self._meta.fields + self._meta.many_to_many]


        last_time_reviser = StateObjectHistory.objects.filter(
                                content_id=self.clinical_trial.id,
                                state_id__in=(State.STATE_EM_REVISAO,
                                              State.STATE_EM_AVALIACAO)
                               ).order_by(u'-update_date')
        #If the 3rd last state was evaluation, state, we need to consider that date
        if last_time_reviser.count() >= 3 \
            and last_time_reviser[2].state_id == State.STATE_EM_AVALIACAO:
            last_time_reviser = last_time_reviser[2].update_date
        else:
            #Try to get the 2-times last, if it doesn't exists,
            #get the last time of revision.. and if it not exsits(still in evaluation)
            #so use the 1920 year date(not matter the date fr this case)
            last_time_reviser = last_time_reviser[1].update_date \
                                        if last_time_reviser.count() > 1 \
                                        else (
                                            last_time_reviser[0].update_date \
                                                if last_time_reviser.exists() \
                                                else datetime(1920, 1, 1)
                                        )

        query = StateObjectFieldObservation.objects.filter(
            field_name__in=fields_names,
            content_id=self.id,
            content_type_id=ContentType.objects.get_for_model(self),
            update_date__gt=last_time_reviser
        ).order_by(u'update_date').values_list(u'field_name', flat=True)


        if self.clinical_trial.date_last_publication:
            query = query.filter(update_date__gt=self.clinical_trial.date_last_publication)

        return query
        

    def get_last_wk_state_observations_for_registrant(self):
        fields_names = [i.name for i in self._meta.fields + self._meta.many_to_many]
        last_time_registrant = StateObjectHistory.objects.filter(
                                content_id=self.clinical_trial.id,
                                state_id__in=(State.STATE_EM_PREENCHIMENTO,
                                              State.STATE_RESUBMETIDO)
                               ).order_by(u'-update_date')
        query = StateObjectFieldObservation.objects.filter(
            field_name__in=fields_names,
            content_id=self.id,
            content_type_id=ContentType.objects.get_for_model(self),
            update_date__gt=last_time_registrant.update_date
        ).order_by(u'update_date').values_list(u'field_name', flat=True)

        if self.clinical_trial.date_last_publication:
            query = query.filter(update_date__gt=self.clinical_trial.date_last_publication)

        return query


class Identifier(models.Model):
    """
    Defines the associated code/value between the
    SetupIdentifier and a ClinicalTrial
    """
    setup_identifier = models.ForeignKey(
        'administration.SetupIdentifier',
        verbose_name=_('Identifier'),
        help_text=u'r')

    clinical_trial = models.ForeignKey(
        'ClinicalTrial',
        verbose_name=_('Clinical trial'),
         )

    code = models.CharField(
        max_length=100,
        verbose_name=_('Code'),
        help_text=u'r'
    )

    description = models.TextField(
        verbose_name=_(u'Description'),
        null=True,
        blank=True,
        help_text=u'r'
    )
    description = models.CharField(
        verbose_name=_(u'Description'),
        null=True,
        blank=True,
        max_length=2000,
        help_text=u'r'
    )
    date = models.DateField(
        verbose_name=_(u'Date'),
        null=True,
        blank=True,
        help_text=u'r'
    )

    class Meta:
        verbose_name = _('Identifier')
        verbose_name_plural = _('Identifiers')
        #unique_together = (u'setup_identifier', u'clinical_trial')

    def __unicode__(self):
        return self.code

    def get_last_wk_state_observations_for_reviser(self):
        fields_names = [i.name for i in self._meta.fields + self._meta.many_to_many]
        last_time_reviser = StateObjectHistory.objects.filter(
                                content_id=self.clinical_trial.id,
                                state_id__in=(State.STATE_EM_REVISAO,
                                              State.STATE_EM_AVALIACAO)
                               ).order_by(u'-update_date')
        #If the 3rd last state was evaluation, state, we need to consider that date
        if last_time_reviser.count() >= 3 \
            and last_time_reviser[2].state_id == State.STATE_EM_AVALIACAO:
            last_time_reviser = last_time_reviser[2].update_date
        else:
            #Try to get the 2-times last, if it doesn't exists,
            #get the last time of revision.. and if it not exsits(still in evaluation)
            #so use the 1920 year date(not matter the date fr this case)
            last_time_reviser = last_time_reviser[1].update_date \
                                        if last_time_reviser.count() > 1 \
                                        else (
                                            last_time_reviser[0].update_date \
                                                if last_time_reviser.exists() \
                                                else datetime(1920, 1, 1)
                                        )
        query = StateObjectFieldObservation.objects.filter(
            field_name__in=fields_names,
            content_id=self.id,
            content_type_id=ContentType.objects.get_for_model(self),
            update_date__gt=last_time_reviser
        ).order_by(u'update_date').values_list(u'field_name', flat=True)

        if self.clinical_trial.date_last_publication:
            query = query.filter(update_date__gt=self.clinical_trial.date_last_publication)

        return query

    def get_last_wk_state_observations_for_registrant(self):
        fields_names = [i.name for i in self._meta.fields + self._meta.many_to_many]
        last_time_registrant = StateObjectHistory.objects.filter(
                                content_id=self.clinical_trial.id,
                                state_id__in=(State.STATE_EM_PREENCHIMENTO,
                                              State.STATE_RESUBMETIDO)
                               ).order_by(u'-update_date')
        last_time_registrant = last_time_registrant[1] \
                                    if last_time_registrant.count() > 1 \
                                    else last_time_registrant[0]
        query = StateObjectFieldObservation.objects.filter(
            field_name__in=fields_names,
            content_id=self.id,
            content_type_id=ContentType.objects.get_for_model(self),
            update_date__gt=last_time_registrant.update_date
        ).order_by(u'update_date').values_list(u'field_name', flat=True)

        if self.clinical_trial.date_last_publication:
            query = query.filter(update_date__gt=self.clinical_trial.date_last_publication)

        return query


class Descriptor(models.Model):
    """Defines a descriptor for the vocabulary"""
    TYPE_GENERAL = u'G'
    TYPE_SPECIFIC = u'S'
    TYPE_CHOICES = (
        (TYPE_GENERAL, _('General')),
        (TYPE_SPECIFIC, _('Specific'))
    )


    VOCABULARY_CID10 = u'CID-10'
    VOCABULARY_DECS = u'DeCS'
    VOCABULARY_CAS = u'CAS'
    VOCABULARY_CHOICES = (
        (VOCABULARY_CID10, u'CID-10'),
        (VOCABULARY_DECS, u'DeCS'),
        (VOCABULARY_CAS, u'CAS')
    )

    VOCABULARY_CHOICES_WITHOUT_CAS = (
        (VOCABULARY_CID10, u'CID-10'),
        (VOCABULARY_DECS, u'DeCS'),
    )

    clinical_trial = models.ForeignKey(
        'ClinicalTrial',
        verbose_name=_('Clinical trial'),
         
    )

    parent_vocabulary = models.CharField(
        max_length=6,
        verbose_name=_(u'Parent Vocabulary'),
        choices=VOCABULARY_CHOICES,
        help_text=('<p>O VOCABULÁRIO é o conjunto padronizado de termos e palavras que são usados para uniformizar a identificação e descrição dos assuntos, temas e aspectos tratados na produção científica da área de saúde. Há dois vocabulários controlados de descritores, o DeCS (Descritores em Ciências da Saúde) ou CID-10 (Classificação Internacional de Doenças). Ao preencher o campo VOCABULÁRIO você deve escolher UM dos dois vocabulários e buscar o(s) termo(s) mais adequado(s) ao seu estudo.</p>')
    )

    vocabulary_item_en = models.CharField(
        max_length=4000,
        verbose_name=_(u'Vocabulary Item English'),
         
    )

    vocabulary_item_es = models.CharField(
        max_length=4000,
        verbose_name=_(u'Vocabulary Item Spanish'),
         
    )

    vocabulary_item_pt = models.CharField(
        max_length=4000,
        verbose_name=_(u'Vocabulary Item Portuguese'),
    )

    descriptor_type = models.CharField(
        max_length=1,
        choices=TYPE_CHOICES,
        verbose_name=_('Descriptor type'),
        help_text=('<p>No campo TIPO DE DESCRITOR para as CONDIÇÕES DE SAÚDE OU PROBLEMAS você deve escolher entre DESCRITOR GERAL e DESCRITOR ESPECÍFICO. ATENÇÃO é OBRIGATÓRIO a exestiência dos DOIS descritores, tanto o GERAL quando o ESPECÍFICO.ATENÇÃO: É possível informar mais de um Descritor Específico e mais de um Descritor Geral clicando em adicionar outro.</p>O DESCRITOR GERAL designa o objeto de interesse de forma ampla permitindo ao interessado fazer uma primeira seleção e triagem a partir de categorias mais abrangentes como Doenças Respiratórias - C08, Saúde da Mulher - SP2.006.022 ou Equilíbrio Postural - G11.427.690 .</p><p>DESCRITOR ESPECÍFICO designa especificamente a condição de saúde ou problema estudado. Ele deve ser também coerente com o DESCRITOR GERAL. O registrante deverá escolher, dentre os descritores DeCS (Descritores em Ciências da Saúde) ou CID-10 (Classificação Internacional de Doenças), o mais adequado e específico TOMANDO POR BASE AS CONDIÇÕES DE SAÚDE RELATADAS NO CAMPO “CONDIÇÕES DE SAUDE OU PROBLEMAS;</p><p>Exemplo: Se no campo “CONDIÇÕES DE SAÚDE OU PROBLEMAS” Foi Indicado (Hipertensão, Claudicação Intermitente) no campo “DESCRITORES ESPECÍFICOS PARA AS CONDIÇÕES DE SAÚDE” devem constar os seguintes descritores: "Hipertensão - C14.907.489", Claudicação Intermitente - C14.907.137.126.669.</p>'))

    class Meta:
        verbose_name = _('Descriptor')
        verbose_name_plural = _('Descriptors')

    def __unicode__(self):
        return self.descriptor_type

    def get_vocabulary_item_in_current_ct_languages(self):
        setup = Setup.objects.get()
        languages = []
        if self.clinical_trial.language_1_mandatory:
            code = setup.language_1_code
            code_attr = getattr(self,
                                u'vocabulary_item_%s' % code.code.split('-')[0]
                                )
            languages.append({u'language': code,
                              u'text': code_attr})
        if self.clinical_trial.language_2_mandatory:
            code = setup.language_2_code
            code_attr = getattr(self,
                                u'vocabulary_item_%s' % code.code.split('-')[0]
                                )
            languages.append({u'language': code,
                              u'text': code_attr})
        if self.clinical_trial.language_3_mandatory:
            code = setup.language_3_code
            code_attr = getattr(self,
                                u'vocabulary_item_%s' % code.code.split('-')[0]
                                )
            languages.append({u'language': code,
                              u'text': code_attr})
        
        return languages

    def get_last_wk_state_observations_for_reviser(self):
        fields_names = [i.name for i in self._meta.fields + self._meta.many_to_many]
        last_time_reviser = StateObjectHistory.objects.filter(
                                content_id=self.clinical_trial.id,
                                state_id__in=(State.STATE_EM_REVISAO,
                                              State.STATE_EM_AVALIACAO)
                               ).order_by(u'-update_date')
        #If the 3rd last state was evaluation, state, we need to consider that date
        if last_time_reviser.count() >= 3 \
            and last_time_reviser[2].state_id == State.STATE_EM_AVALIACAO:
            last_time_reviser = last_time_reviser[2].update_date
        else:
            #Try to get the 2-times last, if it doesn't exists,
            #get the last time of revision.. and if it not exsits(still in evaluation)
            #so use the 1920 year date(not matter the date fr this case)
            last_time_reviser = last_time_reviser[1].update_date \
                                        if last_time_reviser.count() > 1 \
                                        else (
                                            last_time_reviser[0].update_date \
                                                if last_time_reviser.exists() \
                                                else datetime(1920, 1, 1)
                                        )

        query = StateObjectFieldObservation.objects.filter(
            field_name__in=fields_names,
            content_id=self.id,
            content_type_id=ContentType.objects.get_for_model(self),
            update_date__gt=last_time_reviser
        ).order_by(u'update_date').values_list(u'field_name', flat=True)

        if self.clinical_trial.date_last_publication:
            query = query.filter(update_date__gt=self.clinical_trial.date_last_publication)

        return query


    def get_last_wk_state_observations_for_registrant(self):
        fields_names = [i.name for i in self._meta.fields + self._meta.many_to_many]
        last_time_registrant = StateObjectHistory.objects.filter(
                                content_id=self.clinical_trial.id,
                                state_id__in=(State.STATE_EM_PREENCHIMENTO,
                                              State.STATE_RESUBMETIDO)
                               ).order_by(u'-update_date')
        last_time_registrant = last_time_registrant[1] \
                                    if last_time_registrant.count() > 1 \
                                    else last_time_registrant[0]
        query = StateObjectFieldObservation.objects.filter(
            field_name__in=fields_names,
            content_id=self.id,
            content_type_id=ContentType.objects.get_for_model(self),
            update_date__gt=last_time_registrant.update_date
        ).order_by(u'update_date').values_list(u'field_name', flat=True)

        if self.clinical_trial.date_last_publication:
            query = query.filter(update_date__gt=self.clinical_trial.date_last_publication)

        return query
        

class CTAttachment(models.Model):
    """Defines the association between a ClinicalTrial and an Attachment"""
    clinical_trial = models.ForeignKey(
        'ClinicalTrial',
        verbose_name=_('Clinical trial'),
         
    )

    attachment = models.ForeignKey(
        'administration.Attachment',
        verbose_name=_('Attachment'),
        help_text=('<p>Para estudos aprovados a partir de 15 de janeiro de 2012 é obrigatório anexar em formato PDF o Parecer Consubstanciado da Plataforma Brasil.</p><p>Para os estudos anteriores a criação da Plataforma Brasil é obrigatório anexar em formato PDF o parecer de aprovação do CEP.</p><p>ATENÇÃO: Anteriomente a Plataforma Brasil o CAAE era gerado pelo SISNEP. É necesssário que o documento anexado tenha o número do CAAE.</p>'),
    )

    file = models.FileField(
        upload_to=u'clinical_trial_attachments',
        null=True,
        blank=True,
        verbose_name=_('File'),
        max_length=5000)

    link = models.URLField(
        null=True,
        blank=True,
        verbose_name=_('Link'),
        max_length=5000)

    description = models.CharField(
        null=True,
        max_length=5000,
        verbose_name=_('Description'),
        help_text = ('<p>No campo DESCRIÇÃO informar o nome do documeto anexado.</p>'),
    )

    public = models.BooleanField(
        default=False,
        verbose_name=_('Public'),
        help_text=\
            _('Would you like to make the access to this information public?')
        )

    class Meta:
        verbose_name = _('CTAttachment')
        verbose_name_plural = _('CTAttachments')

    def __unicode__(self):
        return self.attachment.name

    def get_last_wk_state_observations_for_reviser(self):
        fields_names = [i.name for i in self._meta.fields + self._meta.many_to_many]

        last_time_reviser = StateObjectHistory.objects.filter(
                                content_id=self.clinical_trial.id,
                                state_id__in=(State.STATE_EM_REVISAO,
                                              State.STATE_EM_AVALIACAO)
                               ).order_by(u'-update_date')
        #If the 3rd last state was evaluation, state, we need to consider that date
        if last_time_reviser.count() >= 3 \
            and last_time_reviser[2].state_id == State.STATE_EM_AVALIACAO:
            last_time_reviser = last_time_reviser[2].update_date
        else:
            #Try to get the 2-times last, if it doesn't exists,
            #get the last time of revision.. and if it not exsits(still in evaluation)
            #so use the 1920 year date(not matter the date fr this case)
            last_time_reviser = last_time_reviser[1].update_date \
                                        if last_time_reviser.count() > 1 \
                                        else (
                                            last_time_reviser[0].update_date \
                                                if last_time_reviser.exists() \
                                                else datetime(1920, 1, 1)
                                        )

        query = StateObjectFieldObservation.objects.filter(
            field_name__in=fields_names,
            content_id=self.id,
            content_type_id=ContentType.objects.get_for_model(self),
            update_date__gt=last_time_reviser
        ).order_by(u'update_date').values_list(u'field_name', flat=True)

        if self.clinical_trial.date_last_publication:
            query = query.filter(update_date__gt=self.clinical_trial.date_last_publication)

        return query
        

    def get_last_wk_state_observations_for_registrant(self):
        fields_names = [i.name for i in self._meta.fields + self._meta.many_to_many]
        last_time_registrant = StateObjectHistory.objects.filter(
                                content_id=self.clinical_trial.id,
                                state_id__in=(State.STATE_EM_PREENCHIMENTO,
                                              State.STATE_RESUBMETIDO)
                               ).order_by(u'-update_date')
        last_time_registrant = last_time_registrant[1] \
                                    if last_time_registrant.count() > 1 \
                                    else last_time_registrant[0]
        query = StateObjectFieldObservation.objects.filter(
            field_name__in=fields_names,
            content_id=self.id,
            content_type_id=ContentType.objects.get_for_model(self),
            update_date__gt=last_time_registrant.update_date
        ).order_by(u'update_date').values_list(u'field_name', flat=True)

        if self.clinical_trial.date_last_publication:
            query = query.filter(update_date__gt=self.clinical_trial.date_last_publication)

        return query



class Sponsor(models.Model):
    """Defines a sponsor for the ClinicalTrial"""
    SPONSOR_TYPE_PRIMARY = u'P'
    SPONSOR_TYPE_SECUNDARY = u'S'
    SPONSOR_TYPE_SUPPORT = u'U'
    TYPE_CHOICES = (
        (SPONSOR_TYPE_PRIMARY, _('Primary')),
        (SPONSOR_TYPE_SECUNDARY, _('Secundary')),
        (SPONSOR_TYPE_SUPPORT, _('Source of Monetary or Material Support'))
    )

    clinical_trial = models.ForeignKey(
        'ClinicalTrial',
        verbose_name=_('Clinical Trial'),
    )

    institution = models.ForeignKey(
        'administration.Institution',
        verbose_name=_('Institution'),
        related_name="sponsor_items",
        null=True,
        blank=True)

    sponsor_type = models.CharField(
        max_length=1,
        choices=TYPE_CHOICES,
        verbose_name=_('Sponsor type'),
        help_text="Sponsor Type"
    )
    support_source = models.TextField(
        _(u'Support Source'),
        max_length=3000,
        null=True,
        blank=True,
        help_text=('Informar por extenso o nome da fonte de fomento ou apoio financeriro que podem ser por exemplo: fundação, companhia, hospital, universidade, laboratório/indústria,FAPESP, CAPES, CNPq, FINEP).<br />COLOCAR TODAS AS SIGLAS POR EXTENSO.')
    )

    study_location = models.TextField(
        _(u'Study Location'),
        max_length=3000,
        null=True,
        blank=True,
        help_text=('Informar os locais onde se realizarão o recrutamento, intervenções ou análise dos dados.<br />COLOCAR TODAS AS SIGLAS POR EXTENSO.')
    )

    research_centers_in_brazil = models.TextField(
        _(u'Research Centers in Brazil'),
        max_length=3000,
        null=True,
        blank=True,
        help_text=('Nome de TODOS OS CENTROS DE PESQUISA NO BRASIL onde está sendo conduzido o estudo.Em caso de ESTUDOS MULTICÊNTRICOS informar  qual deles é o CENTRO COORDENADOR.<br />COLOCAR TODAS AS SIGLAS POR EXTENSO.')
    )

    class Meta:
        verbose_name = _('Sponsor')
        verbose_name_plural = _('Sponsors')

    def __unicode__(self):
        return self.sponsor_type

    def get_last_wk_state_observations_for_reviser(self):
        fields_names = [i.name for i in self._meta.fields + self._meta.many_to_many]

        last_time_reviser = StateObjectHistory.objects.filter(
                                content_id=self.clinical_trial.id,
                                state_id__in=(State.STATE_EM_REVISAO,
                                              State.STATE_EM_AVALIACAO)
                               ).order_by(u'-update_date')
        #If the 3rd last state was evaluation, state, we need to consider that date
        if last_time_reviser.count() >= 3 \
            and last_time_reviser[2].state_id == State.STATE_EM_AVALIACAO:
            last_time_reviser = last_time_reviser[2].update_date
        else:
            #Try to get the 2-times last, if it doesn't exists,
            #get the last time of revision.. and if it not exsits(still in evaluation)
            #so use the 1920 year date(not matter the date fr this case)
            last_time_reviser = last_time_reviser[1].update_date \
                                        if last_time_reviser.count() > 1 \
                                        else (
                                            last_time_reviser[0].update_date \
                                                if last_time_reviser.exists() \
                                                else datetime(1920, 1, 1)
                                        )

        query = StateObjectFieldObservation.objects.filter(
            field_name__in=fields_names,
            content_id=self.id,
            content_type_id=ContentType.objects.get_for_model(self),
            update_date__gt=last_time_reviser
        ).order_by(u'update_date').values_list(u'field_name', flat=True)

        if self.clinical_trial.date_last_publication:
            query = query.filter(update_date__gt=self.clinical_trial.date_last_publication)

        return query
        

    def get_last_wk_state_observations_for_registrant(self):
        fields_names = [i.name for i in self._meta.fields + self._meta.many_to_many]
        last_time_registrant = StateObjectHistory.objects.filter(
                                content_id=self.clinical_trial.id,
                                state_id__in=(State.STATE_EM_PREENCHIMENTO,
                                              State.STATE_RESUBMETIDO)
                               ).order_by(u'-update_date')
        last_time_registrant = last_time_registrant[1] \
                                    if last_time_registrant.count() > 1 \
                                    else last_time_registrant[0]
        query = StateObjectFieldObservation.objects.filter(
            field_name__in=fields_names,
            content_id=self.id,
            content_type_id=ContentType.objects.get_for_model(self),
            update_date__gt=last_time_registrant.update_date
        ).order_by(u'update_date').values_list(u'field_name', flat=True)

        if self.clinical_trial.date_last_publication:
            query = query.filter(update_date__gt=self.clinical_trial.date_last_publication)

        return query


class Contact(models.Model):
    """Defines a contact and it's characteristics"""
    institution = models.ForeignKey(
        'administration.Institution',
        verbose_name=_('Institution'),
        null=True,
        blank=True)

    first_name = models.CharField(
        max_length=100,
        verbose_name=_('First name'),
         
    )

    middle_name = models.CharField(
        max_length=100,
        verbose_name=_('Middle name'),
        null=True,
        blank=True)

    last_name = models.CharField(
        max_length=100,
        verbose_name=_('Last name'),
         
    )

    email = models.EmailField(
        max_length=100,
        verbose_name=_('Email'),
         
    )

    address = models.CharField(
        max_length=500,
        verbose_name=_('Address'),
         
    )

    city = models.CharField(
        max_length=500,
        verbose_name=_('City'),
         
    )

    state = models.CharField(
        max_length=500,
        verbose_name=_('State'),
         
    )

    country = models.ForeignKey(
        u'administration.Country',
        verbose_name=_(u'Country'),
         
    )

    postal_code = models.CharField(
        max_length=50,
        verbose_name=_('Postal code'),
         
    )

    phone = models.CharField(
        max_length=40,
        verbose_name=_('Phone'),
        help_text=_(u'* Required field - +[DDI][DDD][phone number]. Example: +555432223492'))

    class Meta:
        verbose_name = _('Contact')
        verbose_name_plural = _('Contacts')

    def __unicode__(self):
        name = '%s' % self.first_name
        if self.middle_name:
            name += ' %s' % self.middle_name
        if self.last_name:
            name += ' %s' % self.last_name
        return name
    
    @classmethod
    def delete_orphan_contacts(cls):
        """
        Delete all orphan contacts
        i.e: don't have any clinical trial association
        """
        cls.objects.all().annotate(
            ctcontact_count=models.Count(u'ctcontact')
        ).filter(
            ctcontact_count=0
        ).delete()


class CTContact(models.Model):
    """
    Defines a Contact association for the ClinicalTrial
    """

    TYPE_PUBLIC_QUERIES = u'P'
    TYPE_SCIENTIFIC_QUERIES = u'S'
    TYPE_SITE_QUERIES = u'W'

    TYPE_CHOICES = (
        (TYPE_PUBLIC_QUERIES, _('Public queries')),
        (TYPE_SCIENTIFIC_QUERIES, _('Scientific queries')),
        (TYPE_SITE_QUERIES, _('Site queries')) # W as for 'web'
    )

    TYPE_CHOICES_ENGLISH = (
        (TYPE_PUBLIC_QUERIES, 'Public queries'),
        (TYPE_SCIENTIFIC_QUERIES, 'Scientific queries'),
        (TYPE_SITE_QUERIES, 'Site queries') # W as for 'web'
    )

    contact = models.ForeignKey(
        'Contact',
        verbose_name=_('Contact'),
         
    )

    clinical_trial = models.ForeignKey(
        'ClinicalTrial',
        verbose_name=_('Clinical trial'),
         
    )

    contact_type = models.CharField(
        max_length=1,
        choices=TYPE_CHOICES,
        verbose_name=_('Contact type'),
         
    )

    class Meta:
        verbose_name = _('CTContact')
        verbose_name_plural = _('CTContacts')
        unique_together = (u'contact', u'clinical_trial', u'contact_type')

    def __unicode__(self):
        return self.contact.first_name + ' - ' + self.contact_type

    def get_last_wk_state_observations_for_reviser(self):
        fields_names = [i.name for i in self._meta.fields + self._meta.many_to_many]

        last_time_reviser = StateObjectHistory.objects.filter(
                                content_id=self.clinical_trial.id,
                                state_id__in=(State.STATE_EM_REVISAO,
                                              State.STATE_EM_AVALIACAO)
                               ).order_by(u'-update_date')
        #If the 3rd last state was evaluation, state, we need to consider that date
        if last_time_reviser.count() >= 3 \
            and last_time_reviser[2].state_id == State.STATE_EM_AVALIACAO:
            last_time_reviser = last_time_reviser[2].update_date
        else:
            #Try to get the 2-times last, if it doesn't exists,
            #get the last time of revision.. and if it not exsits(still in evaluation)
            #so use the 1920 year date(not matter the date fr this case)
            last_time_reviser = last_time_reviser[1].update_date \
                                        if last_time_reviser.count() > 1 \
                                        else (
                                            last_time_reviser[0].update_date \
                                                if last_time_reviser.exists() \
                                                else datetime(1920, 1, 1)
                                        )
        
        query = StateObjectFieldObservation.objects.filter(
            field_name__in=fields_names,
            content_id=self.id,
            content_type_id=ContentType.objects.get_for_model(self),
            update_date__gt=last_time_reviser
        ).order_by(u'update_date').values_list(u'field_name', flat=True)

        if self.clinical_trial.date_last_publication:
            query = query.filter(update_date__gt=self.clinical_trial.date_last_publication)

        return query


    def get_last_wk_state_observations_for_registrant(self):
        fields_names = [i.name for i in self._meta.fields + self._meta.many_to_many]
        last_time_registrant = StateObjectHistory.objects.filter(
                                content_id=self.clinical_trial.id,
                                state_id__in=(State.STATE_EM_PREENCHIMENTO,
                                              State.STATE_RESUBMETIDO)
                               ).order_by(u'-update_date')
        last_time_registrant = last_time_registrant[1] \
                                    if last_time_registrant.count() > 1 \
                                    else last_time_registrant[0]
        query = StateObjectFieldObservation.objects.filter(
            field_name__in=fields_names,
            content_id=self.id,
            content_type_id=ContentType.objects.get_for_model(self),
            update_date__gt=last_time_registrant.update_date
        ).order_by(u'update_date').values_list(u'field_name', flat=True)

        if self.clinical_trial.date_last_publication:
            query = query.filter(update_date__gt=self.clinical_trial.date_last_publication)

        return query


class CTIntervention(models.Model):
    """
    Interventions of a clinical trial(when study type of trial is intervention).
    """
    OTHER_ARM_TYPE = 5
    ARM_TYPE_CHOICES = ((1, _(u'Experimental')),
                        (2, _(u'Active Comparator')),
                        (3, _(u'Placebo Comparator')),
                        (4, _(u'No Intervention')),
                        (OTHER_ARM_TYPE, _(u'Other')))

    clinical_trial = models.ForeignKey(
        'ClinicalTrial',
        verbose_name=_('Clinical trial'),
        related_name=u'interventions',
         
    )

    intervention_name = models.CharField(
        _(u'Intervention Name'),
        max_length=150,
        help_text=('<p>No campo  “Nome da Intervenção”:</p><p>1) Para estudos com drogas use nome genérico; para outros tipos de intervenções proporcionar um breve nome descritivo.</p><p>2) Para as novas drogas de investigação que ainda não têm um nome genérico, um nome químico, empresa ou número de série pode ser usado em uma base temporária. Assim que o nome genérico foi estabelecido, atualizar os registros em conformidade.</p><p>3) Para os tipos de intervenção não-droga, fornecer um nome de intervenção com pormenor suficiente para que possa ser distinguida de outras intervenções semelhantes.</p><p>Exemplos:<br />Metformina<br />Dipirona<br />Estilo de vida aconselhamento</p>'))

    number_of_patients = models.IntegerField(
        _(u'Number of Patients'),
        help_text=('Número de pacientes: Informar o número de pacientes que compõe este grupo da intervenção.'))

    arm_type = models.IntegerField(
        _(u'Arm Type'),
        choices=ARM_TYPE_CHOICES,
        help_text=('<p>Escolher entre as opções  a que se identifica com a característica da intervenção.</p><p>Exemplo:<br />Experimental: É o braço teste composto pela medicação, procedimento ou material alvo do estudo.</p><p>Comparador ativo: Em um estudo com controle ativo, os participantes são randomizados para o tratamento teste ou para um tratamento controle ativo. Os estudos com controle ativo podem ter dois objetivos em relação à demonstração de eficácia: mostrar que o tratamento teste é tão eficaz quanto o tratamento padrão, ou que é superior ao tratamento eficaz conhecido.</p><p>Comparador Placebo: Uso do placebo como controle. O uso de placebo, ou não tratamento, é aceitável em estudos quando não houver atualmente intervenções comprovadas; ou quando, por razões metodológicas cientificamente válidas e irrefutáveis ou quando o uso de placebo for necessário para definir a eficácia ou segurança de uma intervenção, e se os participantes que receberem placebo ou não tratamento não estiverem sujeitos a risco de danos sérios ou irreversíveis.</p><p>Nenhuma intervenção: O uso de não tratamento, é aceitável em estudos quando não houver atualmente intervenções comprovadas;</p>Outro'))
    
    description_other = models.CharField(_(u'Description (Other)'),
                                         max_length=450,
                                         null=True,
                                         blank=True)
    description = models.TextField(
        _(u'Intervention Description'),
        max_length=1000,
        help_text=('<p>Deve ser suficientemente pormenorizado para que seja possível fazer a distinção entre os braços de um estudo (por exemplo, a comparação de diferentes dosagens de drogas) e / ou entre as intervenções semelhantes (por exemplo, a comparação de vários desfibriladores cardíacos implantáveis). Nas intervenções envolvendo drogas a descrição deve incluir via de administração (VO, IM,EV,ID,Mucosa), dosagem, freqüência e duração. Nos demais casos Informar freqüência e duração. Se o estudo tiver mais de um braço clicar em adicionar outro.</p><p>Exemplo: 50 mg / m2, IV (na veia) no dia 5 de cada ciclo de 28 dias, por 12 meses.</p>'))

    def get_last_wk_state_observations_for_reviser(self):
        fields_names = [i.name for i in self._meta.fields + self._meta.many_to_many]

        last_time_reviser = StateObjectHistory.objects.filter(
                                content_id=self.clinical_trial.id,
                                state_id__in=(State.STATE_EM_REVISAO,
                                              State.STATE_EM_AVALIACAO)
                               ).order_by(u'-update_date')
        #If the 3rd last state was evaluation, state, we need to consider that date
        if last_time_reviser.count() >= 3 \
            and last_time_reviser[2].state_id == State.STATE_EM_AVALIACAO:
            last_time_reviser = last_time_reviser[2].update_date
        else:
            #Try to get the 2-times last, if it doesn't exists,
            #get the last time of revision.. and if it not exsits(still in evaluation)
            #so use the 1920 year date(not matter the date fr this case)
            last_time_reviser = last_time_reviser[1].update_date \
                                        if last_time_reviser.count() > 1 \
                                        else (
                                            last_time_reviser[0].update_date \
                                                if last_time_reviser.exists() \
                                                else datetime(1920, 1, 1)
                                        )

        query = StateObjectFieldObservation.objects.filter(
            field_name__in=fields_names,
            content_id=self.id,
            content_type_id=ContentType.objects.get_for_model(self),
            update_date__gt=last_time_reviser
        ).order_by(u'update_date').values_list(u'field_name', flat=True)

        if self.clinical_trial.date_last_publication:
            query = query.filter(update_date__gt=self.clinical_trial.date_last_publication)

        return query

    def get_last_wk_state_observations_for_registrant(self):
        fields_names = [i.name for i in self._meta.fields + self._meta.many_to_many]
        last_time_registrant = StateObjectHistory.objects.filter(
                                content_id=self.clinical_trial.id,
                                state_id__in=(State.STATE_EM_PREENCHIMENTO,
                                              State.STATE_RESUBMETIDO)
                               ).order_by(u'-update_date')
        last_time_registrant = last_time_registrant[1] \
                                    if last_time_registrant.count() > 1 \
                                    else last_time_registrant[0]
        query = StateObjectFieldObservation.objects.filter(
            field_name__in=fields_names,
            content_id=self.id,
            content_type_id=ContentType.objects.get_for_model(self),
            update_date__gt=last_time_registrant.update_date

        ).order_by(u'update_date').values_list(u'field_name', flat=True)

        if self.clinical_trial.date_last_publication:
            query = query.filter(update_date__gt=self.clinical_trial.date_last_publication)

        return query


class CTInterventionObservation(models.Model):
    """
    Interventions of a clinical trial(when study type of trial is observation).
    """
    STUDY_TYPE_CHOICES = ((1, _(u'Case Control')),
                          (2, _(u'Transversal')),
                          (3, _(u'Coorte')),
                          (4, _(u'Other')),)

    clinical_trial = models.ForeignKey(
        'ClinicalTrial',
        verbose_name=_('Clinical trial'),
        related_name=u'intervention_observations',
         
    )

    group_name = models.CharField(
        _(u'Group Name'),
        max_length=150,
        help_text=('<p>O nome abreviado usado para identificar o grupo.</p><p>Exemplos:<br />- O ajuste da dose de estatina.<br />- A doença renal crônica, sem anemia.</p>')
    )

    study_type = models.IntegerField(
        _(u'Study Type'),
        choices=STUDY_TYPE_CHOICES,
        help_text=('<p>Caso-controle:  A principal diferença é que o CASO-CONTROLE sempre parte da pessoa DOENTE. SEMPRE !!!Eu vou observar e perguntar esses dois grupos: casos e controles - quem não foi exposto e quem foi.No caso-controle eu vejo o presente (a doença já esta instalada, por isso fala que começa da doença) ai, pergunta-se o que aconteceu no passado.</p><p>Nos estudos de caso-controle avalia-se inicialmente quem tem (caso) ou não (controle) o evento de interesse.</p><p>Por exemplo, um estudo de caso-controle iniciado para estudar a relação entre fumo (exposição) e câncer de pulmão (desfecho) poderia começar pela seleção de 80 casos de câncer de pulmão e 100 controles (sem câncer de pulmão). Em seguida, os casos e controles seriam questionados para determinar quais deles eram fumantes, o tempo de exposição e a média de cigarros/dia. Então, seriam estabelecidas 4 categorias:</p><p>• a)fumantes com câncer,<br />• b) não fumantes com câncer,<br />• c) fumantes sem câncer,<br />• d) não fumantes sem câncer.</p><p>Corte transversal:  Neste caso o pesquisador deve ver a associação entre a exposição e o desfecho em apenas um instante na linha do tempo e, assim, é possível avaliar a prevalência da doença e utilizar a razão de prevalência para avaliar a força da associação entre a exposição e a doença”. (2002)</p><p>• Fotografia instantânea do estudoverifica-se o “agora”.<br />• Razoavelmente baratos, pois são considerados estudos comuns.<br />• Exemplos: Se homens respondem melhor à cirurgia cardíaca do que as mulheres.<br /> Recém-nascidos com baixo peso têm problemas de mal formação cardíaca?</p><p>Coorte: Uma coorte é um grupo de indivíduos definido a partir de suas características pessoais (idade, sexo, etc.), nos quais se observa, mediante a exames repetidos, a aparição de uma enfermidade (ou outro desfecho) determinada.</p><p>Então a coorte representa pessoas definidas. A coorte é um estudo OBSERVACIONAL LONGITUDINAL (observar por 10 - 15 anos, mas sem fazer alguma intervenção).</p>')
         
    )

    description = models.TextField(
        _(u'Observation Description'),
        max_length=1000,
        help_text = ('<p>Para os Estudos Observacionais a descrição será a metodologia usada no acompanhamento e observação dos pacientes para a ocorrência de um determinado evento.  Lembre-se de informar também o número de pacientes e Objetivo do estudo , Tempo de acompanhamento.</p><p>Caso o estudo tenha grupos, PARA CADA GRUPO deve ser informado: Número de Pacientes, se são expostos ou controle, e o que foi feito ou observado em cada grupo.</p><p>Exemplo 1:<br />Estudo Analítico, onde realizou-se as seguintes etapas : 1) técnicas de observação de campo (rotina do paciente na instituição: idas à farmácia, freqüência das consultas médicas, etc.); 2) análise do banco de dados da farmácia (quantidade de pacientes inscritos para retirada de ARV, freqüência de retirada, sexo e faixa etária dos pacientes que fazem uso de ARV); 3) construção do roteiro preliminar da entrevista; 4) testagem do roteiro em campo; 5) elaboração do roteiro estruturado da entrevista.<br /> Foram escolhidos 32 pacientes deacordo com os critérios de sexo, faixa etária e adesão e não-adesão ao tratamento ARV.<br />Esses 32 pacientes estavam divididos em dois grupos: 16 que observavam as práticas de adesão e 16 que não as observavam, cada um subdividido igualmente entre homens e mulheres. O critério utilizado para adesão ao tratamento foi o critério da farmácia do hospital: o paciente que retira regularmente (todo mês) a medicação é considerado aderente, o que não o faz há mais de três meses entra na categoria de abandono de tratamento.<br />Utilizou-se uma única entrevista composta de associações livres e 24 perguntas abertas, destinada a explorar duas questões principais: a relação do sujeito com sua condição de soropositivo e com seu tratamento ARV. Antes do início da entrevista algumas perguntas obre dados sociodemográficos dos pacientes foram realizadas. As entrevistas foramgravadas e transcritas e duraram, em média, 40-45 minutos.</p><p>Exemplo2:<br /> Estudo Analítico,  onde um  grupo de 30 pacientes serão submetidos a uma avaliação clínica do tipo entrevista, exame do sono realizado através de polissonografia basal de noite inteira, provas de função pulmonar, teste de força muscular ventilatória, teste de pressão negativa expiratória durante a respiração espontânea, análise da atividade nervosa autonômica através da análise da variabilidade da frequência cardíaca durante o sono e aplicação de questionários específicos para a apnéia do sono, sonolência diurna excessiva e qualidade de vida. Os exames serão realizados apenas uma vez.</p><p>Exemplo3:<br />Estudo Analítico, onde  foram incluídos 30 tabagistas inscritos no Programa de Orientação e Conscientização Anti Tabagismo da Faculdade de Ciências e Tecnologia da Universidade Estadual Paulista que consistiu de palestras, apresentação de vídeos e realização de dinâmicas em grupo com o objetivo de manter os participantes motivados a parar de fumar. A frequência das reuniões foi de 2 vezes por semana no primeiro mês (até a data estipulada para a cessação tabagística dos participantes), sendo diminuída para semanal no segundo mês, quinzenal no terceiro mês e mensal até completar um ano de acompanhamento.</p>')
    )

    def get_last_wk_state_observations_for_reviser(self):
        fields_names = [i.name for i in self._meta.fields + self._meta.many_to_many]

        last_time_reviser = StateObjectHistory.objects.filter(
                                content_id=self.clinical_trial.id,
                                state_id__in=(State.STATE_EM_REVISAO,
                                              State.STATE_EM_AVALIACAO)
                               ).order_by(u'-update_date')
        #If the 3rd last state was evaluation, state, we need to consider that date
        if last_time_reviser.count() >= 3 \
            and last_time_reviser[2].state_id == State.STATE_EM_AVALIACAO:
            last_time_reviser = last_time_reviser[2].update_date
        else:
            #Try to get the 2-times last, if it doesn't exists,
            #get the last time of revision.. and if it not exsits(still in evaluation)
            #so use the 1920 year date(not matter the date fr this case)
            last_time_reviser = last_time_reviser[1].update_date \
                                        if last_time_reviser.count() > 1 \
                                        else (
                                            last_time_reviser[0].update_date \
                                                if last_time_reviser.exists() \
                                                else datetime(1920, 1, 1)
                                        )

        query = StateObjectFieldObservation.objects.filter(
            field_name__in=fields_names,
            content_id=self.id,
            content_type_id=ContentType.objects.get_for_model(self),
            update_date__gt=last_time_reviser
        ).order_by(u'update_date').values_list(u'field_name', flat=True)

        if self.clinical_trial.date_last_publication:
            query = query.filter(update_date__gt=self.clinical_trial.date_last_publication)

        return query

    def get_last_wk_state_observations_for_registrant(self):
        fields_names = [i.name for i in self._meta.fields + self._meta.many_to_many]
        last_time_registrant = StateObjectHistory.objects.filter(
                                content_id=self.clinical_trial.id,
                                state_id__in=(State.STATE_EM_PREENCHIMENTO,
                                              State.STATE_RESUBMETIDO)
                               ).order_by(u'-update_date')
        last_time_registrant = last_time_registrant[1] \
                                    if last_time_registrant.count() > 1 \
                                    else last_time_registrant[0]
        query = StateObjectFieldObservation.objects.filter(
            field_name__in=fields_names,
            content_id=self.id,
            content_type_id=ContentType.objects.get_for_model(self),
            update_date__gt=last_time_registrant.update_date

        ).order_by(u'update_date').values_list(u'field_name', flat=True)

        if self.clinical_trial.date_last_publication:
            query = query.filter(update_date__gt=self.clinical_trial.date_last_publication)

        return query

