#-*- coding: utf-8 -*-
"""
Url for clinical_trials app.
"""
from django.conf.urls import patterns, url
from django.contrib.auth.decorators import login_required
from clinical_trials import views

urlpatterns = patterns(
    '',
    url(
        r'^public/(?P<rbr>[-\w]+)/$',
            views.PublicClinicalTrialDetailView.as_view(),
        name='clinical_trial_detail_public'),

    url(
        r'^(?P<pk>\d+)/$',
        login_required(
            views.ClinicalTrialDetailView.as_view()),
        name='clinical_trial_detail'),

    url(
        r'^download/(?P<rbr>[-\w]+)/$',
            views.ClinicalTrialDownloadView.as_view(),
        name='clinical_trial_download'),

    url(
        r'^add/$',
        login_required(
            views.ClinicalTrialAddView.as_view()),
        name='clinical_trial_add'),

    url(
        r'^(?P<pk>\d+)/delete/$',
        login_required(
            views.ClinicalTrialDeleteView.as_view()),
        name='clinical_trial_delete'),

    url(
        r'^(?P<pk>\d+)/edit/$',
        login_required(
            views.ClinicalTrialEditListView.as_view()),
        name='clinical_trial_edit_list'),


    url(
        r'^(?P<pk>\d+)/group/management/$',
        login_required(
            views.ClinicalTrialGroupManagementView.as_view()),
        name='clinical_trial_group_manage'),

    url(
        r'^(?P<pk>\d+)/edit/identification/$',
        login_required(
            views.ClinicalTrialIdentificationView.as_view()),
        name='clinical_trial_edit_identification'),

    url(
        r'^(?P<pk>\d+)/edit/sponsors/$',
        login_required(
            views.ClinicalTrialSponsorsView.as_view()),
        name='clinical_trial_edit_sponsors'),

    url(
        r'^(?P<pk>\d+)/edit/health-conditions/$',
        login_required(
            views.ClinicalTrialHealthConditionView.as_view()),
        name='clinical_trial_edit_health_conditions'),

    url(
        r'^(?P<pk>\d+)/edit/interventions/$',
        login_required(
            views.ClinicalTrialInterventionView.as_view()),
        name='clinical_trial_edit_interventions'),

    url(
        r'^(?P<pk>\d+)/edit/recruitment/$',
        login_required(
            views.ClinicalTrialRecruitmentView.as_view()),
        name='clinical_trial_edit_recruitment'),

    url(
        r'^(?P<pk>\d+)/edit/study-type/$',
        login_required(
            views.ClinicalTrialStudyTypeView.as_view()),
        name='clinical_trial_edit_study_type'),

    url(
        r'^(?P<pk>\d+)/edit/outcomers/$',
        login_required(
            views.ClinicalTrialOutcomesView.as_view()),
        name='clinical_trial_edit_outcomes'),

    url(
        r'^(?P<pk>\d+)/edit/contacts/$',
        login_required(
            views.ClinicalTrialContactsView.as_view()),
        name='clinical_trial_edit_contacts'),

    url(
        r'^(?P<pk>\d+)/edit/attachments/$',
        login_required(
            views.ClinicalTrialAttachmentsView.as_view()),
        name='clinical_trial_edit_attacments'),

    url(
        r'^(?P<pk>\d+)/edit/send/$',
        login_required(
            views.ClinicalTrialSendView.as_view()),
        name='clinical_trial_edit_send'),

    url(
        r'^manage/$',
        login_required(
            views.ClinicalTrialManageView.as_view()),
        name='clinical_trial_admin_manage'),

    url(
        r'ct-contacts/(?P<clinical_trial_pk>\d+)/add/(?P<contact_type>P|S|W)$',
        login_required(
            views.ClinicalTrialCreateContact.as_view()),
        name='clinical_trial_add_contact'),
    url(
        r'^field-revisor-screen/(?P<clinical_trial_id>\d+)/(?P<content_type_id>\d+)/(?P<content_id>\d+)/(?P<field_name>\w+)/$',
        login_required(
            views.ClinicalTrialFieldRevisorView.as_view()),
        name='field_revisor_screen'),
    url(
        r'^field-registrant-screen/(?P<clinical_trial_id>\d+)/(?P<content_type_id>\d+)/(?P<content_id>\d+)/(?P<field_name>\w+)/$',
        login_required(
            views.ClinicalTrialFieldRegistrantView.as_view()),
        name='field_registrant_screen'),
    url(
        r'^add-workflow-field-observation/(?P<clinical_trial_id>\d+)/(?P<content_type_id>\d+)/(?P<content_id>\d+)/(?P<field_name>\w+)/$',
        login_required(
            views.ClinicalTrialAddFieldObservationView.as_view()),
        name='add_workflow_field_observation'),
    url(
        r'^list-workflow-field-observation/(?P<clinical_trial_id>\d+)/(?P<content_type_id>\d+)/(?P<content_id>\d+)/(?P<field_name>\w+)/$',
        login_required(
            views.ClinicalTrialListFieldObservationView.as_view()),
        name='list_workflow_field_observation'),
    url(
        r'^list-workflow-observations/(?P<clinical_trial_id>\d+)/$',
        login_required(
            views.ClinicalTrialListObservationsView.as_view()),
        name='list_workflow_pending_observations'),
    url(
        r'^list-field-changes-log/(?P<clinical_trial_id>\d+)/(?P<content_type_id>\d+)/(?P<content_id>\d+)/(?P<field_name>\w+)/$',
        login_required(
            views.ClinicalTrialListFieldChangeHistoryView.as_view()),
        name='list_field_changes_log'),

    url(
        r'^(?P<pk>\d+)/history/$',
        login_required(
            views.ClinicalTrialHistoryView.as_view()),
        name='clinicaltrial_history'),

    url(
        r'no-holder-cts/$',
        login_required(
            views.NoholderCTView.as_view()),
        name='no_holder_cts_list'),

    url(
        r'descriptors-cid-10-search/$',
        login_required(
            views.DescriptorsCid10SearchJsonView.as_view()),
        name='descriptors_cid10_search'),

    url(
        r'descriptors-decs-search/$',
        login_required(
            views.DescriptorsDeCSSearchJsonView.as_view()),
        name='descriptors_decs_search'),

    url(
        r'^accept-in-group/(?P<pk>\d+)/$',
        login_required(
            views.ClinicalTrialAcceptInGroupView.as_view()),
        name='clinical_trial_accept_in_group'),

    url(
        r'^check-for-changes/(?P<pk>\d+)/$',
        login_required(
            views.ClinicalTrialChangeStudyStatusView.as_view()),
        name='clinical_trial_check_for_changes'),
    url(
        r'^check-for-changes/(?P<pk>\d+)/step2/$',
        login_required(
            views.ClinicalTrialCheckForChangesView.as_view()),
        name='clinical_trial_check_for_changes_step2'),
    url(
        r'^snapshot-view/(?P<pk>\d+)/$',
        views.ClinicalTrialSnapshotView.as_view(),
        name='clinicaltrial_snapshot_view'),

)
