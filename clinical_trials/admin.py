from django.contrib import admin
from django.contrib.auth.models import User, Group
from clinical_trials.models import *

admin.site.register(ClinicalTrial)

admin.site.register(CTDetail)

admin.site.register(Outcome)

admin.site.register(StudyStatus)

admin.site.register(Descriptor)

admin.site.register(CTAttachment)

admin.site.register(Sponsor)

admin.site.register(Contact)

admin.site.register(CTContact)
