#-*- coding: utf-8 -*-
import string

from random import randrange, choice
from django.contrib.auth.models import Group
from django.db.models import Count
from django.db.models import Q
from aggregate_if import Count as CountIf
from decimal import Decimal
from workflows.models import StateObjectRelation, State

BASE36 = string.digits+string.ascii_lowercase
BASE28 = ''.join(d for d in BASE36 if d not in '1l0aeiou') 

def generate_trial_id(prefix, num_digits):
    s = str(randrange(2,10)) # start with a numeric digit 2...9
    s += ''.join(choice(BASE28) for i in range(1, num_digits))
    return '-'.join([prefix, s])


def get_next_by_occupation(group_name):
	"""
	Return the user of the group with the largest difference
	between the proportion of number occupation value
	(in	relation of others user of the same group) and
	the proportion of number of clinical trials it's holding
	(in	relation of others user of the same group).
	"""
	not_registrant_status_trials = StateObjectRelation.objects.exclude(
		state__in=[State.STATE_EM_PREENCHIMENTO,
				   State.STATE_PUBLICADO,
				   State.STATE_RESUBMETIDO,
				   State.STATE_REVISANDO_PREENCHIMENTO]
		).values_list(u'content_id', flat=True)
	total_occupation = sum(
		[i or 0 for i in Group.objects.get(
							name=group_name
						 ).user_set.all().values_list(
						 					u'ocuppation',
						 					flat=True)]
						 				   )
	total_holding = sum(
				Group.objects.get(
					name=group_name
				 ).user_set.all().annotate(
				 	holding_count=CountIf('holding_clinical_trials',
				 					only=Q(pk__in=not_registrant_status_trials))
				 ).values_list(u'holding_count', flat=True)
			)
	print total_holding
	revisers = []
	for i in Group.objects.get(
				name=group_name
			 ).user_set.all().annotate(
			 	holding_count=CountIf('holding_clinical_trials',
			 					only=Q(pk__in=not_registrant_status_trials))
			 ):
		revisers.append(
			(
			i,
			((Decimal(i.ocuppation or 0))/(Decimal(total_occupation) or 1))*100
			-
			(Decimal(i.holding_count)/(Decimal(total_holding) or 1))*100
			)
		)

	best_choice = None
	for i in revisers:
		if best_choice is None:
			best_choice = i
		if i[1] > best_choice[1]:
			best_choice = i
	if best_choice is not None:
		return best_choice[0]
	return None

