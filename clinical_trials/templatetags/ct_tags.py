#-*- coding: utf-8 -*-
from django import template
from workflows.models import StateObjectFieldObservation, State, StateObjectHistory
from clinical_trials.models import ClinicalTrial
from django.contrib.contenttypes.models import ContentType
from log.middleware import GetLog
from log.utils import get_changed_fields
from datetime import datetime
register = template.Library()

def count_observations(context, clinical_trial_id, content_type_id, content_id, field_name):
    clinical_trial = ClinicalTrial.objects.get(pk=clinical_trial_id)
    if clinical_trial.holder.is_registrant():
        last_time = StateObjectHistory.objects.filter(
                                    content_id=clinical_trial_id,
                                    state_id__in=(State.STATE_EM_PREENCHIMENTO,
                                                  State.STATE_RESUBMETIDO)
                                   ).order_by(u'-update_date')
    
    if clinical_trial.holder.is_reviser() or clinical_trial.holder.is_evaluator():
        last_time = StateObjectHistory.objects.filter(
                                    content_id=clinical_trial_id,
                                    state_id__in=(State.STATE_EM_REVISAO,
                                                  State.STATE_EM_AVALIACAO)
                                   ).order_by(u'-update_date')

    #If the 3rd last state was evaluation, state, we need to consider that date
    if last_time.count() >= 3 \
        and last_time[2].state_id == State.STATE_EM_AVALIACAO:
        last_time = last_time[2].update_date
    else:
        #Try to get the 2-times last, if it doesn't exists,
        #get the last time of revision.. and if it not exsits(still in evaluation)
        #so use the 1920 year date(not matter the date fr this case)
        last_time = last_time[1].update_date \
                                    if last_time.count() > 1 \
                                    else (
                                        last_time[0].update_date \
                                            if last_time.exists() \
                                            else datetime(1920, 1, 1)
                                    )

    query = StateObjectFieldObservation.objects.filter(
            field_name=field_name,
            content_id=content_id,
            content_type_id=content_type_id,
            update_date__gt=last_time
        )

    if clinical_trial.date_last_publication:
            query = query.filter(update_date__gt=clinical_trial.date_last_publication)

    context[u'count_observations_result'] = query.count()

    changed_fields = get_changed_fields(ClinicalTrial.objects.get(pk=clinical_trial_id))
    if changed_fields:
        context[u'field_changed'] = field_name in changed_fields.get(content_id, [])
    else:
        context[u'field_changed'] = False

    return ''
    

def has_changes_since_last_publication(context, clinical_trial, content_type_id, content_id, field_name):
    if True:#clinical_trial.date_last_publication:
        if True:#clinical_trial.holder.is_reviser() or \
            #clinical_trial.holder.is_evaluator():
            model_class = ContentType.objects.get(pk=content_type_id).model_class()
            name = model_class.__name__
            history = GetLog(name, int(content_id), field_name)()
            date_to_compare = clinical_trial.date_last_publication
            try:
                date_last_resubmited = StateObjectHistory.objects.filter(
                                            content_id=clinical_trial.pk,
                                            state__pk=State.STATE_RESUBMETIDO
                                        ).latest('update_date').update_date
                if date_to_compare and date_to_compare > date_last_resubmited:
                    pass
                else:
                    date_to_compare = date_last_resubmited
            except StateObjectHistory.DoesNotExist:
                #No action if except because not fund last resubmited date
                pass
            if not date_to_compare:
                context[u'has_changes_since_last_publication'] = False
                return ''
            date_to_compare = date_to_compare.replace(tzinfo=None)

            for his in history:
                if date_to_compare and his[u'date_stamp'] > date_to_compare:
                    context[u'has_changes_since_last_publication'] = True
                    return ''


    context[u'has_changes_since_last_publication'] = False

    return ''

register.simple_tag(count_observations, takes_context=True)
register.simple_tag(has_changes_since_last_publication, takes_context=True)

@register.filter('fieldtype')
def fieldtype(field):
    return field.field.widget.__class__.__name__