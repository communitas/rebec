#-*- coding: utf-8 -*-
"""
Forms for the clinical_trials app.
"""
from django import forms
from django.forms.models import modelformset_factory
from django.utils import timezone
from clinical_trials.models import ClinicalTrial, CTDetail, Sponsor, \
     Identifier, Descriptor, CTContact, CTAttachment, Outcome, Contact, \
     CTIntervention
from clinical_trials.widgets import ChoicesSpanWidget
from administration.models import Setup, Country, CTGroup
from django.utils.translation import ugettext as _, ugettext_lazy
from django_select2 import AutoModelSelect2Field, AutoModelSelect2MultipleField
from django_select2.widgets import AutoHeavySelect2Widget
from django_select2.util import JSFunctionInContext
from administration.models import Institution, CTGroup, CTGroupUser
from django.db.models import Q
from django.contrib.auth import get_user_model
from workflows.models import State
User = get_user_model()

class ClinicalTrialForm(forms.ModelForm):
    """
    Clinical Trial Creation Form
    """
    accept_terms = forms.BooleanField(required=True, label=ugettext_lazy('Accept Terms'))
    accept_modal = forms.CharField(required=True, label='', widget=forms.HiddenInput)
    class Meta:
        model = ClinicalTrial
        fields = (u'group',
                  u'language_1_mandatory',
                  u'language_2_mandatory',
                  u'language_3_mandatory',
                  u'study_type',)

    def __init__(self, *args, **kwargs):
        """
        Method override to delete the
        unnecessaries lagnguage_X_mandatory fields.
        """
        self.request = kwargs.pop(u'request')
        super(ClinicalTrialForm, self).__init__(*args, **kwargs)
        self.setup = Setup.objects.get()
        self.fields[u'study_type'].required = True
        if not self.setup.language_1_mandatory:
            self.fields[u'language_1_mandatory'].required = False
        else:
            self.fields[u'language_1_mandatory'].initial = True
        
        if self.setup.language_1_code:
            self.fields[u'language_1_mandatory'].label = \
                self.setup.language_1_code.description
        else:
            del self.fields[u'language_1_mandatory']

        if not self.setup.language_2_mandatory:
            self.fields[u'language_2_mandatory'].required = False
        else:
            self.fields[u'language_2_mandatory'].initial = True
        
        if self.setup.language_2_code:
            self.fields[u'language_2_mandatory'].label = \
                self.setup.language_2_code.description
        else:
            del self.fields[u'language_2_mandatory']

        if not self.setup.language_3_mandatory:
            self.fields[u'language_3_mandatory'].required = False
        else:
            self.fields[u'language_3_mandatory'].initial = True
        
        if self.setup.language_3_code:
            self.fields[u'language_3_mandatory'].label = \
                self.setup.language_3_code.description
        else:
            del self.fields[u'language_3_mandatory']

        #Just the user active ctgroups
        self.fields[u'group'].queryset = CTGroup.objects.filter(
            pk__in=self.request.user.ctgroups.filter(
                status=CTGroupUser.STATUS_ACTIVE,
                ct_group__status=CTGroup.STATUS_ACTIVE).values_list(
                    'ct_group',
                    flat=True))
        #If the user have just one group, set it as initial
        if self.fields[u'group'].queryset.count() == 1:
            self.initial[u'group'] = self.fields[u'group'].queryset.get().pk

    def clean_language_1_mandatory(self):
        """
        This language mandatory cannot be False if the system's
        Setup is set to be a language mandatory.
        """
        if self.setup.language_1_mandatory and \
            not self.cleaned_data[u'language_1_mandatory']:
            raise forms.ValidationError(_(u'This option cannot be False.'))
        return self.cleaned_data[u'language_1_mandatory']

    def clean_language_2_mandatory(self):
        """
        This language mandatory cannot be False if the system's
        Setup is set to be a language mandatory.
        """
        if self.setup.language_2_mandatory and\
            not self.cleaned_data[u'language_2_mandatory']:
            raise forms.ValidationError(_(u'This option cannot be False.'))
        return self.cleaned_data[u'language_2_mandatory']

    def clean_language_3_mandatory(self):
        """
        This language mandatory cannot be False if the system's
        Setup is set to be a language mandatory.
        """
        if self.setup.language_3_mandatory and\
            not self.cleaned_data[u'language_3_mandatory']:
            raise forms.ValidationError(_(u'This option cannot be False.'))
        return self.cleaned_data[u'language_3_mandatory']

    def clean(self):
        """
        If the unique error is the accept_modal hidden field error, so set
        the "show" on the accept_modal field error to show the modal in
        the template. The modal will have this field corrected filled, so it
        will not have the error again.
        """
        if len(self.errors) == 1 and self.errors.get(u'accept_modal', None):
            self.errors[u'accept_modal'] = 'show'
        return self.cleaned_data


class ClinicalTrialGroupChangeForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        """
        Method override to set the groups allowed
        """
        self.request = kwargs.pop(u'request')
        super(ClinicalTrialGroupChangeForm, self).__init__(*args, **kwargs)

        #Just the user active ctgroups
        self.fields[u'group'].queryset = CTGroup.objects.filter(
            pk__in=self.request.user.ctgroups.filter(
                status=CTGroupUser.STATUS_ACTIVE,
                ct_group__status=CTGroup.STATUS_ACTIVE).values_list(
                    'ct_group',
                    flat=True))
        #If the user have just one group, set it as initial
        if self.fields[u'group'].queryset.count() == 1:
            self.initial[u'group'] = self.fields[u'group'].queryset.get().pk

    class Meta:
        model = ClinicalTrial
        fields = (u'group',)


class ClinicalTrialIdentificationForm(forms.ModelForm):
    accept_modal = forms.CharField(required=False, label='', widget=forms.HiddenInput)

    def __init__(self, *args, **kwargs):
        super(ClinicalTrialIdentificationForm, self).__init__(*args, **kwargs)
        self.fields[u'study_final_date'].widget.manual_format = True
        self.fields[u'study_final_date'].widget.format = '%d/%m/%Y'
        self.fields[u'study_final_date'].required = True
        
        #self.fields['study_type'].choices = \
        #    filter(lambda x: x[0] != '', self.fields['study_type'].choices)

    class Meta:
        model = ClinicalTrial
        fields = [u'study_final_date', u'study_status', u'study_type']

    def clean_study_final_date(self):
        """
        Validation for the current date to be higher or equal than the study
        final date.
        """
        if self.cleaned_data[u'study_final_date'] < timezone.now().date():
            raise forms.ValidationError(_(u"This date can't be lower "
                                          u"than the current date."))
        return self.cleaned_data[u'study_final_date']

    def clean(self):
        """
        If the unique error is the accept_modal hidden field error, so set
        the "show" on the accept_modal field error to show the modal in
        the template. The modal will have this field corrected filled, so it
        will not have the error again.
        """
        if self.instance.study_type <> self.cleaned_data['study_type']:
            if not self.cleaned_data.get(u'accept_modal', None):
                self.errors[u'accept_modal'] = 'show'
        return self.cleaned_data


class CTDetailIdentificationForm(forms.ModelForm):
    """
    For of first step of clinical trial detail
    """
    class Meta:
        model = CTDetail
        fields = (u'scientific_title', u'public_title',
                  u'acronym', u'study_description')

CTDetailIdentificationFormSet = modelformset_factory(
    model=CTDetail,
    form=CTDetailIdentificationForm,
    extra=0)


class IdentifierForm(forms.ModelForm):
    """
    Form for identifier on ClinicalTrial update.
    """

    class Meta:
        model = Identifier

    def __init__(self, *args, **kwargs):

        super(IdentifierForm, self).__init__(*args, **kwargs)
        
        #Except the empty-form, all other forms must have the special widget
        #that doesn't allow change
        #from IPython import embed;embed()
        must_pre_appear = self.fields[u'setup_identifier'].queryset.filter(
                                            appear_in_identifications_step=True
                          ).values_list(u'pk', flat=True)
        must_not_pre_appear = self.fields[u'setup_identifier'].queryset.filter(
                                           appear_in_identifications_step=False
                              ).values_list(u'pk', flat=True)

        if u'__prefix__' not in kwargs.get(u'prefix', u'') and \
            (int(self.data.get(
                  self['setup_identifier'].html_name, '0') or '0') in must_pre_appear \
                or \
               int(self.initial.get(
                  'setup_identifier', '0')) in must_pre_appear):
                self.fields['setup_identifier'].widget = ChoicesSpanWidget()
                self.fields['setup_identifier'].widget.choices = \
                    ((i.pk, unicode(i)) for i in \
                        self.fields[u'setup_identifier'].queryset.filter(
                                           appear_in_identifications_step=True))

        else:
            self.fields[u'setup_identifier'].queryset = \
                self.fields[u'setup_identifier'].queryset.filter(
                    appear_in_identifications_step=False)


    def clean_code(self, *args, **kwargs):
        """
        Validate the data on the selected setup_identifier mask.
        """
        if not self.cleaned_data.has_key(u'setup_identifier') or \
           not self.cleaned_data[u'code']:
            raise forms.ValidationError(_(u"The code didn't validade"))
        #if the identifier dont have a mask specified, skip mask validation
        if self.cleaned_data[u'setup_identifier'].mask \
           and not self.validate(self.cleaned_data[u'setup_identifier'].mask,
                             self.cleaned_data[u'code']):
            raise forms.ValidationError(_(u"The code didn't validade"))

        return self.cleaned_data[u'code']

    def validate(self, mask, value):
        from string import ascii_letters, digits
        from itertools import izip_longest
        for m, v in izip_longest(mask, value):
            if v is None:
                v = ''
            if m == u'L' and not v in ascii_letters:
                return False
            elif m == u'N' and not v in digits:
                return False
            elif m == u'A':
                pass#anything is allowed
            elif m not in ('N', 'L', 'A') and m != v:
                return False
        return True



class AutoHeavySelect2WidgetWithExtraParams(AutoHeavySelect2Widget):
    """
    A django_select2 AutoHeavySelect2Widget override, to get aditional
    query parameters from the javascript widget.
    """
    def init_options(self):
        super(AutoHeavySelect2WidgetWithExtraParams, self).init_options()
        self.options['ajax']['data'] = JSFunctionInContext('get_url_parameters')


class InstitutionChoices(AutoModelSelect2Field):
    """
    A django_select2 class to configure the autocomplete search for Institutions
    """
    queryset = Institution.objects
    search_fields = [u'name__icontains',
                     u'description__icontains',]
    widget = AutoHeavySelect2WidgetWithExtraParams

    def prepare_qs_params(self, request, search_term, search_fields):
        """
        Create a new AND parameter to retrieve only the active institutions or
        the moderation institutions of the group of the actual clinical_trial.
        """
        queryset = super(InstitutionChoices, self).prepare_qs_params(
            request,
            search_term,
            search_fields)
        
        ct_id = request.GET.get(u'clinical_trial_id', None)
        
        #get all ctgroups of this clinical_trial
        groups = CTGroup.objects.filter(
            clinicaltrial__pk=ct_id
        )
        clinical_trials_same_group = ClinicalTrial.objects.filter(
            group__in=groups
        )
        #get all active institutions...
        query = Q(status=Institution.STATUS_ACTIVE)
        #or the moderation of other clinical trials
        #of the same group of this clinical trial...
        query = query | Q(Q(sponsor_items__clinical_trial__in=clinical_trials_same_group) & Q(status=Institution.STATUS_MODERATION))
        #or the moderation of this clinical trial.
        query = query | Q(Q(sponsor_items__clinical_trial_id=ct_id) & Q(status=Institution.STATUS_MODERATION))
        institution_query = Institution.objects.filter(query)
        queryset[u'and'][u'pk__in'] = institution_query

        return queryset

class ClinicalTrialSponsorForm(forms.ModelForm):
    """
    Sponsor form of a Clinical Trial.
    """

    institution = InstitutionChoices(label=ugettext_lazy(u'Institution'), required=False,
        help_text=('<p>A INSTITUIÇÃO deve ser escolhida dentre as já cadastradas em nosso sistema. Caso sua instituição não conste, você deve formalmente solicitar a  inclusão dela pelo e-mail rebec@icict.fiocruz.br informando<br />1) o nome completo da instituição,<br />2) o CNPJ,<br />3) o endereço completo com CEP,<br />4) telefone e e-mail institucionais para checagem das informações.</p>'))

    class Meta:
        model = Sponsor
        fields = [u'sponsor_type', u'institution', u'support_source',
                  u'study_location',
                  u'research_centers_in_brazil']

    def __init__(self, *args, **kwargs):
        super(ClinicalTrialSponsorForm, self).__init__(*args, **kwargs)
        institution = self.initial.get(u'institution', None)
        inactive_institution_error = _(u'This institution has been refused.')
        if institution is not None and not self.data.get(self[u'institution'].html_name, None):
            institution = Institution.objects.get(pk=institution)
            if institution.status == Institution.STATUS_INACTIVE:
                self.errors['institution'] = [inactive_institution_error]
        
        self.fields[u'sponsor_type'].choices = \
            (i for i in self.fields[u'sponsor_type'].choices if i[0])

    def clean(self, *args, **kwargs):
        """
        Validate the fields based on the sponsor_type field.
        """
        required_error = [_(u'This field is required.')]
        if self.cleaned_data[u'sponsor_type'] == u'U':
            if not self.cleaned_data.get(u'support_source', None):
                self.errors[u'support_source'] = required_error

            if not self.cleaned_data.get(u'study_location', None):
                self.errors[u'study_location'] = required_error

            if not self.cleaned_data.get(u'research_centers_in_brazil', None):
                self.errors[u'research_centers_in_brazil'] = required_error
        else:
            if not self.cleaned_data.get(u'institution', None):
                self.errors[u'institution'] = required_error

        return self.cleaned_data


class CTDetailHealthConditionForm(forms.ModelForm):
    """
    Form of the "Health Conditions" step
    """
    class Meta:
        model = CTDetail
        fields = (u'health_condition', )


CTDetailHealthConditionFormSet = modelformset_factory(
    model=CTDetail,
    form=CTDetailHealthConditionForm,
    extra=0)

from django_select2 import HeavySelect2ChoiceField
from django_select2 import HeavySelect2Widget
from django.core.urlresolvers import reverse_lazy
class DescriptorForm(forms.ModelForm):
    """
    Form for a Clinical Trial Descriptor.
    """
    parent_vocabulary = forms.ChoiceField(
        choices=Descriptor.VOCABULARY_CHOICES_WITHOUT_CAS
    )

    class Meta:
        model = Descriptor
        fields = (u'descriptor_type',
                  u'parent_vocabulary',
                  u'vocabulary_item_en',
                  u'vocabulary_item_es',
                  u'vocabulary_item_pt')

    def __init__(self, *args, **kwargs):
        super(DescriptorForm, self).__init__(*args, **kwargs)
        self.fields[u'vocabulary_item_en'].widget.attrs[u'readonly'] = u'readonly'
        self.fields[u'vocabulary_item_es'].widget.attrs[u'readonly'] = u'readonly'
        self.fields[u'vocabulary_item_pt'].widget.attrs[u'readonly'] = u'readonly'

        
class HealthConditionsDescriptorForm(DescriptorForm):
    """
    Form for a Clinical Trial Descriptor, with some overrides
    for the Health Conditions step.
    """
    def __init__(self, *args, **kwargs):
        super(HealthConditionsDescriptorForm, self).__init__(*args, **kwargs)
        self.fields[u'descriptor_type'].choices = \
            [i for i in self.fields[u'descriptor_type'].choices if i[0] != u'I']


class InterventionsDescriptorForm(DescriptorForm):
    """
    Form for a Clinical Trial Descriptor, with some overrides
    for the Interventions step.
    """
    class Meta:
        model = Descriptor
        fields = (u'parent_vocabulary',
                  u'vocabulary_item_en',
                  u'vocabulary_item_es',
                  u'vocabulary_item_pt')

    def save(self, *args, **kwargs):
        self.instance.descriptor_type = u'I'
        return super(InterventionsDescriptorForm, self).save(*args, **kwargs)

class CTInterventionForm(forms.ModelForm):
    """
    Form of the "Interventions" step
    """
    class Meta:
        model = CTIntervention
        fields = (u'intervention_name', u'number_of_patients',
                  u'arm_type', u'description_other', u'description')


class ClinicalTrialInterventionAssignmentsForm(forms.ModelForm):
    """
    Form for a Clinical Trial Intervention Assignment.
    """
    class Meta:
        model = ClinicalTrial
        fields = [u'intervention_codes']
        widgets = {u'intervention_codes': forms.widgets.CheckboxSelectMultiple}

    def __init__(self, *args, **kwargs):
        super(ClinicalTrialInterventionAssignmentsForm, self).__init__(
            *args,
            **kwargs)
        self.fields[u'intervention_codes'].label = _('Intervention Code(s)')



class CTDetailRecruitmentForm(forms.ModelForm):
    """
    Form of the "Recruitment" step
    """

    def clean_inclusion_criteria(self):
        """
        Separate the terms in ";"
        The select2 tokenizer sends every term separated by ",", and here,
        we separate each one on ";"
        """
        return ';'.join(self.cleaned_data[u'inclusion_criteria'].split(','))

    def clean_exclusion_criteria(self):
        """
        Separate the terms in ";"
        The select2 tokenizer sends every term separated by ",", and here,
        we separate each one on ";"
        """
        return ';'.join(self.cleaned_data[u'exclusion_criteria'].split(','))

    class Meta:
        model = CTDetail
        fields = (u'inclusion_criteria', u'exclusion_criteria')

CTDetailRecruitmentFormSet = modelformset_factory(
    model=CTDetail,
    form=CTDetailRecruitmentForm,
    extra=0)


class RecruitmentCountryChoices(AutoModelSelect2MultipleField):
    """
    A django_select2 class to configure the autocomplete search
    for Countries.
    """
    queryset = Country.objects
    search_fields = [u'description__icontains',]


class ClinicalRecruitmentStepForm(forms.ModelForm):
    """
    Form for the Clinical Trial Recruitment step.
    """
    recruitment_countries = RecruitmentCountryChoices(
        label=ugettext_lazy(u'Recruitment countries'),
        help_text= ('O registrante deverá Informar um ou mais países em que o seu estudo acontece, caso haja recrutamento em outros países além do Brasil.'))
    minimum_age_no_limit = forms.TypedChoiceField(
                   coerce=lambda x: x == 'True',
                   choices=((True, ugettext_lazy('Yes')), (False, ugettext_lazy('No'))),
                   label=ugettext_lazy(u'Minimum age without limit?'),
                   help_text=('O Registrante deverá escolher entre as opções Sim para quando não tem limite mínimo de idade ou Não para quando tem limite de idade.')
                )
    maximum_age_no_limit = forms.TypedChoiceField(
                   coerce=lambda x: x == 'True',
                   choices=((True, ugettext_lazy('Yes')), (False, ugettext_lazy('No'))),
                   label=ugettext_lazy(u'Maximum age without limit?'),
                   help_text= ('O registrante deverá escrever numericamente, a idade mínima (caso haja), para inclusão de participantes em seu estudo.')
                )

    class Meta:
        model = ClinicalTrial
        fields = [u'recruitment_countries',
                  u'date_first_enrollment', u'date_last_enrollment',
                  u'gender', u'minimum_age_no_limit',
                  u'inclusion_minimum_age', u'minimum_age_unit',
                  u'maximum_age_no_limit', u'inclusion_maximum_age',
                  u'maximum_age_unit', u'world_target_size',
                  u'brazil_target_size', u'statistical_sample_analysis']

    def __init__(self, request, *args, **kwargs):
        super(ClinicalRecruitmentStepForm, self).__init__(*args, **kwargs)
        if request.LANGUAGE_CODE == u'pt':
            for f in (u'date_first_enrollment', u'date_last_enrollment'):
                self.fields[f].widget.manual_format = True
                self.fields[f].widget.format = '%d/%m/%Y'

    def clean(self, *args, **kwargs):
        dfe =  self.cleaned_data.get(u'date_first_enrollment', None)
        dle =  self.cleaned_data.get(u'date_last_enrollment', None)
        if dfe and dle:
            if dfe > dle:
                raise forms.ValidationError(
                    _(u'Date first enrollment must be lower than date last enrollment.')
                )

            if self.instance.study_final_date:
                if dle > self.instance.study_final_date:
                    raise forms.ValidationError(
                        _(u'Date last enrollment must be lower than the study final date.')
                    )
        
        return self.cleaned_data


class ClinicalStudyTypeStepForm(forms.ModelForm):
    """
    Form for the Clinical Trial Study Type step.
    """
    class Meta:
        model = ClinicalTrial
        fields = [u'expanded_access_program',
                  u'study_purpose', u'intervention_assignment',
                  u'number_arms', u'masking_type', u'allocation_type',
                  u'study_phase', u'followup_period', u'reference_period']


class OutcomeForm(forms.Form):
    """
    Form for the Clinical Trial Outcomes step.
    """
    pass


class CTContactForm(forms.ModelForm):
    """
    Form for the Clinical Trial Contact step.
    """
    class Meta:
        model = CTContact
        fields = (u'contact_type', u'contact')
    def __init__(self, ct_id, *args, **kwargs):
        super(CTContactForm, self).__init__(*args, **kwargs)
        self.fields['contact'].queryset = \
            self.fields['contact'].queryset.filter(
                         ctcontact__clinical_trial__pk=ct_id
                    ).distinct()


class CTAttachmentForm(forms.ModelForm):
    """
    Form for the Clinical Trial Attachment step.
    """
    class Meta:
        model = CTAttachment

    def clean(self):
        """
        Allow just a file OR a link, depending
        on the attachment type.
        """
        import mimetypes, magic
        link = self.cleaned_data.get(u'link', None)
        file = self.cleaned_data.get(u'file', None)
        attachment = self.cleaned_data.get(u'attachment', None)
        if attachment:
            if u'L' in attachment.attachment_type:
                if not link:
                    raise forms.ValidationError(
                        _(u'Please provide a link.'))
                if file:
                    raise forms.ValidationError(
                        _(u'This attachment type is just for a link.'))

            else:
                if not file:
                    raise forms.ValidationError(
                        _(u'Please provide a file.'))
                if link:
                    raise forms.ValidationError(
                        _(u'This attachment type is just for a file.'))

                ftype = mimetypes.MimeTypes().types_map_inv[1][
                            magic.from_buffer(self.cleaned_data['file'].read(),
                                mime=True)
                            ][0].replace('.', '')
                extensions = {'P': ['pdf'],
                              'J': ['jpg', 'jpeg'],
                              'G': ['gif'],
                              'N': ['png'],
                              'B': ['bmp']}

                allowed_exts = []
                for i in attachment.attachment_type:
                    allowed_exts += extensions[i]
                if ftype.lower() not in allowed_exts:
                    raise forms.ValidationError(
                            _(u'This attachment must be one of this types: %(types)s.') %
                            {u'types': attachment.get_attachment_type_display()})
                if attachment.max_file_size and \
                    file.size > attachment.max_file_size * 1000 *1000:
                    raise forms.ValidationError(
                            _(u'The max file size is %(size)sMB') %
                                {u'size': attachment.max_file_size})

        return self.cleaned_data


class UserChoices(AutoModelSelect2Field):
    """
    A django_select2 class to configure the autocomplete search
    for Users.
    """
    queryset = User.objects
    search_fields = [u'email__icontains',
                     u'first_name__icontains',
                     u'last_name__icontains',
                     u'username__icontains',]
    def prepare_qs_params(self, request, search_term, search_fields):
        """
        Just active users
        """
        queryset = super(UserChoices, self).prepare_qs_params(
            request,
            search_term,
            search_fields
        )
        
        queryset[u'and'][u'is_active'] = True
        return queryset



class CTGroupChoices(AutoModelSelect2Field):
    """
    A django_select2 class to configure the autocomplete search
    for CTGroup.
    """
    queryset = CTGroup.objects
    search_fields = [u'name__icontains',
                     u'description__icontains',]

    def prepare_qs_params(self, request, search_term, search_fields):
        """
        Just active ctgroups
        """
        queryset = super(CTGroupChoices, self).prepare_qs_params(
            request,
            search_term,
            search_fields
        )
        
        queryset[u'and'][u'status'] = CTGroup.STATUS_ACTIVE
        return queryset



class UserOfMyGroupChoices(AutoModelSelect2Field):
    """
    A django_select2 class to configure the autocomplete search
    for Users.
    """
    queryset = User.objects
    search_fields = [u'email__icontains',
                     u'first_name__icontains',
                     u'last_name__icontains',
                     u'username__icontains',]
    def prepare_qs_params(self, request, search_term, search_fields):
        """
        Just active users
        """
        queryset = super(UserOfMyGroupChoices, self).prepare_qs_params(
            request,
            search_term,
            search_fields
        )
        
        queryset[u'and'][u'is_active'] = True
        queryset[u'and'][u'ctgroups__ct_group__pk__in'] = \
            request.user.ctgroups.filter(
                type_user=CTGroupUser.TYPE_ADMINISTRATOR
            ).values_list(u'ct_group__pk', flat=True)
        queryset[u'and'][u'ctgroups__status'] = CTGroupUser.STATUS_ACTIVE
        return queryset


class UserOfSelectedGroupChoices(AutoModelSelect2Field):
    """
    A django_select2 class to configure the autocomplete search
    for Users.
    """
    queryset = User.objects
    search_fields = [u'email__icontains',
                     u'first_name__icontains',
                     u'last_name__icontains',
                     u'username__icontains',]
    widget = AutoHeavySelect2WidgetWithExtraParams
    def prepare_qs_params(self, request, search_term, search_fields):
        """
        Just active users
        """
        queryset = super(UserOfSelectedGroupChoices, self).prepare_qs_params(
            request,
            search_term,
            search_fields
        )
        group_id = request.GET.get('group_id', None)
        if not group_id:
            return queryset.none()
        queryset[u'and'][u'is_active'] = True
        queryset[u'and'][u'ctgroups__ct_group__pk'] = group_id
        queryset[u'and'][u'ctgroups__status'] = CTGroupUser.STATUS_ACTIVE

        return queryset

class MyCTGroupsChoices(AutoModelSelect2Field):
    """
    A django_select2 class to configure the autocomplete search
    for CTGroup.
    """
    queryset = CTGroup.objects
    search_fields = [u'name__icontains',
                     u'description__icontains',]

    def prepare_qs_params(self, request, search_term, search_fields):
        """
        Just active ctgroups
        """
        queryset = super(MyCTGroupsChoices, self).prepare_qs_params(
            request,
            search_term,
            search_fields
        )
        
        queryset[u'and'][u'status'] = CTGroup.STATUS_ACTIVE
        queryset[u'and'][u'pk__in'] = \
            request.user.ctgroups.filter(
                type_user=CTGroupUser.TYPE_ADMINISTRATOR
            ).values_list(u'ct_group', flat=True)
        return queryset

class ClinicalTrialManagementForm(forms.ModelForm):
    """
    Form for a Clinical Trial administrator management.
    """
    user = UserOfSelectedGroupChoices(required=False, label=ugettext_lazy('User'))
    group = MyCTGroupsChoices(required=False, label=ugettext_lazy('Group'),
        widget=AutoHeavySelect2WidgetWithExtraParams(
                select2_options={'placeholder':_(u'Search'), 'width': '1px'}
            ),)
    class Meta:
        model = ClinicalTrial
        fields = [u'user', u'group']

    def save(self, *args, **kwargs):
        super(ClinicalTrialManagementForm, self).save(*args, **kwargs)
        #if this trial is on a registrant state, change the holder too
        if self.instance.get_workflow_state().pk in (
            State.STATE_EM_PREENCHIMENTO,
            State.STATE_RESUBMETIDO,
            State.STATE_PUBLICADO,
            State.STATE_REVISANDO_PREENCHIMENTO
        ):
            self.instance.holder = self.instance.user
            self.instance.save()
class ClinicalTrialManagementSearchForm(forms.Form):
    """
    Auxiliar search form for the Clinical Trial administrator management view.
    """
    user = UserOfMyGroupChoices(required=False, label=ugettext_lazy(u'User'))
    group = MyCTGroupsChoices(required=False, label=ugettext_lazy(u'Group'))
    clinical_trial = forms.CharField(max_length=50, required=False, label=ugettext_lazy(u'Clinical Trial'))

class ContactForm(forms.ModelForm):
    search_address = forms.CharField(
        max_length=200,
        label=ugettext_lazy(u'Search for Address'),
        required=False
    )
    class Meta:
        model = Contact
        fields = (u'institution', u'first_name', u'middle_name', u'last_name',
                  u'email', u'search_address', u'address', u'city', u'state',
                  u'postal_code', u'phone')


class ContactForm2(forms.ModelForm):
    institution = InstitutionChoices(
            required=False,
            label=ugettext_lazy('Institution'),
            widget=AutoHeavySelect2WidgetWithExtraParams(
                select2_options={'placeholder':_(u'Search'), 'width': '1px'}
            )
        )
    search_address = forms.CharField(
        max_length=200,
        label=ugettext_lazy(u'Search for Address'),
        required=False
    )
    class Meta:
        model = Contact
        fields = (u'institution', u'first_name', u'middle_name', u'last_name',
                  u'email', u'search_address', u'address', u'city', u'state',
                  u'country', u'postal_code', u'phone')


class ShowJustRevisorsAndEvaluatorsForm(forms.ModelForm):
    class Meta:
        model = ClinicalTrial
        fields = (u'holder',)

    def __init__(self, *args, **kwargs):
        """
        Override to filter the holder queryset to just revisers and evaluators,
        who is active, ordered by username.
        """
        super(ShowJustRevisorsAndEvaluatorsForm, self).__init__(*args, **kwargs)
        self.fields[u'holder'].queryset = \
            self.fields[u'holder'].queryset.filter(
                    groups__name__in=(u'reviser', 'evaluator'),
                    is_active=True,
            ).order_by(u'username')
        self.fields[u'holder'].label_from_instance = \
                lambda x: '%s - %s' %(x.username, x.name)


class AddNewWorkflowGeneralObservationForm(forms.Form):

    new_obs = forms.CharField(label=ugettext_lazy(u'New General Observation'),
                              widget=forms.Textarea)


class ChangeStudyStatusForm(forms.ModelForm):

    class Meta:
        model = ClinicalTrial
        fields = (u'study_status',)
