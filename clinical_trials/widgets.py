#-*- coding: utf-8 -*-
from django import forms
from django.utils.safestring import mark_safe

class ChoicesSpanWidget(forms.Widget):
    '''
    Renders a value wrapped in a <span> tag.
    '''

    def render(self, name, value, attrs=None):
        return mark_safe((u'<span >%s</span>'
                          u'<input type="hidden" name="%s" value="%s">') \
                          % (dict(self.choices).get(int(value or -1), u''),
                             name,
                             value))
