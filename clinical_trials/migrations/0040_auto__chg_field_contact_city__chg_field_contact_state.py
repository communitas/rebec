# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'Contact.city'
        db.alter_column(u'clinical_trials_contact', 'city', self.gf('django.db.models.fields.CharField')(max_length=500))

        # Changing field 'Contact.state'
        db.alter_column(u'clinical_trials_contact', 'state', self.gf('django.db.models.fields.CharField')(max_length=500))

    def backwards(self, orm):

        # Changing field 'Contact.city'
        db.alter_column(u'clinical_trials_contact', 'city', self.gf('django.db.models.fields.CharField')(max_length=100))

        # Changing field 'Contact.state'
        db.alter_column(u'clinical_trials_contact', 'state', self.gf('django.db.models.fields.CharField')(max_length=100))

    models = {
        u'administration.allocationtype': {
            'Meta': {'object_name': 'AllocationType'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'administration.attachment': {
            'Meta': {'object_name': 'Attachment'},
            'attachment_type': ('multiselectfield.db.fields.MultiSelectField', [], {'max_length': '11'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max_file_size': ('django.db.models.fields.DecimalField', [], {'default': '0', 'null': 'True', 'max_digits': '10', 'decimal_places': '2', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'required': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'setup': ('django.db.models.fields.related.ForeignKey', [], {'default': '1', 'to': u"orm['administration.Setup']"})
        },
        u'administration.country': {
            'Meta': {'ordering': "[u'description']", 'object_name': 'Country'},
            'code': ('django.db.models.fields.CharField', [], {'max_length': '2', 'primary_key': 'True'}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'administration.ctgroup': {
            'Meta': {'object_name': 'CTGroup'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'institution': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'ctgroups'", 'null': 'True', 'on_delete': 'models.PROTECT', 'to': u"orm['administration.Institution']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'status': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'status_institution': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        u'administration.institution': {
            'Meta': {'ordering': "[u'name']", 'object_name': 'Institution'},
            'city': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'cnpj': ('django.db.models.fields.CharField', [], {'max_length': '18', 'null': 'True', 'blank': 'True'}),
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['administration.Country']", 'on_delete': 'models.PROTECT'}),
            'cpf': ('django.db.models.fields.CharField', [], {'max_length': '14', 'null': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '1000'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'institution_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['administration.InstitutionType']", 'on_delete': 'models.PROTECT'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'postal_address': ('django.db.models.fields.CharField', [], {'max_length': '1000', 'blank': 'True'}),
            'state': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'default': "u'M'", 'max_length': '1'}),
            'temp_workflow_observation': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'type_person': ('django.db.models.fields.CharField', [], {'default': "u'PF'", 'max_length': '2'}),
            'zip_code': ('django.db.models.fields.CharField', [], {'max_length': '18', 'null': 'True', 'blank': 'True'})
        },
        u'administration.institutiontype': {
            'Meta': {'object_name': 'InstitutionType'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'description_english': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'administration.interventionassignment': {
            'Meta': {'object_name': 'InterventionAssignment'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'administration.interventioncode': {
            'Meta': {'object_name': 'InterventionCode'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'administration.language': {
            'Meta': {'object_name': 'Language'},
            'code': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'administration.maskingtype': {
            'Meta': {'object_name': 'MaskingType'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'administration.setup': {
            'Meta': {'object_name': 'Setup'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'language_1_code': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'setup_set_lang1'", 'null': 'True', 'to': u"orm['administration.Language']"}),
            'language_1_mandatory': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'language_2_code': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'setup_set_lang2'", 'null': 'True', 'to': u"orm['administration.Language']"}),
            'language_2_mandatory': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'language_3_code': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'setup_set_lang3'", 'null': 'True', 'to': u"orm['administration.Language']"}),
            'language_3_mandatory': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'terms_of_use': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'terms_of_use_lang2': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'terms_of_use_lang3': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'})
        },
        u'administration.setupidentifier': {
            'Meta': {'object_name': 'SetupIdentifier'},
            'appear_in_identifications_step': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'help_text_code_lang_1': ('django.db.models.fields.TextField', [], {'max_length': '1000', 'null': 'True', 'blank': 'True'}),
            'help_text_code_lang_2': ('django.db.models.fields.TextField', [], {'max_length': '1000', 'null': 'True', 'blank': 'True'}),
            'help_text_code_lang_3': ('django.db.models.fields.TextField', [], {'max_length': '1000', 'null': 'True', 'blank': 'True'}),
            'help_text_date_lang_1': ('django.db.models.fields.TextField', [], {'max_length': '1000', 'null': 'True', 'blank': 'True'}),
            'help_text_date_lang_2': ('django.db.models.fields.TextField', [], {'max_length': '1000', 'null': 'True', 'blank': 'True'}),
            'help_text_date_lang_3': ('django.db.models.fields.TextField', [], {'max_length': '1000', 'null': 'True', 'blank': 'True'}),
            'help_text_description_lang_1': ('django.db.models.fields.TextField', [], {'max_length': '1000', 'null': 'True', 'blank': 'True'}),
            'help_text_description_lang_2': ('django.db.models.fields.TextField', [], {'max_length': '1000', 'null': 'True', 'blank': 'True'}),
            'help_text_description_lang_3': ('django.db.models.fields.TextField', [], {'max_length': '1000', 'null': 'True', 'blank': 'True'}),
            'help_text_lang_1': ('django.db.models.fields.TextField', [], {'max_length': '1000', 'null': 'True', 'blank': 'True'}),
            'help_text_lang_2': ('django.db.models.fields.TextField', [], {'max_length': '1000', 'null': 'True', 'blank': 'True'}),
            'help_text_lang_3': ('django.db.models.fields.TextField', [], {'max_length': '1000', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'issuing_body': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'mandatory': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'mask': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'administration.studyphase': {
            'Meta': {'object_name': 'StudyPhase'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'administration.studypurpose': {
            'Meta': {'object_name': 'StudyPurpose'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'clinical_trials.clinicaltrial': {
            'Meta': {'object_name': 'ClinicalTrial'},
            'allocation_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['administration.AllocationType']", 'null': 'True', 'blank': 'True'}),
            'brazil_target_size': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'code': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True'}),
            'date_creation': ('django.db.models.fields.DateField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'date_first_enrollment': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'date_last_change': ('django.db.models.fields.DateField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'date_last_enrollment': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'date_last_publication': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'date_last_state_change': ('django.db.models.fields.DateField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'expanded_access_program': ('django.db.models.fields.CharField', [], {'max_length': '1', 'null': 'True', 'blank': 'True'}),
            'followup_period': ('django.db.models.fields.CharField', [], {'max_length': '2', 'null': 'True', 'blank': 'True'}),
            'gender': ('django.db.models.fields.CharField', [], {'max_length': '1', 'null': 'True', 'blank': 'True'}),
            'group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['administration.CTGroup']", 'null': 'True', 'blank': 'True'}),
            'holder': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'holding_clinical_trials'", 'null': 'True', 'to': u"orm['login.RebecUser']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'inclusion_maximum_age': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'inclusion_minimum_age': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'intervention_assignment': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['administration.InterventionAssignment']", 'null': 'True', 'blank': 'True'}),
            'intervention_codes': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['administration.InterventionCode']", 'null': 'True', 'blank': 'True'}),
            'language_1_mandatory': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'language_2_mandatory': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'language_3_mandatory': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'masking_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['administration.MaskingType']", 'null': 'True', 'blank': 'True'}),
            'maximum_age_no_limit': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'maximum_age_unit': ('django.db.models.fields.CharField', [], {'max_length': '1', 'null': 'True', 'blank': 'True'}),
            'minimum_age_no_limit': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'minimum_age_unit': ('django.db.models.fields.CharField', [], {'max_length': '1', 'null': 'True', 'blank': 'True'}),
            'number_arms': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'recruitment_countries': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['administration.Country']", 'symmetrical': 'False'}),
            'reference_period': ('django.db.models.fields.CharField', [], {'max_length': '2', 'null': 'True', 'blank': 'True'}),
            'statistical_sample_analysis': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'study_final_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'study_phase': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['administration.StudyPhase']", 'null': 'True', 'blank': 'True'}),
            'study_purpose': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['administration.StudyPurpose']", 'null': 'True', 'blank': 'True'}),
            'study_status': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'study_type': ('django.db.models.fields.CharField', [], {'max_length': '1', 'null': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['login.RebecUser']"}),
            'world_target_size': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        u'clinical_trials.contact': {
            'Meta': {'object_name': 'Contact'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'city': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['administration.Country']"}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '100'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'institution': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['administration.Institution']", 'null': 'True', 'blank': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'middle_name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            'postal_code': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'state': ('django.db.models.fields.CharField', [], {'max_length': '500'})
        },
        u'clinical_trials.ctattachment': {
            'Meta': {'object_name': 'CTAttachment'},
            'attachment': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['administration.Attachment']"}),
            'clinical_trial': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['clinical_trials.ClinicalTrial']"}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            'file': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'link': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'public': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'clinical_trials.ctcontact': {
            'Meta': {'unique_together': "((u'contact', u'clinical_trial', u'contact_type'),)", 'object_name': 'CTContact'},
            'clinical_trial': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['clinical_trials.ClinicalTrial']"}),
            'contact': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['clinical_trials.Contact']"}),
            'contact_type': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'clinical_trials.ctdetail': {
            'Meta': {'object_name': 'CTDetail'},
            'acronym': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'clinical_trial': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['clinical_trials.ClinicalTrial']"}),
            'exclusion_criteria': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'health_condition': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'inclusion_criteria': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'intervention': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'language_code': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'public_title': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'scientific_title': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'study_description': ('django.db.models.fields.TextField', [], {})
        },
        u'clinical_trials.ctintervention': {
            'Meta': {'object_name': 'CTIntervention'},
            'arm_type': ('django.db.models.fields.IntegerField', [], {}),
            'clinical_trial': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'interventions'", 'to': u"orm['clinical_trials.ClinicalTrial']"}),
            'description': ('django.db.models.fields.TextField', [], {'max_length': '1000'}),
            'description_other': ('django.db.models.fields.CharField', [], {'max_length': '450', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'intervention_name': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'number_of_patients': ('django.db.models.fields.IntegerField', [], {})
        },
        u'clinical_trials.ctinterventionobservation': {
            'Meta': {'object_name': 'CTInterventionObservation'},
            'clinical_trial': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'intervention_observations'", 'to': u"orm['clinical_trials.ClinicalTrial']"}),
            'description': ('django.db.models.fields.TextField', [], {'max_length': '1000'}),
            'group_name': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'study_type': ('django.db.models.fields.IntegerField', [], {})
        },
        u'clinical_trials.descriptor': {
            'Meta': {'object_name': 'Descriptor'},
            'clinical_trial': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['clinical_trials.ClinicalTrial']"}),
            'descriptor_type': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'parent_vocabulary': ('django.db.models.fields.CharField', [], {'max_length': '6'}),
            'vocabulary_item_en': ('django.db.models.fields.CharField', [], {'max_length': '4000'}),
            'vocabulary_item_es': ('django.db.models.fields.CharField', [], {'max_length': '4000'}),
            'vocabulary_item_pt': ('django.db.models.fields.CharField', [], {'max_length': '4000'})
        },
        u'clinical_trials.identifier': {
            'Meta': {'unique_together': "((u'setup_identifier', u'clinical_trial'),)", 'object_name': 'Identifier'},
            'clinical_trial': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['clinical_trials.ClinicalTrial']"}),
            'code': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '2000', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'setup_identifier': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['administration.SetupIdentifier']"})
        },
        u'clinical_trials.outcome': {
            'Meta': {'object_name': 'Outcome'},
            'clinical_trial': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['clinical_trials.ClinicalTrial']"}),
            'found_outcome': ('django.db.models.fields.TextField', [], {'max_length': '20000', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'measurement_method': ('django.db.models.fields.CharField', [], {'max_length': '20000'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '20000'}),
            'outcome_type': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'point_of_time_of_interest': ('django.db.models.fields.CharField', [], {'max_length': '20000'})
        },
        u'clinical_trials.recruitmentinterrupt': {
            'Meta': {'object_name': 'RecruitmentInterrupt'},
            'clinical_trial': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'recruitment_interruptions'", 'to': u"orm['clinical_trials.ClinicalTrial']"}),
            'comments': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'date_time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'interrupt': ('django.db.models.fields.BooleanField', [], {}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['login.RebecUser']"})
        },
        u'clinical_trials.sponsor': {
            'Meta': {'object_name': 'Sponsor'},
            'clinical_trial': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['clinical_trials.ClinicalTrial']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'institution': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'sponsor_items'", 'null': 'True', 'to': u"orm['administration.Institution']"}),
            'research_centers_in_brazil': ('django.db.models.fields.TextField', [], {'max_length': '3000', 'null': 'True', 'blank': 'True'}),
            'sponsor_type': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'study_location': ('django.db.models.fields.TextField', [], {'max_length': '3000', 'null': 'True', 'blank': 'True'}),
            'support_source': ('django.db.models.fields.TextField', [], {'max_length': '3000', 'null': 'True', 'blank': 'True'})
        },
        u'clinical_trials.studystatus': {
            'Meta': {'object_name': 'StudyStatus'},
            'clinical_trial': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['clinical_trials.ClinicalTrial']"}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'login.rebecuser': {
            'Meta': {'object_name': 'RebecUser'},
            'country': ('django.db.models.fields.related.ForeignKey', [], {'default': '1', 'to': u"orm['administration.Country']"}),
            'cpf': ('django.db.models.fields.CharField', [], {'max_length': '14', 'null': 'True', 'blank': 'True'}),
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'gender': ('django.db.models.fields.IntegerField', [], {}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'ocuppation': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'passport': ('django.db.models.fields.CharField', [], {'max_length': '14', 'null': 'True', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'race': ('django.db.models.fields.IntegerField', [], {}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        }
    }

    complete_apps = ['clinical_trials']