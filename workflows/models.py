from django.db import models
from django.conf import settings
from django.contrib.contenttypes import generic
from django.contrib.contenttypes.models import ContentType
from django.utils.translation import ugettext_lazy as _



class Workflow(models.Model):
    """A workflow consists of a sequence of connected (through transitions)
    states. It can be assigned to a model and / or model instances. If a
    model instance has a workflow it takes precendence over the model's
    workflow.

    :attr model: The model the workflow belongs to. Can be any
    :attr content: The object the workflow belongs to.
    :attr name: The unique name of the workflow.
    :attr states: The states of the workflow.
    :attr initial_state: The initial state the model / content gets if created.
    """
    name = models.CharField(
        _(u"Name"),
        max_length=100,
        unique=True)
    initial_state = models.ForeignKey(
        "State",
        verbose_name=_(u"Initial state"),
        related_name="workflow_state",
        blank=True,
        null=True)
    permissions = models.ManyToManyField(
        "permissions.Permission",
        verbose_name=_(u"Permissions"),
        symmetrical=False,
        through="WorkflowPermissionRelation")

    def __unicode__(self):
        return self.name

    def get_initial_state(self):
        """Returns the initial state of the workflow. Takes the first one if
        no state has been defined.
        """
        if self.initial_state:
            return self.initial_state
        else:
            try:
                return self.states.all()[0]
            except IndexError:
                return None

    def get_objects(self):
        """Returns all objects which have this workflow assigned. Globally
        (via the object's content type) or locally (via the object itself).
        """
        import workflows.utils
        objs = []

        # Get all objects whose content type has this workflow
        for wmr in WorkflowModelRelation.objects.filter(workflow=self):
            ctype = wmr.content_type
            # We have also to check whether the global workflow is not
            # overwritten.
            for obj in ctype.model_class().objects.all():
                if workflows.utils.get_workflow(obj) == self:
                    objs.append(obj)

        # Get all objects whose local workflow this workflow
        for wor in WorkflowObjectRelation.objects.filter(workflow=self):
            if wor.content not in objs:
                objs.append(wor.content)

        return objs

    def set_to(self, ctype_or_obj):
        """Sets the workflow to passed content type or object. See the specific
        methods for more information.

        :param ctype_or_obj: The content type or the object to which the
            workflow should be set. Can be either a ContentType instance or any
            Django model instance.
        """
        if isinstance(ctype_or_obj, ContentType):
            return self.set_to_model(ctype_or_obj)
        else:
            return self.set_to_object(ctype_or_obj)

    def set_to_model(self, ctype):
        """Sets the workflow to the passed content type. If the content
        type has already an assigned workflow the workflow is overwritten.

        :param ctype: The content type which gets the workflow. Can be any
            Django model instance.
        """
        try:
            wor = WorkflowModelRelation.objects.get(content_type=ctype)
        except WorkflowModelRelation.DoesNotExist:
            WorkflowModelRelation.objects.create(content_type=ctype,
                                                 workflow=self)
        else:
            wor.workflow = self
            wor.save()

    def set_to_object(self, obj):
        """Sets the workflow to the passed object.

        If the object has already the given workflow nothing happens. Otherwise
        the workflow is set to the objectthe state is set to the workflow's
        initial state.

        :param obj: The object which gets the workflow.
        """
        import workflows.utils

        ctype = ContentType.objects.get_for_model(obj)
        try:
            wor = WorkflowObjectRelation.objects.get(content_type=ctype,
                                                     content_id=obj.id)
        except WorkflowObjectRelation.DoesNotExist:
            WorkflowObjectRelation.objects.create(content=obj, workflow=self)
            workflows.utils.set_state(obj, self.initial_state)
        else:
            if wor.workflow != self:
                wor.workflow = self
                wor.save()
                workflows.utils.set_state(self.initial_state)


class State(models.Model):
    """A certain state within workflow.

    :attr name: The unique name of the state within the workflow.
    :attr workflow: The workflow to which the state belongs.
    :attr transitions: The transitions of a workflow state.
    """
    #states fixed PKs
    STATE_EM_PREENCHIMENTO = 1
    STATE_AGUARDANDO_MODERACAO = 2
    STATE_REVISANDO_PREENCHIMENTO = 3
    STATE_AGUARDANDO_AVALIACAO = 4
    STATE_EM_AVALIACAO = 5
    STATE_AGUARDANDO_REVISAO = 6
    STATE_EM_REVISAO = 7
    STATE_RESUBMETIDO = 8
    STATE_PUBLICADO = 9
    STATE_EM_MODERACAO_RESUBMETIDO = 10
    STATE_REVISAO_PREENCHIMENTO_RESUBMETIDO = 11
    

    name = models.CharField(
        _(u"Name"),
        max_length=100)
    workflow = models.ForeignKey(
        Workflow,
        verbose_name=_(u"Workflow"),
        related_name="states")
    transitions = models.ManyToManyField(
        "Transition",
        verbose_name=_(u"Transitions"),
        blank=True,
        null=True,
        related_name="states")

    class Meta:
        ordering = ("name", )

    def __unicode__(self):
        return "%s" % self.name

    def get_allowed_transitions(self, obj, user):
        """Returns all allowed transitions for passed object and user.

        param obj: The object to get the transitions.
        param user: The user to verify the permissions.
        """
        from permissions.utils import has_permission
        #TODO: ver se estas verificacoes sao realmente necessarias
        return self.transitions.all()
        transitions = []
        for transition in self.transitions.all():
            permission = transition.permission
            if permission is None:
                transitions.append(transition)
            else:
                # First we try to get the objects specific has_permission
                # method (in case the object inherits from the PermissionBase
                # class).
                try:
                    if obj.has_permission(user, permission.codename):
                        transitions.append(transition)
                except AttributeError:
                    if has_permission(obj, user,
                                                        permission.codename):
                        transitions.append(transition)
        return transitions


class Transition(models.Model):
    """A transition from a source to a destination state. The transition can
    be used from several source states.

    :attr name: The unique name of the transition within a workflow.
    :attr workflow: The workflow to which the transition belongs. Must be a
        Workflow instance.
    :attr destination: The state after a transition has been processed. Must be
        a State instance.
    :attr condition: The condition when the transition is available. Can be any
        python expression.
    :attr permission: The necessary permission to process the transition. Must
        be a Permission instance.
    """
    TRANSITION_AVALIAR_PREENCHIMENTO = 1
    TRANSITION_CONCLUIR_PREENCHIMENTO_PARA_AVALIACAO = 2
    TRANSITION_CONCLUIR_PREENCHIMENTO = 3
    TRANSITION_AGUARDAR_AVALIACAO = 4
    TRANSITION_REVISAR_PREENCHIMENTO = 5
    TRANSITION_ENVIAR_MODERACAO = 6
    TRANSITION_CONCLUIR_REVISAO_AVALIACAO = 7
    TRANSITION_CONCLUIR_REVISAO_REVISAO = 8
    TRANSITION_INICIAR_AVALIACAO = 9
    TRANSITION_CONCLUIR_AVALIACAO = 10
    TRANSITION_INICIAR_REVISAO = 11
    TRANSITION_CONCLUIR_REVISAO = 12
    TRANSITION_RESUBMETER = 13
    TRANSITION_REVISAR = 14
    TRANSITION_AGUARDAR_REVISAO = 15
    TRANSITION_REENVIAR_A_REVISAO = 16
    TRANSITION_AVALIAR_PREENCHIMENTO_RESUBMETIDO = 17
    TRANSITION_REVISAR_PREENCHIMENTO_RESUBMETIDO = 18
    TRANSITION_ENVIAR_REVISAO = 19
    TRANSITION_ENVIAR_MODERACAO_RESUBMETIDO = 20

    name = models.CharField(
        _(u"Name"),
        max_length=100)
    workflow = models.ForeignKey(
        Workflow,
        verbose_name=_(u"Workflow"),
        related_name="transitions")
    destination = models.ForeignKey(
        State,
        verbose_name=_(u"Destination"),
        null=True,
        blank=True,
        related_name="destination_state")
    condition = models.CharField(
        _(u"Condition"),
        blank=True,
        max_length=100)
    permission = models.ForeignKey(
        "permissions.Permission",
        verbose_name=_(u"Permission"),
        blank=True,
        null=True)

    def __unicode__(self):
        return self.name


class StateObjectRelation(models.Model):
    """Stores the workflow state of an object.

    Provides a way to give any object a workflow state without changing the
    object's model.

    :attr content: The object for which the state is stored. This can be any
        instance of a Django model.
    :attr state: The state of content. This must be a State instance.
    :attr update_date: The date when was update the state.
    """
    content_type = models.ForeignKey(
        ContentType,
        verbose_name=_(u"Content type"),
        related_name="state_object",
        blank=True,
        null=True)
    content_id = models.PositiveIntegerField(
        _(u"Content id"),
        blank=True,
        null=True)
    content = generic.GenericForeignKey(
        ct_field="content_type",
        fk_field="content_id")
    state = models.ForeignKey(
        State,
        verbose_name=_(u"State"))
    update_date = models.DateTimeField(
        auto_now=True,
        verbose_name=_(u"Update date"),)

    def __unicode__(self):
        return "%s %s - %s" % (self.content_type.name, self.content_id,
                               self.state.name)

    class Meta:
        unique_together = ("content_type", "content_id", "state")


class WorkflowObjectRelation(models.Model):
    """Stores an workflow of an object.

    Provides a way to give any object a workflow without changing the object's
    model.

    :attr content: The object for which the workflow is stored. This can be any
        instance of a Django model.
    :attr workflow: The workflow which is assigned to an object. This needs to
        be a workflow instance.
    """
    content_type = models.ForeignKey(
        ContentType,
        verbose_name=_(u"Content type"),
        related_name="workflow_object",
        blank=True,
        null=True)
    content_id = models.PositiveIntegerField(
        _(u"Content id"),
        blank=True,
        null=True)
    content = generic.GenericForeignKey(
        ct_field="content_type",
        fk_field="content_id")
    workflow = models.ForeignKey(
        Workflow,
        verbose_name=_(u"Workflow"),
        related_name="wors")

    class Meta:
        unique_together = ("content_type", "content_id")

    def __unicode__(self):
        return "%s %s - %s" % (self.content_type.name, self.content_id,
                               self.workflow.name)


class StateObjectHistory(models.Model):
    """Stores the workflow state history of an object.

    Provides a way to give any object a workflow state history without changing
    the object's model.

    :attr content: The object for which the state is stored. This can be any
        instance of a Django model.
    :attr observation: An observation of the transition of state.
    :attr state: The state of content. This must be a State instance.
    :attr update_date: The date when was update the state.
    :attr user: User that changed state.
    """
    content_type = models.ForeignKey(
        ContentType,
        verbose_name=_(u"Content type"),
        related_name="state_history",
        blank=True,
        null=True)
    content_id = models.PositiveIntegerField(
        _("Content id"),
        blank=True,
        null=True)
    content = generic.GenericForeignKey(
        ct_field="content_type",
        fk_field="content_id")
    observation = models.CharField(
        max_length=2000,
        verbose_name=_(u"Observation"),
        blank=True,
        null=True)
    state = models.ForeignKey(
        State,
        verbose_name=_(u"State"))
    update_date = models.DateTimeField(
        verbose_name=_(u"Update Date"))
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name=_(u"User"),
        on_delete=models.PROTECT,
        blank=True,
        null=True)
    trial_snapshot = models.TextField(u"Clinical trial snapshot"
                                      u"on this given state change",
                                      null=True, blank=True)

    def __unicode__(self):
        return u"%s %s - %s" % (self.content_type.name, self.content_id,
                                self.state.name)


class StateObjectFieldObservation(models.Model):
    """Stores the observation of field on state an object.

    :attr content: The object for which the state is stored. This can be any
        instance of a Django model.
    :attr field_name:  The name of the field.
    :attr observation: The observation fo the field.
    :attr objects_state: The register of the state of content.
    :attr update_date: The date when was update the state.
    :attr user: User that changed state.
    """
    content_type = models.ForeignKey(
        ContentType,
        verbose_name=_(u"Content type"),
        related_name="state_observations",
        blank=True,
        null=True)
    content_id = models.PositiveIntegerField(
        _(u"Content id"),
        blank=True,
        null=True)
    content = generic.GenericForeignKey(
        ct_field="content_type",
        fk_field="content_id")
    field_name = models.CharField(
        max_length=200,
        verbose_name=_(u"Field Name"))
    observation = models.CharField(
        max_length=2000,
        verbose_name=_(u"Observation"))
    object_state = models.ForeignKey(
        StateObjectHistory,
        verbose_name=_(u"Object State"),
        related_name="fields_observation")
    update_date = models.DateTimeField(
        auto_now_add=True,
        verbose_name=_(u"Update Date"))
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name=_(u"User"),
        on_delete=models.PROTECT,
        blank=True,
        null=True)


    def __unicode__(self):
        return u"%s - %s" % (self.field_name, self.object_state)

    def get_clinical_trial(self):
        obj = self.content_type.get_all_objects_for_this_type().get(pk=self.content_id)
        return getattr(obj, u'clinical_trial', obj)


class StateObjectGeneralObservation(models.Model):
    """
    Stores the observation of an state of an object.
    """
    content_type = models.ForeignKey(
        ContentType,
        verbose_name=_(u"Content type"),
        related_name="state_general_observations",
        blank=True,
        null=True)
    content_id = models.PositiveIntegerField(
        _(u"Content id"),
        blank=True,
        null=True)
    content = generic.GenericForeignKey(
        ct_field="content_type",
        fk_field="content_id")
    observation = models.CharField(
        max_length=2000,
        verbose_name=_(u"Observation"))
    object_state = models.ForeignKey(
        StateObjectHistory,
        verbose_name=_(u"Object State"),
        related_name="general_observations")
    update_date = models.DateTimeField(
        auto_now_add=True,
        verbose_name=_(u"Update Date"))
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name=_(u"User"),
        on_delete=models.PROTECT,
        blank=True,
        null=True)


    class Meta:
        ordering = [u'update_date',]

    def __unicode__(self):
        return u"%s - %s" % (self.content, self.object_state)

    def get_clinical_trial(self):
        obj = self.content_type.get_all_objects_for_this_type().get(pk=self.content_id)
        return getattr(obj, u'clinical_trial', obj)



class WorkflowModelRelation(models.Model):
    """Stores an workflow for a model (ContentType).

    Provides a way to give any object a workflow without changing the model.

    :attr content_type: The content type for which the workflow is stored.
        This can be any instance of a Django model.
    :attr workflow: The workflow which is assigned to an object. This needs to
        be a workflow instance.
    """
    content_type = models.ForeignKey(
        ContentType,
        verbose_name=_(u"Content Type"),
        unique=True)
    workflow = models.ForeignKey(
        Workflow,
        verbose_name=_(u"Workflow"),
        related_name="wmrs")

    def __unicode__(self):
        return "%s - %s" % (self.content_type.name, self.workflow.name)

# Permissions relation #######################################################


class WorkflowPermissionRelation(models.Model):
    """Stores the permissions for which a workflow is responsible.

    :attr workflow: The workflow which is responsible for the permissions.
        Needs to be a Workflow instance.
    :attr permission: The permission for which the workflow is responsible.
        Needs to be a Permission instance.
    """
    workflow = models.ForeignKey(Workflow)
    permission = models.ForeignKey(
        "permissions.Permission",
        related_name="permissions")

    class Meta:
        unique_together = ("workflow", "permission")

    def __unicode__(self):
        return "%s %s" % (self.workflow.name, self.permission.name)


class StateInheritanceBlock(models.Model):
    """Stores inheritance block for state and permission.

    :attr state: The state for which the inheritance is blocked. Needs to be a
        State instance.
    :attr permission: The permission for which the instance is blocked. Needs
        to be a Permission instance.
    """
    state = models.ForeignKey(
        State,
        verbose_name=_(u"State"))
    permission = models.ForeignKey(
        "permissions.Permission",
        verbose_name=_(u"Permission"))

    def __unicode__(self):
        return "%s %s" % (self.state.name, self.permission.name)


class StatePermissionRelation(models.Model):
    """Stores granted permission for state and role.

    :attr state: The state for which the role has the permission. Needs to be a
        State instance.
    :attr permission: The permission for which the workflow is responsible.
        Needs to be a Permission instance.
    :attr role: The role for which the state has the permission. Needs to be a
        lfc Role instance.
    """
    state = models.ForeignKey(
        State,
        verbose_name=_(u"State"))
    permission = models.ForeignKey(
        "permissions.Permission",
        verbose_name=_(u"Permission"))
    role = models.ForeignKey(
        "permissions.Role",
        verbose_name=_(u"Role"))

    def __unicode__(self):
        return "%s %s %s" % (self.state.name, self.role.name,
                             self.permission.name)

