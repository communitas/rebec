from django.contrib import admin
from workflows.models import State
from workflows.models import StateInheritanceBlock
from workflows.models import StatePermissionRelation
from workflows.models import StateObjectRelation
from workflows.models import Transition
from workflows.models import Workflow
from workflows.models import WorkflowObjectRelation
from workflows.models import WorkflowModelRelation
from workflows.models import WorkflowPermissionRelation
from workflows.models import StateObjectHistory
from workflows.models import StateObjectFieldObservation


class StateInline(admin.TabularInline):
    model = State


class WorkflowAdmin(admin.ModelAdmin):
    inlines = [
        StateInline,
    ]

admin.site.register(Workflow, WorkflowAdmin)


class StateObjectRelationAdmin(admin.ModelAdmin):
    list_display = ('content_type', 'content_id', 'state', 'update_date')
    list_filter = ('state', 'content_type')
    search_fields = ('content_id',)


class StateObjectHistoryAdmin(admin.ModelAdmin):
    list_display = ('content_type', 'content_id', 'state', 'update_date')
    list_filter = ('state', 'content_type')
    search_fields = ('content_id',)

admin.site.register(StateObjectRelation, StateObjectRelationAdmin)
admin.site.register(StateObjectHistory, StateObjectHistoryAdmin)

admin.site.register(State)
admin.site.register(StateInheritanceBlock)
admin.site.register(StatePermissionRelation)
admin.site.register(Transition)
admin.site.register(WorkflowObjectRelation)
admin.site.register(WorkflowModelRelation)
admin.site.register(WorkflowPermissionRelation)
admin.site.register(StateObjectFieldObservation)
