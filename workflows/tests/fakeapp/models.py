from django.db import models

class TestModel(models.Model):
    """Defines test model"""
    field1 = models.CharField(max_length=100, null=True)
    field2 = models.CharField(max_length=100, null=True)
    date_stamp = models.DateTimeField(max_length=100, null=True)