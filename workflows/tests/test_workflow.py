# -*- coding: utf-8 -*-
from django.test import TestCase
from django.contrib.contenttypes.models import ContentType
from workflows.models import (Workflow, StateObjectRelation,
                              StateObjectHistory, StateObjectFieldObservation)

from workflows.utils import *

from django.test.utils import override_settings
from django.conf import settings
from django.core.management import call_command
from django.db.models import loading
from workflows.tests.fakeapp.models import TestModel

from django.contrib.auth import get_user_model
User = get_user_model()


class WorkflowRelationTestCase(TestCase):
    """Workflow state relations class test."""

    @classmethod
    def setUpClass(cls):
        """Register a fake app and a test Model."""
        cls.original_installed_apps = settings.INSTALLED_APPS
        settings.INSTALLED_APPS += ('workflows.tests.fakeapp',)
        loading.cache.loaded = False
        call_command('syncdb', interactive=False, verbosity=0)
        super(WorkflowRelationTestCase, cls).setUpClass()

    @classmethod
    def tearDownClass(cls):
        """Give back orginal settings."""
        settings.INSTALLED_APPS = cls.original_installed_apps
        loading.cache.loaded = False
        super(WorkflowRelationTestCase, cls).tearDownClass()

    def setUp(self):
        """Create a instance to set a state workflow.
        """
        self.user = User.objects.create_user('user', 'user@email.com',
                                             'pass', gender=1, race=1, country_id=u'BR')
        self.user.is_superuser = True
        self.user.save()

        self.instance = TestModel.objects.create(field1='Foo')
        self.content_type = ContentType.objects.get_for_model(self.instance)
        workflow = Workflow.objects.get(pk=1)
        set_workflow(self.instance, workflow)
        self.state = get_state(self.instance)

    def tearDown(self):
        """Delete obj instance and user."""
        self.instance.delete()
        self.user.stateobjecthistory_set.all().delete()
        self.user.stateobjectfieldobservation_set.all().delete()
        self.user.delete()

    def test_set_field_observation(self):
        """Test 'test_set_field_observation' function.
        """
        set_field_observation(
            self.instance.pk,
            self.content_type,
            self.instance.pk,
            u'field1',
            u'observation',
            self.user,
            self.content_type,
        )

        self.assertEqual(StateObjectFieldObservation.objects.all().count(), 1)

        StateObjectFieldObservation.objects.all().delete()

    def test_get_field_observation(self):
        """Test 'test_get_field_observation' function.
        """
        set_field_observation(
            self.instance.pk,
            self.content_type,
            self.instance.pk,
            u'field1',
            u'observation_field1_one',
            self.user,
            self.content_type,
        )
        set_field_observation(
            self.instance.pk,
            self.content_type,
            self.instance.pk,
            u'field1',
            u'observation_field1_two',
            self.user,
            self.content_type,
        )

        transitions = get_allowed_transitions(self.instance, self.user)
        do_transition(self.instance, transitions[0], self.user)

        set_field_observation(
            self.instance.pk,
            self.content_type,
            self.instance.pk,
            u'field1',
            u'observation_field1',
            self.user,
            self.content_type,
        )

        observations = get_field_observation(
            self.instance,
            u'field1',
            self.state,
        )
        result = StateObjectFieldObservation.objects.filter(
            object_state__state=self.state).order_by(u'update_date')
        self.assertEqual(list(observations), list(result))

        observations = get_field_observation(
            self.instance,
            u'field2',
        )
        result = StateObjectFieldObservation.objects.filter(
            field_name=u'field2')
        self.assertEqual(list(observations), list(result))

        StateObjectFieldObservation.objects.all().delete()
        StateObjectHistory.objects.all().delete()

    def test_remove_states_from_object(self):
        """Test 'remove_states_from_object' function.
        """
        object_state = StateObjectHistory.objects.get(content_id=self.instance.pk)

        StateObjectFieldObservation.objects.create(
            content_type=self.content_type,
            content_id=self.instance.id,
            field_name=u'field1',
            observation=u'observation',
            object_state=object_state,
            user=self.user)

        self.assertEqual(StateObjectHistory.objects.all().count(), 1)
        self.assertEqual(StateObjectRelation.objects.all().count(), 1)
        self.assertEqual(StateObjectFieldObservation.objects.all().count(), 1)

        remove_states_from_object(self.instance)

        self.assertEqual(StateObjectHistory.objects.all().count(), 0)
        self.assertEqual(StateObjectRelation.objects.all().count(), 0)
        self.assertEqual(StateObjectFieldObservation.objects.all().count(), 0)

        workflow = Workflow.objects.get(pk=1)
        set_workflow(self.instance, workflow)
        set_initial_state(self.instance)
