# -*- coding: utf-8 -*-
import os
import json
from lxml import html, etree
from datetime import datetime, timedelta
from administration.models import Country, Institution, InstitutionType, \
                                  Setup, StudyPurpose, InterventionCode, \
                                  MaskingType, AllocationType, StudyPhase,  \
                                  SetupIdentifier
from clinical_trials.models import Contact, ClinicalTrial
from clinical_trials.helpers import generate_trial_id
from login.models import RebecUser
from django.contrib.auth.models import Group
import psycopg2

from clinical_trials.views import PublicClinicalTrialDetailView
from workflows.models import StateObjectHistory
from django.test import RequestFactory

REVISER_GROUP = Group.objects.get(name='reviser')
usuarios_nao_encontrados = []
patrocinadores_inexistentes = []
datas_recruamento_invalidas = []
#BASE_DIR = '/home/eerlo/Downloads/OLD/rebec_migracao/'
BASE_DIR = '/home/eerlo/Downloads/json_source/'
def tira_lixo(valor):
    if not hasattr(valor, '__getitem__') or not valor:
        return valor
    if valor[0] in ('"', "'"):
        valor = valor[1:]
    if valor[-1] in ('"', "'"):
        valor = valor[:-1]
    return valor

def tira_lixo_dict(valor):
    new_valor = {}
    for k, v in valor.items():
        new_valor[k] = tira_lixo(v)
    return new_valor

COUNTRY_TRANSLATE = json.loads(open(BASE_DIR+'/traducao_paises.json').read())
COUNTRY = json.loads(open(BASE_DIR+'/country.json').read())
for c, i in enumerate(COUNTRY_TRANSLATE):
    for j in COUNTRY:
        if j['label'] == i['label']:
            COUNTRY_TRANSLATE[c]['country_id'] = i['id']

def get_country_id_from_br_name(br_name):
    for i in COUNTRY_TRANSLATE:
        if br_name == i['description']:
            return i['object_id']

COUNTRY_LABEL_ID = {}
for coi in COUNTRY:
    COUNTRY_LABEL_ID[coi['id']] = coi[u'label']

#countries
def import_countries():
    BULK = []
    print 'Deleting and importing Countries...'
    Country.objects.all().delete()
    a = json.loads(open(BASE_DIR+'/country.json').read())
    q = len(a)
    for c, i in enumerate(a):
        try:
            #Country.objects.create(code=i['label'], description=i['description'])
            BULK.append(Country(code=i['label'], description=i['description']))
        except:
            print 'Erro importando country'
            from IPython import embed;embed()
        print 'criado %s de %s' % (c, q)
    print 'BULK!!'
    Country.objects.bulk_create(BULK)
import_countries()

#institutions
def import_institutions():
    BULK = []
    print 'Deleting and importing Types ofInstitutions...'
    InstitutionType.objects.all().delete()
    a = json.loads(open(BASE_DIR+'/institution_type.json').read())
    q = len(a)
    for c, i in enumerate(a):
        InstitutionType.objects.create(id=i['id'],
                                       description=tira_lixo(i['description']),
                                       description_english=tira_lixo(i['description']))
        print 'criado %s de %s' % (c, q)


    print 'Deleting and importing Institutions...'
    Institution.objects.all().delete()
    a = json.loads(open(BASE_DIR+'/instituicoes.json').read())
    q = len(a)
    inst_type_default = InstitutionType.objects.get_or_create(description='Migração para o Rebec2.0',
                                                   description_english='Migration to Rebec2.0'
                )[0].pk
    for c, i in enumerate(a):
        dados = {'id': i['id'],
                 'name': i['name'],
                 'postal_address': i['address'],
                 'country_id': COUNTRY_LABEL_ID[i['country_id']], 'city': i['city'],
                 'state': i['state'], 'institution_type_id': i['i_type_id'] or inst_type_default,
                 'status': u'M'}
        dados = tira_lixo_dict(dados)
        #falta o institution_type e o status da instituição
        #Institution.objects.get_or_create(**dados)
        BULK.append(Institution(**dados))
        print 'criado %s de %s' % (c, q)
    print 'BULK!!'
    Institution.objects.bulk_create(BULK)
import_institutions()


#contacts
def import_contacts():
    BULK = []
    print 'Deleting and importing Contacts...'
    Contact.objects.all().delete()
    a = json.loads(open(BASE_DIR+'/contatos.json').read())
    q = len(a)
    for c, i in enumerate(a):
        dados = {'id': i['id'], 'first_name': i['firstname'],
                 'middle_name': i['middlename'],
                 'last_name': i['lastname'],
                 'email': i['email'],
                 'institution_id': i['affiliation_id'],
                 'address': i['address'],
                 'country_id': COUNTRY_LABEL_ID[i['country_id']], 'city': i['city'],
                 'state': i['state'], 'postal_code': i['zip'],
                 'phone': i['telephone']}
        dados = tira_lixo_dict(dados)
        #Contact.objects.create(**dados)
        BULK.append(Contact(**dados))
        print 'criado %s de %s' % (c, q)
    print 'BULK!!'
    Contact.objects.bulk_create(BULK)
import_contacts()



#users
def import_users():
    print 'Deleting and importing Users...'
    #RebecUser.objects.all().delete()
    a = json.loads(open(BASE_DIR+'/usuarios_rebec.json').read())
    q = len(a)
    for c, i in enumerate(a):
        dados = {'id': i['id'], 'first_name': i['first_name'],
                 'username': i['username'],
                 'last_name': i['last_name'], 'email': i['email'],
                 'is_staff': False, 'is_superuser': False,
                 'date_joined': i['date_joined'], 'gender': 1, 'race': 1,
                 'name': '%s %s' %(i['first_name'] or '', i['last_name'] or '')}
        dados = tira_lixo_dict(dados)

        #staff e superuser sempre False fixo, pois só o usuário 'super' pode ser
        #staff e/ou superuser.
        #dados['is_superuser'] = dados['is_superuser'] == '1'
        #dados['is_staff'] = dados['is_staff'] == '1'
        #falta o gender e race
        new_user = RebecUser.objects.create(**dados)
        new_user.groups.add(Group.objects.get(name='registrant'))
        print 'criado %s de %s' % (c, q)
import_users()

REVISERS_USERS = json.loads(open(BASE_DIR + 'revisores.json').read())

def import_trials():
    print 'Deleting and importing Clinical Trials...'
    ClinicalTrial.objects.all().delete()

    
    #REVISER_USER = RebecUser.objects.get_or_create(
    #    username='migration_auto_gen_reviser',
    #    first_name='Rebec2.0 Migration auto generated user',
    #    last_name='Rebec2.0 Migration auto generated user',
    #    is_staff=False,
    #    is_superuser=False,
    #    gender=1,
    #    race=1,
    #    email='autogeneratedrebecuser@autogeneratedrebecuser.com'
    #)[0]
    CORINGA_USER = RebecUser.objects.get_or_create(
        username='user_not_found',
        first_name='Rebec2.0 not found user',
        last_name='Rebec2.0 not found user',
        is_staff=False,
        is_superuser=False,
        gender=1,
        race=1,
        email='coringarebecuser@rebec.org.br',
        id=99999
    )[0]
    #Coloca o coringa user como registrante
    CORINGA_USER.groups.add(Group.objects.get(name=u'registrant'))

    RBRS_RELATION = {}
    for rbr_rel in json.loads(open(BASE_DIR+'/trial_id_X_RBR.json').read()):
        RBRS_RELATION[int(rbr_rel[u'trial_id'])] = rbr_rel['RBR']

    UTNS_RELATION = {}
    for utn_rel in json.loads(open(BASE_DIR+'/trial_id_X_UTN.json').read()):
        UTNS_RELATION[utn_rel['utrn_number']] = utn_rel[u'trial_id']

    TRIALS_EXCLUSION_CRITERIAS = {}
    for trial_and_exclusion \
            in json.loads(open(BASE_DIR+'criterios_exclusao.json').read()):
        TRIALS_EXCLUSION_CRITERIAS[trial_and_exclusion[u'trial_id']] = \
            {'en': trial_and_exclusion[u'exclusion_criteria']}
    EXCLUSION_TRANSLATED = json.loads(open(BASE_DIR+'traducao_criterios_exclusao.json').read())
    for key_trialid in TRIALS_EXCLUSION_CRITERIAS.keys():
        
        for trex in [trex for trex in EXCLUSION_TRANSLATED if trex[u'trial_id'] == key_trialid]:
            if trex[u'exclusion_criteria_trad']:
                TRIALS_EXCLUSION_CRITERIAS[key_trialid][trex[u'language']] = trex[u'exclusion_criteria_trad']

    CREATION_DATES = {}
    for cdate in json.loads(open(BASE_DIR+'data_criacao.json').read()):
        CREATION_DATES[cdate[u'id']] = datetime.strptime(cdate[u'created'], u'%Y-%m-%d %H:%M:%S')

    REGISTRATION_DATES = {}
    for cdate in json.loads(open(BASE_DIR+'registration_date.json').read()):
        if cdate[u'date_registration']:
            REGISTRATION_DATES[cdate[u'trial_id']] = datetime.strptime(cdate[u'date_registration'], u'%Y-%m-%d %H:%M:%S')


    SECUNDARY_IDS = {}
    for secid in json.loads(open(BASE_DIR+'secundary_id.json').read()):
        if not SECUNDARY_IDS.has_key(secid[u'trial_id']):
            SECUNDARY_IDS[secid[u'trial_id']] = []
        if not secid[u'issuing_authority'] == u'UTN' and not secid[u'_deleted'] == u'1':
            SECUNDARY_IDS[secid[u'trial_id']].append(secid)

    ATTACHMENT_RELATIONS = {}
    for attach_rel in json.loads(open(BASE_DIR+'anexos.json').read()):
        if not ATTACHMENT_RELATIONS.has_key(int(attach_rel[u'trial_id'])):
            ATTACHMENT_RELATIONS[int(attach_rel[u'trial_id'])] = []
        ATTACHMENT_RELATIONS[int(attach_rel[u'trial_id'])].append(
            attach_rel)

    
    print 'carregando descritores...'
    if os.path.exists('descriptors_cache.json'):
        DESCRIPTORS = json.loads(open(u'descriptors_cache.json').read())
    else:
        descriptors_traducao_file = json.loads(open(BASE_DIR+'/descritores_traducao.json').read())
        DESCRIPTORS = {}
        for desctipt in json.loads(open(BASE_DIR+'/descritores.json').read()):
            this_new = {u'id': desctipt[u'id'], u'code': desctipt[u'code'], 'parent_vocabulary': desctipt['vocabulary'], 'descriptor_type': 'G' if desctipt[u'level'].strip() == u'general' else 'S', 'vocabulary_item_en': desctipt[u'text'].strip() }
            for desctipt_trans in descriptors_traducao_file:
                if desctipt_trans[u'object_id'] == this_new[u'id']:
                    if desctipt_trans[u'language'] == u'en' and desctipt_trans[u'text']:
                        this_new[u'vocabulary_item_en'] = desctipt_trans[u'text']
                    if desctipt_trans[u'language'] == u'es' and desctipt_trans[u'text']:
                        this_new[u'vocabulary_item_es'] = desctipt_trans[u'text']
                    if desctipt_trans[u'language'] == u'pt-br' and desctipt_trans[u'text']:
                        this_new[u'vocabulary_item_pt'] = desctipt_trans[u'text']

            if DESCRIPTORS.has_key(desctipt['trial_id']):
                DESCRIPTORS[desctipt['trial_id']] += [this_new]
            else:
                DESCRIPTORS[desctipt['trial_id']] = [this_new]

        open('descriptors_cache.json', 'w').write(json.dumps(DESCRIPTORS))


    a = json.loads(open(BASE_DIR+'rows.json').read())
    q = len(a)
    for c, i in enumerate(a):
        if False:#not RebecUser.objects.filter(username__iexact=i['LOGIN_REBEC']).exists():
            usuarios_nao_encontrados.append(i['LOGIN_REBEC'])
            print 'criando %s de %s - Usuario nao encontrado: %s' % (c, q,
                    i['LOGIN_REBEC'])
            continue
        print 'criando %s de %s - %s' % (c, q, UTNS_RELATION.get(i['UTN'], None))
        #if i['UTN'] != u'U1111-1119-7435':
        #    continue

        try:
            if i['UTN']:
                id_usar = UTNS_RELATION.get(i['UTN'], None)
            else:
                id_usar = None
            try:
                novo_trial = ClinicalTrial.objects.create(
                        user=RebecUser.objects.get(username__iexact=i['LOGIN_REBEC']),
                        holder=RebecUser.objects.get(username__iexact=i['LOGIN_REBEC']),
                      statistical_sample_analysis='F',
                      id=id_usar
                )
            except psycopg2.IntegrityError:
                novo_trial = ClinicalTrial.objects.create(
                        user=RebecUser.objects.get(username__iexact=i['LOGIN_REBEC']),
                        holder=RebecUser.objects.get(username__iexact=i['LOGIN_REBEC']),
                      statistical_sample_analysis='F',
                      id=id_usar
                )
        except RebecUser.DoesNotExist:
            novo_trial = ClinicalTrial.objects.create(
                        user=CORINGA_USER,
                        holder=CORINGA_USER,
                      statistical_sample_analysis='F',
                      id=id_usar
                )
        except Exception, exc:
            import ipdb;ipdb.set_trace()

        novo_trial.date_creation = CREATION_DATES.get(novo_trial.id, novo_trial.date_creation)

        #if 'Nao definido -' not in i['DESFECHO_PRIMARIO_1_PT']:
        novo_trial.outcome_set.create(outcome_type='P',
                                      found_outcome=i['DESFECHO_PRIMARIO_1_PT'],
                                      name=u'Primary')
        novo_trial.outcome_set.create(outcome_type='S',
                                      found_outcome=i['DESFECHO_SECUNDARIO_1_PT'],
                                      name=u'Secondary')

        RECRUITMENT_COUNTRIES = {}
        for rec_count in json.loads(open(BASE_DIR+'/paises_recrutamento.json').read()):
            if RECRUITMENT_COUNTRIES.has_key(rec_count['trial_id']):
                RECRUITMENT_COUNTRIES[rec_count['trial_id']] += [COUNTRY_LABEL_ID[rec_count[u'countrycode_id']]]
            else:
                RECRUITMENT_COUNTRIES[rec_count['trial_id']] = [COUNTRY_LABEL_ID[rec_count[u'countrycode_id']]]

        for rec_count in RECRUITMENT_COUNTRIES.get(novo_trial.id, []):
            novo_trial.recruitment_countries.add(
                Country.objects.get(pk=rec_count)
            )

        if Institution.objects.filter(name=i['PATROCINADOR_PRIMARIO']).exists():
            #import ipdb;ipdb.set_trace()
            novo_trial.sponsor_set.create(sponsor_type='P',
                                 institution=Institution.objects.filter(
                                     name=i['PATROCINADOR_PRIMARIO']).first())
        else:
            if ' definido -' not in  i['PATROCINADOR_PRIMARIO']:
                print "PATROCINADOR INEXISTENTE - %s" % i['PATROCINADOR_PRIMARIO']
                patrocinadores_inexistentes.append([novo_trial.pk, i['PATROCINADOR_PRIMARIO']])
        #    novo_trial.sponsor_set.create(sponsor_type='P',
        #                         support_source=i['PATROCINADOR_PRIMARIO']) 

        if Institution.objects.filter(name=i['PATROCINADOR_SECUNDARIO_1']).exists():
            novo_trial.sponsor_set.create(sponsor_type='S',
                                 institution=Institution.objects.filter(
                                     name=i['PATROCINADOR_SECUNDARIO_1']).first())
        else:
            if ' definido -' not in  i['PATROCINADOR_SECUNDARIO_1']:
                print "PATROCINADOR INEXISTENTE - %s" % i['PATROCINADOR_SECUNDARIO_1']
                patrocinadores_inexistentes.append([novo_trial.pk, i['PATROCINADOR_SECUNDARIO_1']])
        #    novo_trial.sponsor_set.create(sponsor_type='S',
        #                         support_source=i['PATROCINADOR_SECUNDARIO_1']) 

        if Institution.objects.filter(name=i['PATROCINADOR_SECUNDARIO_2']).exists():
            novo_trial.sponsor_set.create(sponsor_type='S',
                                 institution=Institution.objects.filter(
                                     name=i['PATROCINADOR_SECUNDARIO_2']).first())
        else:
            if ' definido -' not in  i['PATROCINADOR_SECUNDARIO_2']:
                print "PATROCINADOR INEXISTENTE - %s" % i['PATROCINADOR_SECUNDARIO_2']
                patrocinadores_inexistentes.append([novo_trial.pk, i['PATROCINADOR_SECUNDARIO_2']])
        #    novo_trial.sponsor_set.create(sponsor_type='U',
        #                         support_source=i['PATROCINADOR_SECUNDARIO_2']) 


        if ' definido -' not in i[u'APOIO_FINANCEIRO_OU_MATERIAL_1']:
            novo_trial.sponsor_set.create(sponsor_type='U',
                  support_source=i[u'APOIO_FINANCEIRO_OU_MATERIAL_1'].strip())

        if ' definido -' not in i[u'APOIO_FINANCEIRO_OU_MATERIAL_2']:
            novo_trial.sponsor_set.create(sponsor_type='U',
                  support_source=i[u'APOIO_FINANCEIRO_OU_MATERIAL_2'].strip())


        #if ' definido -' not in i['INTERVENCOES_PT']:



        novo_trial.exclusion_criteria = \
            TRIALS_EXCLUSION_CRITERIAS.get(novo_trial.pk, None)

        novo_trial.identifier_set.create(setup_identifier_id=2,
                                         code=i['UTN'].strip(),
                                         description=u"Organização Mundial de "
                                         u"Saúde"
                                        )
        for identificador in SECUNDARY_IDS.get(novo_trial.pk, []):
            try:
                setup_identifier = SetupIdentifier.objects.get(name__icontains=identificador[u'issuing_authority'])
            except SetupIdentifier.DoesNotExist:
                setup_identifier = SetupIdentifier.objects.get(pk=4)#outros
            novo_trial.identifier_set.create(setup_identifier=setup_identifier,
                                             code=identificador[u'id_number'],
                                             description=identificador[u'issuing_authority'])
        #for nome_identificador in [u'ID_SECUNDARIOS_1', u'ID_SECUNDARIOS_2',
        #                           u'ID_SECUNDARIOS_3']:
        #    if ' definido -' not in i[nome_identificador].lower():
        #        if u'Comitê de Ética'.lower().replace(u'ê', u'e').replace(u'é', u'e') in i[nome_identificador].lower().replace(u'ê', u'e').replace(u'é', u'e'):
        #            #if not novo_trial.identifier_set.filter(setup_identifier_id=3).exists():
        #            novo_trial.identifier_set.create(
        #                                       setup_identifier_id=3,
        #                                       code=i[nome_identificador],
        #                                       description=i[nome_identificador]
        #                                     )
        #        elif 'Plataforma Brasil'.lower() in i[nome_identificador].lower():
        #            #if not novo_trial.identifier_set.filter(setup_identifier_id=1).exists():
        #            novo_trial.identifier_set.create(
        #                    setup_identifier_id=1,
        #                    code=i[nome_identificador],
        #                    description=i[nome_identificador]
        #                )
        #        else:
        #            #if not novo_trial.identifier_set.filter(setup_identifier_id=4).exists():
        #            novo_trial.identifier_set.create(
        #                    setup_identifier_id=4,
        #                    code=i[nome_identificador],
        #                    description=i[nome_identificador]
        #                )


        


        novo_trial.brazil_target_size = novo_trial.world_target_size = i['TAMANHO_DA_AMOSTRA_ALVO']
        if i['BRACOS'].isdigit():
            novo_trial.number_arms = int(i['BRACOS'])
        else:
            novo_trial.number_arms = 0


        novo_trial.minimum_age_no_limit = i['UNIDADE_IDADE_MIN'] == 'Sem limite'
        novo_trial.maximum_age_no_limit = i['UNIDADE_IDADE_MAX'] == 'Sem limite'

        CONVERSAO_LIMITES_IDADES = {
            'anos': 'Y',
            'meses': 'M',
            'semanas': 'W',
            'dias': 'D',
            'horas': 'H'

        }
        if not novo_trial.minimum_age_no_limit:
            novo_trial.minimum_age_unit = \
                CONVERSAO_LIMITES_IDADES[i['UNIDADE_IDADE_MIN'].encode(u'utf-8').strip().lower()]

        if not novo_trial.maximum_age_no_limit:
            novo_trial.maximum_age_unit = \
                CONVERSAO_LIMITES_IDADES[i['UNIDADE_IDADE_MAX'].encode(u'utf-8').strip().lower()]


        if i['IDADE_MIN'].isdigit():
           novo_trial.inclusion_minimum_age = int(i['IDADE_MIN'])

        if i['IDADE_MAX'].isdigit():
           novo_trial.inclusion_maximum_age = int(i['IDADE_MAX'])


        if novo_trial.minimum_age_no_limit and novo_trial.inclusion_minimum_age > 0:
               novo_trial.minimum_age_unit = u'Y'
               novo_trial.minimum_age_no_limit = False

        if novo_trial.maximum_age_no_limit and novo_trial.inclusion_maximum_age > 0:
               novo_trial.maximum_age_unit = u'Y'
               novo_trial.maximum_age_no_limit = False


        CONVERSAO_ENFOQUE = {
                u'Diagnóstico'.lower(): 1,
                u'Etiológico'.lower(): 2,
                u'Prognóstico'.lower(): 3,
                u'Prevenção'.lower(): 4,
                u'Tratamento'.lower(): 5,
                u'Outro'.lower(): 6,
        }
        if ' definido -' not in i[u'ENFOQUE_DO_ESTUDO'] and i[u'ENFOQUE_DO_ESTUDO']:
            novo_trial.study_purpose_id = \
                CONVERSAO_ENFOQUE[i[u'ENFOQUE_DO_ESTUDO'].strip().lower()]


        CONVERSAO_DESENHO_INTERVENCAO = {
            'Grupo único': 1,
            'Paralelo': 2,
            'Cruzado': 3,
            'Fatorial': 4,
            'Outro': 4
        }
        if ' definido -' not in i[u'DESENHO_DA_INTERVENCAO'] and i[u'DESENHO_DA_INTERVENCAO']:
            try:
                novo_trial.intervention_assignment_id = \
                    CONVERSAO_DESENHO_INTERVENCAO[i[u'DESENHO_DA_INTERVENCAO'].encode('utf-8')]
            except KeyError:
                pass
        else:
            novo_trial.intervention_assignment_id = 6


        CONVERSAO_STUDY_STATUS = {
            'ainda': 'N',
            'completa': 'A',
            'cancelado': 'W',
            'outros': 'O',
            'conclu': 'C',
            'temporariamente': 'S',
            'recrutando': 'R',
            'prematuro': 'T'
        }
        study_status = i[u'SITUACAO_DO_RECRUTAMENTO'].lower()
        study_status = [sta for sta in CONVERSAO_STUDY_STATUS.keys() if sta in study_status]
        study_status = study_status[0] if study_status else 'outros'

        novo_trial.study_status = CONVERSAO_STUDY_STATUS[study_status]


        CONVERSAO_MASCARAMENTO = {
            'Aberto': 1,
            'Unicego': 2,
            'Duplo-cego': 3,
            'Triplo-cego': 4,
            'N/A': 7,
            'Desconhecido': 7,
        }
        if ' definido -' not in i[u'MASCARAMENTO']:
            novo_trial.masking_type_id = \
                    CONVERSAO_MASCARAMENTO[i[u'MASCARAMENTO'].encode(u'utf-8')]
        else:
            novo_trial.masking_type_id = 7


        novo_trial.study_type = 'I' if i['TIPO_DE_ESTUDO'] == 'intervencao' \
                                 else 'O'



        for desctipt in DESCRIPTORS.get(novo_trial.id, []):
            novo_trial.descriptor_set.create(descriptor_type=desctipt[u'descriptor_type'],
            parent_vocabulary={'DeCS': 'DeCS', 'ICD-10': 'CID-10', 'CAS': 'CAS'}[desctipt[u'parent_vocabulary'].strip()],
            vocabulary_item_pt=(desctipt.get('code', '') + ' - ' + desctipt.get(u'vocabulary_item_pt', '').strip()) if desctipt.has_key(u'vocabulary_item_pt') else '',
            vocabulary_item_en=(desctipt.get('code', '') + ' - ' + desctipt.get(u'vocabulary_item_en', '').strip()) if desctipt.has_key(u'vocabulary_item_en') else '',
            vocabulary_item_es=(desctipt.get('code', '') + ' - ' + desctipt.get(u'vocabulary_item_es', '').strip()) if desctipt.has_key(u'vocabulary_item_es') else ''
             )



        if i[u'DATA_PRIMEIRO_RECRUTAMENTO'] and \
           i[u'DATA_PRIMEIRO_RECRUTAMENTO'] != u'None': 
            try:
                novo_trial.date_first_enrollment = \
                 datetime.strptime(i[u'DATA_PRIMEIRO_RECRUTAMENTO'], '%Y-%m-%d')
            except ValueError:
                try:
                    novo_trial.date_first_enrollment = \
                     datetime.strptime(i[u'DATA_PRIMEIRO_RECRUTAMENTO'], '%d/%m/%Y')
                except ValueError:
                    try:
                        novo_trial.date_first_enrollment = \
                         datetime.strptime(i[u'DATA_PRIMEIRO_RECRUTAMENTO'], '%d/%m%Y')
                    except ValueError:
                        if i[u'DATA_PRIMEIRO_RECRUTAMENTO'].strip() == u'31/11/2011':
                            novo_trial.date_first_enrollment = \
                                datetime.strptime(u'30/11/2011', '%d/%m%Y')
                        datas_recruamento_invalidas.append(i[u'DATA_PRIMEIRO_RECRUTAMENTO'])
                        print 'Data invalida!!! %s' % i[u'DATA_PRIMEIRO_RECRUTAMENTO'] 


        if i[u'DATA_ULTIMO_RECRUTAMENTO'] and \
           i[u'DATA_ULTIMO_RECRUTAMENTO'] != u'None': 
            try:
                novo_trial.date_last_enrollment = \
                   datetime.strptime(i[u'DATA_ULTIMO_RECRUTAMENTO'], '%Y-%m-%d')
            except ValueError:
                try:
                    novo_trial.date_last_enrollment = \
                       datetime.strptime(i[u'DATA_ULTIMO_RECRUTAMENTO'], '%d/%m/%Y')
                except ValueError:
                    try:
                        novo_trial.date_last_enrollment = \
                           datetime.strptime(i[u'DATA_ULTIMO_RECRUTAMENTO'], '%d/%m%Y')
                    except ValueError:
                        datas_recruamento_invalidas.append(i[u'DATA_ULTIMO_RECRUTAMENTO'])
                        print 'Data invalida!!! %s' % i[u'DATA_ULTIMO_RECRUTAMENTO'] 



        GENEROS = {"masculino": "M",
                   "feminino": "F",
                   "ambos": "B"}
        if GENEROS.has_key(i[u'GENERO'].lower()):
            novo_trial.gender = GENEROS[i[u'GENERO'].lower()]

        PROGRAMAS_ACESSO = {"desconhecido": "U",
                            "sim": "Y",
                            "nao": "N"}
        if PROGRAMAS_ACESSO.has_key(i[u'PROGRAMA_DE_ACESSO'].lower()):
            novo_trial.expanded_access_program = \
                PROGRAMAS_ACESSO[i[u'PROGRAMA_DE_ACESSO'].lower()]


        CONVERSAO_INTERVENTION_CODES = {
            u'outro': 9,
            u'dispositivo': 2,
            u'comportamental': 5,
            u'suplementação alimentar': 7,
            u'medicamento': 8,
            u'radiação': 4,
            u'procedimento/cirurgia': 3,
            u'genética': 6,
            u'produto biológico/vacina': 1
        }

        if 'nao definido' not in i['CATEGORIA_DAS_INTERVENCOES_1_PT'].lower():
            novo_trial.intervention_codes.add(
                InterventionCode.objects.get(pk=CONVERSAO_INTERVENTION_CODES[i['CATEGORIA_DAS_INTERVENCOES_1_PT'].lower().strip()]))

        if 'nao definido' not in i['CATEGORIA_DAS_INTERVENCOES_2_PT'].lower():
            novo_trial.intervention_codes.add(
                InterventionCode.objects.get(pk=CONVERSAO_INTERVENTION_CODES[i['CATEGORIA_DAS_INTERVENCOES_2_PT'].lower().strip()]))


        if i['ALOCACAO'].lower().strip() == 'randomizado controlado':
            novo_trial.allocation_type_id = 1
        else:
            novo_trial.allocation_type_id = 4


        INTERVENTION_DESCRIPTIONS = {}
        for int_desc in json.loads(open(BASE_DIR+'/clinical_trials.json').read()):
            INTERVENTION_DESCRIPTIONS[int_desc['id']] = int_desc[u'i_freetext']


        if novo_trial.study_type == 'I':
            if 'definido -' not in i[u'DESCRITORES_INTERVENCAO_1_PT'].strip().lower():
                novo_trial.interventions.create(
                    intervention_name='; '.join(novo_trial.intervention_codes.values_list(u'description', flat=True)),
                    description=INTERVENTION_DESCRIPTIONS.get(novo_trial.pk, '').strip(),
                    number_of_patients=novo_trial.brazil_target_size,
                    arm_type=5
                )

            if 'definido -' not in i[u'DESCRITORES_INTERVENCAO_2_PT'].strip().lower():
                novo_trial.interventions.create(
                    intervention_name='; '.join(novo_trial.intervention_codes.values_list(u'description', flat=True)),
                    description=INTERVENTION_DESCRIPTIONS.get(novo_trial.pk, '').strip(),
                    number_of_patients=novo_trial.brazil_target_size,
                    arm_type=5
                )

            if 'definido -' not in i[u'DESCRITORES_INTERVENCAO_3_PT'].strip().lower():
                novo_trial.interventions.create(
                    intervention_name='; '.join(novo_trial.intervention_codes.values_list(u'description', flat=True)),
                    description=INTERVENTION_DESCRIPTIONS.get(novo_trial.pk, '').strip(),
                    number_of_patients=novo_trial.brazil_target_size,
                    arm_type=5
                )
        else:
            if 'definido -' not in i[u'DESCRITORES_INTERVENCAO_1_PT'].strip().lower():
                novo_trial.intervention_observations.create(
                    study_type=4,
                    group_name=u'Grupo Observacional nao definido(migracao para o novo ReBec), preencha o valor',
                    description=u'%s\n%s' % (i[u'INTERVENCOES_PT'].strip(), i[u'DESCRITORES_INTERVENCAO_1_PT'].strip()),
                )
            if 'definido -' not in i[u'DESCRITORES_INTERVENCAO_2_PT'].strip().lower():
                novo_trial.intervention_observations.create(
                    study_type=4,
                    group_name=u'Grupo Observacional nao definido(migracao para o novo ReBec), preencha o valor',
                    description=u'%s\n%s' % (i[u'INTERVENCOES_PT'].strip(), i[u'DESCRITORES_INTERVENCAO_2_PT'].strip()),
                )
            if 'definido -' not in i[u'DESCRITORES_INTERVENCAO_3_PT'].strip().lower():
                novo_trial.intervention_observations.create(
                    study_type=4,
                    group_name=u'Grupo Observacional nao definido(migracao para o novo ReBec), preencha o valor',
                    description=u'%s\n%s' % (i[u'INTERVENCOES_PT'].strip(), i[u'DESCRITORES_INTERVENCAO_3_PT'].strip()),
                )



        if 'nao definido' not in i['FASE'].lower():
            if i['FASE'].lower().strip() == 'n/a':
                novo_trial.study_phase_id = 1
            elif i['FASE'].lower().strip().isdigit():
                novo_trial.study_phase_id = {'0': 2,
                                             '1': 3,
                                             '2': 4,
                                             '3': 5,
                                             '4': 6}[i['FASE'].lower().strip()]
            else:
                novo_trial.study_phase_id = {0: 2,
                                             1: 3,
                                             2: 4,
                                             3: 5,
                                             4: 6}[min([int(fase_estudo) for fase_estudo in i['FASE'].lower().strip().split('-')])]


        if i[u'TEMPORALIDADE'].strip().lower() == 'retrospectivo':
            novo_trial.reference_period = 'RT'
            novo_trial.followup_period = None
        elif i[u'TEMPORALIDADE'].strip().lower() == 'prospectivo':
            novo_trial.reference_period = 'PR'
            novo_trial.followup_period = None
        elif i[u'TEMPORALIDADE'].strip().lower() == 'transversal':
            novo_trial.followup_period = 'TR'
            novo_trial.reference_period = None
        elif i[u'TEMPORALIDADE'].strip().lower() == 'retrospectivo e prospectivo':
            novo_trial.reference_period = 'RT'
            novo_trial.followup_period = None
        else:
            novo_trial.followup_period = None
            novo_trial.reference_period = None




        novo_trial.study_final_date = datetime.now().date() + timedelta(days=365*5)


        novo_trial.code = RBRS_RELATION.get(novo_trial.pk, None)




        ctdetail_pt = novo_trial.ctdetail_set.create(
            language_code=Setup.objects.get().language_1_code.description)
        novo_trial.language_1_mandatory = True
        TEM_IDIOMA_EN = i[u'TITULO_CIENTIFICO_EN'].strip() and 'o definido' not in i[u'TITULO_CIENTIFICO_EN'].strip().lower()
        if TEM_IDIOMA_EN:
            ctdetail_en = novo_trial.ctdetail_set.create(
                language_code=Setup.objects.get().language_2_code.description)
            novo_trial.language_2_mandatory = True
        TEM_IDIOMA_ES = i[u'TITULO_CIENTIFICO_ES'].strip() and 'o definido' not in i[u'TITULO_CIENTIFICO_ES'].strip().lower()
        if TEM_IDIOMA_ES:
            ctdetail_es = novo_trial.ctdetail_set.create(
                language_code=Setup.objects.get().language_3_code.description)
            novo_trial.language_3_mandatory = True


        ctdetail_pt.study_description = '''Este é um campo aberto onde o pesquisador deve FALAR DE FORMA BREVE sobre a pesquisa como um todo, seria um resumo. CASO PERTINENTE, inclua nesse breve resumo informações quanto a follow-up, períodos de wash-out, alterações no tratamento que podem ocorrem com a evolução do sujeito, ao decorrer do projeto'''
        if TEM_IDIOMA_EN:
            ctdetail_en.study_description = ctdetail_pt.study_description

        ctdetail_pt.inclusion_criteria = i['CRITERIOS_DE_INCLUSAO_PT']

        if TEM_IDIOMA_EN:
            ctdetail_en.inclusion_criteria = i['CRITERIOS_DE_INCLUSAO_EN']
        if TEM_IDIOMA_ES:
            ctdetail_es.inclusion_criteria = i['CRITERIOS_DE_INCLUSAO_ES']

        ctdetail_pt.exclusion_criteria = TRIALS_EXCLUSION_CRITERIAS.get(novo_trial.pk, {}).get('pt-br', '').replace('\n', '').replace('\r', '')
        if TEM_IDIOMA_EN:
            ctdetail_en.exclusion_criteria = TRIALS_EXCLUSION_CRITERIAS.get(novo_trial.pk, {}).get('en', '').replace('\n', '').replace('\r', '')
        if TEM_IDIOMA_ES:
            ctdetail_es.exclusion_criteria = TRIALS_EXCLUSION_CRITERIAS.get(novo_trial.pk, {}).get('es', '').replace('\n', '').replace('\r', '')

        ctdetail_pt.public_title = i[u'TITULO_PUBLICO_PT']
        if TEM_IDIOMA_EN:
            ctdetail_en.public_title = i[u'TITULO_PUBLICO_EN']
        if TEM_IDIOMA_ES:
            ctdetail_es.public_title = i[u'TITULO_PUBLICO_ES']

        ctdetail_pt.scientific_title = i[u'TITULO_CIENTIFICO_PT'] 
        if TEM_IDIOMA_EN:
            ctdetail_en.scientific_title = i[u'TITULO_CIENTIFICO_EN'] 
        if TEM_IDIOMA_ES:
            ctdetail_es.scientific_title = i[u'TITULO_CIENTIFICO_ES'] 

        ctdetail_pt.acronym = i[u'ACRONIMO_CIENTIFICO_PT']
        if TEM_IDIOMA_EN:
            ctdetail_en.acronym = i[u'ACRONIMO_CIENTIFICO_EN']
        if TEM_IDIOMA_ES:
            ctdetail_es.acronym = i[u'ACRONIMO_CIENTIFICO_ES']

        ctdetail_pt.health_condition = i[u'CONDICOES_DE_SAUDE_PT']
        if TEM_IDIOMA_EN:
            ctdetail_en.health_condition = i[u'CONDICOES_DE_SAUDE_EN']
        if TEM_IDIOMA_ES:
            ctdetail_es.health_condition = i[u'CONDICOES_DE_SAUDE_ES']


        ctdetail_pt.save()
        if TEM_IDIOMA_EN:
            ctdetail_en.save()
        if TEM_IDIOMA_ES:
            ctdetail_es.save()

        novo_trial.save()

        for ctact in json.loads(open(BASE_DIR+'/trial_id_X_contact_id_in_scientific_contact.json').read()):
            if int(novo_trial.pk or 0) == int(ctact[u'trial_id']):
                novo_trial.ctcontact_set.create(contact_type='S', contact_id=ctact[u'contact_id'])

        for ctact in json.loads(open(BASE_DIR+'/trial_id_X_contact_id_in_site_contact.json').read()):
            if int(novo_trial.pk or 0) == int(ctact[u'trial_id']):
                novo_trial.ctcontact_set.create(contact_type='W', contact_id=ctact[u'contact_id'])

        for ctact in json.loads(open(BASE_DIR+'/trial_id_X_contact_id_in_public_contact.json').read()):
            if int(novo_trial.pk or 0) == int(ctact[u'trial_id']):
                novo_trial.ctcontact_set.create(contact_type='P', contact_id=ctact[u'contact_id'])


        for attachment in ATTACHMENT_RELATIONS.get(int(novo_trial.pk), []):
            novo_trial.ctattachment_set.create(
                public=int(attachment[u'public']) == 1,
                description=attachment[u'description'],
                link=attachment[u'attach_url'] or None,
                file=attachment[u'file'] or None,
                attachment_id=3 if attachment[u'file'] else 4
            )


        #Block to deal with workflow
        status = i[u'STATUS']
        #import ipdb;ipdb.set_trace()
        from workflows.models import Transition
        from workflows.utils import do_transition
        last_reviser_name = 'admin_elias'
        reviser_user = None
        try:
            try:
                reviser_username = [revuser for revuser in REVISERS_USERS if int(revuser[u'TRIAL_ID']) == int(novo_trial.pk)][0][u'REVISOR']
            except IndexError:
                reviser_username = None

            reviser_username = {'adm_elias': 'admin_elias',
                                'adm_roberta': 'adm_roberta'}.get(reviser_username, None)
            if reviser_username is None:
                reviser_username = last_reviser_name
                last_reviser_name = 'admin_elias' if last_reviser_name == 'adm_roberta' else 'adm_roberta'
            reviser_user = RebecUser.objects.get(username=reviser_username)
        except RebecUser.DoesNotExist:
            print 'Entrando no IPDB por problema de revisor!!'
            import ipdb;ipdb.set_trace()
            #print 'aaa'
        if not reviser_user:
            print 'Entrando no IPDB por problema de revisor 222!!'
            import ipdb;ipdb.set_trace()


        if status == 'pending':
            Institution.objects.filter(sponsor_items__clinical_trial__pk=novo_trial.pk).update(status=u'A')
            do_transition(novo_trial,
                Transition.objects.get(pk=Transition.TRANSITION_AGUARDAR_REVISAO),
                novo_trial.holder, observation=u"Automatic workflow transition "
                                      "generated by the migration to ReBec2.0.")

            #if reviser_user is None:
            #    print u'nao tem revisor, mas esta pendente de revisao -> %s' %novo_trial.pk
            #    raise Exception('nao tem revisor, mas está pendente de revisao -> %s' %novo_trial.pk)
            novo_trial.holder = reviser_user
            if 'reviser' not in novo_trial.holder.groups.all().values_list('name', flat=True):
                novo_trial.holder.groups.clear()
                novo_trial.holder.groups.add(REVISER_GROUP)

            do_transition(novo_trial,
                Transition.objects.get(pk=Transition.TRANSITION_INICIAR_REVISAO),
                novo_trial.holder, observation=u"Automatic workflow transition "
                                      "generated by the migration to ReBec2.0.")


        if status == 'resubmit':
            Institution.objects.filter(sponsor_items__clinical_trial__pk=novo_trial.pk).update(status=u'A')

            do_transition(novo_trial,
                Transition.objects.get(pk=Transition.TRANSITION_AGUARDAR_REVISAO),
                novo_trial.holder, observation=u"Automatic workflow transition "
                                      "generated by the migration to ReBec2.0.")

            do_transition(novo_trial,
                Transition.objects.get(pk=Transition.TRANSITION_INICIAR_REVISAO),
                reviser_user, observation=u"Automatic workflow transition "
                                      "generated by the migration to ReBec2.0.")

            do_transition(novo_trial,
                Transition.objects.get(pk=Transition.TRANSITION_RESUBMETER),
                reviser_user, observation=u"Automatic workflow transition "
                                      "generated by the migration to ReBec2.0.")

        if status == 'published':
            Institution.objects.filter(sponsor_items__clinical_trial__pk=novo_trial.pk).update(status=u'A')

            do_transition(novo_trial,
                Transition.objects.get(pk=Transition.TRANSITION_AGUARDAR_REVISAO),
                novo_trial.holder, observation=u"Automatic workflow transition "
                                      "generated by the migration to ReBec2.0.")

            do_transition(novo_trial,
                Transition.objects.get(pk=Transition.TRANSITION_INICIAR_REVISAO),
                reviser_user, observation=u"Automatic workflow transition "
                                      "generated by the migration to ReBec2.0.")
            do_transition(novo_trial,
                Transition.objects.get(pk=Transition.TRANSITION_CONCLUIR_REVISAO),
                reviser_user, observation=u"Automatic workflow transition "
                                      "generated by the migration to ReBec2.0.")


            if not novo_trial.code:
                novo_trial.code = generate_trial_id(u'RBR', 5)
            novo_trial.save()
            # save the clinical trial data on the historical database
            # overriding possible trial data that may exist there
            #from IPython import embed;embed()
            novo_trial.save_historical_data()

            #save the trial snapshot after publishing
            request_fake = RequestFactory().get('/tanto-faz-a-url/')
            class S(object):
                def get(self, *args, **kwargs):
                    return False
                def set(self, *args, **kwargs):
                    return False
            class U(object):
                pk=-9
                def is_authenticated(self):
                    return True
                def is_staff(self):
                    return False
                def is_superuser(self):
                    return False


            request_fake.user = RebecUser.objects.first()
            request_fake.session = S()
            request_fake.LANGUAGE_CODE = u'pt'
            public_detail_content = PublicClinicalTrialDetailView.as_view()(
            request_fake, rbr=novo_trial.code).rendered_content
            content = html.fromstring(public_detail_content).cssselect(
                u'#main-content')[0]
            last_history = StateObjectHistory.objects.filter(
                           content_id=novo_trial.pk).latest(u'update_date')
            last_history.trial_snapshot = etree.tostring(content)
            last_history.save()


        if novo_trial.code:
            novo_trial.date_last_publication = REGISTRATION_DATES.get(novo_trial.id, novo_trial.date_creation)

        #from IPython import embed;embed()
        novo_trial.save()#saving one last time to make sure the data is saved indeed!



import_trials()

def change_sequences():
    from django.db import connection
    cursor = connection.cursor()
    cursor.execute("select sequence_name from information_schema.sequences where sequence_name like '%clinical%' or sequence_name like '%administration%' or sequence_name like '%login%';");
    sequences = cursor.fetchall()

    for sequence in sequences:
        cursor.execute('alter sequence %s restart with 100000;' % sequence[0])
change_sequences()

print '\n\n\nFinalizado:\n\n'
print 'Usuarios nao encontrados: %s'
for i in usuarios_nao_encontrados:
    print i
print '\n\nPatrocinadores inexistentes: %s\n' % len(patrocinadores_inexistentes)
open('patrocinadores_inexistentes.json', 'w').write(json.dumps(patrocinadores_inexistentes))
for i in patrocinadores_inexistentes:
    print 'trial: %s, patrocinador: %s' % (i[0], i[1])

print '\n\nDatas de recrutamento inválidas: %s\n\n' % datas_recruamento_invalidas

