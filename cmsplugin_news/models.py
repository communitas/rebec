try:
    from django.utils.timezone import now as datetime_now
    datetime_now  # workaround for pyflakes
except ImportError:
    from datetime import datetime
    datetime_now = datetime.now

from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse

from cms.models import CMSPlugin

from djangocms_text_ckeditor.fields import HTMLField

from . import settings

import random


class PublishedNewsManager(models.Manager):
    """
        Filters out all unpublished and items with a publication date in the future
    """
    def get_query_set(self):
        return super(PublishedNewsManager, self).get_query_set() \
                    .filter(is_published=True) \
                    .filter(pub_date__lte=datetime_now())


class Tag(models.Model):
    """
    Model for Tag.
    """
    name = models.CharField(
        verbose_name=_('Tag'),
        max_length=50)

    class Meta:
        verbose_name = _('Tag')
        verbose_name_plural = _('Tags')

    def __unicode__(self):
        return unicode(self.name)


class News(models.Model):
    """
    Model for News
    """

    title = models.CharField(
        verbose_name=_('Title'),
        max_length=255)
    slug = models.SlugField(
        verbose_name=_('Slug'),
        unique_for_date='pub_date',
        help_text=_('A slug is a short name which uniquely identifies the news'
                    ' item for this day'))
    excerpt = models.TextField(
        verbose_name=_('Description'),
        blank=True)
    content = HTMLField(
        verbose_name=_('Content'),
        blank=True)
    image = models.ImageField(
        verbose_name=_("Image"),
        upload_to=u'image',
        null=True,
        blank=True)
    is_published = models.BooleanField(
        verbose_name=_('Published'),
        default=False)
    pub_date = models.DateTimeField(
        verbose_name=_('Publication date'),
        default=datetime_now)
    created = models.DateTimeField(
        auto_now_add=True,
        editable=False)
    updated = models.DateTimeField(
        auto_now=True,
        editable=False)
    tags = models.ManyToManyField(Tag,
        verbose_name=_('Tags'),
        null=True,
        blank=True)

    published = PublishedNewsManager()
    objects = models.Manager()

    link = models.URLField(
        verbose_name=_('Link'),
        blank=True,
        null=True,
        help_text=_('This link will be used a absolute url'
            ' for this item and replaces the view logic. <br />Note that by'
            ' default this only applies for items with an empty "content"'
            ' field.'),
        editable=False)

    class Meta:
        verbose_name = _('News')
        verbose_name_plural = _('News')
        ordering = ('-pub_date', )

    def __unicode__(self):
        return self.title

    def get_absolute_url(self):
        if settings.LINK_AS_ABSOLUTE_URL and self.link:
            if settings.USE_LINK_ON_EMPTY_CONTENT_ONLY and not self.content:
                return self.link
        return reverse('news_detail',
                       kwargs={'year': self.pub_date.strftime("%Y"),
                               'month': self.pub_date.strftime("%m"),
                               'day': self.pub_date.strftime("%d"),
                               'slug': self.slug})

    def get_tags(self):
        return self.tags.all()

    def get_aleatory_tag(self):
        result = self.tags.all()
        if result:
            return random.choice(result)
        return ""


class LatestNewsPlugin(CMSPlugin):
    """
        Model for the settings when using the latest news cms plugin
    """
    limit = models.PositiveIntegerField(
        verbose_name=_('Number of news items to show'),
        help_text=_('Limits the number of items that will be displayed'))
