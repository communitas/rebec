��          �   %   �      0  C   1  G   u     �  K   �          %     -     5  1   A     s     x     }  	   �  	   �     �     �     �     �  	   �     �  �   �     �     �  ^  �  F     I   d     �  :   �     �        	          /   &     V     \     d     u     }     �     �     �     �     �     �     �     y     �                                                                	                   
                                            %(count)d newsitem was published %(count)d newsitems were published %(count)d newsitem was unpublished %(count)d newsitems were unpublished A feed full of news A slug is a short name which uniquely identifies the news item for this day Archive Content Excerpt Latest news Limits the number of items that will be displayed Link News News App News feed News menu No news yet Number of news items to show Publication date Publish selected news Published Slug This link will be used a absolute url for this item and replaces the view logic. <br />Note that by default this only applies for items with an empty "content" field. Title Unpublish selected news Project-Id-Version: 0.4.1
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-05-06 21:03+0200
PO-Revision-Date: 2014-05-06 21:14+0200
Last-Translator: Jakub Veverka <veverka.kuba@gmail.com>
Language-Team: Czech
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1)
 %(count)d zpráva byla publikována %(count)d zpráv bylo publikováno %(count)d zpráva bylo odpublikováno (count)d zpráv bylo odpublikováno Zásoba novinek Slug je zkratka, která identifikuje novinku pro tento den Archiv Obsah Výňatek Nejnovější novinky Limituje počet novinek, které budou zobrazeny Odkaz Novinky Aplikace Novinky Novinky Menu novinky Žádné novinky Počet novinek k zobrazení Datum zveřejnění Publikovat vybrané zprávy Zveřejněno Slug Tento odkaz bude použit jako absolutní url a nahradí aplikační view. <br />Toto se hodí pro novinky s prázdným obsahem. Název Odpublikovat vybrané zprávy 