#-*- coding: utf-8 -*-

from django.shortcuts import render
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic import ListView, DetailView, View, TemplateView
from administration.models import (
    Institution, InstitutionType, Country, Setup, SetupIdentifier,
    CTGroup, CTGroupUser, StudyPurpose,
    InterventionAssignment, MaskingType, AllocationType, StudyPhase,
    ObservationStudyDesign, TimePerspective, InterventionCode,
    Attachment, StateDaysLimit, StateChangeEmailTemplate)
from django.core.urlresolvers import reverse_lazy, reverse
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import Group
from django.contrib.contenttypes.models import ContentType
from django.db.models.deletion import ProtectedError
from django.http import HttpResponse
from clinical_trials.models import ClinicalTrial, Sponsor, Contact
from clinical_trials.forms import ContactForm
from django.http import HttpResponse, HttpResponseRedirect
from extra_views import (
    CreateWithInlinesView, UpdateWithInlinesView, InlineFormSet, NamedFormsetsMixin,
    ModelFormSetView)
from extra_views.generic import GenericInlineFormSet
from django.utils.translation import ugettext as _,  ugettext_lazy as __
from generics.mixins import *
from django.contrib import messages
from django.contrib.auth import get_user_model
from administration.forms import (
    AdministrationGroupManagementForm,
    AdministrationGroupManagementSearchForm,
    InstitutionModerationChangeForm,
    InstitutionForm, InstitutionCreationForm,
    UserOccupationManagementSearchForm,
    ClinicalTrialsofRegistrantsOfGroupSearchForm,
    CTHolderForm, CTOwnerForm, GroupsListSearchForm,
    CTAllListSearchForm, DasboardSearchForm, DasboardSearchTwoForm,
    InstitutionListSearchForm, SetupIdentifierForm,
    InstitutionListSearchForm, CTIndicatorDateSearchForm, CTIndicatorPublishedFilterForm)
from django.forms.models import modelformset_factory
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.mail import send_mail
from django.conf import settings
from django.shortcuts import get_object_or_404, render_to_response
from administration.forms import CTGroupForm, CTGroupUserForm
from administration.response import RebecResponse
from workflows.models import (StateObjectRelation, State, Transition,
                              StateObjectGeneralObservation)
from workflows.utils import do_transition
from django.core.exceptions import PermissionDenied
from django.contrib.messages.views import SuccessMessageMixin
from administration.forms import AttachmentTypeFormSet, StateDaysLimitForm, \
    RevisorAndDateSearchForm, EvaluatorAndDateSearchForm, AttachmentTypeForm
from django.db.models import Q, Count
from administration.models import StateDaysLimit
from workflows.models import StateObjectHistory, State
from clinical_trials.helpers import get_next_by_occupation
from login.forms import UserChangeForm
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
import json
from import_export_config.models import ExportConfig
from datetime import datetime, timedelta

User = get_user_model()

class ClinicalTrialsByEvaluatorAndStateView(ListView):
    model = ClinicalTrial
    template_name = \
        u'administration/administration/clinical_trials_by_evaluator_and_state.html'

    def get_queryset(self):

        queryset = super(ClinicalTrialsByEvaluatorAndStateView, self).get_queryset()

        queryset = queryset.filter(holder__groups__name=u'evaluator')

        search_form = EvaluatorAndDateSearchForm(self.request.GET or None)
        if search_form.is_valid():
            reviser = search_form.cleaned_data.get(u'evaluator', None)
            if reviser:
                queryset = queryset.filter(holder__pk=reviser.pk)

            date_from = search_form.cleaned_data.get(u'date_from', None)
            if date_from:
                queryset = queryset.filter(date_last_state_change__gte=date_from)

            date_to = search_form.cleaned_data.get(u'date_to', None)
            if date_from:
                queryset = queryset.filter(date_last_state_change__lte=date_to)

        return queryset

    def get_context_data(self, **kwargs):
        context = super(ClinicalTrialsByEvaluatorAndStateView, self).get_context_data(
            **kwargs
        )

        context[u'search_form'] = EvaluatorAndDateSearchForm(
            self.request.GET or None)
        context[u'get_parameters'] = self.request.get_full_path().split('/')[-1]

        return context


class ClinicalTrialsByRevisorAndStateView(ListView):
    model = ClinicalTrial
    template_name = \
        u'administration/administration/clinical_trials_by_revisor_and_state.html'

    def get_queryset(self):

        queryset = super(ClinicalTrialsByRevisorAndStateView, self).get_queryset()

        queryset = queryset.filter(holder__groups__name=u'reviser')

        search_form = RevisorAndDateSearchForm(self.request.GET or None)
        if search_form.is_valid():
            reviser = search_form.cleaned_data.get(u'revisor', None)
            if reviser:
                queryset = queryset.filter(holder__pk=reviser.pk)

            date_from = search_form.cleaned_data.get(u'date_from', None)
            if date_from:
                queryset = queryset.filter(date_last_state_change__gte=date_from)

            date_to = search_form.cleaned_data.get(u'date_to', None)
            if date_from:
                queryset = queryset.filter(date_last_state_change__lte=date_to)

        return queryset

    def get_context_data(self, **kwargs):
        context = super(ClinicalTrialsByRevisorAndStateView, self).get_context_data(
            **kwargs
        )

        context[u'search_form'] = RevisorAndDateSearchForm(
            self.request.GET or None)
        context[u'get_parameters'] = self.request.get_full_path().split('/')[-1]

        return context

    def get_success_url(self):
        """
        Set a success message and return the url to redirect user to.
        """
        messages.success(self.request, _(u'Users occupation management success.'))
        return self.request.get_full_path()


class UsersOccupationManageView(PermissionRequiredMixin, ModelFormSetView):
    """
    Management of occupation of users for administrators
    """
    model = User
    #form_class = ClinicalTrialManagementForm
    fields = [u'ocuppation',]
    template_name = \
                   u'administration/administration/users_occupation_management.html'
    extra = 0
    permissions_required = ['admin.manager_permission',]

    def get_pagination(self, queryset):
        paginator = Paginator(queryset, 20)

        return paginator

    def get_page(self):
        return self.request.GET.get(u'page', 1)

    def get_page_object_list(self, queryset, page):
        page = self.get_page()
        paginator = self.get_pagination(queryset)
        try:
            self.page = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            self.page = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            self.page = paginator.page(paginator.num_pages)


    def get_queryset(self):

        queryset = super(UsersOccupationManageView, self).get_queryset()
        queryset = queryset.filter(groups__name__in=[u'reviser', u'evaluator'])
        user = self.request.GET.get(u'user', None)
        user_type = self.request.GET.get(u'user_type', None)

        if user:
            queryset = queryset.filter(pk=user)

        if user_type in (u'E', u'R'):
            if user_type == u'E':
                queryset = queryset.filter(groups__name=u'evaluator')
            else:
                queryset = queryset.filter(groups__name=u'reviser')


        not_registrant_status_trials = StateObjectRelation.objects.exclude(
        state__in=[State.STATE_EM_PREENCHIMENTO,
                   State.STATE_PUBLICADO,
                   State.STATE_RESUBMETIDO,
                   State.STATE_REVISANDO_PREENCHIMENTO]
        ).values_list(u'content_id', flat=True)
        #annotate the holding clinical_trials_count
        queryset = queryset.order_by(u'-pk')
        self.get_page_object_list(queryset, self.get_page())
        queryset = queryset.filter(pk__in=[i.pk for i in self.page.object_list])
        for i in queryset:
            i.number_cts = \
                i.holding_clinical_trials.filter(
                    pk__in=not_registrant_status_trials).count()

        return queryset

    def get_context_data(self, **kwargs):
        context = super(UsersOccupationManageView, self).get_context_data(
            **kwargs
        )
        context[u'current_page'] = self.page
        context[u'search_form'] = UserOccupationManagementSearchForm(
            self.request.GET or None)
        context[u'get_parameters'] = self.request.get_full_path().split('/')[-1]
        context[u'next_prevision_list_revisor'] = \
            get_next_by_occupation(u'reviser')
        context[u'next_prevision_list_evaluator'] = \
            get_next_by_occupation(u'evaluator')

        return context

    def get_success_url(self):
        """
        Set a success message and return the url to redirect user to.
        """
        messages.success(self.request, _(u'Users occupation management success.'))
        return self.request.get_full_path()


class UsersOccupationManageListCTsView(PermissionRequiredMixin, ModelFormSetView):
    model = ClinicalTrial
    template_name = u'administration/administration/list-user-trials.html'
    fields = [u'holder',]
    form_class = CTHolderForm
    extra = 0
    permissions_required = ['admin.manager_permission']

    def get_queryset(self):
        not_registrant_status_trials = StateObjectRelation.objects.exclude(
        state__in=[State.STATE_EM_PREENCHIMENTO,
                   State.STATE_PUBLICADO,
                   State.STATE_RESUBMETIDO,
                   State.STATE_REVISANDO_PREENCHIMENTO]
        ).values_list(u'content_id', flat=True)
        queryset = super(UsersOccupationManageListCTsView, self).get_queryset()
        queryset = queryset.filter(holder__pk=self.kwargs[u'pk'])
        queryset = queryset.filter(pk__in=not_registrant_status_trials)
        return queryset

    def get_success_url(self):
        """
        Set a success message and return the url to redirect user to.
        """
        messages.success(self.request, _(u'Users of Clinical Trials Saved.'))
        return reverse(u'occupation_management')

    def get_context_data(self, *args, **kwargs):
        context = super(
                UsersOccupationManageListCTsView,
                self
            ).get_context_data(*args, **kwargs)
        context[u'this_user'] = User.objects.get(pk=self.kwargs[u'pk'])

        return context

class CountryList(PermissionRequiredMixin, ListView):
    """
    Country list view
    permissions = ('administrator',).
    """
    model = Country
    template_name = 'administration/administration/country_list.html'
    permissions_required = ['admin.administrador_permission']

    def get_queryset(self):
        """Get all Contry objects or query by description."""
        result = Country.objects.all()
        query = self.request.GET
        q = self.request.GET.get('q')
        if q:
            result = result.filter(description__icontains=q)
        return result


class CountryDetail(PermissionRequiredMixin, DetailView):
    """
    Country detail view
    permissions = ('administrator',).
    """
    model = Country
    template_name = "base/detail_view.html"
    permissions_required = ['admin.administrador_permission']


class CountryCreate(SuccessMessageMixin, PermissionRequiredMixin, CreateView):
    """
    Country create view
    permissions = ('administrator',).
    """
    model = Country
    success_url = reverse_lazy('country_list')
    template_name = 'base/form_view.html'
    permissions_required = ['admin.administrador_permission']
    success_message = __(u'Successfully Saved!')


class CountryUpdate(SuccessMessageMixin, PermissionRequiredMixin, UpdateView):
    """Country update view
    permissions = ('administrator',).
    """
    model = Country
    success_url = reverse_lazy('country_list')
    template_name = 'base/form_view.html'
    permissions_required = ['admin.administrador_permission']
    success_message = _(u'Successfully Saved!')



class CountryDelete(SuccessMessageMixin, PermissionRequiredMixin, DeleteView):
    """
    Country delete view
    permissions = ('administrator',).
    """
    model = Country
    success_url = reverse_lazy('country_list')
    template_name = 'base/delete_view.html'
    permissions_required = ['admin.administrador_permission']
    success_message = __(u'Successfully Deleted!')

    def delete(self, request, *args, **kwargs):
        """Return a message when object is protect to delete on cascade."""
        self.object = self.get_object()
        try:
            return super(CountryDelete, self).delete(request, *args, **kwargs)
        except ProtectedError:
            return HttpResponse(_("Can not delete this entry"))


class InstitutionTypeList(PermissionRequiredMixin, ListView):
    """
    InstitutionType list view
    permissions = ('administrator',).
    """
    model = InstitutionType
    template_name = 'administration/administration/institution_type_list.html'
    permissions_required = ['admin.administrador_permission']

    def get_queryset(self):
        """Get all InstitutionType objects or query by description."""
        result = InstitutionType.objects.all()
        query = self.request.GET
        q = self.request.GET.get('q')
        if q:
            result = result.filter(description__icontains=q)
        return result


class InstitutionTypeDetail(PermissionRequiredMixin, DetailView):
    """
    InstitutionType detail view
    permissions = ('administrator',).
    """
    model = InstitutionType
    template_name = "base/detail_view.html"
    permissions_required = ['admin.administrador_permission']


class InstitutionTypeCreate(SuccessMessageMixin, PermissionRequiredMixin, CreateView):
    """
    InstitutionType create view
    permissions = ('administrator',).
    """
    model = InstitutionType
    success_url = reverse_lazy('institution_type_list')
    template_name = 'base/form_view.html'
    permissions_required = ['admin.administrador_permission']
    success_message = __(u'Successfully Saved!')


class InstitutionTypeUpdate(SuccessMessageMixin, PermissionRequiredMixin, UpdateView):
    """
    InstitutionType update view
    permissions = ('administrator',).
    """
    model = InstitutionType
    success_url = reverse_lazy('institution_type_list')
    template_name = 'base/form_view.html'
    permissions_required = ['admin.administrador_permission']
    success_message = __(u'Successfully Saved!')



class InstitutionTypeDelete(SuccessMessageMixin, PermissionRequiredMixin, DeleteView):
    """
    InstitutionType delete view
    permissions = ('administrator',).
    """
    model = InstitutionType
    success_url = reverse_lazy('institution_type_list')
    template_name = 'base/delete_view.html'
    permissions_required = ['admin.administrador_permission']
    success_message = _(u'Successfully Deleted!')


    def delete(self, request, *args, **kwargs):
        """Return a message when object is protect to delete on cascade."""
        self.object = self.get_object()
        try:
            return super(
                InstitutionTypeDelete, self).delete(request, *args, **kwargs)
        except ProtectedError:
            return HttpResponse(_("Can not delete this entry"))


class InstitutionList(PermissionRequiredMixin, ListView):
    """
    Institution list view
    permissions = ('administrator',).
    """
    model = Institution
    template_name = 'administration/administration/institution_list.html'
    permissions_required = ['admin.administrador_permission']
    paginate_by = 20
    form_class = InstitutionListSearchForm

    def get_context_data(self, **kwargs):
        context = super(InstitutionList, self).get_context_data(
            **kwargs)
        context[u'search_form'] = InstitutionListSearchForm(
            self.request.GET)

        return context

    def get_queryset(self):
        """
        Get all Institution objects or query by name or status.
        The default institution query for this view must exclude
        the institutions in moderation status and the institutions
        of sponsors in clinical trials in EM_PREENCHIMENTO
        workflow status.
        """
        queryset = super(InstitutionList, self).get_queryset()
        result = queryset.exclude(
            sponsor_items__clinical_trial__pk__in=\
                StateObjectRelation.objects.filter(
                    state=State.STATE_EM_PREENCHIMENTO
                ).values_list(u'content_id', flat=True)
        ).exclude(
            status=Institution.STATUS_MODERATION
        )

        name = self.request.GET.get('name')
        code = self.request.GET.get('code')

        if name:
            result = result.filter(name__icontains=name)
        if code:
            result = result.filter(id=code)
        return result


class InstitutionDetail(PermissionRequiredMixin, DetailView):
    """
    Institution detail view
    permissions = ('administrator',).
    """
    model = Institution
    template_name = "base/detail_view.html"
    permissions_required = ['admin.administrador_permission']


class InstitutionCreate(SuccessMessageMixin, PermissionRequiredMixin, CreateView):
    """
    Institution create view
    permissions = ('administrator',).
    """
    model = Institution
    form_class = InstitutionCreationForm
    template_name = 'administration/administration/institution_form.html'
    permissions_required = ['admin.administrador_permission']
    success_message = __(u'Successfully Saved!')

    def get_success_url(self, *args, **kwargs):
        """Set Institution status to 'Active' (A)."""
        if self.request.user.has_perm('admin.administrador_permission'):
            self.object.status = "A"
            self.object.save()
        return reverse_lazy('institution_list')


class InstitutionCreateSponsor(CreateView):
    """
    Register Institution and add as a Clinical Trial Sponsor
    """
    model = Institution
    form_class = InstitutionCreationForm
    template_name = 'administration/administration/institution_form.html'

    def get_context_data(self, *args, **kwargs):
        """
        Return a message to confirm to add Institution as Clinical Trial
        Sponsor and return Clinical Trial pk to view.
        """
        context = super(
            InstitutionCreateSponsor, self).get_context_data(*args, **kwargs)
        context[u'message'] = _(
            "Add to institution as Clinical Trial sponsor?")
        context[u'cancel_url'] = '/'
        # TODO return to clinical trial edit view
        # context[u'cancel_url'] = reverse_lazy(u'ctdetails_edit', args=(self.kwargs['clinical_trial_pk'],))
        return context

    def get_success_url(self, *args, **kwargs):
        """
        Save Institution as a Clinical Trial Sponsor
        and redirect to Clinical Trial edit view.
        If use has administration permissions set Institution
        status to 'Active' (A).
        """
        self.object.status = "M"
        if self.request.user.has_perm('admin.administrador_permission'):
            self.object.status = "A"
            self.object.save()
        sponsor_type = self.kwargs['sponsor_type']
        clinical_trials_pk = self.kwargs['clinical_trial_pk']
        if sponsor_type in tuple(item[0] for item in Sponsor.TYPE_CHOICES):
            result = ClinicalTrial.objects.filter(pk=clinical_trials_pk)
            if result.exists():
                new = Sponsor(
                    clinical_trial=result.first(),
                    institution=self.object, sponsor_type=sponsor_type)
                new.save()
        messages.success(self.request, __(u'Success created new institution.'))
        return reverse(u'clinical_trial_edit_sponsors', args=(clinical_trials_pk,))


class InstitutionCreateForGroup(CreateView):
    """
    Register Institution and add in a group
    """
    model = Institution
    form_class = InstitutionCreationForm
    template_name = 'administration/administration/institution_form.html'

    def get_context_data(self, *args, **kwargs):
        """
        Return a message to confirm to add Institution as Clinical Trial
        Sponsor and return Clinical Trial pk to view.
        """
        context = super(
            InstitutionCreateForGroup, self).get_context_data(*args, **kwargs)
        context[u'message'] = _(
            "Add to institution to the group?")
        context[u'cancel_url'] = '/'
        # TODO return to clinical trial edit view
        # context[u'cancel_url'] = reverse_lazy(u'ctdetails_edit', args=(self.kwargs['clinical_trial_pk'],))
        return context

    def get_success_url(self, *args, **kwargs):
        """
        Save Institution to the group
        and redirect to group edit view.
        """
        group_pk = self.kwargs['group_pk']
        CTGroup.objects.filter(pk=group_pk).update(institution=self.object)
        CTGroup.objects.filter(pk=group_pk).update(
            status_institution=CTGroup.STATUS_INSTITUTION_PENDING
        )
        self.object.status = "M"
        if self.request.user.has_perm('admin.administrador_permission'):
            self.object.status = "A"
            self.object.save()
            CTGroup.objects.filter(pk=group_pk).update(
                status_institution=CTGroup.STATUS_INSTITUTION_APPROVED
            )

        messages.success(self.request,
                        _(u'Success created new institution for this group.')
                        )
        return reverse(u'ctgroup_edit', args=(group_pk,))

class InstitutionCreateForNewGroup(CreateView):
    """
    Register Institution and add in a group
    """
    model = Institution
    form_class = InstitutionCreationForm
    template_name = 'administration/administration/institution_form.html'

    def get_context_data(self, *args, **kwargs):
        """
        Return a message to confirm to add Institution as Clinical Trial
        Sponsor and return Clinical Trial pk to view.
        """
        context = super(
            InstitutionCreateForNewGroup, self).get_context_data(*args, **kwargs)
        context[u'message'] = _(
            "Add to institution to the group?")
        context[u'cancel_url'] = '/'
        # TODO return to clinical trial edit view
        # context[u'cancel_url'] = reverse_lazy(u'ctdetails_edit', args=(self.kwargs['clinical_trial_pk'],))
        return context

    def get_success_url(self, *args, **kwargs):
        """
        Save Institution to the group
        and redirect to group edit view.
        """
        self.object.status = "M"
        if self.request.user.has_perm('admin.administrador_permission'):
            self.object.status = "A"
            self.object.save()

        messages.success(self.request,
                        _(u'Success created new institution.')
                        )
        return reverse(u'ctgroup_add') + \
            '?restore_data=yes&inst_id=%s' % self.object.pk

class InstitutionUpdate(SuccessMessageMixin, PermissionRequiredMixin, UpdateView):
    """
    Institution update view
    permissions = ('administrator',).
    """
    model = Institution
    form_class = InstitutionForm
    success_url = reverse_lazy('institution_list')
    template_name = 'administration/administration/institution_form.html'
    permissions_required = ['admin.administrador_permission']
    success_message = _(u'Successfully Saved!')


class SetupIdentifierList(PermissionRequiredMixin, ListView):
    """
    List Setup Identifier
    permissions = ('administrator',).
    """
    model = SetupIdentifier
    template_name = 'administration/administration/setup_identifier_list.html'
    permissions_required = ['admin.administrador_permission']

    def get_queryset(self):
        """Get all Setup Identifier objects or query by issuing_body."""
        result = SetupIdentifier.objects.all()
        query = self.request.GET
        q = self.request.GET.get('q')
        if q:
            result = result.filter(issuing_body__icontains=q)
        return result


class SetupIdentifierDetail(PermissionRequiredMixin, DetailView):
    """
    Detail Setup Identifier
    permissions = ('administrator',).
    """
    model = SetupIdentifier
    template_name = "base/detail_view.html"
    permissions_required = ['admin.administrador_permission']


class SetupIdentifierCreate(SuccessMessageMixin, PermissionRequiredMixin, CreateView):
    """
    Register setup identifier
    permissions = ('administrator',).
    """
    model = SetupIdentifier
    form_class = SetupIdentifierForm
    success_url = reverse_lazy('setup_identifier_list')
    template_name = 'administration/administration/setup_identifier.html'
    permissions_required = ['admin.administrador_permission']
    success_message = __(u'Successfully Saved!')


class SetupIdentifierUpdate(SuccessMessageMixin, PermissionRequiredMixin, UpdateView):
    """
    Edit setup identifier
    permissions = ('administrator',).
    """
    model = SetupIdentifier
    form_class = SetupIdentifierForm
    success_url = reverse_lazy('setup_identifier_list')
    template_name = 'administration/administration/setup_identifier.html'
    permissions_required = ['admin.administrador_permission']
    success_message = __(u'Successfully Saved!')


class SetupIdentifierDelete(SuccessMessageMixin, PermissionRequiredMixin, DeleteView):
    """
    Delete Setup identifier
    permissions = ('administrator',).
    """
    model = SetupIdentifier
    success_url = reverse_lazy('setup_identifier_list')
    template_name = 'base/delete_view.html'
    permissions_required = ['admin.administrador_permission']
    success_message = __(u'Successfully Deleted!')


class AttachmentInline(InlineFormSet):
    """attachments Inline."""
    model = Attachment
    extra = 0
    formset_class = AttachmentTypeFormSet
    form_class=AttachmentTypeForm


class StateDaysLimitInline(InlineFormSet):
    """state days limit Inline."""
    model = StateDaysLimit
    extra = 0
    form_class = StateDaysLimitForm


class SetupUpdate(NamedFormsetsMixin, PermissionRequiredMixin, UpdateWithInlinesView):
    """
    View edition setup
    permissions = ('administrator',).
    """
    model = Setup
    template_name = 'administration/administration/setup_update.html'
    success_url = reverse_lazy('setup')
    inlines = [AttachmentInline, StateDaysLimitInline]
    permissions_required = ['admin.administrador_permission']
    inlines_names = [u'formset', u'formset_days']
    response_class = RebecResponse


    def get_object(self):
        """Return Setup object (there is always only one)."""
        return Setup.objects.get(pk=1)

    def get_success_url(self, *args, **kwargs):
        messages.success(self.request, _(u'Setup Successfully Saved!'))
        return super(SetupUpdate, self).get_success_url(*args, **kwargs)


class CTGroupUserInline(InlineFormSet):
    """Inline Group User"""
    model = CTGroupUser
    form_class = CTGroupUserForm
    extra = 0



class CreateCTGroup(NamedFormsetsMixin, CreateWithInlinesView):
    """Create new Group"""
    template_name = 'administration/administration/ctgroup.html'
    model = CTGroup
    inlines = [CTGroupUserInline]
    success_url = reverse_lazy('ctgroup_list')
    form_class = CTGroupForm
    inlines_names = [u'formset']
    response_class = RebecResponse


    def get_context_data(self, *args, **kwargs):
        context = super(CreateCTGroup, self).get_context_data(*args, **kwargs)
        context[u'restore_data'] = self.request.GET.get(u'restore_data', u'no')
        inst_id = self.request.GET.get(u'inst_id', None)
        form = context[u'form']
        form.initial[u'institution'] = inst_id
        context[u'form'] = form
        return context

    def get_success_url(self, *args, **kwargs):
        """
        When selecting an institution status is pending
        if not already is active.
        """
        institution = self.object.institution

        if not institution:
            self.object.status=CTGroup.STATUS_ACTIVE
            users = self.object.users.all().count()
            if users > 0:
                self.object.users.all().update(status=CTGroupUser.STATUS_ACTIVE)
            self.object.save()
        else:
            self.object.status_institution=CTGroup.STATUS_INSTITUTION_PENDING
            self.object.status=CTGroup.STATUS_ACTIVE
            users = self.object.users.all().count()
            self.object.ctgrouplog_set.create(user=self.request.user,
                    message=_('Institution %s is pending approve.' \
                                % institution.name))
            if users > 0:
                self.object.users.all().update(status=CTGroupUser.STATUS_ACTIVE)
            self.object.save()
            messages.success(self.request,
                             _(u"You created a group with an institution, you "
                               u"will need to wait for the group's moderation"))

        """
        If the User to add the group he turns admim if he does
        not turn Adds administrator automatically.
        """
        user = self.request.user
        user_logged = self.object.users.all().filter(user=user).exists()
        if not user_logged :
            self.object.users.get_or_create(
                status=2, type_user='a', user=self.request.user)
        else:
            self.object.users.all().filter(user=user).update(type_user='a', status=2)
        messages.success(self.request, _(u'Group Successfully Saved!'))
        return super(CreateCTGroup, self).get_success_url()


class MyGroupsList(ListView):
    """
    View list only the groups that
    are logged in the user.
    """
    model = CTGroup
    template_name = 'administration/administration/ctgroup_list.html'
    response_class = RebecResponse

    def get_queryset(self):
        """
        Returns all groups that are logged user.
        """
        return CTGroupUser.objects.filter(user=self.request.user.pk)


class CTGroupList(ListView):
    """
    View lists all active groups are not logged user.
    """
    model = CTGroup
    template_name = 'administration/administration/list_ctgroup.html'
    form_class = GroupsListSearchForm
    extra = 0

    def get_context_data(self, **kwargs):
        context = super(CTGroupList, self).get_context_data(
            **kwargs)
        context[u'search_form'] = GroupsListSearchForm(
            self.request.GET)

        return context

    def get_queryset(self):

        queryset = super(CTGroupList, self).get_queryset()

        result = CTGroup.objects.filter(
            status=1
        ).exclude(users__user=self.request.user)


        group = self.request.GET.get(u'group', None)
        institution = self.request.GET.get(u'institution', None)

        if group:
            queryset = queryset.filter(
                id=group, status=1).exclude(users__user=self.request.user)

        if institution:
            queryset = queryset.filter(institution=institution, id__in=result)

        if group or institution:
            return queryset
        else:
            return queryset.none()


class UpdateCTGroup(NamedFormsetsMixin, UpdateWithInlinesView):
    """
    Edit only the administrator group which is.
    permissions = 'registrant'
    """
    template_name = 'administration/administration/ctgroup.html'
    model = CTGroup
    inlines = [CTGroupUserInline]
    success_url = reverse_lazy('ctgroup_list')
    form_class = CTGroupForm
    inlines_names = [u'formset']
    response_class = RebecResponse


    def get_context_data(self, *args, **kwargs):
        context = super(UpdateCTGroup, self).get_context_data(*args, **kwargs)
        context[u'ctgroup_logs'] = \
            self.object.ctgrouplog_set.all().order_by(u'date')
        context[u'ctgroup_trials'] = \
            self.object.clinicaltrial_set.all().order_by('pk')
        context[u'form_add_trial_in_group'] = \
            ClinicalTrialsofRegistrantsOfGroupSearchForm()
        return context

    def add_trial_to_group(self, group_id, trial_id):
        trial = ClinicalTrial.objects.get(pk=trial_id)
        trial.group_id = group_id
        trial.save()
        messages.success(self.request, _(u'Trial added to group.'))
        return HttpResponseRedirect(reverse_lazy('ctgroup_edit', args=(group_id,)))

    def post(self,*args, **kwargs):
        if self.request.POST.get(u'trial', None):
            return self.add_trial_to_group(kwargs.get(u'pk'),
                                           self.request.POST.get(u'trial'))

        self.institution_changed = \
            unicode(CTGroup.objects.get(
                pk=self.kwargs[u'pk']
            ).institution_id) != self.request.POST[u'institution']
        return super(UpdateCTGroup, self).post(*args, **kwargs)


    def get_success_url(self, *args, **kwargs):
        """
        When selecting an institution status is pending
        if not already is active.
        If you edit a group that institution has and add a user as an
        administrator status is pending approval, if the institution
        does not have the status is already active.
        """
        ctgroup = self.object
        institution = self.object.institution
        user_adm = self.object.users.filter(type_user='a').filter(status=None)
        user_regis = self.object.users.filter(type_user='r').filter(status=None)
        user_regis_2 = self.object.users.filter(type_user='r').filter(status=CTGroupUser.STATUS_PENDING_APROV_ADM)

        if ctgroup.status_institution == CTGroup.STATUS_INSTITUTION_PENDING:
            if self.request.POST[u'institution'] == '':
                ctgroup.status_institution=None
                ctgroup.save()

        elif ctgroup.status_institution == None:
            if self.request.POST[u'institution'] != '':
                ctgroup.status_institution=CTGroup.STATUS_INSTITUTION_PENDING
                ctgroup.save()
                self.object.ctgrouplog_set.create(user=self.request.user,
                        message=_('Institution %s is pending approve.' \
                                    % ctgroup.institution.name))
            elif self.request.POST[u'institution'] == '':
                ctgroup.status_institution=None
                ctgroup.save()
        elif ctgroup.status_institution == CTGroup.STATUS_INSTITUTION_APPROVED:
            if self.request.POST[u'institution'] == '':
                ctgroup.status_institution=None
                ctgroup.save()
            if user_adm.exists():
                user_adm.update(status=CTGroupUser.STATUS_PENDING_APROV_ADM)

        if institution and ctgroup.status_institution == CTGroup.STATUS_INSTITUTION_PENDING:
            if user_adm.exists():
                user_adm.update(status=CTGroupUser.STATUS_ACTIVE)

        if not institution:
            if user_adm.exists():
                user_adm.update(status=CTGroupUser.STATUS_ACTIVE)

        if user_regis:
            user_regis.update(status=CTGroupUser.STATUS_ACTIVE)

        if user_regis_2:
            user_regis_2.update(status=CTGroupUser.STATUS_ACTIVE)

        if self.institution_changed and ctgroup.institution_id:
            messages.success(self.request,
                _(u'You changed the institution of the group, so it will need to be approved.'))
            ctgroup.status_institution=CTGroup.STATUS_INSTITUTION_PENDING
            ctgroup.save()
            self.object.ctgrouplog_set.create(user=self.request.user,
                        message=_('Institution was changed to %s and is pending approve.' \
                                    % ctgroup.institution.name))

        messages.success(self.request, _(u'Group Successfully Saved!'))
        return reverse_lazy('ctgroup_list')


class RemoveCTFromGroup(TemplateView):
    template_name = u'administration/administration/remove-ct-from-group.html'

    def get_context_data(self, ct_id, *args, **kwargs):
        context = super(RemoveCTFromGroup, self).get_context_data(*args,
                                                                  **kwargs)
        context[u'clinical_trial'] = get_object_or_404(ClinicalTrial, pk=ct_id)
        return context

    def post(self, request, ct_id, *args, **kwargs):
        clinical_trial = get_object_or_404(ClinicalTrial, pk=ct_id)
        group_pk = clinical_trial.group.pk
        clinical_trial.group = None
        clinical_trial.save()
        messages.success(request, _(u'Clinical trial removed from group'))
        return HttpResponseRedirect(reverse(u'ctgroup_edit', args=(group_pk,)))


class ChangeCTOwner(UpdateView):
    template_name = u'administration/administration/change-ct-holder.html'
    model = ClinicalTrial
    fields = (u'user',)
    form_class = CTOwnerForm

    def get_success_url(self):
        messages.success(self.request, _(u'Clinical trial owner changed.'))
        return reverse(u'ctgroup_edit', args=(self.object.group.pk,))


class ManagerCTGroupBlock(View):
    """
    CTGroup moderation view.
    Administrator user locks or unlocks.
    """
    model = CTGroup
    template_name = "administration/administration/ctrgroup_participate_confirm.html"
    response_class = RebecResponse

    def set_context_csrf(self, request, context):
        from django.core.context_processors import csrf
        context.update(csrf(request))

    def get_object(self, pk):
        return get_object_or_404(CTGroupUser, pk=pk)

    def get(self, request, pk, decision):
        """Shows message conrfim accept or delete."""
        ctgroup = self.get_object(pk)
        context = {}
        if decision == u'B':
            #check if has other administrators on group
            if not self.get_object(pk).ct_group.users.filter(
                type_user=CTGroupUser.TYPE_ADMINISTRATOR,
                status=CTGroupUser.STATUS_ACTIVE
                ).exclude(pk=pk).exists():
                messages.error(
                    request,
                    u'This group has only this administrator active.'
                )
                if request.is_ajax():
                    return render_to_response(
                        u'base/clean_modal.html',
                        {u'messages': messages.get_messages(request)}
                    )
                return HttpResponseRedirect(
                    reverse(
                        u'ctgroup_edit',
                        args=(self.get_object(pk).ct_group.pk,)
                    )
                )

            message = _(u'Want to block user?')
        else:
            message = _(u'Want to unlock user?')
        context[u'message'] = message
        context[u'request'] = request
        context[u'cancel_url'] = \
            reverse(u'ctgroup_edit', args=(self.get_object(pk).ct_group.pk,))
        self.set_context_csrf(request, context)
        return render_to_response(self.template_name, context)

    def post(self, request, pk, decision):
        ctgroup = self.get_object(pk)
        if decision == u'B':
            if request.POST.get(u'confirm', None) == u'yes':
                """
                If the administrator wants to block user:
                then the user status is inactive.
                """
                user= self.request.user.pk
                user_group = ctgroup.user.pk
                if user != user_group:
                    ctgroup.status = 3
                    ctgroup.save()
                    messages.success(self.request, _(u"The user group was inativated."))
                else:
                    """
                    If the user tries to block: Information message.
                    """
                    messages.error(self.request, _(u"you can not be inativated"))
        if decision == u'D':
            if request.POST.get(u'confirm', None) == u'yes':
                    if ctgroup.type_user == CTGroupUser.TYPE_ADMINISTRATOR:
                        messages.success(self.request, _(u"User unlocked in this group."))
                        ctgroup.status = CTGroupUser.STATUS_ACTIVE
                    else:
                        ctgroup.status = 2
                        messages.success(self.request, _(u"The User belongs to group."))
                    ctgroup.save()

        return HttpResponseRedirect(
            reverse(u'ctgroup_edit', args=(self.get_object(pk).ct_group.pk,))
            )


class CTGroupDetail(DetailView):
    """
    Shows all the details of the group.
    """
    model = CTGroup
    template_name = "administration/administration/ctgroup_detail.html"
    inlines = [CTGroupUserInline]
    response_class = RebecResponse

    def get_context_data(self, *args, **kwargs):
        """
        Added inline items of CTGroupUser.
        """
        context_data = super(
            CTGroupDetail, self).get_context_data(*args, **kwargs)
        context_data['ctgroup_formset'] =  self.object.users.all()
        context_data[u'ctgroup_logs'] = \
            self.object.ctgrouplog_set.all().order_by(u'date')
        context_data[u'ctgroup_trials'] = \
            self.object.clinicaltrial_set.all().order_by('pk')
        return context_data


class ManagerCTGroup(DetailView):
    template_name = 'administration/administration/manager_ctgroup.html'
    model = CTGroup
    inlines = [CTGroupUserInline]
    success_url = reverse_lazy('ctgroup_list')
    response_class = RebecResponse

    def get_context_data(self, *args, **kwargs):
        """Return related ctgroup objects."""
        context_data = super(
            ManagerCTGroup, self).get_context_data(*args, **kwargs)
        context_data['ctgroup_formset'] = self.object.users.all()
        return context_data


class ManagerCTGroupDecision(View):
    """
    CTGroup moderation view.
    Administrator is managing output and input of users in the group.
    """
    model = CTGroup
    template_name = "administration/administration/ctrgroup_participate_confirm.html"
    response_class = RebecResponse

    def set_context_csrf(self, request, context):
        from django.core.context_processors import csrf
        context.update(csrf(request))

    def get_object(self, pk):
        return get_object_or_404(CTGroupUser, pk=pk)

    def get(self, request, pk, decision):
        """Shows message conrfim accept or delete."""
        ctgroup = self.get_object(pk)
        context = {}
        if decision == u'C':
            message = _(u'Want to confirm participation in the group?')
        else:
            message = _(u'Confirm the deletion of the participant in the group?')
        context[u'message'] = message
        context[u'request'] = request
        context[u'cancel_url'] = reverse(u'ctgroup_manager', args=(ctgroup.pk,))
        self.set_context_csrf(request, context)
        return render_to_response(self.template_name, context)

    def post(self, request, pk, decision):
        user_pk = pk
        if decision == u'C':
            """
            If the user is accepted or user is prevented from leaving the group,
            then participating in user group:
            the state is active, displays success message, send an e-mail.
            """
            if request.POST.get(u'confirm', None) == u'yes':
                group_user = CTGroupUser.objects.get(pk=user_pk)
                group_user.status = 2
                group_user.save()
                messages.success(request, _(u'Success user participates in the group!'))
                send_mail(
                    'Groups Clinical Trial',
                    u'Você foi aceito no grupo!',
                    'desenvrebec@gmail.com',
                    [group_user.user.email])
        else:
            """
            If the user is rejected or you want to leave the group:
            status = 4  idles, displays success message,
            send e-mail.
            """
            if request.POST.get(u'confirm', None) == u'yes':
                group_user = CTGroupUser.objects.get(pk=user_pk)
                group_user.status = 3
                group_user.save()
                messages.success(request, _(u'User does not participate in the Group!'))
                send_mail(
                    'Groups Clinical Trial',
                    'Você não pertence ao grupo',
                    'desenvrebec@gmail.com',
                    [group_user.user.email])
        return HttpResponseRedirect(reverse(u'home'))


class CTGroupParticipate(View):
    """
    View controls user participation.
    """
    model = CTGroup
    template_name = "administration/administration/ctrgroup_participate_confirm.html"
    response_class = RebecResponse

    def set_context_csrf(self, request, context):
        from django.core.context_processors import csrf
        context.update(csrf(request))

    def get_object(self, pk):
        return get_object_or_404(CTGroup, pk=pk)

    def get(self, request, pk, decision):
        """Shows part of message and exit."""
        ctgroup = self.get_object(pk)
        context = {}
        if decision == u'P':
            message = _(u'Want to join the group?')
        context[u'message'] = message
        context[u'request'] = request
        context[u'cancel_url'] = reverse(u'ctgroup_list')
        self.set_context_csrf(request, context)
        return render_to_response(self.template_name, context)

    def post(self, request, pk, decision):
        """
        If you wish to attend:
        Is pending approval from the group administrator.
        """
        ctgroup = self.get_object(pk)
        user=self.request.user
        if decision == u'P':
            if request.POST.get(u'confirm', None) == u'yes':
                CTGroupUser.objects.get_or_create(
                status=1,
                type_user='r',
                user=self.request.user,
                ct_group=ctgroup)
                messages.success(request, _(u'You have requested to join the group.'))
        return HttpResponseRedirect(reverse(u'ctgroup_list'))


class CTGroupSignOut(View):
    """
    View controls output of users.
    """
    model = CTGroup
    template_name = "administration/administration/ctrgroup_participate_confirm.html"
    response_class = RebecResponse

    def set_context_csrf(self, request, context):
        from django.core.context_processors import csrf
        context.update(csrf(request))

    def get_object(self, pk):
        return get_object_or_404(CTGroupUser, pk=pk).ct_group

    def get(self, request, pk, decision):
        """Shows part of message and exit."""
        ctgroup = self.get_object(pk)
        context = {}
        if decision == u'S':
            message = _(u'Want to leave the group?')
        context[u'message'] = message
        context[u'request'] = request
        context[u'cancel_url'] = reverse(u'ctgroup_list')
        self.set_context_csrf(request, context)
        return render_to_response(self.template_name, context)

    def post(self, request, pk, decision):
        """
        If you want to leave User: checks whether administrator or registrant.
        """
        ctgroup = self.get_object(pk)
        user=self.request.user
        if decision == u'S':
            if request.POST.get(u'confirm', None) == u'yes':
                group = CTGroupUser.objects.filter(ct_group=ctgroup).filter(status=2)
                if group.filter(user=user).filter(type_user='a'):
                    if group.filter(type_user='a').count() == 1 :
                        """
                        If User is administrator and not only has
                        an administrator and then can not get out of the group.
                        """
                        messages.error(
                            self.request,
                            _(u"You can not leave this group because it is the only administrator"))
                    else:
                        """
                        If User is administrator and other administrators
                        must then email
                        is sent to the portal administrator is pending.
                        """
                        send_mail(
                            'Groups Clinical Trial',
                            'O user deseja sair do grupo!',
                            'desenvrebec@gmail.com',
                            ['desenvrebec@gmail.com'])
                        messages.success(self.request, _(u"Email sent to administrator protal!"))
                        group.filter(user=self.request.user).update(status=4)
                elif group.filter(user=user).filter(type_user='r'):
                    """
                    If User is Registrant: checks for clinical Trails attached to it.
                    """
                    ct_group = group.filter(user=user).filter(type_user='r')
                    if  ClinicalTrial.objects.filter(user=user).filter(group=ct_group).count() == 0:
                        """
                        If User is not the registrant and have tests linked him to
                        the group and then your is pending approval from the group
                        administrator.
                        """
                        ct_group.update(status=4)
                        messages.success(
                            self.request,
                            _(u"Your request has been sent to the group administrator!"))
                    else:
                        """
                        If the user has clinical trials linked to the group
                        can not get out, shows message only.
                        """
                        messages.error(
                            self.request,
                            _(u"You have clinical trials can not leave the group!"))
        return HttpResponseRedirect(reverse(u'ctgroup_list'))


class UserProfileEdit(UpdateView):
    """Edit user profile."""
    template_name = 'administration/administration/user_profile_edit.html'
    model = User
    fields = ['username', 'name', 'email', 'gender','race', 'country', 'passport', 'cpf']
    response_class = RebecResponse
    form_class = UserChangeForm

    def get_success_url(self, *args, **kwargs):
        """
        Returns On success message for saving data.
        """
        messages.success(self.request, _('Save success!'))
        return reverse('user_profile_edit', args=(self.object.pk,))


class UserProfileDetail(DetailView):
    """Detail of user profile"""
    model = User
    template_name = 'administration/administration/user_profile_detail.html'


class StudyPurposeList(PermissionRequiredMixin, ListView):
    """
    Study Purpose list view
    """
    model = StudyPurpose
    template_name = 'administration/administration/study_purpose_list.html'
    permissions_required = ['admin.administrador_permission']


class StudyPurposeDetail(PermissionRequiredMixin, DetailView):
    """
    Study Purpose detail view
    """
    model = StudyPurpose
    template_name = "base/detail_view.html"
    permissions_required = ['admin.administrador_permission']


class StudyPurposeCreate(SuccessMessageMixin, PermissionRequiredMixin, CreateView):
    """
    Study Purpose create view
    """
    model = StudyPurpose
    success_url = reverse_lazy('study_purpose_list')
    template_name = 'base/form_view.html'
    permissions_required = ['admin.administrador_permission']
    success_message = __(u'Successfully Saved!')



class StudyPurposeUpdate(SuccessMessageMixin, PermissionRequiredMixin, UpdateView):
    """
    Study Purpose update view
    """
    model = StudyPurpose
    success_url = reverse_lazy('study_purpose_list')
    template_name = 'base/form_view.html'
    permissions_required = ['admin.administrador_permission']
    success_message = __(u'Successfully Saved!')



class StudyPurposeDelete(SuccessMessageMixin, PermissionRequiredMixin, DeleteView):
    """
    Study Purpose delete view
    """
    model = StudyPurpose
    success_url = reverse_lazy('study_purpose_list')
    template_name = 'base/delete_view.html'
    permissions_required = ['admin.administrador_permission']
    success_message = __(u'Successfully Deleted!')



class InterventionAssignmentList(PermissionRequiredMixin, ListView):
    """
    Intervention Assignment list view
    """
    model = InterventionAssignment
    template_name = 'administration/administration/intervention_assignment_list.html'
    permissions_required = ['admin.administrador_permission']


class InterventionAssignmentDetail(PermissionRequiredMixin, DetailView):
    """
    Intervention Assignment detail view
    """
    model = InterventionAssignment
    template_name = "base/detail_view.html"
    permissions_required = ['admin.administrador_permission']


class InterventionAssignmentCreate(SuccessMessageMixin, PermissionRequiredMixin, CreateView):
    """
    Intervention Assignment create view
    """
    model = InterventionAssignment
    success_url = reverse_lazy('intervention_assignment_list')
    template_name = 'base/form_view.html'
    permissions_required = ['admin.administrador_permission']
    success_message = __(u'Successfully Saved!')


class InterventionAssignmentUpdate(SuccessMessageMixin, PermissionRequiredMixin, UpdateView):
    """
    Intervention Assignment update view
    """
    model = InterventionAssignment
    success_url = reverse_lazy('intervention_assignment_list')
    template_name = 'base/form_view.html'
    permissions_required = ['admin.administrador_permission']
    success_message = __(u'Successfully Saved!')


class InterventionAssignmentDelete(SuccessMessageMixin, PermissionRequiredMixin, DeleteView):
    """
    Intervention Assignment delete view
    """
    model = InterventionAssignment
    success_url = reverse_lazy('intervention_assignment_list')
    template_name = 'base/delete_view.html'
    permissions_required = ['admin.administrador_permission']
    success_message = __(u'Successfully Deleted!')


class MaskingTypeList(PermissionRequiredMixin, ListView):
    """
    Masking Type list view
    """
    model = MaskingType
    template_name = 'administration/administration/masking_type_list.html'
    permissions_required = ['admin.administrador_permission']


class MaskingTypeDetail(PermissionRequiredMixin, DetailView):
    """
    Masking Type detail view
    """
    model = MaskingType
    template_name = "base/detail_view.html"
    permissions_required = ['admin.administrador_permission']


class MaskingTypeCreate(SuccessMessageMixin, PermissionRequiredMixin, CreateView):
    """
    Masking Type create view
    """
    model = MaskingType
    success_url = reverse_lazy('masking_type_list')
    template_name = 'base/form_view.html'
    permissions_required = ['admin.administrador_permission']
    success_message = __(u'Successfully Saved!')


class MaskingTypeUpdate(SuccessMessageMixin, PermissionRequiredMixin, UpdateView):
    """
    Masking Type update view
    """
    model = MaskingType
    success_url = reverse_lazy('masking_type_list')
    template_name = 'base/form_view.html'
    permissions_required = ['admin.administrador_permission']
    success_message = __(u'Successfully Saved!')


class MaskingTypeDelete(SuccessMessageMixin, PermissionRequiredMixin, DeleteView):
    """
    Masking Type delete view
    """
    model = MaskingType
    success_url = reverse_lazy('masking_type_list')
    template_name = 'base/delete_view.html'
    permissions_required = ['admin.administrador_permission']
    success_message = __(u'Successfully Deleted!')


class AllocationTypeList(PermissionRequiredMixin, ListView):
    """
    Allocation Type list view
    """
    model = AllocationType
    template_name = 'administration/administration/allocation_type_list.html'
    permissions_required = ['admin.administrador_permission']


class AllocationTypeDetail(PermissionRequiredMixin, DetailView):
    """
    Allocation Type detail view
    """
    model = AllocationType
    template_name = "base/detail_view.html"
    permissions_required = ['admin.administrador_permission']


class AllocationTypeCreate(SuccessMessageMixin, PermissionRequiredMixin, CreateView):
    """
    Allocation Type create view
    """
    model = AllocationType
    success_url = reverse_lazy('allocation_type_list')
    template_name = 'base/form_view.html'
    permissions_required = ['admin.administrador_permission']
    success_message = __(u'Successfully Saved!')


class AllocationTypeUpdate(SuccessMessageMixin, PermissionRequiredMixin, UpdateView):
    """
    Allocation Type update view
    """
    model = AllocationType
    success_url = reverse_lazy('allocation_type_list')
    template_name = 'base/form_view.html'
    permissions_required = ['admin.administrador_permission']
    success_message = __(u'Successfully Saved!')


class AllocationTypeDelete(SuccessMessageMixin, PermissionRequiredMixin, DeleteView):
    """
    Allocation Type delete view
    """
    model = AllocationType
    success_url = reverse_lazy('allocation_type_list')
    template_name = 'base/delete_view.html'
    permissions_required = ['admin.administrador_permission']
    success_message = __(u'Successfully Deleted!')


class StudyPhaseList(PermissionRequiredMixin, ListView):
    """
    Study Phase list view
    """
    model = StudyPhase
    template_name = 'administration/administration/study_phase_list.html'
    permissions_required = ['admin.administrador_permission']


class StudyPhaseDetail(PermissionRequiredMixin, DetailView):
    """
    Study Phase detail view
    """
    model = StudyPhase
    template_name = "base/detail_view.html"
    permissions_required = ['admin.administrador_permission']


class StudyPhaseCreate(SuccessMessageMixin, PermissionRequiredMixin, CreateView):
    """
    Study Phase create view
    """
    model = StudyPhase
    success_url = reverse_lazy('study_phase_list')
    template_name = 'base/form_view.html'
    permissions_required = ['admin.administrador_permission']
    success_message = __(u'Successfully Saved!')


class StudyPhaseUpdate(SuccessMessageMixin, PermissionRequiredMixin, UpdateView):
    """
    Study Phase update view
    """
    model = StudyPhase
    success_url = reverse_lazy('study_phase_list')
    template_name = 'base/form_view.html'
    permissions_required = ['admin.administrador_permission']
    success_message = __(u'Successfully Saved!')


class StudyPhaseDelete(SuccessMessageMixin, PermissionRequiredMixin, DeleteView):
    """
    Study Phase delete view.
    """
    model = StudyPhase
    success_url = reverse_lazy('study_phase_list')
    template_name = 'base/delete_view.html'
    permissions_required = ['admin.administrador_permission']
    success_message = __(u'Successfully Deleted!')


class ObservationStudyDesignList(PermissionRequiredMixin, ListView):
    """
    Observation Study Design list view.
    """
    model = ObservationStudyDesign
    template_name = 'administration/administration/observation_study_design_list.html'
    permissions_required = ['admin.administrador_permission']


class ObservationStudyDesignDetail(PermissionRequiredMixin, DetailView):
    """
    Observation Study Design detail view.
    """
    model = ObservationStudyDesign
    template_name = "base/detail_view.html"
    permissions_required = ['admin.administrador_permission']


class ObservationStudyDesignCreate(SuccessMessageMixin, PermissionRequiredMixin, CreateView):
    """
    Observation Study Design create view.
    """
    model = ObservationStudyDesign
    success_url = reverse_lazy('observation_study_design_list')
    template_name = 'base/form_view.html'
    permissions_required = ['admin.administrador_permission']
    success_message = __(u'Successfully Saved!')


class ObservationStudyDesignUpdate(SuccessMessageMixin, PermissionRequiredMixin, UpdateView):
    """
    Observation Study Design update view.
    """
    model = ObservationStudyDesign
    success_url = reverse_lazy('observation_study_design_list')
    template_name = 'base/form_view.html'
    permissions_required = ['admin.administrador_permission']
    success_message = __(u'Successfully Saved!')


class ObservationStudyDesignDelete(SuccessMessageMixin, PermissionRequiredMixin, DeleteView):
    """
    Observation Study Design delete view.
    """
    model = ObservationStudyDesign
    success_url = reverse_lazy('observation_study_design_list')
    template_name = 'base/delete_view.html'
    permissions_required = ['admin.administrador_permission']
    success_message = __(u'Successfully Deleted!')


class TimePerspectiveList(PermissionRequiredMixin, ListView):
    """
    Time Perspective list view.
    """
    model = TimePerspective
    template_name = 'administration/administration/time_perspective_list.html'
    permissions_required = ['admin.administrador_permission']


class TimePerspectiveDetail(PermissionRequiredMixin, DetailView):
    """
    Time Perspective detail view.
    """
    model = TimePerspective
    template_name = "base/detail_view.html"
    permissions_required = ['admin.administrador_permission']


class TimePerspectiveCreate(SuccessMessageMixin, PermissionRequiredMixin, CreateView):
    """
    Time Perspective create view.
    """
    model = TimePerspective
    success_url = reverse_lazy('time_perspective_list')
    template_name = 'base/form_view.html'
    permissions_required = ['admin.administrador_permission']
    success_message = __(u'Successfully Saved!')


class TimePerspectiveUpdate(SuccessMessageMixin, PermissionRequiredMixin, UpdateView):
    """
    Time Perspective update view.
    """
    model = TimePerspective
    success_url = reverse_lazy('time_perspective_list')
    template_name = 'base/form_view.html'
    permissions_required = ['admin.administrador_permission']
    success_message = __(u'Successfully Saved!')


class TimePerspectiveDelete(SuccessMessageMixin, PermissionRequiredMixin, DeleteView):
    """
    Time Perspective delete view.
    """
    model = TimePerspective
    success_url = reverse_lazy('time_perspective_list')
    template_name = 'base/delete_view.html'
    permissions_required = ['admin.administrador_permission']
    success_message = __(u'Successfully Deleted!')


class InterventionCodeList(PermissionRequiredMixin, ListView):
    """
    Intervention Code list view.
    """
    model = InterventionCode
    template_name = 'administration/administration/intervention_code_list.html'
    permissions_required = ['admin.administrador_permission']


class InterventionCodeDetail(PermissionRequiredMixin, DetailView):
    """
    Intervention Code detail view.
    """
    model = InterventionCode
    template_name = "base/detail_view.html"
    permissions_required = ['admin.administrador_permission']


class InterventionCodeCreate(SuccessMessageMixin, PermissionRequiredMixin, CreateView):
    """
    Intervention Code create view.
    """
    model = InterventionCode
    success_url = reverse_lazy('intervention_code_list')
    template_name = 'base/form_view.html'
    permissions_required = ['admin.administrador_permission']
    current_menu = u'intervention_code'
    success_message = __(u'Successfully Saved!')


class InterventionCodeUpdate(SuccessMessageMixin, PermissionRequiredMixin, UpdateView):
    """
    Intervention Code update view.
    """
    model = InterventionCode
    success_url = reverse_lazy('intervention_code_list')
    template_name = 'base/form_view.html'
    permissions_required = ['admin.administrador_permission']
    success_message = __(u'Successfully Saved!')


class InterventionCodeDelete(SuccessMessageMixin, PermissionRequiredMixin, DeleteView):
    """
    Intervention Code delete view.
    """
    model = InterventionCode
    success_url = reverse_lazy('intervention_code_list')
    template_name = 'base/delete_view.html'
    permissions_required = ['admin.administrador_permission']
    success_message = __(u'Successfully Deleted!')


class CTGroupsManageView(PermissionRequiredMixin, ModelFormSetView):
    """
    Management of groups for administrators
    Show a formset of clinical trial groups with pagination support, to change
    just the current page's ct groups.
    """
    model = CTGroup
    form_class = AdministrationGroupManagementForm
    template_name = \
                   u'administration/administration/admin_manage_group_form.html'
    extra = 0
    permissions_required = ['admin.administrador_permission', 'admin.manager_permission']

    def get_pagination(self, queryset):
        paginator = Paginator(queryset, 20)

        return paginator

    def get_page(self):
        return self.request.GET.get(u'page', 1)

    def get_page_object_list(self, queryset, page):
        page = self.get_page()
        paginator = self.get_pagination(queryset)
        try:
            self.page = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            self.page = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            self.page = paginator.page(paginator.num_pages)


    def get_queryset(self):

        queryset = super(CTGroupsManageView, self).get_queryset()

        institution = self.request.GET.get(u'institution', None)
        group = self.request.GET.get(u'group', None)

        if institution:
            queryset = queryset.filter(institution=institution)
        if group:
            queryset = queryset.filter(pk=group)

        queryset = queryset.order_by(u'-pk')
        self.get_page_object_list(queryset, self.get_page())
        queryset = queryset.filter(pk__in=[i.pk for i in self.page.object_list])

        return queryset

    def get_context_data(self, **kwargs):
        context = super(CTGroupsManageView, self).get_context_data(**kwargs)
        context[u'current_page'] = self.page
        context[u'search_form'] = AdministrationGroupManagementSearchForm(
            self.request.GET)
        context[u'get_parameters'] = self.request.get_full_path().split('/')[-1]

        return context

    def get_success_url(self):
        """
        Set a success message and return the url to redirect user to.
        """
        messages.success(self.request, _(u'Groups management success.'))
        return self.request.get_full_path()


class DashboardView(TemplateView):
    """
    The index dashboard view.
    """
    template_name = u'administration/administration/dashboard.html'

    def _get_manager_context(self):
        return {u'graph_1': self._get_graph_1(),
                u'graph_2': self._get_graph_2(),
                u'graph_3': self._get_graph_3(),
                u'graph_5': self._get_graph_5(),
                u'linegraph_1': self._get_linegraph_1()}


    def _get_linegraph_1(self):
        one_year_before = datetime.now() - timedelta(days=12*30)
        values = ClinicalTrial.objects.filter(
            date_creation__gte=one_year_before
        ).order_by(u'date_creation').values_list(u'date_creation', flat=True)


        months_count = {}
        for i in values:
            if not months_count.has_key((i.month, i.year)):
                months_count[(i.month, i.year)] = 0

            months_count[(i.month, i.year)] += 1
        months_count_correct = {}
        dates_between = [datetime(one_year_before.year, one_year_before.month, 1) + timedelta(days=i*31) for i in range(1,13)]
        for i in dates_between:
            if not months_count.has_key((i.month, i.year)):
                months_count_correct[(i.month, i.year)] = 0
            else:
                months_count_correct[(i.month, i.year)] = months_count[(i.month, i.year)]
        months_count = months_count_correct
        mc_ordered = sorted(months_count.items(), key=lambda x: datetime(x[0][1], x[0][0], 1))
        data1 = ''.join(['[gd(%s,%s,%s), %s],' % (k[1], k[0]-1, 1, v) for k, v in mc_ordered])
        data1 = '[%s]' % data1[:-1]

        values = ClinicalTrial.objects.filter(
            date_last_publication__gte=one_year_before,
        ).order_by(u'date_last_publication').values_list(u'date_last_publication', flat=True)


        months_count = {}
        for i in [val for val in values if val is not None]:
            if not months_count.has_key((i.month, i.year)):
                months_count[(i.month, i.year)] = 0

            months_count[(i.month, i.year)] += 1

        months_count_correct = {}
        dates_between = [datetime(one_year_before.year, one_year_before.month, 1) + timedelta(days=i*31) for i in range(1,13)]
        for i in dates_between:
            if not months_count.has_key((i.month, i.year)):
                months_count_correct[(i.month, i.year)] = 0
            else:
                months_count_correct[(i.month, i.year)] = months_count[(i.month, i.year)]
        months_count = months_count_correct
        mc_ordered = sorted(months_count.items(), key=lambda x: datetime(x[0][1], x[0][0], 1))
        data2 = ''.join(['[gd(%s,%s,%s), %s],' % (k[1], k[0]-1, 1, v) for k, v in mc_ordered])
        data2 = '[%s]' % data2[:-1]

        return {u'data1': data1, u'data2': data2}

    def _get_revisor_context(self, filter_color=None, filter_color_two=None):
        em_revisao = StateObjectRelation.objects.filter(
            state__pk=State.STATE_EM_REVISAO).values_list(u'content_id', flat=True)
        ct_em_revisao = ClinicalTrial.objects.filter(
            pk__in=em_revisao,
            holder=self.request.user
            )

        aguardando_revisao = StateObjectRelation.objects.filter(
            state__pk=State.STATE_AGUARDANDO_REVISAO).values_list(
                u'content_id', flat=True
            )
        ct_aguardando_revisao = ClinicalTrial.objects.filter(
            pk__in=aguardando_revisao,
            holder=self.request.user
            )

        if filter_color:
            ct_em_revisao = [i for i in ct_em_revisao if i.get_remaining_days()[1] in filter_color]
        if filter_color_two:
            ct_aguardando_revisao = [i for i in ct_aguardando_revisao if i.get_remaining_days()[1] in filter_color_two]


        return {u'graph_6': self._get_graph_6(),
                u'graph_7': self._get_graph_7(),
                u'revisor_clinical_trial_em_revisao': ct_em_revisao,
                u'revisor_ct_aguardando_revisao': ct_aguardando_revisao,
                u'search_form': DasboardSearchForm(
                    self.request.GET),
                 u'search_form_two':
                 DasboardSearchTwoForm(
                    self.request.GET),}

    def _get_administrador_context(self):
        group_institution_pending = CTGroup.objects.filter(
                    status_institution=CTGroup.STATUS_INSTITUTION_PENDING,
                    status=CTGroup.STATUS_ACTIVE,
                    institution__status=Institution.STATUS_ACTIVE
                    )
        group_administrator_pending = CTGroupUser.objects.filter(
            status=CTGroupUser.STATUS_PENDING_APROV_ADM,
            ct_group__status=CTGroup.STATUS_ACTIVE,
        ).filter(Q(ct_group__status_institution=CTGroup.STATUS_INSTITUTION_APPROVED)
                    |
                Q(ct_group__institution__isnull=True))

        moderation_status_cts = StateObjectRelation.objects.filter(
                                    state__pk__in=\
                                    (State.STATE_AGUARDANDO_MODERACAO,
                                     State.STATE_EM_MODERACAO_RESUBMETIDO)
                                ).values_list('content_id', flat=True)

        institution_pending1 = Institution.objects.filter(
                status=Institution.STATUS_MODERATION
            ).filter(
                Q(sponsor_items__clinical_trial__pk__in=moderation_status_cts)
            ).values_list(u'pk', flat=True)

        institution_pending2 = Institution.objects.filter(
            status=Institution.STATUS_MODERATION
            ).annotate(
                number_of_ctgroups=Count('ctgroups'),
            ).filter(
                number_of_ctgroups__gt=0
            ).values_list(u'pk', flat=True)

        institution_pending = set(tuple(institution_pending1) + tuple(institution_pending2))
        institution_pending = Institution.objects.filter(pk__in=institution_pending)

        return {u'group_institution_pending': group_institution_pending,
                u'group_administrator_pending': group_administrator_pending,
                u'institution_pending': institution_pending}

    def _get_evaluator_context(self, filter_color=None, filter_color_two=None):
        """
        If User is logged valiador shows clinical trials that are waiting to Evaluating
        and those who are being evaluated. Makes calculation to generate the graphs.
        """
        aguardando_avaliacao = StateObjectRelation.objects.filter(
            state__pk=State.STATE_AGUARDANDO_AVALIACAO).values_list(u'content_id', flat=True)

        em_avaliacao = StateObjectRelation.objects.filter(
            state__pk=State.STATE_EM_AVALIACAO).values_list(u'content_id', flat=True)

        ct_aguardando_avaliacao = ClinicalTrial.objects.filter(
                pk__in=aguardando_avaliacao).filter(holder=self.request.user)

        ct_em_avaliacao= ClinicalTrial.objects.filter(
                pk__in=em_avaliacao).filter(holder=self.request.user)

        if filter_color:
            ct_em_avaliacao = [i for i in ct_em_avaliacao if i.get_remaining_days()[1] in filter_color]
        if filter_color_two:
            ct_aguardando_avaliacao = [i for i in ct_aguardando_avaliacao if i.get_remaining_days()[1] in filter_color_two]

        return {u'graph_8': self._get_graph_8(),
                u'graph_9': self._get_graph_9(),
                u'clinical_trial_em_avaliacao': ct_em_avaliacao[:10],
                u'clinical_trial_pending_avaliacao': ct_aguardando_avaliacao[:10],
                u'search_form': DasboardSearchForm(
                    self.request.GET),
                 u'search_form_two':
                 DasboardSearchTwoForm(
                    self.request.GET),}

    def _get_graph_8(self):
        now = datetime.now()
        red = 0
        green = 0
        yellow = 0
        black = 0

        for i in StateDaysLimit.objects.filter(state=State.STATE_AGUARDANDO_AVALIACAO):
            for j in StateObjectHistory.objects.filter(
                content_id__in=StateObjectHistory.objects.filter(
                                state=i.state,
                                content_id__in=self.request.user.\
                                    holding_clinical_trials.all().values_list(
                                        u'pk', flat=True
                                    )
                                ).values_list(u'content_id', flat=True),
                state=i.state):

                days_in_state = (now - j.update_date.replace(tzinfo=None)).days
                if days_in_state <= i.green:
                    green += 1
                elif days_in_state < i.yellow and days_in_state > i.green:
                    yellow += 1
                elif days_in_state > i.yellow and days_in_state < i.red:
                    red += 1
                elif days_in_state > i.red:
                    black += 1


        if not green and not yellow and not red and not black:
            return {}



        dataset = [
         {
            u'label': _("In Time"),
            u'color': "#006400",
            u'data': green
         },
         {
            u'label': _("Late"),
            u'color': "#FF0000",
            u'data': red
         },
         {
            u'label': _("Need Attention"),
            u'color': "#FFFF00",
            u'data': yellow
         },
         {
            u'label': _("went the deadline"),
            u'color': "#000000",
            u'data': black
         }
         ]

        options = {
            u'series': {
                u'pie': {
                    u'show': True,
                    u'label': {
                        u'show': True
                    }
                }
            },
            u'legend': {
                u'show': True,
                u'labelBoxBorderColor': "#ffffff",
                u'position': "ne",
                u'color': "#000000",
            }
        }

        return {u'options': json.dumps(options), u'dataset': json.dumps(dataset)}

    def _get_graph_9(self):
        now = datetime.now()
        red = 0
        green = 0
        yellow = 0
        black  = 0

        for i in StateDaysLimit.objects.filter(
            state=State.STATE_EM_AVALIACAO
        ):
            for j in StateObjectHistory.objects.filter(
                content_id__in=StateObjectHistory.objects.filter(
                                state=i.state,
                                content_id__in=self.request.user.\
                                    holding_clinical_trials.all().values_list(
                                        u'pk', flat=True
                                    )
                                ).values_list(u'content_id', flat=True),
                state=i.state):

                days_in_state = (now - j.update_date.replace(tzinfo=None)).days
                if days_in_state <= i.green:
                    green += 1
                elif days_in_state <= i.yellow and days_in_state > i.green:
                    yellow += 1
                elif days_in_state > i.yellow and days_in_state <= i.red:
                    red += 1
                elif days_in_state > i.red:
                    black += 1

        if not green and not yellow and not red and not black:
            return {}

        dataset = [
         {
            u'label': _("In Time"),
            u'color': "#006400",
            u'data': green
         },
         {
            u'label': _("Late"),
            u'color': "#FF0000",
            u'data': red
         },
         {
            u'label': _("Need Attention"),
            u'color': "#FFFF00",
            u'data': yellow
         },
         {
            u'label': _("went the deadline"),
            u'color': "#000000",
            u'data': black
         }
         ]

        options = {
            u'series': {
                u'pie': {
                    u'show': True,
                    u'label': {
                        u'show': True
                    }
                }
            },
            u'legend': {
                u'show': True,
                u'labelBoxBorderColor': "#ffffff",
                u'position': "ne",
                u'color': "#000000",
            }
        }

        return {u'options': json.dumps(options), u'dataset': json.dumps(dataset)}


    def _get_graph_7(self):
        now = datetime.now()
        green = 0
        yellow = 0
        red = 0
        black  = 0
        for i in StateDaysLimit.objects.filter(state=State.STATE_EM_REVISAO):
            last_date_by_ct_id = {}
            for j in StateObjectHistory.objects.filter(
                content_id__in=StateObjectHistory.objects.filter(
                                state=i.state,
                                content_id__in=self.request.user.\
                                    holding_clinical_trials.all().values_list(
                                        u'pk', flat=True
                                    )
                               ).values_list(u'content_id', flat=True),
                state=i.state):

                #Just do this action if actually has at least one clinical trial in this current state
                if StateObjectRelation.objects.filter(state=i.state, content_id=j.content_id).exists():
                    if last_date_by_ct_id.has_key(j.content_id):
                        if last_date_by_ct_id[j.content_id] < j.update_date:
                            last_date_by_ct_id[j.content_id] = j.update_date
                    else:
                        last_date_by_ct_id[j.content_id] = j.update_date
            for v in last_date_by_ct_id.values():
                days_in_state = (now - v.replace(tzinfo=None)).days
                if days_in_state < i.green:
                    green += 1
                elif days_in_state <= i.yellow and days_in_state > i.green:
                    yellow += 1
                elif days_in_state > i.yellow and days_in_state <= i.red:
                    red += 1
                elif days_in_state > i.red:
                    black += 1


        if not green and not yellow and not red and not black:
            return {}


        dataset = [
         {
            u'label': _("In Time"),
            u'color': "#006400",
            u'data': green
         },
         {
            u'label': _("Late"),
            u'color': "#FF0000",
            u'data': red
         },
         {
            u'label': _("Need Attention"),
            u'color': "#FFFF00",
            u'data': yellow
         },
         {
            u'label': _("went the deadline"),
            u'color': "#000000",
            u'data': black
         }
         ]

        options = {
            u'series': {
                u'pie': {
                    u'show': True,
                    u'label': {
                        u'show': True
                    }
                }
            },
            u'legend': {
                u'show': True,
                u'labelBoxBorderColor': "#ffffff",
                u'position': "ne",
                u'color': "#000000",
            }
        }

        return {u'options': json.dumps(options), u'dataset': json.dumps(dataset)}


    def _get_graph_6(self):
        now = datetime.now()
        green = 0
        yellow = 0
        red = 0
        black  = 0
        for i in StateDaysLimit.objects.filter(
            state=State.STATE_AGUARDANDO_REVISAO
        ):
            last_date_by_ct_id = {}
            for j in StateObjectHistory.objects.filter(
                content_id__in=StateObjectHistory.objects.filter(
                                state=i.state,
                                content_id__in=self.request.user.\
                                    holding_clinical_trials.all().values_list(
                                        u'pk', flat=True
                                    )
                               ).values_list(u'content_id', flat=True),
                state=i.state):

                #Just do this action if actually has at least one clinical trial in this current state
                #from IPython import embed;embed()
                if StateObjectRelation.objects.filter(state=i.state, content_id=j.content_id).exists():
                    if last_date_by_ct_id.has_key(j.content_id):
                        if last_date_by_ct_id[j.content_id] < j.update_date:
                            last_date_by_ct_id[j.content_id] = j.update_date
                    else:
                        last_date_by_ct_id[j.content_id] = j.update_date
            for v in last_date_by_ct_id.values():
                days_in_state = (now - v.replace(tzinfo=None)).days
                if days_in_state < i.green:
                    green += 1
                elif days_in_state < i.yellow and days_in_state > i.green:
                    yellow += 1
                elif days_in_state > i.yellow and days_in_state < i.red:
                    red += 1
                elif days_in_state > i.red:
                    black += 1

        if not green and not yellow and not red and not black:
            return {}



        dataset = [
         {
            u'label': _("In Time"),
            u'color': "#006400",
            u'data': green
         },
         {
            u'label': _("Late"),
            u'color': "#FF0000",
            u'data': red
         },
         {
            u'label': _("Need Attention"),
            u'color': "#FFFF00",
            u'data': yellow
         },
         {
            u'label': _("went the deadline"),
            u'color': "#000000",
            u'data': black
         }
         ]

        options = {
            u'series': {
                u'pie': {
                    u'show': True,
                    u'label': {
                        u'show': True
                    }
                }
            },
            u'legend': {
                u'show': True,
                u'labelBoxBorderColor': "#ffffff",
                u'position': "ne",
                u'color': "#000000",
            }
        }

        return {u'options': json.dumps(options), u'dataset': json.dumps(dataset)}



    def _get_graph_5(self):
        """
        Get the data to generate the graph, with the jquery.flot
        """
        now = datetime.now()
        green = 0
        yellow = 0
        red = 0
        black  = 0
        for i in StateDaysLimit.objects.all():
            last_date_by_ct_id = {}
            for j in StateObjectHistory.objects.filter(
                content_id__in=StateObjectHistory.objects.filter(
                                state=i.state
                               ).values_list(u'content_id', flat=True),
                state=i.state):

                #Just do this action if actually has at least one clinical trial in this current state
                if StateObjectRelation.objects.filter(state=i.state, content_id=j.content_id).exists():
                    if last_date_by_ct_id.has_key(j.content_id):
                        if last_date_by_ct_id[j.content_id] < j.update_date:
                            last_date_by_ct_id[j.content_id] = j.update_date
                    else:
                        last_date_by_ct_id[j.content_id] = j.update_date
            for v in last_date_by_ct_id.values():
                days_in_state = (now - v.replace(tzinfo=None)).days
                if days_in_state <= i.green:
                    green += 1
                elif days_in_state <= i.yellow and days_in_state > i.green:
                    yellow += 1
                elif days_in_state > i.yellow and days_in_state <= i.red:
                    red += 1
                elif days_in_state > i.red:
                    black += 1


        dataset = [
         {
            u'label': _("Initial deadline"),
            u'color': "#006400",
            u'data': green
         },
         {
            u'label': _("Late"),
            u'color': "#FF0000",
            u'data': red
         },
         {
            u'label': _("Initial deadline has expired"),
            u'color': "#FFFF00",
            u'data': yellow
         },
         {
            u'label': _("Very late"),
            u'color': "#000000",
            u'data': black
         }
         ]

        options = {
            u'series': {
                u'pie': {
                    u'show': True,
                    u'label': {
                        u'show': True
                    }
                }
            },
            u'legend': {
                u'show': True,
                u'labelBoxBorderColor': "#ffffff",
                u'position': "ne",
                u'color': "#000000",
            }
        }

        return {u'options': json.dumps(options), u'dataset': json.dumps(dataset)}


    def _get_graph_3(self):
        """
        Get the data to generate the graph, with the jquery.flot
        """
        evaluators = User.objects.filter(
            groups__name=u'reviser'
        ).order_by(u'pk')
        states_pks = State.objects.filter(
            pk__in=(State.STATE_EM_REVISAO,
                    State.STATE_AGUARDANDO_REVISAO
            )).order_by(u'pk').values_list(u'pk', flat=True)

        dataset = []
        states_dict = {}
        for i in states_pks:
            states_dict[i] = []
            for c, j in enumerate(evaluators):
                stats = StateObjectRelation.objects.filter(
                    content_id__in=j.holding_clinical_trials.all(),
                    state_id=i
                ).count()
                states_dict[i].append([c, stats])



        for c, s in enumerate(states_dict.items()):

            dataset.append(
                {u'label': State.objects.get(pk=s[0]).name,
                 u'data': s[1],
                 u'bars': {
                    u'show': True,
                    u'barWidth': 0.1,
                    u'fill': True,
                    u'lineWidth': 1,
                    u'order': c
                    }
                }
            )

        ticks = list(enumerate([i.username for i in evaluators]))

        options = {
            u'series': {
                u'bars': {
                    u'show': True
                }
            },
            u'bars': {
                u'align': "center",
                u'barWidth': 0.5
            },
            u'xaxis': {
                u'axisLabel': _("Reviser"),
                u'axisLabelUseCanvas': True,
                u'axisLabelFontSizePixels': 12,
                u'axisLabelFontFamily': 'Verdana, Arial',
                u'axisLabelPadding': 10,
                u'ticks': ticks
            },
            u'yaxis': {
                u'axisLabel': _("Number of Trials"),
                u'axisLabelUseCanvas': True,
                u'axisLabelFontSizePixels': 12,
                u'axisLabelFontFamily': 'Verdana, Arial',
                u'axisLabelPadding': 3
            },
            u'legend': {
                u'noColumns': 1,
                u'labelBoxBorderColor': "#000000",

                u'show': True,
            },
            u'grid': {
                u'hoverable': True,
                u'borderWidth': 2,
                u'backgroundColor': { u'colors': ["#ffffff", "#EDF5FF"] }
            }
        }
        return {u'options': json.dumps(options), u'dataset': json.dumps(dataset)}


    def _get_graph_2(self):
        """
        Get the data to generate the graph, with the jquery.flot
        """
        evaluators = User.objects.filter(
            groups__name=u'evaluator'
        ).order_by(u'pk')
        states_pks = State.objects.filter(
            pk__in=(State.STATE_EM_AVALIACAO,
                    State.STATE_AGUARDANDO_AVALIACAO
            )).order_by(u'pk').values_list(u'pk', flat=True)

        dataset = []
        states_dict = {}
        for i in states_pks:
            states_dict[i] = []
            for c, j in enumerate(evaluators):
                stats = StateObjectRelation.objects.filter(
                    content_id__in=j.holding_clinical_trials.all(),
                    state_id=i
                ).count()
                states_dict[i].append([c, stats])



        for c, s in enumerate(states_dict.items()):

            dataset.append(
                {u'label': State.objects.get(pk=s[0]).name,
                 u'data': s[1],
                 u'bars': {
                    u'show': True,
                    u'barWidth': 0.1,
                    u'fill': True,
                    u'lineWidth': 1,
                    u'order': c
                    }
                }
            )

        ticks = list(enumerate([i.username for i in evaluators]))

        options = {
            u'series': {
                u'bars': {
                    u'show': True
                }
            },
            u'bars': {
                u'align': "center",
                u'barWidth': 0.5
            },
            u'xaxis': {
                u'axisLabel': _("Evaluator"),
                u'axisLabelUseCanvas': True,
                u'axisLabelFontSizePixels': 12,
                u'axisLabelFontFamily': 'Verdana, Arial',
                u'axisLabelPadding': 10,
                u'ticks': ticks
            },
            u'yaxis': {
                u'axisLabel': _("Number of Trials"),
                u'axisLabelUseCanvas': True,
                u'axisLabelFontSizePixels': 12,
                u'axisLabelFontFamily': 'Verdana, Arial',
                u'axisLabelPadding': 3
            },
            u'legend': {
                u'noColumns': 1,
                u'labelBoxBorderColor': "#000000",

                u'show': True,
            },
            u'grid': {
                u'hoverable': True,
                u'borderWidth': 2,
                u'backgroundColor': { u'colors': ["#ffffff", "#EDF5FF"] }
            }
        }
        return {u'options': json.dumps(options), u'dataset': json.dumps(dataset)}

    def _get_graph_1(self):
        """
        Get the data to generate the graph, with the jquery.flot
        """
        values = list(StateObjectRelation.objects.all().values(
                u'state__name', u'state__pk'
               ).annotate(count=Count('content_id')))

        data = []
        ticks = []
        for c, i in enumerate((State.STATE_EM_PREENCHIMENTO, State.STATE_AGUARDANDO_MODERACAO,
                  State.STATE_AGUARDANDO_AVALIACAO, State.STATE_EM_AVALIACAO,
                  State.STATE_AGUARDANDO_REVISAO, State.STATE_EM_REVISAO,
                  State.STATE_RESUBMETIDO, State.STATE_PUBLICADO,
                  State.STATE_REVISANDO_PREENCHIMENTO, State.STATE_EM_MODERACAO_RESUBMETIDO,
                  State.STATE_REVISAO_PREENCHIMENTO_RESUBMETIDO)):
            have_this_state = False
            for v in values:
                if v[u'state__pk'] == i:
                    have_this_state = True
                    data.append((c, v[u'count']))
                    ticks.append((c, v[u'state__name']))
            if not have_this_state:
                data.append((c, 0))
                ticks.append((c, State.objects.get(pk=i).name))


        dataset = json.dumps([{ u'label': _(u'Clinical Trials By State'),
                     u'data': data,
                     u'color': "#5482FF" }]);

        options = {
            u'series': {
                u'bars': {
                    u'show': True
                }
            },
            u'bars': {
                u'align': "center",
                u'barWidth': 0.5
            },
            u'xaxis': {
                u'axisLabel': _("Trial State"),
                u'axisLabelUseCanvas': True,
                u'axisLabelFontSizePixels': 12,
                u'axisLabelFontFamily': 'Verdana, Arial',
                u'axisLabelPadding': 10,
                u'ticks': ticks
            },
            u'yaxis': {
                u'axisLabel': _("Number of Trials"),
                u'axisLabelUseCanvas': True,
                u'axisLabelFontSizePixels': 12,
                u'axisLabelFontFamily': 'Verdana, Arial',
                u'axisLabelPadding': 3
            },
            u'legend': {
                u'noColumns': 0,
                u'labelBoxBorderColor': "#000000",
                u'position': "nw"
            },
            u'grid': {
                u'hoverable': True,
                u'borderWidth': 2,
                u'backgroundColor': { u'colors': ["#ffffff", "#EDF5FF"] }
            }
        }
        return {u'options': json.dumps(options), u'dataset': dataset}

    def get_context_data(self):
        context = super(DashboardView, self).get_context_data()
        filter_color = dict(self.request.GET.iterlists())
        filter_color = filter_color.get(u'favorite_colors', None)
        filter_color_two = dict(self.request.GET.iterlists())
        filter_color_two = filter_color_two.get(u'favorite_colors_two', None)

        if self.request.user.is_manager():
            context.update(self._get_manager_context())

        if self.request.user.is_reviser():
            context.update(self._get_revisor_context(filter_color,filter_color_two))

        if self.request.user.is_evaluator():
            context.update(self._get_evaluator_context(filter_color,filter_color_two))

        if self.request.user.is_administrador():
            context.update(self._get_administrador_context())

        user = self.request.user

        em_preenchimento = StateObjectRelation.objects.filter(
            state__pk=State.STATE_EM_PREENCHIMENTO).values_list(u'content_id', flat=True)

        resubmetido = StateObjectRelation.objects.filter(
            state__pk=State.STATE_RESUBMETIDO).order_by(u'update_date').values_list(u'content_id', flat=True)

        revisando_preenchimento = StateObjectRelation.objects.filter(
            state__pk=State.STATE_REVISANDO_PREENCHIMENTO).values_list(u'content_id', flat=True)

        revisando_preenchimento_resubmetido = StateObjectRelation.objects.filter(
            state__pk=State.STATE_REVISAO_PREENCHIMENTO_RESUBMETIDO).values_list(u'content_id', flat=True)

        aguardando_avaliacao = StateObjectRelation.objects.filter(
            state__pk=State.STATE_AGUARDANDO_AVALIACAO).values_list(u'content_id', flat=True)

        em_avaliacao = StateObjectRelation.objects.filter(
            state__pk=State.STATE_EM_AVALIACAO).values_list(u'content_id', flat=True)

        aguardando_revisao = StateObjectRelation.objects.filter(
            state__pk=State.STATE_AGUARDANDO_REVISAO).values_list(u'content_id', flat=True)

        em_revisao = StateObjectRelation.objects.filter(
            state__pk=State.STATE_EM_REVISAO).values_list(u'content_id', flat=True)

        aguardando_moderacao = StateObjectRelation.objects.filter(
            state__pk=State.STATE_AGUARDANDO_MODERACAO).values_list(u'content_id', flat=True)

        aguardando_moderacao_resubmetido = StateObjectRelation.objects.filter(
            state__pk=State.STATE_EM_MODERACAO_RESUBMETIDO).values_list(u'content_id', flat=True)

        puclicado = StateObjectRelation.objects.filter(
            state__pk=State.STATE_PUBLICADO).values_list(u'content_id', flat=True)
        registrant_adm = user.ctgroups.filter(type_user='a').exists()

        if user.has_perm ('admin.registrant_permission'):
            """
            If user logged is registrant, it shows all clinical trials,
            separated by: Fill, pending, under review and published.
            """
            if registrant_adm:
                """
                If the User is logged adminitrador also shows the pendencies of users:
                users who want to join the group and users who want to leave the group.
                """
                group_adm = CTGroup.objects.filter(users__user=user, users__type_user='a')
                context[u'users_penging_aprov'] = CTGroupUser.objects.filter(
                    ct_group=group_adm).filter(status=1)
                context[u'users_penging_exclusion'] = CTGroupUser.objects.filter(
                    ct_group=group_adm).filter(status=4)

            context[u'clinical_trial_em_preenchimento'] = ClinicalTrial.objects.filter(
                user=user, pk__in=em_preenchimento).order_by('-date_creation')

            context[u'clinical_trial_pending'] = \
                ClinicalTrial.objects.filter(
                    Q(pk__in=revisando_preenchimento) | Q(pk__in=resubmetido) | Q(pk__in=revisando_preenchimento_resubmetido)).filter(user=user)[:10]

            context[u'registrant_clinical_trial_em_revisao'] = \
                ClinicalTrial.objects.filter(
                    Q(pk__in=aguardando_avaliacao) | Q(pk__in=em_avaliacao) | Q(pk__in=aguardando_revisao) | Q(pk__in=em_revisao) | Q(pk__in=aguardando_moderacao) | Q(pk__in=aguardando_moderacao_resubmetido)
                ).filter(user=user)

            context[u'clinical_trial_publicado'] = ClinicalTrial.objects.filter(
                user=user, pk__in=puclicado)

        return context


class AccessibilityView(TemplateView):
    template_name = u'administration/administration/accessibility.html'



class CTReviserListBeingReviewed(ListView):
    """
    Reviser: list clinical trail Being Reviewed.
    """
    model = ClinicalTrial
    template_name = \
        u'administration/administration/ct_reviser_being_reviewed.html'

    def get_queryset(self):
        user = self.request.user
        em_revisao = StateObjectRelation.objects.filter(
            state__pk=State.STATE_EM_REVISAO).values_list(u'content_id', flat=True)
        queryset = ClinicalTrial.objects.filter(pk__in=em_revisao)
        if not user.groups.filter(name=u'manager').exists():
            queryset = queryset.filter(holder=user)

        return queryset

class CTReviserListReviewPending(ListView):
    """
    Reviser: list clinical trail ReviewPending.
    """
    model = ClinicalTrial
    template_name = \
        u'administration/administration/ct_reviser_review_pending.html'

    def get_queryset(self):
        user = self.request.user
        aguardando_revisao = StateObjectRelation.objects.filter(
            state__pk=State.STATE_AGUARDANDO_REVISAO).values_list(u'content_id', flat=True)
        queryset = ClinicalTrial.objects.filter(pk__in=aguardando_revisao)
        if not user.groups.filter(name=u'manager').exists():
            queryset = queryset.filter(holder=user)

        return queryset

class CTEvaluatorListEvaluation(ListView):
    """
    Evaluator: list clinical trail in evaluation.
    """
    model = ClinicalTrial
    template_name = \
        u'administration/administration/ct_evaluator_in_evaluation.html'

    def get_queryset(self):
        user = self.request.user
        em_avaliacao = StateObjectRelation.objects.filter(
            state__pk=State.STATE_EM_AVALIACAO).values_list(u'content_id', flat=True)
        queryset = ClinicalTrial.objects.filter(pk__in=em_avaliacao)
        if not user.groups.filter(name=u'manager').exists():
            queryset = queryset.filter(holder=user)

        return queryset

class CTEvaluatorListAwaitingAssessment(ListView):
    """
    Evaluator: list clinical trail in Awaiting Assessment.
    """
    model = ClinicalTrial
    template_name = \
        u'administration/administration/ct_evaluator_assessment_list.html'

    def get_queryset(self):
        user = self.request.user
        aguardando_avaliacao = StateObjectRelation.objects.filter(
            state__pk=State.STATE_AGUARDANDO_AVALIACAO).values_list(u'content_id', flat=True)
        queryset = ClinicalTrial.objects.filter(pk__in=aguardando_avaliacao)
        if not user.groups.filter(name=u'manager').exists():
            queryset = queryset.filter(holder=user)

        return queryset


class CTRegistrantFillList(ListView):
    """
    Registrant Profile: list clinical trail in filling.
    """
    model = ClinicalTrial
    template_name = \
        u'administration/administration/ct_registrant_fill_list.html'

    def get_queryset(self):
        user = self.request.user
        em_preenchimento = StateObjectRelation.objects.filter(
            state__pk=State.STATE_EM_PREENCHIMENTO).values_list(u'content_id', flat=True)
        queryset = ClinicalTrial.objects.filter(
                user=user, pk__in=em_preenchimento)

        return queryset


class CTRegistrantPendingList(ListView):
    """
    Registrant Profile: list clinical trail in pending.
    """
    model = ClinicalTrial
    template_name = \
        u'administration/administration/ct_registrant_pending_list.html'


    def get_queryset(self):
        user = self.request.user
        resubmetido = StateObjectRelation.objects.filter(
            state__pk=State.STATE_RESUBMETIDO).values_list(u'content_id', flat=True)

        revisando_preenchimento = StateObjectRelation.objects.filter(
            state__pk=State.STATE_REVISANDO_PREENCHIMENTO).values_list(u'content_id', flat=True)

        queryset = \
                ClinicalTrial.objects.filter(
                    Q(pk__in=revisando_preenchimento) | Q(pk__in=resubmetido)).filter(user=user)

        return queryset


class CTRegistrantReviewList(ListView):
    """
    Registrant Profile: list clinical trail in review.
    """
    model = ClinicalTrial
    template_name = \
        u'administration/administration/ct_registrant_review_list.html'

    def get_queryset(self):
        user = self.request.user
        aguardando_avaliacao = StateObjectRelation.objects.filter(
            state__pk=State.STATE_AGUARDANDO_AVALIACAO).values_list(u'content_id', flat=True)

        em_avaliacao = StateObjectRelation.objects.filter(
            state__pk=State.STATE_EM_AVALIACAO).values_list(u'content_id', flat=True)

        aguardando_revisao = StateObjectRelation.objects.filter(
            state__pk=State.STATE_AGUARDANDO_REVISAO).values_list(u'content_id', flat=True)

        em_revisao = StateObjectRelation.objects.filter(
            state__pk=State.STATE_EM_REVISAO).values_list(u'content_id', flat=True)

        aguardando_moderacao = StateObjectRelation.objects.filter(
            state__pk=State.STATE_AGUARDANDO_MODERACAO).values_list(u'content_id', flat=True)

        queryset = \
                ClinicalTrial.objects.filter(
                    Q(pk__in=aguardando_avaliacao) | Q(pk__in=em_avaliacao) | Q(pk__in=aguardando_revisao) | Q(pk__in=em_revisao) | Q(pk__in=aguardando_moderacao)).filter(user=user)

        return queryset

class CTRegistrantPublicList(ListView):
    """
    Registrant Profile: list clinical trail in filling.
    """
    model = ClinicalTrial
    template_name = \
        u'administration/administration/ct_registrant_public_list.html'

    def get_queryset(self):
        user = self.request.user
        puclicado = StateObjectRelation.objects.filter(
            state__pk=State.STATE_PUBLICADO).values_list(u'content_id', flat=True)

        queryset = ClinicalTrial.objects.filter(
                user=user, pk__in=puclicado)

        return queryset


class CTGroupsInstitutionPendingList(ListView):
    model = CTGroup
    template_name = \
        u'administration/administration/ctgroups_institution_pending_list.html'

    def get_queryset(self):
        return CTGroup.objects.filter(
                status_institution=CTGroup.STATUS_INSTITUTION_PENDING,
                status=CTGroup.STATUS_ACTIVE,
                institution__status=Institution.STATUS_ACTIVE
               )

class CTGroupsInstitutionPendingView(PermissionRequiredMixin, TemplateView):
    """
    View to accept or refuse pending institutions ct group associations request.
    """
    template_name = u'administration/administration/moderation_des.html'
    success_url = reverse_lazy(u'home')
    permissions_required = ['admin.administrador_permission']

    def get_context_data(self, group_pk, decision):
        self.object = self.get_group(group_pk)
        context = super(CTGroupsInstitutionPendingView, self).get_context_data()
        if decision == u'A':
            context[u'message'] = _(u'Do you want to accept?')
        else:
            context[u'message'] = _(u'Do you want to refuse?')
        context[u'back_url'] = reverse(u'home')

        return context

    def post(self, request, group_pk, decision):
        self.object = self.get_group(group_pk)
        if request.POST.get(u'decision', u'no') == u'yes':
            if decision == u'A':
                self.object.status_institution = \
                    CTGroup.STATUS_INSTITUTION_APPROVED
                if self.object.users.all().filter(status=5).exists():
                    self.object.users.all().filter(status=5).update(status=2)

                self.object.ctgrouplog_set.create(user=request.user,
                        message=_('Institution %s has been approved.' \
                                    % self.object.name))
                messages.success(request, _(u'Group Approved'))
            else:
                self.object.status_institution = \
                    CTGroup.STATUS_INSTITUTION_CANCEL
                self.object.institution = None
                self.object.ctgrouplog_set.create(user=request.user,
                        message=_('Institution %s has been refused.' \
                                    % self.object.name))
                messages.success(request, _(u'Group Refused'))
            self.object.save()

            return HttpResponseRedirect(self.success_url)

        messages.error(request, _(u'Invalid Decision'))
        return HttpResponseRedirect(u'.')

    def get_group(self, group_pk):
        return get_object_or_404(CTGroup, pk=group_pk)


class InstitutionPendingListView(PermissionRequiredMixin, ListView):
    model = Institution
    template_name = u'administration/administration/institution_moderation.html'
    permissions_required = ['admin.administrador_permission']
    def get_queryset(self):
        queryset = super(InstitutionPendingListView, self).get_queryset()
        moderation_status_cts = StateObjectRelation.objects.filter(
                                    state__pk__in=\
                                    (State.STATE_AGUARDANDO_MODERACAO,
                                     State.STATE_EM_MODERACAO_RESUBMETIDO)
                                ).values_list('content_id', flat=True)
        return queryset.filter(
                status=Institution.STATUS_MODERATION
            ).filter(
                Q(sponsor_items__clinical_trial__pk__in=moderation_status_cts)
            ).distinct()


class InstitutionPendingView(PermissionRequiredMixin, TemplateView):
    """
    View to accept or refuse pending institutions creations request.
    """
    template_name = \
        u'administration/administration/institution_moderation_decision.html'
    success_url = reverse_lazy(u'institution_pending_list')
    permissions_required = ['admin.administrador_permission']
    response_class = RebecResponse

    def get_context_data(self, institution_pk, decision):
        self.object = self.get_institution(institution_pk)
        context = super(InstitutionPendingView, self).get_context_data()
        if decision == u'A':
            context[u'message'] = _(u'Approve Institution?')
        else:
            context[u'message'] = _(u'Refuse Institution?')
        context[u'object'] = self.object
        context[u'decision'] = decision
        context[u'back_url'] = reverse(u'institution_pending_list')

        return context

    def _check_clinical_trial_has_more_moderation_sponsors(self):
        try:
            return self.object.sponsor_items.all()[0].\
                clinical_trial.sponsor_set.filter(
                    institution__status=Institution.STATUS_MODERATION
                ).exists()
        except IndexError:
            return False

    def _check_clinical_trial_has_refused_sponsors(self):
            return self.object.sponsor_items.all()[0].\
                clinical_trial.sponsor_set.filter(
                    institution__status=Institution.STATUS_INACTIVE
                ).exists()

    def _get_all_refused_institutions_observations(self):
            observations = ''
            institutions_pks = []
            for i in self.object.sponsor_items.all()[0].\
                clinical_trial.sponsor_set.filter(
                    institution__status=Institution.STATUS_INACTIVE
                ).values_list(u'institution__temp_workflow_observation',
                              u'institution__pk'):
                observations += i[0] or ''
                institutions_pks.append(i[1])

            Institution.objects.filter(
                pk__in=institutions_pks
            ).update(temp_workflow_observation='')
            return observations

    def post(self, request, institution_pk, decision):
        self.object = self.get_institution(institution_pk)
        if request.POST.get(u'decision', u'no') == u'yes':
            if decision == u'A':
                self.object.status = \
                    Institution.STATUS_ACTIVE
                messages.success(request, _(u'Institution Approved'))
                for i in self.object.ctgroups.all():
                    i.ctgrouplog_set.create(user=request.user,
                        message=_('Institution %s has been accepted.' \
                                    % self.object.name))
                self.object.save()
            else:
                self.object.status = \
                    Institution.STATUS_INACTIVE
                messages.success(request, _(u'Institution Refused'))
                self.object.temp_workflow_observation = \
                    self.request.POST.get(u'observation', u'')
                #reproved, so all groups using this institution must have
                #the relation erased
                self.object.save()
                for i in self.object.ctgroups.all():
                    i.ctgrouplog_set.create(user=request.user,
                        message=_('Institution %s has been refused.' \
                                    % self.object.name))
                self.object.ctgroups.all().update(institution=None,
                    status_institution=CTGroup.STATUS_INSTITUTION_CANCEL)

                for current_sponsor in self.object.sponsor_items.all():
                    StateObjectGeneralObservation.objects.create(
                        user=request.user,
                        content_type=ContentType.objects.get_for_model(ClinicalTrial),
                        content_id=current_sponsor.clinical_trial.pk,
                        observation=_(u'The institution %s was refused due to the following reason: %s ') % (
                                        self.object.name,
                                        self.request.POST.get(u'observation', u'')
                                    ),
                        object_state=StateObjectHistory.objects.filter(
                                         content_id=current_sponsor.clinical_trial.pk
                                     ).latest(u'update_date')
                    )
            if not self._check_clinical_trial_has_more_moderation_sponsors():
                for current_sponsor in self.object.sponsor_items.all():
                    clinical_trial = current_sponsor.clinical_trial
                    clinical_trial_workflow_state = clinical_trial.get_workflow_state()
                    if self._check_clinical_trial_has_refused_sponsors():
                        if clinical_trial_workflow_state.pk == State.STATE_AGUARDANDO_MODERACAO:
                            do_transition(
                                clinical_trial,
                                Transition.objects.get(
                                    pk=Transition.TRANSITION_REVISAR_PREENCHIMENTO
                                ),
                                self.request.user,
                                self._get_all_refused_institutions_observations()
                            )
                        elif clinical_trial_workflow_state.pk == State.STATE_EM_MODERACAO_RESUBMETIDO:
                            do_transition(
                                clinical_trial,
                                Transition.objects.get(
                                    pk=Transition.TRANSITION_REVISAR_PREENCHIMENTO_RESUBMETIDO
                                ),
                                self.request.user,
                                self._get_all_refused_institutions_observations()
                            )

                    else:
                        if clinical_trial_workflow_state.pk == State.STATE_AGUARDANDO_MODERACAO:
                            if Group.objects.get(
                                     name=u'evaluator'
                                 ).user_set.all().exists():
                                do_transition(
                                    clinical_trial,
                                    Transition.objects.get(
                                        pk=Transition.TRANSITION_AGUARDAR_AVALIACAO
                                    ),
                                    self.request.user,
                                )
                                clinical_trial.holder = get_next_by_occupation(u'evaluator')
                                clinical_trial.save()
                            else:
                                do_transition(
                                    clinical_trial,
                                    Transition.objects.get(
                                        pk=Transition.TRANSITION_AGUARDAR_REVISAO
                                    ),
                                    self.request.user
                                )
                                clinical_trial.holder = get_next_by_occupation(u'reviser')
                                clinical_trial.save()
                        elif clinical_trial_workflow_state.pk == State.STATE_EM_MODERACAO_RESUBMETIDO:
                            do_transition(
                                    clinical_trial,
                                    Transition.objects.get(
                                        pk=Transition.TRANSITION_REENVIAR_A_REVISAO
                                    ),
                                    self.request.user
                                )
                            clinical_trial.holder = \
                                    StateObjectHistory.objects.filter(
                                        content_id=clinical_trial.id
                                    ).filter(
                                        state_id=State.STATE_RESUBMETIDO
                                    ).latest('update_date').user
                            clinical_trial.save()

            return HttpResponseRedirect(self.success_url)

        messages.error(request, _(u'Invalid Decision'))
        return HttpResponseRedirect(u'institution_pending_list')

    def get_institution(self, institution_pk):
        return get_object_or_404(Institution, pk=institution_pk)

class InstitutionPendingChangeView(PermissionRequiredMixin, TemplateView):
    """
    View to change a created institution and substitute
    it on the clinical trial and the group that uses it.
    """
    template_name = \
        u'administration/administration/institution_change_moderation.html'
    success_url = reverse_lazy(u'home')
    permissions_required = ['admin.administrador_permission']

    def _check_clinical_trial_has_more_moderation_sponsors(self):
            return self.new_institution.sponsor_items.all()[0].\
                clinical_trial.sponsor_set.filter(
                    institution__status=Institution.STATUS_MODERATION
                ).exists()

    def _check_clinical_trial_has_refused_sponsors(self):
            return self.new_institution.sponsor_items.all()[0].\
                clinical_trial.sponsor_set.filter(
                    institution__status=Institution.STATUS_INACTIVE
                ).exists()

    def _get_all_refused_institutions_observations(self):
            observations = ''
            institutions_pks = []
            for i in self.new_institution.sponsor_items.all()[0].\
                clinical_trial.sponsor_set.filter(
                    institution__status=Institution.STATUS_INACTIVE
                ).values_list(u'institution__temp_workflow_observation',
                              u'institution__pk'):
                observations += i[0] or ''
                institutions_pks.append(i[1])

            Institution.objects.filter(
                pk__in=institutions_pks
            ).update(temp_workflow_observation='')
            return observations

    def get_context_data(self, institution_pk):
        self.object = self.get_institution(institution_pk)
        context = super(InstitutionPendingChangeView, self).get_context_data()

        context[u'change_form'] = InstitutionModerationChangeForm()

        return context

    def post(self, request, institution_pk):
        self.object = self.get_institution(institution_pk)
        form = InstitutionModerationChangeForm(request.POST)
        if form.is_valid():
            self.object.status = Institution.STATUS_INACTIVE
            self.object.save()
            #change all the clinical_trial sponsors of
            #this institution to the selected new one
            sponsors_using = self.object.sponsor_items.all().update(
                institution=form.cleaned_data[u'institution'])
            ctgroups_using = self.object.ctgroups.all().update(
                institution=form.cleaned_data[u'institution']
            )
            self.new_institution = form.cleaned_data[u'institution']
            if sponsors_using:
                clinical_trial = self.new_institution.sponsor_items.all()[0].clinical_trial
                if not self._check_clinical_trial_has_more_moderation_sponsors():
                    if self._check_clinical_trial_has_refused_sponsors():
                        do_transition(
                            self.new_institution.sponsor_items.all()[0].clinical_trial,
                            Transition.objects.get(
                                pk=Transition.TRANSITION_REVISAR_PREENCHIMENTO
                            ),
                            self.request.user,
                            self._get_all_refused_institutions_observations()
                        )
                    else:
                        if Group.objects.get(
                                 name=u'evaluator'
                             ).user_set.all().exists():
                            do_transition(
                                self.new_institution.sponsor_items.all()[0].clinical_trial,
                                Transition.objects.get(
                                    pk=Transition.TRANSITION_AGUARDAR_AVALIACAO
                                ),
                                self.request.user,
                            )
                            clinical_trial.holder = get_next_by_occupation(u'evaluator')
                            clinical_trial.save()
                        else:
                            do_transition(
                                self.new_institution.sponsor_items.all()[0].clinical_trial,
                                Transition.objects.get(
                                    pk=Transition.TRANSITION_AGUARDAR_REVISAO
                                ),
                                self.request.user
                            )
                            clinical_trial.holder = get_next_by_occupation(u'reviser')
                            clinical_trial.save()

            messages.success(request, _(u'Institution changed'))
            return HttpResponseRedirect(self.success_url)

        context = self.get_context_data(institution_pk)
        context[u'change_form'] = form

        return self.render_to_response(
            context
            )

    def get_institution(self, institution_pk):
        return get_object_or_404(Institution, pk=institution_pk)


class CTGroupsAdministratorPendingList(ListView):
    model = CTGroup
    template_name =u'administration/administration/ctgroups_admin_pending_list.html'

    def get_queryset(self):
        groups = CTGroupUser.objects.filter(
            status=CTGroupUser.STATUS_PENDING_APROV_ADM,
            ct_group__status=CTGroup.STATUS_ACTIVE
            ).filter(
                Q(ct_group__status_institution=CTGroup.STATUS_INSTITUTION_APPROVED)
                    |
                Q(ct_group__institution__isnull=True)
                )
        return groups


class GroupAdministratorPendingView(PermissionRequiredMixin, TemplateView):
    """
    View to allow or not allow group adminissssstrator's requests.
    """
    template_name = u'administration/administration/moderation_decision.html'
    success_url = reverse_lazy(u'home')
    permissions_required = ['admin.administrador_permission']

    def get_context_data(self, ctgroupuser_pk, decision):
        self.object = self.get_ctgroupuser(ctgroupuser_pk)
        context = super(GroupAdministratorPendingView, self).get_context_data()
        if decision == u'A':
            context[u'message'] = _(u'Do you want to accept?')
        else:
            context[u'message'] = _(u'Do you want to refuse?')

        context[u'back_url'] = reverse(u'home')

        return context

    def post(self, request, ctgroupuser_pk, decision):
        self.object = self.get_ctgroupuser(ctgroupuser_pk)
        if request.POST.get(u'decision', u'no') == u'yes':
            if decision == u'A':
                self.object.status = \
                    CTGroupUser.STATUS_ACTIVE

                messages.success(request, _(u'Group Administrator Approved'))
            else:
                self.object.status = \
                    CTGroupUser.STATUS_INACTIVE

                #Notify user by email
                message = u'Your request to be an administrator of'
                message += u' group %s was rejected' % self.object.ct_group.name
                message += u' by the system administrator.'
                send_mail(
                    _(u'Group Administration Moderation'),
                    _(message),
                    settings.DEFAULT_FROM_EMAIL,
                    [self.object.user.email],
                    fail_silently=False
                    )
                messages.success(request, _(u'Group Administrator Refused'))
            self.object.save()


            return HttpResponseRedirect(self.success_url)

        messages.error(request, _(u'Invalid Decision'))
        return HttpResponseRedirect(u'.')

    def get_ctgroupuser(self, ctgroupuser_pk):
        return get_object_or_404(CTGroupUser, pk=ctgroupuser_pk)


class ContactsManageView(ListView):
    model = Contact
    template_name = u'administration/administration/contacts_manage.html'

    def get_queryset(self):
        """
        Return just the contacts associated in the clinical trials of the
        user.
        """
        queryset = super(ContactsManageView, self).get_queryset()
        return queryset.filter(
                ctcontact__clinical_trial__user=self.request.user
               ).distinct()



class ContactManageEditView(UpdateView):
    model = Contact
    form_class = ContactForm
    template_name = u'administration/administration/contact_manage_edit.html'
    success_url = reverse_lazy(u'contacts_manage')

    def get_object(self):
        """
        Just return the obect if it's of a clinical trial of the user.
        """
        obj = super(ContactManageEditView, self).get_object()
        if not obj.ctcontact_set.filter(
            clinical_trial__user=self.request.user
            ).exists():
            raise PermissionDenied

        return obj

    def get_success_url(self):
        messages.success(self.request, _(u'Contact successfully updated.'))
        return self.request.GET.get(u'back_url', self.success_url)

class RecruitmentInterruptListView(ListView):
    model = ClinicalTrial
    template_name = \
        u'administration/administration/recruitment_interrupt_list.html'

    def get_context_data(self):
        context = super(RecruitmentInterruptListView, self).get_context_data()
        context[u'show_all_dates'] = self.request.GET.get(
            u'show_all_dates',
            u'no'
        )
        return context

    def get_queryset(self):
        """
        Return just the clinical trials of the
        groups that the current user is administrator.
        """
        queryset = super(RecruitmentInterruptListView, self).get_queryset()
        now = datetime.now()
        if not self.request.GET.get(u'show_all_dates', 'no') == 'yes':
            queryset = queryset.filter(
                date_first_enrollment__lte=now,
                date_last_enrollment__gte=now
            )
        published_pks = StateObjectRelation.objects.filter(
            state=State.STATE_PUBLICADO
        ).values_list(u'content_id', flat=True)

        queryset = queryset.filter(pk__in=published_pks)
        #final filter, justo the clinical trials with a group
        #that the current user is administrator, or clinical trials
        #without group, but created by the current user
        return queryset.filter(
                Q(
                group__in=
                    self.request.user.ctgroups.filter(
                        type_user=CTGroupUser.TYPE_ADMINISTRATOR,
                        status=CTGroupUser.STATUS_ACTIVE
                    ).values_list(u'ct_group__pk', flat=True)
                )
                |
                Q(
                    group__isnull=True,
                    user=self.request.user
                )
               )


class RecruitmentInterruptView(DetailView):
    model = ClinicalTrial
    template_name = \
        u'administration/administration/recruitment_interrupt_confirmation.html'

    def get_object(self):
        """
        Check that this clinical trial is of a group that the current user
        is administrator.
        """
        obj = super(RecruitmentInterruptView, self).get_object()
        if obj.group.pk in self.request.user.ctgroups.filter(
                               type_user=CTGroupUser.TYPE_ADMINISTRATOR,
                               status=CTGroupUser.STATUS_ACTIVE
                           ).values_list(u'ct_group__pk', flat=True):
            return obj

        raise PermissionDenied

    def post(self, request, pk):
        """
        Get the user decision and interrupt/cancel interruption
        of a recruitment.
        """
        self.object = self.get_object()
        self.is_interrupted = self.object.is_interrupted()
        comments = self.request.POST.get(u'comments', None)
        if not self.is_interrupted and not comments:
            messages.error(self.request, _(u'You need to provide a comment.'))
            return HttpResponseRedirect(
                reverse(u'recruitment_interrupt', args=(self.object.pk,))
            )

        if self.request.POST.get(u'confirm', u'no') == u'yes':
            if self.is_interrupted:
                self.object.recruitment_interruptions.create(
                    interrupt=False,
                    user=request.user,
                )
                message = _(u'Recruitment interruption successfully canceled.')
            else:
                self.object.recruitment_interruptions.create(
                    interrupt=True,
                    comments=comments,
                    user=request.user,
                )
                message = _(u'Recruitment successfully interrupted.')
            messages.success(self.request, message)
        else:
            messages.error(self.request, _(u'Invalid Option'))

        return HttpResponseRedirect(reverse(u'recruitment_interrupt_list'))


class RecruitmentInterruptionHistoryView(DetailView):
    model = ClinicalTrial
    template_name = \
        u'administration/administration/recruitment_interruption_history.html'

    def get_object(self):
        """
        Check that this clinical trial is of a group that the current user
        is administrator.
        """
        obj = super(RecruitmentInterruptionHistoryView, self).get_object()
        if obj.user == self.request.user and obj.group is None:
            return obj
        if obj.group.pk in self.request.user.ctgroups.filter(
                               type_user=CTGroupUser.TYPE_ADMINISTRATOR,
                               status=CTGroupUser.STATUS_ACTIVE
                           ).values_list(u'ct_group__pk', flat=True):

            return obj

        raise PermissionDenied


class CTAllList(ListView):
    """
    View lists all active groups are not logged user.
    """
    model = ClinicalTrial
    template_name = 'administration/administration/ct_list_all.html'
    form_class = CTAllListSearchForm
    extra = 0

    def get(self, *args, **kwargs):
        response = super(CTAllList, self).get(*args, **kwargs)
        if self.export:
            return self.response

        return response

    def get_context_data(self, **kwargs):
        context = super(CTAllList, self).get_context_data(
            **kwargs)
        context[u'search_form'] = CTAllListSearchForm(
            self.request.GET)
        query_string = self.request.META[u'QUERY_STRING']
        if query_string:
            context[u'filter_made'] = True
            query_string_export = query_string + '&export=yes'
        else:
            context[u'filter_made'] = False
            query_string_export = '?export=yes'

        context[u'query_string_export'] = query_string_export

        from django.core.paginator import Paginator
        paginator = Paginator(context[u'object_list'], 20)

        try:
            context[u'page'] = paginator.page(self.request.GET.get(u'page', 1))
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            context[u'page'] = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            context[u'page'] = paginator.page(paginator.num_pages)

        context[u'query_string'] = query_string or '?a=1'
        context[u'query_string'] = context[u'query_string'].replace(u'&page=%s' % context[u'page'].number, '')
        return context

    def get_queryset(self):

        queryset = super(CTAllList, self).get_queryset()

        group = self.request.GET.get(u'group', None)
        code = self.request.GET.get(u'code', None)
        holder = self.request.GET.get(u'holder', None)
        user = self.request.GET.get(u'user', None)
        rbr = self.request.GET.get(u'rbr', None)
        wf_state = self.request.GET.get(u'wf_state', None)

        if group:
            queryset = queryset.filter(group=group)
        if code:
            queryset = queryset.filter(id=code)
        if user:
            queryset = queryset.filter(user=user)
        if holder:
            queryset = queryset.filter(holder=holder)
        if rbr:
            queryset = queryset.filter(code=rbr)
        if wf_state and wf_state != 'all':
            queryset = queryset.filter(
                    pk__in=StateObjectRelation.objects.filter(
                            state__pk=wf_state
                       ).values_list(u'content_id', flat=True)
                )

        self.export = False
        if self.request.GET.has_key(u'export') and \
                self.request.user.is_administrador():
            self.export = True
            export_data = '<?xml version="1.0" encoding="UTF-8"?><trials>'
            export_config = ExportConfig.objects.get(
                                active=True
                            )
            for ct in queryset:
                export_data += export_config.generate_xml(
                                    ct=ct,
                                    with_declaration=False
                               )

            export_data += '</trials>'

            self.response = HttpResponse(export_data,
                                         content_type='application/xml')

            self.response['Content-Disposition'] = \
                                        'attachment; filename=%s_all.xml' \
                                            % export_config.file_name

        if group or code or user or rbr or wf_state or holder:
            return queryset
        else:
            return queryset.none()


class HelpView(TemplateView):

    template_name = u'portal/home.html'


def _get_help(request):
    """
    **TEMPLATE_CONTEXT_PROCESSOR:**
    Receives the URL and checks if there is help for that url and if the logged User
    has access, if you handle the items that help turns into dictionary list and
    returns a JSON.
    """
    from administration.models import Help, ItemsHelps, Language, HelpLanguage
    from django.core.urlresolvers import resolve
    path = request.path
    language_request = request.LANGUAGE_CODE
    try:
        url_help = resolve(path).url_name
    except:
        try:
            url_help = resolve('/' + language_request + path).url_name
        except:
            return {}
    help_c = []
    request_user = request.user.id
    help_c = Help.objects.filter(url_name=url_help)
    allow = False
    if request.user.is_authenticated():
        user = User.objects.get(id=request_user)
        groups = [group.name for group in user.groups.all()]
        if user.is_registrant_administrator():
            groups.append(u'register_admin')
        for help_type in help_c:
            for help_user_type in help_type.type_user:
                if  help_user_type in groups:
                    allow = True
    else:
        for help_type in help_c:
            if help_type.type_user == [u'']:
                allow = True

    tour = {}
    tour['tour'] = {}
    if allow:
        helplanguage = HelpLanguage.objects.filter(help=help_c)
        if helplanguage:
            language = Language.objects.get(code=language_request)
            help_d = HelpLanguage.objects.filter(language=language, help=url_help)
            if help_d:
                help_din = help_d[0].pk
                item = ItemsHelps.objects.filter(help_language=help_din).order_by('pk')
            else:
                help_din = HelpLanguage.objects.filter(help=url_help)[0].pk
                item = ItemsHelps.objects.filter(help_language=help_din).order_by('pk')

            help_list = []
            if item:
                for i in item:
                    item_help = {
                        'element': i.element,
                        'placement': i.placement,
                        'title': i.title,
                        'content': i.content,
                        'next': i.next,
                        'prev': i.prev,
                        'animation': i.animation,
                        'backdrop': i.backdrop,
                        'backdropPadding': i.backdropPadding,
                        'storage': 'window.sessionStorage',
                    }
                    if i.onShow:
                        item_help['onShow']= i.onShow

                    if i.onShown:
                        item_help['onShown']= i.onShown

                    if i.onHide:
                        item_help['onHide']= i.onHide

                    if i.onNext:
                        item_help['onNext']= i.onNext

                    if i.onPrev:
                        item_help['onPrev']= i.onPrev

                    if i.path:
                        item_help['path']= i.path

                    help_list.append(item_help)
                tour['tour'] = json.dumps({"name": url_help, "steps": help_list})
                tour['reset'] = ""
                if 'tour' in request.GET:
                    tour['reset'] = url_help
                return tour
            else:
                return tour
        else:
            return tour
    else:
        return tour


from dateutil.relativedelta import relativedelta

class IndicatorsByClinicalTrialView(TemplateView):
    template_name = \
        'administration/administration/indicators_by_clinical_trial.html'

    def get_stats1(self):
        """
        Time(hours) since first revision
        flot-ibct1-placeholder
        """
        published_state = State.objects.get(pk=State.STATE_PUBLICADO)
        revision_state = State.objects.get(pk=State.STATE_EM_REVISAO)
        waiting_moderation_state = State.objects.get(pk=State.STATE_AGUARDANDO_MODERACAO)
        waiting_revision_state = State.objects.get(pk=State.STATE_AGUARDANDO_REVISAO)
        waiting_avaliacao_state = State.objects.get(pk=State.STATE_AGUARDANDO_AVALIACAO)

        datefirst = datetime.now()
        datelast = datetime.now()

        search_form = CTIndicatorDateSearchForm(self.request.GET or None)
        medias = []
        dias = 0
        cts = 0
        hours = 0
        months = []
        id_medias = 0
        if search_form.is_valid():
            datefirst = search_form.cleaned_data.get(u'date_first', None)
            datelast = search_form.cleaned_data.get(u'date_last', None)

            state_history = StateObjectHistory.objects.filter(update_date__gt=datefirst, update_date__lt=datelast, state_id=revision_state.pk).values_list('pk', flat=True)

            datefirst.replace(day=1)
            current = datefirst
            while current <= datelast:
                med = 0
                cts = 0
                dias = 0
                hours = 0
                historys_mes = StateObjectHistory.objects.filter(pk__in=state_history, update_date__month=current.month, update_date__year=current.year).values_list('content_id', flat=True)
                clinical_trials_pks = ClinicalTrial.objects.filter(code__isnull=False, pk__in=historys_mes)
                for ct in clinical_trials_pks:
                    history = StateObjectHistory.objects.filter(content_id=ct.pk)
                    if history:
                        datetime_in_revision = None
                        datetime_before_revision = None
                        date = history.filter(state=revision_state).dates('update_date', 'day', order='DESC')
                        datetime_in_revision = date[0]
                        cts += 1
                        if datetime_before_revision == None:
                            dates_mod = history.filter(state=waiting_moderation_state).dates('update_date', 'day', order='ASC')
                            dates_arevis = history.filter(state=waiting_revision_state).dates('update_date', 'day', order='ASC')
                            dates_avaliacao = history.filter(state=waiting_avaliacao_state).dates('update_date', 'day', order='ASC')

                            if dates_mod:
                                datetime_before_revision = dates_mod[0]
                            elif dates_avaliacao:
                                datetime_before_revision = dates_avaliacao[0]
                            elif dates_arevis:
                                datetime_before_revision = dates_arevis[0]
                        dias += (datetime_in_revision - datetime_before_revision).days
                        if datetime_in_revision and datetime_before_revision and not dias:
                            hours += ((datetime_in_revision - datetime_before_revision).seconds)/60/60

                if cts and dias:
                    med = (dias+hours)/cts
                    medias.append((current.month, med))
                    months.append((current.month, '%s/%s' %(current.month, current.year)))

                current += relativedelta(months=1)


        dataset = json.dumps([{ u'label': _(u'Indicators by clinital trial'),
                     u'data': medias,
                     u'color': "#5482FF" }]);

        options = {
            u'series': {
                u'bars': {
                    u'show': True
                }
            },
            u'bars': {
                u'align': "center",
                u'barWidth': 0.5
            },

            u'xaxis': {
                u'axisLabel': _("Months"),
                u'axisLabelUseCanvas': True,
                u'axisLabelFontSizePixels': 12,
                u'axisLabelFontFamily': 'Verdana, Arial',
                u'axisLabelPadding': 10,
                u'ticks': months
            },
            u'yaxis': {
                u'axisLabel': _("Days since revision to publication"),
                u'axisLabelUseCanvas': True,
                u'axisLabelFontSizePixels': 12,
                u'axisLabelFontFamily': 'Verdana, Arial',
                u'axisLabelPadding': 3
            },
            u'legend': {
                u'noColumns': 0,
                u'labelBoxBorderColor': "#000000",
                u'position': "nw"
            },
            u'grid': {
                u'hoverable': True,
                u'borderWidth': 2,
                u'backgroundColor': { u'colors': ["#ffffff", "#EDF5FF"] }
            }
        }

        return {u'options': json.dumps(options), u'dataset': dataset}


    def get_stats2(self):
        """
        Total time(hours) in waiting revision state
        flot-ibr2-placeholder
        """
        waiting_revision_state = State.objects.get(pk=State.STATE_AGUARDANDO_REVISAO)
        revision_state = State.objects.get(pk=State.STATE_EM_REVISAO)

        search_form = CTIndicatorDateSearchForm(self.request.GET or None)
        medias = []
        dias = 0
        cts = 0
        hours = 0
        months = []
        if search_form.is_valid():
            datefirst = search_form.cleaned_data.get(u'date_first', None)
            datelast = search_form.cleaned_data.get(u'date_last', None)

            state_history = StateObjectHistory.objects.filter(update_date__gt=datefirst, update_date__lt=datelast, state_id=revision_state.pk).values_list('pk', flat=True)

            datefirst.replace(day=1)
            current = datefirst
            while current <= datelast:
                med = 0
                cts = 0
                dias = 0
                hours = 0
                historys_mes = StateObjectHistory.objects.filter(pk__in=state_history, update_date__month=current.month, update_date__year=current.year).values_list('content_id', flat=True)
                clinical_trials_pks = ClinicalTrial.objects.filter(code__isnull=False, pk__in=historys_mes)
                for ct in clinical_trials_pks:
                    history = StateObjectHistory.objects.filter(content_id=ct.pk)
                    if history:
                        datetime_in_revision = None
                        datetime_before_revision = None
                        date = history.filter(state=revision_state).dates('update_date', 'day', order='DESC')
                        datetime_in_revision = date[0]
                        cts += 1
                        if datetime_before_revision == None:
                            dates_arevis = history.filter(state=waiting_revision_state).dates('update_date', 'day', order='DESC')

                            if dates_arevis:
                                datetime_before_revision = dates_arevis[0]
                        dias += (datetime_in_revision - datetime_before_revision).days
                        if datetime_in_revision and datetime_before_revision and not dias:
                            hours += ((datetime_in_revision - datetime_before_revision).seconds)/60/60

                if cts:
                    med = (dias+hours)/cts
                    medias.append((current.month, med))
                    months.append((current.month, '%s/%s' %(current.month, current.year)))


                current += relativedelta(months=1)

        dataset = json.dumps([{ u'label': _(u'Indicators by clinical trial'),
                     u'data': medias,
                     u'color': "#5482FF" }]);

        options = {
            u'series': {
                u'bars': {
                    u'show': True
                }
            },
            u'bars': {
                u'align': "center",
                u'barWidth': 0.5
            },
            u'xaxis': {
                u'axisLabel': _("Months"),
                u'axisLabelUseCanvas': True,
                u'axisLabelFontSizePixels': 12,
                u'axisLabelFontFamily': 'Verdana, Arial',
                u'axisLabelPadding': 10,
                u'ticks': months
            },
            u'yaxis': {
                u'axisLabel': _("Hours of waiting revision state"),
                u'axisLabelUseCanvas': True,
                u'axisLabelFontSizePixels': 12,
                u'axisLabelFontFamily': 'Verdana, Arial',
                u'axisLabelPadding': 3
            },
            u'legend': {
                u'noColumns': 0,
                u'labelBoxBorderColor': "#000000",
                u'position': "nw"
            },
            u'grid': {
                u'hoverable': True,
                u'borderWidth': 2,
                u'backgroundColor': { u'colors': ["#ffffff", "#EDF5FF"] }
            }
        }

        return {u'options': json.dumps(options), u'dataset': dataset}

    def get_stats3(self):
        """
        Total time(hours) in revision state
        flot-ibr3-placeholder
        """
        published_state = State.objects.get(pk=State.STATE_PUBLICADO)
        revision_state = State.objects.get(pk=State.STATE_EM_REVISAO)
        datefirst = datetime.now()
        datelast = datetime.now()

        search_form = CTIndicatorDateSearchForm(self.request.GET or None)
        medias = []
        dias = 0
        cts = 0
        hours = 0
        months = []
        if search_form.is_valid():
            datefirst = search_form.cleaned_data.get(u'date_first', None)
            datelast = search_form.cleaned_data.get(u'date_last', None)

            state_history = StateObjectHistory.objects.filter(update_date__gt=datefirst, update_date__lt=datelast, state_id=published_state.pk).values_list('pk', flat=True)

            datefirst.replace(day=1)
            current = datefirst
            while current <= datelast:
                med = 0
                cts = 0
                hours = 0
                dias = 0
                historys_mes = StateObjectHistory.objects.filter(pk__in=state_history, update_date__month=current.month, update_date__year=current.year).values_list('content_id', flat=True)
                clinical_trials_pks = ClinicalTrial.objects.filter(code__isnull=False, pk__in=historys_mes)
                for ct in clinical_trials_pks:
                    history = StateObjectHistory.objects.filter(content_id=ct.pk)
                    if history:
                        datetime_in_revision = None
                        datetime_before_revision = None
                        date = history.filter(state=published_state).dates('update_date', 'day', order='DESC')
                        datetime_in_revision = date[0]
                        cts += 1
                        if datetime_before_revision == None:
                            dates_arevis = history.filter(state=revision_state).dates('update_date', 'day', order='DESC')
                            datetime_before_revision = dates_arevis[0]

                        dias += (datetime_in_revision - datetime_before_revision).days
                        if datetime_in_revision and datetime_before_revision and not dias:
                            hours += ((datetime_in_revision - datetime_before_revision).seconds)/60/60
                if cts:
                    med = (dias+hours)/cts
                    if not med:
                        med = 0.5
                    medias.append((current.month, med))
                    months.append((current.month, '%s/%s' %(current.month, current.year)))

                current += relativedelta(months=1)

        dataset = json.dumps([{ u'label': _(u'Indicators by clinical trial'),
                     u'data': medias,
                     u'color': "#5482FF" }]);

        options = {
            u'series': {
                u'bars': {
                    u'show': True
                }
            },
            u'bars': {
                u'align': "center",
                u'barWidth': 0.5
            },
            u'xaxis': {
                u'axisLabel': _("Months"),
                u'axisLabelFontSizePixels': 12,
                u'axisLabelFontFamily': 'Verdana, Arial',
                u'axisLabelPadding': 10,
                u'ticks': months
            },
            u'yaxis': {
                u'axisLabel': _("Hours of waiting revision state"),
                u'axisLabelUseCanvas': True,
                u'axisLabelFontSizePixels': 12,
                u'axisLabelFontFamily': 'Verdana, Arial',
                u'axisLabelPadding': 3
            },
            u'legend': {
                u'noColumns': 0,
                u'labelBoxBorderColor': "#000000",
                u'position': "nw"
            },
            u'grid': {
                u'hoverable': True,
                u'borderWidth': 2,
                u'backgroundColor': { u'colors': ["#ffffff", "#EDF5FF"] }
            }
        }

        return {u'options': json.dumps(options), u'dataset': dataset}



    def get_context_data(self):
        context = super(IndicatorsByClinicalTrialView, self).get_context_data()
        context[u'search_form'] = CTIndicatorDateSearchForm(
            self.request.GET)
        context[u'cts_stats1'] = self.get_stats1()
        context[u'cts_stats2'] = self.get_stats2()
        context[u'cts_stats3'] = self.get_stats3()
        return context

class IndicatorsByReviserView(TemplateView):
    template_name = \
        'administration/administration/indicators_by_reviser.html'


    def get_stats1(self):
        """
        Time(hours) since first revision
        flot-ibr1-placeholder
        """
        published_state = State.objects.get(pk=State.STATE_PUBLICADO)
        revision_state = State.objects.get(pk=State.STATE_EM_REVISAO)
        clinical_trials_pks = ClinicalTrial.objects.filter(
            code__isnull=False).values_list('pk', flat=True)
        revisers_times = {}
        for ct in clinical_trials_pks:
            history = StateObjectHistory.objects.filter(
                content_id=ct).order_by(u'update_date')
            datetime_in_revision = None
            datetime_after_revision = None
            for cont, hist in enumerate(history, start=1):
                if hist.state_id == revision_state.pk and datetime_in_revision is None:
                    datetime_in_revision = hist.update_date.replace(tzinfo=None)

                if hist.state_id == published_state.pk and datetime_in_revision is not None:
                    datetime_after_revision = hist.update_date.replace(tzinfo=None)

                if datetime_in_revision is not None and \
                   datetime_after_revision is not None:
                    if not revisers_times.has_key(hist.user_id):
                        revisers_times[hist.user_id] = []
                    revisers_times[hist.user_id].append(
                        (datetime_after_revision - datetime_in_revision).total_seconds()
                    )
                    break

        revisers_stats = {}
        for rev, rev_times in revisers_times.items():
            revisers_stats[User.objects.get(pk=rev)] = (sum(rev_times) / (len(rev_times) or 1))/60/60

        #from IPython import embed;embed()
        data = []
        ticks = []
        for c, i in enumerate(revisers_stats.items()):
            data.append((c, i[1]))
            ticks.append((c, i[0].get_full_name() or i[0].username))

        dataset = json.dumps([{ u'label': _(u'Indicators by reviser'),
                     u'data': data,
                     u'color': "#5482FF" }]);

        options = {
            u'series': {
                u'bars': {
                    u'show': True
                }
            },
            u'bars': {
                u'align': "center",
                u'barWidth': 0.5
            },
            u'xaxis': {
                u'axisLabel': _("Reviser"),
                u'axisLabelUseCanvas': True,
                u'axisLabelFontSizePixels': 12,
                u'axisLabelFontFamily': 'Verdana, Arial',
                u'axisLabelPadding': 10,
                u'ticks': ticks
            },
            u'yaxis': {
                u'axisLabel': _("Hours since revision to publication"),
                u'axisLabelUseCanvas': True,
                u'axisLabelFontSizePixels': 12,
                u'axisLabelFontFamily': 'Verdana, Arial',
                u'axisLabelPadding': 3
            },
            u'legend': {
                u'noColumns': 0,
                u'labelBoxBorderColor': "#000000",
                u'position': "nw"
            },
            u'grid': {
                u'hoverable': True,
                u'borderWidth': 2,
                u'backgroundColor': { u'colors': ["#ffffff", "#EDF5FF"] }
            }
        }

        return {u'options': json.dumps(options), u'dataset': dataset}

    def get_stats2(self):
        """
        Total time(hours) in waiting revision state
        flot-ibr2-placeholder
        """
        waiting_revision_state = State.objects.get(pk=State.STATE_AGUARDANDO_REVISAO)
        clinical_trials_pks = ClinicalTrial.objects.filter(
            code__isnull=False).values_list('pk', flat=True)
        revisers_times = {}
        for ct in clinical_trials_pks:
            history = StateObjectHistory.objects.filter(
                content_id=ct).order_by(u'update_date')
            datetime_in_wrevision = None
            datetime_after_wrevision = None
            for cont, hist in enumerate(history, start=1):
                if datetime_in_wrevision is not None:
                    datetime_after_wrevision = hist.update_date.replace(tzinfo=None)
                    reviser_user = hist.user_id
                    if not revisers_times.has_key(reviser_user):
                        revisers_times[reviser_user] = []

                if hist.state_id == waiting_revision_state.pk:
                    datetime_in_wrevision = hist.update_date.replace(tzinfo=None)

                if datetime_in_wrevision is not None and cont == len(history):
                    datetime_after_wrevision = datetime.now().replace(tzinfo=None)
                    reviser_user = ClinicalTrial.objects.get(pk=ct).user.pk
                    if not revisers_times.has_key(reviser_user):
                        revisers_times[reviser_user] = []

                if datetime_in_wrevision is not None and \
                   datetime_after_wrevision is not None:
                    revisers_times[reviser_user].append(
                        (datetime_after_wrevision - datetime_in_wrevision).total_seconds()
                    )
                    datetime_in_wrevision = None
                    datetime_after_wrevision = None

        revisers_stats = {}
        for rev, rev_times in revisers_times.items():
            revisers_stats[User.objects.get(pk=rev)] = (sum(rev_times) / (len(rev_times) or 1))/60/60

        #from IPython import embed;embed()
        data = []
        ticks = []
        for c, i in enumerate(revisers_stats.items()):
            data.append((c, i[1]))
            ticks.append((c, i[0].get_full_name() or i[0].username))

        dataset = json.dumps([{ u'label': _(u'Indicators by reviser'),
                     u'data': data,
                     u'color': "#5482FF" }]);

        options = {
            u'series': {
                u'bars': {
                    u'show': True
                }
            },
            u'bars': {
                u'align': "center",
                u'barWidth': 0.5
            },
            u'xaxis': {
                u'axisLabel': _("Reviser"),
                u'axisLabelUseCanvas': True,
                u'axisLabelFontSizePixels': 12,
                u'axisLabelFontFamily': 'Verdana, Arial',
                u'axisLabelPadding': 10,
                u'ticks': ticks
            },
            u'yaxis': {
                u'axisLabel': _("Hours of waiting revision state"),
                u'axisLabelUseCanvas': True,
                u'axisLabelFontSizePixels': 12,
                u'axisLabelFontFamily': 'Verdana, Arial',
                u'axisLabelPadding': 3
            },
            u'legend': {
                u'noColumns': 0,
                u'labelBoxBorderColor': "#000000",
                u'position': "nw"
            },
            u'grid': {
                u'hoverable': True,
                u'borderWidth': 2,
                u'backgroundColor': { u'colors': ["#ffffff", "#EDF5FF"] }
            }
        }

        return {u'options': json.dumps(options), u'dataset': dataset}

    def get_stats3(self):
        """
        Total time(hours) in revision state
        flot-ibr3-placeholder
        """
        revision_state = State.objects.get(pk=State.STATE_EM_REVISAO)
        clinical_trials_pks = ClinicalTrial.objects.filter(
            code__isnull=False).values_list('pk', flat=True)
        revisers_times = {}
        for ct in clinical_trials_pks:
            history = StateObjectHistory.objects.filter(
                content_id=ct).order_by(u'update_date')
            datetime_in_revision = None
            datetime_after_revision = None
            for cont, hist in enumerate(history, start=1):
                if datetime_in_revision is not None:
                    datetime_after_revision = hist.update_date.replace(tzinfo=None)
                    reviser_user = hist.user_id
                    if not revisers_times.has_key(reviser_user):
                        revisers_times[reviser_user] = []

                if hist.state_id == revision_state.pk:
                    datetime_in_revision = hist.update_date.replace(tzinfo=None)

                if datetime_in_revision is not None and \
                     datetime_after_revision is None and cont == len(history):
                    datetime_after_revision = datetime.now().replace(tzinfo=None)
                    reviser_user = ClinicalTrial.objects.get(pk=ct).user.pk
                    if not revisers_times.has_key(reviser_user):
                        revisers_times[reviser_user] = []

                if datetime_in_revision is not None and \
                   datetime_after_revision is not None:
                    revisers_times[reviser_user].append(
                        (datetime_after_revision - datetime_in_revision).total_seconds()
                    )
                    datetime_in_revision = None
                    datetime_after_revision = None

        revisers_stats = {}
        for rev, rev_times in revisers_times.items():
            revisers_stats[User.objects.get(pk=rev)] = (sum(rev_times) / (len(rev_times) or 1))/60/60

        #from IPython import embed;embed()
        data = []
        ticks = []
        for c, i in enumerate(revisers_stats.items()):
            data.append((c, i[1]))
            ticks.append((c, i[0].get_full_name() or i[0].username))

        dataset = json.dumps([{ u'label': _(u'Indicators by reviser'),
                     u'data': data,
                     u'color': "#5482FF" }]);

        options = {
            u'series': {
                u'bars': {
                    u'show': True
                }
            },
            u'bars': {
                u'align': "center",
                u'barWidth': 0.5
            },
            u'xaxis': {
                u'axisLabel': _("Reviser"),
                u'axisLabelUseCanvas': True,
                u'axisLabelFontSizePixels': 12,
                u'axisLabelFontFamily': 'Verdana, Arial',
                u'axisLabelPadding': 10,
                u'ticks': ticks
            },
            u'yaxis': {
                u'axisLabel': _("Hours of waiting revision state"),
                u'axisLabelUseCanvas': True,
                u'axisLabelFontSizePixels': 12,
                u'axisLabelFontFamily': 'Verdana, Arial',
                u'axisLabelPadding': 3
            },
            u'legend': {
                u'noColumns': 0,
                u'labelBoxBorderColor': "#000000",
                u'position': "nw"
            },
            u'grid': {
                u'hoverable': True,
                u'borderWidth': 2,
                u'backgroundColor': { u'colors': ["#ffffff", "#EDF5FF"] }
            }
        }

        return {u'options': json.dumps(options), u'dataset': dataset}



    def get_context_data(self):
        context = super(IndicatorsByReviserView, self).get_context_data()
        context[u'revisers_stats1'] = self.get_stats1()
        context[u'revisers_stats2'] = self.get_stats2()
        context[u'revisers_stats3'] = self.get_stats3()
        return context

class IndicatorsByEvaluatorView(TemplateView):
    template_name = \
        'administration/administration/indicators_by_evaluator.html'

    def get_stats2(self):
        """
        Total time(hours) in waiting evaluation state
        flot-ibe2-placeholder
        """
        waiting_evaluation_state = State.objects.get(pk=State.STATE_AGUARDANDO_AVALIACAO)
        clinical_trials_pks = ClinicalTrial.objects.filter(
            code__isnull=False).values_list('pk', flat=True)
        revisers_times = {}
        for ct in clinical_trials_pks:
            history = StateObjectHistory.objects.filter(
                content_id=ct).order_by(u'update_date')
            datetime_in_wrevision = None
            datetime_after_wrevision = None
            for cont, hist in enumerate(history, start=1):
                if datetime_in_wrevision is not None:
                    datetime_after_wrevision = hist.update_date.replace(tzinfo=None)
                    reviser_user = hist.user_id
                    if not revisers_times.has_key(reviser_user):
                        revisers_times[reviser_user] = []

                if hist.state_id == waiting_evaluation_state.pk:
                    datetime_in_wrevision = hist.update_date.replace(tzinfo=None)

                if datetime_in_wrevision is not None and cont == len(history):
                    datetime_after_wrevision = datetime.now().replace(tzinfo=None)
                    reviser_user = ClinicalTrial.objects.get(pk=ct).user.pk
                    if not revisers_times.has_key(reviser_user):
                        revisers_times[reviser_user] = []

                if datetime_in_wrevision is not None and \
                   datetime_after_wrevision is not None:
                    revisers_times[reviser_user].append(
                        (datetime_after_wrevision - datetime_in_wrevision).total_seconds()
                    )
                    datetime_in_wrevision = None
                    datetime_after_wrevision = None

        revisers_stats = {}
        for rev, rev_times in revisers_times.items():
            revisers_stats[User.objects.get(pk=rev)] = (sum(rev_times) / (len(rev_times) or 1))/60/60

        #from IPython import embed;embed()
        data = []
        ticks = []
        for c, i in enumerate(revisers_stats.items()):
            data.append((c, i[1]))
            ticks.append((c, i[0].get_full_name() or i[0].username))

        dataset = json.dumps([{ u'label': _(u'Indicators by evaluator'),
                     u'data': data,
                     u'color': "#5482FF" }]);

        options = {
            u'series': {
                u'bars': {
                    u'show': True
                }
            },
            u'bars': {
                u'align': "center",
                u'barWidth': 0.5
            },
            u'xaxis': {
                u'axisLabel': _("Evaluator"),
                u'axisLabelUseCanvas': True,
                u'axisLabelFontSizePixels': 12,
                u'axisLabelFontFamily': 'Verdana, Arial',
                u'axisLabelPadding': 10,
                u'ticks': ticks
            },
            u'yaxis': {
                u'axisLabel': _("Hours of waiting evaluation state"),
                u'axisLabelUseCanvas': True,
                u'axisLabelFontSizePixels': 12,
                u'axisLabelFontFamily': 'Verdana, Arial',
                u'axisLabelPadding': 3
            },
            u'legend': {
                u'noColumns': 0,
                u'labelBoxBorderColor': "#000000",
                u'position': "nw"
            },
            u'grid': {
                u'hoverable': True,
                u'borderWidth': 2,
                u'backgroundColor': { u'colors': ["#ffffff", "#EDF5FF"] }
            }
        }

        return {u'options': json.dumps(options), u'dataset': dataset}

    def get_stats3(self):
        """
        Total time(hours) in evaluation state
        flot-ibe3-placeholder
        """
        revision_state = State.objects.get(pk=State.STATE_EM_AVALIACAO)
        clinical_trials_pks = ClinicalTrial.objects.filter(
            code__isnull=False).values_list('pk', flat=True)
        revisers_times = {}
        for ct in clinical_trials_pks:
            history = StateObjectHistory.objects.filter(
                content_id=ct).order_by(u'update_date')
            datetime_in_revision = None
            datetime_after_revision = None
            for cont, hist in enumerate(history, start=1):
                if datetime_in_revision is not None:
                    datetime_after_revision = hist.update_date.replace(tzinfo=None)
                    reviser_user = hist.user_id
                    if not revisers_times.has_key(reviser_user):
                        revisers_times[reviser_user] = []

                if hist.state_id == revision_state.pk:
                    datetime_in_revision = hist.update_date.replace(tzinfo=None)

                if datetime_in_revision is not None and cont == len(history):
                    datetime_after_revision = datetime.now().replace(tzinfo=None)
                    reviser_user = ClinicalTrial.objects.get(pk=ct).user.pk
                    if not revisers_times.has_key(reviser_user):
                        revisers_times[reviser_user] = []

                if datetime_in_revision is not None and \
                   datetime_after_revision is not None:
                    revisers_times[reviser_user].append(
                        (datetime_after_revision - datetime_in_revision).total_seconds()
                    )
                    datetime_in_revision = None
                    datetime_after_revision = None

        revisers_stats = {}
        for rev, rev_times in revisers_times.items():
            revisers_stats[User.objects.get(pk=rev)] = (sum(rev_times) / (len(rev_times) or 1))/60/60

        #from IPython import embed;embed()
        data = []
        ticks = []
        for c, i in enumerate(revisers_stats.items()):
            data.append((c, i[1]))
            ticks.append((c, i[0].get_full_name() or i[0].username))

        dataset = json.dumps([{ u'label': _(u'Indicators by evaluator'),
                     u'data': data,
                     u'color': "#5482FF" }]);

        options = {
            u'series': {
                u'bars': {
                    u'show': True
                }
            },
            u'bars': {
                u'align': "center",
                u'barWidth': 0.5
            },
            u'xaxis': {
                u'axisLabel': _("Evaluator"),
                u'axisLabelUseCanvas': True,
                u'axisLabelFontSizePixels': 12,
                u'axisLabelFontFamily': 'Verdana, Arial',
                u'axisLabelPadding': 10,
                u'ticks': ticks
            },
            u'yaxis': {
                u'axisLabel': _("Hours of waiting evaluation state"),
                u'axisLabelUseCanvas': True,
                u'axisLabelFontSizePixels': 12,
                u'axisLabelFontFamily': 'Verdana, Arial',
                u'axisLabelPadding': 3
            },
            u'legend': {
                u'noColumns': 0,
                u'labelBoxBorderColor': "#000000",
                u'position': "nw"
            },
            u'grid': {
                u'hoverable': True,
                u'borderWidth': 2,
                u'backgroundColor': { u'colors': ["#ffffff", "#EDF5FF"] }
            }
        }

        return {u'options': json.dumps(options), u'dataset': dataset}


    def get_context_data(self):
        context = super(IndicatorsByEvaluatorView, self).get_context_data()
        context[u'evaluators_stats2'] = self.get_stats2()
        context[u'evaluators_stats3'] = self.get_stats3()
        return context


class StateChangeEmailTemplates(ListView):
    model = StateChangeEmailTemplate
    template_name = 'administration/administration/emailtemplate_list.html'
    permissions_required = ['admin.administrador_permission']


class StateChangeEmailTemplatesAdd(CreateView):
    model = StateChangeEmailTemplate
    template_name = 'base/form_view.html'
    permissions_required = ['admin.administrador_permission']

    def get_success_url(self):
        messages.success(self.request, __(u'Email template successfully created.'))
        return reverse_lazy(u'state_change_email_templates')


class StateChangeEmailTemplatesChange(UpdateView):
    model = StateChangeEmailTemplate
    template_name = 'base/form_view.html'
    permissions_required = ['admin.administrador_permission']

    def get_success_url(self):
        messages.success(self.request, __(u'Email template successfully changed.'))
        return reverse_lazy(u'state_change_email_templates')

class StateChangeEmailTemplatesDelete(DeleteView):
    model = StateChangeEmailTemplate
    template_name = 'base/delete_view.html'
    permissions_required = ['admin.administrador_permission']

    def get_success_url(self):
        messages.success(self.request, __(u'Email template successfully deleted.'))
        return reverse_lazy(u'state_change_email_templates')


class IndicatorsClinicalTrialsPublished(TemplateView):
    """Indicators of published clinical trials, is done by the calculation period."""

    template_name = 'administration/administration/indicators_by_ctpublished.html'
    permissions_required = ['admin.manager_permission']

    def get_context_data(self, **kwargs):
        context = super(IndicatorsClinicalTrialsPublished, self).get_context_data(
            **kwargs)

        context[u'search_form'] = CTIndicatorPublishedFilterForm(
            self.request.GET)

        context[u'cts_table1'] = self.get_tables()

        return context

    def get_intervention(self, cts, pageFlag=False):
        intervations = InterventionCode.objects.all()
        dict_intervation = {}
        list_intervation = []
        ct_intervation = 0

        for i in intervations:
            ct_intervation = cts.filter(intervention_codes=i).count()
            if ct_intervation:
                dict_intervation[i.description] = ct_intervation

        list_intervation = (sorted(dict_intervation.items(), key=lambda elem: elem[1], reverse=True))
        if pageFlag:
            paginator = Paginator(list_intervation, 20)
            page = self.request.GET.get(u'page')
            try:
                interventions_pages = paginator.page(page)
            except PageNotAnInteger:
                # If page is not an integer, deliver first page.
                interventions_pages = paginator.page(1)
            except EmptyPage:
                # If page is out of range (e.g. 9999), deliver last page of results.
                interventions_pages = paginator.page(paginator.num_pages)

            back = True
            return {u'list_intervation': interventions_pages,
                    u'pages':interventions_pages,
                    u'back': back}
        else:
            return list_intervation[:10]

    def get_institution(self, cts, pageFlag=False):
        sponsors = Sponsor.objects.filter(sponsor_type='P', clinical_trial__in=cts).distinct('pk')
        list_institution = []
        dict_institution = {}
        qnt_cts = 0
        for inst in sponsors:
            qnt_cts = inst.institution.sponsor_items.filter(sponsor_type='P', clinical_trial__in=cts).count()
            if qnt_cts:
                dict_institution[inst.institution.name] = qnt_cts

        list_institution = (sorted(dict_institution.items(), key=lambda elem: elem[1], reverse=True))
        if pageFlag:
            paginator = Paginator(list_institution, 20)
            page = self.request.GET.get(u'page')
            try:
                institution_pages = paginator.page(page)
            except PageNotAnInteger:
                # If page is not an integer, deliver first page.
                institution_pages = paginator.page(1)
            except EmptyPage:
                # If page is out of range (e.g. 9999), deliver last page of results.
                institution_pages = paginator.page(paginator.num_pages)

            back = True
            return {u'list_institution': institution_pages,
                    u'pages':institution_pages,
                    u'back': back}
        else:
            return list_institution[:10]

    def get_country(self, cts, pageFlag=False):
        sponsors = Sponsor.objects.filter(sponsor_type='P', clinical_trial__in=cts).distinct('pk')
        institutions = []
        dict_country = {}
        list_country = []
        countrys = []
        for sponsor in sponsors:
            if (sponsor.institution.country) not in countrys:
                countrys.append(sponsor.institution.country)
            if (sponsor.institution) not in institutions:
                institutions.append(sponsor.institution.pk)

        qnt_cts_country = 0;
        for country in countrys:
            institution_coutry = Institution.objects.filter(pk__in=institutions, country=country)
            for i in institution_coutry:
                qnt_cts_country += Sponsor.objects.filter(sponsor_type='P', clinical_trial__in=cts, institution=i).count()

                if qnt_cts_country:
                    dict_country[country] = qnt_cts_country

        list_country = (sorted(dict_country.items(), key=lambda elem: elem[1], reverse=True))
        if pageFlag:
            paginator = Paginator(list_country, 20)
            page = self.request.GET.get(u'page')
            try:
                country_pages = paginator.page(page)
            except PageNotAnInteger:
                # If page is not an integer, deliver first page.
                country_pages = paginator.page(1)
            except EmptyPage:
                # If page is out of range (e.g. 9999), deliver last page of results.
                country_pages = paginator.page(paginator.num_pages)

            back = True
            return {u'list_country': country_pages,
                    u'pages': country_pages,
                    u'back': back}
        else:
            return list_country[:10]

    def get_uf(self, cts, pageFlag=False):
        # Quantidade de cts publicados por estado
        sponsors = Sponsor.objects.filter(sponsor_type='P', clinical_trial__in=cts).distinct('pk')
        institutions = []
        dict_uf = {}
        list_uf = []
        qnt_cts_uf = 0
        stats = []
        for sponsor in sponsors:
            state = sponsor.institution.state
            if state:
                if (sponsor.institution) not in institutions:
                    institutions.append(sponsor.institution.pk)
                if state not in stats:
                    stats.append(state)

        for uf in stats:
            qnt_cts_uf = 0
            intitutions_uf = Institution.objects.filter(state=uf, pk__in=institutions)
            for i in intitutions_uf:
                qnt_cts_uf += Sponsor.objects.filter(sponsor_type='P', clinical_trial__in=cts, institution=i).distinct('pk').count()
            if qnt_cts_uf:
                dict_uf[uf] = qnt_cts_uf

        list_uf = (sorted(dict_uf.items(), key=lambda elem: elem[1], reverse=True))
        if pageFlag:
            paginator = Paginator(list_uf, 20)
            page = self.request.GET.get(u'page')
            try:
                uf_pages = paginator.page(page)
            except PageNotAnInteger:
                # If page is not an integer, deliver first page.
                uf_pages = paginator.page(1)
            except EmptyPage:
                # If page is out of range (e.g. 9999), deliver last page of results.
                uf_pages = paginator.page(paginator.num_pages)

            back = True
            return {u'list_uf': uf_pages,
                    u'pages': uf_pages,
                    u'back': back}
        else:
            return list_uf[:10]

    def get_tables(self):
        """Quantidade de estudos observacionais e intervenção"""
        search_form = CTIndicatorPublishedFilterForm(self.request.GET or None)
        observation = 0
        intervention = 0
        list_intervation = []
        list_institution = []
        list_country = []
        list_uf = []
        see_all = False

        if search_form.is_valid():
            published_state = State.objects.get(pk=State.STATE_PUBLICADO)
            datefirst = search_form.cleaned_data.get(u'date_first', None)
            datelast = search_form.cleaned_data.get(u'date_last', None)
            if datefirst and datelast:
                state_history = StateObjectHistory.objects.filter(update_date__gt=datefirst, update_date__lt=datelast, state_id=published_state.pk).values_list('content_id', flat=True)
                cts = ClinicalTrial.objects.filter(pk__in=state_history)

                if self.request.GET.has_key("query_all"):
                    # Quantidade de Clinical Trial de observação e intervenção
                    observation = 0
                    intervention = 0
                    for ct in cts:
                        if ct.study_type == 'I':
                            intervention += 1
                        elif ct.study_type == 'O':
                            observation += 1
                    list_intervation = self.get_intervention(cts)
                    list_institution = self.get_institution(cts)
                    list_country = self.get_country(cts)
                    list_uf = self.get_uf(cts)
                    see_all = True

                elif self.request.GET.has_key("query_intervation"):
                    return self.get_intervention(cts, True)
                elif self.request.GET.has_key("query_institution"):
                    return self.get_institution(cts, True)
                elif self.request.GET.has_key("query_country"):
                    return self.get_country(cts, True)
                elif self.request.GET.has_key("query_state"):
                    return self.get_uf(cts, True)

        return {u'observation': observation,
                u'intervention': intervention,
                u'list_intervation': list_intervation,
                u'list_institution': list_institution,
                u'list_country': list_country,
                u'list_uf': list_uf,
                u'see_all': see_all}
