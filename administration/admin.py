# -*- coding: utf-8 -*-
from django.contrib import admin
from administration.models import *
from django.utils.translation import ugettext_lazy as _


class CountryAdmin(admin.ModelAdmin):
    """Country admin class."""
    pass


class InstitutionTypeAdmin(admin.ModelAdmin):
    """InstitutionType admin class."""
    pass


class InstitutionAdmin(admin.ModelAdmin):
    """Institution admin class."""
    list_display = (
        'name', 'postal_address', 'city', 'state',
        'country', 'institution_type', 'status')
    list_display_links = (
        'name', 'postal_address', 'city', 'state',
        'country', 'institution_type', 'status')
    list_filter = ['status']
    search_fields = ('name',)

    fieldsets = (
        (_('Institution'), {'fields': (
            'name', 'postal_address', 'city', 'state',
            'country', 'institution_type', 'status')}),
        )

    def get_actions(self, request):
        """Remove delete action."""
        actions = super(InstitutionAdmin, self).get_actions(request)
        del actions['delete_selected']
        return actions


class SetupAdmin(admin.ModelAdmin):
    """Setup admin class."""
    pass


class SetupIdentifierAdmin(admin.ModelAdmin):
    """SetupIdentifier admin class."""
    pass


class CTGroupAdmin(admin.ModelAdmin):
    """CTGroup admin class."""
    pass


class CTGroupUserAdmin(admin.ModelAdmin):
    """CTGroupUser admin class."""
    pass


class StudyPurposeAdmin(admin.ModelAdmin):
    """Study Purpose admin class."""
    pass


class InterventionAssignmentAdmin(admin.ModelAdmin):
    """Intervention Assignment admin class."""
    pass


class MaskingTypeAdmin(admin.ModelAdmin):
    """Masking Type Admin class."""
    pass


class AllocationTypeAdmin(admin.ModelAdmin):
    """Allocation Type Admin class."""
    pass


class StudyPhaseAdmin(admin.ModelAdmin):
    """Study Phase Admin class."""
    pass


class ObservationStudyDesignAdmin(admin.ModelAdmin):
    """Observation Study Design Admin class."""
    pass


class TimePerspectiveAdmin(admin.ModelAdmin):
    """Time Perspective Admin class."""
    pass


class InterventionCodeAdmin(admin.ModelAdmin):
    """Intervention Code Admin class."""
    pass


class AttachmentAdmin(admin.ModelAdmin):
    """Attachment Admin class."""
    pass

class StateDaysLimitAdmin(admin.ModelAdmin):
    """ Admin class."""
    pass

class HelpAdmin(admin.ModelAdmin):
    """ Admin class."""
    pass

class ItemsHelpInlineAdmin(admin.TabularInline):
    model = ItemsHelps
    extra = 0

class HelpLanguageAdmin(admin.ModelAdmin):
    inlines = [ItemsHelpInlineAdmin]

admin.site.register(Country, CountryAdmin)

admin.site.register(InstitutionType, InstitutionTypeAdmin)

admin.site.register(Institution, InstitutionAdmin)

admin.site.register(Setup, SetupAdmin)

admin.site.register(SetupIdentifier, SetupIdentifierAdmin)

admin.site.register(CTGroup, CTGroupAdmin)

admin.site.register(CTGroupUser, CTGroupUserAdmin)

admin.site.register(StudyPurpose, StudyPurposeAdmin)

admin.site.register(InterventionAssignment, InterventionAssignmentAdmin)

admin.site.register(MaskingType, MaskingTypeAdmin)

admin.site.register(AllocationType, AllocationTypeAdmin)

admin.site.register(StudyPhase, StudyPhaseAdmin)

admin.site.register(ObservationStudyDesign, ObservationStudyDesignAdmin)

admin.site.register(TimePerspective, TimePerspectiveAdmin)

admin.site.register(InterventionCode, InterventionCodeAdmin)

admin.site.register(Attachment, AttachmentAdmin)

admin.site.register(StateDaysLimit, StateDaysLimitAdmin)

admin.site.register(Help, HelpAdmin)

admin.site.register(HelpLanguage, HelpLanguageAdmin)
