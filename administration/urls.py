# -*- coding: utf-8 -*-

from django.conf.urls import patterns, include, url
from django.contrib.auth.decorators import login_required, permission_required
from administration import views, models
from administration.views import HelpView
from django.views.generic import TemplateView


urlpatterns = patterns(
    '',
    url(
        r'^$',
        login_required(
            TemplateView.as_view(
                template_name='administration/administration/administration_index.html')),
        name='administration'),
    url(
        r'occupation-management/$',
        login_required(
            views.UsersOccupationManageView.as_view()),
        name='occupation_management'),
    url(
        r'clinical-trials-by-revisor-and-state/$',
        login_required(
            views.ClinicalTrialsByRevisorAndStateView.as_view()),
        name='ct_by_revisor_and_state'),
    url(
        r'clinical-trials-by-evaluator-and-state/$',
        login_required(
            views.ClinicalTrialsByEvaluatorAndStateView.as_view()),
        name='ct_by_evaluator_and_state'),
    url(
        r'occupation-management-list-cts/(?P<pk>\d+)/$',
        login_required(
            views.UsersOccupationManageListCTsView.as_view()),
        name='occupation_management_list_user_cts'),
    url(
        r'recruitment-interrupt/$',
        login_required(
            views.RecruitmentInterruptListView.as_view()),
        name='recruitment_interrupt_list'),
    url(
        r'recruitment-interrupt/(?P<pk>\d+)/$',
        login_required(
            views.RecruitmentInterruptView.as_view()),
        name='recruitment_interrupt'),
    url(
        r'recruitment-interruption-history/(?P<pk>\d+)/$',
        login_required(
            views.RecruitmentInterruptionHistoryView.as_view()),
        name='recruitment_interruption_history'),

    url(
        r'contacts/manage/(?P<pk>\d+)/$',
        login_required(
            views.ContactManageEditView.as_view()),
        name='contact_manage_edit'),

    url(
        r'country/add$',
        login_required(
            views.CountryCreate.as_view()),
        name='country_add'),

    url(
        r'country/(?P<pk>\w+)$',
        login_required(
            views.CountryDetail.as_view()),
        name='country_detail'),

    url(
        r'country/(?P<pk>\w+)/edit$',
        login_required(
            views.CountryUpdate.as_view()),
        name='country_edit'),

    url(
        r'country/(?P<pk>\w+)/delete$',
        login_required(
            views.CountryDelete.as_view()),
        name='country_delete'),

    url(
        r'country/',
        login_required(
            views.CountryList.as_view()),
        name='country_list'),

    url(
        r'institution_type/add$',
        login_required(
            views.InstitutionTypeCreate.as_view()),
        name='institution_type_add'),

    url(
        r'institution_type/(?P<pk>\d+)$',
        login_required(
            views.InstitutionTypeDetail.as_view()),
        name='institution_type_detail'),

    url(
        r'institution_type/(?P<pk>\d+)/edit$',
        login_required(
            views.InstitutionTypeUpdate.as_view()),
        name='institution_type_edit'),

    url(
        r'institution_type/(?P<pk>\d+)/delete$',
        login_required(
            views.InstitutionTypeDelete.as_view()),
        name='institution_type_delete'),

    url(
        r'institution_type/',
        login_required(
            views.InstitutionTypeList.as_view()),
        name='institution_type_list'),

    url(
        r'institution/add$',
        login_required(
            views.InstitutionCreate.as_view()),
        name='institution_add'),

    url(
        r'institution/(?P<clinical_trial_pk>\d+)/add/(?P<sponsor_type>P|S|U)$',
        login_required(
            views.InstitutionCreateSponsor.as_view()),
        name='institution_add_sponsor'),

    url(
        r'institution/(?P<group_pk>\d+)/add/$',
        login_required(
            views.InstitutionCreateForGroup.as_view()),
        name='institution_add_for_group'),

    url(
        r'institution/add/$',
        login_required(
            views.InstitutionCreateForNewGroup.as_view()),
        name='institution_add_for_new_group'),

    url(
        r'institution/(?P<pk>\d+)$',
        login_required(
            views.InstitutionDetail.as_view()),
        name='institution_detail'),

    url(
        r'institution/(?P<pk>\d+)/edit$',
        login_required(
            views.InstitutionUpdate.as_view()),
        name='institution_edit'),

    url(
        r'institution/$',
        login_required(
            views.InstitutionList.as_view()),
        name='institution_list'),

    url(
        r'setup_identifier/add$',
        login_required(
            views.SetupIdentifierCreate.as_view()),
        name='setup_identifier_add'),

    url(
        r'setup_identifier/(?P<pk>\d+)$',
        login_required(
            views.SetupIdentifierDetail.as_view()),
        name='setup_identifier_detail'),

    url(
        r'setup_identifier/(?P<pk>\d+)/edit$',
        login_required(
            views.SetupIdentifierUpdate.as_view()),
        name='setup_identifier_edit'),

    url(
        r'setup_identifier/(?P<pk>\d+)/delete$',
        login_required(views.SetupIdentifierDelete.as_view()),
        name='setup_identifier_delete'),

    url(
        r'setup_identifier/',
        login_required(
            views.SetupIdentifierList.as_view()),
        name='setup_identifier_list'),

    url(
        r'^setup/$',
        login_required(
            views.SetupUpdate.as_view()),
        name='setup'),

    url(
        r'ctgroup/add$',
        login_required(
            views.CreateCTGroup.as_view()),
        name='ctgroup_add'),

    url(
        r'ctgroup/(?P<pk>\d+)/$',
        login_required(
            views.CTGroupDetail.as_view()),
        name='ctgroup_detail'),

    url(
        r'ctgroup/(?P<pk>\d+)/edit$',
        login_required(
            views.UpdateCTGroup.as_view()),
        name='ctgroup_edit'),

    url(
        r'ctgroup/$',
        login_required(
            views.MyGroupsList.as_view()),
        name='ctgroup_list'),

    url(
        r'ctgroup/remove-ct/(?P<ct_id>\d+)/$',
        login_required(
            views.RemoveCTFromGroup.as_view()),
        name='group-adm-remove-ct-from-group'),

    url(
        r'ctgroup/change-ct-owner/(?P<pk>\d+)/$',
        login_required(
            views.ChangeCTOwner.as_view()),
        name='group-adm-change-ct-owner'),

    url(
        r'ctgroup/list/$',
        login_required(
            views.CTGroupList.as_view()),
        name='list_ctgroup'),

    url(
        r'administration/state-change-email-templates/$',
        login_required(
            views.StateChangeEmailTemplates.as_view()),
        name='state_change_email_templates'),

    url(
        r'administration/state-change-email-templates/add/$',
        login_required(
            views.StateChangeEmailTemplatesAdd.as_view()),
        name='state_change_email_templates_add'),

    url(
        r'administration/state-change-email-templates/(?P<pk>\d+)/edit/$',
        login_required(
            views.StateChangeEmailTemplatesChange.as_view()),
        name='state_change_email_templates_change'),

    url(
        r'administration/state-change-email-templates/(?P<pk>\d+)/delete/$',
        login_required(
            views.StateChangeEmailTemplatesDelete.as_view()),
        name='state_change_email_templates_delete'),

    url(
        r'profile/(?P<pk>\d+)/edit$',
        login_required(
            views.UserProfileEdit.as_view()),
        name='user_profile_edit'),

    url(
        r'user/(?P<pk>\w+)$',
        login_required(
            views.UserProfileDetail.as_view()),
        name='user_profile_detail'),


    url(
        r'ctgroup/(?P<pk>\d+)/(?P<decision>S)$',
        login_required(
            views.CTGroupSignOut.as_view()),
        name='ctgroup_sign_out'),

    url(
        r'ctgroup/(?P<pk>\d+)/manager$',
        login_required(
            views.ManagerCTGroup.as_view()),
        name='ctgroup_manager'),

    url(
        r'ctgroup/manager/(?P<pk>\d+)/(?P<decision>C|R)$',
        login_required(
            views.ManagerCTGroupDecision.as_view()),
        name='ctgroup_manager_decision'),

    url(
        r'ctgroup/block/(?P<pk>\d+)/(?P<decision>B|D)$',
        login_required(
            views.ManagerCTGroupBlock.as_view()),
        name='ctgroup_block'),

    url(
        r'study_purpose/add$',
        login_required(
            views.StudyPurposeCreate.as_view()),
        name='study_purpose_add'),

    url(
        r'study_purpose/(?P<pk>\d+)$',
        login_required(
            views.StudyPurposeDetail.as_view()),
        name='study_purpose_detail'),

    url(
        r'study_purpose/(?P<pk>\d+)/edit$',
        login_required(
            views.StudyPurposeUpdate.as_view()),
        name='study_purpose_edit'),

    url(
        r'study_purpose/(?P<pk>\d+)/delete$',
        login_required(
            views.StudyPurposeDelete.as_view()),
        name='study_purpose_delete'),

    url(
        r'study_purpose/',
        login_required(
            views.StudyPurposeList.as_view()),
        name='study_purpose_list'),

    url(
        r'intervention_assignment/add$',
        login_required(
            views.InterventionAssignmentCreate.as_view()),
        name='intervention_assignment_add'),

    url(
        r'intervention_assignment/(?P<pk>\d+)$',
        login_required(
            views.InterventionAssignmentDetail.as_view()),
        name='intervention_assignment_detail'),

    url(
        r'intervention_assignment/(?P<pk>\d+)/edit$',
        login_required(
            views.InterventionAssignmentUpdate.as_view()),
        name='intervention_assignment_edit'),

    url(
        r'intervention_assignment/(?P<pk>\d+)/delete$',
        login_required(
            views.InterventionAssignmentDelete.as_view()),
        name='intervention_assignment_delete'),

    url(
        r'intervention_assignment/',
        login_required(
            views.InterventionAssignmentList.as_view()),
        name='intervention_assignment_list'),

    url(
        r'masking_type/add$',
        login_required(
            views.MaskingTypeCreate.as_view()),
        name='masking_type_add'),

    url(
        r'masking_type/(?P<pk>\d+)$',
        login_required(
            views.MaskingTypeDetail.as_view()),
        name='masking_type_detail'),

    url(
        r'masking_type/(?P<pk>\d+)/edit$',
        login_required(
            views.MaskingTypeUpdate.as_view()),
        name='masking_type_edit'),

    url(
        r'masking_type/(?P<pk>\d+)/delete$',
        login_required(
            views.MaskingTypeDelete.as_view()),
        name='masking_type_delete'),

    url(
        r'masking_type/',
        login_required(
            views.MaskingTypeList.as_view()),
        name='masking_type_list'),

    url(
        r'allocation_type/add$',
        login_required(
            views.AllocationTypeCreate.as_view()),
        name='allocation_type_add'),

    url(
        r'allocation_type/(?P<pk>\d+)$',
        login_required(
            views.AllocationTypeDetail.as_view()),
        name='allocation_type_detail'),

    url(
        r'allocation_type/(?P<pk>\d+)/edit$',
        login_required(
            views.AllocationTypeUpdate.as_view()),
        name='allocation_type_edit'),

    url(
        r'allocation_type/(?P<pk>\d+)/delete$',
        login_required(
            views.AllocationTypeDelete.as_view()),
        name='allocation_type_delete'),

    url(
        r'allocation_type/',
        login_required(
            views.AllocationTypeList.as_view()),
        name='allocation_type_list'),

    url(
        r'study_phase/add$',
        login_required(
            views.StudyPhaseCreate.as_view()),
        name='study_phase_add'),

    url(
        r'study_phase/(?P<pk>\d+)$',
        login_required(
            views.StudyPhaseDetail.as_view()),
        name='study_phase_detail'),

    url(
        r'study_phase/(?P<pk>\d+)/edit$',
        login_required(
            views.StudyPhaseUpdate.as_view()),
        name='study_phase_edit'),

    url(
        r'study_phase/(?P<pk>\d+)/delete$',
        login_required(
            views.StudyPhaseDelete.as_view()),
        name='study_phase_delete'),

    url(
        r'study_phase/',
        login_required(
            views.StudyPhaseList.as_view()),
        name='study_phase_list'),

    url(
        r'observation_study_design/add$',
        login_required(
            views.ObservationStudyDesignCreate.as_view()),
        name='observation_study_design_add'),

    url(
        r'observation_study_design/(?P<pk>\d+)$',
        login_required(
            views.ObservationStudyDesignDetail.as_view()),
        name='observation_study_design_detail'),

    url(
        r'observation_study_design/(?P<pk>\d+)/edit$',
        login_required(
            views.ObservationStudyDesignUpdate.as_view()),
        name='observation_study_design_edit'),

    url(
        r'observation_study_design/(?P<pk>\d+)/delete$',
        login_required(
            views.ObservationStudyDesignDelete.as_view()),
        name='observation_study_design_delete'),

    url(
        r'observation_study_design/',
        login_required(
            views.ObservationStudyDesignList.as_view()),
        name='observation_study_design_list'),

    url(
        r'time_perspective/add$',
        login_required(
            views.TimePerspectiveCreate.as_view()),
        name='time_perspective_add'),

    url(
        r'time_perspective/(?P<pk>\d+)$',
        login_required(
            views.TimePerspectiveDetail.as_view()),
        name='time_perspective_detail'),

    url(
        r'time_perspective/(?P<pk>\d+)/edit$',
        login_required(
            views.TimePerspectiveUpdate.as_view()),
        name='time_perspective_edit'),

    url(
        r'time_perspective/(?P<pk>\d+)/delete$',
        login_required(
            views.TimePerspectiveDelete.as_view()),
        name='time_perspective_delete'),

    url(
        r'time_perspective/',
        login_required(
            views.TimePerspectiveList.as_view()),
        name='time_perspective_list'),

    url(
        r'intervention_code/add$',
        login_required(
            views.InterventionCodeCreate.as_view()),
        name='intervention_code_add'),

    url(
        r'intervention_code/(?P<pk>\d+)$',
        login_required(
            views.InterventionCodeDetail.as_view()),
        name='intervention_code_detail'),

    url(
        r'intervention_code/(?P<pk>\d+)/edit$',
        login_required(
            views.InterventionCodeUpdate.as_view()),
        name='intervention_code_edit'),

    url(
        r'intervention_code/(?P<pk>\d+)/delete$',
        login_required(
            views.InterventionCodeDelete.as_view()),
        name='intervention_code_delete'),

    url(
        r'intervention_code/',
        login_required(
            views.InterventionCodeList.as_view()),
        name='intervention_code_list'),

    url(
        r'^groups/manage/$',
        login_required(
            views.CTGroupsManageView.as_view()),
        name='administration_admin_manage_groups'),

    url(
        r'^groups/group-administrators-pending-list/$',
        login_required(
            views.CTGroupsAdministratorPendingList.as_view()),
        name='group_admin_pending_list'),


    url(
        r'^groups/group-institution-pending-list/$',
        login_required(
            views.CTGroupsInstitutionPendingList.as_view()),
        name='group_institution_pending_list'),
   
    url(
        r'^groups/(?P<group_pk>\d+)/institution-pending/(?P<decision>A|R)/$',
        login_required(
            views.CTGroupsInstitutionPendingView.as_view()),
        name='group_institution_pending_decision'),

    url(
        r'^institution/(?P<institution_pk>\d+)/pending/(?P<decision>A|R)/$',
        login_required(
            views.InstitutionPendingView.as_view()),
        name='institution_pending_decision'),

    url(
        r'^institution/pending-list/$',
        login_required(
            views.InstitutionPendingListView.as_view()),
        name='institution_pending_list'),
    url(
        r'^institution/(?P<institution_pk>\d+)/pending/change/$',
        login_required(
            views.InstitutionPendingChangeView.as_view()),
        name='institution_pending_change_institution'),

    url(
        r'^group-administrators/(?P<ctgroupuser_pk>\d+)/pending/(?P<decision>A|R)/$',
        login_required(
            views.GroupAdministratorPendingView.as_view()),
        name='pending_group_administrator_decision'),

    url(
        r'^ct_registrant_em_preenchimento_list/$',
        login_required(
            views.CTRegistrantFillList.as_view()),
        name='ct_registrant_fill_list'),

    url(
        r'^ct_registrant_pending_list/$',
        login_required(
            views.CTRegistrantPendingList.as_view()),
        name='ct_registrant_pending_list'),

    url(
        r'^ct_registrant_review_list/$',
        login_required(
            views.CTRegistrantReviewList.as_view()),
        name='ct_registrant_review_list'),

    url(
        r'^ct_registrant_public_list/$',
        login_required(
            views.CTRegistrantPublicList.as_view()),
        name='ct_registrant_public_list'),

    url(
        r'^ct_evaluator_awaiting_assessment_list/$',
        login_required(
            views.CTEvaluatorListAwaitingAssessment.as_view()),
        name='ct_evaluator_awaiting_assessment_list'),

    url(
        r'^ct_evaluator_in_evaluation_list/$',
        login_required(
            views.CTEvaluatorListEvaluation.as_view()),
        name='ct_evaluator_in_evaluation_list'),

    url(
        r'^ct_reviser_being_reviewed/$',
        login_required(
            views.CTReviserListBeingReviewed.as_view()),
        name='ct_reviser_being_reviewed'),

    url(
        r'^ct_reviser_review_pending/$',
        login_required(
            views.CTReviserListReviewPending.as_view()),
        name='ct_reviser_review_pending'),


    url(
        r'clinical_trails/list/$',
        login_required(
            views.CTAllList.as_view()),
        name='clinical_trails_all'),


    url(
        r'indicators-by-clinical-trial/$',
        login_required(
            views.IndicatorsByClinicalTrialView.as_view()),
        name='indicator_by_clinical_trial'),

    url(
        r'indicators-by-reviser/$',
        login_required(
            views.IndicatorsByReviserView.as_view()),
        name='indicator_by_reviser'),

    url(
        r'indicators-by-evaluator/$',
        login_required(
            views.IndicatorsByEvaluatorView.as_view()),
        name='indicator_by_evaluator'),

    url(
        r'clinical_trails_published/$',
        login_required(
            views.IndicatorsClinicalTrialsPublished.as_view()),
        name='clinical_trails_published'),
    
)