# -*- coding: utf-8 -*-

from django import forms
from administration.models import CTGroupUser, Institution, CTGroup, \
Attachment, StateDaysLimit, SetupIdentifier, Setup
from django_select2 import AutoModelSelect2Field, AutoModelSelect2MultipleField, AutoHeavySelect2Widget
from django.contrib.auth import get_user_model
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext, ugettext_lazy
from django.utils.safestring import mark_safe
from django.forms.models import modelformset_factory
from clinical_trials.models import ClinicalTrial
from clinical_trials.forms import AutoHeavySelect2WidgetWithExtraParams
from localflavor.br.forms import BRCPFField, BRCNPJField
from django.forms.extras.widgets import SelectDateWidget
from django.contrib.admin import widgets
from datetime import datetime, timedelta

User = get_user_model()

class TrialsOfRegistrantsOfGroupChoices(AutoModelSelect2Field):
    """
    A django_select2 class to configure the autocomplete search for Institutions
    """
    queryset = ClinicalTrial.objects
    search_fields = [u'pk__icontains',]
    widget = AutoHeavySelect2WidgetWithExtraParams

    def prepare_qs_params(self, request, search_term, search_fields):
        """
        Create a new AND parameter to retrieve only the trials of registrants that
        are of the current group, and trials that don't have any group associated.
        """
        queryset = super(TrialsOfRegistrantsOfGroupChoices, self).prepare_qs_params(
            request,
            search_term,
            search_fields)
        
        group_id = request.GET.get(u'group_id', None)
        
        trials_pks = ClinicalTrial.objects.filter(
                         group__isnull=True,
                         user__ctgroups__ct_group__pk__in=group_id
                     ).values_list(u'pk', flat=True)
        queryset[u'and'][u'pk__in'] = trials_pks
        return queryset


class ClinicalTrialsofRegistrantsOfGroupSearchForm(forms.Form):
    trial = TrialsOfRegistrantsOfGroupChoices()


class RegistrantUsersOfGroupChoices(AutoModelSelect2Field):
    """
    A django_select2 class to configure the autocomplete search for registrant users
    of the current group(received as a GET parameter).
    """
    queryset = User.objects
    search_fields = [u'email__icontains',
                     u'first_name__icontains',
                     u'last_name__icontains',
                     u'username__icontains',]
    widget = AutoHeavySelect2WidgetWithExtraParams

    def prepare_qs_params(self, request, search_term, search_fields):
        """
        Create a new AND parameter to retrieve only the users of the group
        that is informed in the GET parameter.
        """
        queryset = super(RegistrantUsersOfGroupChoices, self).prepare_qs_params(
            request,
            search_term,
            search_fields)
        
        ct_id = request.GET.get(u'clinical_trial_id', None)
        
        users = ClinicalTrial.objects.get(pk=ct_id).group.users.filter(
            user__groups__name='registrant',
            user__is_active=True
        ).values_list(u'user__pk', flat=True)
        queryset[u'and'][u'pk__in'] = users

        return queryset



class UserChoices(AutoModelSelect2Field):
    """
    A django_select2 class to configure the autocomplete search
    for Users.
    """
    queryset = User.objects
    search_fields = [u'email__icontains',
                     u'first_name__icontains',
                     u'last_name__icontains',
                     u'username__icontains',]


    def __init__(self, *args, **kwargs):
        super(UserChoices, self).__init__(*args, **kwargs)


    def prepare_qs_params(self, request, search_term, search_fields):
        """
        Just active users and not the current request user
        """

        queryset = super(AutoModelSelect2Field, self).prepare_qs_params(
            request,
            search_term,
            search_fields
        )
        
        queryset[u'and'][u'is_active'] = True
    
        return queryset


class UserChoicesGroup(AutoModelSelect2Field):
    """
    A django_select2 class to configure the autocomplete search
    for Users.
    """
    queryset = User.objects.filter(groups__name=u'registrant')
    search_fields = [u'email__icontains',
                     u'first_name__icontains',
                     u'last_name__icontains',
                     u'username__icontains',]

    def prepare_qs_params(self, request, search_term, search_fields):
        """
        Just active users and not the current request user
        """
        queryset = super(AutoModelSelect2Field, self).prepare_qs_params(
            request,
            search_term,
            search_fields
        )
        
        queryset[u'and'][u'is_active'] = True
        
        queryset[u'and'][u'pk__in'] = \
            User.objects.all().exclude(pk=request.user.pk)
        return queryset

class UserRevisorChoices(AutoModelSelect2Field):
    """
    A django_select2 class to configure the autocomplete search
    for Users.
    """
    queryset = User.objects.filter(groups__name=u'reviser')
    search_fields = [u'email__icontains',
                     u'first_name__icontains',
                     u'last_name__icontains',
                     u'username__icontains',]
    def prepare_qs_params(self, request, search_term, search_fields):
        """
        Just active users
        """
        queryset = super(AutoModelSelect2Field, self).prepare_qs_params(
            request,
            search_term,
            search_fields
        )
        
        queryset[u'and'][u'is_active'] = True
        return queryset

class UserEvaluatorChoices(AutoModelSelect2Field):
    """
    A django_select2 class to configure the autocomplete search
    for Users.
    """
    queryset = User.objects.filter(groups__name=u'evaluator')
    search_fields = [u'email__icontains',
                     u'first_name__icontains',
                     u'last_name__icontains',
                     u'username__icontains',]
    def prepare_qs_params(self, request, search_term, search_fields):
        """
        Just active users
        """
        queryset = super(AutoModelSelect2Field, self).prepare_qs_params(
            request,
            search_term,
            search_fields
        )
        
        queryset[u'and'][u'is_active'] = True
        return queryset

class RevisorAndDateSearchForm(forms.Form):
	"""
	Revisor and De Search Form
	"""
	revisor = UserRevisorChoices(required=False, label=ugettext_lazy('Revisor'))
	date_from = forms.DateField(required=False, label=ugettext_lazy('Date from'))
	date_to = forms.DateField(required=False, label=ugettext_lazy('Date to'))

class EvaluatorAndDateSearchForm(forms.Form):
	"""
	Revisor and De Search Form
	"""
	evaluator = UserEvaluatorChoices(required=False, label=ugettext_lazy('Evaluator'))
	date_from = forms.DateField(required=False, label=ugettext_lazy('Date from'))
	date_to = forms.DateField(required=False, label=ugettext_lazy('Date to'))


class UserOccupationManagementSearchForm(forms.Form):
    """
    Auxiliar search form for the Users Occupation administrator management view.
    """
    USER_TYPES = (
       (u'A', ugettext_lazy(u'All')),
       (u'R', ugettext_lazy(u'Reviser')),
       (u'E', ugettext_lazy(u'Evaluator')), 
   )
    user = UserChoices(required=False, label=ugettext_lazy(u'User'))
    user_type = forms.ChoiceField(
        choices=USER_TYPES, required=False,
        label=ugettext_lazy(u'User Type'))


class AttachmentTypeForm(forms.ModelForm):
    class Meta:
        model = Attachment

    def __init__(self, *args, **kwargs):
        super(AttachmentTypeForm, self).__init__(*args, **kwargs)
        #change the setup field widget, to make it hide in the html
        self.fields['setup'].widget = forms.HiddenInput()


_AttachmentTypeFormSet = modelformset_factory(
    model=Attachment,
    extra=0)

class AttachmentTypeFormSet(_AttachmentTypeFormSet):
    
    def __init__(self, *args, **kwargs):
        kwargs.pop(u'save_as_new')
        kwargs.pop(u'instance')
        super(AttachmentTypeFormSet, self).__init__(*args, **kwargs)

    def clean(self, *args, **kwargs):
        """
        Checks was placed to exclude and has clinical trial,
        causes no short screen and error message.
        """
        for i in self.forms:
            if i.cleaned_data[u'id']:
                if i.cleaned_data.get(u'DELETE', False):
                    if i.cleaned_data[u'id'].has_clinical_trials():
                        i.cleaned_data[u'DELETE'] = False
                        i.data = i.data.copy()
                        i.data[i.prefix+u'-DELETE'] = []
                        raise forms.ValidationError(ugettext(u"You can't delete this attachment type, because it haves clinical trials using it."))
            else:
                if not 'name' in i.cleaned_data and not 'attachment_type' in i.cleaned_data:
                    i.cleaned_data[u'DELETE'] = True
        return super(AttachmentTypeFormSet, self).clean(*args, **kwargs)


class StateDaysLimitForm(forms.ModelForm):
    class Meta:
        model = StateDaysLimit

    def clean(self, *args, **kwargs):
        if self.cleaned_data.get(u'yellow', 0) >= self.cleaned_data.get(u'red', 0):
            raise forms.ValidationError(ugettext(u'Day in yellow can not be greater than the days in red.'))
        if self.cleaned_data.get(u'green', 0) >= self.cleaned_data.get(u'yellow', 0):
            raise forms.ValidationError(ugettext(u'Day in green can not be greater than the days in yellow.'))
        if self.cleaned_data.get(u'yellow', 0) < 0 or self.cleaned_data.get(u'red', 0) < 0 or self.cleaned_data.get(u'green', 0) < 0:
            raise forms.ValidationError(ugettext(u'The number of days can not be negative.'))
        return self.cleaned_data


class InstitutionChoices(AutoModelSelect2Field):
    queryset = Institution.objects
    search_fields = [u'name__icontains',
                     u'description__icontains',]
        
    def prepare_qs_params(self, request, search_term, search_fields):
        """
        Create a new AND parameter to retrieve only the active institutions or
        the moderation institutions of the group of the actual clinical_trial.
        """
        queryset = super(InstitutionChoices, self).prepare_qs_params(
            request,
            search_term,
            search_fields)
        
        queryset[u'and'][u'status'] = Institution.STATUS_ACTIVE

        return queryset


class CTGroupChoices(AutoModelSelect2Field):
    queryset = CTGroup.objects
    search_fields = [u'name__icontains',
                     u'description__icontains',]


class UserMultipleChoices(AutoModelSelect2MultipleField):
    queryset = User.objects
    search_fields = [u'email__icontains',
                     u'first_name__icontains',
                     u'last_name__icontains',
                     u'username__icontains',]


class AdministrationGroupManagementForm(forms.ModelForm):
    institution = InstitutionChoices(
        required=False,
        label=ugettext_lazy('institution'),
        widget=AutoHeavySelect2Widget(
            select2_options={'placeholder':ugettext_lazy(u'Search'), 'width': '1px'}
        )
    )
    administrators = UserMultipleChoices(
        required=True, label=ugettext_lazy('Administrators'))
    class Meta:
        model = CTGroup
        fields = [u'status', u'institution']

    def save(self, *args, **kwargs):
        instance = super(AdministrationGroupManagementForm, self).save(
            *args,
            **kwargs)
        instance.users.filter(
        	type_user=CTGroupUser.TYPE_ADMINISTRATOR).delete()
        for i in self.cleaned_data[u'administrators']:
            instance.users.create(
            	user=i,
            	status=2,
            	type_user=CTGroupUser.TYPE_ADMINISTRATOR)
        return instance

    def __init__(self, *args, **kwargs):
        super(AdministrationGroupManagementForm, self).__init__(*args, **kwargs)
        self.fields[u'administrators'].initial = \
                [i.user for i in self.instance.users.filter(
                type_user=CTGroupUser.TYPE_ADMINISTRATOR)]


    def _build_error_msg_status_with_ct(self):
        help_link = u'<a href="%s?%s">here</a>' % (
                    reverse(u'clinical_trial_admin_manage'),
                    u'group=%s' % self.instance.pk
                    )

        return mark_safe(
            ugettext(u'This groups have Clinical Trials. You can change it %s ' \
                % help_link)
            )


    def clean_status(self):
        
        if self.cleaned_data[u'status'] == CTGroup.STATUS_INACTIVE:
            if self.instance.clinicaltrial_set.exists():
                raise forms.ValidationError(
                    self._build_error_msg_status_with_ct()
                    )

        return self.cleaned_data[u'status']


class AdministrationGroupManagementSearchForm(forms.Form):
    institution = InstitutionChoices(
        required=False,
        label=ugettext_lazy(u'Institution'))
    group = CTGroupChoices(
        required=False,
        label=ugettext_lazy(u'Group'))


class InstitutionModerationChangeForm(forms.Form):
    institution = InstitutionChoices(
        required=True,
        label=ugettext_lazy(u'Institution'))


class CTGroupForm(forms.ModelForm):
    institution = InstitutionChoices(
        required=False,
        label=ugettext_lazy(u'Institution'),
        help_text=ugettext_lazy('Enter an institution if this group will be managed on a corporate basis.'))

    class Meta:
        model = CTGroup
        fields = ['name', 'description', 'institution',]


class CTGroupUserForm(forms.ModelForm):
    user = UserChoicesGroup()

    class Meta:
        model = CTGroupUser
        fields =['type_user', 'user']

    def clean(self, *args, **kwargs):
        """
        If the user is the only administrator can not be changed to registrant.
        """
        if self.cleaned_data.get(u'DELETE', False):
            if self.cleaned_data[u'user'].clinicaltrial_set.filter(
                group=self.cleaned_data[u'ct_group']).exists():
                raise forms.ValidationError(
                        ugettext("This user can't be removed, beacuse he have clinical trials on this group"))
        if self.instance.pk and self.is_valid():
            if self.cleaned_data['type_user'] == 'r':
                user = self.cleaned_data['user'].pk
                group = self.cleaned_data['ct_group'].pk
                groups_adm = CTGroup.objects.get(pk=group).users.filter(type_user='a').exclude(user=user).exists()
                if not groups_adm:
                    raise forms.ValidationError(
                        ugettext("Type of user can not be changed! You're the only administrator!"))
        return self.cleaned_data

from workflows.models import State
from workflows.utils import get_state
class CTOwnerForm(forms.ModelForm):
    user = RegistrantUsersOfGroupChoices()

    def __init__(self, instance, *args, **kwargs):
        super(CTOwnerForm, self).__init__(instance=instance, *args, **kwargs)


    class Meta:
        model = ClinicalTrial
        fields =[u'user']


class CTHolderForm(forms.ModelForm):
    holder = UserChoices()

    def __init__(self, instance, *args, **kwargs):
        super(CTHolderForm, self).__init__(instance=instance, *args, **kwargs)
        state = get_state(instance).pk
        if state in (State.STATE_AGUARDANDO_REVISAO, State.STATE_EM_REVISAO):
            self.fields[u'holder'] = UserRevisorChoices()
        elif state in (State.STATE_AGUARDANDO_AVALIACAO, State.STATE_EM_AVALIACAO):
            self.fields[u'holder'] = UserEvaluatorChoices()   

    class Meta:
        model = ClinicalTrial
        fields =[u'holder']


class InstitutionCreationForm(forms.ModelForm):
    search_address = forms.CharField(
        required=False,
        label=ugettext_lazy(u'Search Address')
    )
    cpf = BRCPFField(
        label=ugettext_lazy(u'CPF'),
        help_text=ugettext_lazy('The CPF has 11 digits.'),
        required=False)
    cnpj = BRCNPJField(
        label=ugettext_lazy(u'CNPJ'),
        help_text=ugettext_lazy('The CNPJ has 14 digits.'),
        required=False) 
    
    error_messages = {
        'duplicate_cpf': ugettext_lazy("A Institution with that cpf already exists."),
        'duplicate_cnpj': ugettext_lazy("A Institution with that cnpj already exists."),
    }

    class Meta:
        model = Institution
        fields = (u'name', u'description', u'search_address', u'postal_address', u'zip_code',
                  u'city', u'state', u'country', u'institution_type', u'type_person',
                  u'cpf', u'cnpj')

    def clean_cpf(self):
        """
        Verify if cpf already exists.
        """
        cpf = self.cleaned_data["cpf"]
        if cpf != u'':
            try:
                Institution.objects.get(cpf=cpf, status='A')
            except Institution.DoesNotExist:
                return cpf
            raise forms.ValidationError(
                self.error_messages['duplicate_cpf'],
                code='duplicate_cpf',
            )

    def clean_cnpj(self):
        """
        Verify if cnpj already exists.
        """
        cnpj = self.cleaned_data["cnpj"]
        if cnpj != u'':
            try:
                Institution.objects.get(cnpj=cnpj, status='A')
            except Institution.DoesNotExist:
                return cnpj
            raise forms.ValidationError(
                self.error_messages['duplicate_cnpj'],
                code='duplicate_cnpj',
            )

    def clean(self, *args, **kwargs):
        if self.cleaned_data.get(u'type_person', 0) == u'PF':
            if not self.cleaned_data.get(u'cpf', 0):
                if self.cleaned_data.get(u'cnpj', 0):
                    raise forms.ValidationError(
                        ugettext('Private Individual has been selected enter the cpf.'))
            elif self.cleaned_data.get(u'cnpj', 0):
                raise forms.ValidationError(
                        ugettext('Enter only cpf.'))
        elif self.cleaned_data.get(u'type_person', 0) == u'PJ':
            if not self.cleaned_data.get(u'cnpj', 0):
                if self.cleaned_data.get(u'cpf', 0):
                    raise forms.ValidationError(
                        ugettext('Legal Entity has been selected enter the cnpj.'))
            elif self.cleaned_data.get(u'cpf', 0):
                raise forms.ValidationError(
                        ugettext('Enter only cnpj.'))
        return self.cleaned_data


class InstitutionForm(forms.ModelForm):
    search_address = forms.CharField(
        required=False,
        label=ugettext_lazy(u'Search Address')
    )

    error_messages = {
        'duplicate_cpf': ugettext_lazy("A Institution with that cpf already exists."),
        'duplicate_cnpj': ugettext_lazy("A Institution with that cnpj already exists."),
    }

    class Meta:
        model = Institution
        fields = (u'name', u'description', u'search_address', u'postal_address', u'zip_code',
                  u'city', u'state', u'country', u'institution_type', u'status', u'type_person',
                  u'cpf', u'cnpj' )

    def __init__(self, *args, **kwargs):
        super(InstitutionForm, self).__init__(*args, **kwargs)
        #If the status isn't in Moderation, dnt allow to go back to moderation
        if not self.instance.status == Institution.STATUS_MODERATION:
            self.fields[u'status'].choices = \
                (i for i in self.fields[u'status'].choices \
                if i[0] != Institution.STATUS_MODERATION)

    def clean(self,  *args, **kwargs):
        """
        The intituição is active checks if there is no other with same cnpj or cpf
        that is active if there is no let save.
        """
        status_institution = self.cleaned_data['status']
        pk = self.instance.id
        if status_institution == 'A':
            cpf = self.cleaned_data["cpf"]
            cnpj = self.cleaned_data["cnpj"]
            if cpf != u'':
                if Institution.objects.filter(cpf=cpf, status='A').exclude(pk=pk).exists():
                    raise forms.ValidationError(
                        self.error_messages['duplicate_cpf'],
                        code='duplicate_cpf',
                    )
                else:
                    return self.cleaned_data
            elif cnpj != u'':
                if Institution.objects.filter(cnpj=cnpj, status='A').exclude(pk=pk).exists():
                    raise forms.ValidationError(
                        self.error_messages['duplicate_cnpj'],
                        code='duplicate_cnpj',
                    )
                else:
                   return self.cleaned_data 
        return self.cleaned_data


class GroupsListSearchForm(forms.Form):
    """
    Auxiliar search form for the Grupos List view.
    """
    group = CTGroupChoices(
        required=False,
        label=ugettext_lazy('Group'))
    institution = InstitutionChoices(
        required=False,
        label=ugettext_lazy('Institution'))

from login.models import RebecUser
class CTAllListSearchForm(forms.Form):
    """
    Auxiliar search form for the CT List view.
    """
    CT_STATES = [(i.pk, i.name,) for i in State.objects.all().order_by('pk')]
    CT_STATES = [('all', ugettext('All States'),)] + CT_STATES
    group = CTGroupChoices(required=False, label=ugettext_lazy('Group'))
    code = forms.IntegerField(required=False, label=ugettext_lazy('Code-ID'))
    user = UserChoicesGroup(required=False, label=ugettext_lazy('User'))
    holder = forms.ModelChoiceField(required=False, label=ugettext_lazy('Holder'),
                                 queryset=RebecUser.objects.filter(
                                    groups__name__in=(u'reviser', u'evaluator')).order_by(u'username'))
    rbr = forms.CharField(required=False, label=ugettext_lazy('RBR'))
    wf_state = forms.ChoiceField(required=False,
                                 label=ugettext_lazy('Workflow State'),
                                 choices=CT_STATES)


FILTER_COLORS_CHOICES = ( ('green', ugettext_lazy('Green')),
                            ('yellow', ugettext_lazy('Yellow')),
                            ('red', ugettext_lazy('Red')),
                            ('black', ugettext_lazy('Black')))

class DasboardSearchForm(forms.Form):
    """
    Dashboards filters and De Search Form
    """
    favorite_colors = forms.MultipleChoiceField(required=False,
        widget=forms.CheckboxSelectMultiple, choices=FILTER_COLORS_CHOICES,
        label=ugettext_lazy('Filter by color remaining days'))
    

class DasboardSearchTwoForm(forms.Form):
    """
    Dashboards filters and De Search Form
    """
    favorite_colors_two = forms.MultipleChoiceField(required=False,
        widget=forms.CheckboxSelectMultiple, choices=FILTER_COLORS_CHOICES,
        label=ugettext_lazy('Filter by color remaining days'))


class InstitutionListChoices(AutoModelSelect2Field):
    queryset = Institution.objects
    search_fields = [u'name__icontains']


class InstitutionListSearchForm(forms.Form):
    """
    Auxiliar search form for the Institution List view.
    """
    code = forms.IntegerField(required=False, label=ugettext_lazy('Code-ID'))
    name = forms.CharField(required=False, label=ugettext_lazy('Name'))


class SetupIdentifierForm(forms.ModelForm):
    class Meta:
        model = SetupIdentifier

    def __init__(self, *args, **kwargs):
        super(SetupIdentifierForm, self).__init__(*args, **kwargs)
        self.setup = Setup.objects.get()
        if self.setup.language_1_code_id:
            self.fields['help_text_lang_1'].label += \
                u': ' + unicode(self.setup.language_1_code)
            self.fields['help_text_code_lang_1'].label += \
                u': ' + unicode(self.setup.language_1_code)
            self.fields['help_text_description_lang_1'].label += \
                u': ' + unicode(self.setup.language_1_code)
            self.fields['help_text_date_lang_1'].label += \
                u': ' + unicode(self.setup.language_1_code)
        else:
            del self.fields['help_text_lang_1']
            del self.fields['help_text_code_lang_1']
            del self.fields['help_text_description_lang_1']
            del self.fields['help_text_date_lang_1']

        if self.setup.language_2_code_id:
            self.fields['help_text_lang_2'].label += \
                u': ' + unicode(self.setup.language_2_code)
            self.fields['help_text_code_lang_2'].label += \
                u': ' + unicode(self.setup.language_2_code)
            self.fields['help_text_description_lang_2'].label += \
                u': ' + unicode(self.setup.language_2_code)
            self.fields['help_text_date_lang_2'].label += \
                u': ' + unicode(self.setup.language_2_code)
        else:
            del self.fields['help_text_lang_2']
            del self.fields['help_text_code_lang_2']
            del self.fields['help_text_description_lang_2']
            del self.fields['help_text_date_lang_2']

        if self.setup.language_3_code_id:
            self.fields['help_text_lang_3'].label += \
                u': ' + unicode(self.setup.language_3_code)
            self.fields['help_text_code_lang_3'].label += \
                u': ' + unicode(self.setup.language_3_code)
            self.fields['help_text_description_lang_3'].label += \
                u': ' + unicode(self.setup.language_3_code)
            self.fields['help_text_date_lang_3'].label += \
                u': ' + unicode(self.setup.language_3_code)
        else:
            del self.fields['help_text_lang_3']
            del self.fields['help_text_code_lang_3']
            del self.fields['help_text_description_lang_3']
            del self.fields['help_text_date_lang_3']

class CTIndicatorDateSearchForm(forms.Form):
    date_first = forms.DateField(
        label=ugettext_lazy('Date First'),
        input_formats=[
            '%d/%m/%Y',
        ],
    )

    date_last = forms.DateField(
        label=ugettext_lazy('Date Last'),
        input_formats=[
            '%d/%m/%Y',
        ],
    )

    def clean(self):
        cleaned_data = super(CTIndicatorDateSearchForm, self).clean()
        date1 = cleaned_data.get('date_first')
        date2 = cleaned_data.get('date_last')

        if date1 and date2:
            if date1 > date2:
                raise forms.ValidationError(ugettext_lazy('First date must be less than the second date.'))

        return cleaned_data


class CTIndicatorPublishedFilterForm(forms.Form):
    date_first = forms.DateField(
        required=False,
        label=ugettext_lazy('Date First'),
        input_formats=[
            '%d/%m/%Y',
        ],
    )

    date_last = forms.DateField(
        required=False,
        label=ugettext_lazy('Date Last'),
        input_formats=[
            '%d/%m/%Y',
        ],
    )

    def clean(self):
        cleaned_data = super(CTIndicatorPublishedFilterForm, self).clean()
        date1 = cleaned_data.get('date_first')
        date2 = cleaned_data.get('date_last')

        if date1 and date2:
            if date1 > date2:
                raise forms.ValidationError(ugettext_lazy('First date must be less than the second date.'))

        return cleaned_data
