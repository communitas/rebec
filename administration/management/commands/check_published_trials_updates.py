#-*- coding: utf-8 -*-

from django.core.management.base import BaseCommand, CommandError
from workflows.models import State, StateObjectHistory
from clinical_trials.models import ClinicalTrial
from datetime import datetime, timedelta
from django.core.mail import send_mail
from django.utils.translation import ugettext as _
from django.core.urlresolvers import reverse
from django.conf import settings



class Command(BaseCommand):
    help = ('Check the publiched clinical trials, for each 6'
            ' months until th study_final_date, send to the owner'
            ' an email so he/she can verify the trial for possible changes.')

    def handle(self, *args, **options):
        print u'Checking trials...'
        today = datetime.today().date()
        published_trials = StateObjectHistory.objects.filter(
                               state__pk=State.STATE_PUBLICADO
                           ).distinct(u'content_id').values_list(
                                                         u'content_id',
                                                         flat=True)
        for trial in ClinicalTrial.objects.filter(
                         pk__in=published_trials,
                         study_final_date__gte=today
                     ):
            days = (today - trial.date_last_change).days
            if trial.get_workflow_state().pk == State.STATE_PUBLICADO and days >= 180:
                print (u'Trial %s ... yes, has been %s days. '
                       u'Sending e-mail to %s.') % (trial.pk, days, trial.user.email)
                send_mail(
                    _(u'Clinical trial %s: Check for changes') %trial.code,
                    (u'Hy %s. \n\n'
                     u'Has been %s days since your last check into the '
                     u' clinical trial %s.\nSo please, click in the following '
                     u'link to inform us about any changes: %s\n\n'
                     u'This trial Study Final Date is: %s, so you will receive '
                     u'an email notification from 6 to 6 months until this date.'
                    ) % (trial.user.name,
                         days,
                         trial.code,
                         settings.SITE_URL + reverse(
                                                 u'clinical_trial_check_for_changes',
                                                 args=(trial.pk,)
                                             ),
                         trial.study_final_date.strftime(u'%d/%m/%Y')),
                    None,
                    [trial.user.email])
            else:
                print u'Trial %s ... no' % trial.pk


#StateObjectHistory.objects.filter(state__pk=State.STATE_PUBLICADO).distinct(u'content_id').values_list(u'content_id', flat=True)