# -*- coding: utf-8 -*-

from django.test import TestCase
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.test.client import RequestFactory
from administration.models import *
from administration.views import *
from django.core.urlresolvers import reverse_lazy
from django.core.exceptions import PermissionDenied
from django.utils.translation import ugettext as _
from mock import Mock, MagicMock
from django.contrib import messages

User = get_user_model()

def create_user(groups=[]):
    """Create a user and add to a group parameter.
    """
    user = User.objects.create_user(
        'user', 'user@email.com',
        'pass', gender=1, race=1, country_id=u'BR')
    user.save()
    for group in groups:
        g = Group.objects.get(name=group)
        g.user_set.add(user)
    return user


class InstitutionTypeTestCase(TestCase):
    """InstitutionType class test """
    def setUp(self):
        """Register InstitutionType and add user with permission"""
        self.factory = RequestFactory()
        self.institution_type = InstitutionType.objects.create(
            description='chemistry',)
        self.user = create_user(['administrador'])
        self.original_success = messages.success
        messages.success = Mock()
        self.original_info = messages.info
        messages.info = Mock()
        self.original_error = messages.error
        messages.error = Mock()

    def tearDown(self):
        """Delete InstitutionType and user"""
        self.institution_type.delete()
        self.user.delete()
        messages.success = self.original_success
        messages.info = self.original_info
        messages.error = self.original_error

    def test_institution_type_list_permission(self):
        """Verify InstitutionType list permission"""
        request = self.factory.get(reverse_lazy('institution_type_list'))
        request.user = self.user
        response = InstitutionTypeList.as_view()(request)
        self.assertEqual(response.status_code, 200)
        self.user.delete()
        self.user = create_user()
        request = self.factory.get(reverse_lazy('institution_type_list'))
        request.user = self.user
        self.assertRaises(
            PermissionDenied, InstitutionTypeList.as_view(), request)
        self.user.delete()
        self.user = create_user(['administrador'])

    def test_institution_type_detail_permission(self):
        """Verify InstitutionType detail permission"""
        request = self.factory.get(reverse_lazy(
            'institution_type_detail', args=(self.institution_type.pk,)))
        request.user = self.user
        response = InstitutionTypeDetail.as_view()(
            request, pk=self.institution_type.pk)
        self.assertEqual(response.status_code, 200)
        self.user.delete()
        self.user = create_user()
        request = self.factory.get(reverse_lazy(
            'institution_type_detail', args=(self.institution_type.pk,)))
        request.user = self.user
        self.assertRaises(
            PermissionDenied, InstitutionTypeDetail.as_view(),
            request, args=(self.institution_type.pk,))
        self.user.delete()
        self.user = create_user(['administrador'])

    def test_institution_type_create_permission(self):
        """Verify InstitutionType create permission"""
        request = self.factory.get(reverse_lazy('institution_type_add'))
        request.user = self.user
        response = InstitutionTypeCreate.as_view()(request)
        self.assertEqual(response.status_code, 200)
        self.user.delete()
        self.user = create_user()
        request = self.factory.get(reverse_lazy('institution_type_add'))
        request.user = self.user
        self.assertRaises(
            PermissionDenied, InstitutionTypeCreate.as_view(), request)
        self.user.delete()
        self.user = create_user(['administrador'])

    def test_institution_type_update_permission(self):
        """Verify InstitutionType update permission"""
        request = self.factory.get(reverse_lazy(
            'institution_type_edit', args=(self.institution_type.pk,)))
        request.user = self.user
        response = InstitutionTypeUpdate.as_view()(
            request, pk=self.institution_type.pk)
        self.assertEqual(response.status_code, 200)
        self.user.delete()
        self.user = create_user()
        request = self.factory.get(reverse_lazy(
            'institution_type_edit', args=(self.institution_type.pk,)))
        request.user = self.user
        self.assertRaises(
            PermissionDenied, InstitutionTypeUpdate.as_view(),
            request, args=(self.institution_type.pk,))
        self.user.delete()
        self.user = create_user(['administrador'])

    def test_institution_type_delete_permission(self):
        """Verify InstitutionType delete permission"""
        request = self.factory.get(reverse_lazy(
            'institution_type_delete', args=(self.institution_type.pk,)))
        request.user = self.user
        response = InstitutionTypeDelete.as_view()(
            request, pk=self.institution_type.pk)
        self.assertEqual(response.status_code, 200)

        request = self.factory.post(reverse_lazy(
            'institution_type_delete', args=(self.institution_type.pk,)), {})
        request.user = self.user
        response = InstitutionTypeDelete.as_view()(
            request, pk=self.institution_type.pk)
        self.assertEqual(response.status_code, 302)

        self.user.delete()
        self.user = create_user()
        request = self.factory.get(reverse_lazy(
            'institution_type_delete', args=(self.institution_type.pk,)))
        request.user = self.user
        self.assertRaises(
            PermissionDenied, InstitutionTypeDelete.as_view(),
            request, args=(self.institution_type.pk,))

        request = self.factory.post(reverse_lazy(
            'institution_type_delete', args=(self.institution_type.pk,)), {})
        request.user = self.user
        self.assertRaises(
            PermissionDenied, InstitutionTypeDelete.as_view(),
            request, args=(self.institution_type.pk,))

        self.user.delete()
        self.user = create_user(['administrador'])

    def test_institution_type_list(self):
        """Verify InstitutionType objects"""
        request = self.factory.get(reverse_lazy('institution_type_list'))
        request.user = self.user
        response = InstitutionTypeList.as_view()(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.context_data['institutiontype_list'].count(), 1)
        
    def test_institution_type_delete(self):
        """Verify InstitutionType delete view"""
        self.assertEqual(InstitutionType.objects.all().count(), 1)

        request = self.factory.post(reverse_lazy(
            'institution_type_delete', args=(self.institution_type.pk,)), {})
        request.user = self.user
        response = InstitutionTypeDelete.as_view()(
            request, pk=self.institution_type.pk)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(InstitutionType.objects.all().count(), 0)

        country = Country.objects.create(
            pk=u"br",
            description='Brasil')
        self.institution_type = InstitutionType.objects.create(
            description='chemistry')
        institution = Institution.objects.create(
            name='',
            description='',
            postal_address='',
            city='',
            state='',
            country=country,
            status='M',
            institution_type=self.institution_type)

        request = self.factory.post(reverse_lazy(
            'institution_type_delete', args=(self.institution_type.pk,)), {})
        request.user = self.user
        response = InstitutionTypeDelete.as_view()(
            request, pk=self.institution_type.pk)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content.decode('utf-8'),  _("Can not delete this entry"))
        institution.delete()
        country.delete()

    def test_institution_type_query(self):
        """Verify InstitutionType query"""
        institution_type2 = InstitutionType.objects.create(
            description='pharmaceutical',)
        institution_type2.save()
        request = self.factory.get(reverse_lazy(
            'institution_type_list'), {'q': 'pharmaceutical'})
        request.user = self.user
        response = InstitutionTypeList.as_view()(request)
        self.assertEqual(response.status_code, 200)
        request = self.factory.get(
            reverse_lazy('institution_type_list'), {'q': 'chemistry'})
        request.user = self.user
        response = InstitutionTypeList.as_view()(request)
        self.assertEqual(response.status_code, 200)
        institution_type2.delete()
