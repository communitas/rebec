# -*- coding: utf-8 -*-

from django.test import TestCase
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from administration.views import *
from django.test.client import RequestFactory
from django.core.urlresolvers import reverse_lazy
from django.core.exceptions import PermissionDenied
from django.utils.translation import ugettext as _
from mock import Mock, MagicMock
from django.contrib import messages

User = get_user_model()


def create_user(groups=[]):
    """Create a user and add to a group parameter.
    """
    user = User.objects.create_user(
        'user', 'user@email.com',
        'pass', gender=1, race=1, country_id=u'BR')
    user.save()
    for group in groups:
        g = Group.objects.get(name=group)
        g.user_set.add(user)
    return user

class InterventionAssignmentTesteCase(TestCase):
    """Intervention Assignment class test."""
    def setUp(self):
        """Register intervention Porpose."""
        self.intervention = InterventionAssignment.objects.create(
            description='teste')
        self.factory = RequestFactory()
        self.user = create_user(['administrador'])
        self.original_success = messages.success
        messages.success = Mock()
        self.original_info = messages.info
        messages.info = Mock()
        self.original_error = messages.error
        messages.error = Mock()

    def tearDown(self):
        """Delete Intervention Assignment and user."""
        self.user.delete()
        self.intervention.delete()
        messages.success = self.original_success
        messages.info = self.original_info
        messages.error = self.original_error

    def test_intervention_assignment_create(self):
        """Test Intervention Assignment create view."""
        post_data = {"pk": 2, "description": "teste111"}
        request_post = self.factory.post(
            reverse_lazy('intervention_assignment_add'), data=post_data)
        request_post.user = self.user
        response_post = InterventionAssignmentCreate.as_view()(request_post)
        self.assertEqual(response_post.status_code, 302)
        self.assertEqual(InterventionAssignment.objects.all().count(), 2)

    def test_intervention_assignment_update(self):
        """Test Intervention Assignment update view."""
        alter_post = {"description": "ab"}
        request_post = self.factory.post(
            reverse_lazy('intervention_assignment_edit', args=(self.intervention.pk,)), data=alter_post)
        request_post.user = self.user
        response_post = InterventionAssignmentUpdate.as_view()(request_post, pk=self.intervention.pk)
        self.assertEqual(response_post.status_code, 302)
        intervention = InterventionAssignment.objects.get(pk=self.intervention.pk)
        self.assertEqual(intervention.description, alter_post['description'])

    def test_intervention_assignment_list(self):
        """Test Intervention Assignment list view."""
        request = self.factory.get(
            reverse_lazy('intervention_assignment_list'))
        request.user = self.user
        response = InterventionAssignmentList.as_view()(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.context_data[u'interventionassignment_list'].count(), 1)

    def test_intervention_assignment_delete(self):
        """Test Intervention Assignment delete view."""
        intervention = InterventionAssignment.objects.create(
            description='tata')
        request = self.factory.post(
            reverse_lazy('intervention_assignment_delete', args=(intervention.pk,)), {})
        request.user = self.user
        response = InterventionAssignmentDelete.as_view()(request, pk=intervention.pk)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(InterventionAssignment.objects.all().count(), 1)

    def test_intervention_assignment_list_permission(self):
        """Verify intervention assignment list permission"""
        request = self.factory.get(reverse_lazy('intervention_assignment_list'))
        request.user = self.user
        response = InterventionAssignmentList.as_view()(request)
        self.assertEqual(response.status_code, 200)
        self.user.delete()
        self.user = create_user()
        request = self.factory.get(reverse_lazy('intervention_assignment_list'))
        request.user = self.user
        self.assertRaises(PermissionDenied, InterventionAssignmentList.as_view(), request)
        self.user.delete()
        self.user = create_user(['administrador'])

    def test_intervention_detail_permission(self):
        """Verify InterventionAssignment detail permission"""
        request = self.factory.get(
            reverse_lazy('intervention_assignment_detail', args=(self.intervention.pk,)))
        request.user = self.user
        response = InterventionAssignmentDetail.as_view()(request, pk=self.intervention.pk)
        self.assertEqual(response.status_code, 200)
        self.user.delete()
        self.user = create_user()
        request = self.factory.get(
            reverse_lazy('intervention_assignment_detail', args=(self.intervention.pk,)))
        request.user = self.user
        self.assertRaises(
            PermissionDenied, InterventionAssignmentDetail.as_view(),
            request, args=(self.intervention.pk,))
        self.user.delete()
        self.user = create_user(['administrador'])

    def test_intervention_create_permission(self):
        """Verify InterventionAssignment create permission"""
        request = self.factory.post(reverse_lazy('intervention_assignment_add'))
        request.user = self.user
        response = InterventionAssignmentCreate.as_view()(request)
        self.assertEqual(response.status_code, 200)
        self.user.delete()
        self.user = create_user()
        request = self.factory.get(reverse_lazy('intervention_assignment_add'))
        request.user = self.user
        self.assertRaises(
            PermissionDenied, InterventionAssignmentCreate.as_view(), request)
        self.user.delete()
        self.user = create_user(['administrador'])

    def test_intervention_update_permission(self):
        """Verify InterventionAssignment update permission"""
        request = self.factory.get(
            reverse_lazy('intervention_assignment_edit', args=(self.intervention.pk,)))
        request.user = self.user
        response = InterventionAssignmentUpdate.as_view()(request, pk=self.intervention.pk)
        self.assertEqual(response.status_code, 200)
        self.user.delete()
        self.user = create_user()
        request = self.factory.get(
            reverse_lazy('intervention_assignment_edit', args=(self.intervention.pk,)))
        request.user = self.user
        self.assertRaises(
            PermissionDenied, InterventionAssignmentUpdate.as_view(),
            request, args=(self.intervention.pk,))
        self.user.delete()
        self.user = create_user(['administrador'])

    def test_intervention_delete_permission(self):
        """Verify InterventionAssignment delete permission"""
        request = self.factory.get(
            reverse_lazy('intervention_assignment_delete', args=(self.intervention.pk,)))
        request.user = self.user
        response = InterventionAssignmentDelete.as_view()(request, pk=self.intervention.pk)
        self.assertEqual(response.status_code, 200)

        request = self.factory.post(
            reverse_lazy('intervention_assignment_delete', args=(self.intervention.pk,)), {})
        request.user = self.user
        response = InterventionAssignmentDelete.as_view()(request, pk=self.intervention.pk)
        self.assertEqual(response.status_code, 302)

        self.user.delete()
        self.user = create_user()
        request = self.factory.get(
            reverse_lazy('intervention_assignment_delete', args=(self.intervention.pk,)))
        request.user = self.user
        self.assertRaises(
            PermissionDenied, InterventionAssignmentDelete.as_view(),
            request, args=(self.intervention.pk,))

        request = self.factory.post(
            reverse_lazy('intervention_assignment_delete', args=(self.intervention.pk,)), {})
        request.user = self.user
        self.assertRaises(
            PermissionDenied, InterventionAssignmentDelete.as_view(),
            request, args=(self.intervention.pk,))

        self.user.delete()
        self.user = create_user(['administrador'])
