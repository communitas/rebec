#-*- coding: utf-8 -*-
from django.test import TestCase
from django.test.client import RequestFactory

from django.contrib.auth import get_user_model
from administration.models import Setup
from administration.views import DashboardView
from django.core.urlresolvers import reverse, resolve
from mock import Mock
from django.contrib import messages

from administration.models import (
    SetupIdentifier, InstitutionType, CTGroup,
    Institution, Country, CTGroupUser, StateDaysLimit, Setup
    )
from django.contrib.auth.models import Group
from django.core.exceptions import PermissionDenied
from clinical_trials.models import ClinicalTrial, Sponsor
from administration import views
from workflows.utils import set_state, get_state
from workflows.models import State, StateObjectHistory
from workflows import utils
from workflows.models import StateObjectRelation, State
from datetime import datetime, timedelta

User = get_user_model()


class RegistrantDashboardTestCase(TestCase):

    def setUp(self):

        self.factory = RequestFactory()
        self.setup = Setup.objects.create(
            language_1_code_id=127,
            language_1_mandatory=True,
            language_2_code_id=28,
            language_2_mandatory=True)
        self.user1 = User.objects.create(username=u'e',
                                        email=u'e@e.com',
                                        gender=1,
                                        race=1,
                                        country_id=u'BR')
        self.user1.groups.add(
            Group.objects.get(
                name=u'registrant')
            )
       
        self.institutiontype1 = InstitutionType.objects.create(
            description=u'T1'
            )
        self.country = Country.objects.create(
            pk=u"br",
            description='Brasil')

        self.institution1 = Institution.objects.create(
            name=u'abc',
            status=u'M',
            institution_type=self.institutiontype1,
            country=self.country
        )

        self.group1 = CTGroup.objects.create(
            name='g1',
            description='g1',
            visibility=1,
        )

        self.ct1 = ClinicalTrial.objects.create(user=self.user1)

    def tearDown(self):
        self.setup.delete()
        self.group1.users.all().delete()
        self.group1.delete()
        self.institution1.delete()
        self.institutiontype1.delete()
        self.user1.stateobjecthistory_set.all().delete()
        self.user1.delete()
        self.ct1.delete()

    def test_dashboard_registrante(self):
        """
        Test Dashboard registrant. Show Clinical Trial in all states.
        """

        request = self.factory.get(
            reverse(u'home')
        )
        request.user = self.user1
        
        view_return = DashboardView.as_view()(request)
        self.assertEqual(view_return.status_code, 200)
        self.assertEqual(view_return.context_data['clinical_trial_em_preenchimento'][0].user.pk, self.user1.pk)
        self.assertEqual(view_return.context_data['clinical_trial_em_preenchimento'].count(), 1)
        self.assertEqual(utils.get_state(self.ct1).pk, State.STATE_EM_PREENCHIMENTO)
        utils.set_state(self.ct1, State.objects.get(pk=State.STATE_RESUBMETIDO))
        self.assertEqual(view_return.context_data['clinical_trial_pending'][0].user.pk, self.user1.pk)
        self.assertEqual(view_return.context_data['clinical_trial_pending'].count(), 1)
        self.assertEqual(utils.get_state(self.ct1).pk, State.STATE_RESUBMETIDO)
        utils.set_state(self.ct1, State.objects.get(pk=State.STATE_REVISANDO_PREENCHIMENTO))
        self.assertEqual(view_return.context_data['clinical_trial_pending'][0].user.pk, self.user1.pk)
        self.assertEqual(view_return.context_data['clinical_trial_pending'].count(), 1)
        self.assertEqual(utils.get_state(self.ct1).pk, State.STATE_REVISANDO_PREENCHIMENTO)
        utils.set_state(self.ct1, State.objects.get(pk=State.STATE_AGUARDANDO_AVALIACAO))
        self.assertEqual(view_return.context_data['registrant_clinical_trial_em_revisao'][0].user.pk, self.user1.pk)
        self.assertEqual(view_return.context_data['registrant_clinical_trial_em_revisao'].count(), 1)
        self.assertEqual(utils.get_state(self.ct1).pk, State.STATE_AGUARDANDO_AVALIACAO)
        utils.set_state(self.ct1, State.objects.get(pk=State.STATE_PUBLICADO))
        self.assertEqual(view_return.context_data['clinical_trial_publicado'][0].user.pk, self.user1.pk)
        self.assertEqual(view_return.context_data['clinical_trial_publicado'].count(), 1)
        self.assertEqual(utils.get_state(self.ct1).pk, State.STATE_PUBLICADO)
        utils.set_state(self.ct1, State.objects.get(pk=State.STATE_EM_PREENCHIMENTO))
    

    def test_dashboard_registrante_adm(self):
        """ Test Dashboard registrant is administrator of group. Show pending user"""

        user_adm = CTGroupUser.objects.create(
            user=self.user1,
            type_user='a',
            status=2,
            ct_group=self.group1)
        
        user2 = User.objects.create(username=u'teste',
                                        email=u'e@e.com',
                                        gender=1,
                                        race=1,
                                        country_id=u'BR')
        user2.groups.add(
            Group.objects.get(
                name=u'registrant')
        )
        user_group = CTGroupUser.objects.create(
            user=user2,
            type_user='a',
            status=1,
            ct_group=self.group1)

        request = self.factory.get(
            reverse(u'home')
        )
        request.user = self.user1
        
        view_return = DashboardView.as_view()(request)
        self.assertEqual(view_return.status_code, 200)
        self.assertEqual(view_return.context_data[u'users_penging_aprov'][0].ct_group.pk, self.group1.pk)
        user_group.delete()
        user2.delete()
        user_adm.delete()


    def test_dashboard_registrante_adm_exclusion(self):
        """ Test Dashboard registrant is administrator of group. Show pending user"""
        user_adm = CTGroupUser.objects.create(
            user=self.user1,
            type_user='a',
            status=2,
            ct_group=self.group1)
        
        user2 = User.objects.create(username=u'teste',
                                        email=u'e@e.com',
                                        gender=1,
                                        race=1,
                                        country_id=u'BR')
        self.user1.groups.add(
            Group.objects.get(
                name=u'registrant')
        )
        user_group = CTGroupUser.objects.create(
            user=user2,
            type_user='a',
            status=4,
            ct_group=self.group1)

        request = self.factory.get(
            reverse(u'home')
        )
        request.user = self.user1
        
        view_return = DashboardView.as_view()(request)
        self.assertEqual(view_return.status_code, 200)
        self.assertEqual(view_return.context_data[u'users_penging_exclusion'][0].ct_group.pk, self.group1.pk)
        user_group.delete()
        user2.delete()
        user_adm.delete()

class EvaluatorDashboard(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.setup = Setup.objects.create(
            language_1_code_id=127,
            language_1_mandatory=True,
            language_2_code_id=28,
            language_2_mandatory=True)
        self.user1 = User.objects.create(username=u'e',
                                        email=u'e@e.com',
                                        gender=1,
                                        race=1,
                                        country_id=u'BR')
        self.user1.groups.add(
            Group.objects.get(
                name=u'registrant')
            )

        self.user2 = User.objects.create(username=u'evaluator',
                                        email=u'e@e.com',
                                        gender=1,
                                        race=1,
                                        country_id=u'BR')

        self.user2.groups.add(
            Group.objects.get(
                name=u'evaluator')
            )

        self.ct1 = ClinicalTrial.objects.create(user=self.user1, holder=self.user2)
        self.ct2 = ClinicalTrial.objects.create(user=self.user1, holder=self.user2)
        self.ct3 = ClinicalTrial.objects.create(user=self.user1, holder=self.user2)
        self.ct4 = ClinicalTrial.objects.create(user=self.user1, holder=self.user2)
        self.ct5 = ClinicalTrial.objects.create(user=self.user1, holder=self.user2)
        self.ct6 = ClinicalTrial.objects.create(user=self.user1, holder=self.user2)

        self.aguardando_avaliacao = State.objects.get(
            pk=State.STATE_AGUARDANDO_AVALIACAO)
        
        self.dias = StateDaysLimit.objects.create(
            red=10,
            yellow=5,
            green=2,
            setup=self.setup,
            state=self.aguardando_avaliacao)


    def tearDown(self):
        self.setup.delete()
        self.user1.stateobjecthistory_set.all().delete()
        self.user1.delete()
        self.ct1.delete()
        self.ct2.delete()
        self.ct3.delete()
        self.ct4.delete()
        self.ct5.delete()
        self.ct6.delete()
        self.dias.delete()


    def test_dashboard_avaliador(self):
        """Test Evaluator Dashboard, Graphical tests awaiting assessment."""
        utils.set_state(self.ct1, State.objects.get(pk=State.STATE_AGUARDANDO_AVALIACAO))
        utils.set_state(self.ct2, State.objects.get(pk=State.STATE_AGUARDANDO_AVALIACAO))
        utils.set_state(self.ct3, State.objects.get(pk=State.STATE_AGUARDANDO_AVALIACAO))
        utils.set_state(self.ct4, State.objects.get(pk=State.STATE_AGUARDANDO_AVALIACAO))
        utils.set_state(self.ct5, State.objects.get(pk=State.STATE_AGUARDANDO_AVALIACAO))
        utils.set_state(self.ct6, State.objects.get(pk=State.STATE_AGUARDANDO_AVALIACAO))
        history_1 = StateObjectHistory.objects.filter(
            content_id=self.ct1.pk, state_id=State.STATE_AGUARDANDO_AVALIACAO
            ).update(
                update_date=datetime(2014,9,5))

        history_2 = StateObjectHistory.objects.filter(
            content_id=self.ct2.pk, state_id=State.STATE_AGUARDANDO_AVALIACAO
            ).update(
                update_date=datetime(2014,9,5))
            
        history_3 = StateObjectHistory.objects.filter(
            content_id=self.ct3.pk, state_id=State.STATE_AGUARDANDO_AVALIACAO
            ).update(
                update_date=datetime(2014,9,9))
       
        history_4 = StateObjectHistory.objects.filter(
            content_id=self.ct4.pk, state_id=State.STATE_AGUARDANDO_AVALIACAO
            ).update(
                update_date=datetime(2014,9,9))
        
        history_5 = StateObjectHistory.objects.filter(
            content_id=self.ct5.pk, state_id=State.STATE_AGUARDANDO_AVALIACAO
            ).update(
                update_date=datetime(2014,8,9))

        history_6 = StateObjectHistory.objects.filter(
            content_id=self.ct6.pk, state_id=State.STATE_AGUARDANDO_AVALIACAO
            ).update(
                update_date=datetime(2014,8,9))

        request = self.factory.get(
            reverse(u'home')
        )
        request.user = self.user2
        view_return = DashboardView.as_view()(request)
        self.assertEqual(view_return.status_code, 200)       
        self.assertEqual(view_return.context_data['clinical_trial_pending_avaliacao'].count(), 6)
        self.assertEqual(utils.get_state(self.ct1).pk, State.STATE_AGUARDANDO_AVALIACAO)
        