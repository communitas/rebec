# -*- coding: utf-8 -*-

from django.test import TestCase
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from administration.views import *
from django.test.client import RequestFactory
from django.core.urlresolvers import reverse_lazy
from django.core.exceptions import PermissionDenied
from django.utils.translation import ugettext as _
from django.test.client import Client, RequestFactory
from mock import Mock, MagicMock
from django.contrib import messages
from clinical_trials.models import *
from clinical_trials.views import *
from workflows import utils
from workflows.models import StateObjectRelation, State

User = get_user_model()

def create_user(groups=[]):
    """Create a user and add to a group parameter.
    """
    user = User.objects.create_user(
        'user', 'user@email.com',
        'pass', gender=1, race=1, country_id=u'BR')
    user.save()
    for group in groups:
        g = Group.objects.get(name=group)
        g.user_set.add(user)
    return user

class CTListsDashboardTesteCase(TestCase):
    def setUp(self):
        self.user_registrant = create_user(['registrant'])
        self.clinical_trial = ClinicalTrial.objects.create(
            user=self.user_registrant)
        self.factory = RequestFactory()
        
        
    def tearDown(self):
        """Delete Masking Type and user."""
        self.clinical_trial.delete()
        self.user_registrant.delete()
    
    def test_list_ct_em_prencimento(self):
        request = self.factory.get(reverse_lazy('ct_registrant_fill_list'))
        request.user = self.user_registrant
        response = CTRegistrantFillList.as_view()(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context_data['clinicaltrial_list'][0].user.pk, self.user_registrant.pk)
        self.assertEqual(response.context_data['clinicaltrial_list'].count(), 1)
        self.assertEqual(utils.get_state(self.clinical_trial).pk, State.STATE_EM_PREENCHIMENTO)


    def test_list_ct_pending(self):
        utils.set_state(self.clinical_trial, State.objects.get(pk=State.STATE_RESUBMETIDO))
        request = self.factory.get(reverse_lazy('ct_registrant_pending_list'))
        request.user = self.user_registrant
        response = CTRegistrantPendingList.as_view()(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(utils.get_state(self.clinical_trial).pk, State.STATE_RESUBMETIDO)
        self.assertEqual(response.context_data['clinicaltrial_list'][0].user.pk, self.user_registrant.pk)
        utils.set_state(self.clinical_trial, State.objects.get(pk=State.STATE_EM_PREENCHIMENTO))

    def test_list_ct_review(self):
        utils.set_state(self.clinical_trial, State.objects.get(pk=State.STATE_AGUARDANDO_AVALIACAO))
        request = self.factory.get(reverse_lazy('ct_registrant_review_list'))
        request.user = self.user_registrant
        response = CTRegistrantReviewList.as_view()(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context_data['clinicaltrial_list'][0].user.pk, self.user_registrant.pk)
        self.assertEqual(utils.get_state(self.clinical_trial).pk, State.STATE_AGUARDANDO_AVALIACAO)
        utils.set_state(self.clinical_trial, State.objects.get(pk=State.STATE_EM_AVALIACAO))
        request = self.factory.get(reverse_lazy('ct_registrant_review_list'))
        request.user = self.user_registrant
        response = CTRegistrantReviewList.as_view()(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context_data['clinicaltrial_list'][0].user.pk, self.user_registrant.pk)
        self.assertEqual(utils.get_state(self.clinical_trial).pk, State.STATE_EM_AVALIACAO)
        utils.set_state(self.clinical_trial, State.objects.get(pk=State.STATE_AGUARDANDO_REVISAO))
        request = self.factory.get(reverse_lazy('ct_registrant_review_list'))
        request.user = self.user_registrant
        response = CTRegistrantReviewList.as_view()(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context_data['clinicaltrial_list'][0].user.pk, self.user_registrant.pk)
        self.assertEqual(utils.get_state(self.clinical_trial).pk, State.STATE_AGUARDANDO_REVISAO)
        utils.set_state(self.clinical_trial, State.objects.get(pk=State.STATE_EM_REVISAO))
        request = self.factory.get(reverse_lazy('ct_registrant_review_list'))
        request.user = self.user_registrant
        response = CTRegistrantReviewList.as_view()(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context_data['clinicaltrial_list'][0].user.pk, self.user_registrant.pk)
        self.assertEqual(utils.get_state(self.clinical_trial).pk, State.STATE_EM_REVISAO)
        utils.set_state(self.clinical_trial, State.objects.get(pk=State.STATE_AGUARDANDO_MODERACAO))
        request = self.factory.get(reverse_lazy('ct_registrant_review_list'))
        request.user = self.user_registrant
        response = CTRegistrantReviewList.as_view()(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context_data['clinicaltrial_list'][0].user.pk, self.user_registrant.pk)
        self.assertEqual(utils.get_state(self.clinical_trial).pk, State.STATE_AGUARDANDO_MODERACAO)
        utils.set_state(self.clinical_trial, State.objects.get(pk=State.STATE_EM_PREENCHIMENTO))

    def test_list_ct_public(self):
        utils.set_state(self.clinical_trial, State.objects.get(pk=State.STATE_PUBLICADO))
        request = self.factory.get(reverse_lazy('ct_registrant_public_list'))
        request.user = self.user_registrant
        response = CTRegistrantPublicList.as_view()(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context_data['clinicaltrial_list'][0].user.pk, self.user_registrant.pk)
        self.assertEqual(response.context_data['clinicaltrial_list'].count(), 1)
        self.assertEqual(utils.get_state(self.clinical_trial).pk, State.STATE_PUBLICADO)
        utils.set_state(self.clinical_trial, State.objects.get(pk=State.STATE_EM_PREENCHIMENTO))


class CTEvaluatorTesteCase(TestCase):
    def setUp(self):
        self.evaluator = User.objects.create_user(
            'user1', 'user@email.com',
            'pass', gender=1, race=1, country_id=u'BR'
        )
        group = Group.objects.get(name='evaluator')
        group.user_set.add(self.evaluator)
        self.registrant = User.objects.create_user(
            'user2', 'user@email.com',
            'pass', gender=1, race=1, country_id=u'BR'
        )
        groups = Group.objects.get(name='registrant')
        groups.user_set.add(self.registrant)
        self.clinical_trial = ClinicalTrial.objects.create(
            user = self.registrant,
            holder=self.evaluator)
        self.factory = RequestFactory()
        
        
    def tearDown(self):
        """Delete Masking Type and user."""
        self.clinical_trial.delete()
        self.registrant.stateobjecthistory_set.all().delete()
        self.evaluator.stateobjecthistory_set.all().delete()
        self.registrant.delete()
        self.evaluator.delete()

    def test_list_evaluator_aguardando_avaliacao(self):
        utils.set_state(self.clinical_trial, State.objects.get(pk=State.STATE_AGUARDANDO_AVALIACAO))
        request = self.factory.get(reverse_lazy('ct_evaluator_awaiting_assessment_list'))
        request.user = self.evaluator
        response = CTEvaluatorListAwaitingAssessment.as_view()(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(utils.get_state(self.clinical_trial).pk, State.STATE_AGUARDANDO_AVALIACAO)

    def test_list_evaluator_em_avaliacao(self):
        utils.set_state(self.clinical_trial, State.objects.get(pk=State.STATE_EM_AVALIACAO))
        request = self.factory.get(reverse_lazy('ct_evaluator_in_evaluation_list'))
        request.user = self.evaluator
        response = CTEvaluatorListEvaluation.as_view()(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(utils.get_state(self.clinical_trial).pk, State.STATE_EM_AVALIACAO)

class CTReviserTesteCase(TestCase):
    def setUp(self):
        self.reviser = User.objects.create_user(
            'user1', 'user@email.com',
            'pass', gender=1, race=1, country_id=u'BR'
        )
        group = Group.objects.get(name='reviser')
        group.user_set.add(self.reviser)
        self.registrant = User.objects.create_user(
            'user2', 'user@email.com',
            'pass', gender=1, race=1, country_id=u'BR'
        )
        groups = Group.objects.get(name='registrant')
        groups.user_set.add(self.registrant)
        self.clinical_trial = ClinicalTrial.objects.create(
            user = self.registrant,
            holder=self.reviser)
        self.factory = RequestFactory()
        
        
    def tearDown(self):
        """Delete Masking Type and user."""
        self.clinical_trial.delete()
        self.registrant.stateobjecthistory_set.all().delete()
        self.reviser.stateobjecthistory_set.all().delete()
        self.registrant.delete()
        self.reviser.delete()

    def test_list_reviser_aguardando_revisao(self):
        utils.set_state(self.clinical_trial, State.objects.get(pk=State.STATE_AGUARDANDO_REVISAO))
        request = self.factory.get(reverse_lazy('ct_reviser_review_pending'))
        request.user = self.reviser
        response = CTReviserListReviewPending.as_view()(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(utils.get_state(self.clinical_trial).pk, State.STATE_AGUARDANDO_REVISAO)

    def test_list_revisor_em_revisao(self):
        utils.set_state(self.clinical_trial, State.objects.get(pk=State.STATE_EM_REVISAO))
        request = self.factory.get(reverse_lazy('ct_reviser_being_reviewed'))
        request.user = self.reviser
        response = CTReviserListBeingReviewed.as_view()(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(utils.get_state(self.clinical_trial).pk, State.STATE_EM_REVISAO)

