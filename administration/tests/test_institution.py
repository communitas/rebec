# -*- coding: utf-8 -*-

from django.test import TestCase
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.test.client import RequestFactory
from administration.models import *
from administration.views import *
from django.core.urlresolvers import reverse_lazy
from django.core.exceptions import PermissionDenied
from django.utils.translation import ugettext as _
from mock import Mock
from workflows.models import State
from workflows.utils import set_state
from django.contrib import messages


User = get_user_model()


def create_user(groups=[], username=None):
    """Create a user and add to a group parameter.
    """
    if username is None:
        username = u'user'
    user = User.objects.create_user(
        username, 'user@email.com',
        'pass', gender=1, race=1, country_id=u'BR')
    user.save()
    for group in groups:
        g = Group.objects.get(name=group)
        g.user_set.add(user)
    return user


class InstitutionTestCase(TestCase):
    """Institution class test """
    def setUp(self):
        """Register Institution and add user with permission"""
        self.factory = RequestFactory()
        self.user = create_user(['administrador'])
        self.country = Country.objects.create(
            pk=u"br",
            description='Brasil')
        self.institution_type = InstitutionType.objects.create(
            description='chemistry')
        self.institution = Institution.objects.create(
            name='Rebec',
            description='Rebec',
            postal_address='',
            city='',
            state='',
            country=self.country,
            status=Institution.STATUS_ACTIVE,
            institution_type=self.institution_type,
            type_person="PF")
        self.institution2 = Institution.objects.create(
            name='Institution of EM_PREENCHIMENTO ct',
            description='Institution of EM_PREENCHIMENTO ct',
            postal_address='',
            city='',
            state='',
            country=self.country,
            status=Institution.STATUS_ACTIVE,
            institution_type=self.institution_type,
            type_person="PF")

        self.institution3 = Institution.objects.create(
            name='Institution in moderation',
            description='Institution in moderation',
            postal_address='',
            city='',
            state='',
            country=self.country,
            status=Institution.STATUS_MODERATION,
            institution_type=self.institution_type,
            type_person="PF")

        self.institution4 = Institution.objects.create(
            name='Other',
            description='Other',
            postal_address='',
            city='',
            state='',
            country=self.country,
            status=Institution.STATUS_ACTIVE,
            institution_type=self.institution_type,
            type_person="PF")

        from clinical_trials.models import Sponsor, ClinicalTrial
        self.clinical_trial = ClinicalTrial.objects.create(
            user=self.user)

        self.clinical_trial.sponsor_set.create(
            institution=self.institution2
        )

        self.original_success = messages.success
        messages.success = Mock()
        self.original_info = messages.info
        messages.info = Mock()
        self.original_error = messages.error
        messages.error = Mock()

    def tearDown(self):
        """Delete Institution and user"""
        self.clinical_trial.delete()
        self.clinical_trial.sponsor_set.all().delete()
        self.institution.delete()
        self.institution2.delete()
        self.institution3.delete()
        self.institution4.delete()
        self.institution_type.institution_set.all().delete()
        self.institution_type.delete()
        self.country.delete()
        self.user.delete()
        messages.success = self.original_success
        messages.info = self.original_info
        messages.error = self.original_error

    def test_institution_list_permission(self):
        """Verify Institution list permission"""
        request = self.factory.get(reverse_lazy('institution_list'))
        request.user = self.user
        response = InstitutionList.as_view()(request)
        self.assertEqual(response.status_code, 200)
        user = create_user(username=u'other_user')
        request = self.factory.get(reverse_lazy('institution_list'))
        request.user = user
        self.assertRaises(
            PermissionDenied, InstitutionList.as_view(), request)
        user.delete()

    def test_institution_detail_permission(self):
        """Verify Institution detail permission"""
        request = self.factory.get(
            reverse_lazy('institution_detail', args=(self.institution.pk,)))
        request.user = self.user
        response = InstitutionDetail.as_view()(
            request, pk=self.institution.pk)
        self.assertEqual(response.status_code, 200)
        user = create_user(username=u'other_user')
        request = self.factory.get(
            reverse_lazy('institution_detail', args=(self.institution.pk,)))
        request.user = user
        self.assertRaises(
            PermissionDenied, InstitutionDetail.as_view(),
            request, args=(self.institution.pk,))
        user.delete()

    def test_institution_create_permission(self):
        """Verify Institution create permission"""
        request = self.factory.get(reverse_lazy('institution_add'))
        request.user = self.user
        response = InstitutionCreate.as_view()(request)
        self.assertEqual(response.status_code, 200)
        user = create_user(username=u'other_user')
        request = self.factory.get(reverse_lazy('institution_add'))
        request.user = user
        self.assertRaises(
            PermissionDenied, InstitutionCreate.as_view(), request)
        user.delete()

    def test_institution_update_permission(self):
        """Verify Institution update permission"""
        request = self.factory.get(
            reverse_lazy('institution_edit', args=(self.institution.pk,)))
        request.user = self.user
        response = InstitutionUpdate.as_view()(
            request, pk=self.institution.pk)
        self.assertEqual(response.status_code, 200)
        user = create_user(username=u'otheruser')
        request = self.factory.get(
            reverse_lazy('institution_edit', args=(self.institution.pk,)))
        request.user = user
        self.assertRaises(
            PermissionDenied, InstitutionUpdate.as_view(),
            request, args=(self.institution.pk,))
        user.delete()

    def test_institution_create(self):
        """Verify Institution create"""
        self.assertEqual(Institution.objects.all().count(), 4)
        post_data = {
            'name': 'OtherName',
            'description': 'Other',
            'postal_address': '',
            'city': '',
            'state': '',
            'country': "br",
            'institution_type': self.institution_type.pk,
            'type_person': 'PF',
            'cpf': '533.117.842-19'
        }
        request_post = self.factory.post(
            reverse_lazy('institution_add'), data=post_data)
        request_post.user = self.user
        response_post = InstitutionCreate.as_view()(request_post)
        self.assertEqual(response_post.status_code, 302)
        self.assertEqual(Institution.objects.all().count(), 5)
        institution = Institution.objects.get(name=u"OtherName")
        self.assertEqual(institution.status, u'A')
        institution.delete()

    def test_institution_update(self):
        """Verify Institution update"""
        post_data = {
            'name': 'Other',
            'description': 'Other',
            'postal_address': '',
            'city': '',
            'state': '',
            'country': "br",
            'institution_type': self.institution_type.pk,
            'status': Institution.STATUS_INACTIVE,
            'type_person': 'PF',
            'cpf': '533.117.842-19'
        }
        request_post = self.factory.post(
            reverse_lazy('institution_edit', args=(self.institution.pk,)),
            data=post_data
        )
        request_post.user = self.user
        response_post = InstitutionUpdate.as_view()(
            request_post,
            pk=self.institution.pk
        )
        self.assertEqual(response_post.status_code, 302)
        #refresh local institution data
        self.institution = Institution.objects.get(pk=self.institution.pk)
        self.assertEqual(self.institution.status, Institution.STATUS_INACTIVE)


    def test_institution_moderation_choices(self):
        """
        The form must show the Moderation status choice only if the
        current status of the instance is moderation
        """
        request_get = self.factory.get(
            reverse_lazy('institution_edit', args=(self.institution.pk,)),
        )
        request_get.user = self.user
        self.institution.status = Institution.STATUS_MODERATION
        self.institution.save()
        response_get = InstitutionUpdate.as_view()(
            request_get,
            pk=self.institution.pk
        )
        self.assertTrue(
            dict(
                response_get.context_data[u'form'].fields[u'status'].choices
            ).has_key(Institution.STATUS_MODERATION)
        )

        self.institution.status = Institution.STATUS_ACTIVE
        self.institution.save()
        response_get = InstitutionUpdate.as_view()(
            request_get,
            pk=self.institution.pk
        )
        self.assertFalse(
            dict(
                response_get.context_data[u'form'].fields[u'status'].choices
            ).has_key(Institution.STATUS_MODERATION)
        )

        self.institution.status = Institution.STATUS_INACTIVE
        self.institution.save()
        response_get = InstitutionUpdate.as_view()(
            request_get,
            pk=self.institution.pk
        )
        self.assertFalse(
            dict(
                response_get.context_data[u'form'].fields[u'status'].choices
            ).has_key(Institution.STATUS_MODERATION)
        )

    def test_institution_list(self):
        """Verify Institution objects"""
        request = self.factory.get(reverse_lazy('institution_list'))
        request.user = self.user
        response = InstitutionList.as_view()(request)
        object_list = response.context_data[u'object_list']
        self.assertEqual(
            object_list.filter(pk=self.institution2.pk).count(),
             0
        )
        self.assertEqual(
            object_list.filter(pk=self.institution3.pk).count(),
             0
        )
        self.assertEqual(
            object_list.filter(pk=self.institution1.pk).count(),
             1
        )

    def test_institution_query(self):
        """Verify Institution query"""
        
        request = self.factory.get(reverse_lazy(
            'institution_list'), {'name': 'rebec'})
        request.user = self.user
        response = InstitutionList.as_view()(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.context_data['institution_list'].count(),
            1
        )
        request = self.factory.get(
            reverse_lazy('institution_list'), {'status': 'A'})
        request.user = self.user
        self.institution.status = Institution.STATUS_MODERATION
        self.institution.save()
        response = InstitutionList.as_view()(request)
        self.assertEqual(response.status_code, 200)

        self.assertEqual(
            response.context_data['institution_list'].count(),
            1
        )

    def test_institution_list(self):
        """
        Verify Institution list, it must now retrieve from database
        the institutions in moderation status and of the clinical_trials
        that are in "EM_PREENCHIMENTO" workflow status.
        """
        request = self.factory.get(
            reverse_lazy(u'institution_list')
        )
        request.user = self.user
        response = InstitutionList.as_view()(request)
        self.assertEqual(response.status_code, 200)

        self.assertEqual(
            response.context_data['institution_list'].count(),
            2
        )


    def test_institution_sponsor_create(self):
        """Verify Institution Sponsor create"""
        request = self.factory.get(reverse_lazy(
            'institution_add_sponsor', args=(self.clinical_trial.pk, 'P')))
        request.user = self.user
        response = InstitutionCreateSponsor.as_view()(
            request, clinical_trial_pk=self.clinical_trial.pk, sponsor_type='P')
        self.assertEqual(response.status_code, 200)
        post_data = {
            'name': 'OtherNameOfInstitution',
            'description': 'Other',
            'postal_address': '',
            'city': '',
            'state': '',
            'country': "br",
            'institution_type': self.institution_type.pk,
            'type_person': 'PF',
            'cpf': '093.576.451-87'
        }
        self.clinical_trial.sponsor_set.all().delete()
        self.assertEqual(Sponsor.objects.all().count(), 0)
        self.assertEqual(Institution.objects.all().count(), 4)
        self.factory = RequestFactory()
        request_post = self.factory.post(
            reverse_lazy(
                'institution_add_sponsor',
                args=(self.clinical_trial.pk, 'P',)),
            data=post_data)
        request_post.user = self.user
        self.assertEqual(messages.success.called, False)
        response_post = InstitutionCreateSponsor.as_view()(
            request_post, clinical_trial_pk=self.clinical_trial.pk,
            sponsor_type=u'P')
        self.assertEqual(messages.success.called, True)
        self.assertEqual(response_post.status_code, 302)
        self.assertEqual(Institution.objects.all().count(), 5)
        self.assertEqual(Sponsor.objects.all().count(), 1)
        Sponsor.objects.latest(u'pk').delete()
        created_institution = Institution.objects.get(
            name='OtherNameOfInstitution'
        )
        created_institution.sponsor_items.all().delete()
        created_institution.delete()

class InstitutionPendingListTestCase(TestCase):

    def setUp(self):

        self.factory = RequestFactory()
        self.user1 = User.objects.create(username=u'e',
                                        email=u'e@e.com',
                                        gender=1,
                                        race=1,
                                        country_id=u'BR')
        self.user1.groups.add(
            Group.objects.get(
                name=u'administrador')
            )

        self.country1 = Country.objects.create(
            description=u"Best Country",
            code=u"el"
            )
        self.institutiontype1 = InstitutionType.objects.create(
            description=u'T1'
            )

        self.institution1 = Institution.objects.create(
            name=u'abc',
            status=u'M',
            country=self.country1,
            institution_type=self.institutiontype1,
            type_person="PF"
        )
        self.institution2 = Institution.objects.create(
            name=u'abcd',
            status=u'A',
            country=self.country1,
            institution_type=self.institutiontype1,
            type_person="PF"
        )

        self.ct1 = ClinicalTrial.objects.create(user=self.user1)
        self.ct1.sponsor_set.create(
            sponsor_type=Sponsor.SPONSOR_TYPE_PRIMARY,
            institution=self.institution1)

        self.setup = Setup()
        self.setup.language_1_code_id = 167
        self.setup.language_1_mandatory = True
        self.setup.language_2_code_id = 57
        self.setup.language_2_mandatory = True
        self.setup.language_3_code_id = 61
        self.setup.language_3_mandatory = False
        self.setup.queue_distribution = 'lala'
        self.setup.current_position_qd0 = 1
        self.setup.save()

    def tearDown(self):

        self.institution1.delete()
        self.institution2.delete()
        self.institutiontype1.delete()
        self.user1.stateobjecthistory_set.all().delete()
        self.user1.delete()
        self.country1.delete()
        self.ct1.delete()

    def test_list_pendings(self):
        
        request = self.factory.get(
            reverse(u'institution_pending_list')
            )
        request.user = self.user1
        set_state(
            self.ct1,
            State.objects.get(pk=State.STATE_AGUARDANDO_MODERACAO)
        )
        view_return = InstitutionPendingListView.as_view()(request)
        self.assertIn(
            self.institution1, 
            view_return.context_data[u'object_list']
            )

        self.assertNotIn(
            self.institution2, 
            view_return.context_data[u'object_list']
            )

