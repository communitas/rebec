# -*- coding: utf-8 -*-

from django.test import TestCase
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from administration.views import *
from django.test.client import RequestFactory
from django.core.urlresolvers import reverse_lazy
from django.core.exceptions import PermissionDenied
from django.utils.translation import ugettext as _
from django.test.client import Client, RequestFactory
from mock import Mock, MagicMock
from django.contrib import messages

User = get_user_model()


class CTGroupCreateTesteCase(TestCase):
    """Group Update class test."""
    def setUp(self):
        """Register CTGroup"""
        self.user1 = User.objects.create(username=u'e',
                                        email=u'e@e.com',
                                        gender=1,
                                        race=1,
                                        country_id=u'BR')
        self.user1.groups.add(
            Group.objects.get(
                name=u'registrant')
            )
        self.user2 = User.objects.create(username=u'eliane',
                                        email=u'e@e.com',
                                        gender=1,
                                        race=1,
                                        country_id=u'BR')
        self.user2.groups.add(
            Group.objects.get(
                name=u'registrant')
            )
        self.country = Country.objects.create(
            description='Brasil',
            code="zs")
        self.institution_type = InstitutionType.objects.create(
            description='a')
        self.institution = Institution.objects.create(
            name='Rebec',
            description='Rebec',
            postal_address='',
            city='',
            state='',
            country=self.country,
            status='A',
            institution_type=self.institution_type)

        self.ct_group = CTGroup.objects.create(
            name='teste1',
            status_institution=None,
            description='teste',
            institution=None,
            status=1,
            visibility=1)
        self.ct_group_user = CTGroupUser.objects.create(
            status=2,
            type_user='a',
            user=self.user1,
            ct_group=self.ct_group)
        
        self.factory = RequestFactory()
        self.original_success = messages.success
        messages.success = Mock()
        self.original_info = messages.info
        messages.info = Mock()
        self.original_error = messages.error
        messages.error = Mock()
        
    def tearDown(self):
        """Delete Masking Type and user."""
        self.ct_group_user.delete()
        self.ct_group.users.all().delete()
        self.ct_group.delete()
        self.user1.ctgroups.all().delete()
        self.user2.ctgroups.all().delete()
        self.user1.delete()
        self.user2.delete()
        self.institution.ctgroups.all().delete()
        self.institution.delete()
        self.country.delete()
        self.institution_type.delete()
        messages.success = self.original_success
        messages.info = self.original_info
        messages.error = self.original_error

    def test_ct_group_institution_create(self):
        """Test CTGroup create view."""
        post_data = {
            u'users-1-id': [u''],
            u'name': [u'teste'],
            u'users-0-type_user': [u''],
            u'visibility': [u'1'],
            u'users-0-id': [u''],
            u'users-TOTAL_FORMS': [u'2'],
            u'users-INITIAL_FORMS': [u'0'],
            u'users-MAX_NUM_FORMS': [u'1000'],
            u'users-1-type_user': [u''],
            u'institution': self.institution.pk,
            u'description': [u'teste'],
        }
        request_post = self.factory.post(
            reverse_lazy('ctgroup_add'), data=post_data)
        request_post.user = self.user1
        response_post = CreateCTGroup.as_view()(request_post)
        self.assertEqual(response_post.status_code, 302)
        self.assertEqual(CTGroup.objects.all().count(), 2)
        ctgroup = CTGroup.objects.latest(u'pk')
        self.assertEqual(ctgroup.status_institution, 3)
        self.assertEqual(ctgroup.status, 1)
        ctgroup.users.all().delete()
        ctgroup.delete()

    def test_ct_group_create(self):
        """Test CTGroup create view."""
        post_data = {
            u'users-1-id': [u''],
            u'name': [u'teste'],
            u'users-0-type_user': [u''],
            u'visibility': [u'1'],
            u'users-0-id': [u''],
            u'users-TOTAL_FORMS': [u'2'],
            u'users-INITIAL_FORMS': [u'0'],
            u'users-MAX_NUM_FORMS': [u'1000'],
            u'users-1-type_user': [u''],
            u'institution': [u''],
            u'description': [u'teste'],
        }
        request_post = self.factory.post(
            reverse_lazy('ctgroup_add'), data=post_data)
        request_post.user = self.user1
        response_post = CreateCTGroup.as_view()(request_post)
        self.assertEqual(response_post.status_code, 302)
        self.assertEqual(CTGroup.objects.all().count(), 2)
        ctgroup = CTGroup.objects.latest(u'pk')
        self.assertEqual(ctgroup.status_institution, None)
        self.assertEqual(ctgroup.status, 1)
        ctgroup.users.all().delete()
        ctgroup.delete()

    def test_ct_group_user_registrant_create(self):
        """Test CTGroup create view."""
        post_data = {
            u'users-0-ct_group': [u''],
            u'name': [u'teste21'],
            u'users-0-type_user': [u'r'],
            u'visibility': [u'1'],
            u'users-0-id': [u'', u''],
            u'users-0-user': self.user2.pk,
            u'users-TOTAL_FORMS': [u'1'],
            u'users-INITIAL_FORMS': [u'0'],
            u'users-MAX_NUM_FORMS': [u'1000'],
            u'institution': self.institution.pk,
            u'description': [u'teste21']
        }
        request_post = self.factory.post(
            reverse_lazy('ctgroup_add'), data=post_data)
        request_post.user = self.user1
        response_post = CreateCTGroup.as_view()(request_post)
        self.assertEqual(response_post.status_code, 302)
        self.assertEqual(CTGroup.objects.all().count(), 2)
        ctgroup = CTGroupUser.objects.latest(u'pk')
        self.assertEqual(ctgroup.status, 2)      

    def test_ct_group_user_registrant_sem_instituicao_create(self):
        """Test CTGroup create view."""
        post_data = {
            u'users-0-ct_group': [u''],
            u'name': [u'testeregis'],
            u'users-0-type_user': [u'r'],
            u'visibility': [u'1'],
            u'users-0-id': [u'', u''],
            u'users-0-user': self.user2.pk,
            u'users-TOTAL_FORMS': [u'1'],
            u'users-INITIAL_FORMS': [u'0'],
            u'users-MAX_NUM_FORMS': [u'1000'],
            u'institution': [u''],
            u'description': [u'testeregis']
        }
        request_post = self.factory.post(
            reverse_lazy('ctgroup_add'), data=post_data)
        request_post.user = self.user1
        response_post = CreateCTGroup.as_view()(request_post)
        self.assertEqual(response_post.status_code, 302)
        self.assertEqual(CTGroup.objects.all().count(), 2)
        ctgroup = CTGroupUser.objects.latest(u'pk')
        self.assertEqual(ctgroup.status, 2)
        ctgroup.delete()

    def test_ct_group_user_adm_sem_instituicao_create(self):
        """Test CTGroup create view."""
        post_data = {
            u'users-0-ct_group': [u''],
            u'name': [u'testeadmseminstitution'],
            u'users-0-type_user': [u'a'],
            u'visibility': [u'1'],
            u'users-0-id': [u'', u''],
            u'users-0-user': self.user2.pk,
            u'users-TOTAL_FORMS': [u'1'],
            u'users-INITIAL_FORMS': [u'0'],
            u'users-MAX_NUM_FORMS': [u'1000'],
            u'institution': [u''],
            u'description': [u'testeadmseminstitution']
        }
        request_post = self.factory.post(
            reverse_lazy('ctgroup_add'), data=post_data)
        request_post.user = self.user1
        response_post = CreateCTGroup.as_view()(request_post)
        self.assertEqual(response_post.status_code, 302)
        self.assertEqual(CTGroup.objects.all().count(), 2)
        ctgroup = CTGroupUser.objects.latest(u'pk')
        self.assertEqual(ctgroup.status, 2)
        ctgroup.delete()


class CTGroupUpdateTesteCase(TestCase):
    """Group Update class test."""
    def setUp(self):
        """Register CTGroup"""
        self.user1 = User.objects.create(username=u'e',
                                        email=u'e@e.com',
                                        gender=1,
                                        race=1,
                                        country_id=u'BR')
        self.user1.groups.add(
            Group.objects.get(
                name=u'registrant')
            )
        self.user2 = User.objects.create(username=u'eliane',
                                        email=u'e@e.com',
                                        gender=1,
                                        race=1,
                                        country_id=u'BR')
        self.user2.groups.add(
            Group.objects.get(
                name=u'registrant')
            )
        self.country = Country.objects.create(
            description='Brasil',
            code="zs")
        self.institution_type = InstitutionType.objects.create(
            description='a')
        self.institution = Institution.objects.create(
            name='Rebec',
            description='Rebec',
            postal_address='',
            city='',
            state='',
            country=self.country,
            status='A',
            institution_type=self.institution_type)

        self.ct_group = CTGroup.objects.create(
            name='teste1',
            status_institution=None,
            description='teste',
            institution=None,
            status=1,
            visibility=1)
        self.ct_group_user = CTGroupUser.objects.create(
            status=2,
            type_user='a',
            user=self.user1,
            ct_group=self.ct_group)
        
        self.factory = RequestFactory()
        self.original_success = messages.success
        messages.success = Mock()
        self.original_info = messages.info
        messages.info = Mock()
        self.original_error = messages.error
        messages.error = Mock()
        
    def tearDown(self):
        """Delete Masking Type and user."""
        self.ct_group_user.delete()
        self.ct_group.users.all().delete()
        self.ct_group.delete()
        self.user1.ctgroups.all().delete()
        self.user2.ctgroups.all().delete()
        self.user1.delete()
        self.user2.delete()
        self.institution.ctgroups.all().delete()
        self.institution.delete()
        self.country.delete()
        self.institution_type.delete()
        messages.success = self.original_success
        messages.info = self.original_info
        messages.error = self.original_error

    def test_ctgroup_institution_update_add_adm(self):
        """
        Test: when editing a group that owns the
        institution status is pending.
        """
        user3 = User.objects.create(username=u'elianeadm',
                                        email=u'e@e.com',
                                        gender=1,
                                        race=1,
                                        country_id=u'BR')
        user3.groups.add(
            Group.objects.get(
                name=u'registrant')
            )
        ct_group_user = CTGroupUser.objects.create(
            status=2,
            type_user='a',
            user=user3,
            ct_group=self.ct_group)

        alter_post ={
        u'users-1-ct_group': [u''],
        u'institution': self.institution.pk,
        u'users-INITIAL_FORMS': [u'1'],
        u'users-0-id': ct_group_user.pk,
        u'users-1-user': self.user2.pk,
        u'description': [u'teste'],
        u'users-1-type_user': [u'a'],
        u'users-1-id': '',
        u'visibility': [u'1'],
        u'users-0-user': user3.pk,
        u'users-0-type_user': [u'a'],
        u'users-0-ct_group': self.ct_group.pk,
        u'name': [u'teste'],
        u'users-TOTAL_FORMS': [u'2'],
        u'users-MAX_NUM_FORMS': [u'1000']}


        request_post = self.factory.post(reverse_lazy('ctgroup_edit', args=(self.ct_group.pk, )), data=alter_post)
        request_post.user = user3
        response_post = UpdateCTGroup.as_view()(request_post, pk=self.ct_group.pk)
        self.assertEqual(response_post.status_code, 302)
        ctgroup = CTGroupUser.objects.latest(u'pk')
        self.assertEqual(ctgroup.status, 2)
        ct_group_user.delete()
        user3.delete()

    def test_ctgroup_institution_update_pendent(self):
        """
        Test: when editing a group that owns the
        institution status is pending.
        """
        user3 = User.objects.create(username=u'elianeadm',
                                        email=u'e@e.com',
                                        gender=1,
                                        race=1,
                                        country_id=u'BR')
        user3.groups.add(
            Group.objects.get(
                name=u'registrant')
            )
        ct_group_3 = CTGroup.objects.create(
            name='teste1',
            status_institution=CTGroup.STATUS_INSTITUTION_PENDING,
            description='teste',
            institution=self.institution,
            status=1,
            visibility=1
        )

        ct_group_user_3 = CTGroupUser.objects.create(
            status=2,
            type_user='a',
            user=user3,
            ct_group=ct_group_3)

        

        alter_post ={
        u'users-1-ct_group': [u''],
        u'institution': [u''],
        u'users-INITIAL_FORMS': [u'1'],
        u'users-0-id': ct_group_user_3.pk,
        u'description': [u'teste'],
        u'visibility': [u'1'],
        u'users-0-user': user3.pk,
        u'users-0-type_user': [u'a'],
        u'users-0-ct_group': ct_group_3.pk,
        u'name': [u'teste'],
        u'users-TOTAL_FORMS': [u'1'],
        u'users-MAX_NUM_FORMS': [u'1000']}


        request_post = self.factory.post(reverse_lazy('ctgroup_edit', args=(ct_group_3.pk, )), data=alter_post)
        request_post.user = user3
        response_post = UpdateCTGroup.as_view()(request_post, pk=ct_group_3.pk)
        self.assertEqual(response_post.status_code, 302)
        ct_group_user_3.delete()
        ct_group_3.delete()
        user3.delete()

    def test_ctgroup_institution_update_pendent_2(self):
        """
        Test: when editing a group that owns the
        institution status is pending.
        """
        user3 = User.objects.create(username=u'elianeadm',
                                        email=u'e@e.com',
                                        gender=1,
                                        race=1,
                                        country_id=u'BR')
        user3.groups.add(
            Group.objects.get(
                name=u'registrant')
            )
        ct_group_3 = CTGroup.objects.create(
            name='teste1',
            status_institution=CTGroup.STATUS_INSTITUTION_PENDING,
            description='teste',
            institution=self.institution,
            status=1,
            visibility=1
        )

        ct_group_user_3 = CTGroupUser.objects.create(
            status=2,
            type_user='a',
            user=user3,
            ct_group=ct_group_3)

        

        alter_post ={
        u'users-1-ct_group': [u''],
        u'institution': self.institution.pk,
        u'users-INITIAL_FORMS': [u'1'],
        u'users-0-id': ct_group_user_3.pk,
        u'description': [u'teste'],
        u'visibility': [u'1'],
        u'users-0-user': user3.pk,
        u'users-0-type_user': [u'a'],
        u'users-0-ct_group': ct_group_3.pk,
        u'name': [u'teste'],
        u'users-TOTAL_FORMS': [u'1'],
        u'users-MAX_NUM_FORMS': [u'1000']}


        request_post = self.factory.post(reverse_lazy('ctgroup_edit', args=(ct_group_3.pk, )), data=alter_post)
        request_post.user = user3
        response_post = UpdateCTGroup.as_view()(request_post, pk=ct_group_3.pk)
        self.assertEqual(response_post.status_code, 302)
        ctgroup = CTGroup.objects.get(pk=ct_group_3.pk)
        self.assertEqual(ctgroup.status_institution, CTGroup.STATUS_INSTITUTION_PENDING)
        ct_group_user_3.delete()
        ct_group_3.delete()
        user3.delete()

    def test_ctgroup_institution_update_none(self):
        """
        Test: when editing a group that owns the
        institution status is pending.
        """
        user3 = User.objects.create(username=u'elianeadm',
                                        email=u'e@e.com',
                                        gender=1,
                                        race=1,
                                        country_id=u'BR')
        user3.groups.add(
            Group.objects.get(
                name=u'registrant')
            )
        ct_group_3 = CTGroup.objects.create(
            name='teste1',
            status_institution=None,
            description='teste',
            institution=None,
            status=1,
            visibility=1
        )

        ct_group_user_3 = CTGroupUser.objects.create(
            status=2,
            type_user='a',
            user=user3,
            ct_group=ct_group_3)

        

        alter_post ={
        u'users-1-ct_group': [u''],
        u'institution': [u''],
        u'users-INITIAL_FORMS': [u'1'],
        u'users-0-id': ct_group_user_3.pk,
        u'description': [u'teste'],
        u'visibility': [u'1'],
        u'users-0-user': user3.pk,
        u'users-0-type_user': [u'a'],
        u'users-0-ct_group': ct_group_3.pk,
        u'name': [u'teste'],
        u'users-TOTAL_FORMS': [u'1'],
        u'users-MAX_NUM_FORMS': [u'1000']}


        request_post = self.factory.post(reverse_lazy('ctgroup_edit', args=(ct_group_3.pk, )), data=alter_post)
        request_post.user = user3
        response_post = UpdateCTGroup.as_view()(request_post, pk=ct_group_3.pk)
        self.assertEqual(response_post.status_code, 302)
        ctgroup = CTGroup.objects.get(pk=ct_group_3.pk)
        self.assertEqual(ctgroup.status_institution, None)
        ct_group_user_3.delete()
        ct_group_3.delete()
        user3.delete()

    def test_ctgroup_institution_update_none_aproved(self):
        """
        Test: when editing a group that owns the
        institution status is pending.
        """
        user3 = User.objects.create(username=u'elianeadm',
                                        email=u'e@e.com',
                                        gender=1,
                                        race=1,
                                        country_id=u'BR')
        user3.groups.add(
            Group.objects.get(
                name=u'registrant')
            )
        ct_group_3 = CTGroup.objects.create(
            name='teste1',
            status_institution=CTGroup.STATUS_INSTITUTION_APPROVED,
            description='teste',
            institution=self.institution,
            status=1,
            visibility=1
        )

        ct_group_user_3 = CTGroupUser.objects.create(
            status=2,
            type_user='a',
            user=user3,
            ct_group=ct_group_3)

        

        alter_post ={
        u'users-1-ct_group': [u''],
        u'institution': [u''],
        u'users-INITIAL_FORMS': [u'1'],
        u'users-0-id': ct_group_user_3.pk,
        u'description': [u'teste'],
        u'visibility': [u'1'],
        u'users-0-user': user3.pk,
        u'users-0-type_user': [u'a'],
        u'users-0-ct_group': ct_group_3.pk,
        u'name': [u'teste'],
        u'users-TOTAL_FORMS': [u'1'],
        u'users-MAX_NUM_FORMS': [u'1000']}


        request_post = self.factory.post(reverse_lazy('ctgroup_edit', args=(ct_group_3.pk, )), data=alter_post)
        request_post.user = user3
        response_post = UpdateCTGroup.as_view()(request_post, pk=ct_group_3.pk)
        self.assertEqual(response_post.status_code, 302)
        ctgroup = CTGroup.objects.get(pk=ct_group_3.pk)
        self.assertEqual(ctgroup.status_institution, None)
        ct_group_user_3.delete()
        ct_group_3.delete()
        user3.delete()

    def test_ctgroup_institution_update_aproved(self):
        """
        Test: when editing a group that owns the
        institution status is pending.
        """
        user3 = User.objects.create(username=u'elianeadm',
                                        email=u'e@e.com',
                                        gender=1,
                                        race=1,
                                        country_id=u'BR')
        user3.groups.add(
            Group.objects.get(
                name=u'registrant')
            )
        user4 = User.objects.create(username=u'elianeadmaaa',
                                        email=u'e@e.com',
                                        gender=1,
                                        race=1,
                                        country_id=u'BR')
        user4.groups.add(
            Group.objects.get(
                name=u'registrant')
            )
        ct_group_3 = CTGroup.objects.create(
            name='teste1',
            status_institution=CTGroup.STATUS_INSTITUTION_APPROVED,
            description='teste',
            institution=self.institution,
            status=1,
            visibility=1
        )

        ct_group_user_3 = CTGroupUser.objects.create(
            status=2,
            type_user='a',
            user=user3,
            ct_group=ct_group_3)

        

        alter_post ={
        u'users-1-ct_group': [u''],
        u'institution': self.institution.pk,
        u'users-INITIAL_FORMS': [u'1'],
        u'users-0-id': ct_group_user_3.pk,
        u'users-1-id': '',
        u'description': [u'teste'],
        u'visibility': [u'1'],
        u'users-0-user': user3.pk,
        u'users-0-type_user': [u'a'],
        u'users-0-ct_group': ct_group_3.pk,
        u'users-1-user': user4.pk,
        u'users-1-type_user': [u'a'],
        u'users-1-ct_group': ct_group_3.pk,
        u'name': [u'teste'],
        u'users-TOTAL_FORMS': [u'2'],
        u'users-MAX_NUM_FORMS': [u'1000']}


        request_post = self.factory.post(reverse_lazy('ctgroup_edit', args=(ct_group_3.pk, )), data=alter_post)
        request_post.user = user3
        response_post = UpdateCTGroup.as_view()(request_post, pk=ct_group_3.pk)
        self.assertEqual(response_post.status_code, 302)
        ctgroup = CTGroupUser.objects.latest(u'pk')
        self.assertEqual(ctgroup.status, CTGroupUser.STATUS_PENDING_APROV_ADM)
        ct_group_user_3.delete()
        ct_group_3.users.all().delete()
        ct_group_3.delete()
        user3.delete()
        user4.delete()

    def test_ctgroup_institution_update_aproved_add_user(self):
        """
        Test: when editing a group that owns the
        institution status is pending.
        """
        user3 = User.objects.create(username=u'elianeadm',
                                        email=u'e@e.com',
                                        gender=1,
                                        race=1,
                                        country_id=u'BR')
        user3.groups.add(
            Group.objects.get(
                name=u'registrant')
            )
        user4 = User.objects.create(username=u'elianeadmaaa',
                                        email=u'e@e.com',
                                        gender=1,
                                        race=1,
                                        country_id=u'BR')
        user4.groups.add(
            Group.objects.get(
                name=u'registrant')
            )
        ct_group_3 = CTGroup.objects.create(
            name='teste1',
            status_institution=CTGroup.STATUS_INSTITUTION_PENDING,
            description='teste',
            institution=self.institution,
            status=1,
            visibility=1
        )

        ct_group_user_3 = CTGroupUser.objects.create(
            status=2,
            type_user='a',
            user=user3,
            ct_group=ct_group_3)

        

        alter_post ={
        u'users-1-ct_group': [u''],
        u'institution': self.institution.pk,
        u'users-INITIAL_FORMS': [u'1'],
        u'users-0-id': ct_group_user_3.pk,
        u'users-1-id': '',
        u'description': [u'teste'],
        u'visibility': [u'1'],
        u'users-0-user': user3.pk,
        u'users-0-type_user': [u'a'],
        u'users-0-ct_group': ct_group_3.pk,
        u'users-1-user': user4.pk,
        u'users-1-type_user': [u'a'],
        u'users-1-ct_group': ct_group_3.pk,
        u'name': [u'teste'],
        u'users-TOTAL_FORMS': [u'2'],
        u'users-MAX_NUM_FORMS': [u'1000']}


        request_post = self.factory.post(reverse_lazy('ctgroup_edit', args=(ct_group_3.pk, )), data=alter_post)
        request_post.user = user3
        response_post = UpdateCTGroup.as_view()(request_post, pk=ct_group_3.pk)
        self.assertEqual(response_post.status_code, 302)
        ctgroup = CTGroupUser.objects.latest(u'pk')
        self.assertEqual(ctgroup.status, CTGroupUser.STATUS_ACTIVE)
        ct_group_user_3.delete()
        ct_group_3.users.all().delete()
        ct_group_3.delete()
        user3.delete()
        user4.delete()

    def test_ctgroup_update_aproved_add_user(self):
        """
        Test: when editing a group that owns the
        institution status is pending.
        """
        user3 = User.objects.create(username=u'elianeadm',
                                        email=u'e@e.com',
                                        gender=1,
                                        race=1,
                                        country_id=u'BR')
        user3.groups.add(
            Group.objects.get(
                name=u'registrant')
            )
        user4 = User.objects.create(username=u'elianeadmaaa',
                                        email=u'e@e.com',
                                        gender=1,
                                        race=1,
                                        country_id=u'BR')
        user4.groups.add(
            Group.objects.get(
                name=u'registrant')
            )
        ct_group_3 = CTGroup.objects.create(
            name='teste1',
            status_institution=None,
            description='teste',
            institution=None,
            status=1,
            visibility=1
        )

        ct_group_user_3 = CTGroupUser.objects.create(
            status=2,
            type_user='a',
            user=user3,
            ct_group=ct_group_3)

        

        alter_post ={
        u'users-1-ct_group': [u''],
        u'institution': [u''],
        u'users-INITIAL_FORMS': [u'1'],
        u'users-0-id': ct_group_user_3.pk,
        u'users-1-id': '',
        u'description': [u'teste'],
        u'visibility': [u'1'],
        u'users-0-user': user3.pk,
        u'users-0-type_user': [u'a'],
        u'users-0-ct_group': ct_group_3.pk,
        u'users-1-user': user4.pk,
        u'users-1-type_user': [u'a'],
        u'users-1-ct_group': ct_group_3.pk,
        u'name': [u'teste'],
        u'users-TOTAL_FORMS': [u'2'],
        u'users-MAX_NUM_FORMS': [u'1000']}


        request_post = self.factory.post(reverse_lazy('ctgroup_edit', args=(ct_group_3.pk, )), data=alter_post)
        request_post.user = user3
        response_post = UpdateCTGroup.as_view()(request_post, pk=ct_group_3.pk)
        self.assertEqual(response_post.status_code, 302)
        ctgroup = CTGroupUser.objects.latest(u'pk')
        self.assertEqual(ctgroup.status, CTGroupUser.STATUS_ACTIVE)
        ct_group_user_3.delete()
        ct_group_3.users.all().delete()
        ct_group_3.delete()
        user3.delete()
        user4.delete()

    def test_ctgroup_update_aproved_add_user_regis(self):
        """
        Test: when editing a group that owns the
        institution status is pending.
        """
        user3 = User.objects.create(username=u'elianeadm',
                                        email=u'e@e.com',
                                        gender=1,
                                        race=1,
                                        country_id=u'BR')
        user3.groups.add(
            Group.objects.get(
                name=u'registrant')
            )
        user4 = User.objects.create(username=u'elianeadmaaa',
                                        email=u'e@e.com',
                                        gender=1,
                                        race=1,
                                        country_id=u'BR')
        user4.groups.add(
            Group.objects.get(
                name=u'registrant')
            )
        ct_group_3 = CTGroup.objects.create(
            name='teste1',
            status_institution=None,
            description='teste',
            institution=None,
            status=1,
            visibility=1
        )

        ct_group_user_3 = CTGroupUser.objects.create(
            status=2,
            type_user='a',
            user=user3,
            ct_group=ct_group_3)

        

        alter_post ={
        u'users-1-ct_group': [u''],
        u'institution': [u''],
        u'users-INITIAL_FORMS': [u'1'],
        u'users-0-id': ct_group_user_3.pk,
        u'users-1-id': '',
        u'description': [u'teste'],
        u'visibility': [u'1'],
        u'users-0-user': user3.pk,
        u'users-0-type_user': [u'a'],
        u'users-0-ct_group': ct_group_3.pk,
        u'users-1-user': user4.pk,
        u'users-1-type_user': [u'r'],
        u'users-1-ct_group': ct_group_3.pk,
        u'name': [u'teste'],
        u'users-TOTAL_FORMS': [u'2'],
        u'users-MAX_NUM_FORMS': [u'1000']}


        request_post = self.factory.post(reverse_lazy('ctgroup_edit', args=(ct_group_3.pk, )), data=alter_post)
        request_post.user = user3
        response_post = UpdateCTGroup.as_view()(request_post, pk=ct_group_3.pk)
        self.assertEqual(response_post.status_code, 302)
        ctgroup = CTGroupUser.objects.latest(u'pk')
        self.assertEqual(ctgroup.status, CTGroupUser.STATUS_ACTIVE)
        ct_group_user_3.delete()
        ct_group_3.users.all().delete()
        ct_group_3.delete()
        user3.delete()
        user4.delete()

class CTGroupParticipateTesteCase(TestCase):
    """Group Update class test."""
    def setUp(self):
        """Register CTGroup"""
        self.user1 = User.objects.create(username=u'e',
                                        email=u'e@e.com',
                                        gender=1,
                                        race=1,
                                        country_id=u'BR')
        self.user1.groups.add(
            Group.objects.get(
                name=u'registrant')
            )
        self.user2 = User.objects.create(username=u'eliane',
                                        email=u'e@e.com',
                                        gender=1,
                                        race=1,
                                        country_id=u'BR')
        self.user2.groups.add(
            Group.objects.get(
                name=u'registrant')
            )
        self.user3 = User.objects.create(username=u'participate',
                                        email=u'e@e.com',
                                        gender=1,
                                        race=1,
                                        country_id=u'BR')
        self.user3.groups.add(
            Group.objects.get(
                name=u'registrant')
            )
        self.country = Country.objects.create(
            description='Brasil',
            code="zs")
        self.institution_type = InstitutionType.objects.create(
            description='a')
        self.institution = Institution.objects.create(
            name='Rebec',
            description='Rebec',
            postal_address='',
            city='',
            state='',
            country=self.country,
            status='A',
            institution_type=self.institution_type)

        self.ct_group = CTGroup.objects.create(
            name='teste1',
            status_institution=None,
            description='teste',
            institution=None,
            status=1,
            visibility=1)
        self.ct_group_user = CTGroupUser.objects.create(
            status=2,
            type_user='a',
            user=self.user1,
            ct_group=self.ct_group)
        
        self.factory = RequestFactory()
        self.original_success = messages.success
        messages.success = Mock()
        self.original_info = messages.info
        messages.info = Mock()
        self.original_error = messages.error
        messages.error = Mock()
        
    def tearDown(self):
        """Delete Masking Type and user."""
        self.ct_group_user.delete()
        self.ct_group.users.all().delete()
        self.ct_group.delete()
        self.user1.ctgroups.all().delete()
        self.user2.ctgroups.all().delete()
        self.user3.ctgroups.all().delete()
        self.user1.delete()
        self.user2.delete()
        self.user3.delete()
        self.institution.ctgroups.all().delete()
        self.institution.delete()
        self.country.delete()
        self.institution_type.delete()
        messages.success = self.original_success
        messages.info = self.original_info
        messages.error = self.original_error
        

    def test_ctgroup_sign_out_adm_dois(self):
        """
        Test ctgroup:
        Teste view sign out.
        """
        
        ct_group_adm = CTGroup.objects.create(
            name='teste1',
            status_institution=None,
            description='teste',
            institution=None,
            status=1,
            visibility=1
        )
        ct_group_user_adm_dois = CTGroupUser.objects.create(
            status=2,
            type_user='a',
            user=self.user1,
            ct_group=ct_group_adm,
        )
        
        request = self.factory.post(reverse_lazy(
            'ctgroup_sign_out',
            args=(ct_group_user_adm_dois.pk, 'S')),
        data={u'confirm': u'yes'})
        request.user = self.user1
        response = CTGroupSignOut.as_view()(
            request, pk=ct_group_user_adm_dois.pk, decision='S')
        self.assertEqual(response.status_code, 302)
        self.assertEqual(messages.error.called, True)
        ctgroup = CTGroupUser.objects.get(pk=ct_group_user_adm_dois.pk)
        self.assertEqual(ctgroup.status, 2)
        ct_group_user_adm_dois.delete()
        ct_group_adm.users.all().delete()
        ct_group_adm.delete()

    def test_ctgroup_sign_out_resgister(self):
        """
        Test ctgroup:
        Teste view sign out.
        """
        
        ctgroup_registrante = CTGroup.objects.create(
            name='teste1jjj',
            status_institution=None,
            description='testefff',
            institution=None,
            status=1,
            visibility=1
        )

        ct_group_user_register = CTGroupUser.objects.create(
            status=2,
            type_user='r',
            user=self.user2,
            ct_group=ctgroup_registrante
        )

        request = self.factory.post(reverse_lazy(
            'ctgroup_sign_out',
            args=(ct_group_user_register.pk, 'S')),
        data={u'confirm': u'yes'})
        request.user = self.user2
        response = CTGroupSignOut.as_view()(
            request, pk=ct_group_user_register.pk, decision='S')
        self.assertEqual(response.status_code, 302)
        self.assertEqual(messages.success.called, True)
        ctgroup = CTGroupUser.objects.get(pk=ct_group_user_register.pk)
        self.assertEqual(ctgroup.status, 4)
        ct_group_user_register.delete()
        ctgroup_registrante.delete()

   
class CTGroupManagerTesteCase(TestCase):
    """Group Update class test."""
    def setUp(self):
        """Register CTGroup"""
        self.user1 = User.objects.create(username=u'e',
                                        email=u'e@e.com',
                                        gender=1,
                                        race=1,
                                        country_id=u'BR')
        self.user1.groups.add(
            Group.objects.get(
                name=u'registrant')
            )
        self.user2 = User.objects.create(username=u'eliane',
                                        email=u'e@e.com',
                                        gender=1,
                                        race=1,
                                        country_id=u'BR')
        self.user2.groups.add(
            Group.objects.get(
                name=u'registrant')
            )
        self.user3 = User.objects.create(username=u'participate',
                                        email=u'e@e.com',
                                        gender=1,
                                        race=1,
                                        country_id=u'BR')
        self.user3.groups.add(
            Group.objects.get(
                name=u'registrant')
            )
        self.country = Country.objects.create(
            description='Brasil',
            code="zs")
        self.institution_type = InstitutionType.objects.create(
            description='a')
        self.institution = Institution.objects.create(
            name='Rebec',
            description='Rebec',
            postal_address='',
            city='',
            state='',
            country=self.country,
            status='A',
            institution_type=self.institution_type)

        self.ct_group = CTGroup.objects.create(
            name='teste1',
            status_institution=None,
            description='teste',
            institution=None,
            status=1,
            visibility=1)
        self.ct_group_user = CTGroupUser.objects.create(
            status=2,
            type_user='a',
            user=self.user1,
            ct_group=self.ct_group)
        
        self.factory = RequestFactory()
        self.original_success = messages.success
        messages.success = Mock()
        self.original_info = messages.info
        messages.info = Mock()
        self.original_error = messages.error
        messages.error = Mock()
        
    def tearDown(self):
        """Delete Masking Type and user."""
        self.ct_group_user.delete()
        self.ct_group.users.all().delete()
        self.ct_group.delete()
        self.user1.ctgroups.all().delete()
        self.user2.ctgroups.all().delete()
        self.user3.ctgroups.all().delete()
        self.user1.delete()
        self.user2.delete()
        self.user3.delete()
        self.institution.ctgroups.all().delete()
        self.institution.delete()
        self.country.delete()
        self.institution_type.delete()
        messages.success = self.original_success
        messages.info = self.original_info
        messages.error = self.original_error

    def test_manager_ctgroup_accepted_view(self):
        """
        Test functionality to accept user group.
        User requested to join the group and is pending test
        accepts this usuauário.
        """
        
        ctgroup_manager = CTGroup.objects.create(
            name='teste1123',
            status_institution=None,
            description='testekj',
            institution=None,
            status=1,
            visibility=1
        )

        ct_group_user_manager = CTGroupUser.objects.create(
            status=1,
            type_user='r',
            user=self.user3,
            ct_group=ctgroup_manager
        )

        ct_group_user_manager_adm = CTGroupUser.objects.create(
            status=2,
            type_user='a',
            user=self.user1,
            ct_group=ctgroup_manager
        )

        request = self.factory.post(reverse_lazy(
            'ctgroup_manager_decision',
            args=(ct_group_user_manager.pk, 'C')),
        data={u'confirm': u'yes'})
        request.user = self.user1
        response = ManagerCTGroupDecision.as_view()(
            request, pk=ct_group_user_manager.pk, decision='C')
        self.assertEqual(response.status_code, 302)
        self.assertEqual(messages.success.called, True)
        ctgroup = CTGroupUser.objects.get(pk=ct_group_user_manager.pk)
        self.assertEqual(ctgroup.status, 2)
        ct_group_user_manager.delete()
        ctgroup_manager.users.all().delete()
        ctgroup_manager.delete()

    def test_manager_ctgroup_not_accepted_exclusion_view(self):
        """
        Test functionality lets not leave the user group.
        The user asked to leave the group, but do not let administrator.
        """
        ctgroup_manager = CTGroup.objects.create(
            name='teste1çlçk',
            status_institution=None,
            description='teste',
            institution=None,
            status=1,
            visibility=1
        )

        ct_group_user_manager = CTGroupUser.objects.create(
            status=4,
            type_user='r',
            user=self.user2,
            ct_group=ctgroup_manager
        )

        ct_group_user_manager_adm = CTGroupUser.objects.create(
            status=2,
            type_user='a',
            user=self.user1,
            ct_group=ctgroup_manager
        )

        request = self.factory.post(reverse_lazy(
            'ctgroup_manager_decision',
            args=(ct_group_user_manager.pk, 'C')),
        data={u'confirm': u'yes'})
        request.user = self.user1
        response = ManagerCTGroupDecision.as_view()(
            request, pk=ct_group_user_manager.pk, decision='C')
        self.assertEqual(response.status_code, 302)
        self.assertEqual(messages.success.called, True)
        ctgroup = CTGroupUser.objects.get(pk=ct_group_user_manager.pk)
        self.assertEqual(ctgroup.status, 2)
        ct_group_user_manager.delete()
        ctgroup_manager.users.all().delete()
        ctgroup_manager.delete()

    def test_manager_ctgroup_not_accepted_participate_view(self):
        """
        Test functionality: User agrees not to join the group.
        User requested to join the group, but rejects administrator.
        """
        user_teste = User.objects.create_user(
            username='teste',
            email='user@email.com',
            password='pass',
            gender=1,
            race=1,
            country_id=u'BR'
        )
        
        ctgroup_manager = CTGroup.objects.create(
            name='teste1',
            description='teste',
            institution=None,
            status=1,
            visibility=1
        )

        ct_group_user_manager = CTGroupUser.objects.create(
            status=1,
            type_user='r',
            user=user_teste,
            ct_group=ctgroup_manager)

        ct_group_user_manager_adm = CTGroupUser.objects.create(
            
            status=2,
            type_user='a',
            user=self.user1,
            ct_group=ctgroup_manager)

        request = self.factory.post(reverse_lazy(
            'ctgroup_manager_decision',
            args=(ct_group_user_manager.pk, 'R')),
        data={u'confirm': u'yes'})
        request.user = self.user1
        response = ManagerCTGroupDecision.as_view()(
            request, pk=ct_group_user_manager.pk, decision='R')
        self.assertEqual(response.status_code, 302)
        self.assertEqual(messages.success.called, True)
        ctgroup = CTGroupUser.objects.get(pk=ct_group_user_manager.pk)
        self.assertEqual(ctgroup.status, 3)
        ct_group_user_manager.delete()
        user_teste.delete()
        ctgroup_manager.users.all().delete()
        ctgroup_manager.delete()

    def test_manager_ctgroup_exclude_user_view(self):
        """
        Test functionality: User requests to leave the Group.
        The administrator leaves the usuáro leave the group.
        """
        user_teste = User.objects.create_user(
            username='teste',
            email='user@email.com',
            password='pass',
            gender=1,
            race=1,
            country_id=u'BR'
        )
        
        ctgroup_manager = CTGroup.objects.create(
            name='teste1',
            description='teste',
            institution=None,
            status=1,
            visibility=1)

        ct_group_user_manager = CTGroupUser.objects.create(
            status=4,
            type_user='r',
            user=user_teste,
            ct_group=ctgroup_manager)

        ct_group_user_manager_adm = CTGroupUser.objects.create(
            
            status=2,
            type_user='a',
            user=self.user1,
            ct_group=ctgroup_manager)

        request = self.factory.post(reverse_lazy(
            'ctgroup_manager_decision',
            args=(ct_group_user_manager.pk, 'R')),
        data={u'confirm': u'yes'})
        request.user = self.user1
        response = ManagerCTGroupDecision.as_view()(
            request, pk=ct_group_user_manager.pk, decision='R')
        self.assertEqual(response.status_code, 302)
        self.assertEqual(messages.success.called, True)
        ctgroup = CTGroupUser.objects.get(pk=ct_group_user_manager.pk)
        self.assertEqual(ctgroup.status, 3)
        ct_group_user_manager.delete()
        user_teste.delete()
        ctgroup_manager.users.all().delete()
        ctgroup_manager.delete()

    def test_detail_ct_group(self):
        """Verify CTGroup detail."""
        request = self.factory.get(
            reverse_lazy('ctgroup_detail', args=(self.ct_group.pk,)))
        request.user = self.user1
        response = CTGroupDetail.as_view()(
            request, pk=self.ct_group.pk)
        self.assertEqual(response.status_code, 200)
        ctgroup = CTGroup.objects.get(pk=self.ct_group.pk)
        self.assertEqual(
            response.context_data['ctgroup_formset'].count(), 1)

    def test_detail_manager_ct_group(self):
        """Verify CTGroup detail."""
        request = self.factory.get(
            reverse_lazy('ctgroup_manager', args=(self.ct_group.pk,)))
        request.user = self.user1
        response = ManagerCTGroup.as_view()(
            request, pk=self.ct_group.pk)
        self.assertEqual(response.status_code, 200)
        ctgroup = CTGroup.objects.get(pk=self.ct_group.pk)
        self.assertEqual(
            response.context_data['ctgroup_formset'].count(), 1)

    def test_ct_group_block(self):
        """Test CTGroup: Administrator user blocks"""
        user_teste = User.objects.create_user(
            username='teste',
            email='user@email.com',
            password='pass',
            gender=1,
            race=1,
            country_id=u'BR'
        )
        ctgroup_block = CTGroup.objects.create(
            name='teste1',
            status_institution=None,
            description='teste',
            institution=None,
            status=1,
            visibility=1)

        ct_group_user_block_um = CTGroupUser.objects.create(
            status=2,
            type_user='a',
            user=self.user1,
            ct_group=ctgroup_block)

        ct_group_user_block_dois = CTGroupUser.objects.create(
            
            status=2,
            type_user='r',
            user=user_teste,
            ct_group=ctgroup_block)

        request_post = self.factory.post(reverse_lazy(
            'ctgroup_block',
            args=(ct_group_user_block_dois.pk, 'B')), data={u'confirm': u'yes'})
        request_post.user = self.user1
        response_post = ManagerCTGroupBlock.as_view()(request_post,
            pk=ct_group_user_block_dois.pk,
            decision='B')
        self.assertEqual(response_post.status_code, 302)
        self.assertEqual(messages.success.called, True)
        ctgroup = CTGroupUser.objects.get(pk=ct_group_user_block_dois.pk)
        self.assertEqual(ctgroup.status, 3)
        ct_group_user_block_dois.delete()
        ct_group_user_block_um.delete()
        user_teste.delete()
        ctgroup_block.delete()

    def test_ct_group_block_adm(self):
        """Test CTGroup: Administrator tries to block."""
        user_teste = User.objects.create_user(
            username='teste',
            email='user@email.com',
            password='pass',
            gender=1,
            race=1,
            country_id=u'BR'
        )
        ctgroup_block = CTGroup.objects.create(
            name='teste1',
            status_institution=None,
            description='teste',
            institution=None,
            status=1,
            visibility=1)

        ct_group_user_block_um = CTGroupUser.objects.create(
            status=2,
            type_user='a',
            user=self.user1,
            ct_group=ctgroup_block)

        ct_group_user_block_dois = CTGroupUser.objects.create(
            
            status=2,
            type_user='a',
            user=user_teste,
            ct_group=ctgroup_block)

        request_post = self.factory.post(reverse_lazy(
            'ctgroup_block',
            args=(ct_group_user_block_dois.pk, 'B')), data={u'confirm': u'yes'})
        request_post.user = user_teste
        response_post = ManagerCTGroupBlock.as_view()(request_post,
            pk=ct_group_user_block_dois.pk,
            decision='B')
        self.assertEqual(response_post.status_code, 302)
        self.assertEqual(messages.error.called, True)
        ct_group_user_block_dois.delete()
        ct_group_user_block_um.delete()
        user_teste.delete()
        ctgroup_block.delete()

    def test_ct_group_unlock(self):
        """Test CTGroup: Administrator unlocks user"""
        user_teste = User.objects.create_user(
            username='teste',
            email='user@email.com',
            password='pass',
            gender=1,
            race=1,
            country_id=u'BR'
            )
        ctgroup_block = CTGroup.objects.create(
            name='teste1',
            status_institution=None,
            description='teste',
            institution=None,
            status=1,
            visibility=1)

        ct_group_user_block_um = CTGroupUser.objects.create(
            status=2,
            type_user='a',
            user=self.user1,
            ct_group=ctgroup_block)

        ct_group_user_block_dois = CTGroupUser.objects.create(
            
            status=3,
            type_user='r',
            user=user_teste,
            ct_group=ctgroup_block)

        request_post = self.factory.post(reverse_lazy(
            'ctgroup_block',
            args=(ct_group_user_block_dois.pk, 'D')), data={u'confirm': u'yes'})
        request_post.user = self.user1
        response_post = ManagerCTGroupBlock.as_view()(request_post,
            pk=ct_group_user_block_dois.pk,
            decision='D')
        self.assertEqual(response_post.status_code, 302)
        self.assertEqual(messages.success.called, True)
        ctgroup = CTGroupUser.objects.get(pk=ct_group_user_block_dois.pk)
        self.assertEqual(ctgroup.status, 2)
        ct_group_user_block_dois.delete()
        ct_group_user_block_um.delete()
        user_teste.delete()
        ctgroup_block.delete()