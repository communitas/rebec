#-*- coding: utf-8 -*-
from django.test import TestCase
from django.test.client import RequestFactory

from django.contrib.auth import get_user_model
from administration.models import Setup
from administration.views import *
from django.core.urlresolvers import reverse, resolve
from mock import Mock
from django.contrib import messages

from administration.models import *
from django.contrib.auth.models import Group
User = get_user_model()
from django.core.exceptions import PermissionDenied
from clinical_trials.models import ClinicalTrial, Sponsor
from administration import views
from workflows.utils import set_state, get_state
from workflows.models import State, StateObjectHistory


class GroupInstitutionPendingListTestCase(TestCase):

    def setUp(self):

        self.factory = RequestFactory()
        self.user1 = User.objects.create(username=u'e',
                                        email=u'e@e.com',
                                        gender=1,
                                        race=1,
                                        country_id=u'BR')
        self.user1.groups.add(
            Group.objects.get(
                name=u'administrador')
            )

        self.country1 = Country.objects.create(
            description=u"Best Country",
            code=u"el"
            )
        self.institutiontype1 = InstitutionType.objects.create(
            description=u'T1'
            )

        self.institution1 = Institution.objects.create(
            name=u'abc',
            status=u'M',
            country=self.country1,
            institution_type=self.institutiontype1)
        self.institution2 = Institution.objects.create(
            name=u'abcd',
            status=u'A',
            country=self.country1,
            institution_type=self.institutiontype1)
        self.group1 = CTGroup.objects.create(
            name='g1',
            visibility=1,
            status_institution=CTGroup.STATUS_INSTITUTION_APPROVED
            )
        self.group2 = CTGroup.objects.create(
            name='g2',
            visibility=1,
            status_institution=CTGroup.STATUS_INSTITUTION_PENDING
            )

    def tearDown(self):

        self.group1.users.all().delete()
        self.group1.delete()
        self.group2.users.all().delete()
        self.group2.delete()
        self.institution1.delete()
        self.institution2.delete()
        self.institutiontype1.delete()
        self.user1.stateobjecthistory_set.all().delete()
        self.user1.delete()
        self.country1.delete()

    def test_list_pendings(self):
        
        request = self.factory.get(
            reverse(u'group_institution_pending_list')
            )
        request.user = self.user1
        
        view_return = CTGroupsInstitutionPendingList.as_view()(request)
        self.assertIn(
            self.group2, 
            view_return.context_data[u'object_list']
            )

        self.assertNotIn(
            self.group1, 
            view_return.context_data[u'object_list']
            )

class AdministratorInstitutionAcceptRefuseChangeTestCase(TestCase):

    def setUp(self):

        self.factory = RequestFactory()
        self.user1 = User.objects.create(username=u'e',
                                        email=u'e@e.com',
                                        gender=1,
                                        race=1,
                                        country_id=u'BR')
        self.user1.groups.add(
            Group.objects.get(
                name=u'administrador')
            )
        self.user2 = User.objects.create(username=u'a',
                                        email=u'a@a.com',
                                        gender=1,
                                        race=2,
                                        country_id=u'BR')
        self.country1 = Country.objects.create(
            description=u"Best Country",
            code=u"el",
            )

        self.institutiontype1 = InstitutionType.objects.create(
            description=u'T1'
            )

        self.institution1 = Institution.objects.create(
            name=u'abc',
            status=u'M',
            country=self.country1,
            institution_type=self.institutiontype1)
        self.ct1 = ClinicalTrial.objects.create(user=self.user1)
        self.ct1.sponsor_set.create(
            sponsor_type=Sponsor.SPONSOR_TYPE_PRIMARY,
            institution=self.institution1)
        self.institution2 = Institution.objects.create(
            name=u'abcd',
            status=u'A',
            country=self.country1,
            institution_type=self.institutiontype1)
        self.institution3 = Institution.objects.create(
            name=u'the third',
            status=u'M',
            country=self.country1,
            institution_type=self.institutiontype1)
        self.ct1.sponsor_set.create(
            sponsor_type=Sponsor.SPONSOR_TYPE_PRIMARY,
            institution=self.institution3)
        self.group1 = CTGroup.objects.create(
            name='g1',
            visibility=1,
            status_institution=CTGroup.STATUS_INSTITUTION_APPROVED
            )
        self.group2 = CTGroup.objects.create(
            name='g2',
            visibility=1,
            status_institution=CTGroup.STATUS_INSTITUTION_PENDING
            )

        self.setup = Setup()
        self.setup.language_1_code_id = 167
        self.setup.language_1_mandatory = True
        self.setup.language_2_code_id = 57
        self.setup.language_2_mandatory = True
        self.setup.language_3_code_id = 61
        self.setup.language_3_mandatory = False
        self.setup.queue_distribution = 'lala'
        self.setup.current_position_qd0 = 1
        self.setup.save()
        
        #mocks
        self.original_success = messages.success
        messages.success = Mock()

    def tearDown(self):

        messages.success = self.original_success
        self.group1.users.all().delete()
        self.group1.delete()
        self.group2.users.all().delete()
        self.group2.delete()
        self.institution1.delete()
        self.institution2.delete()
        self.institution3.delete()
        self.institutiontype1.delete()
        self.ct1.delete()
        self.user1.stateobjecthistory_set.all().delete()
        self.user1.delete()
        self.user2.stateobjecthistory_set.all().delete()
        self.user2.delete()
        self.country1.delete()

    def test_accept(self):
        request = self.factory.post(
            reverse(
                u'institution_pending_decision',
                args=(self.institution1.pk, u'A',)),
            data={u'decision': u'yes'}
            )
        request.user = self.user1
        
        self.assertEqual(messages.success.called, False)
        set_state(
            self.ct1,
            State.objects.get(pk=State.STATE_AGUARDANDO_MODERACAO)
        )

        self.assertEqual(
            Institution.objects.get(pk=self.institution1.pk).status,
            Institution.STATUS_MODERATION)


        view_return = InstitutionPendingView.as_view()(
            request,
            self.institution1.pk,
            u'A')
        #the workflow status must be on moderation
        #because we have 1 more institution to moderate 
        self.assertEqual(get_state(self.ct1).pk, State.STATE_AGUARDANDO_MODERACAO)

        request = self.factory.post(
            reverse(
                u'institution_pending_decision',
                args=(self.institution3.pk, u'A',)),
            data={u'decision': u'yes'}
            )
        request.user = self.user1

        view_return = InstitutionPendingView.as_view()(
            request,
            self.institution3.pk,
            u'A')

        #now, must be AGUARDANDO_REVISAO because there's no more institutions left to moderate,
        #and none have been rejected, plus we don't have evaluators on the system
        self.assertEqual(get_state(self.ct1).pk, State.STATE_AGUARDANDO_REVISAO)

        self.assertEqual(messages.success.called, True)
        #check the redirect success status
        self.assertEqual(view_return.status_code, 302)

        self.assertEqual(
            Institution.objects.get(pk=self.institution1.pk).status,
            Institution.STATUS_ACTIVE)

    def test_refuse(self):
        request = self.factory.post(
            reverse(
                u'institution_pending_decision',
                args=(self.institution1.pk, u'R',)),
            data={u'decision': u'yes', u'observation': u'first'}
            )
        request.user = self.user1
        
        self.assertEqual(messages.success.called, False)

        set_state(
            self.ct1,
            State.objects.get(pk=State.STATE_AGUARDANDO_MODERACAO)
        )

        self.assertEqual(
            Institution.objects.get(pk=self.institution1.pk).status,
            Institution.STATUS_MODERATION)

        view_return = InstitutionPendingView.as_view()(
            request,
            self.institution1.pk,
            u'R')

        #the workflow status must be on moderation
        #because we have 1 more institution to moderate 
        self.assertEqual(get_state(self.ct1).pk, State.STATE_AGUARDANDO_MODERACAO)

        request = self.factory.post(
            reverse(
                u'institution_pending_decision',
                args=(self.institution3.pk, u'R',)),
            data={u'decision': u'yes', u'observation': u'because_yes!'}
            )
        request.user = self.user1

        view_return = InstitutionPendingView.as_view()(
            request,
            self.institution3.pk,
            u'R')

        #now, must be REVISANDO_PREENCHIMENTO because there's no more institutions left to moderate,
        #and none have been rejected, plus we don't have evaluators on the system
        self.assertEqual(get_state(self.ct1).pk, State.STATE_REVISANDO_PREENCHIMENTO)
        self.assertEqual(
            StateObjectHistory.objects.latest(u'update_date').observation,
            u'firstbecause_yes!'
        )
        self.assertEqual(messages.success.called, True)
        #check the redirect success status
        self.assertEqual(view_return.status_code, 302)

        self.assertEqual(
            Institution.objects.get(pk=self.institution1.pk).status,
            Institution.STATUS_INACTIVE)

    def test_change(self):
        request = self.factory.post(
            reverse(
                u'institution_pending_change_institution',
                args=(self.institution1.pk,)),
            data={u'institution': self.institution2.pk}
            )
        request.user = self.user1
        self.institution3.status = u'A'
        self.institution3.save()
        self.assertEqual(messages.success.called, False)

        set_state(
            self.ct1,
            State.objects.get(pk=State.STATE_AGUARDANDO_MODERACAO)
        )

        self.assertEqual(
            Institution.objects.get(pk=self.institution1.pk).status,
            Institution.STATUS_MODERATION)

        view_return = InstitutionPendingChangeView.as_view()(
            request,
            self.institution1.pk)

        self.assertEqual(messages.success.called, True)
        #check the redirect success status
        self.assertEqual(view_return.status_code, 302)

        self.assertEqual(get_state(self.ct1).pk, State.STATE_AGUARDANDO_REVISAO)

        self.assertEqual(
            Institution.objects.get(pk=self.institution1.pk).status,
            Institution.STATUS_INACTIVE)

        self.assertTrue(
            self.ct1.sponsor_set.filter(
                institution__pk=self.institution2.pk
            ).exists()
        )

        set_state(
            self.ct1,
            State.objects.get(pk=State.STATE_AGUARDANDO_MODERACAO)
        )

        request.user = self.user1
        self.institution3.status = u'I'
        self.institution3.save()
        self.ct1.sponsor_set.update(institution=self.institution1)
        sponsor_inst3 = self.ct1.sponsor_set.first()
        sponsor_inst3.institution = self.institution3
        sponsor_inst3.save()
        self.institution1.status = u"M"
        self.institution1.save()

        view_return = InstitutionPendingChangeView.as_view()(
            request,
            self.institution1.pk)

        self.assertEqual(get_state(self.ct1).pk, State.STATE_REVISANDO_PREENCHIMENTO)


class AdministratorGroupInstitutionAcceptRefuseTestCase(TestCase):

    def setUp(self):

        self.factory = RequestFactory()
        self.user1 = User.objects.create(username=u'e',
                                        email=u'e@e.com',
                                        gender=1,
                                        race=1,
                                        country_id=u'BR')
        self.user1.groups.add(
            Group.objects.get(
                name=u'administrador')
            )
        self.user2 = User.objects.create(username=u'a',
                                        email=u'a@a.com',
                                        gender=1,
                                        race=2,
                                        country_id=u'BR')
        self.country1 = Country.objects.create(
            description=u"Best Country",
            code=u"el",
            )
        self.institutiontype1 = InstitutionType.objects.create(
            description=u'T1'
            )

        self.institution1 = Institution.objects.create(
            name=u'abc',
            status=u'M',
            country=self.country1,
            institution_type=self.institutiontype1)
        self.institution2 = Institution.objects.create(
            name=u'abcd',
            status=u'A',
            country=self.country1,
            institution_type=self.institutiontype1)
        self.group1 = CTGroup.objects.create(
            name='g1',
            visibility=1,
            status_institution=CTGroup.STATUS_INSTITUTION_PENDING,
            institution=self.institution1,
            )
        self.group2 = CTGroup.objects.create(
            name='g2',
            visibility=1,
            status_institution=CTGroup.STATUS_INSTITUTION_APPROVED
            )

        self.setup = Setup()
        self.setup.language_1_code_id = 167
        self.setup.language_1_mandatory = True
        self.setup.language_2_code_id = 57
        self.setup.language_2_mandatory = True
        self.setup.language_3_code_id = 61
        self.setup.language_3_mandatory = False
        self.setup.queue_distribution = 'lala'
        self.setup.current_position_qd0 = 1
        self.setup.save()
        
        #mocks
        self.original_success = messages.success
        messages.success = Mock()

    def tearDown(self):

        messages.success = self.original_success
        self.group1.users.all().delete()
        self.group1.delete()
        self.group2.users.all().delete()
        self.group2.delete()
        self.institution1.delete()
        self.institution2.delete()
        self.institutiontype1.delete()
        self.user1.delete()
        self.user2.delete()
        self.country1.delete()

    def test_accept(self):
        request = self.factory.post(
            reverse(
                u'group_institution_pending_decision',
                args=(self.group1.pk, u'A',)),
            data={u'decision': u'yes'}
            )
        request.user = self.user1
        
        self.assertEqual(messages.success.called, False)

        self.assertEqual(
            CTGroup.objects.get(pk=self.group1.pk).status_institution,
            CTGroup.STATUS_INSTITUTION_PENDING)

        view_return = CTGroupsInstitutionPendingView.as_view()(
            request,
            self.group1.pk,
            u'A')

        self.assertEqual(messages.success.called, True)
        #check the redirect success status
        self.assertEqual(view_return.status_code, 302)

        self.assertEqual(
            CTGroup.objects.get(pk=self.group1.pk).status_institution,
            CTGroup.STATUS_INSTITUTION_APPROVED)

    def test_refuse(self):
        request = self.factory.post(
            reverse(
                u'group_institution_pending_decision',
                args=(self.group1.pk, u'R',)),
            data={u'decision': u'yes'}
            )
        request.user = self.user1
        
        self.assertEqual(messages.success.called, False)

        self.assertEqual(
            CTGroup.objects.get(pk=self.group1.pk).status_institution,
            CTGroup.STATUS_INSTITUTION_PENDING)

        view_return = CTGroupsInstitutionPendingView.as_view()(
            request,
            self.group1.pk,
            u'R')

        self.assertEqual(messages.success.called, True)
        #check the redirect success status
        self.assertEqual(view_return.status_code, 302)

        self.assertEqual(
            CTGroup.objects.get(pk=self.group1.pk).status_institution,
            CTGroup.STATUS_INSTITUTION_CANCEL)

class GroupAdministratorModerationTestCase(TestCase):

    def setUp(self):

        self.factory = RequestFactory()
        self.user1 = User.objects.create(username=u'e',
                                        email=u'e@e.com',
                                        gender=1,
                                        race=1,
                                        country_id=u'BR')
        self.user1.groups.add(
            Group.objects.get(
                name=u'administrador')
            )
        self.user2 = User.objects.create(username=u'a',
                                        email=u'a@a.com',
                                        gender=1,
                                        race=2,
                                        country_id=u'BR')
        self.country1 = Country.objects.create(
            description=u"Best Country",
            code=u"el",
            )
        self.institutiontype1 = InstitutionType.objects.create(
            description=u'T1'
            )

        self.institution1 = Institution.objects.create(
            name=u'abc',
            status=u'M',
            country=self.country1,
            institution_type=self.institutiontype1)
        self.institution2 = Institution.objects.create(
            name=u'abcd',
            status=u'A',
            country=self.country1,
            institution_type=self.institutiontype1)
        self.group1 = CTGroup.objects.create(
            name='g1',
            visibility=1,
            status_institution=CTGroup.STATUS_INSTITUTION_PENDING,
            institution=self.institution1,
        )
        self.group2 = CTGroup.objects.create(
            name='g2',
            visibility=1,
            status_institution=CTGroup.STATUS_INSTITUTION_APPROVED,
            institution=self.institution1
        )

        self.ctgroupuser1 = CTGroupUser.objects.create(
            user=self.user2,
            ct_group=self.group2,
            type_user=CTGroupUser.TYPE_ADMINISTRATOR,
            status=CTGroupUser.STATUS_PENDING_APROV_ADM
        )

        self.setup = Setup()
        self.setup.language_1_code_id = 167
        self.setup.language_1_mandatory = True
        self.setup.language_2_code_id = 57
        self.setup.language_2_mandatory = True
        self.setup.language_3_code_id = 61
        self.setup.language_3_mandatory = False
        self.setup.queue_distribution = 'lala'
        self.setup.current_position_qd0 = 1
        self.setup.save()
        
        #mocks
        self.original_success = messages.success
        messages.success = Mock()

        self.original_send_mail = views.send_mail
        views.send_mail = Mock()

    def tearDown(self):

        messages.success = self.original_success
        views.send_mail = self.original_send_mail
        self.group1.users.all().delete()
        self.group1.delete()
        self.group2.users.all().delete()
        self.group2.delete()
        self.institution1.delete()
        self.institution2.delete()
        self.institutiontype1.delete()
        self.user1.delete()
        self.user2.delete()
        self.country1.delete()

    def test_accept(self):
        request = self.factory.post(
            reverse(
                u'pending_group_administrator_decision',
                args=(self.ctgroupuser1.pk, u'A',)),
            data={u'decision': u'yes'}
            )
        request.user = self.user1
        
        self.assertEqual(messages.success.called, False)

        self.assertEqual(
            CTGroupUser.objects.get(pk=self.ctgroupuser1.pk).status,
            CTGroupUser.STATUS_PENDING_APROV_ADM)

        view_return = GroupAdministratorPendingView.as_view()(
            request,
            self.ctgroupuser1.pk,
            u'A')

        self.assertEqual(messages.success.called, True)
        #check the redirect success status
        self.assertEqual(view_return.status_code, 302)

        self.assertEqual(
            CTGroupUser.objects.get(pk=self.ctgroupuser1.pk).status,
            CTGroupUser.STATUS_ACTIVE)

    def test_refuse(self):
        request = self.factory.post(
            reverse(
                u'pending_group_administrator_decision',
                args=(self.ctgroupuser1.pk, u'R',)),
            data={u'decision': u'yes'}
            )
        request.user = self.user1
        
        self.assertEqual(messages.success.called, False)

        self.assertEqual(
            CTGroupUser.objects.get(pk=self.ctgroupuser1.pk).status,
            CTGroupUser.STATUS_PENDING_APROV_ADM)

        #check if the e-mail for the user has not been sended yet
        self.assertEqual(views.send_mail.called, False)

        view_return = GroupAdministratorPendingView.as_view()(
            request,
            self.ctgroupuser1.pk,
            u'R')

        self.assertEqual(messages.success.called, True)
        #check the redirect success status
        self.assertEqual(view_return.status_code, 302)

        self.assertEqual(
            CTGroupUser.objects.get(pk=self.ctgroupuser1.pk).status,
            CTGroupUser.STATUS_INACTIVE)

        #check if the e-mail for the user has been sended
        self.assertEqual(views.send_mail.called, True)
