# -*- coding: utf-8 -*-

from django.test import TestCase
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.test.client import RequestFactory
from administration.models import *
from administration.views import *
from django.core.urlresolvers import reverse_lazy
from django.core.exceptions import PermissionDenied
from django.utils.translation import ugettext as _
from mock import Mock, MagicMock
from django.contrib import messages

User = get_user_model()


def create_user(groups=[]):
    """Create a user and add to a group parameter.
    """
    user = User.objects.create_user(
        'user', 'user@email.com',
        'pass', gender=1, race=1, country_id=u'BR')
    user.save()
    for group in groups:
        g = Group.objects.get(name=group)
        g.user_set.add(user)
    return user


class CountryTestCase(TestCase):
    """Country class test"""
    def setUp(self):
        """Register Country and add user with permission"""
        self.factory = RequestFactory()
        self.user = create_user(['administrador'])
        self.country = Country.objects.get(pk=u'AF')
        self.original_success = messages.success
        messages.success = Mock()
        self.original_info = messages.info
        messages.info = Mock()
        self.original_error = messages.error
        messages.error = Mock()

    def tearDown(self):
        """Delete country and user"""
        self.user.delete()
        messages.success = self.original_success
        messages.info = self.original_info
        messages.error = self.original_error

    def test_country_list_permission(self):
        """Verify Country list permission"""
        request = self.factory.get(reverse_lazy('country_list'))
        request.user = self.user
        response = CountryList.as_view()(request)
        self.assertEqual(response.status_code, 200)
        self.user.delete()
        self.user = create_user()
        request = self.factory.get(reverse_lazy('country_list'))
        request.user = self.user
        self.assertRaises(PermissionDenied, CountryList.as_view(), request)
        self.user.delete()
        self.user = create_user(['administrador'])

    def test_country_detail_permission(self):
        """Verify Country detail permission"""
        request = self.factory.get(
            reverse_lazy('country_detail', args=(self.country.pk,)))
        request.user = self.user
        response = CountryDetail.as_view()(request, pk=self.country.pk)
        self.assertEqual(response.status_code, 200)
        self.user.delete()
        self.user = create_user()
        request = self.factory.get(
            reverse_lazy('country_detail', args=(self.country.pk,)))
        request.user = self.user
        self.assertRaises(
            PermissionDenied, CountryDetail.as_view(),
            request, args=(self.country.pk,))
        self.user.delete()
        self.user = create_user(['administrador'])

    def test_country_create_permission(self):
        """Verify Country create permission"""
        request = self.factory.get(reverse_lazy('country_add'))
        request.user = self.user
        response = CountryCreate.as_view()(request)
        self.assertEqual(response.status_code, 200)
        self.user.delete()
        self.user = create_user()
        request = self.factory.get(reverse_lazy('country_add'))
        request.user = self.user
        self.assertRaises(PermissionDenied, CountryCreate.as_view(), request)
        self.user.delete()
        self.user = create_user(['administrador'])

    def test_country_update_permission(self):
        """Verify Country update permission"""
        request = self.factory.get(
            reverse_lazy('country_edit', args=(self.country.pk,)))
        request.user = self.user
        response = CountryUpdate.as_view()(request, pk=self.country.pk)
        self.assertEqual(response.status_code, 200)
        self.user.delete()
        self.user = create_user()
        request = self.factory.get(
            reverse_lazy('country_edit', args=(self.country.pk,)))
        request.user = self.user
        self.assertRaises(
            PermissionDenied, CountryUpdate.as_view(),
            request, args=(self.country.pk,))
        self.user.delete()
        self.user = create_user(['administrador'])

    def test_country_delete_permission(self):
        """Verify Country delete permission"""
        request = self.factory.get(
            reverse_lazy('country_delete', args=(self.country.pk,)))
        request.user = self.user
        response = CountryDelete.as_view()(request, pk=self.country.pk)
        self.assertEqual(response.status_code, 200)

        request = self.factory.post(
            reverse_lazy('country_delete', args=(self.country.pk,)), {})
        request.user = self.user
        response = CountryDelete.as_view()(request, pk=self.country.pk)
        self.assertEqual(response.status_code, 302)

        self.user.delete()
        self.user = create_user()
        request = self.factory.get(
            reverse_lazy('country_delete', args=(self.country.pk,)))
        request.user = self.user
        self.assertRaises(
            PermissionDenied, CountryDelete.as_view(),
            request, args=(self.country.pk,))

        request = self.factory.post(
            reverse_lazy('country_delete', args=(self.country.pk,)), {})
        request.user = self.user
        self.assertRaises(
            PermissionDenied, CountryDelete.as_view(),
            request, args=(self.country.pk,))

        self.user.delete()
        self.user = create_user(['administrador'])

    def test_country_list(self):
        """Verify Country objects"""
        before = Country.objects.all().count()
        request = self.factory.get(reverse_lazy('country_list'))
        request.user = self.user
        response = CountryList.as_view()(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context_data[u'country_list'].count(), before)
        self.assertEqual(
            response.context_data['country_list'][0].pk, self.country.pk)

    def test_country_delete(self):
        """Verify Country delete view"""
        before = Country.objects.all().count()

        request = self.factory.post(
            reverse_lazy('country_delete', args=(u'BR',)), {})
        request.user = self.user
        response = CountryDelete.as_view()(request, pk=u'BR')
        self.assertEqual(response.status_code, 302)
        self.assertEqual(Country.objects.all().count(), before-1)

        institution_type = InstitutionType.objects.create(
            description='chemistry')
        institution = Institution.objects.create(
            name='',
            description='',
            postal_address='',
            city='',
            state='',
            country=Country.objects.get(pk=u'US'),
            status='M',
            institution_type=institution_type)

        request = self.factory.post(
            reverse_lazy('country_delete', args=(u'US',)), {})
        request.user = self.user
        response = CountryDelete.as_view()(request, pk=u'US')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content.decode('utf-8'),  _("Can not delete this entry"))
        institution.delete()
        institution_type.delete()

    def test_country_query(self):
        """Verify Country query"""
        request = self.factory.get(
            reverse_lazy('country_list'), {'q': 'Italy'})
        request.user = self.user
        response = CountryList.as_view()(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context_data['country_list'][0].pk, 'IT')
        