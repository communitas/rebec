#-*- coding: utf-8 -*-
from django.test import TestCase
from django.test.client import RequestFactory

from django.contrib.auth import get_user_model
from administration.models import Setup
from administration.views import CTGroupsManageView
from django.core.urlresolvers import reverse, resolve
from mock import Mock
from django.contrib import messages

from administration.models import (
    SetupIdentifier, InstitutionType, CTGroup,
    Institution, Country
    )
from clinical_trials.models import ClinicalTrial
from django.contrib.auth.models import Group
User = get_user_model()
from django.core.exceptions import PermissionDenied

class AdministrationCTGroupAdminManagementTestCase(TestCase):

    def setUp(self):

        self.factory = RequestFactory()
        self.user1 = User.objects.create(username=u'e',
                                        email=u'e@e.com',
                                        gender=1,
                                        race=1,
                                        country_id=u'BR')
        self.user1.groups.add(Group.objects.get(name=u'administrador'))
        self.user2 = User.objects.create(username=u'a',
                                        email=u'a@a.com',
                                        gender=1,
                                        race=1,
                                        country_id=u'BR')
        self.user3 = User.objects.create(username=u'f',
                                        email=u'f@f.com',
                                        gender=1,
                                        race=1,
                                        country_id=u'BR')

        self.country1 = Country.objects.create(description=u"Best Country", code=u"el")
        self.institutiontype1 = InstitutionType.objects.create(description=u'T1')

        self.institution1 = Institution.objects.create(
            name=u'abc',
            status=u'A',
            country=self.country1,
            institution_type=self.institutiontype1)
        self.group1 = CTGroup.objects.create(name='g1', visibility=1)
        self.group2 = CTGroup.objects.create(name='g2', visibility=1)
        self.group3 = CTGroup.objects.create(name='g3', visibility=1)

        self.setup = Setup()
        self.setup.language_1_code_id = 167
        self.setup.language_1_mandatory = True
        self.setup.language_2_code_id = 57
        self.setup.language_2_mandatory = True
        self.setup.language_3_code_id = 61
        self.setup.language_3_mandatory = False
        self.setup.queue_distribution = 'lala'
        self.setup.current_position_qd0 = 1
        self.setup.save()

        self.ct1 = ClinicalTrial.objects.create(user=self.user1)

        self.post_data = {u'form-0-administrators': [self.user1.pk,
                                                     self.user2.pk,],
                          u'form-0-id': [self.group1.pk],
                          u'form-0-institution': [self.institution1.pk],
                          u'form-0-status': [u'2'],
                          u'form-1-administrators': [self.user3.pk],
                          u'form-1-id': [self.group2.pk],
                          u'form-1-institution': [u''],
                          u'form-1-status': [u'1'],
                          u'form-2-administrators': [self.user1.pk],
                          u'form-2-id': [self.group3.pk],
                          u'form-2-institution': [self.institution1.pk],
                          u'form-2-status': [u'1'],
                          u'form-INITIAL_FORMS': [u'3'],
                          u'form-MAX_NUM_FORMS': [u'1000'],
                          u'form-TOTAL_FORMS': [u'3']}

        
        #mocks
        self.original_success = messages.success
        messages.success = Mock()

    def tearDown(self):

        messages.success = self.original_success
        self.group1.users.all().delete()
        self.group1.delete()
        self.group2.users.all().delete()
        self.group2.delete()
        self.group3.users.all().delete()
        self.group3.delete()
        self.institution1.delete()
        self.institutiontype1.delete()
        self.ct1.delete()
        self.user1.delete()
        self.user2.delete()
        self.user3.delete()
        self.country1.delete()

    def test_update_groups_data(self):
        
        request = self.factory.post(
            reverse(u'administration_admin_manage_groups'),
            data=self.post_data)
        request.user = self.user1
        #check success isn't called yet
        self.assertEqual(messages.success.called, False)

        view_return = CTGroupsManageView.as_view()(request)

        #now, the success must has a call
        self.assertEqual(messages.success.called, True)
        #check the redirect success status
        self.assertEqual(view_return.status_code, 302)

        
        self.assertEqual(
            CTGroup.objects.get(pk=self.group1.pk).institution,
            self.institution1)
        self.assertIsNone(
            CTGroup.objects.get(pk=self.group2.pk).institution
            )
        self.assertEqual(
            CTGroup.objects.get(pk=self.group3.pk).institution,
            self.institution1)

        self.assertIn(
            self.user1,
            [i.user for i in self.group1.users.all()])
        self.assertIn(
            self.user2,
            [i.user for i in self.group1.users.all()])
        self.assertNotIn(
            self.user3,
            [i.user for i in self.group1.users.all()])
        self.assertIn(
            self.user3,
            [i.user for i in self.group2.users.all()])

        self.assertEqual(
            CTGroup.objects.get(pk=self.group3.pk).status,
            1)
        self.assertEqual(
            CTGroup.objects.get(pk=self.group2.pk).status,
            1)
        self.assertEqual(
            CTGroup.objects.get(pk=self.group1.pk).status,
            2)

        #now, test the initial for administrators fields
        request = self.factory.get(
            reverse(u'administration_admin_manage_groups'))
        request.user = self.user1
        view_return = CTGroupsManageView.as_view()(request)

        #this tests admit(and automatically test it, too) that ordering is -pk
        self.assertIn(
            self.user1,
            view_return.context_data[u'formset'].forms[0].\
                fields[u'administrators'].initial
            )
        self.assertNotIn(
            self.user2,
            view_return.context_data[u'formset'].forms[0].\
                fields[u'administrators'].initial
            )
        self.assertIn(
            self.user3,
            view_return.context_data[u'formset'].forms[1].\
                fields[u'administrators'].initial
            )
        self.assertNotIn(
            self.user2,
            view_return.context_data[u'formset'].forms[1].\
                fields[u'administrators'].initial
            )
        self.assertIn(
            self.user1,
            view_return.context_data[u'formset'].forms[2].\
                fields[u'administrators'].initial
            )

    def test_filters(self):
        """
        Tests the GET filters
        """
        
        #GET filter for institution
        request = self.factory.get(
            reverse(u'administration_admin_manage_groups')+\
                u'?institution=%s' % self.institution1.pk)
        request.user = self.user1
        view_return = CTGroupsManageView.as_view()(request)
        #no results, ok
        self.assertEqual(len(view_return.context_data[u'formset'].forms), 0)

        #update the group 2, to have a institution
        self.group2.institution = self.institution1
        self.group2.save()

        view_return = CTGroupsManageView.as_view()(request)
        
        #now, the group2 result
        self.assertEqual(len(view_return.context_data[u'formset'].forms), 1)


        #test for the group GET parameter
        request = self.factory.get(
            reverse(u'administration_admin_manage_groups')+\
                u'?group=%s' % self.group1.pk)
        request.user = self.user1
        view_return = CTGroupsManageView.as_view()(request)

        #return just a for, correct
        self.assertEqual(len(view_return.context_data[u'formset'].forms), 1)

        #test the insttution AND the group
        request = self.factory.get(
            reverse(u'administration_admin_manage_groups')\
                +u'?group=%s&institution=%s' % (
                    self.group1.pk,
                    self.institution1.pk))
        request.user = self.user1
        view_return = CTGroupsManageView.as_view()(request)

        #no results
        self.assertEqual(len(view_return.context_data[u'formset'].forms), 0)

        #now, with a group that haves an institution
        request = self.factory.get(
            reverse(u'administration_admin_manage_groups')+\
                u'?group=%s&institution=%s' % (
                    self.group2.pk,
                    self.institution1.pk))
        request.user = self.user1
        view_return = CTGroupsManageView.as_view()(request)

        #one result, correct
        self.assertEqual(len(view_return.context_data[u'formset'].forms), 1)

        def test_access_permission(self):
            """Verify Institution moderation permission"""
            request = self.factory.get(reverse_lazy(
                'administration_admin_manage_groups'))
            request.user = self.user1
            response = CTGroupsManageView.as_view()(request)
            self.assertEqual(response.status_code, 200)
            request.user = self.user2
            self.assertRaises(
                PermissionDenied, CTGroupsManageView.as_view(),
                request)


    def test_update_to_inactive_error(self):
        
        self.ct1.group = self.group1
        self.ct1.save()

        request = self.factory.post(
            reverse(u'administration_admin_manage_groups'),
            data=self.post_data)
        request.user = self.user1
        #check success isn't called yet
        self.assertEqual(messages.success.called, False)

        view_return = CTGroupsManageView.as_view()(request)

        #now, the success must not has a call even,
        #because an error must be raised
        self.assertEqual(messages.success.called, False)
        #check the redirect success status
        self.assertEqual(view_return.status_code, 200)

        #Assert that we have an error
        self.assertEqual(
            len(view_return.context_data[u'formset'].errors[0][u'status']),
            1
            )