# -*- coding: utf-8 -*-

from django.test import TestCase
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.test.client import RequestFactory
from administration.models import *
from clinical_trials.models import *
from administration.views import *
from django.core.urlresolvers import reverse_lazy
from django.core.exceptions import PermissionDenied
from django.utils.translation import ugettext as _
from django.contrib import messages
from mock import Mock, MagicMock


User = get_user_model()


def create_user(groups=[]):
    """Create a user and add to a group parameter.
    """
    user = User.objects.create_user(
        'user', 'user@email.com',
        'pass', gender=1, race=1, country_id=u'BR')
    user.save()
    for group in groups:
        g = Group.objects.get(name=group)
        g.user_set.add(user)
    return user


class SetupTesteCase(TestCase):
    """Setup classe test."""
    def setUp(self):
        """Register Setup"""
        self.factory = RequestFactory()
        self.user = create_user(['administrador'])
        self.setup = Setup.objects.create(
            language_1_code_id=127,
            language_1_mandatory=True,
            language_2_code_id=28,
            language_2_mandatory=True)
        self.original_success = messages.success
        messages.success = Mock()
        self.original_info = messages.info
        messages.info = Mock()
        self.original_error = messages.error
        messages.error = Mock()

    def tearDown(self):
        """Delete Setup and user."""
        self.setup.delete()
        self.user.delete()
        Attachment.objects.all().delete()
        StateDaysLimit.objects.all().delete()
        self.user.stateobjecthistory_set.all().delete()
        messages.success = self.original_success
        messages.info = self.original_info
        messages.error = self.original_error

    def test_setup_update_permission(self):
        """Verify Setup update permission."""
        request = self.factory.get(reverse_lazy('setup'))
        request.user = self.user
        response = SetupUpdate.as_view()(request, pk=self.setup.pk)
        self.assertEqual(response.status_code, 200)
        self.user.delete()
        self.user = create_user()
        request = self.factory.get(reverse_lazy('setup'))
        request.user = self.user
        self.assertRaises(PermissionDenied, SetupUpdate.as_view(), request)
        self.user.delete()
        self.user = create_user(['administrador'])

    def test_setup_attachment(self):
        attachment = Attachment.objects.create(
            setup=self.setup,
            name="file",
            attachment_type="F")

        clinical_trial = ClinicalTrial.objects.create(
            user=self.user)

        post_data = {
            u'language_2_code': u'en-US',
            u'language_2_mandatory': True,
            u'form-0-DELETE': True,
            u'form-TOTAL_FORMS': u'1',
            u'form-0-id': (1, 1),
            u'form-INITIAL_FORMS': u'1',
            u'form-0-setup': u'1',
            u'current_position_qd0': u'',
            u'queue_distribution': u'',
            u'language_3_code': u'',
            u'form-MAX_NUM_FORMS': u'1000',
            u'form-0-required': True,
            u'terms_of_use': u'',
            u'language_1_code': u'pt-BR',
            u'form-0-attachment_type': u'L',
            u'form-0-name': u'arquivo',
            u'language_1_mandatory': True,
        }

        request = self.factory.get(reverse_lazy('setup'))
        request.user = self.user
        response = SetupUpdate.as_view()(request, pk=self.setup.pk)
        self.assertEqual(response.status_code, 200)
        attachment.delete()
        clinical_trial.delete()


    def test_create_days_limit(self):
        post_data = {
            u'language_2_code': [27],
            u'language_2_mandatory': [u'on'],
            u'statedayslimit_set-0-yellow': [u'5'],
            u'statedayslimit_set-TOTAL_FORMS': [u'1'],
            u'statedayslimit_set-__prefix__-state': [u''],
            u'statedayslimit_set-0-red': [u'10'],
            u'statedayslimit_set-0-green': [u'2'],
            u'statedayslimit_set-__prefix__-red': [u''],
            u'statedayslimit_set-MAX_NUM_FORMS': [u'1000'],
            u'current_position_qd0': [u''],
            u'queue_distribution': [u''],
            u'language_3_code': [u''],
            u'statedayslimit_set-__prefix__-yellow': [u''],
            u'form-__prefix__-attachment_type': [u''],
            u'language_1_code': [127],
            u'statedayslimit_set-__prefix__-id': [u'', u''],
            u'form-__prefix__-name': [u''],
            u'language_1_mandatory': [u'on'],
            u'statedayslimit_set-__prefix__-setup': [u'1'],
            u'form-TOTAL_FORMS': [u'0'],
            u'form-__prefix__-id': [u'', u''],
            u'statedayslimit_set-0-setup': [u''],
            u'form-INITIAL_FORMS': [u'0'],
            u'statedayslimit_set-__prefix__-DELETE': [u''],
            u'form-__prefix__-DELETE': [u''],
            u'statedayslimit_set-0-id': [u'', u''],
            u'form-MAX_NUM_FORMS': [u'1000'],
            u'statedayslimit_set-INITIAL_FORMS': [u'0'],
            u'terms_of_use': [u''],
            u'statedayslimit_set-0-state': [u'7'],
            u'form-__prefix__-setup': [u'1']
        }

        request = self.factory.post(reverse_lazy('setup'), data=post_data)
        request.user = self.user
        response = SetupUpdate.as_view()(request, pk=self.setup.pk)
        self.assertEqual(response.status_code, 302)

    def test_create_days_limit_error(self):
        post_data = {
            u'language_2_code': [27],
            u'language_2_mandatory': [u'on'],
            u'statedayslimit_set-0-yellow': [u'20'],
            u'statedayslimit_set-TOTAL_FORMS': [u'2'],
            u'statedayslimit_set-__prefix__-state': [u''],
            u'statedayslimit_set-0-red': [u'10'],
            u'statedayslimit_set-__prefix__-red': [u''],
            u'statedayslimit_set-MAX_NUM_FORMS': [u'1000'],
            u'current_position_qd0': [u''],
            u'queue_distribution': [u''],
            u'language_3_code': [u''],
            u'statedayslimit_set-__prefix__-yellow': [u''],
            u'form-__prefix__-attachment_type': [u''],
            u'language_1_code': [127],
            u'statedayslimit_set-__prefix__-id': [u'', u''],
            u'form-__prefix__-name': [u''],
            u'language_1_mandatory': [u'on'],
            u'statedayslimit_set-__prefix__-setup': [u'1'],
            u'form-TOTAL_FORMS': [u'0'],
            u'form-__prefix__-id': [u'', u''],
            u'statedayslimit_set-0-setup': [u''],
            u'form-INITIAL_FORMS': [u'0'],
            u'statedayslimit_set-__prefix__-DELETE': [u''],
            u'form-__prefix__-DELETE': [u''],
            u'statedayslimit_set-0-id': [u'', u''],
            u'form-MAX_NUM_FORMS': [u'1000'],
            u'statedayslimit_set-INITIAL_FORMS': [u'0'],
            u'terms_of_use': [u''],
            u'statedayslimit_set-0-state': [u'8'],
            u'form-__prefix__-setup': [u'1']
        }

        request = self.factory.post(reverse_lazy('setup'), data=post_data)
        request.user = self.user
        response = SetupUpdate.as_view()(request, pk=self.setup.pk)
        self.assertEqual(response.status_code, 200)
