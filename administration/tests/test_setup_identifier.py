# -*- coding: utf-8 -*-

from django.test import TestCase
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.test.client import RequestFactory
from administration.models import *
from administration.views import *
from django.core.urlresolvers import reverse_lazy
from django.core.exceptions import PermissionDenied
from django.utils.translation import ugettext as _
from mock import Mock, MagicMock
from django.contrib import messages

User = get_user_model()


def create_user(groups=[]):
    """Create a user and add to a group parameter.
    """
    user = User.objects.create_user(
        'user', 'user@email.com',
        'pass', gender=1, race=1, country_id=u'BR')
    user.save()
    for group in groups:
        g = Group.objects.get(name=group)
        g.user_set.add(user)
    return user


class SetupIdentifierTesteCase(TestCase):
    """Setup Identifier class test."""
    def setUp(self):
        """Register Setup Identifier."""
        self.factory = RequestFactory()
        self.user = create_user(['administrador'])
        self.setup = SetupIdentifier.objects.create(
            issuing_body='teste',
            mandatory=True,
            mask='xxx',
            name='tes')
        self.original_success = messages.success
        messages.success = Mock()
        self.original_info = messages.info
        messages.info = Mock()
        self.original_error = messages.error
        messages.error = Mock()

    def tearDown(self):
        """Delete Setup Identifier and user."""
        self.setup.delete()
        self.user.delete()
        messages.success = self.original_success
        messages.info = self.original_info
        messages.error = self.original_error

    def test_setup_identifier_list_permission(self):
        """Verify Setup Identifier list permission."""
        request = self.factory.get(reverse_lazy('setup_identifier_list'))
        request.user = self.user
        response = SetupIdentifierList.as_view()(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.context_data['setupidentifier_list'].count(), 1)
        self.user.delete()
        self.user = create_user()
        request = self.factory.get(reverse_lazy('setup_identifier_list'))
        request.user = self.user
        self.assertRaises(
            PermissionDenied, SetupIdentifierList.as_view(), request)
        self.user.delete()
        self.user = create_user(['administrador'])

    def test_setup_identifier_detail_permission(self):
        """Verify Setup Identifier detail permission."""
        request = self.factory.get(
            reverse_lazy('setup_identifier_detail', args=(self.setup.pk,)))
        request.user = self.user
        response = SetupIdentifierDetail.as_view()(request, pk=self.setup.pk)
        self.assertEqual(response.status_code, 200)
        self.user.delete()
        self.user = create_user()
        request = self.factory.get(
            reverse_lazy('setup_identifier_detail', args=(self.setup.pk,)))
        request.user = self.user
        self.assertRaises(
            PermissionDenied, SetupIdentifierDetail.as_view(),
            request, args=(self.setup.pk,))
        self.user.delete()
        self.user = create_user(['administrador'])

    def test_setup_identifier_create_permission(self):
        """Verify Setup Identifier create permission."""
        request = self.factory.get(reverse_lazy('setup_identifier_add'))
        request.user = self.user
        response = SetupIdentifierCreate.as_view()(request)
        self.assertEqual(response.status_code, 200)
        self.user.delete()
        self.user = create_user()
        request = self.factory.get(reverse_lazy('setup_identifier_add'))
        request.user = self.user
        self.assertRaises(
            PermissionDenied, SetupIdentifierCreate.as_view(), request)
        self.user.delete()
        self.user = create_user(['administrador'])

    def test_setup_identifier_update_permission(self):
        """Verify Setup Identifier update permission."""
        request = self.factory.get(
            reverse_lazy('setup_identifier_edit', args=(self.setup.pk,)))
        request.user = self.user
        response = SetupIdentifierUpdate.as_view()(request, pk=self.setup.pk)
        self.assertEqual(response.status_code, 200)
        self.user.delete()
        self.user = create_user()
        request = self.factory.get(
            reverse_lazy('setup_identifier_edit', args=(self.setup.pk,)))
        request.user = self.user
        self.assertRaises(
            PermissionDenied, SetupIdentifierUpdate.as_view(),
            request, args=(self.setup.pk,))
        self.user.delete()
        self.user = create_user(['administrador'])

    def test_setup_identifier_delete_permission(self):
        """Verify Setup Identifier delete permission."""
        request = self.factory.get(
            reverse_lazy('setup_identifier_delete', args=(self.setup.pk,)))
        request.user = self.user
        response = SetupIdentifierDelete.as_view()(request, pk=self.setup.pk)
        self.assertEqual(response.status_code, 200)

        request = self.factory.post(
            reverse_lazy('setup_identifier_delete', args=(self.setup.pk,)), {})
        request.user = self.user
        response = SetupIdentifierDelete.as_view()(request, pk=self.setup.pk)
        self.assertEqual(response.status_code, 302)

        self.user.delete()
        self.user = create_user()
        request = self.factory.get(
            reverse_lazy('setup_identifier_delete', args=(self.setup.pk,)))
        request.user = self.user
        self.assertRaises(
            PermissionDenied, SetupIdentifierDelete.as_view(),
            request, args=(self.setup.pk,))

        request = self.factory.post(
            reverse_lazy('setup_identifier_delete', args=(self.setup.pk,)), {})
        request.user = self.user
        self.assertRaises(
            PermissionDenied, SetupIdentifierDelete.as_view(),
            request, args=(self.setup.pk,))

        self.user.delete()
        self.user = create_user(['administrador'])

    def test_setup_identifier_create(self):
        """Test Setup Identifier create view."""
        self.assertEqual(SetupIdentifier.objects.all().count(), 1)
        post_data = {"issuing_body": "a", "mask": "b", "name": "tt"}
        request_post = self.factory.post(
            reverse_lazy('setup_identifier_add'), data=post_data)
        request_post.user = self.user
        response_post = SetupIdentifierCreate.as_view()(request_post)
        self.assertEqual(response_post.status_code, 302)
        self.assertEqual(SetupIdentifier.objects.all().count(), 2)
        
    def test_setup_identifier_update(self):
        """Test Setup Identifier update view."""
        self.assertEqual(SetupIdentifier.objects.all().count(), 1)
        alter_post = {"issuing_body": "ab", "mask": "bcv", "name": "ty"}
        request_post = self.factory.post(
            reverse_lazy('setup_identifier_edit', args=(self.setup.pk,)), data=alter_post)
        request_post.user = self.user
        response_post = SetupIdentifierUpdate.as_view()(request_post, pk=self.setup.pk)
        self.assertEqual(response_post.status_code, 302)
        setup = SetupIdentifier.objects.get(pk=self.setup.pk)
        self.assertEqual(setup.issuing_body, alter_post['issuing_body'])
        self.assertEqual(setup.mask, alter_post['mask'])

    def test_setup_identifier_list(self):
        """Test Setup Identifier list view."""
        request = self.factory.get(reverse_lazy('setup_identifier_list'))
        request.user = self.user
        response = SetupIdentifierList.as_view()(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.context_data[u'setupidentifier_list'].count(), 1)
        self.assertEqual(
            response.context_data[u'setupidentifier_list'][0].pk,
            self.setup.pk)

    def test_setup_identifier_delete(self):
        """Test Setup Identifier delete view."""
        self.assertEqual(SetupIdentifier.objects.all().count(), 1)
        setup = SetupIdentifier.objects.create(
            issuing_body='teste',
            mandatory=True,
            mask='xxx')
        request = self.factory.post(
            reverse_lazy('setup_identifier_delete', args=(setup.pk,)), {})
        request.user = self.user
        response = SetupIdentifierDelete.as_view()(request, pk=setup.pk)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(SetupIdentifier.objects.all().count(), 1)

    def test_setup_identifier_query(self):
        """Verify  query"""
        setup = SetupIdentifier.objects.create(
            issuing_body='teste',
            mandatory=True,
            mask='xxx')
        setup.save()
        request = self.factory.get(reverse_lazy(
            'setup_identifier_list'), {'q': 'teste'})
        request.user = self.user
        response = SetupIdentifierList.as_view()(request)
        self.assertEqual(response.status_code, 200)
        request = self.factory.get(
            reverse_lazy('setup_identifier_list'), {'q': 'chemistry'})
        request.user = self.user
        response = SetupIdentifierList.as_view()(request)
        self.assertEqual(response.status_code, 200)
        setup.delete()

