# -*- coding: utf-8 -*-

from django.test import TestCase
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from administration.views import *
from django.test.client import RequestFactory
from django.core.urlresolvers import reverse_lazy
from django.core.exceptions import PermissionDenied
from django.utils.translation import ugettext as _
from mock import Mock, MagicMock
from django.contrib import messages

User = get_user_model()


def create_user(groups=[]):
    """Create a user and add to a group parameter.
    """
    user = User.objects.create_user(
        'user', 'user@email.com',
        'pass', gender=1, race=1, country_id=u'BR')
    user.save()
    for group in groups:
        g = Group.objects.get(name=group)
        g.user_set.add(user)
    return user

class ObservationStudyDesignTesteCase(TestCase):
    """Observation Study Design class test."""
    def setUp(self):
        """Register observation Porpose."""
        self.observation = ObservationStudyDesign.objects.create(
            description='teste')
        self.factory = RequestFactory()
        self.user = create_user(['administrador'])
        self.original_success = messages.success
        messages.success = Mock()
        self.original_info = messages.info
        messages.info = Mock()
        self.original_error = messages.error
        messages.error = Mock()


    def tearDown(self):
        """Delete user."""
        self.user.delete()
        self.observation.delete()
        messages.success = self.original_success
        messages.info = self.original_info
        messages.error = self.original_error

    def test_observation_study_design_create(self):
        """Test Observation Study Design create view."""
        post_data = {"description": "teste111"}
        request_post = self.factory.post(
            reverse_lazy('observation_study_design_add'), data=post_data)
        request_post.user = self.user
        response_post = ObservationStudyDesignCreate.as_view()(request_post)
        self.assertEqual(response_post.status_code, 302)
        self.assertEqual(ObservationStudyDesign.objects.all().count(), 2)

    def test_observation_study_design_update(self):
        """Test Observation Study Design update view."""
        alter_post = {"description": "ab"}
        request_post = self.factory.post(
            reverse_lazy('observation_study_design_edit', args=(self.observation.pk,)), data=alter_post)
        request_post.user = self.user
        response_post = ObservationStudyDesignUpdate.as_view()(request_post, pk=self.observation.pk)
        self.assertEqual(response_post.status_code, 302)
        observation = ObservationStudyDesign.objects.get(pk=self.observation.pk)
        self.assertEqual(observation.description, alter_post['description'])

    def test_observation_study_design_list(self):
        """Test Observation Study Design list view."""
        request = self.factory.get(
            reverse_lazy('observation_study_design_list'))
        request.user = self.user
        response = ObservationStudyDesignList.as_view()(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.context_data[u'observationstudydesign_list'].count(), 1)

    def test_observation_study_design_delete(self):
        """Test Observation Study Design delete view."""
        observation = ObservationStudyDesign.objects.create(
            description='tata')
        request = self.factory.post(
            reverse_lazy('observation_study_design_delete', args=(observation.pk,)), {})
        request.user = self.user
        response = ObservationStudyDesignDelete.as_view()(request, pk=observation.pk)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(ObservationStudyDesign.objects.all().count(), 1)

    def test_observation_study_design_list_permission(self):
        """Verify observation code list permission"""
        request = self.factory.get(reverse_lazy('observation_study_design_list'))
        request.user = self.user
        response = ObservationStudyDesignList.as_view()(request)
        self.assertEqual(response.status_code, 200)
        self.user.delete()
        self.user = create_user()
        request = self.factory.get(reverse_lazy('observation_study_design_list'))
        request.user = self.user
        self.assertRaises(PermissionDenied, ObservationStudyDesignList.as_view(), request)
        self.user.delete()
        self.user = create_user(['administrador'])

    def test_observation_detail_permission(self):
        """Verify ObservationStudyDesign detail permission"""
        request = self.factory.get(
            reverse_lazy('observation_study_design_detail', args=(self.observation.pk,)))
        request.user = self.user
        response = ObservationStudyDesignDetail.as_view()(request, pk=self.observation.pk)
        self.assertEqual(response.status_code, 200)
        self.user.delete()
        self.user = create_user()
        request = self.factory.get(
            reverse_lazy('observation_study_design_detail', args=(self.observation.pk,)))
        request.user = self.user
        self.assertRaises(
            PermissionDenied, ObservationStudyDesignDetail.as_view(),
            request, args=(self.observation.pk,))
        self.user.delete()
        self.user = create_user(['administrador'])

    def test_observation_create_permission(self):
        """Verify ObservationStudyDesign create permission"""
        request = self.factory.post(reverse_lazy('observation_study_design_add'))
        request.user = self.user
        response = ObservationStudyDesignCreate.as_view()(request)
        self.assertEqual(response.status_code, 200)
        self.user.delete()
        self.user = create_user()
        request = self.factory.get(reverse_lazy('observation_study_design_add'))
        request.user = self.user
        self.assertRaises(
            PermissionDenied, ObservationStudyDesignCreate.as_view(), request)
        self.user.delete()
        self.user = create_user(['administrador'])

    def test_observation_update_permission(self):
        """Verify ObservationStudyDesign update permission"""
        request = self.factory.get(
            reverse_lazy('observation_study_design_edit', args=(self.observation.pk,)))
        request.user = self.user
        response = ObservationStudyDesignUpdate.as_view()(request, pk=self.observation.pk)
        self.assertEqual(response.status_code, 200)
        self.user.delete()
        self.user = create_user()
        request = self.factory.get(
            reverse_lazy('observation_study_design_edit', args=(self.observation.pk,)))
        request.user = self.user
        self.assertRaises(
            PermissionDenied, ObservationStudyDesignUpdate.as_view(),
            request, args=(self.observation.pk,))
        self.user.delete()
        self.user = create_user(['administrador'])

    def test_observation_delete_permission(self):
        """Verify ObservationStudyDesign delete permission"""
        request = self.factory.get(
            reverse_lazy('observation_study_design_delete', args=(self.observation.pk,)))
        request.user = self.user
        response = ObservationStudyDesignDelete.as_view()(request, pk=self.observation.pk)
        self.assertEqual(response.status_code, 200)

        request = self.factory.post(
            reverse_lazy('observation_study_design_delete', args=(self.observation.pk,)), {})
        request.user = self.user
        response = ObservationStudyDesignDelete.as_view()(request, pk=self.observation.pk)
        self.assertEqual(response.status_code, 302)

        self.user.delete()
        self.user = create_user()
        request = self.factory.get(
            reverse_lazy('observation_study_design_delete', args=(self.observation.pk,)))
        request.user = self.user
        self.assertRaises(
            PermissionDenied, ObservationStudyDesignDelete.as_view(),
            request, args=(self.observation.pk,))

        request = self.factory.post(
            reverse_lazy('observation_study_design_delete', args=(self.observation.pk,)), {})
        request.user = self.user
        self.assertRaises(
            PermissionDenied, ObservationStudyDesignDelete.as_view(),
            request, args=(self.observation.pk,))

        self.user.delete()
        self.user = create_user(['administrador'])
