# -*- coding: utf-8 -*-

from django.test import TestCase
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from administration.views import *
from django.test.client import RequestFactory
from django.core.urlresolvers import reverse_lazy
from django.core.exceptions import PermissionDenied
from django.utils.translation import ugettext as _
from mock import Mock, MagicMock
from django.contrib import messages

User = get_user_model()


def create_user(groups=[]):
    """Create a user and add to a group parameter.
    """
    user = User.objects.create_user(
        'user', 'user@email.com',
        'pass', gender=1, race=1, country_id=u'BR')
    user.save()
    for group in groups:
        g = Group.objects.get(name=group)
        g.user_set.add(user)
    return user

class MaskingTypeTesteCase(TestCase):
    """Masking Type class test."""
    def setUp(self):
        """Register masking Porpose."""
        self.masking = MaskingType.objects.create(
            description='teste')
        self.factory = RequestFactory()
        self.user = create_user(['administrador'])
        self.original_success = messages.success
        messages.success = Mock()
        self.original_info = messages.info
        messages.info = Mock()
        self.original_error = messages.error
        messages.error = Mock()

    def tearDown(self):
        """Delete Masking Type and user."""
        self.user.delete()
        messages.success = self.original_success
        messages.info = self.original_info
        messages.error = self.original_error

    def test_masking_type_create(self):
        """Test Masking Type create view."""
        post_data = {"description": "teste111"}
        request_post = self.factory.post(
            reverse_lazy('masking_type_add'), data=post_data)
        request_post.user = self.user
        response_post = MaskingTypeCreate.as_view()(request_post)
        self.assertEqual(response_post.status_code, 302)
        self.assertEqual(MaskingType.objects.all().count(), 2)

    def test_masking_type_update(self):
        """Test Masking Type update view."""
        alter_post = {"description": "ab"}
        request_post = self.factory.post(
            reverse_lazy('masking_type_edit', args=(self.masking.pk,)), data=alter_post)
        request_post.user = self.user
        response_post = MaskingTypeUpdate.as_view()(request_post, pk=self.masking.pk)
        self.assertEqual(response_post.status_code, 302)
        masking = MaskingType.objects.get(pk=self.masking.pk)
        self.assertEqual(masking.description, alter_post['description'])

    def test_masking_type_list(self):
        """Test Masking Type list view."""
        request = self.factory.get(
            reverse_lazy('masking_type_list'))
        request.user = self.user
        response = MaskingTypeList.as_view()(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.context_data[u'maskingtype_list'].count(), 1)

    def test_masking_type_delete(self):
        """Test Masking Type delete view."""
        masking = MaskingType.objects.create(
            description='tata')
        request = self.factory.post(
            reverse_lazy('masking_type_delete', args=(masking.pk,)), {})
        request.user = self.user
        response = MaskingTypeDelete.as_view()(request, pk=masking.pk)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(MaskingType.objects.all().count(), 1)

    def test_masking_type_list_permission(self):
        """Verify masking code list permission"""
        request = self.factory.get(reverse_lazy('masking_type_list'))
        request.user = self.user
        response = MaskingTypeList.as_view()(request)
        self.assertEqual(response.status_code, 200)
        self.user.delete()
        self.user = create_user()
        request = self.factory.get(reverse_lazy('masking_type_list'))
        request.user = self.user
        self.assertRaises(PermissionDenied, MaskingTypeList.as_view(), request)
        self.user.delete()
        self.user = create_user(['administrador'])

    def test_masking_detail_permission(self):
        """Verify MaskingType detail permission"""
        request = self.factory.get(
            reverse_lazy('masking_type_detail', args=(self.masking.pk,)))
        request.user = self.user
        response = MaskingTypeDetail.as_view()(request, pk=self.masking.pk)
        self.assertEqual(response.status_code, 200)
        self.user.delete()
        self.user = create_user()
        request = self.factory.get(
            reverse_lazy('masking_type_detail', args=(self.masking.pk,)))
        request.user = self.user
        self.assertRaises(
            PermissionDenied, MaskingTypeDetail.as_view(),
            request, args=(self.masking.pk,))
        self.user.delete()
        self.user = create_user(['administrador'])

    def test_masking_create_permission(self):
        """Verify MaskingType create permission"""
        request = self.factory.post(reverse_lazy('masking_type_add'))
        request.user = self.user
        response = MaskingTypeCreate.as_view()(request)
        self.assertEqual(response.status_code, 200)
        self.user.delete()
        self.user = create_user()
        request = self.factory.get(reverse_lazy('masking_type_add'))
        request.user = self.user
        self.assertRaises(
            PermissionDenied, MaskingTypeCreate.as_view(), request)
        self.user.delete()
        self.user = create_user(['administrador'])

    def test_masking_update_permission(self):
        """Verify MaskingType update permission"""
        request = self.factory.get(
            reverse_lazy('masking_type_edit', args=(self.masking.pk,)))
        request.user = self.user
        response = MaskingTypeUpdate.as_view()(request, pk=self.masking.pk)
        self.assertEqual(response.status_code, 200)
        self.user.delete()
        self.user = create_user()
        request = self.factory.get(
            reverse_lazy('masking_type_edit', args=(self.masking.pk,)))
        request.user = self.user
        self.assertRaises(
            PermissionDenied, MaskingTypeUpdate.as_view(),
            request, args=(self.masking.pk,))
        self.user.delete()
        self.user = create_user(['administrador'])

    def test_masking_delete_permission(self):
        """Verify MaskingType delete permission"""
        request = self.factory.get(
            reverse_lazy('masking_type_delete', args=(self.masking.pk,)))
        request.user = self.user
        response = MaskingTypeDelete.as_view()(request, pk=self.masking.pk)
        self.assertEqual(response.status_code, 200)

        request = self.factory.post(
            reverse_lazy('masking_type_delete', args=(self.masking.pk,)), {})
        request.user = self.user
        response = MaskingTypeDelete.as_view()(request, pk=self.masking.pk)
        self.assertEqual(response.status_code, 302)

        self.user.delete()
        self.user = create_user()
        request = self.factory.get(
            reverse_lazy('masking_type_delete', args=(self.masking.pk,)))
        request.user = self.user
        self.assertRaises(
            PermissionDenied, MaskingTypeDelete.as_view(),
            request, args=(self.masking.pk,))

        request = self.factory.post(
            reverse_lazy('masking_type_delete', args=(self.masking.pk,)), {})
        request.user = self.user
        self.assertRaises(
            PermissionDenied, MaskingTypeDelete.as_view(),
            request, args=(self.masking.pk,))

        self.user.delete()
        self.user = create_user(['administrador'])
