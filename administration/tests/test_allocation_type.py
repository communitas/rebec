# -*- coding: utf-8 -*-

from django.test import TestCase
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from administration.views import *
from administration.models import *
from django.test.client import RequestFactory
from django.core.urlresolvers import reverse_lazy
from django.core.exceptions import PermissionDenied
from django.utils.translation import ugettext as _
from mock import Mock, MagicMock
from django.contrib import messages

User = get_user_model()


def create_user(groups=[]):
    """Create a user and add to a group parameter.
    """
    user = User.objects.create_user(
        'user', 'user@email.com',
        'pass', gender=1, race=1, country_id=u'BR')
    user.save()
    for group in groups:
        g = Group.objects.get(name=group)
        g.user_set.add(user)
    return user

class AllocationTypeTesteCase(TestCase):
    """Allocation Type class test."""
    def setUp(self):
        """Register allocation Porpose."""
        self.allocation = AllocationType.objects.create(
            description='teste')
        self.factory = RequestFactory()
        self.user = create_user(['administrador'])
        self.original_success = messages.success
        messages.success = Mock()
        self.original_info = messages.info
        messages.info = Mock()
        self.original_error = messages.error
        messages.error = Mock()

    def tearDown(self):
        """Delete user."""
        self.user.delete()
        self.allocation.delete()
        messages.success = self.original_success
        messages.info = self.original_info
        messages.error = self.original_error

    def test_allocation_type_create(self):
        """Test Allocation Type create view."""
        post_data = {"description": "teste111"}
        request_post = self.factory.post(
            reverse_lazy('allocation_type_add'), data=post_data)
        request_post.user = self.user
        response_post = AllocationTypeCreate.as_view()(request_post)
        self.assertEqual(response_post.status_code, 302)
        self.assertEqual(AllocationType.objects.all().count(), 2)
        AllocationType.objects.all().delete()

    def test_allocation_type_update(self):
        """Test Allocation Type update view."""
        alter_post = {"description": "ab"}
        request_post = self.factory.post(
            reverse_lazy('allocation_type_edit', args=(self.allocation.pk,)), data=alter_post)
        request_post.user = self.user
        response_post = AllocationTypeUpdate.as_view()(request_post, pk=self.allocation.pk)
        self.assertEqual(response_post.status_code, 302)
        allocation = AllocationType.objects.get(pk=self.allocation.pk)
        self.assertEqual(allocation.description, alter_post['description'])
        
    def test_allocation_type_list(self):
        """Test Allocation Type list view."""
        request = self.factory.get(
            reverse_lazy('allocation_type_list'))
        request.user = self.user
        response = AllocationTypeList.as_view()(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.context_data[u'allocationtype_list'].count(), 1)

    def test_allocation_type_delete(self):
        """Test Allocation Type delete view."""
        allocation = AllocationType.objects.create(
            description='tata')
        request = self.factory.post(
            reverse_lazy('allocation_type_delete', args=(allocation.pk,)), {})
        request.user = self.user
        response = AllocationTypeDelete.as_view()(request, pk=allocation.pk)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(AllocationType.objects.all().count(), 1)


    def test_allocation_list_permission(self):
        """Verify AllocationType list permission"""
        request = self.factory.get(reverse_lazy('allocation_type_list'))
        request.user = self.user
        response = AllocationTypeList.as_view()(request)
        self.assertEqual(response.status_code, 200)
        self.user.delete()
        self.user = create_user()
        request = self.factory.get(reverse_lazy('allocation_type_list'))
        request.user = self.user
        self.assertRaises(PermissionDenied, AllocationTypeList.as_view(), request)
        self.user.delete()
        self.user = create_user(['administrador'])

    def test_allocation_detail_permission(self):
        """Verify AllocationType detail permission"""
        request = self.factory.get(
            reverse_lazy('allocation_type_detail', args=(self.allocation.pk,)))
        request.user = self.user
        response = AllocationTypeDetail.as_view()(request, pk=self.allocation.pk)
        self.assertEqual(response.status_code, 200)
        self.user.delete()
        self.user = create_user()
        request = self.factory.get(
            reverse_lazy('allocation_type_detail', args=(self.allocation.pk,)))
        request.user = self.user
        self.assertRaises(
            PermissionDenied, AllocationTypeDetail.as_view(),
            request, args=(self.allocation.pk,))
        self.user.delete()
        self.user = create_user(['administrador'])

    def test_allocation_create_permission(self):
        """Verify AllocationType create permission"""
        request = self.factory.post(reverse_lazy('allocation_type_add'))
        request.user = self.user
        response = AllocationTypeCreate.as_view()(request)
        self.assertEqual(response.status_code, 200)
        self.user.delete()
        self.user = create_user()
        request = self.factory.get(reverse_lazy('allocation_type_add'))
        request.user = self.user
        self.assertRaises(
            PermissionDenied, AllocationTypeCreate.as_view(), request)
        self.user.delete()
        self.user = create_user(['administrador'])

    def test_allocation_update_permission(self):
        """Verify AllocationType update permission"""
        request = self.factory.get(
            reverse_lazy('allocation_type_edit', args=(self.allocation.pk,)))
        request.user = self.user
        response = AllocationTypeUpdate.as_view()(request, pk=self.allocation.pk)
        self.assertEqual(response.status_code, 200)
        self.user.delete()
        self.user = create_user()
        request = self.factory.get(
            reverse_lazy('allocation_type_edit', args=(self.allocation.pk,)))
        request.user = self.user
        self.assertRaises(
            PermissionDenied, AllocationTypeUpdate.as_view(),
            request, args=(self.allocation.pk,))
        self.user.delete()
        self.user = create_user(['administrador'])

    def test_allocation_delete_permission(self):
        """Verify AllocationType delete permission"""
        request = self.factory.get(
            reverse_lazy('allocation_type_delete', args=(self.allocation.pk,)))
        request.user = self.user
        response = AllocationTypeDelete.as_view()(request, pk=self.allocation.pk)
        self.assertEqual(response.status_code, 200)

        request = self.factory.post(
            reverse_lazy('allocation_type_delete', args=(self.allocation.pk,)), {})
        request.user = self.user
        response = AllocationTypeDelete.as_view()(request, pk=self.allocation.pk)
        self.assertEqual(response.status_code, 302)

        self.user.delete()
        self.user = create_user()
        request = self.factory.get(
            reverse_lazy('allocation_type_delete', args=(self.allocation.pk,)))
        request.user = self.user
        self.assertRaises(
            PermissionDenied, AllocationTypeDelete.as_view(),
            request, args=(self.allocation.pk,))

        request = self.factory.post(
            reverse_lazy('allocation_type_delete', args=(self.allocation.pk,)), {})
        request.user = self.user
        self.assertRaises(
            PermissionDenied, AllocationTypeDelete.as_view(),
            request, args=(self.allocation.pk,))

        self.user.delete()
        self.user = create_user(['administrador'])
