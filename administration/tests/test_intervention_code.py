# -*- coding: utf-8 -*-

from django.test import TestCase
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from administration.views import *
from django.test.client import RequestFactory
from django.core.urlresolvers import reverse_lazy
from django.core.exceptions import PermissionDenied
from django.utils.translation import ugettext as _
from mock import Mock, MagicMock
from django.contrib import messages

User = get_user_model()


def create_user(groups=[]):
    """Create a user and add to a group parameter.
    """
    user = User.objects.create_user(
        'user', 'user@email.com',
        'pass', gender=1, race=1, country_id=u'BR')
    user.save()
    for group in groups:
        g = Group.objects.get(name=group)
        g.user_set.add(user)
    return user

class InterventionCodeTesteCase(TestCase):
    """Intervention Code class test."""
    def setUp(self):
        """Register Intervention Code Porpose."""
        self.intervention = InterventionCode.objects.create(
            description='teste')
        self.factory = RequestFactory()
        self.user = create_user(['administrador'])
        self.original_success = messages.success
        messages.success = Mock()
        self.original_info = messages.info
        messages.info = Mock()
        self.original_error = messages.error
        messages.error = Mock()

    def tearDown(self):
        """Delete user."""
        self.user.delete()
        self.intervention.delete()
        messages.success = self.original_success
        messages.info = self.original_info
        messages.error = self.original_error

    def test_intervention_code_create(self):
        """Test Intervention Code create view."""
        post_data = {"description": "teste111"}
        request_post = self.factory.post(
            reverse_lazy('intervention_code_add'), data=post_data)
        request_post.user = self.user
        response_post = InterventionCodeCreate.as_view()(request_post)
        self.assertEqual(response_post.status_code, 302)
        self.assertEqual(InterventionCode.objects.all().count(), 2)
        intervention_code = InterventionCode.objects.get(pk=self.intervention.pk)
        intervention_code.delete()

    def test_intervention_code_update(self):
        """Test Intervention Code update view."""
        alter_post = {"description": "ab"}
        request_post = self.factory.post(
            reverse_lazy('intervention_code_edit', args=(self.intervention.pk,)), data=alter_post)
        request_post.user = self.user
        response_post = InterventionCodeUpdate.as_view()(request_post, pk=self.intervention.pk)
        self.assertEqual(response_post.status_code, 302)
        intervention_code = InterventionCode.objects.get(pk=self.intervention.pk)
        self.assertEqual(intervention_code.description, alter_post['description'])
        intervention_code.delete()

    def test_intervention_code_list(self):
        """Test Intervention Code list view."""
        request = self.factory.get(
            reverse_lazy('intervention_code_list'))
        request.user = self.user
        response = InterventionCodeList.as_view()(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.context_data[u'interventioncode_list'].count(), 1)

    def test_intervention_code_delete(self):
        """Test Intervention Code delete view."""
        intervention_code = InterventionCode.objects.create(
            description='tata')
        request = self.factory.post(
            reverse_lazy('intervention_code_delete', args=(intervention_code.pk,)), {})
        request.user = self.user
        response = InterventionCodeDelete.as_view()(request, pk=intervention_code.pk)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(InterventionCode.objects.all().count(), 1)

    def test_intervention_code_list_permission(self):
        """Verify intervention code list permission"""
        request = self.factory.get(reverse_lazy('intervention_code_list'))
        request.user = self.user
        response = InterventionCodeList.as_view()(request)
        self.assertEqual(response.status_code, 200)
        self.user.delete()
        self.user = create_user()
        request = self.factory.get(reverse_lazy('intervention_code_list'))
        request.user = self.user
        self.assertRaises(PermissionDenied, InterventionCodeList.as_view(), request)
        self.user.delete()
        self.user = create_user(['administrador'])

    def test_intervention_detail_permission(self):
        """Verify Interventioncode detail permission"""
        request = self.factory.get(
            reverse_lazy('intervention_code_detail', args=(self.intervention.pk,)))
        request.user = self.user
        response = InterventionCodeDetail.as_view()(request, pk=self.intervention.pk)
        self.assertEqual(response.status_code, 200)
        self.user.delete()
        self.user = create_user()
        request = self.factory.get(
            reverse_lazy('intervention_code_detail', args=(self.intervention.pk,)))
        request.user = self.user
        self.assertRaises(
            PermissionDenied, InterventionCodeDetail.as_view(),
            request, args=(self.intervention.pk,))
        self.user.delete()
        self.user = create_user(['administrador'])

    def test_intervention_create_permission(self):
        """Verify Interventioncode create permission"""
        request = self.factory.post(reverse_lazy('intervention_code_add'))
        request.user = self.user
        response = InterventionCodeCreate.as_view()(request)
        self.assertEqual(response.status_code, 200)
        self.user.delete()
        self.user = create_user()
        request = self.factory.get(reverse_lazy('intervention_code_add'))
        request.user = self.user
        self.assertRaises(
            PermissionDenied, InterventionCodeCreate.as_view(), request)
        self.user.delete()
        self.user = create_user(['administrador'])

    def test_intervention_update_permission(self):
        """Verify Interventioncode update permission"""
        request = self.factory.get(
            reverse_lazy('intervention_code_edit', args=(self.intervention.pk,)))
        request.user = self.user
        response = InterventionCodeUpdate.as_view()(request, pk=self.intervention.pk)
        self.assertEqual(response.status_code, 200)
        self.user.delete()
        self.user = create_user()
        request = self.factory.get(
            reverse_lazy('intervention_code_edit', args=(self.intervention.pk,)))
        request.user = self.user
        self.assertRaises(
            PermissionDenied, InterventionCodeUpdate.as_view(),
            request, args=(self.intervention.pk,))
        self.user.delete()
        self.user = create_user(['administrador'])

    def test_intervention_delete_permission(self):
        """Verify Interventioncode delete permission"""
        request = self.factory.get(
            reverse_lazy('intervention_code_delete', args=(self.intervention.pk,)))
        request.user = self.user
        response = InterventionCodeDelete.as_view()(request, pk=self.intervention.pk)
        self.assertEqual(response.status_code, 200)

        request = self.factory.post(
            reverse_lazy('intervention_code_delete', args=(self.intervention.pk,)), {})
        request.user = self.user
        response = InterventionCodeDelete.as_view()(request, pk=self.intervention.pk)
        self.assertEqual(response.status_code, 302)

        self.user.delete()
        self.user = create_user()
        request = self.factory.get(
            reverse_lazy('intervention_code_delete', args=(self.intervention.pk,)))
        request.user = self.user
        self.assertRaises(
            PermissionDenied, InterventionCodeDelete.as_view(),
            request, args=(self.intervention.pk,))

        request = self.factory.post(
            reverse_lazy('intervention_code_delete', args=(self.intervention.pk,)), {})
        request.user = self.user
        self.assertRaises(
            PermissionDenied, InterventionCodeDelete.as_view(),
            request, args=(self.intervention.pk,))

        self.user.delete()
        self.user = create_user(['administrador'])
