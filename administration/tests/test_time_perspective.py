# -*- coding: utf-8 -*-

from django.test import TestCase
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from administration.views import *
from django.test.client import RequestFactory
from django.core.urlresolvers import reverse_lazy
from django.core.exceptions import PermissionDenied
from django.utils.translation import ugettext as _
from django.contrib import messages
from mock import Mock, MagicMock


User = get_user_model()


def create_user(groups=[]):
    """Create a user and add to a group parameter.
    """
    user = User.objects.create_user(
        'user', 'user@email.com',
        'pass', gender=1, race=1, country_id=u'BR')
    user.save()
    for group in groups:
        g = Group.objects.get(name=group)
        g.user_set.add(user)
    return user

class TimePerspectiveTesteCase(TestCase):
    """Time Perspective class test."""
    def setUp(self):
        """Register Time Perspective Porpose."""
        self.time_perspective = TimePerspective.objects.create(
            description='teste')
        self.factory = RequestFactory()
        self.user = create_user(['administrador'])
        self.original_success = messages.success
        messages.success = Mock()
        self.original_info = messages.info
        messages.info = Mock()
        self.original_error = messages.error
        messages.error = Mock()

    def tearDown(self):
        """Delete user."""
        self.user.delete()
        self.time_perspective.delete()
        messages.success = self.original_success
        messages.info = self.original_info
        messages.error = self.original_error

    def test_time_perspective_create(self):
        """Test Time Perspective create view."""
        post_data = {"description": "teste111"}
        request_post = self.factory.post(
            reverse_lazy('time_perspective_add'), data=post_data)
        request_post.user = self.user
        response_post = TimePerspectiveCreate.as_view()(request_post)
        self.assertEqual(response_post.status_code, 302)
        self.assertEqual(TimePerspective.objects.all().count(), 2)

    def test_time_perspective_update(self):
        """Test Time Perspective update view."""
        alter_post = {"description": "ab"}
        request_post = self.factory.post(
            reverse_lazy('time_perspective_edit', args=(self.time_perspective.pk,)), data=alter_post)
        request_post.user = self.user
        response_post = TimePerspectiveUpdate.as_view()(request_post, pk=self.time_perspective.pk)
        self.assertEqual(response_post.status_code, 302)
        time_perspective = TimePerspective.objects.get(pk=self.time_perspective.pk)
        self.assertEqual(time_perspective.description, alter_post['description'])

    def test_time_perspective_list(self):
        """Test Time Perspective list view."""
        request = self.factory.get(
            reverse_lazy('time_perspective_list'))
        request.user = self.user
        response = TimePerspectiveList.as_view()(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.context_data[u'timeperspective_list'].count(), 1)
        
    def test_time_perspective_delete(self):
        """Test Time Perspective delete view."""
        time_perspective = TimePerspective.objects.create(
            description='tata')
        request = self.factory.post(
            reverse_lazy('time_perspective_delete', args=(time_perspective.pk,)), {})
        request.user = self.user
        response = TimePerspectiveDelete.as_view()(request, pk=time_perspective.pk)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(TimePerspective.objects.all().count(), 1)

    def test_time_perspective_list_permission(self):
        """Verify time_perspective list permission"""
        request = self.factory.get(reverse_lazy('time_perspective_list'))
        request.user = self.user
        response = TimePerspectiveList.as_view()(request)
        self.assertEqual(response.status_code, 200)
        self.user.delete()
        self.user = create_user()
        request = self.factory.get(reverse_lazy('time_perspective_list'))
        request.user = self.user
        self.assertRaises(PermissionDenied, TimePerspectiveList.as_view(), request)
        self.user.delete()
        self.user = create_user(['administrador'])

    def test_time_perspective_detail_permission(self):
        """Verify TimePerspective detail permission"""
        request = self.factory.get(
            reverse_lazy('time_perspective_detail', args=(self.time_perspective.pk,)))
        request.user = self.user
        response = TimePerspectiveDetail.as_view()(request, pk=self.time_perspective.pk)
        self.assertEqual(response.status_code, 200)
        self.user.delete()
        self.user = create_user()
        request = self.factory.get(
            reverse_lazy('time_perspective_detail', args=(self.time_perspective.pk,)))
        request.user = self.user
        self.assertRaises(
            PermissionDenied, TimePerspectiveDetail.as_view(),
            request, args=(self.time_perspective.pk,))
        self.user.delete()
        self.user = create_user(['administrador'])

    def test_time_perspective_create_permission(self):
        """Verify TimePerspective create permission"""
        request = self.factory.post(reverse_lazy('time_perspective_add'))
        request.user = self.user
        response = TimePerspectiveCreate.as_view()(request)
        self.assertEqual(response.status_code, 200)
        self.user.delete()
        self.user = create_user()
        request = self.factory.get(reverse_lazy('time_perspective_add'))
        request.user = self.user
        self.assertRaises(
            PermissionDenied, TimePerspectiveCreate.as_view(), request)
        self.user.delete()
        self.user = create_user(['administrador'])

    def test_time_perspective_update_permission(self):
        """Verify TimePerspective update permission"""
        request = self.factory.get(
            reverse_lazy('time_perspective_edit', args=(self.time_perspective.pk,)))
        request.user = self.user
        response = TimePerspectiveUpdate.as_view()(request, pk=self.time_perspective.pk)
        self.assertEqual(response.status_code, 200)
        self.user.delete()
        self.user = create_user()
        request = self.factory.get(
            reverse_lazy('time_perspective_edit', args=(self.time_perspective.pk,)))
        request.user = self.user
        self.assertRaises(
            PermissionDenied, TimePerspectiveUpdate.as_view(),
            request, args=(self.time_perspective.pk,))
        self.user.delete()
        self.user = create_user(['administrador'])

    def test_time_perspective_delete_permission(self):
        """Verify TimePerspective delete permission"""
        request = self.factory.get(
            reverse_lazy('time_perspective_delete', args=(self.time_perspective.pk,)))
        request.user = self.user
        response = TimePerspectiveDelete.as_view()(request, pk=self.time_perspective.pk)
        self.assertEqual(response.status_code, 200)

        request = self.factory.post(
            reverse_lazy('time_perspective_delete', args=(self.time_perspective.pk,)), {})
        request.user = self.user
        response = TimePerspectiveDelete.as_view()(request, pk=self.time_perspective.pk)
        self.assertEqual(response.status_code, 302)

        self.user.delete()
        self.user = create_user()
        request = self.factory.get(
            reverse_lazy('time_perspective_delete', args=(self.time_perspective.pk,)))
        request.user = self.user
        self.assertRaises(
            PermissionDenied, TimePerspectiveDelete.as_view(),
            request, args=(self.time_perspective.pk,))

        request = self.factory.post(
            reverse_lazy('time_perspective_delete', args=(self.time_perspective.pk,)), {})
        request.user = self.user
        self.assertRaises(
            PermissionDenied, TimePerspectiveDelete.as_view(),
            request, args=(self.time_perspective.pk,))

        self.user.delete()
        self.user = create_user(['administrador'])

