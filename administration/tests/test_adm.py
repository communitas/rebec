# -*- coding: utf-8 -*-

from datetime import datetime
from django.test import TestCase
from administration.backends import EmailBackend
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.test.client import Client, RequestFactory
from administration.models import *
from administration.views import *
from django.core.urlresolvers import reverse_lazy
from mock import Mock, MagicMock
from django.contrib import messages
from clinical_trials.models import Contact
from django.core.exceptions import PermissionDenied
from workflows.models import State
from workflows.utils import set_state
User = get_user_model()


def create_user(groups=[]):
    """Create a user and add to a group parameter.
    """
    user = User.objects.create_user(
        'user', 'user@email.com',
        'pass', gender=1, race=1, country_id=u'BR')
    user.save()
    for group in groups:
        g = Group.objects.get(name=group)
        g.user_set.add(user)
    return user


class EmailBackendTesteCase(TestCase):
    """Classe test autenticate email and username"""

    def setUp(self):
        """Create user"""
        self.user = create_user()

    def tearDown(self):
        """Delete user"""
        self.user.delete()

    def test_user_autenticate_username(self):
        """User authentication by username"""
        self.client.login(username='user', password='pass')
        self.assertEqual(self.client.session['_auth_user_id'], self.user.pk)

    def test_user_autenticate_email(self):
        """User authentication by email"""
        self.client.login(username='user@email.com', password='pass')
        self.assertEqual(self.client.session['_auth_user_id'], self.user.pk)

    def test_user_not_autenticate(self):
        """User authentication failed"""
        self.client.login(username='u', password='pass')
        self.assertEqual(self.client.login(), False)


class UserProfileUpdateTesteCase(TestCase):

    def setUp(self):
        """Create user"""
        self.user = User.objects.create_user(
            name='teste',
            username='admin',
            gender=1,
            race=1,
            email='admin@admin.com',
            country_id=u'BR',
            password='Teste123@'
        )
        self.user2 = User.objects.create_user(
            name='tes',
            username='adm',
            gender=1,
            race=1,
            email='tes@admin.com',
            country_id=u'BR',
            password='Teste123@'
        )
        self.factory = RequestFactory()
        self.messages = messages.success

    def tearDown(self):
        """Delete user"""
        self.user.delete()
        self.user2.delete()
        messages.success = self.messages

    def test_update_profile(self):
        """Test update profile, and verify messages return."""
        alter_post = {
            'username': 'admin',
            'race': 1,
            'gender': 2,
            'name': 'admin',
            'email': 'admin@admin.com',
            'password':'Teste123@',
            'country':'BR'
        }
        messages.success = MagicMock(return_value="Ok")
        request_post = self.factory.post(
            reverse('user_profile_edit', args=(self.user.pk,)), data=alter_post)
        response_post = UserProfileEdit.as_view()(request_post, pk=self.user.pk)
        self.assertEqual(response_post.status_code, 302)
        self.assertEqual(messages.success.called, True)
        

    def test_user_detail(self):
        """Verify User detail."""
        request = self.factory.get(
            reverse_lazy('user_profile_detail', args=(self.user2.pk,)))
        request.user = self.user
        response = UserProfileDetail.as_view()(request, pk=self.user2.pk)
        self.assertEqual(response.status_code, 200)
        
        
class ContactsManagementTestCase(TestCase):

    def setUp(self):
        self.factory = RequestFactory()
        self.user = create_user()

        self.user2 = User.objects.create_user(
            'user2', 'user2@email.com',
            'pass', gender=1, race=1, country_id=u'BR')
        self.user2.save()

        self.setup = Setup()
        self.setup.language_1_code_id = 167
        self.setup.language_1_mandatory = True
        self.setup.language_2_code_id = 57
        self.setup.language_2_mandatory = True
        self.setup.language_3_code_id = 61
        self.setup.language_3_mandatory = False
        self.setup.queue_distribution = 'lala'
        self.setup.current_position_qd0 = 1
        self.setup.save()


        self.group1 = CTGroup.objects.create(name='g1', visibility=1)
        
        self.group2 = CTGroup.objects.create(name='g2', visibility=1)
        self.group3 = CTGroup.objects.create(name='g3', visibility=1)
        self.user.ctgroups.create(ct_group=self.group2,
                                  status=CTGroupUser.STATUS_ACTIVE,
                                  type_user=CTGroupUser.TYPE_ADMINISTRATOR)

        self.user.ctgroups.create(ct_group=self.group1,
                                  status=CTGroupUser.STATUS_ACTIVE,
                                  type_user=CTGroupUser.TYPE_REGISTRANT)

        self.user.ctgroups.create(ct_group=self.group3,
                                  status=CTGroupUser.STATUS_INACTIVE,
                                  type_user=CTGroupUser.TYPE_ADMINISTRATOR)

        self.user2.ctgroups.create(ct_group=self.group1,
                                  status=CTGroupUser.STATUS_ACTIVE,
                                  type_user=CTGroupUser.TYPE_ADMINISTRATOR)

        self.user2.ctgroups.create(ct_group=self.group2,
                                  status=CTGroupUser.STATUS_ACTIVE,
                                  type_user=CTGroupUser.TYPE_REGISTRANT)

        self.user2.ctgroups.create(ct_group=self.group3,
                                  status=CTGroupUser.STATUS_INACTIVE,
                                  type_user=CTGroupUser.TYPE_ADMINISTRATOR)


        self.ct1 = ClinicalTrial()
        self.ct1.user = self.user2
        self.ct1.group = self.group1
        self.ct1.save()

        self.ct2 = ClinicalTrial()
        self.ct2.user = self.user2
        self.ct2.group = self.group3
        self.ct2.save()

        self.country = Country.objects.create(code="br")
        self.inst_type = InstitutionType.objects.create()
        self.institution = Institution.objects.create(
            country=self.country,
            institution_type=self.inst_type
        )
        self.contact1 = Contact.objects.create(
            institution=self.institution,
            phone=u'phone',
            country=self.country
        )
        self.contact2 = Contact.objects.create(
            institution=self.institution,
            phone=u'phone',
            country=self.country
        )
        self.contact1.ctcontact_set.create(clinical_trial=self.ct1)
        self.contact2.ctcontact_set.create(clinical_trial=self.ct2)
        #mocks
        self.original_success = messages.success
        messages.success = Mock()

    def tearDown(self):

        messages.success = self.original_success
        self.setup.delete()
        self.ct1.delete()
        self.ct2.delete()
        self.group1.users.all().delete()
        self.group2.users.all().delete()
        self.group3.users.all().delete()
        self.group1.delete()
        self.group2.delete()
        self.group3.delete()
        self.user.delete()
        self.user2.delete()
        self.contact1.delete()
        self.contact2.delete()

    def test_get_object_permission(self):
        request = self.factory.get(
            reverse(u'contact_manage_edit', args=(self.contact1.pk,))
        )

        request.user = self.user

        self.assertRaises(
            PermissionDenied,
            ContactManageEditView.as_view(),
            request,
            pk=self.contact1.pk
        )

        

        request.user = self.user2

        response = ContactManageEditView.as_view()(
            request, pk=self.contact1.pk
        )
        self.assertEqual(response.status_code, 200)


class RecruitmentInterruptionsTestCase(TestCase):

    def setUp(self):
        self.factory = RequestFactory()
        self.user = create_user()

        self.user2 = User.objects.create_user(
            'user2', 'user2@email.com',
            'pass', gender=1, race=1, country_id=u'BR')
        self.user2.save()

        self.setup = Setup()
        self.setup.language_1_code_id = 167
        self.setup.language_1_mandatory = True
        self.setup.language_2_code_id = 57
        self.setup.language_2_mandatory = True
        self.setup.language_3_code_id = 61
        self.setup.language_3_mandatory = False
        self.setup.queue_distribution = 'lala'
        self.setup.current_position_qd0 = 1
        self.setup.save()


        self.group1 = CTGroup.objects.create(name='g1', visibility=1)
        
        self.group2 = CTGroup.objects.create(name='g2', visibility=1)
        self.group3 = CTGroup.objects.create(name='g3', visibility=1)
        self.user.ctgroups.create(ct_group=self.group2,
                                  status=CTGroupUser.STATUS_ACTIVE,
                                  type_user=CTGroupUser.TYPE_ADMINISTRATOR)

        self.user.ctgroups.create(ct_group=self.group1,
                                  status=CTGroupUser.STATUS_ACTIVE,
                                  type_user=CTGroupUser.TYPE_REGISTRANT)

        self.user.ctgroups.create(ct_group=self.group3,
                                  status=CTGroupUser.STATUS_INACTIVE,
                                  type_user=CTGroupUser.TYPE_ADMINISTRATOR)

        self.user2.ctgroups.create(ct_group=self.group1,
                                  status=CTGroupUser.STATUS_ACTIVE,
                                  type_user=CTGroupUser.TYPE_ADMINISTRATOR)

        self.user2.ctgroups.create(ct_group=self.group2,
                                  status=CTGroupUser.STATUS_ACTIVE,
                                  type_user=CTGroupUser.TYPE_REGISTRANT)

        self.user2.ctgroups.create(ct_group=self.group3,
                                  status=CTGroupUser.STATUS_INACTIVE,
                                  type_user=CTGroupUser.TYPE_ADMINISTRATOR)


        self.ct1 = ClinicalTrial()
        self.ct1.user = self.user
        self.ct1.group = self.group1
        self.ct1.save()

        self.ct2 = ClinicalTrial()
        self.ct2.user = self.user2
        self.ct2.group = self.group3
        self.ct2.save()

        #mocks
        self.original_success = messages.success
        messages.success = Mock()

    def tearDown(self):

        messages.success = self.original_success
        self.setup.delete()
        self.ct1.delete()
        self.ct2.delete()
        self.group1.users.all().delete()
        self.group2.users.all().delete()
        self.group3.users.all().delete()
        self.group1.delete()
        self.group2.delete()
        self.group3.delete()
        self.user.stateobjecthistory_set.all().delete()
        self.user.delete()
        self.user2.delete()

    def test_list(self):
        request = self.factory.get(reverse(u'recruitment_interrupt_list'))
        request.user = self.user


        response = RecruitmentInterruptListView.as_view()(request)
        self.assertEqual(response.context_data[u'object_list'].count(), 0)


        request.user = self.user2
        self.ct2.date_first_enrollment = datetime.now().date()
        self.ct2.date_last_enrollment = datetime.now().date()
        self.ct1.date_first_enrollment = datetime.now().date()
        self.ct1.date_last_enrollment = datetime.now().date()
        self.ct2.save()
        self.ct1.save()
        set_state(self.ct1, State.objects.get(pk=State.STATE_PUBLICADO))


        response = RecruitmentInterruptListView.as_view()(request)
        self.assertEqual(response.context_data[u'object_list'].count(), 1)

    def test_get_object_permission(self):
        request = self.factory.get(
                reverse(u'recruitment_interrupt', args=(self.ct1.pk,))
        )
        request.user = self.user

        self.assertRaises(
            PermissionDenied,
            RecruitmentInterruptView.as_view(),
            request,
            pk=self.ct1.pk
        )

        request.user = self.user2

        response = RecruitmentInterruptView.as_view()(
            request, pk=self.ct1.pk
        )
        self.assertEqual(response.status_code, 200)

    def test_change_interruption_state(self):
        #interrupt the recruitment
        request = self.factory.post(
                reverse(u'recruitment_interrupt', args=(self.ct1.pk,)),
                data={u'confirm': u'yes', u'comments': u'i want it'}
        )
        request.user = self.user2
        self.assertEqual(self.ct1.recruitment_interruptions.all().count(), 0)
        response = RecruitmentInterruptView.as_view()(
            request, pk=self.ct1.pk
        )
        self.assertEqual(response.status_code, 302)
        self.assertEqual(self.ct1.recruitment_interruptions.all().count(), 1)
        self.assertTrue(self.ct1.recruitment_interruptions.get().interrupt)
        self.assertEqual(self.ct1.recruitment_interruptions.get().comments, u'i want it')
        
        self.assertTrue(self.ct1.is_interrupted())

        #now, stop interruption
        response = RecruitmentInterruptView.as_view()(
            request, pk=self.ct1.pk
        )
        self.assertEqual(response.status_code, 302)
        self.assertEqual(self.ct1.recruitment_interruptions.all().count(), 2)
        self.assertFalse(self.ct1.recruitment_interruptions.latest(u'date_time').interrupt)
        self.assertIsNone(self.ct1.recruitment_interruptions.latest(u'date_time').comments)
        
        self.assertFalse(self.ct1.is_interrupted())
