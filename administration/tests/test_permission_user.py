# -*- coding: utf-8 -*-

from django.test import TestCase
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from administration.models import *
from administration.views import *
from django.test.client import RequestFactory
from django.core.urlresolvers import reverse_lazy
from django.utils.translation import ugettext as _
from login.models import *
from django.db.models import Q
from workflows.models import StateObjectRelation, State
from workflows import utils



User = get_user_model()


class PermissionUserTestCase(TestCase):
    """Country class test"""
    def setUp(self):
        """Register Country and add user with permission"""
        self.factory = RequestFactory()
        self.user1 = User.objects.create(username=u'e',
                                        email=u'e@e.com',
                                        gender=1,
                                        race=1,
                                        country_id=u'BR')
        self.user1.groups.add(
            Group.objects.get(
                name=u'registrant')
            )
        self.user2 = User.objects.create(username=u'el',
                                        email=u'ej@e.com',
                                        gender=1,
                                        race=1,
                                        country_id=u'BR')
        self.user2.groups.add(
            Group.objects.get(
                name=u'registrant')
            )
        self.user3 = User.objects.create(username=u'eli',
                                        email=u'eli@e.com',
                                        gender=1,
                                        race=1,
                                        country_id=u'BR')
        self.user3.groups.add(
            Group.objects.get(
                name=u'registrant')
            )
        self.user4 = User.objects.create(username=u'admin',
                                        email=u'admin@admin.com',
                                        gender=1,
                                        race=1,
                                        country_id=u'BR')
        self.user4.groups.add(
            Group.objects.get(
                name=u'administrador')
            )
        self.country = Country.objects.create(
            description='zzzzzzzzzzz',
            code="za")

        self.institution_type = InstitutionType.objects.create(
            description='a')

        self.institution = Institution.objects.create(
            name='Rebec',
            description='Rebec',
            postal_address='',
            city='',
            state='',
            country=self.country,
            status='A',
            institution_type=self.institution_type)

        self.ct_group = CTGroup.objects.create(
            name='teste1',
            description='teste',
            status=1,
            visibility=1)

        self.ct_group_user_1 = CTGroupUser.objects.create(
            status=2,
            type_user='a',
            user=self.user1,
            ct_group=self.ct_group)
        
        self.ct_group_user_2 = CTGroupUser.objects.create(
            status=2,
            type_user='r',
            user=self.user2,
            ct_group=self.ct_group)
       
  
        
    def tearDown(self):
        """Delete objects"""
        self.ct_group_user_2.delete()
        self.ct_group_user_1.delete()
        self.ct_group.delete()
        self.institution.delete()
        self.institution_type.delete()
        self.user1.stateobjecthistory_set.all().delete()
        self.user2.stateobjecthistory_set.all().delete()
        self.user2.delete()
        self.user1.delete()
        self.user3.delete()
        self.country.delete()

    def test_is_registrant_administrator(self):
        self.assertTrue(self.user1.is_registrant_administrator())
        self.assertFalse(self.user2.is_registrant_administrator())

    def test_get_pendencies_registrant(self):
        ct1 = ClinicalTrial.objects.create(user=self.user1, holder=self.user1)
        utils.set_state(ct1, State.objects.get(pk=State.STATE_RESUBMETIDO))
        self.assertEqual(self.user1.get_pendencies(), 1)
        ct_group_user_3 = CTGroupUser.objects.create(
            status=1,
            type_user='r',
            user=self.user3,
            ct_group=self.ct_group)
        self.assertEqual(self.user1.get_pendencies(), 2)
        ct_group_user_3.delete()
        ct_group_user_4 = CTGroupUser.objects.create(
            status=4,
            type_user='r',
            user=self.user3,
            ct_group=self.ct_group)
        self.assertEqual(self.user1.get_pendencies(), 2)
        ct_group_user_4.delete()
        ct1.delete()

    def test_get_pendencies_adm(self):
        institution_moderation = Institution.objects.create(
            name='Moderation',
            description='Moderation',
            postal_address='',
            city='',
            state='',
            country=self.country,
            status='M',
            institution_type=self.institution_type)
        ct2 = ClinicalTrial.objects.create(user=self.user2)
        sponsor = Sponsor.objects.create(
            sponsor_type='P', institution=institution_moderation,
            clinical_trial=ct2)
        utils.set_state(ct2, State.objects.get(pk=State.STATE_AGUARDANDO_MODERACAO))
        self.assertEqual(self.user4.get_pendencies(), 1)
        sponsor.delete()
        institution_moderation.delete()
        ct2.delete()
        ct_group_pending = CTGroup.objects.create(
            name='pending',
            description='pending',
            status=1,
            status_institution=CTGroup.STATUS_INSTITUTION_PENDING,
            visibility=1)
        self.assertEqual(self.user4.get_pendencies(), 1)
        ct_group_pending.delete()


class UserAdministratorGroupTestCase(TestCase):
    """Country class test"""
    def setUp(self):
        """Register Country and add user with permission"""
        self.factory = RequestFactory()
        self.user1 = User.objects.create(username=u'e',
                                        email=u'e@e.com',
                                        gender=1,
                                        race=1,
                                        country_id=u'BR')
        self.user1.groups.add(
            Group.objects.get(
                name=u'registrant')
            )

        self.ct_group = CTGroup.objects.create(
            name='teste123',
            description='teste123',
            status=1,
            visibility=1)

        self.ct_group_user_1 = CTGroupUser.objects.create(
            status=2,
            type_user='a',
            user=self.user1,
            ct_group=self.ct_group)
        
    def tearDown(self):
        """Delete objects"""
        self.ct_group_user_1.delete()
        self.ct_group.delete()
        self.user1.delete()

    def get_administrator_group(self):
        self.assertEqual(self.ct_group.get_administrator_group().count(), 1)
