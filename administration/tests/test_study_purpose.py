# -*- coding: utf-8 -*-

from django.test import TestCase
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from administration.views import *
from django.test.client import RequestFactory
from django.core.urlresolvers import reverse_lazy
from django.core.exceptions import PermissionDenied
from django.utils.translation import ugettext as _
from mock import Mock, MagicMock
from django.contrib import messages

User = get_user_model()


def create_user(groups=[]):
    """Create a user and add to a group parameter.
    """
    user = User.objects.create_user(
        'user', 'user@email.com',
        'pass', gender=1, race=1, country_id=u'BR')
    user.save()
    for group in groups:
        g = Group.objects.get(name=group)
        g.user_set.add(user)
    return user

class StudyPurposeTesteCase(TestCase):
    """Study Purpose class test."""
    def setUp(self):
        """Register Study Porpose."""
        self.study = StudyPurpose.objects.create(description='teste')
        self.factory = RequestFactory()
        self.user = create_user(['administrador'])
        self.original_success = messages.success
        messages.success = Mock()
        self.original_info = messages.info
        messages.info = Mock()
        self.original_error = messages.error
        messages.error = Mock()


    def tearDown(self):
        """Delete Study Purpose and user."""
        self.user.delete()
        self.study.delete()
        messages.success = self.original_success
        messages.info = self.original_info
        messages.error = self.original_error

    def test_study_purpose_create(self):
        """Test Study Purpose create view."""
        post_data = {"description": "teste111"}
        request_post = self.factory.post(
            reverse_lazy('study_purpose_add'), data=post_data)
        request_post.user = self.user
        response_post = StudyPurposeCreate.as_view()(request_post)
        self.assertEqual(response_post.status_code, 302)
        self.assertEqual(StudyPurpose.objects.all().count(), 2)
       
    def test_study_purpose_update(self):
        """Test Study Purpose update view."""
        alter_post = {"description": "ab"}
        request_post = self.factory.post(
            reverse_lazy('study_purpose_edit', args=(self.study.pk,)), data=alter_post)
        request_post.user = self.user
        response_post = StudyPurposeUpdate.as_view()(request_post, pk=self.study.pk)
        self.assertEqual(response_post.status_code, 302)
        study = StudyPurpose.objects.get(pk=self.study.pk)
        self.assertEqual(study.description, alter_post['description'])

    def test_study_purpose_list(self):
        """Test Study Purpose list view."""
        request = self.factory.get(reverse_lazy('study_purpose_list'))
        request.user = self.user
        response = StudyPurposeList.as_view()(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.context_data[u'studypurpose_list'].count(),
            1)

    def test_study_purpose_delete(self):
        """Test Study Purpose delete view."""
        study = StudyPurpose.objects.create(description='tata')
        request = self.factory.post(
            reverse_lazy('study_purpose_delete', args=(study.pk,)), {})
        request.user = self.user
        response = StudyPurposeDelete.as_view()(request, pk=study.pk)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(StudyPurpose.objects.all().count(), 1)

    def test_study_purpose_list_permission(self):
        """Verify study code list permission"""
        request = self.factory.get(reverse_lazy('study_purpose_list'))
        request.user = self.user
        response = StudyPurposeList.as_view()(request)
        self.assertEqual(response.status_code, 200)
        self.user.delete()
        self.user = create_user()
        request = self.factory.get(reverse_lazy('study_purpose_list'))
        request.user = self.user
        self.assertRaises(PermissionDenied, StudyPurposeList.as_view(), request)
        self.user.delete()
        self.user = create_user(['administrador'])

    def test_study_detail_permission(self):
        """Verify StudyPurpose detail permission"""
        request = self.factory.get(
            reverse_lazy('study_purpose_detail', args=(self.study.pk,)))
        request.user = self.user
        response = StudyPurposeDetail.as_view()(request, pk=self.study.pk)
        self.assertEqual(response.status_code, 200)
        self.user.delete()
        self.user = create_user()
        request = self.factory.get(
            reverse_lazy('study_purpose_detail', args=(self.study.pk,)))
        request.user = self.user
        self.assertRaises(
            PermissionDenied, StudyPurposeDetail.as_view(),
            request, args=(self.study.pk,))
        self.user.delete()
        self.user = create_user(['administrador'])

    def test_study_create_permission(self):
        """Verify StudyPurpose create permission"""
        request = self.factory.post(reverse_lazy('study_purpose_add'))
        request.user = self.user
        response = StudyPurposeCreate.as_view()(request)
        self.assertEqual(response.status_code, 200)
        self.user.delete()
        self.user = create_user()
        request = self.factory.get(reverse_lazy('study_purpose_add'))
        request.user = self.user
        self.assertRaises(
            PermissionDenied, StudyPurposeCreate.as_view(), request)
        self.user.delete()
        self.user = create_user(['administrador'])

    def test_study_update_permission(self):
        """Verify StudyPurpose update permission"""
        request = self.factory.get(
            reverse_lazy('study_purpose_edit', args=(self.study.pk,)))
        request.user = self.user
        response = StudyPurposeUpdate.as_view()(request, pk=self.study.pk)
        self.assertEqual(response.status_code, 200)
        self.user.delete()
        self.user = create_user()
        request = self.factory.get(
            reverse_lazy('study_purpose_edit', args=(self.study.pk,)))
        request.user = self.user
        self.assertRaises(
            PermissionDenied, StudyPurposeUpdate.as_view(),
            request, args=(self.study.pk,))
        self.user.delete()
        self.user = create_user(['administrador'])

    def test_study_delete_permission(self):
        """Verify StudyPurpose delete permission"""
        request = self.factory.get(
            reverse_lazy('study_purpose_delete', args=(self.study.pk,)))
        request.user = self.user
        response = StudyPurposeDelete.as_view()(request, pk=self.study.pk)
        self.assertEqual(response.status_code, 200)

        request = self.factory.post(
            reverse_lazy('study_purpose_delete', args=(self.study.pk,)), {})
        request.user = self.user
        response = StudyPurposeDelete.as_view()(request, pk=self.study.pk)
        self.assertEqual(response.status_code, 302)

        self.user.delete()
        self.user = create_user()
        request = self.factory.get(
            reverse_lazy('study_purpose_delete', args=(self.study.pk,)))
        request.user = self.user
        self.assertRaises(
            PermissionDenied, StudyPurposeDelete.as_view(),
            request, args=(self.study.pk,))

        request = self.factory.post(
            reverse_lazy('study_purpose_delete', args=(self.study.pk,)), {})
        request.user = self.user
        self.assertRaises(
            PermissionDenied, StudyPurposeDelete.as_view(),
            request, args=(self.study.pk,))

        self.user.delete()
        self.user = create_user(['administrador'])
