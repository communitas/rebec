# -*- coding: utf-8 -*-

from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import ValidationError
from django.core.urlresolvers import reverse
from django.conf import settings
from multiselectfield import MultiSelectField

class Language(models.Model):
    code = models.CharField(max_length=20)
    description = models.CharField(max_length=100)

    class Meta:
        verbose_name = _(u'Language')
        verbose_name_plural = _(u'Languages')

    def __unicode__(self):
        return self.description

class Country(models.Model):
    """Defines the model for country"""
    description = models.CharField(
        max_length=100,
        verbose_name=_('Description'))

    #description_ptbr = models.CharField(
    #    max_length=100,
    #    verbose_name=_('Brazilian Portuguese Name'))

    code = models.CharField(
        max_length=2,
        verbose_name=_('Code'),
        primary_key=True)

    class Meta:
        verbose_name = _('Country')
        verbose_name_plural = _('Countries')
        ordering = [u'description']

    def __unicode__(self):
        return self.description

    def get_all_fields(self):
        """Returns a list of all field names on the instance."""
        fields = dict((field.verbose_name, self._get_FIELD_display(field)) for field in self._meta.fields if field.name != 'id')
        return fields

    def get_model_name(self):
        """Returns a model name."""
        return self._meta.verbose_name.title()


class InstitutionType(models.Model):
    """Defines the model for institution type"""
    description = models.CharField(
        max_length=200,
        verbose_name=_('Description'))

    description_english = models.CharField(
        max_length=200,
        verbose_name=_('English Description'))

    class Meta:
        verbose_name = _('Institution Type')
        verbose_name_plural = _('Institution Types')

    def __unicode__(self):
        return u'%s / %s' % (self.description, self.description_english)

    def get_all_fields(self):
        """Returns a list of all field names on the instance."""
        fields = dict((field.verbose_name, self._get_FIELD_display(field)) for field in self._meta.fields if field.name != 'id')
        return fields

    def get_model_name(self):
        """Returns a model name."""
        return self._meta.verbose_name.title()


class Institution(models.Model):
    """Defines the model for institution"""
    STATUS_MODERATION = u'M'
    STATUS_ACTIVE = u'A'
    STATUS_INACTIVE = u'I'
    STATUS_CHOICES = (
        (STATUS_MODERATION, _('Moderation')),
        (STATUS_ACTIVE, _('Active')),
        (STATUS_INACTIVE, _('Inactive')),
    )

    PESSOA_FISICA = u'PF'
    PESSOA_JURIDICA = u'PJ'
    TYPE_PERSON_CHOICES = (
        (PESSOA_FISICA, _('Private Individual')),
        (PESSOA_JURIDICA, _('Legal Entity')),
    )

    name = models.CharField(
        max_length=300,
        blank=False,
        verbose_name=_('Name'))
    description = models.CharField(
        max_length=1000,
        blank=False,
        verbose_name=_('Description'))
    postal_address = models.CharField(
        max_length=1000,
        blank=True,
        verbose_name=_('Postal Address'))
    zip_code =  models.CharField(
        _('Zip Code'),
        blank=True,
        null=True,
        max_length=18,
        help_text=_('The Zip Code Ex:99999-999'))
    city = models.CharField(
        max_length=100,
        blank=True,
        verbose_name=_('City'))
    state = models.CharField(
        max_length=50,
        blank=True,
        verbose_name=_('State'))
    country = models.ForeignKey(
        Country,
        verbose_name=_('Country'),
        on_delete=models.PROTECT)
    status = models.CharField(
        max_length=1,
        choices=STATUS_CHOICES,
        verbose_name=_(u'Status'),
        default=u'M')
    institution_type = models.ForeignKey(
        InstitutionType,
        verbose_name=_('Institution Type'),
        on_delete=models.PROTECT)
    temp_workflow_observation = models.CharField(
        null=True,
        blank=True,
        max_length=500
    )
    type_person = models.CharField(
        max_length=2,
        choices=TYPE_PERSON_CHOICES,
        verbose_name=_(u'Type Person'),
        default=u'PF')
    cpf =  models.CharField(
        _('CPF'),
        blank=True,
        null=True,
        max_length=14,
        help_text=_('The cpf has 11 digits.'))
    cnpj =  models.CharField(
        _('CNPJ'),
        blank=True,
        null=True,
        max_length=18,
        help_text=_('The cnpj has 14 digits.'))

    class Meta:
        verbose_name = _('Institution')
        verbose_name_plural = _('Institutions')
        ordering = [u'name']

    def __unicode__(self):
        return self.name

    def get_all_fields(self):
        """Returns a list of all field names on the instance."""
        fields = dict((field.verbose_name, self._get_FIELD_display(field)) for field in self._meta.fields if field.name != 'id')
        return fields

    def get_model_name(self):
        """Returns a model name."""
        return self._meta.verbose_name.title()


class Setup(models.Model):
    """Defines the model for setup"""
    language_1_code = models.ForeignKey(
        Language,
        null=True,
        blank=True,
        verbose_name=_('Code - language 1'),
        related_name=u'setup_set_lang1')
    language_1_mandatory = models.BooleanField(
        default=False,
        blank=True,
        verbose_name=_('Mandatory - language 1'))
    language_2_code = models.ForeignKey(
        Language,
        null=True,
        blank=True,
        verbose_name=_('Code - language 2'),
        related_name=u'setup_set_lang2')
    language_2_mandatory = models.BooleanField(
        default=False,
        blank=True,
        verbose_name=_('Mandatory - language 2'))
    language_3_code = models.ForeignKey(
        Language,
        null=True,
        blank=True,
        verbose_name=_('Code - language 3'),
        related_name=u'setup_set_lang3')
    language_3_mandatory = models.BooleanField(
        default=False,
        blank=True,
        verbose_name=_('Mandatory - language 3'))
    terms_of_use = models.TextField(
        null=True,
        blank=True,
        verbose_name=_(u"Terms of Use Language 1"))

    terms_of_use_lang2 = models.TextField(
        null=True,
        blank=True,
        verbose_name=_(u"Terms of Use Language 2"))

    terms_of_use_lang3 = models.TextField(
        null=True,
        blank=True,
        verbose_name=_(u"Terms of Use Language 3"))

    class Meta:
        verbose_name = _('Setup')
        verbose_name_plural = _('Setups')


class SetupIdentifier(models.Model):
    """
    Defines a identifier that can have an associated
    value to the clinical trial
    """
    name = models.CharField(
        max_length=100,
        verbose_name=_('Identifier Name'))
    issuing_body = models.CharField(
        max_length=100,
        verbose_name=_('Issuing Body'))
    mandatory = models.BooleanField(
        default=False,
        verbose_name=_('Mandatory'))
    mask = models.CharField(
        max_length=50,
        null=True,
        blank=True,
        verbose_name=_('Mask'),
        help_text=_(
            u'''Value mask for the codes. Use any character,
                or the replaces "N" for number, "A" for alphanumeric and "L" for Letter.'''
            u''' Example: NN.A/L is two number, a slash and a letter.'''))
    help_text_lang_1 = \
        models.TextField(max_length=1000,
                         verbose_name=_(u'Help Text for identifier field'),
                         null=True,
                         blank=True)
    help_text_lang_2 = \
        models.TextField(max_length=1000, 
                        verbose_name=_(u'Help Text for identifier field'),
                        null=True,
                        blank=True)
    help_text_lang_3 = \
        models.TextField(max_length=1000,
                         verbose_name=_(u'Help Text for identifier field'),
                         null=True,
                         blank=True)

    help_text_code_lang_1 = \
        models.TextField(max_length=1000,
                         verbose_name=_(u'Help Text for code field'),
                         null=True,
                         blank=True)
    help_text_code_lang_2 = \
        models.TextField(max_length=1000,
                         verbose_name=_(u'Help Text for code field'),
                         null=True,
                         blank=True)
    help_text_code_lang_3 = \
        models.TextField(max_length=1000,
                         verbose_name=_(u'Help Text for code field'),
                         null=True,
                         blank=True)
    help_text_description_lang_1 = \
        models.TextField(max_length=1000,
                         verbose_name=_(u'Help Text for description field'),
                         null=True,
                         blank=True)
    help_text_description_lang_2 = \
        models.TextField(max_length=1000,
                         verbose_name=_(u'Help Text for description field'),
                         null=True,
                         blank=True)
    help_text_description_lang_3 = \
        models.TextField(max_length=1000,
                         verbose_name=_(u'Help Text for description field'),
                         null=True,
                         blank=True)
    help_text_date_lang_1 = \
        models.TextField(max_length=1000,
                         verbose_name=_(u'Help Text for date field'),
                         null=True,
                         blank=True)
    help_text_date_lang_2 = \
        models.TextField(max_length=1000,
                         verbose_name=_(u'Help Text for date field'),
                         null=True,
                         blank=True)
    help_text_date_lang_3 = \
        models.TextField(max_length=1000,
                         verbose_name=_(u'Help Text for date field'),
                         null=True,
                         blank=True)
    appear_in_identifications_step = \
        models.BooleanField(default=True,
                            verbose_name=_(u'Appear in Identification Step'),
                            help_text=_(u'Check this field if this identifier '
                                        u'must be an option in the '
                                        u'clinical trial\'s identification step'
                                       )
                           )

    class Meta:
        verbose_name = _('Setup Identifier')
        verbose_name_plural = _('Setup Identifiers')

    def __unicode__(self):
        return u'%s - %s' %(self.name, self.issuing_body)

    def get_all_fields(self):
        """Returns a list of all field names on the instance."""
        fields = dict((field.verbose_name, self._get_FIELD_display(field)) for field in self._meta.fields if field.name != 'id')
        return fields

    def get_model_name(self):
        """Returns a model name."""
        return self._meta.verbose_name.title()


class CTGroup(models.Model):

    STATUS_INSTITUTION_APPROVED = 1
    STATUS_INSTITUTION_CANCEL = 2
    STATUS_INSTITUTION_PENDING = 3

    STATUS_INSTITUTION_CHOICES = (
        (STATUS_INSTITUTION_APPROVED, _('Approved')),
        (STATUS_INSTITUTION_CANCEL, _('Cancel')),
        (STATUS_INSTITUTION_PENDING, _('Pending')),
    )

    STATUS_ACTIVE = 1
    STATUS_INACTIVE = 2

    STATUS_CHOICES = (
        (STATUS_ACTIVE, _('Active')),
        (STATUS_INACTIVE, _('Inactive')),
    )

    name = models.CharField(
        max_length=50,
        verbose_name=_('Name'),
        help_text=_('Choose a meaningful name for the group'))

    description = models.CharField(
        max_length=100,
        verbose_name=_('Description'))

    status_institution = models.IntegerField(
        verbose_name=_('Status institution'),
        choices=STATUS_INSTITUTION_CHOICES,
        blank=True,
        null=True)

    institution = models.ForeignKey(
        Institution,
        verbose_name=_('Institution'),
        related_name="ctgroups",
        on_delete=models.PROTECT,
        blank=True,
        null=True,
        help_text=_('Enter an institution if this group will be managed on a corporate basis.'))

    status = models.IntegerField(
        verbose_name=_('Status'),
        choices=STATUS_CHOICES,
        default=1)


    class Meta:
        verbose_name = _('Clinical Trial Group')
        verbose_name_plural = _('Clinical Trials Groups')

    def __unicode__(self):
        return unicode(self.name)

    def get_all_fields(self):
        """Returns a list of all field names on the instance."""
        fields = dict((field.verbose_name, self._get_FIELD_display(field)) for field in self._meta.fields if field.name != 'id')
        return fields

    def get_model_name(self):
        """Returns a model name."""
        return self._meta.verbose_name.title()

    def get_administrator_group(self):
        user_adm = self.users.all().filter(type_user="a")
        return user_adm


class CTGroupUser(models.Model):

    STATUS_PENDING_APROV = 1
    STATUS_ACTIVE = 2
    STATUS_INACTIVE = 3
    STATUS_PENDING_EXCLUSION = 4
    STATUS_PENDING_APROV_ADM = 5


    STATUS_CHOICES = (
        (STATUS_PENDING_APROV, _('Pending Aprove')),
        (STATUS_ACTIVE, _('Active')),
        (STATUS_INACTIVE, _('Inactive')),
        (STATUS_PENDING_EXCLUSION, _('Pending Exclusion')),
        (STATUS_PENDING_APROV_ADM, _('Pending Aprove Adm'))
    )


    TYPE_ADMINISTRATOR = u'a'
    TYPE_REGISTRANT = u'r'

    TYPE_CHOICHES = (
        (TYPE_ADMINISTRATOR, _('Administrator')),
        (TYPE_REGISTRANT, _('Registrant')),
    )

    status = models.IntegerField(
        verbose_name=_('Status'),
        choices=STATUS_CHOICES,
        null=True,
        blank=True)
    type_user = models.CharField(
        max_length=1,
        verbose_name=_('Group Type'),
        choices=TYPE_CHOICHES)
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name=_('User'),
        related_name="ctgroups",
        on_delete=models.PROTECT)
    ct_group = models.ForeignKey(
        CTGroup,
        verbose_name=_('CT Group'),
        related_name="users",
        on_delete=models.PROTECT)

    class Meta:
        verbose_name = _('Clinical Trials Group User')
        verbose_name_plural = _('Clinical Trials Groups Users')
        unique_together = (u'user', u'ct_group')

    def __unicode__(self):
        return unicode(self.ct_group)

    def get_all_fields(self):
        """Returns a list of all field names on the instance."""
        fields = dict((field.verbose_name, self._get_FIELD_display(field)) for field in self._meta.fields if field.name != 'id')
        return fields

    def get_model_name(self):
        """Returns a model name."""
        return self._meta.verbose_name.title()


class CTGroupLog(models.Model):
    """
    Logs of CTGroup lifecycle
    """
    ctgroup = models.ForeignKey(CTGroup,
                               verbose_name=_(u'Clinical Trial Group'))
    date = models.DateTimeField(auto_now_add=True,
                                verbose_name=_(u'Date of Event'))
    message = models.TextField(max_length=1000,
                               verbose_name=_(u'Message of Log'))
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             verbose_name=_(u'User'))

    class Meta:
        verbose_name = _(u'Log of Clinical Trial Group')
        verbose_name = _(u'Logs of Clinical Trials Groups')
        ordering = (u'date',)


class StudyPurpose(models.Model):
    """Defines the purpose for the clinical trial"""

    description = models.CharField(
        max_length=100,
        verbose_name=_('Description'))

    class Meta:
        verbose_name = _('Study Purpose')
        verbose_name_plural = _('Study Purposes')

    def __unicode__(self):
        return self.description

    def get_model_name(self):
        """Returns a model name."""
        return self._meta.verbose_name.title()

    def get_all_fields(self):
        """Returns a list of all field names on the instance."""
        fields = dict((field.verbose_name, self._get_FIELD_display(field)) for field in self._meta.fields if field.name != 'id')
        return fields

class InterventionAssignment(models.Model):
    """Defines the intervention assignment"""

    description = models.CharField(
        max_length=100,
        verbose_name=_('Description'))

    class Meta:
        verbose_name = _('Intervention Assignment')
        verbose_name_plural = _('Intervention Assignments')

    def __unicode__(self):
        return self.description

    def get_model_name(self):
        """Returns a model name."""
        return self._meta.verbose_name.title()

    def get_all_fields(self):
        """Returns a list of all field names on the instance."""
        fields = dict((field.verbose_name, self._get_FIELD_display(field)) for field in self._meta.fields if field.name != 'id')
        return fields

class MaskingType(models.Model):
    """Defines the masking type"""

    description = models.CharField(
        max_length=100,
        verbose_name=_('Description'))

    class Meta:
        verbose_name = _('Masking Type')
        verbose_name_plural = _('Masking Types')

    def __unicode__(self):
        return self.description

    def get_model_name(self):
        """Returns a model name."""
        return self._meta.verbose_name.title()

    def get_all_fields(self):
        """Returns a list of all field names on the instance."""
        fields = dict((field.verbose_name, self._get_FIELD_display(field)) for field in self._meta.fields if field.name != 'id')
        return fields


class AllocationType(models.Model):
    """Defines the allocation type"""

    description = models.CharField(
        max_length=100,
        verbose_name=_('Description'))

    class Meta:
        verbose_name = _('Allocation Type')
        verbose_name_plural = _('Allocation Types')

    def __unicode__(self):
        return self.description

    def get_model_name(self):
        """Returns a model name."""
        return self._meta.verbose_name.title()

    def get_all_fields(self):
        """Returns a list of all field names on the instance."""
        fields = dict((field.verbose_name, self._get_FIELD_display(field)) for field in self._meta.fields if field.name != 'id')
        return fields


class StudyPhase(models.Model):
    """Defines the study phase"""

    description = models.CharField(
        max_length=100,
        verbose_name=_('Description'))

    class Meta:
        verbose_name = _('Study Phase')
        verbose_name_plural = _('Study Phases')

    def __unicode__(self):
        return self.description

    def get_model_name(self):
        """Returns a model name."""
        return self._meta.verbose_name.title()

    def get_all_fields(self):
        """Returns a list of all field names on the instance."""
        fields = dict((field.verbose_name, self._get_FIELD_display(field)) for field in self._meta.fields if field.name != 'id')
        return fields


class ObservationStudyDesign(models.Model):
    """Defines the observation degign"""

    description = models.CharField(
        max_length=100,
        verbose_name=_('Description'))

    class Meta:
        verbose_name = _('Observation Study Design')
        verbose_name_plural = _('Observation Study Designs')

    def __unicode__(self):
        return self.description

    def get_model_name(self):
        """Returns a model name."""
        return self._meta.verbose_name.title()

    def get_all_fields(self):
        """Returns a list of all field names on the instance."""
        fields = dict((field.verbose_name, self._get_FIELD_display(field)) for field in self._meta.fields if field.name != 'id')
        return fields


class TimePerspective(models.Model):
    """Defines the time perspective"""

    description = models.CharField(
        max_length=100,
        verbose_name=_('Description'))

    class Meta:
        verbose_name = _('Time Perspective')
        verbose_name_plural = _('Time Perspectives')

    def __unicode__(self):
        return self.description

    def get_model_name(self):
        """Returns a model name."""
        return self._meta.verbose_name.title()

    def get_all_fields(self):
        """Returns a list of all field names on the instance."""
        fields = dict((field.verbose_name, self._get_FIELD_display(field)) for field in self._meta.fields if field.name != 'id')
        return fields

class InterventionCode(models.Model):
    """Defines the intervention type for a clinical trial."""

    description = models.CharField(
        max_length=100,
        verbose_name=_('Description'))

    class Meta:
        verbose_name = _('Intervention Code')
        verbose_name_plural = _('Intervention Codes')

    def __unicode__(self):
        return self.description

    def get_model_name(self):
        """Returns a model name."""
        return self._meta.verbose_name.title()

    def get_all_fields(self):
        """Returns a list of all field names on the instance."""
        fields = dict((field.verbose_name, self._get_FIELD_display(field)) for field in self._meta.fields if field.name != 'id')
        return fields


class Attachment(models.Model):
    """Defines an attachment file or link"""
    TYPE_CHOICES = (
        ('P', _('PDF File')),
        ('J', _('JPG File')),
        ('G', _('GIF File')),
        ('N', _('PNG File')),
        ('B', _('BMP File')),
        ('L', _('Link'))
    )

    setup = models.ForeignKey(
        'Setup',
        verbose_name=_('Setup'),
        default=1)

    name = models.CharField(
        max_length=100,
        verbose_name=_('Name'))

    attachment_type = MultiSelectField(
        choices=TYPE_CHOICES,
        verbose_name=_('Attachment Type'))

    required = models.BooleanField(
        default=False
    )

    max_file_size = models.DecimalField(
            decimal_places=2,
            max_digits=10,
            verbose_name=_('Max file size in MB(zero is unlimited)'),
            default=0,
            blank=True,
            null=True,
    )

    class Meta:
        verbose_name = _('Attachment')
        verbose_name_plural = _('Attachments')

    def __unicode__(self):
        return self.name

    def get_model_name(self):
        """Returns a model name."""
        return self._meta.verbose_name.title()

    def get_all_fields(self):
        """Returns a list of all field names on the instance."""
        fields = dict((field.verbose_name, self._get_FIELD_display(field)) for field in self._meta.fields if field.name != 'id')
        return fields

    def has_clinical_trials(self):
        """
        Verify if this attachment type has al least one
        clinical trial using it.
        """
        return self.ctattachment_set.exists()

class StateDaysLimit(models.Model):
    """Defines the model for Days Limit"""

    red = models.IntegerField(
        max_length=100,
        verbose_name=_('Red'))

    yellow = models.IntegerField(
        max_length=100,
        verbose_name=_('Yellow'))

    green = models.IntegerField(
        max_length=100,
        verbose_name=_('Green'))

    setup = models.ForeignKey(
        'Setup',
        verbose_name=_('Setup'),
        default=1)

    state = models.ForeignKey(
        'workflows.State',
        verbose_name=_('State'),
        unique=True)
    
    class Meta:
        verbose_name = _('State Days Limit')
        verbose_name_plural = _('State Days Limit')

    def __unicode__(self):
        return unicode(self.id)

    def get_all_fields(self):
        """Returns a list of all field names on the instance."""
        fields = dict((field.verbose_name, self._get_FIELD_display(field)) for field in self._meta.fields if field.name != 'id')
        return fields

    def get_model_name(self):
        """Returns a model name."""
        return self._meta.verbose_name.title()


class Help(models.Model):
    """Help dinamic"""

    TYPE_ADMINISTRADOR = u'administrador'
    TYPE_REGISTRANT = u'registrant'
    TYPE_REGISTRANT_ADMIN = u'register_admin'
    TYPE_REVISER = u'reviser'
    TYPE_EVALUATOR = u'evaluator'
    TYPE_MANAGER = u'manager'
    TYPE_EDITOR = u'editor'

    TYPE_USER_CHOICHES = (
        (TYPE_ADMINISTRADOR, _('Administrador')),
        (TYPE_REGISTRANT, _('Registrant')),
        (TYPE_REGISTRANT_ADMIN, _('Registrant Administrador')),
        (TYPE_REVISER, _('Reviser')),
        (TYPE_EVALUATOR, _('Evaluator')),
        (TYPE_MANAGER, _('Manager')),
        (TYPE_EDITOR, _('Editor')),        
    )


    url_name = models.CharField(
        max_length=100,
        verbose_name=_('Url Name'),
        primary_key=True)

    name = models.CharField(
        max_length=100,
        verbose_name=_('Name'))

    type_user = MultiSelectField(
        verbose_name=_('Type User'),
        choices=TYPE_USER_CHOICHES,
        null=True,
        blank=True)


    class Meta:
        verbose_name = _('Help')
        verbose_name_plural = _('Help')

    def __unicode__(self):
        return unicode(self.url_name)

    def get_all_fields(self):
        """Returns a list of all field names on the instance."""
        fields = dict((field.verbose_name, self._get_FIELD_display(field)) for field in self._meta.fields if field.name != 'id')
        return fields

    def get_model_name(self):
        """Returns a model name."""
        return self._meta.verbose_name.title()


class HelpLanguage(models.Model):
    """Help dinamic"""

    help = models.ForeignKey('Help',
        related_name="helps",
        verbose_name=_('Help'))

    language = models.ForeignKey('Language',
        related_name="language",
        verbose_name=_('Language'))

    titulo = models.CharField(max_length=100,
        verbose_name=_('Title'))


    class Meta:
        verbose_name = _('Help Language')
        verbose_name_plural = _('Help Languages')
        unique_together = (u'help', u'language', u'id')

    def __unicode__(self):
        return u'%s - %s' %(self.help, self.language)

    def get_all_fields(self):
        """Returns a list of all field names on the instance."""
        fields = dict((field.verbose_name, self._get_FIELD_display(field)) for field in self._meta.fields if field.name != 'id')
        return fields

    def get_model_name(self):
        """Returns a model name."""
        return self._meta.verbose_name.title()


class ItemsHelps(models.Model):
    """Help dinamic"""

    PLACEMENT_BOTTOM = u'bottom'
    PLACEMENT_TOP = u'top'
    PLACEMENT_LEFT = u'left'
    PLACEMENT_RIGHT = u'right'


    PLACEMENT_CHOICHES = (
        (PLACEMENT_BOTTOM, _('Bottom')),
        (PLACEMENT_TOP, _('Top')),
        (PLACEMENT_LEFT, _('Left')),
        (PLACEMENT_RIGHT, _('right')),
    )


    element = models.CharField(max_length=100,
        verbose_name=_('Element'))

    title = models.CharField(max_length=100,
        verbose_name=_('Title Help'))

    content = models.TextField(
        verbose_name=_(u'Content'))

    placement = models.CharField(
        max_length=6,
        verbose_name=_('Placement'),
        default = 'r',
        choices=PLACEMENT_CHOICHES)

    backdrop = models.BooleanField(
        default=False,
        verbose_name=_('Backdrop'),
    )

    backdropPadding = models.IntegerField(
        max_length=100,
        verbose_name=_('Backdrop Padding'))

    animation = models.BooleanField(
        default=False,
        verbose_name=_('Animation'),
    )

    next = models.IntegerField(
        max_length=100,
        verbose_name=_('Next'))

    prev = models.IntegerField(
        max_length=100,
        verbose_name=_('Prev'))

    onShow = models.CharField(
        max_length=100,
        verbose_name=_('onShow'),
        null=True,
        blank=True)

    onShown = models.CharField(
        max_length=100,
        verbose_name=_('onShown'),
        null=True,
        blank=True)

    onHide = models.CharField(
        max_length=100,
        verbose_name=_('onHide'),
        null=True,
        blank=True)

    onHidden = models.CharField(
        max_length=100,
        verbose_name=_('onHidden'),
        null=True,
        blank=True)

    onNext = models.CharField(
        max_length=100,
        verbose_name=_('onNext'),
        null=True,
        blank=True)

    onPrev = models.CharField(
        max_length=100,
        verbose_name=_('onPrev'),
        null=True,
        blank=True)

    path = models.CharField(
        max_length=100,
        verbose_name=_('Path'),
        null=True,
        blank=True)

    help_language = models.ForeignKey('HelpLanguage',
        related_name="helps_language",
        verbose_name=_('Help Language'))

    class Meta:
        verbose_name = _('Item Help')
        verbose_name_plural = _('Items Helps')
        unique_together = (u'help_language', u'id')

    def __unicode__(self):
        return u'%s - %s' %(self.id, self.help_language)

    def get_all_fields(self):
        """Returns a list of all field names on the instance."""
        fields = dict((field.verbose_name, self._get_FIELD_display(field)) for field in self._meta.fields if field.name != 'id')
        return fields

    def get_model_name(self):
        """Returns a model name."""
        return self._meta.verbose_name.title()


class StateChangeEmailTemplate(models.Model):
    """
    Email templates for clinical trials workflow state changes
    """
    state_from = models.ForeignKey(
                     u'workflows.State',
                     verbose_name=_(u'State from'),
                     related_name=u'emailtemplate_from'
                 )
    state_to = models.ForeignKey(
                   u'workflows.State',
                   verbose_name=_(u'State to'),
                   related_name=u'emailtemplate_to'
               )
    email_text = models.TextField(
                     help_text=_(u'You can use #TRIAL_LINK# for the '
                                 u'trial detail absolute link, and #TRIAL_ID# for the trial'
                                 u' id replacement.')
                 )

    def __unicode__(self, *args, **kwargs):
        return u'Template %s > %s' % (self.state_from, self.state_to)

    class Meta:
        verbose_name = _(u'Email template for state change')
        verbose_name_plural = _(u'Email templates for state changes')
        unique_together = (u'state_from', u'state_to')
