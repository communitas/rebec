from django.template.response import TemplateResponse

class RebecResponse(TemplateResponse):

    @property
    def rendered_content(self):
        template = self.resolve_template(self.template_name)
        context = self.resolve_context(self.context_data)
        if context.get('request').is_ajax():
            content = template.nodelist[0].blocks[u'administration_content'].render(context)
        else:
            content = template.render(context)
        return content