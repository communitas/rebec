# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Language'
        db.create_table(u'administration_language', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('code', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal(u'administration', ['Language'])

        # Adding model 'Country'
        db.create_table(u'administration_country', (
            ('description', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('code', self.gf('django.db.models.fields.CharField')(max_length=2, primary_key=True)),
        ))
        db.send_create_signal(u'administration', ['Country'])

        # Adding model 'InstitutionType'
        db.create_table(u'administration_institutiontype', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=200)),
        ))
        db.send_create_signal(u'administration', ['InstitutionType'])

        # Adding model 'Institution'
        db.create_table(u'administration_institution', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('postal_address', self.gf('django.db.models.fields.CharField')(max_length=300, blank=True)),
            ('zip_code', self.gf('django.db.models.fields.CharField')(max_length=18, null=True, blank=True)),
            ('city', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('state', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('country', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['administration.Country'], on_delete=models.PROTECT)),
            ('status', self.gf('django.db.models.fields.CharField')(default=u'M', max_length=1)),
            ('institution_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['administration.InstitutionType'], on_delete=models.PROTECT)),
            ('temp_workflow_observation', self.gf('django.db.models.fields.CharField')(max_length=500, null=True, blank=True)),
            ('type_person', self.gf('django.db.models.fields.CharField')(default=u'PF', max_length=2)),
            ('cpf', self.gf('django.db.models.fields.CharField')(max_length=14, null=True, blank=True)),
            ('cnpj', self.gf('django.db.models.fields.CharField')(max_length=18, null=True, blank=True)),
        ))
        db.send_create_signal(u'administration', ['Institution'])

        # Adding model 'Setup'
        db.create_table(u'administration_setup', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('language_1_code', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name=u'setup_set_lang1', null=True, to=orm['administration.Language'])),
            ('language_1_mandatory', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('language_2_code', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name=u'setup_set_lang2', null=True, to=orm['administration.Language'])),
            ('language_2_mandatory', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('language_3_code', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name=u'setup_set_lang3', null=True, to=orm['administration.Language'])),
            ('language_3_mandatory', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('terms_of_use', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('terms_of_use_lang2', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('terms_of_use_lang3', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'administration', ['Setup'])

        # Adding model 'SetupIdentifier'
        db.create_table(u'administration_setupidentifier', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('issuing_body', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('mandatory', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('mask', self.gf('django.db.models.fields.CharField')(max_length=50)),
        ))
        db.send_create_signal(u'administration', ['SetupIdentifier'])

        # Adding model 'Vocabulary'
        db.create_table(u'administration_vocabulary', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('version', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('current_version', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('usage', self.gf('multiselectfield.db.fields.MultiSelectField')(max_length=3)),
        ))
        db.send_create_signal(u'administration', ['Vocabulary'])

        # Adding unique constraint on 'Vocabulary', fields ['name', 'version']
        db.create_unique(u'administration_vocabulary', ['name', 'version'])

        # Adding model 'VocabularyItem'
        db.create_table(u'administration_vocabularyitem', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('code', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('description_lang_1', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('description_lang_2', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('description_lang_3', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('vocabulary', self.gf('django.db.models.fields.related.ForeignKey')(related_name='vocabulary_items', to=orm['administration.Vocabulary'])),
        ))
        db.send_create_signal(u'administration', ['VocabularyItem'])

        # Adding model 'CTGroup'
        db.create_table(u'administration_ctgroup', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('status_institution', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('institution', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='ctgroups', null=True, on_delete=models.PROTECT, to=orm['administration.Institution'])),
            ('status', self.gf('django.db.models.fields.IntegerField')(default=1)),
            ('visibility', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal(u'administration', ['CTGroup'])

        # Adding model 'CTGroupUser'
        db.create_table(u'administration_ctgroupuser', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('status', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('type_user', self.gf('django.db.models.fields.CharField')(max_length=1)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(related_name='ctgroups', on_delete=models.PROTECT, to=orm['login.RebecUser'])),
            ('ct_group', self.gf('django.db.models.fields.related.ForeignKey')(related_name='users', on_delete=models.PROTECT, to=orm['administration.CTGroup'])),
        ))
        db.send_create_signal(u'administration', ['CTGroupUser'])

        # Adding unique constraint on 'CTGroupUser', fields ['user', 'ct_group']
        db.create_unique(u'administration_ctgroupuser', ['user_id', 'ct_group_id'])

        # Adding model 'StudyPurpose'
        db.create_table(u'administration_studypurpose', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal(u'administration', ['StudyPurpose'])

        # Adding model 'InterventionAssignment'
        db.create_table(u'administration_interventionassignment', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal(u'administration', ['InterventionAssignment'])

        # Adding model 'MaskingType'
        db.create_table(u'administration_maskingtype', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal(u'administration', ['MaskingType'])

        # Adding model 'AllocationType'
        db.create_table(u'administration_allocationtype', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal(u'administration', ['AllocationType'])

        # Adding model 'StudyPhase'
        db.create_table(u'administration_studyphase', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal(u'administration', ['StudyPhase'])

        # Adding model 'ObservationStudyDesign'
        db.create_table(u'administration_observationstudydesign', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal(u'administration', ['ObservationStudyDesign'])

        # Adding model 'TimePerspective'
        db.create_table(u'administration_timeperspective', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal(u'administration', ['TimePerspective'])

        # Adding model 'InterventionCode'
        db.create_table(u'administration_interventioncode', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal(u'administration', ['InterventionCode'])

        # Adding model 'Attachment'
        db.create_table(u'administration_attachment', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('setup', self.gf('django.db.models.fields.related.ForeignKey')(default=1, to=orm['administration.Setup'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('attachment_type', self.gf('multiselectfield.db.fields.MultiSelectField')(max_length=11)),
            ('required', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('max_file_size', self.gf('django.db.models.fields.DecimalField')(default=0, null=True, max_digits=10, decimal_places=2, blank=True)),
        ))
        db.send_create_signal(u'administration', ['Attachment'])

        # Adding model 'StateDaysLimit'
        db.create_table(u'administration_statedayslimit', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('red', self.gf('django.db.models.fields.IntegerField')(max_length=100)),
            ('yellow', self.gf('django.db.models.fields.IntegerField')(max_length=100)),
            ('green', self.gf('django.db.models.fields.IntegerField')(max_length=100)),
            ('setup', self.gf('django.db.models.fields.related.ForeignKey')(default=1, to=orm['administration.Setup'])),
            ('state', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['workflows.State'], unique=True)),
        ))

        # Adding field 'RebecUser.country'
        db.add_column(u'login_rebecuser', 'country',
        self.gf('django.db.models.fields.related.ForeignKey')(default=0, to=orm['administration.Country']),
          keep_default=False)

        db.send_create_signal(u'administration', ['StateDaysLimit'])


    def backwards(self, orm):
        # Removing unique constraint on 'CTGroupUser', fields ['user', 'ct_group']
        db.delete_unique(u'administration_ctgroupuser', ['user_id', 'ct_group_id'])

        # Removing unique constraint on 'Vocabulary', fields ['name', 'version']
        db.delete_unique(u'administration_vocabulary', ['name', 'version'])

        # Deleting model 'Language'
        db.delete_table(u'administration_language')

        # Deleting model 'Country'
        db.delete_table(u'administration_country')

        # Deleting model 'InstitutionType'
        db.delete_table(u'administration_institutiontype')

        # Deleting model 'Institution'
        db.delete_table(u'administration_institution')

        # Deleting model 'Setup'
        db.delete_table(u'administration_setup')

        # Deleting model 'SetupIdentifier'
        db.delete_table(u'administration_setupidentifier')

        # Deleting model 'Vocabulary'
        db.delete_table(u'administration_vocabulary')

        # Deleting model 'VocabularyItem'
        db.delete_table(u'administration_vocabularyitem')

        # Deleting model 'CTGroup'
        db.delete_table(u'administration_ctgroup')

        # Deleting model 'CTGroupUser'
        db.delete_table(u'administration_ctgroupuser')

        # Deleting model 'StudyPurpose'
        db.delete_table(u'administration_studypurpose')

        # Deleting model 'InterventionAssignment'
        db.delete_table(u'administration_interventionassignment')

        # Deleting model 'MaskingType'
        db.delete_table(u'administration_maskingtype')

        # Deleting model 'AllocationType'
        db.delete_table(u'administration_allocationtype')

        # Deleting model 'StudyPhase'
        db.delete_table(u'administration_studyphase')

        # Deleting model 'ObservationStudyDesign'
        db.delete_table(u'administration_observationstudydesign')

        # Deleting model 'TimePerspective'
        db.delete_table(u'administration_timeperspective')

        # Deleting model 'InterventionCode'
        db.delete_table(u'administration_interventioncode')

        # Deleting model 'Attachment'
        db.delete_table(u'administration_attachment')

        # Deleting model 'StateDaysLimit'
        db.delete_table(u'administration_statedayslimit')


    models = {
        u'administration.allocationtype': {
            'Meta': {'object_name': 'AllocationType'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'administration.attachment': {
            'Meta': {'object_name': 'Attachment'},
            'attachment_type': ('multiselectfield.db.fields.MultiSelectField', [], {'max_length': '11'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max_file_size': ('django.db.models.fields.DecimalField', [], {'default': '0', 'null': 'True', 'max_digits': '10', 'decimal_places': '2', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'required': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'setup': ('django.db.models.fields.related.ForeignKey', [], {'default': '1', 'to': u"orm['administration.Setup']"})
        },
        u'administration.country': {
            'Meta': {'object_name': 'Country'},
            'code': ('django.db.models.fields.CharField', [], {'max_length': '2', 'primary_key': 'True'}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'administration.ctgroup': {
            'Meta': {'object_name': 'CTGroup'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'institution': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'ctgroups'", 'null': 'True', 'on_delete': 'models.PROTECT', 'to': u"orm['administration.Institution']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'status': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'status_institution': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'visibility': ('django.db.models.fields.IntegerField', [], {})
        },
        u'administration.ctgroupuser': {
            'Meta': {'unique_together': "((u'user', u'ct_group'),)", 'object_name': 'CTGroupUser'},
            'ct_group': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'users'", 'on_delete': 'models.PROTECT', 'to': u"orm['administration.CTGroup']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'status': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'type_user': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'ctgroups'", 'on_delete': 'models.PROTECT', 'to': u"orm['login.RebecUser']"})
        },
        u'administration.institution': {
            'Meta': {'object_name': 'Institution'},
            'city': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'cnpj': ('django.db.models.fields.CharField', [], {'max_length': '18', 'null': 'True', 'blank': 'True'}),
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['administration.Country']", 'on_delete': 'models.PROTECT'}),
            'cpf': ('django.db.models.fields.CharField', [], {'max_length': '14', 'null': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'institution_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['administration.InstitutionType']", 'on_delete': 'models.PROTECT'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'postal_address': ('django.db.models.fields.CharField', [], {'max_length': '300', 'blank': 'True'}),
            'state': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'default': "u'M'", 'max_length': '1'}),
            'temp_workflow_observation': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'type_person': ('django.db.models.fields.CharField', [], {'default': "u'PF'", 'max_length': '2'}),
            'zip_code': ('django.db.models.fields.CharField', [], {'max_length': '18', 'null': 'True', 'blank': 'True'})
        },
        u'administration.institutiontype': {
            'Meta': {'object_name': 'InstitutionType'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'administration.interventionassignment': {
            'Meta': {'object_name': 'InterventionAssignment'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'administration.interventioncode': {
            'Meta': {'object_name': 'InterventionCode'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'administration.language': {
            'Meta': {'object_name': 'Language'},
            'code': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'administration.maskingtype': {
            'Meta': {'object_name': 'MaskingType'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'administration.observationstudydesign': {
            'Meta': {'object_name': 'ObservationStudyDesign'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'administration.setup': {
            'Meta': {'object_name': 'Setup'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'language_1_code': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'setup_set_lang1'", 'null': 'True', 'to': u"orm['administration.Language']"}),
            'language_1_mandatory': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'language_2_code': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'setup_set_lang2'", 'null': 'True', 'to': u"orm['administration.Language']"}),
            'language_2_mandatory': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'language_3_code': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'setup_set_lang3'", 'null': 'True', 'to': u"orm['administration.Language']"}),
            'language_3_mandatory': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'terms_of_use': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'terms_of_use_lang2': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'terms_of_use_lang3': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'})
        },
        u'administration.setupidentifier': {
            'Meta': {'object_name': 'SetupIdentifier'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'issuing_body': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'mandatory': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'mask': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'administration.statedayslimit': {
            'Meta': {'object_name': 'StateDaysLimit'},
            'green': ('django.db.models.fields.IntegerField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'red': ('django.db.models.fields.IntegerField', [], {'max_length': '100'}),
            'setup': ('django.db.models.fields.related.ForeignKey', [], {'default': '1', 'to': u"orm['administration.Setup']"}),
            'state': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['workflows.State']", 'unique': 'True'}),
            'yellow': ('django.db.models.fields.IntegerField', [], {'max_length': '100'})
        },
        u'administration.studyphase': {
            'Meta': {'object_name': 'StudyPhase'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'administration.studypurpose': {
            'Meta': {'object_name': 'StudyPurpose'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'administration.timeperspective': {
            'Meta': {'object_name': 'TimePerspective'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'administration.vocabulary': {
            'Meta': {'unique_together': "(('name', 'version'),)", 'object_name': 'Vocabulary'},
            'current_version': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'usage': ('multiselectfield.db.fields.MultiSelectField', [], {'max_length': '3'}),
            'version': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'administration.vocabularyitem': {
            'Meta': {'object_name': 'VocabularyItem'},
            'code': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'description_lang_1': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'description_lang_2': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'description_lang_3': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'vocabulary': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'vocabulary_items'", 'to': u"orm['administration.Vocabulary']"})
        },
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'login.rebecuser': {
            'Meta': {'object_name': 'RebecUser'},
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['administration.Country']"}),
            'cpf': ('django.db.models.fields.CharField', [], {'max_length': '14', 'null': 'True', 'blank': 'True'}),
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'gender': ('django.db.models.fields.IntegerField', [], {}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'ocuppation': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'passport': ('django.db.models.fields.CharField', [], {'max_length': '14', 'null': 'True', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'race': ('django.db.models.fields.IntegerField', [], {}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'permissions.permission': {
            'Meta': {'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            'content_types': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'content_types'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'})
        },
        u'workflows.state': {
            'Meta': {'ordering': "('name',)", 'object_name': 'State'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'transitions': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'states'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['workflows.Transition']"}),
            'workflow': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'states'", 'to': u"orm['workflows.Workflow']"})
        },
        u'workflows.transition': {
            'Meta': {'object_name': 'Transition'},
            'condition': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'destination': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'destination_state'", 'null': 'True', 'to': u"orm['workflows.State']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'permission': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['permissions.Permission']", 'null': 'True', 'blank': 'True'}),
            'workflow': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'transitions'", 'to': u"orm['workflows.Workflow']"})
        },
        u'workflows.workflow': {
            'Meta': {'object_name': 'Workflow'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'initial_state': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'workflow_state'", 'null': 'True', 'to': u"orm['workflows.State']"}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['permissions.Permission']", 'through': u"orm['workflows.WorkflowPermissionRelation']", 'symmetrical': 'False'})
        },
        u'workflows.workflowpermissionrelation': {
            'Meta': {'unique_together': "(('workflow', 'permission'),)", 'object_name': 'WorkflowPermissionRelation'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'permission': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'permissions'", 'to': u"orm['permissions.Permission']"}),
            'workflow': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['workflows.Workflow']"})
        }
    }

    complete_apps = ['administration']
