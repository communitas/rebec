# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
    	from django.core.management import call_command
    	call_command("loaddata", "administration/migrations/fixtures/languages.json")
        call_command("loaddata", "administration/migrations/fixtures/setup.json")
        call_command("loaddata", "administration/migrations/fixtures/country.json")
        call_command("loaddata", "administration/migrations/fixtures/help.json")
        call_command("loaddata", "administration/migrations/fixtures/help_language.json")
        call_command("loaddata", "administration/migrations/fixtures/items_helps.json")

    complete_apps = ['administration']