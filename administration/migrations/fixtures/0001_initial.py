# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Country'
        db.create_table(u'administration_country', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal(u'administration', ['Country'])

        # Adding model 'InstitutionType'
        db.create_table(u'administration_institutiontype', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=200)),
        ))
        db.send_create_signal(u'administration', ['InstitutionType'])

        # Adding model 'Institution'
        db.create_table(u'administration_institution', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('postal_address', self.gf('django.db.models.fields.CharField')(max_length=300, blank=True)),
            ('city', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('state', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('country', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['administration.Country'], on_delete=models.PROTECT)),
            ('status', self.gf('django.db.models.fields.CharField')(default=u'M', max_length=1)),
            ('institution_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['administration.InstitutionType'], on_delete=models.PROTECT)),
        ))
        db.send_create_signal(u'administration', ['Institution'])

        # Adding model 'Setup'
        db.create_table(u'administration_setup', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('language_1_code', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('language_1_mandatory', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('language_2_code', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('language_2_mandatory', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('language_3_code', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('language_3_mandatory', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('queue_distribution', self.gf('django.db.models.fields.CharField')(max_length=300, null=True, blank=True)),
            ('current_position_qd0', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('terms_of_use', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'administration', ['Setup'])

        # Adding model 'SetupIdentifier'
        db.create_table(u'administration_setupidentifier', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('issuing_body', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('mandatory', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('mask', self.gf('django.db.models.fields.CharField')(max_length=50)),
        ))
        db.send_create_signal(u'administration', ['SetupIdentifier'])

        # Adding model 'Vocabulary'
        db.create_table(u'administration_vocabulary', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('version', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('current_version', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'administration', ['Vocabulary'])

        # Adding unique constraint on 'Vocabulary', fields ['name', 'version']
        db.create_unique(u'administration_vocabulary', ['name', 'version'])

        # Adding model 'VocabularyItem'
        db.create_table(u'administration_vocabularyitem', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('code', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('description_lang_1', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('description_lang_2', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('description_lang_3', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('vocabulary', self.gf('django.db.models.fields.related.ForeignKey')(related_name='vocabulary_items', to=orm['administration.Vocabulary'])),
        ))
        db.send_create_signal(u'administration', ['VocabularyItem'])

        # Adding model 'CTGroup'
        db.create_table(u'administration_ctgroup', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('status_institution', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('institution', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='intitution_items', null=True, on_delete=models.PROTECT, to=orm['administration.Institution'])),
            ('status', self.gf('django.db.models.fields.IntegerField')(default=1)),
            ('visibility', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal(u'administration', ['CTGroup'])

        # Adding model 'CTGroupUser'
        db.create_table(u'administration_ctgroupuser', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('status', self.gf('django.db.models.fields.IntegerField')()),
            ('type_user', self.gf('django.db.models.fields.CharField')(max_length=1)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(related_name='user_items', on_delete=models.PROTECT, to=orm['login.RebecUser'])),
            ('ct_group', self.gf('django.db.models.fields.related.ForeignKey')(related_name='users', on_delete=models.PROTECT, to=orm['administration.CTGroup'])),
        ))
        db.send_create_signal(u'administration', ['CTGroupUser'])

        # Adding unique constraint on 'CTGroupUser', fields ['user', 'ct_group']
        db.create_unique(u'administration_ctgroupuser', ['user_id', 'ct_group_id'])


    def backwards(self, orm):
        # Removing unique constraint on 'CTGroupUser', fields ['user', 'ct_group']
        db.delete_unique(u'administration_ctgroupuser', ['user_id', 'ct_group_id'])

        # Removing unique constraint on 'Vocabulary', fields ['name', 'version']
        db.delete_unique(u'administration_vocabulary', ['name', 'version'])

        # Deleting model 'Country'
        db.delete_table(u'administration_country')

        # Deleting model 'InstitutionType'
        db.delete_table(u'administration_institutiontype')

        # Deleting model 'Institution'
        db.delete_table(u'administration_institution')

        # Deleting model 'Setup'
        db.delete_table(u'administration_setup')

        # Deleting model 'SetupIdentifier'
        db.delete_table(u'administration_setupidentifier')

        # Deleting model 'Vocabulary'
        db.delete_table(u'administration_vocabulary')

        # Deleting model 'VocabularyItem'
        db.delete_table(u'administration_vocabularyitem')

        # Deleting model 'CTGroup'
        db.delete_table(u'administration_ctgroup')

        # Deleting model 'CTGroupUser'
        db.delete_table(u'administration_ctgroupuser')


    models = {
        u'administration.country': {
            'Meta': {'object_name': 'Country'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'administration.ctgroup': {
            'Meta': {'object_name': 'CTGroup'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'institution': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'intitution_items'", 'null': 'True', 'on_delete': 'models.PROTECT', 'to': u"orm['administration.Institution']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'status': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'status_institution': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'visibility': ('django.db.models.fields.IntegerField', [], {})
        },
        u'administration.ctgroupuser': {
            'Meta': {'unique_together': "((u'user', u'ct_group'),)", 'object_name': 'CTGroupUser'},
            'ct_group': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'users'", 'on_delete': 'models.PROTECT', 'to': u"orm['administration.CTGroup']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'status': ('django.db.models.fields.IntegerField', [], {}),
            'type_user': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'user_items'", 'on_delete': 'models.PROTECT', 'to': u"orm['login.RebecUser']"})
        },
        u'administration.institution': {
            'Meta': {'object_name': 'Institution'},
            'city': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['administration.Country']", 'on_delete': 'models.PROTECT'}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'institution_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['administration.InstitutionType']", 'on_delete': 'models.PROTECT'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'postal_address': ('django.db.models.fields.CharField', [], {'max_length': '300', 'blank': 'True'}),
            'state': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'default': "u'M'", 'max_length': '1'})
        },
        u'administration.institutiontype': {
            'Meta': {'object_name': 'InstitutionType'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'administration.setup': {
            'Meta': {'object_name': 'Setup'},
            'current_position_qd0': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'language_1_code': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'language_1_mandatory': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'language_2_code': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'language_2_mandatory': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'language_3_code': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'language_3_mandatory': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'queue_distribution': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'terms_of_use': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'})
        },
        u'administration.setupidentifier': {
            'Meta': {'object_name': 'SetupIdentifier'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'issuing_body': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'mandatory': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'mask': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'administration.vocabulary': {
            'Meta': {'unique_together': "(('name', 'version'),)", 'object_name': 'Vocabulary'},
            'current_version': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'version': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'administration.vocabularyitem': {
            'Meta': {'object_name': 'VocabularyItem'},
            'code': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'description_lang_1': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'description_lang_2': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'description_lang_3': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'vocabulary': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'vocabulary_items'", 'to': u"orm['administration.Vocabulary']"})
        },
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'login.rebecuser': {
            'Meta': {'object_name': 'RebecUser'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'gender': ('django.db.models.fields.IntegerField', [], {}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'ocuppation': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'race': ('django.db.models.fields.IntegerField', [], {}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        }
    }

    complete_apps = ['administration']