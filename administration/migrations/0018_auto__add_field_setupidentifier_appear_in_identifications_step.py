# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'SetupIdentifier.appear_in_identifications_step'
        db.add_column(u'administration_setupidentifier', 'appear_in_identifications_step',
                      self.gf('django.db.models.fields.BooleanField')(default=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'SetupIdentifier.appear_in_identifications_step'
        db.delete_column(u'administration_setupidentifier', 'appear_in_identifications_step')


    models = {
        u'administration.allocationtype': {
            'Meta': {'object_name': 'AllocationType'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'administration.attachment': {
            'Meta': {'object_name': 'Attachment'},
            'attachment_type': ('multiselectfield.db.fields.MultiSelectField', [], {'max_length': '11'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max_file_size': ('django.db.models.fields.DecimalField', [], {'default': '0', 'null': 'True', 'max_digits': '10', 'decimal_places': '2', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'required': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'setup': ('django.db.models.fields.related.ForeignKey', [], {'default': '1', 'to': u"orm['administration.Setup']"})
        },
        u'administration.country': {
            'Meta': {'ordering': "[u'description']", 'object_name': 'Country'},
            'code': ('django.db.models.fields.CharField', [], {'max_length': '2', 'primary_key': 'True'}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'administration.ctgroup': {
            'Meta': {'object_name': 'CTGroup'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'institution': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'ctgroups'", 'null': 'True', 'on_delete': 'models.PROTECT', 'to': u"orm['administration.Institution']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'status': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'status_institution': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        u'administration.ctgrouplog': {
            'Meta': {'ordering': "(u'date',)", 'object_name': 'CTGroupLog'},
            'ctgroup': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['administration.CTGroup']"}),
            'date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('django.db.models.fields.TextField', [], {'max_length': '1000'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['login.RebecUser']"})
        },
        u'administration.ctgroupuser': {
            'Meta': {'unique_together': "((u'user', u'ct_group'),)", 'object_name': 'CTGroupUser'},
            'ct_group': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'users'", 'on_delete': 'models.PROTECT', 'to': u"orm['administration.CTGroup']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'status': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'type_user': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'ctgroups'", 'on_delete': 'models.PROTECT', 'to': u"orm['login.RebecUser']"})
        },
        u'administration.help': {
            'Meta': {'object_name': 'Help'},
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'type_user': ('multiselectfield.db.fields.MultiSelectField', [], {'max_length': '72', 'null': 'True', 'blank': 'True'}),
            'url_name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'primary_key': 'True'})
        },
        u'administration.helplanguage': {
            'Meta': {'unique_together': "((u'help', u'language', u'id'),)", 'object_name': 'HelpLanguage'},
            'help': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'helps'", 'to': u"orm['administration.Help']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'language': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'language'", 'to': u"orm['administration.Language']"}),
            'titulo': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'administration.institution': {
            'Meta': {'ordering': "[u'name']", 'object_name': 'Institution'},
            'city': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'cnpj': ('django.db.models.fields.CharField', [], {'max_length': '18', 'null': 'True', 'blank': 'True'}),
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['administration.Country']", 'on_delete': 'models.PROTECT'}),
            'cpf': ('django.db.models.fields.CharField', [], {'max_length': '14', 'null': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '1000'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'institution_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['administration.InstitutionType']", 'on_delete': 'models.PROTECT'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'postal_address': ('django.db.models.fields.CharField', [], {'max_length': '300', 'blank': 'True'}),
            'state': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'default': "u'M'", 'max_length': '1'}),
            'temp_workflow_observation': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'type_person': ('django.db.models.fields.CharField', [], {'default': "u'PF'", 'max_length': '2'}),
            'zip_code': ('django.db.models.fields.CharField', [], {'max_length': '18', 'null': 'True', 'blank': 'True'})
        },
        u'administration.institutiontype': {
            'Meta': {'object_name': 'InstitutionType'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'description_english': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'administration.interventionassignment': {
            'Meta': {'object_name': 'InterventionAssignment'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'administration.interventioncode': {
            'Meta': {'object_name': 'InterventionCode'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'administration.itemshelps': {
            'Meta': {'unique_together': "((u'help_language', u'id'),)", 'object_name': 'ItemsHelps'},
            'animation': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'backdrop': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'backdropPadding': ('django.db.models.fields.IntegerField', [], {'max_length': '100'}),
            'content': ('django.db.models.fields.TextField', [], {}),
            'element': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'help_language': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'helps_language'", 'to': u"orm['administration.HelpLanguage']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'next': ('django.db.models.fields.IntegerField', [], {'max_length': '100'}),
            'onHidden': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'onHide': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'onNext': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'onPrev': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'onShow': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'onShown': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'path': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'placement': ('django.db.models.fields.CharField', [], {'default': "'r'", 'max_length': '6'}),
            'prev': ('django.db.models.fields.IntegerField', [], {'max_length': '100'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'administration.language': {
            'Meta': {'object_name': 'Language'},
            'code': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'administration.maskingtype': {
            'Meta': {'object_name': 'MaskingType'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'administration.observationstudydesign': {
            'Meta': {'object_name': 'ObservationStudyDesign'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'administration.setup': {
            'Meta': {'object_name': 'Setup'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'language_1_code': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'setup_set_lang1'", 'null': 'True', 'to': u"orm['administration.Language']"}),
            'language_1_mandatory': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'language_2_code': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'setup_set_lang2'", 'null': 'True', 'to': u"orm['administration.Language']"}),
            'language_2_mandatory': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'language_3_code': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'setup_set_lang3'", 'null': 'True', 'to': u"orm['administration.Language']"}),
            'language_3_mandatory': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'terms_of_use': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'terms_of_use_lang2': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'terms_of_use_lang3': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'})
        },
        u'administration.setupidentifier': {
            'Meta': {'object_name': 'SetupIdentifier'},
            'appear_in_identifications_step': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'help_text_lang_1': ('django.db.models.fields.TextField', [], {'max_length': '1000', 'null': 'True', 'blank': 'True'}),
            'help_text_lang_2': ('django.db.models.fields.TextField', [], {'max_length': '1000', 'null': 'True', 'blank': 'True'}),
            'help_text_lang_3': ('django.db.models.fields.TextField', [], {'max_length': '1000', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'issuing_body': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'mandatory': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'mask': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'administration.statedayslimit': {
            'Meta': {'object_name': 'StateDaysLimit'},
            'green': ('django.db.models.fields.IntegerField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'red': ('django.db.models.fields.IntegerField', [], {'max_length': '100'}),
            'setup': ('django.db.models.fields.related.ForeignKey', [], {'default': '1', 'to': u"orm['administration.Setup']"}),
            'state': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['workflows.State']", 'unique': 'True'}),
            'yellow': ('django.db.models.fields.IntegerField', [], {'max_length': '100'})
        },
        u'administration.studyphase': {
            'Meta': {'object_name': 'StudyPhase'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'administration.studypurpose': {
            'Meta': {'object_name': 'StudyPurpose'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'administration.timeperspective': {
            'Meta': {'object_name': 'TimePerspective'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'login.rebecuser': {
            'Meta': {'object_name': 'RebecUser'},
            'country': ('django.db.models.fields.related.ForeignKey', [], {'default': '1', 'to': u"orm['administration.Country']"}),
            'cpf': ('django.db.models.fields.CharField', [], {'max_length': '14', 'null': 'True', 'blank': 'True'}),
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'gender': ('django.db.models.fields.IntegerField', [], {}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'ocuppation': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'passport': ('django.db.models.fields.CharField', [], {'max_length': '14', 'null': 'True', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'race': ('django.db.models.fields.IntegerField', [], {}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'permissions.permission': {
            'Meta': {'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            'content_types': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'content_types'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'})
        },
        u'workflows.state': {
            'Meta': {'ordering': "('name',)", 'object_name': 'State'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'transitions': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'states'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['workflows.Transition']"}),
            'workflow': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'states'", 'to': u"orm['workflows.Workflow']"})
        },
        u'workflows.transition': {
            'Meta': {'object_name': 'Transition'},
            'condition': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'destination': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'destination_state'", 'null': 'True', 'to': u"orm['workflows.State']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'permission': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['permissions.Permission']", 'null': 'True', 'blank': 'True'}),
            'workflow': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'transitions'", 'to': u"orm['workflows.Workflow']"})
        },
        u'workflows.workflow': {
            'Meta': {'object_name': 'Workflow'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'initial_state': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'workflow_state'", 'null': 'True', 'to': u"orm['workflows.State']"}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['permissions.Permission']", 'through': u"orm['workflows.WorkflowPermissionRelation']", 'symmetrical': 'False'})
        },
        u'workflows.workflowpermissionrelation': {
            'Meta': {'unique_together': "(('workflow', 'permission'),)", 'object_name': 'WorkflowPermissionRelation'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'permission': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'permissions'", 'to': u"orm['permissions.Permission']"}),
            'workflow': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['workflows.Workflow']"})
        }
    }

    complete_apps = ['administration']