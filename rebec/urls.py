#-*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView
from django.contrib import admin
from django.conf import settings
from django.contrib.auth.views import logout
from registration.views import register
from login.forms import UserCreationForm, AuthenticationForm
admin.autodiscover()
from django.core.urlresolvers import reverse_lazy
from administration.views import DashboardView, HelpView, AccessibilityView
from django.contrib.auth.decorators import login_required
from portal import views as view_portal
from login.views import ResendActivationEmail, login

from django.conf.urls.i18n import i18n_patterns
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = patterns('',
    url(r'^dashboard/$', login_required(DashboardView.as_view()), name="home"),
    url(r'^accessibility/$', login_required(AccessibilityView.as_view()), name="accessibility"),
    url(r'^i18n/setlang', view_portal.set_language),
    url(r'^i18n/', include('django.conf.urls.i18n')),
    url(r'^login/$', login, {'template_name': 'registration/login.html', 'authentication_form': AuthenticationForm}, name='login'),
    url(r'^logout/$', logout, {'template_name': 'registration/logout.html',
                               'next_page': '/'}, name='logout'),
    url(r'^register/$',register, {'backend': 'registration.backends.default.DefaultBackend', 'form_class': UserCreationForm},name='registration_register'),
    url(r'^administration/', include('administration.urls')),
    url(r'^clinicaltrials/', include('clinical_trials.urls')),
    url(r'^import-export-config/', include('import_export_config.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^select2/', include('django_select2.urls')),
    url(r'^news/', include('cmsplugin_news.urls')),
    url(r'^registered/$',
        view_portal.ClinicalTrialRegisteredList.as_view(),
        name='ct_registered_list'),
    url(r'^recruiting/$',
        view_portal.ClinicalTrialRecruitingList.as_view(),
        name='ct_recruiting_list'),
    url(r'^scoreboard/$',
        view_portal.ClinicalTrialSummaryView.as_view(),
        name='ct_summay'),
    url(r'^search/$',
        view_portal.SearchView.as_view(),
        name='search'),
    # url(r'^help/$',
    #     HelpView.as_view(), name=u'home_help'),
    url(r'^resend-activation/$', (ResendActivationEmail.as_view()), name="resend_activation_email"),
    url(r'', include('registration.backends.default.urls')),
    url(r'', include('django.contrib.auth.urls')),
)

urlpatterns += staticfiles_urlpatterns()

urlpatterns += i18n_patterns('',
    url(r'^', include('cms.urls')),
)


handler404 = 'portal.views.handler404'
handler500 = 'portal.views.handler500'
handler403 = 'portal.views.handler403'

from django.conf import settings
if settings.DEBUG:
    urlpatterns += patterns('',
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
            'document_root': settings.MEDIA_ROOT,
        }),
        url(r'^static/(?P<path>.*)$', 'django.views.static.serve', {
            'document_root': settings.STATIC_ROOT,
        }),
    )
