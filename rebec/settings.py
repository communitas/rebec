# -*- coding: utf-8 -*-
"""
Django settings for rebec project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'fs+2w^(v(jw5$d1i$dk8qr6@#f3r_(5yy0j6owrnr1e5%8aq%x'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []

AUTH_USER_MODEL = u'login.RebecUser'

# Application definition

INSTALLED_APPS = (
    u'django.contrib.admin',
    u'django.contrib.auth',
    u'django.contrib.contenttypes',
    u'django.contrib.sessions',
    u'django.contrib.messages',
    u'django.contrib.staticfiles',
    u'django.contrib.sites',
    u'south',
    u'login',
    u'registration',
    u'workflows',
    u'administration',
    u'clinical_trials',
    u'tests',
    u'django_select2',
    u'log',
    u'multiselectfield',
    u'permissions',
    u'cms',  # django CMS itself
    u'mptt',  # utilities for implementing a modified pre-order traversal tree
    u'menus',  # helper for model independent hierarchical website navigation
    u'south',  # intelligent schema and data migrations
    u'sekizai',  # for javascript and css management
    u'djangocms_admin_style',  # for the admin skin. You **must** add 'djangocms_admin_style' in the list **before** 'django.contrib.admin'.
    u'django.contrib.messages',  # to enable messages framework (see :ref:`Enable messages <enable-messages>`)
    u'djangocms_file',
    u'djangocms_picture',
    u'djangocms_link',
    u'djangocms_googlemap',
    #u'djangocms_text_ckeditor',
    u'djangocms_column',
    u'cmsplugin_news',
    u'cmsplugin_contact',
    u'portal',
    u'import_export_config',
)

MIDDLEWARE_CLASSES = (
    u'django.middleware.csrf.CsrfViewMiddleware',
    u'django.contrib.sessions.middleware.SessionMiddleware',
    u'django.middleware.locale.LocaleMiddleware',
    u'django.contrib.auth.middleware.AuthenticationMiddleware',
    u'django.contrib.messages.middleware.MessageMiddleware',
    u'django.middleware.doc.XViewMiddleware',
    u'django.middleware.clickjacking.XFrameOptionsMiddleware',
    u'cms.middleware.user.CurrentUserMiddleware',
    u'cms.middleware.page.CurrentPageMiddleware',
    u'cms.middleware.toolbar.ToolbarMiddleware',
    # u'cms.middleware.language.LanguageCookieMiddleware',
    u'cmsplugin_contact.middleware.ForceResponseMiddleware',
    u'log.middleware.LogModel',
    u'django.middleware.common.CommonMiddleware',
)

ROOT_URLCONF = u'rebec.urls'

WSGI_APPLICATION = u'rebec.wsgi.application'

SITE_ID = 1

# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3') ,
        'USER': '',
        'PASSWORD': '',
        'HOST': '',
    },
    'published_cts': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': 'published_cts.sqlite3'
    }
}
AUTHENTICATION_BACKENDS = ('django.contrib.auth.backends.ModelBackend',
                           'administration.backends.EmailBackend',
                             )
# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

TIME_ZONE = 'America/Sao_Paulo'

USE_I18N = True

USE_L10N = True

USE_TZ = False

DATE_INPUT_FORMATS = [u'%d/%m/%Y', u'%Y-%m-%d', u'%d/%m/%y']

from django.utils.translation import ugettext_lazy as _

LANGUAGE_CODE = 'en'
LANGUAGES = (
    ('en', _('English')),
    ('pt', _('Portuguese')),
)

CMS_LANGUAGE_CONF = {
    'en':['en'],
    'pt':['pt'],
}

CMS_SITE_LANGUAGES = {
    1:['en','pt'],
}

CMS_FRONTEND_LANGUAGES = ("en", "pt")

CMS_LANGUAGES ={
    1: [   {   'code': 'en',
               'fallbacks': ['en'],
               'hide_untranslated': True,
               'name': _('English'),
               'public': True,
               'redirect_on_fallback': True},
           {   'code': 'pt',
               'fallbacks': ['pt'],
               'hide_untranslated': True,
               'name': _('Portuguese'),
               'public': True,
               'redirect_on_fallback': True},
    ]
}

LOCALE_PATHS = (os.path.join(BASE_DIR, 'locale'),)

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

TEMPLATE_DIRS = (
    os.path.join(BASE_DIR, "templates"),
)

# STATIC_ROOT = os.path.join(BASE_DIR, "static")
STATIC_ROOT = os.path.join(BASE_DIR, "static_root")
STATIC_URL = '/static/'

MEDIA_ROOT = os.path.join(BASE_DIR, "media")
MEDIA_URL = "/media/"

# For global static files
STATICFILES_DIRS = (
    os.path.join(BASE_DIR, "static"),
    os.path.join(BASE_DIR, "media"),
)

APPEND_SLASH=True
LOGIN_REDIRECT_URL = '/dashboard/'

JQUERY_JS = '%sjs/jquery-1.10.2.js' % STATIC_URL
JQUERY_UI_JS = '%sjs/jquery-ui.min.js' % STATIC_URL
JQUERY_UI_CSS = '%scss/jquery-ui.min.css' % STATIC_URL

LOGIN_URL = '/login/'
LOGOUT_URL = '/logout/'

ACCOUNT_ACTIVATION_DAYS = 7

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = ''
EMAIL_PORT = 0
EMAIL_USE_TLS = True

EMAIL_HOST_USER = ''
EMAIL_HOST_PASSWORD = ''

DEFAULT_FROM_EMAIL = 'desenvrebec@gmail.com'
RECAPTCHA_PUBLIC_KEY = '6Lci5_oSAAAAADDmW1Jo49i7mYegoOYKkVnh-CGM'
RECAPTCHA_PRIVATE_KEY = '6Lci5_oSAAAAAKq-lsE-Cy9RcJT2S060kPhyb5d1'

MONGO_HOST = 'localhost'
MONGO_PORT = 27017
MONGO_USER = ''
MONGO_PASS = ''
MONGO_DATABASE_NAME = 'rebec'


TEMPLATE_CONTEXT_PROCESSORS = (
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.core.context_processors.static",
    "django.core.context_processors.tz",
    "django.contrib.messages.context_processors.messages",
    "django.core.context_processors.request",
    "administration.views._get_help",
    "sekizai.context_processors.sekizai",
    "cms.context_processors.cms_settings",

)

CMS_TEMPLATES = (
    ('portal/base_portal.html', 'base_template'),
    ('portal/home.html', 'Home'),
)


CMSPLUGIN_CONTACT_FORMS = (
    ('cmsplugin_contact.forms.ContactForm', 'default'),
)

SITE_URL = u'http://rebec.icict.fiocruz.br'

PUBLIC_TRIALS_XML_CACHE_KEY = u'ALL_PUBLIC_TRIALS_XML'

try:
    from localsettings import *
except ImportError, e:
    print e
