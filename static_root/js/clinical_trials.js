$('.fields-wrapper .col-sm-3').on('click', function() {
	var span_icon = $(this).find('span:eq(0)');
	if(span_icon.hasClass('glyphicon-play')){
		span_icon.addClass('caret').removeClass('glyphicon-play');
		$(this).parent().next('div.row').slideDown();
	}
	else {
  		if(span_icon.hasClass('caret')){
    		span_icon.addClass('glyphicon-play').removeClass('caret');
    		$(this).parent().next('div.row').slideUp();
  		}
	}
});

$('.fields-wrapper input, .fields-wrapper textarea').on('focusin',
  function(){
		$(this).parents('.fields-wrapper').addClass('focused');
  }
);

$('.fields-wrapper input, .fields-wrapper textarea').on('focusout',
  function(){
		$(this).parents('.fields-wrapper').removeClass('focused');
  }
);
$('span.field-help, img.field-error').tooltip({'trigger': 'click'});
$('.field-error').parents('.fields-wrapper').addClass('with-error');
$('.field-error').parents('.input').addClass('with-error');
$('option').each(function()
    {
        if( $(this).text() == "---------" ) {
          $(this).text("");
    }
    });

$(document).on('hidden.bs.modal', function (e) {
    $(e.target).removeData('bs.modal');
});