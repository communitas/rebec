function link_barra_brasil () {
    $('#barra-brasil a').click(function(e) {
        var id = $(this).data("windowid");
        if(id == null || id.closed) {
            id =  window.open($(this).attr("href"), '_blank');
        }
        id.focus();
        $(this).data("windowid", id);
        e.preventDefault();
        return false;
        }); 
}

function link_search () {
    setTimeout(function(){
        $('.google-search a').removeAttr('target');
        $('.google-search a').removeAttr('dir');
        $('.google-search a').removeAttr('data-cturl');
        $('.google-search a').removeAttr('data-ctorig');
        $('.gsc-cursor-page').click(function() { link_search() });
        $('.gsc-option').click(function() { link_search() });
    },3500);
}

$(document).ready(function() {
    $(".menu_home > li > a").click( function(e) {
        e.stopPropagation();
        e.preventDefault();
    })
});
