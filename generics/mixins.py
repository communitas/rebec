# -*- coding: utf-8 -*-
from django.core.exceptions import PermissionDenied

class PermissionRequiredMixin(object):
    """ Verify if user has at least one of permissions. 
    """
    permissions_required = []

    def check_permissions(self, user):
        for permission in self.permissions_required:
            if user.has_perm(permission):
                return True
        return False

    def dispatch(self, request, *args, **kwargs):
        if not self.check_permissions(request.user):
            raise PermissionDenied()
        return super(PermissionRequiredMixin, self).dispatch(request, *args, **kwargs)
